cmake_minimum_required(VERSION 3.6)

set(gtest_force_shared_crt ON CACHE BOOL "")

add_definitions(-DUNICODE -D_UNICODE -D_CRT_SECURE_NO_WARNINGS)

set(NoOpenGL 1)
if (DEFINED NoOpenGL)
	add_definitions(-DNoOpenGL)
endif()

add_compile_options($<$<CXX_COMPILER_ID:MSVC>:/MP>)
add_compile_options("/std:c++17")

# First for the generic no-config case (e.g. with mingw)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/Bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/Lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/Lib)

# Second, for multi-config builds (e.g. msvc)
foreach(OUTPUTCONFIG ${CMAKE_CONFIGURATION_TYPES})
	string(TOUPPER ${OUTPUTCONFIG} OUTPUTCONFIG)
	#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CMAKE_SOURCE_DIR}/Bin)
	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CMAKE_SOURCE_DIR}/Lib)
	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CMAKE_SOURCE_DIR}/Lib)
endforeach(OUTPUTCONFIG CMAKE_CONFIGURATION_TYPES)

set(THIRD_PARTY_DIR 3rdParty)

set(THIRD_PARTY_INCLUDE_DIRS
	${THIRD_PARTY_DIR}/assimp/include
	${THIRD_PARTY_DIR}/glew-2.0.0/include
	${THIRD_PARTY_DIR}/glm-0.9.7.6
	${THIRD_PARTY_DIR}/gli/
	${THIRD_PARTY_DIR}/DirectXTex/inc
	${THIRD_PARTY_DIR}/FBX/2015.1/include
	${THIRD_PARTY_DIR}/json/include
	${THIRD_PARTY_DIR}/rttr/include
	${THIRD_PARTY_DIR}/WinPixEventRuntime/Include/WinPixEventRuntime)

include_directories(${THIRD_PARTY_INCLUDE_DIRS})

set(THIRD_PARTY_LIB_DIRS
	${THIRD_PARTY_DIR}/assimp/bin/x64
	${THIRD_PARTY_DIR}/assimp/lib/x64
	${THIRD_PARTY_DIR}/glew-2.0.0/lib/Release/x64
	${THIRD_PARTY_DIR}/DirectXTex/lib/x64
	${THIRD_PARTY_DIR}/FBX/2015.1/lib/vs2013/x64/release
	${THIRD_PARTY_DIR}/rttr/bin
	${THIRD_PARTY_DIR}/rttr/lib
	${THIRD_PARTY_DIR}/WinPixEventRuntime/bin)

link_directories(${THIRD_PARTY_LIB_DIRS})

project(Practices)
	add_subdirectory(Client)
	add_subdirectory(Core)
	add_subdirectory(ImGui)
	add_subdirectory(RHI)
	add_subdirectory(Samples)
