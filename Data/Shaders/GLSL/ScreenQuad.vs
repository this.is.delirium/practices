#version 430

layout(location = 0) in vec2 POSITION;
layout(location = 1) in vec2 TEXCOORD;

layout(std140, binding = 0) uniform uMatrixPerFrame
{
  mat4 uViewProjectionMatrix;
};

out vec2 TexCoordFromVS;
void main()
{
  gl_Position = uViewProjectionMatrix * vec4(POSITION.xy, 0.0, 1.0);
  TexCoordFromVS = TEXCOORD;
}
