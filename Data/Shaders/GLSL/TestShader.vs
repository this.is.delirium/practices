#version 430 core

layout(location = 0) in vec3 POSITION;
layout(location = 1) in vec3 TANGENT;
layout(location = 2) in vec3 BINORMAL;
layout(location = 3) in vec3 NORMAL;
layout(location = 4) in vec2 TEXCOORD;
layout(location = 5) in vec2 TEXCOORD1;

layout(std140, binding = 0) uniform uMatrixPerFrame
{
	mat4	uViewProjectionMatrix;
};

layout(std140, binding = 1) uniform uMatrixPerObject
{
	mat4	uWorldMatrix;
};

out vec2 TexCoords;
out vec4 Color;
void main()
{
	gl_Position = uViewProjectionMatrix * uWorldMatrix * vec4(POSITION, 1.0);
	TexCoords = TEXCOORD;
	Color = vec4(NORMAL.xyz * 0.5 + 0.5, 1.0);
}
