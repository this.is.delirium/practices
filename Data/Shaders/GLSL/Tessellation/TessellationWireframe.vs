#version 430

layout(location = 0) in vec3 POSITION;

out vec3 vPosition;

void main()
{
	vPosition = POSITION;
}
