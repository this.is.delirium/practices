#version 430

layout(vertices = 3) out;

layout(std140, binding = 0) uniform TesselationParams
{
	vec4 innerLevel;
	vec4 outerLevel;
};

in vec3 vPosition[];
out vec3 tcPosition[];

#define ID gl_InvocationID

void main()
{
	tcPosition[ID] = vPosition[ID];
	if (ID == 0)
	{
		gl_TessLevelInner[0] = innerLevel.x;

		gl_TessLevelOuter[0] = outerLevel.x;
		gl_TessLevelOuter[1] = outerLevel.y;
		gl_TessLevelOuter[2] = outerLevel.z;
	}
}
