#version 430

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Velocity;
layout (location = 2) in vec3 Color;
layout (location = 3) in vec2 Settings; // x - life, y - size
layout (location = 4) in int Type;

out vec3	PositionFromVS;
out vec3	VelocityFromVS;
out vec3	ColorFromVS;
out vec2	SettingsFromVS;
out int		TypeFromVS;

void main()
{
	PositionFromVS	= Position;
	VelocityFromVS	= Velocity;
	ColorFromVS		= Color;
	SettingsFromVS	= Settings;
	TypeFromVS		= Type;
}
