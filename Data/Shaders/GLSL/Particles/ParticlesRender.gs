#version 430

layout(std140, binding = 1) uniform uMatrixPerFrame
{
	mat4	uViewProjectionMatrix;
};

layout(std140, binding = 0) uniform uCameraVectors
{
	vec4 CamUp;
	vec4 CamRight;
};

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in vec3	PositionFromVS[];
in vec3	VelocityFromVS[];
in vec3	ColorFromVS[];
in vec2	SettingsFromVS[];
in int		TypeFromVS[];

flat out vec4 ColorFromGS;

void main()
{
	if(TypeFromVS[0] != 0)
	{
		vec4 oldPos = vec4(PositionFromVS[0], 1.0);
		float size = SettingsFromVS[0].y;

		ColorFromGS = vec4(ColorFromVS[0], SettingsFromVS[0].x);

		vec4 curPos = oldPos + (-CamUp - CamRight) * size;
		gl_Position = uViewProjectionMatrix * vec4(curPos.xyz, 1.0);
		EmitVertex();

		curPos = oldPos + (-CamUp + CamRight) * size;
		gl_Position = uViewProjectionMatrix * vec4(curPos.xyz, 1.0);
		EmitVertex();

		curPos = oldPos+(CamUp - CamRight) * size;
		gl_Position = uViewProjectionMatrix * vec4(curPos.xyz, 1.0);
		EmitVertex();

		curPos = oldPos+(CamUp + CamRight) * size;
		gl_Position = uViewProjectionMatrix * vec4(curPos.xyz, 1.0);
		EmitVertex();

		EndPrimitive();
	}
}
