#version 430

layout(points) in;
layout(points, max_vertices = 40) out;

in vec3	PositionFromVS[];
in vec3	VelocityFromVS[];
in vec3	ColorFromVS[];
in vec2	SettingsFromVS[];
in int		TypeFromVS[];

out vec3	PositionFromGS;
out vec3	VelocityFromGS;
out vec3	ColorFromGS;
out vec2	SettingsFromGS; // x - life time, y - size.
out int		TypeFromGS;

layout(std140, binding = 0) uniform uGeneratorSettings
{
	vec4	Position;
	vec4	MinVelocity;
	vec4	MaxVelocity;
	vec4	GravityVector;
	vec4	Color;
	vec4	Seed;
	vec4 	Settings; // x - minLifeTime, y - maxLifeTime, z - size, w - timePassed
	/*vec2	LifeTime;
	vec2	Size;
	float	TimePassed;*/
	int		NumToGenerate;
};

//! 32-bit state of random number generator.
uint RandState;

// =======================================================================
// function : SeedRand
// purpose  : Applies hash function by Thomas Wang to randomize seeds
//            (see http://www.burtleburtle.net/bob/hash/integer.html)
// =======================================================================
void SeedRand (in vec4 theSeed)
{
	RandState = uint (int (theSeed.x) + int (theSeed.y) + int(theSeed.z));

	RandState = (RandState + 0x479ab41du) + (RandState <<  8);
	RandState = (RandState ^ 0xe4aa10ceu) ^ (RandState >>  5);
	RandState = (RandState + 0x9942f0a6u) - (RandState << 14);
	RandState = (RandState ^ 0x5aedd67du) ^ (RandState >>  3);
	RandState = (RandState + 0x17bea992u) + (RandState <<  7);
}

// =======================================================================
// function : RandInt
// purpose  : Generates integer using Xorshift algorithm by G. Marsaglia
// =======================================================================
uint RandInt()
{
	RandState ^= (RandState << 13);
	RandState ^= (RandState >> 17);
	RandState ^= (RandState <<  5);

	return RandState;
}

// =======================================================================
// function : RandFloat
// purpose  : Generates a random float in [0, 1) range
// =======================================================================
float RandFloat()
{
	return float (RandInt()) * (1.f / 4294967296.f);
}

void main()
{
	SeedRand(Seed);

	PositionFromGS = PositionFromVS[0];
	VelocityFromGS = VelocityFromVS[0];
	if (TypeFromVS[0] != 0)
	{
		PositionFromGS += VelocityFromGS * Settings.w;
		VelocityFromGS += GravityVector.xyz * Settings.w;
	}

	ColorFromGS = ColorFromVS[0];
	SettingsFromGS.x = SettingsFromVS[0].x - Settings.w; // life time
	SettingsFromGS.y = SettingsFromVS[0].y; // size out.
	TypeFromGS = TypeFromVS[0];

	if (TypeFromGS == 0)
	{
		EmitVertex();
		EndPrimitive();

		vec3 velocityRange = (MaxVelocity - MinVelocity).xyz;
		float lifeTimeRange = Settings.y - Settings.x;
		for (int i = 0; i < NumToGenerate; ++i)
		{
			PositionFromGS = Position.xyz;
			VelocityFromGS = MinVelocity.xyz + vec3(velocityRange.x * RandFloat(), velocityRange.y * RandFloat(), velocityRange.z * RandFloat());
			ColorFromGS = Color.xyz;
			SettingsFromGS.x = Settings.x + lifeTimeRange * RandFloat();
			SettingsFromGS.y = Settings.z;
			TypeFromGS = 1;
			EmitVertex();
			EndPrimitive();
		}
	}
	else if (SettingsFromGS.x > 0.0)
	{
		EmitVertex();
		EndPrimitive();
	}
}
