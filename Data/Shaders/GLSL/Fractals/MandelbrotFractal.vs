#version 430 core

layout(location = 0) in vec2 POSITION;

layout(std140, binding = 0) uniform MandelbrotParams
{
  vec2 param;
  vec2 viewPos;
  float viewZoom;
  float viewRatio;
  float anim;
  int iters;
  int type;
  int blinkMode;
};

out vec2 pos;

void main(void) {
	pos = POSITION / viewZoom * vec2(viewRatio, 1.0f) + viewPos;
	gl_Position = vec4(POSITION.xy, 0.0f, 1.0f);
}
