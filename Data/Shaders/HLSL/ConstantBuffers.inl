struct PerFrame
{
	float4x4	ViewProjMatrix;
	float4x4	ViewMatrix;
	float4x4	ProjMatrix;
	float4x4	InvProjMatrix;
	float4x4	InvViewMatrix;
	float4		DepthParams;
	float4		CameraPosition;
	float4		CameraDirection;
	float4		CameraUp;
	float4		WindowParams;

	float ZNear() { return DepthParams.x; }
	float ZFar() { return DepthParams.y; }

	float2 WindowSize() { return WindowParams.xy; }
};

struct PerObject
{
	float4x4	WorldMatrix;
	float4x4	NormalMatrix;
	float4		Color;
};

struct Light
{
	float4x4	ViewProjectionMatrix;
	float4		Position;
	float4		Ambient;
	float4		Diffuse;
	float4		Specular;
	float4		ZRange;
	float4		Attenuation; // w - itensity.
};

struct SunLight
{
	float4 SunDir;
};

struct ScreenQuadParams
{
	int TexType;
};

struct MandelbrotParams
{
	float2	param;
	float2	viewPos;
	float	viewZoom;
	float	viewRatio;
	float	anim;
	int		iters;
	int		type;
	int		blinkMode;
};

struct FrustumRanges
{
	float4	ZFars;
	int		NbOfFrustums;
	float3	frPadding;
};

struct CascadedMatrices
{
	float4x4 LightVP[4];
};

struct ParticlesConstantVariables
{
	uint4 Params;
};

struct ParticlesDynamicVariables
{
	float4 AttractorAndTime; // xyz - attractor, w - frameTime;
};

struct ParticlesTimer
{
	float4 Time;
};

struct ViewFrustumPerFrame
{
	float4x4	ViewProjMatrix;
	float2		WinSize;
};

struct TesselationParams
{
	float4 innerLevel;
	float4 outerLevel;
};

struct SSAOParams
{
	float4	Settings;	// x - radius, y - power, z - scale, w - bias.
	int4	Sizes;		// x - kernel size, y - noise size, zw - window size.
};

struct GaussKernel
{
	float4 Values[64];
};

struct SSAOKernel
{
	float4	Vectors[128];
};

struct SSLRParams
{
	float4 Settings;
};

struct LLClearValue
{
	uint4 Value;
};

struct ParallaxMappingParams
{
    float4 Settings;

    float Scale() { return Settings.x; }
    float DivideZoneX() { return Settings.z; }
    float Type() { return Settings.w; }
};
