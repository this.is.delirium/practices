#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4 Position : SV_POSITION;
	float2 Pos2d : POSITION;
};

ConstantBuffer<MandelbrotParams> Params : register(b0);
