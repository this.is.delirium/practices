#include "MandelbrotFractal.inl"

void VSMain(in ScreenQuadNDCInput In, out PSInput Out)
{
	Out.Pos2d = In.Position / Params.viewZoom * float2(Params.viewRatio, 1.0f) + Params.viewPos;
	Out.Position = float4(In.Position, 0.0f, 1.0f);
}
