#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4 Position	: SV_POSITION;
	float2 UV		: TEXCOORD0;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);

Texture2D DiffuseTexture : register(t0);
SamplerState Sampler : register(s0);
