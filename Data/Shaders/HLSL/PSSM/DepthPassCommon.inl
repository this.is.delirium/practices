#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4	Position	: SV_POSITION;
	float	Depth		: DEPTH;
};

struct PSOutput
{
	float Depth : SV_Depth;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);
