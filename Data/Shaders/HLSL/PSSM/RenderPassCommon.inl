#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4	Position		: SV_POSITION;
	float4	WorldPosition	: WPOSITION;
	float4	LightViewPos[4]	: TEXCOORD2;
	float4	Normal			: NORMAL;
	float2	TextureUV		: TEXCOORD0;
	float2	TexScreenCoords	: TEXCOORD1;
	float	Depth			: DEPTH;
	float	NormDepth		: NORMALIZEDEPTH;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);
ConstantBuffer<FrustumRanges> Ranges : register(b2);
ConstantBuffer<CascadedMatrices> CascadedMatricesCB : register(b3);

Texture2D DiffuseTexture : register(t0);
SamplerState Sampler : register(s0);

Texture2DArray DepthTextures : register(t1);
SamplerComparisonState DepthSampler : register(s1);
