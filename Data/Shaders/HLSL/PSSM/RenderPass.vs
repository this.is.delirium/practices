#include "RenderPassCommon.inl"

void VSMain(in MainInput In, out PSInput Out)
{
	Out.WorldPosition	= mul(PerObjectCB.WorldMatrix, float4(In.Position, 1.0f));
	Out.Position 		= mul(PerFrameCB.ViewProjMatrix, Out.WorldPosition);
	Out.Normal			= mul(PerObjectCB.NormalMatrix, float4(In.Normal, 0.0f));
	Out.TextureUV		= In.TextureUV;
	Out.TexScreenCoords	= Out.Position.xy / Out.Position.w;

	Out.Depth			= Out.Position.z;
}
