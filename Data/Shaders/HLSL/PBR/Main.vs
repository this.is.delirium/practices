#include "Main.inl"

void VSMain(in MainInput In, out PSInput Out)
{
	Out.WorldPos	= mul(PerObjectCB.WorldMatrix, float4(In.Position, 1.0f));

	Out.Position	= mul(PerFrameCB.ViewProjMatrix, Out.WorldPos);
	Out.Normal		= normalize(mul(PerObjectCB.NormalMatrix, float4(In.Normal, 0.0f)));
}
