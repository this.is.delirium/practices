#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4 Position	: SV_Position;
	float4 WorldPos	: WorldPosition;
	float4 Normal	: Normal;

};

struct TestMaterial
{
	float4 Albedo;
	float4 BaseFresnel;
	float4 Params;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);
ConstantBuffer<Light> LightCB : register(b2);
ConstantBuffer<TestMaterial> Material : register(b3);

float3 Albedo()
{
	return Material.Albedo.rgb;
}

float Metallic()
{
	return Material.Params.x;
}

float Roughness()
{
	return Material.Params.y;
}

float AO()
{
	return Material.Params.z;
}

float3 GetF0()
{
	return Material.BaseFresnel.rgb;
}
