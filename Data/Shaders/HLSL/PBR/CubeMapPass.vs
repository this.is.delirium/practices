#include "CubeMapPass.inl"

void VSMain(in MainInput In, out PSInput Out)
{
	Out.LocalPos	= In.Position;
	Out.Position	= mul(PerObjectCB.WorldMatrix, float4(In.Position, 1.0f));
	Out.Position	= mul(PerFrameCB.ViewProjMatrix, Out.Position);
	Out.UV			= In.TextureUV;
}
