#include "MainCommon.inl"

[maxvertexcount(3)]
void GSMain(triangle GSInput In[3], inout TriangleStream<PSInput> OutStream)
{
	PSInput Output;

	float2 vsPos0 = PerFrameCB.WinSize * In[0].Position.xy / In[0].Position.w;
	float2 vsPos1 = PerFrameCB.WinSize * In[1].Position.xy / In[1].Position.w;
	float2 vsPos2 = PerFrameCB.WinSize * In[2].Position.xy / In[2].Position.w;

	float2 edge0 = vsPos2 - vsPos1;
	float2 edge1 = vsPos2 - vsPos0;
	float2 edge2 = vsPos1 - vsPos0;
	float area = abs(edge1.x * edge2.y - edge1.y * edge2.x);


	Output.Distance	= float3(area / length(vsPos0), 0.0f, 0.0f);
	Output.Position	= In[0].Position;
	Output.Color	= In[0].Color;
	OutStream.Append(Output);

	Output.Distance	= float3(0.0f, area / length(vsPos1), 0.0f);
	Output.Position	= In[1].Position;
	Output.Color	= In[1].Color;
	OutStream.Append(Output);

	Output.Distance	= float3(0.0f, 0.0f, area / length(vsPos2));
	Output.Position	= In[2].Position;
	Output.Color	= In[2].Color;
	OutStream.Append(Output);

	OutStream.RestartStrip();
}
