#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct GSInput
{
	float4 Position	: POSITION;
	float4 Color	: COLOR;
};

struct PSInput
{
	float3 Distance	: DISTANCE;
	float4 Position	: SV_POSITION;
	float4 Color	: COLOR;
};

ConstantBuffer<ViewFrustumPerFrame> PerFrameCB : register(b0);
