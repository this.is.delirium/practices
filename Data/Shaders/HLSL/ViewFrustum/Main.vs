#include "MainCommon.inl"

void VSMain(in ViewFrustumInput In, out GSInput Out)
{
	Out.Position	= mul(PerFrameCB.ViewProjMatrix, In.Position);
	Out.Color		= In.Color;
}
