#include "ScreenQuadCommon.inl"

void VSMain(in ScreenQuadInput Input, out VSOutput Output)
{
	Output.Position = mul(PerFrameCB.ViewProjMatrix, float4(Input.Position.xy, 0.0f, 1.0f));
	Output.TexCoord = Input.TexCoord;
}
