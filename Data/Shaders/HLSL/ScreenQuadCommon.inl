#include "ConstantBuffers.inl"
#include "InputLayouts.inl"

struct VSOutput
{
	float4	Position	: SV_POSITION;
	float2	TexCoord	: TEXCOORD;
	float	Depth		: DEPTH;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<ScreenQuadParams> Params : register(b1);

Texture2D psTexture : register(t0);
SamplerState psSampler : register(s0);
