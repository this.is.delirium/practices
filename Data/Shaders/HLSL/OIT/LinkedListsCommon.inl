struct ListNode
{
	uint PackedColor;
	uint DepthAndCoverage;
	uint Next;
};

struct NodeData
{
	uint	PackedColor;
	float	Depth;
};

static const float	kMaxFloat		= 3.402823466e+38F;
static const int	kMaxFragments	= 8;
static const uint	kMagicValue		= 0xFFFFFFFF;

uint packColor(in float4 color)
{
	return (uint(color.r * 255) << 24) | (uint(color.g * 255) << 16) | (uint(color.b * 255) << 8) | uint(color.a * 255);
}

float4 unpackColor(in uint color)
{
	float4 output;
	output.r = float((color >> 24) & 0x000000ff) / 255.0f;
	output.g = float((color >> 16) & 0x000000ff) / 255.0f;
	output.b = float((color >> 8) & 0x000000ff) / 255.0f;
	output.a = float(color & 0x000000ff) / 255.0f;
	return saturate(output);
}
