#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

struct PSOutput
{
	float4 Color : SV_TARGET;
};

Texture2D AccumulatedColor : register(t0);
Texture2D AccumulatedAlpha : register(t1);
Texture2D OpaquePass : register(t2);
SamplerState Sampler : register(s0);
