#include "LinkedListsCommon.inl"
#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4 Position : SV_POSITION;
};

Texture2D<uint> StartOffsets : register(t0);
StructuredBuffer<ListNode> FragmentList : register(t1);
Texture2D<float4> Background : register(t2);

SamplerState BackgroundSampler : register(s0);

int Fill(in uint startIndex, inout NodeData sortedFragments[kMaxFragments])
{
	int counter = 0;
	uint index = startIndex;
	for (int i = 0; i < kMaxFragments; ++i)
	{
		if (index != kMagicValue)
		{
			ListNode node = FragmentList[index];

			sortedFragments[counter].PackedColor	= node.PackedColor;
			sortedFragments[counter].Depth			= f16tof32(node.DepthAndCoverage);

			++counter;

			index = node.Next;
		}
		else
		{
			sortedFragments[i]			= (NodeData) 0;
			sortedFragments[i].Depth	= -kMaxFloat;
		}
	}

	return counter;
}

void ShellSort(in uint startIndex, inout NodeData sortedFragments[kMaxFragments])
{
	NodeData tmp = (NodeData) 0;
	for (int k = kMaxFragments / 2; k > 0; k /= 2)
	{
		for (int i = k; i < kMaxFragments; ++i)
		{
			tmp = sortedFragments[i];
			for (int j = i; j >= k; j -= k)
			{
				if (tmp.Depth > sortedFragments[j - k].Depth)
					sortedFragments[j] = sortedFragments[j - k];
				else
					break;
			}
			sortedFragments[j] = tmp;
		}
	}

}
