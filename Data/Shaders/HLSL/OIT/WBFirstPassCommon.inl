#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4	Position	: SV_POSITION;
	float	Depth		: Depth;
};

struct PSOutput
{
	float4	Accum		: SV_Target0;
	float4	Revealage	: SV_Target1;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);
