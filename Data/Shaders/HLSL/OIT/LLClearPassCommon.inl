#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4 Position : SV_POSITION;
};

ConstantBuffer<LLClearValue> ClearValueCB : register(b0);
