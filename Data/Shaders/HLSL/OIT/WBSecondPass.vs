#include "WBSecondPassCommon.inl"

void VSMain(in ScreenQuadNDCInput In, out PSInput Out)
{
	Out.Position = float4(In.Position.xy, 0.0f, 1.0f);
	Out.TexCoord = In.Position;
}
