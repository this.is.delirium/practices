#include "WBFirstPassCommon.inl"

void VSMain(in MainInput In, out PSInput Out)
{
	Out.Position	= mul(PerObjectCB.WorldMatrix, float4(In.Position, 1.0f));
	Out.Position	= mul(PerFrameCB.ViewProjMatrix, Out.Position);

	float zNear	= PerFrameCB.DepthParams.x;
	float zFar	= PerFrameCB.DepthParams.y;
	Out.Depth	= (Out.Position.z - zNear) / (zFar - zNear);
}
