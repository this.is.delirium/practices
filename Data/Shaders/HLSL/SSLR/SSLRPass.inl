#include "Common.inl"

struct PSInput
{
	float4 Position	: SV_POSITION;
	float2 NDC		: TEXCOORD0;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<SSLRParams> Params : register(b1);

Texture2D<float4>	ColorTexture	: register(t0);
Texture2D<float2>	NormalTexture	: register(t1);
Texture2D<float>	DepthTexture	: register(t2);

SamplerState TexSampler : register(s0);

float4 GetViewPosition(in float2 ndc, in float depth)
{
	float4 viewPos = mul(PerFrameCB.InvProjMatrix, float4(ndc, depth, 1.0f));
	viewPos /= viewPos.w;

	return viewPos;
}

float3 GetViewPosition2(in float2 uv, in float depth)
{
	float4 position = 1.0f; 
 
	position.x = uv.x * 2.0f - 1.0f; 
	position.y = -(uv.y * 2.0f - 1.0f); 

	position.z = depth; 
 
	//Transform Position from Homogenous Space to World Space 
	position = mul(PerFrameCB.InvProjMatrix, position); 
 
	position /= position.w;

	return position.xyz;
}

float4 GetWorldPosition(in float2 ndc, in float depth)
{
	return mul(PerFrameCB.InvViewMatrix, GetViewPosition(ndc, depth));
}

float3 GetUV(in float3 viewPos)
{
	float4 uv = mul(PerFrameCB.ProjMatrix, float4(viewPos, 1.0f));
	uv.xy = float2(0.5f, 0.5f) + float2(0.5f, -0.5f) * uv.xy / uv.w;

	return float3(uv.xy, uv.z / uv.w);
}

float GetDepth(in float2 uv)
{
	return DepthTexture.Sample(TexSampler, uv);
}

float4 GetColor(in float2 uv)
{
	return ColorTexture.Sample(TexSampler, uv);
}

float GetStartL()
{
	return Params.Settings.x;
}

float GetReflectionDistance()
{
	return Params.Settings.y;
}
