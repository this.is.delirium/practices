#include "ConstantBuffers.inl"
#include "InputLayouts.inl"

struct PSInput
{
	float4 Position	: SV_POSITION;
	float4 Normal	: NORMAL;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);
