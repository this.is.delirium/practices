struct MainInput
{
	float3 Position		: POSITION;
	float3 Normal		: NORMAL;
	float3 Bitangent	: BITANGENT;
	float3 Tangent		: TANGENT;
	float2 TextureUV	: TEXCOORD0;
};

struct ScreenQuadInput
{
	float2 Position	: POSITION;
	float2 TexCoord	: TEXCOORD;
};

struct ScreenQuadNDCInput
{
	float2 Position : POSITION;
};

struct ParticlesInput
{
	float4 CurrPosition : POSITION;
	float4 PrevPosition : PREVPOSITION;
};

struct ParticlesVertexIdInput
{
	uint id : SV_VertexID;
};

struct ViewFrustumInput
{
	float4 Position	: POSITION;
	float4 Color	: COLOR;
};

struct RenderToTextureInput
{
	float3 Position : POSITION;
};

struct TessellationInput
{
	float3 Position : POSITION;
};
