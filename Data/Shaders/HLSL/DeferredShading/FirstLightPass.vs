#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct VSOutput
{
	float4 Position : SV_POSITION;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<Light> PerLight : register(b1);

void VSMain(in MainInput In, out VSOutput Out)
{
	Out.Position = mul(PerLight.ViewProjectionMatrix, float4(In.Position, 1.0f));
	Out.Position = mul(PerFrameCB.ViewProjMatrix, Out.Position);
}
