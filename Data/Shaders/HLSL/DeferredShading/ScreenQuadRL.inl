#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4 Position	: SV_POSITION;
	float2 TestPos	: TestPos;
};

struct PSOutput
{
	float4 Color	: SV_TARGET0;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<SunLight> SunLightCB : register(b1);

Texture2D DepthTexture 	: register(t0);
Texture2D NormalTexture	: register(t1);
Texture2D ColorTexture	: register(t2);
Texture2D UnusedTexture	: register(t3);

SamplerState TestSampler : register(s0);
