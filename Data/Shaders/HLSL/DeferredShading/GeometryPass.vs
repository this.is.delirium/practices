#include "GeometryPassCommon.inl"

void VSMain(in MainInput In, out PSInput Out)
{
	Out.Position	= mul(PerObjectCB.WorldMatrix, float4(In.Position, 1.0f));
	Out.Position	= mul(PerFrameCB.ViewProjMatrix, Out.Position);

	Out.WComponent	= Out.Position.w;

	Out.Normal		= mul((float3x3)PerObjectCB.NormalMatrix, In.Normal);
	Out.Normal		= mul((float3x3)PerFrameCB.ViewMatrix, Out.Normal);

	Out.TexCoords	= In.TextureUV;

	float zNear		= PerFrameCB.DepthParams.x;
	float zFar		= PerFrameCB.DepthParams.y;
	Out.Depth		= (Out.Position.z - zNear) / (zFar - zNear);
	//Out.Depth		= Out.Position.z / Out.Position.w;
}
