#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4	Position	: SV_POSITION;
	float3	Normal		: NORMAL;
	float	Depth		: DEPTH;
	float	WComponent	: W;
	float2	TexCoords	: TEXCOORD;
};

struct PSOutput
{
	float4	Diffuse		: SV_TARGET0;
	float2	Normal		: SV_TARGET1;
	float	WComponent	: SV_TARGET2;
	float	Depth		: SV_DEPTH;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);

Texture2D DiffuseTexture : register(t0);
SamplerState Sampler : register(s0);
