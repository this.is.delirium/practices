#include "../InputLayouts.inl"

struct PSInput
{
	float4 Position : SV_POSITION;
	float2 TestPos : POSITION;
};

struct PSOutput
{
	float4 Color : SV_TARGET;
};

Texture2D DiffuseTexture : register(t0);
SamplerState Sampler : register(s0);
