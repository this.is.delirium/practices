#include "GPass.inl"

void VSMain(in MainInput In, out PSInput Out)
{
	float4 worldPos	= mul(PerObjectCB.WorldMatrix, float4(In.Position, 1.0f));

	Out.WorldPos	= worldPos;
	Out.Position	= mul(PerFrameCB.ViewProjMatrix, worldPos);

	//-- Translate TBN from LocalSpace to WorldSpace.
	Out.Normal		= mul((float3x3)PerObjectCB.NormalMatrix, In.Normal);
	Out.Tangent		= mul((float3x3)PerObjectCB.NormalMatrix, In.Tangent);
	Out.Bitangent	= mul((float3x3)PerObjectCB.NormalMatrix, In.Bitangent);

	Out.TexCoords	= In.TextureUV;

	Out.Depth		= Out.Position.zw;
}
