#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4 Position		: SV_POSITION;
	float2 TexCoords	: TEXCOORD;
};

struct PSOutput
{
	float4 Color : SV_TARGET;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<Light> PerLight : register(b1);

Texture2D DiffuseTexture 	: register(t0);
Texture2D NormalTexture		: register(t1);
Texture2D DepthTexture		: register(t2);
Texture2D WTexture			: register(t3);

SamplerState TexSampler 	: register(s0);
