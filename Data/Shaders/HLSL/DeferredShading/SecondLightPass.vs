#include "SecondLightPassCommon.inl"

void VSMain(in MainInput In, out PSInput Out)
{
	Out.Position	= mul(PerLight.ViewProjectionMatrix, float4(In.Position, 1.0f));
	Out.Position	= mul(PerFrameCB.ViewProjMatrix, Out.Position);

	Out.TexCoords = (Out.Position.xy / Out.Position.w);
}
