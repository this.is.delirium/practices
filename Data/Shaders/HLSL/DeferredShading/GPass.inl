#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4	Position	: SV_POSITION;
	float4	WorldPos	: WORLDPOS;
	float3	Normal		: NORMAL;
	float3	Tangent		: TANGENT;
	float3	Bitangent	: BITANGENT;
	float2	Depth		: DEPTH;
	float2	TexCoords	: TEXCOORD0;
};

struct PSOutput
{
	float	DepthRT	: SV_TARGET0;
	float2	Normal	: SV_TARGET1;
	float4	Albedo	: SV_TARGET2;
	float4	Unused	: SV_TARGET3;
	float	Depth	: SV_DEPTH;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);

Texture2D AlbedoTexture : register(t0);
Texture2D NormalTexture : register(t1);
Texture2D SpecularTexture : register(t2);

SamplerState Sampler : register(s0);
