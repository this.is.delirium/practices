#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct VSOutput
{
	float4 CurrPosition	: SV_POSITION;
	float4 PrevPosition	: PREVPOSITION;
};

struct GSOutput
{
	float4 Position		: SV_POSITION;
	float2 TexCoords	: TEXCOORDS;
};

struct Particle
{
	float4 CurrPosition;
	float4 PrevPosition;
};

StructuredBuffer<Particle> Particles : register(t0);

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<ParticlesTimer> Timer : register(b1);

Texture2D particleTexture : register(t1);
SamplerState particleSampler : register(s0);
