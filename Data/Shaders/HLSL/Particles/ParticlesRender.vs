#include "ParticlesRenderCommon.inl"

void VSMain(in ParticlesVertexIdInput In, out VSOutput Out)
{
	Particle particle	= Particles[In.id];
	Out.CurrPosition	= particle.CurrPosition;
	Out.PrevPosition	= particle.PrevPosition;
}
