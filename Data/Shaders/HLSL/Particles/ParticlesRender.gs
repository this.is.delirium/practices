#include "ParticlesRenderCommon.inl"

[maxvertexcount(6)]
void GSMain(point VSOutput In[1], inout TriangleStream<GSOutput> OutStream)
{
	GSOutput v0, v1, v2, v3;
	float4 oldPos = In[0].CurrPosition;
	float size = 0.5f;

	float4 CamRight = float4(cross(PerFrameCB.CameraDirection.xyz, PerFrameCB.CameraUp.xyz), 1.0f);

	// The first triangle.
	v0.Position		= oldPos + (-PerFrameCB.CameraUp - CamRight) * size;
	v0.Position		= mul(PerFrameCB.ViewProjMatrix, float4(v0.Position.xyz, 1.0));
	v0.TexCoords	= float2(0.0f, 0.0f);
	OutStream.Append(v0);

	v1.Position		= oldPos + (-PerFrameCB.CameraUp + CamRight) * size;
	v1.Position		= mul(PerFrameCB.ViewProjMatrix, float4(v1.Position.xyz, 1.0));
	v1.TexCoords	= float2(0.0f, 1.0f);
	OutStream.Append(v1);

	v2.Position		= oldPos + (PerFrameCB.CameraUp - CamRight) * size;
	v2.Position		= mul(PerFrameCB.ViewProjMatrix, float4(v2.Position.xyz, 1.0));
	v2.TexCoords	= float2(1.0f, 0.0f);
	OutStream.Append(v2);

	OutStream.RestartStrip();

	// The second triangle.
	v3.Position		= oldPos + (PerFrameCB.CameraUp + CamRight) * size;
	v3.Position		= mul(PerFrameCB.ViewProjMatrix, float4(v3.Position.xyz, 1.0));
	v3.TexCoords	= float2(1.0f, 1.0f);

	OutStream.Append(v2);
	OutStream.Append(v1);
	OutStream.Append(v3);

	OutStream.RestartStrip();
}
