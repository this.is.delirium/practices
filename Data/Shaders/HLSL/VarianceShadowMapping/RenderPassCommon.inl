#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4	Position	    : SV_POSITION;
	float4	Normal		    : NORMAL;
	float2	TexCoord	    : TEXCOORD0;
	float4	ViewDir		    : VIEWDIR;
	float4	LightDir	    : LIGHTDIR;
	float4	LightViewPos	: LIGHTVIEWPOS;
};

struct PSOutput
{
	float4 Color : SV_TARGET;
};

struct VarianceParams
{
    int Type() { return Settings.x; }

    int4 Settings;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);
ConstantBuffer<Light> LightCB : register(b2);
ConstantBuffer<VarianceParams> ParamsCB : register(b3);

Texture2D DepthTexture : register(t0);
SamplerState DepthSampler : register(s0);
SamplerComparisonState DepthCmpSampler : register(s1);
