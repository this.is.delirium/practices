#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4 Position				: SV_POSITION;
	float4 Normal				: NORMAL;
	float2 TexCoords			: TEXCOORD0;
	float3 ToLightTangentSpace	: LightTS;
	float3 ToCameraTangentSpace	: CameraTS;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);
ConstantBuffer<ParallaxMappingParams> ParamsCB : register(b2);

SamplerState Sampler : register(s0);

Texture2D<float4> DiffuseTexture : register(t0);
Texture2D<float4> NormalMap : register(t1);
Texture2D<float> HeightMap : register(t2);
