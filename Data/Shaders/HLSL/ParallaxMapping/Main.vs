#include "MainCommon.inl"

void VSMain(in MainInput In, out PSInput Out)
{
	Out.Normal		= mul(PerObjectCB.NormalMatrix, float4(In.Normal, 0.0f));
	Out.Position	= mul(PerObjectCB.WorldMatrix, float4(In.Position, 1.0f));

    float3 gLightPosition       = float3(2.0f, 2.0f, 2.0f);
    float3 toLightWorldSpace    = normalize(gLightPosition - Out.Position.xyz);
	float3 toCameraWorldSpace	= normalize(gLightPosition - Out.Position.xyz);

    // for plane normal is a vector (0, 0, 1).
    // So, we can take tangent as (1, 0, 0) and tangent as (0, 1, 0).
    float3 tangent      = float3(1.0f, 0.0f, 0.0f);
    float3 bitangent    = float3(0.0f, 1.0f, 0.0f);
    float3x3 TBN        = float3x3(tangent, bitangent, Out.Normal.xyz);

    Out.ToLightTangentSpace     = normalize(mul(TBN, toLightWorldSpace));
    Out.ToCameraTangentSpace    = normalize(mul(TBN, toCameraWorldSpace));

	Out.Position	= mul(PerFrameCB.ViewProjMatrix, Out.Position);
	Out.TexCoords	= In.TextureUV;

}
