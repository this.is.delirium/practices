#include "RenderPassCommon.inl"

void VSMain(in MainInput In, out PSInput Out)
{
	float4 worldPosition	= mul(PerObjectCB.WorldMatrix, float4(In.Position, 1.0f));

	Out.Position			= worldPosition;
	Out.Position			= mul(PerFrameCB.ViewProjMatrix, worldPosition);

	Out.ViewDir				= normalize(PerFrameCB.CameraPosition - worldPosition);
	Out.LightDir			= normalize(LightCB.Position - worldPosition);

	Out.LightViewPos		= mul(LightCB.ViewProjectionMatrix, worldPosition);

	Out.Normal				= mul(PerObjectCB.NormalMatrix, float4(In.Normal, 0.0f));

	Out.TexCoord			= In.TextureUV;
};
