#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

struct PSInput
{
	float4	Position	: SV_POSITION;
	float4	Normal		: NORMAL;
	float2	TexCoord	: TEXCOORD0;
	float4	ViewDir		: VIEWDIR;
	float4	LightDir	: LIGHTDIR;
	float4	LightViewPos	: LIGHTVIEWPOS;
};

struct PSOutput
{
	float4 Color : SV_TARGET;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);
ConstantBuffer<Light> LightCB : register(b2);

Texture2D DepthTexture : register(t0);
SamplerComparisonState DepthSampler : register(s0);
