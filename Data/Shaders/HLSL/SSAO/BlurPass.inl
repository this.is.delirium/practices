#include "Common.inl"

struct PSInput
{
	float4 Position	: SV_POSITION;
	float2 TexCoord	: TEXCOORD0;
};

ConstantBuffer<GaussKernel> GaussKernelCB : register(b0);

Texture2D<float4>	ColorTexture	: register(t0);
Texture2D<float4>	SSAOTexture		: register(t1);

SamplerState TexSampler : register(s0);
