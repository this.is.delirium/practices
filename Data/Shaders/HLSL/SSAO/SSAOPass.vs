#include "SSAOPass.inl"

void VSMain(in ScreenQuadNDCInput In, out PSInput Out)
{
	Out.Position	= float4(In.Position, 0.0f, 1.0f);
	Out.NDC			= Out.Position.xy;
}
