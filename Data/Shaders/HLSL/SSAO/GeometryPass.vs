#include "GeometryPass.inl"

void VSMain(in MainInput In, out PSInput Out)
{
	Out.Position	= mul(PerObjectCB.WorldMatrix, float4(In.Position, 1.0f));
	Out.Position	= mul(PerFrameCB.ViewProjMatrix, Out.Position);

	Out.Normal		= mul((float3x3)PerObjectCB.NormalMatrix, In.Normal);
	Out.Color		= float4(Out.Normal.xyz * 0.5f + 0.5f, 1.0f);
	Out.Normal		= mul((float3x3)PerFrameCB.ViewMatrix, Out.Normal);

	Out.Depth		= Out.Position.z / Out.Position.w;
}
