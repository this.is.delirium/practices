#include "BlurPass.inl"

void VSMain(in ScreenQuadNDCInput In, out PSInput Out)
{
	Out.Position	= float4(In.Position, 0.0f, 1.0f);

	Out.TexCoord	= Out.Position.xy;
	Out.TexCoord.y	*= -1.0f;
	Out.TexCoord	= Out.TexCoord * 0.5f + 0.5f;
}
