#include "Common.inl"

struct PSInput
{
	float4	Position	: SV_POSITION;
	float4	Color		: Diffuse;
	float3	Normal		: Normal;
	float	Depth		: Depth;
};

struct PSOutput
{
	float4	Diffuse	: SV_TARGET0;
	float2	Normal	: SV_TARGET1;
	float	Depth	: SV_DEPTH;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<PerObject> PerObjectCB : register(b1);
