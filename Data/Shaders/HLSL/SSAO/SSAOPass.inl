#include "Common.inl"

struct PSInput
{
	float4 Position	: SV_POSITION;
	float2 NDC		: TEXCOORD0;
};

ConstantBuffer<PerFrame> PerFrameCB : register(b0);
ConstantBuffer<SSAOKernel> Kernel : register(b1);

Texture2D<float4>	ColorTexture	: register(t0);
Texture2D<float2>	NormalTexture	: register(t1);
Texture2D<float>	DepthTexture	: register(t2);
Texture2D<float4>	NoiseTexture	: register(t3);

SamplerState		TexSampler		: register(s0);
SamplerState		NoiseSampler	: register(s1);

float4 ViewPosition(float2 ndc, float depth)
{
	float4 ViewPos	= mul(PerFrameCB.InvProjMatrix, float4(ndc, depth, 1.0f));
	ViewPos			/= ViewPos.w;

	return ViewPos;
}

float Depth(float2 uv)
{
	return DepthTexture.Sample(TexSampler, uv);
}

float4 Color(float2 uv)
{
	return ColorTexture.Sample(TexSampler, uv);
}

float4 Normal(float2 uv)
{
	return DecodeNormal(NormalTexture.Sample(TexSampler, uv));
}

float4 Noise(float2 uv)
{
	return normalize(NoiseTexture.Sample(NoiseSampler, WindowSize() * uv / NoiseSize()));
}
