#include "../ConstantBuffers.inl"
#include "../InputLayouts.inl"

float2 packNormal(in float3 normal)
{
	return float2(normal.xy * 0.5f + 0.5f);
}

float4 DecodeNormal(float2 viewSpaceXY)
{
	float4 normal;
	normal.xy	= viewSpaceXY * 2.0f - 1.0f;
	normal.z	= sqrt(1.0f - dot(normal.xy, normal.xy));
	normal.w	= 0.0f;
	return normal;
}

ConstantBuffer<SSAOParams> Params : register(b2);

int KernelSize()
{
	return Params.Sizes.x;
}

int NoiseSize()
{
	return Params.Sizes.y;
}

float2 WindowSize()
{
	return float2(Params.Sizes.zw);
}

float Radius()
{
	return Params.Settings.x;
}

float Power()
{
	return Params.Settings.y;
}

float Scale()
{
	return Params.Settings.z;
}

float Bias()
{
	return Params.Settings.w;
}
