#include "../InputLayouts.inl"

struct VSOutput
{
	float3 Position : POSITION;
};

void VSMain(in TessellationInput Input, out VSOutput Output)
{
	Output.Position = Input.Position;
}
