#include "../ConstantBuffers.inl"

struct VSOutput
{
	float3 Position : POSITION;
};

struct HSOutput
{
	float3 Position : POSITION;
};

ConstantBuffer<TesselationParams> Params : register(b0);

struct TriangleTess
{
	float EdgeTess[3]	: SV_TessFactor;
	float InsideTess	: SV_InsideTessFactor;
};

TriangleTess PatchTriangle(InputPatch<VSOutput, 3> patch, uint ID : SV_PrimitiveID)
{
	TriangleTess tess;

	tess.InsideTess		= Params.innerLevel.x;
	tess.EdgeTess[0]	= Params.outerLevel.x;
	tess.EdgeTess[1]	= Params.outerLevel.y;
	tess.EdgeTess[2]	= Params.outerLevel.z;

	return tess;
}

[domain("tri")]
[outputcontrolpoints(3)]
[outputtopology("triangle_ccw")]
[patchconstantfunc("PatchTriangle")]
[partitioning("integer")]
HSOutput HSMain(InputPatch<VSOutput, 3> patch, uint i : SV_OutputControlPointID, uint patchID : SV_PrimitiveID)
{
	HSOutput Output;

	Output.Position = patch[i].Position;

	return Output;
}
