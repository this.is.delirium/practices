struct DSOutput
{
	float3 tePosition		: TEPOSITION;
	float3 tePatchDistance	: PATCH_DISTANCE;
	float4 Position			: POSITION;
};

struct GSOutput
{
	float3 PatchDistance	: PATCH_DISTANCE;
	float3 TriangleDistance	: TRIANGLE_DISTANCE;
	float4 Position			: SV_Position;
};

[maxvertexcount(3)]
void GSMain(triangle DSOutput In[3], inout TriangleStream<GSOutput> OutStream)
{
	GSOutput Output;

	float3 A = In[2].tePosition - In[0].tePosition;
	float3 B = In[1].tePosition - In[0].tePosition;

	Output.PatchDistance	= In[0].tePatchDistance;
	Output.TriangleDistance	= float3(1.0f, 0.0f, 0.0f);
	Output.Position 		= In[0].Position;
	OutStream.Append(Output);

	Output.PatchDistance	= In[1].tePatchDistance;
	Output.TriangleDistance	= float3(0.0, 1.0, 0.0);
	Output.Position			= In[1].Position;
	OutStream.Append(Output);

	Output.PatchDistance	= In[2].tePatchDistance;
	Output.TriangleDistance	= float3(0.0, 0.0, 1.0);
	Output.Position			= In[2].Position;
	OutStream.Append(Output);

	OutStream.RestartStrip();
}
