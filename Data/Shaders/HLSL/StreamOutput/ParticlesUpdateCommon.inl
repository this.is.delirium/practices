struct VSInput
{
	float3	Position	: POSITION;
	float3	Velocity	: VELOCITY;
	float3	Color		: COLOR;
	float2	Settings	: SETTINGS; // x - life, y - size
	int		Type		: TYPE;
};

struct VSOutput
{
	float3	Position	: POSITION;
	float3	Velocity	: VELOCITY;
	float3	Color		: COLOR;
	float2	Settings	: SETTINGS;
	int		Type		: TYPE;
};

struct GSOutput
{
	float3	Position	: POSITION;
	float3	Velocity	: VELOCITY;
	float3	Color		: COLOR;
	float2	Settings	: SETTINGS;
	int		Type		: TYPE;
	float4	gPosition	: SV_POSITION;
};

cbuffer GeneratorSettings : register(b0)
{
	float4	Position;
	float4	MinVelocity;
	float4	MaxVelocity;
	float4	GravityVector;
	float4	Color;
	float4	Seed;
	float4 	Settings; // x - minLifeTime, y - maxLifeTime, z - size, w - timePassed
	int		NumToGenerate;
};