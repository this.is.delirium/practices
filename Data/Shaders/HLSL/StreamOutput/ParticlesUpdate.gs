#include "ParticlesUpdateCommon.inl"

// =======================================================================
// function : SeedRand
// purpose  : Applies hash function by Thomas Wang to randomize seeds
//            (see http://www.burtleburtle.net/bob/hash/integer.html)
// =======================================================================
void SeedRand (in float4 theSeed, inout uint RandState)
{
	RandState = uint (int (theSeed.x) + int (theSeed.y) + int(theSeed.z));

	RandState = (RandState + 0x479ab41du) + (RandState <<  8);
	RandState = (RandState ^ 0xe4aa10ceu) ^ (RandState >>  5);
	RandState = (RandState + 0x9942f0a6u) - (RandState << 14);
	RandState = (RandState ^ 0x5aedd67du) ^ (RandState >>  3);
	RandState = (RandState + 0x17bea992u) + (RandState <<  7);
}

// =======================================================================
// function : RandInt
// purpose  : Generates integer using Xorshift algorithm by G. Marsaglia
// =======================================================================
uint RandInt(inout uint RandState)
{
	RandState ^= (RandState << 13);
	RandState ^= (RandState >> 17);
	RandState ^= (RandState <<  5);

	return RandState;
}

// =======================================================================
// function : RandFloat
// purpose  : Generates a random float in [0, 1) range
// =======================================================================
float RandFloat(inout uint RandState)
{
	return float (RandInt(RandState)) * (1.f / 4294967296.f);
}

[maxvertexcount(40)]
void GSMain(point VSOutput In[1], inout PointStream<GSOutput> OutStream)
{
	//! 32-bit state of random number generator.
	uint RandState;

	SeedRand(Seed, RandState);

	GSOutput output = (GSOutput)0;
	output.gPosition = float4(In[0].Position, 1.0f);

	output.Position = In[0].Position;
	output.Velocity = In[0].Velocity;
	if (In[0].Type != 0)
	{
		output.Position += output.Velocity * Settings.w;
		output.Velocity += GravityVector.xyz * Settings.w;
	}

	output.Color = In[0].Color;
	output.Settings.x = In[0].Settings.x - Settings.w; // life time
	output.Settings.y = In[0].Settings.y; // size out.
	output.Type = In[0].Type;

	if (output.Type == 0)
	{
		OutStream.Append(output);
		OutStream.RestartStrip();

		float3 velocityRange = (MaxVelocity - MinVelocity).xyz;
		float lifeTimeRange = Settings.y - Settings.x;
		for (int i = 0; i < NumToGenerate; ++i)
		{
			//output.Position = Position.xyz;
			//output.Velocity = MinVelocity.xyz + float3(velocityRange.x * RandFloat(RandState), velocityRange.y * RandFloat(RandState), velocityRange.z * RandFloat(RandState));
			//output.Color = Color.xyz;
			//output.Settings.x = Settings.x + lifeTimeRange * RandFloat(RandState);
			//output.Settings.y = Settings.z;
			//output.Type = 1;
			output.Position = float3(-1.0f, -1.0f, -1.0f);
			output.Velocity = float3(1.0f, 1.0f, 1.0f);
			output.Color = float3(1.0f, 2.0f, 3.0f);
			output.Settings = float2(99.0f, 22.0f);
			output.Type = 10;

			OutStream.Append(output);
			OutStream.RestartStrip();
		}
	}
	else if (output.Settings.x > 0.0)
	{
		OutStream.Append(output);
		OutStream.RestartStrip();
	}
}
