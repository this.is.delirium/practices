#include "ParticlesUpdateCommon.inl"

void VSMain(in VSInput In, out VSOutput Out)
{
	Out.Position	= In.Position;
	Out.Velocity	= In.Velocity;
	Out.Color		= In.Color;
	Out.Settings	= In.Settings;
	Out.Type		= In.Type;
}
