struct VSInput
{
	float3	Position	: POSITION;
	float3	Velocity	: VELOCITY;
	float3	Color		: COLOR;
	float2	Settings	: SETTINGS; // x - life, y - size
	int		Type		: TYPE;
};

struct VSOutput
{
	float3	Position	: POSITION;
	float3	Velocity	: VELOCITY;
	float3	Color		: COLOR;
	float2	Settings	: SETTINGS;
	int		Type		: TYPE;
};

struct GSOutput
{
	float4 Position	: SV_POSITION;
	float4 Color	: COLOR;
};

cbuffer MatrixPerFrame : register(b0)
{
	float4x4	ViewProjectionMatrix;
};

cbuffer CameraVectors : register(b1)
{
	float4 CamUp;
	float4 CamRight;
};
