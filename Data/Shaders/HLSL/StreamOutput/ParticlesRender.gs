#include "ParticlesRenderCommon.inl"

[maxvertexcount(6)]
void GSMain(point VSOutput In[1], inout TriangleStream<GSOutput> OutStream)
{
	GSOutput output;

	if(In[0].Type != 0)
	{
		float4 v0, v1, v2, v3;
		float4 oldPos = float4(In[0].Position, 1.0f);
		float size = In[0].Settings.y;
		//output.Position = mul(ViewProjectionMatrix, oldPos);
		//OutStream.Append(output);
		//OutStream.RestartStrip();

		output.Color = float4(In[0].Color, In[0].Settings.x);

		v0 = oldPos + (-CamUp - CamRight) * size;
		v0 = mul(ViewProjectionMatrix, float4(v0.xyz, 1.0));
		output.Position = v0;
		OutStream.Append(output);

		v1 = oldPos + (-CamUp + CamRight) * size;
		v1 = mul(ViewProjectionMatrix, float4(v1.xyz, 1.0));
		output.Position = v1;
		OutStream.Append(output);

		v2 = oldPos + (CamUp - CamRight) * size;
		v2 = mul(ViewProjectionMatrix, float4(v2.xyz, 1.0));
		output.Position = v2;
		OutStream.Append(output);

		OutStream.RestartStrip();

		v3 = oldPos + (CamUp + CamRight) * size;
		v3 = mul(ViewProjectionMatrix, float4(v3.xyz, 1.0));

		output.Position = v2;
		OutStream.Append(output);

		output.Position = v1;
		OutStream.Append(output);

		output.Position = v3;
		OutStream.Append(output);

		OutStream.RestartStrip();
	}
}
