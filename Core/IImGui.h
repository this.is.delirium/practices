#pragma once
#include "StdafxCore.h"

class IRenderer;
class RHICommandList;
class Window;
struct ImVec2;

class IImGui
{
public:
	IImGui(Window* window, IRenderer* renderer)
		: mWindow(window), mRenderer(renderer) { }

	virtual ~IImGui() { }

	virtual void Release() = 0;
	virtual bool Initialize(const int numFrames) = 0;

	virtual void NewFrame() = 0;
	virtual void Render(RHICommandList* cmdList) = 0;

	bool FindHoveredWindow(ImVec2* pos, bool excludingChilds);
	virtual bool UpdateMouseCursor() = 0;

protected:
	virtual void InvalidateDeviceObjects() = 0;
	virtual void CreateFontsTexture() = 0;
	virtual bool CreateDeviceObjects() = 0;

protected:
	Window*		mWindow;
	IRenderer*	mRenderer;
};
