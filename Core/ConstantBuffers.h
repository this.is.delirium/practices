#pragma once
#include "StdafxCore.h"
#include <RHIConstBuffer.h>

namespace ConstantBuffers
{
	using namespace DirectX;

	__declspec(align(16))
	struct PerFrame
	{
		XMMATRIX ViewProjMatrix;
		XMMATRIX ViewMatrix;
		XMMATRIX ProjMatrix;
		XMMATRIX InvProjMatrix;
		XMMATRIX InvViewMatrix;
		XMFLOAT4 DepthParams;
		XMFLOAT4 CameraPosition;
		XMFLOAT4 CameraDirection;
		XMFLOAT4 CameraUp;
		XMFLOAT4 WindowParams;
	};

	__declspec(align(16))
	struct PerObject
	{
		XMMATRIX WorldMatrix;
		XMMATRIX NormalMatrix;
		XMFLOAT4 Color;
	};

	_declspec(align(16))
	struct Mandelbrot2d
	{
		Mandelbrot2d()
		:	ParamXY(0.7f, 0.5f),
			ViewPos(0.0f, 0.0f),
			ViewZoom(0.5f),
			ViewRatio(1.0f),
			Anim(0.0f),
			Iterations(10),
			Type(0),
			BlinkMode(0) { }

		XMFLOAT2	ParamXY;
		XMFLOAT2	ViewPos;
		float		ViewZoom;
		float		ViewRatio;
		float		Anim;
		int			Iterations;
		int			Type;
		int			BlinkMode;
	};

	struct Tessellation
	{
		XMFLOAT4 InnerLevel;
		XMFLOAT4 OuterLevel;
	};

	_declspec(align(16))
	struct Light
	{
		XMMATRIX Matrix;
		XMFLOAT4 Position;
		XMFLOAT4 Ambient;
		XMFLOAT4 Diffuse;
		XMFLOAT4 Specular;
		XMFLOAT4 ZRange;
		XMFLOAT4 Attenuation;	// w - itensity.
	};

	_declspec(align(16))
	struct SunLight
	{
		XMFLOAT4 LightDir;
	};

	__declspec(align(16))
	struct ScreenQuadTextureType
	{
		int Type;
	};

	namespace Frustum
	{
		__declspec(align(16))
		struct PerFrame
		{
			XMMATRIX	ViewProjMatrix;
			XMFLOAT2	WinSize;
		};
	}

	namespace PSSM
	{
		__declspec(align(16))
		struct Ranges
		{
			XMFLOAT4	ZFars;
			int			NbOfFrustums;
			XMFLOAT3	Padding;
		};

		struct CascadeMatrices
		{
			XMMATRIX LightVP[4];
		};
	}

	namespace Particles
	{
		struct ConstantVariables
		{
			DirectX::XMUINT4 Params;
		};

		struct DynamicVariables
		{
			DirectX::XMFLOAT4 Attractor;
		};

		struct Timer
		{
			DirectX::XMFLOAT4 Time;
		};
	}

	namespace SSAO
	{
		constexpr int kMaxKernelSize	= 128;
		constexpr int kMinKernelSize	= 4;

		constexpr int kMaxNoiseSize		= 8;
		constexpr int kMinNoiseSize		= 4;

		struct Kernel
		{
			XMFLOAT4 Vectors[kMaxKernelSize];
		};

		struct Params
		{
			FORCEINLINE int32_t& KernelSize() { return Sizes.x; }
			FORCEINLINE int32_t& NoiseSize() { return Sizes.y; }
			FORCEINLINE int32_t& ScreenWidth() { return Sizes.z; }
			FORCEINLINE int32_t& ScreenHeight() { return Sizes.w; }

			FORCEINLINE float& Radius() { return Settings.x; }
			FORCEINLINE float& Power() { return Settings.y; }
			FORCEINLINE float& Scale() { return Settings.z; }
			FORCEINLINE float& Bias() { return Settings.w; }

			DirectX::XMFLOAT4	Settings = DirectX::XMFLOAT4(0.68f, 0.7f, 1.0f, 0.15f);
			DirectX::XMINT4		Sizes = DirectX::XMINT4(8, 6, 0, 0);
		};

		struct GaussKernel
		{
			XMFLOAT4 Values[kMaxNoiseSize * kMaxNoiseSize];
		};
	}

	namespace SSLR
	{
		constexpr float kMinStartL	= 0.1f;
		constexpr float kMaxStartL	= 5.0f;

		constexpr float kMinReflectionDistance = 0.1f;
		constexpr float kMaxReflectionDistance = 3.0f;


		struct Params
		{
			FORCEINLINE float& StartL() { return Settings.x; }
			FORCEINLINE float& ReflectionDistance() { return Settings.y; }

			DirectX::XMFLOAT4 Settings = DirectX::XMFLOAT4(0.1f, 0.5f, 0.0f, 0.0f);
		};
	}
}

using ConstBufferPerFrame			= RHIConstBuffer<ConstantBuffers::PerFrame>;

using ConstBufferPerObject			= RHIConstBuffer<ConstantBuffers::PerObject>;

using ConstBufferMandelbrot2d		= RHIConstBuffer<ConstantBuffers::Mandelbrot2d>;

using ConstBufferTessellation		= RHIConstBuffer<ConstantBuffers::Tessellation>;

using ConstBufferForLight			= RHIConstBuffer<ConstantBuffers::Light>;
using ConstBufferSunLight			= RHIConstBuffer<ConstantBuffers::SunLight>;

using ConstBufferTextureType		= RHIConstBuffer<ConstantBuffers::ScreenQuadTextureType>;

using ConstBufferForFrustumPerFrame	= RHIConstBuffer<ConstantBuffers::Frustum::PerFrame>;

using ConstBufferRanges				= RHIConstBuffer<ConstantBuffers::PSSM::Ranges>;
using ConstBufferCascadeMatrices	= RHIConstBuffer<ConstantBuffers::PSSM::CascadeMatrices>;

using ConstBufferParticlesConstVars	= RHIConstBuffer<ConstantBuffers::Particles::ConstantVariables>;
using ConstBufferParticlesDynamVars	= RHIConstBuffer<ConstantBuffers::Particles::DynamicVariables>;
using ConstBufferParticlesTimer		= RHIConstBuffer<ConstantBuffers::Particles::Timer>;

using ConstBufferSSAOKernel			= RHIConstBuffer<ConstantBuffers::SSAO::Kernel>;
using ConstBufferSSAOParams			= RHIConstBuffer<ConstantBuffers::SSAO::Params>;
using ConstBufferSSAOGaussKernel	= RHIConstBuffer<ConstantBuffers::SSAO::GaussKernel>;

using ConstBufferSSLRParams			= RHIConstBuffer<ConstantBuffers::SSLR::Params>;
