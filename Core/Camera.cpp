#include "StdafxCore.h"
#include "Camera.h"

#include <Render/GraphicsCore.h>
#include <Render/IRenderer.h>
#include "StaticGeometryBuilder.h"
#include "ViewFrustum.h"

#include <D3D12/private/DXMathHelper.h>

using namespace DirectX;
using namespace GraphicsCore;

namespace
{
	/**
	 * Angles Phi and Theta in the radians.
	 */
	XMFLOAT4 SphericalDirection(float pPhi, float pTheta)
	{
		float	lCosTheta = cosf(pTheta);

		return XMFLOAT4(
			cosf(pPhi) * lCosTheta,
			sinf(pPhi) * lCosTheta,
			sinf(pTheta),
			0.0f
		);
	}
}

int Camera::kCameraId = 0;

/**
 * Function : Camera
 */
Camera::Camera()
	: mPosition(1.0f, 0.0f, 0.0f, 1.0f), mDirection(-1.0f, 0.0f, 0.0f, 0.0f), mUp(0.0f, 0.0f, 1.0f, 0.0f), mUseAngles(false),
	mAngles(XM_PI, 0.0f), mZRange(0.1f, 300.0f), mIsDirty(false)
{
	++kCameraId;
	SetDefaultName();

	XMStoreFloat4(&mRight, XMVector3Cross(XMLoadFloat4(&mDirection), XMLoadFloat4(&mUp)));
	MarkDirty();
}

/**
 * Function : Camera
 */
Camera::Camera(const CameraDesc& desc, const std::string& name)
{
	++kCameraId;

	SetName(name);
	Initialize(desc);
}

/**
 * Function : Camera
 */
Camera::Camera(const Camera& camera)
{
	++kCameraId;
	SetDefaultName();

	CameraDesc desc;

	desc.Common.Position	= camera.Position();
	desc.Common.ZRange		= camera.ZRange();

	desc.ProjType			= camera.mProjectionType;

	switch (desc.ProjType)
	{
		case ProjectionType::Perspective:
		{
			desc.PerspPart.AspectRatio	= camera.mAspectRatio;
			desc.PerspPart.FoV			= camera.mFoV;
			break;
		}

		case ProjectionType::Orthographic:
		{
			desc.OrthoPart.View = camera.mViewOrtho;
			break;
		}
	}

	desc.Format = (camera.mUseAngles) ? InitializeFormat::FromSphericalAngles : InitializeFormat::FromDirection;

	switch (desc.Format)
	{
		case InitializeFormat::FromDirection:
		{
			desc.DirPart.Direction	= camera.mDirection;
			desc.DirPart.Up			= camera.mUp;
			break;
		}

		case InitializeFormat::FromSphericalAngles:
		{
			desc.AnglesPart.AngleXRange		= camera.mAngleXRange;
			desc.AnglesPart.AngleYRange		= camera.mAngleYRange;
			desc.AnglesPart.SphericalAngles	= camera.mAngles;
			break;
		}
	}

	Initialize(desc);
}

/**
 * Function : ~Camera
 */
Camera::~Camera()
{
	--kCameraId;
}

/**
 * Function : Initialize
 */
void Camera::Initialize(const CameraDesc& desc)
{
	mPosition		= desc.Common.Position;
	mZRange			= desc.Common.ZRange;
	mProjectionType	= desc.ProjType;

	switch (desc.ProjType)
	{
		using namespace std::placeholders;

		case ProjectionType::Perspective:
		{
			mFoV			= desc.PerspPart.FoV;
			mAspectRatio	= desc.PerspPart.AspectRatio;

			BuildMatrices = std::bind(&Camera::BuildPerspectiveMatrices, this);

			break;
		}

		case ProjectionType::Orthographic:
		{
			mViewOrtho = desc.OrthoPart.View;

			BuildMatrices = std::bind(&Camera::BuildOrthographicMatrices, this);

			break;
		}
	}

	switch (desc.Format)
	{
		case InitializeFormat::FromDirection:
		{
			mDirection	= desc.DirPart.Direction;
			mUp			= desc.DirPart.Up;

			XMStoreFloat4(&mRight, XMVector3Cross(XMLoadFloat4(&mDirection), XMLoadFloat4(&mUp)));
			XMStoreFloat4(&mUp, XMVector3Cross(XMLoadFloat4(&mRight), XMLoadFloat4(&mDirection)));

			mUseAngles	= false;

			break;
		}

		case InitializeFormat::FromSphericalAngles:
		{
			mAngles			= desc.AnglesPart.SphericalAngles;
			mAngleXRange	= desc.AnglesPart.AngleXRange;
			mAngleYRange	= desc.AnglesPart.AngleYRange;
			mUseAngles		= true;

			CalculateDirections();

			break;
		}

		default:
			SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Can't create a camera for %d format."), ToInt(desc.Format));
	}

	MarkDirty();
}

/**
 * Function : InitializeViewFrustum
 */
void Camera::InitializeViewFrustum(const ViewFrustumDesc& desc)
{
	mViewFrustumDesc = desc;
	if (desc.NbOfSplit == 0)
		return;

	if (mViewFrustum.get() == nullptr)
		mViewFrustum = std::make_shared<ViewFrustum>();

	mViewFrustum->Initialize(this, desc.ViewCamera);

	MarkDirty();
}

/**
 * Function : UpdateInternalData
 */
void Camera::UpdateInternalData()
{
	mIsDirty = false;

	BuildMatrices();

	UpdateFrustums();
}

/**
 * Function : CalculateDirections
 * Purpose	: Calculate follow directions: mDirection, mRight, mUp 
 * based on horizontal and vertical angles.
 */
void Camera::CalculateDirections()
{
	float	lTmp	= mAngles.x - XM_PIDIV2;
	mDirection		= SphericalDirection(mAngles.x, mAngles.y);
	mRight			= XMFLOAT4(cos(lTmp), sinf(lTmp), 0.0f, 0.0f);
	XMStoreFloat4(&mUp, XMVector3Cross(XMLoadFloat4(&mRight), XMLoadFloat4(&mDirection)));
}

void Camera::Move(const MoveType type, const float value)
{
	DirectX::XMFLOAT4 direction;
	switch (type)
	{
		case MoveType::Forward:
		{
			direction = mDirection;
			break;
		}
		case MoveType::Backward:
		{
			direction = -mDirection;
			break;
		}
		case MoveType::Right:
		{
			direction = mRight;
			break;
		}
		case MoveType::Left:
		{
			direction = -mRight;
			break;
		}

		default:
			assert(false);
	}

	mPosition.x += direction.x * value * mSpeed;
	mPosition.y += direction.y * value * mSpeed;
	mPosition.z += direction.z * value * mSpeed;

	MarkDirty();
}

/**
 * Function : RotateFromMouse
 * Purpose	: Rotate camera using the position of the cursor.
 * @param pDiffX = (WidthScreen / 2 - MousePos.X).
 * @param pDiffY - (HeightScren / 2 - MousePos.Y). 
 */
void Camera::RotateFromMouse(int pDiffX, int pDiffY, float pDeltaTime)
{
	float lTmp	= mSpeedMouse * pDeltaTime;
	mAngles.x	+= lTmp * (float)pDiffX;
	mAngles.y	+= lTmp * (float)pDiffY;

	{
		const float xDiff = mAngleXRange.y - mAngleXRange.x;
		if (mAngles.x > mAngleXRange.y)
			mAngles.x -= xDiff;
		else if (mAngles.x < mAngleXRange.x)
			mAngles.x += xDiff;
	}

	{
		const float yDiff = mAngleYRange.y - mAngleYRange.x;
		if (mAngles.y > mAngleYRange.y)
			mAngles.y -= yDiff;
		else if (mAngles.y < mAngleYRange.x)
			mAngles.y += yDiff;
	}

	CalculateDirections();
	MarkDirty();
}

/**
 * Function : BuildPerspectiveMatrices
 */
void Camera::BuildPerspectiveMatrices()
{
	if (mUseAngles)
		CalculateDirections();

	mLookAt			= XMMatrixLookToRH(XMLoadFloat4(&mPosition), XMLoadFloat4(&mDirection), XMLoadFloat4(&mUp));
	mProjection		= XMMatrixPerspectiveFovRH(XMConvertToRadians(mFoV), mAspectRatio, mZRange.x, mZRange.y);

	mInvLookAt		= XMMatrixInverse(nullptr, mLookAt);
	mInvProjection	= XMMatrixInverse(nullptr, mProjection);
}

/**
 * Function : BuildMatrices
 */
void Camera::BuildOrthographicMatrices()
{
	if (mUseAngles)
		CalculateDirections();

	mLookAt			= XMMatrixLookToRH(XMLoadFloat4(&mPosition), XMLoadFloat4(&mDirection), XMLoadFloat4(&mUp));
	mProjection		= XMMatrixOrthographicOffCenterRH(mViewOrtho.x, mViewOrtho.y, mViewOrtho.z, mViewOrtho.w, mZRange.x, mZRange.y);

	mInvLookAt		= XMMatrixInverse(nullptr, mLookAt);
	mInvProjection	= XMMatrixInverse(nullptr, mProjection);
}

/**
 * Function : UpdateFrustums
 */
void Camera::UpdateFrustums()
{
	if (mViewFrustumDesc.NbOfSplit == 0)
		return;

	mFrustums.clear();
	mFrustums.resize(mViewFrustumDesc.NbOfSplit);
	StaticGeometryBuilder::BuildFrustums(this, mViewFrustumDesc.LightCamera, mViewFrustumDesc.NbOfSplit, mViewFrustumDesc.Colors, StaticGeometryBuilder::TypeSplitScheme::Practical, mViewFrustumDesc.Lambda);

	mViewFrustum->UpdateMesh();
}

/**
 * Function : SetDefaultName
 */
void Camera::SetDefaultName()
{
	mName = "Camera " + std::to_string(kCameraId);
}

/**
 * Function : UpdateBuffer
 */
void Camera::UpdateBuffer(ConstBufferPerFrame& cb)
{
	auto& cbData = cb.Data();
	cbData.ViewMatrix		= LookAt();
	cbData.ProjMatrix		= Projection();
	cbData.ViewProjMatrix	= ViewProjectionMatrix();
	cbData.InvProjMatrix	= InverseProjection();
	cbData.InvViewMatrix	= InverseLookAt();
	//cbData.DepthParams		= DirectX::XMFLOAT4(camera->ZRange().x, camera->ZRange().y, 0.0f, 0.0f);
	cbData.DepthParams.x	= ZRange().x;
	cbData.DepthParams.y	= ZRange().y;
	cbData.CameraPosition	= Position();
	cbData.CameraDirection	= Direction();
	cbData.CameraUp			= UpVector();
	//cbData.WindowParams		= DirectX::XMFLOAT4(mApp->GetWindow()->Width(), mApp->GetWindow()->Height(), 0.0f, 0.0f);
}

/**
 * Function : Serialize
 */
void Camera::Serialize(nlohmann::json& js) const
{
	//-- ToDo: Serialize dx::vectors
	js["name"]	= mName;
	js["pos"]	= { mPosition.x, mPosition.y, mPosition.z };
	js["range"]	= { mZRange.x, mZRange.y };
	js["type"]	= mProjectionType;

	if (mProjectionType == ProjectionType::Perspective)
	{
		js["fov"] = mFoV;
	}

	auto& jsAngles = js["angles"];
	jsAngles = nlohmann::json::object();
	jsAngles["xrange"]	= { mAngleXRange.x, mAngleXRange.y };
	jsAngles["yrange"]	= { mAngleYRange.x, mAngleYRange.y };
	jsAngles["values"]	= { mAngles.x, mAngles.y };
}

/**
 * Function : Deserialize
 */
void Camera::Deserialize(nlohmann::json& js)
{
	auto& jsonPos	= js["pos"];
	auto& jsonRange	= js["range"];

	Camera::CameraDesc desc;
	desc.Common.Position	= DirectX::XMFLOAT4(jsonPos[0], jsonPos[1], jsonPos[2], 1.0f);
	desc.Common.ZRange		= DirectX::XMFLOAT2(jsonRange[0], jsonRange[1]);

	if (js["type"] == ProjectionType::Perspective)
	{
		desc.ProjType				= Camera::ProjectionType::Perspective;
		desc.PerspPart.FoV			= js["fov"];
		//desc.PerspPart.AspectRatio	= app->GetWindow()->AspectRatio();
		desc.PerspPart.AspectRatio	= GraphicsCore::rs().AspectRatio();
	}

	//if (js["format"] == "angles")
	{
		auto& jsonAngles	= js["angles"];
		auto& jsonXRange	= jsonAngles["xrange"];
		auto& jsonYRange	= jsonAngles["yrange"];
		auto& jsonValues	= jsonAngles["values"];

		desc.Format						= Camera::InitializeFormat::FromSphericalAngles;
		desc.AnglesPart.AngleXRange		= DirectX::XMFLOAT2(jsonXRange[0], jsonXRange[1]);
		desc.AnglesPart.AngleYRange		= DirectX::XMFLOAT2(jsonYRange[0], jsonYRange[1]);
		desc.AnglesPart.SphericalAngles	= DirectX::XMFLOAT2(jsonValues[0], jsonValues[1]);
	}

	SetName(js["name"]);
	Initialize(desc);
}
