#include "StdafxCore.h"
#include "Frustum.h"
#include <Camera.h>

#include <D3D12/private/DXMathHelper.h>

/**
 * Function : Build
 */
void Frustum::Build(Camera* camera)
{
	BuildPlanes(camera);
	BuildPositions(camera);
}

/**
 * Functions : BuildPlanes
 */
void Frustum::BuildPlanes(Camera* camera)
{
	DirectX::XMFLOAT4X4 tmp;
	DirectX::XMStoreFloat4x4(&tmp, camera->ViewProjectionMatrix());

	mPlanes[ToInt(PlaneId::Left)] = Plane(
		tmp._14 + tmp._11,
		tmp._24 + tmp._21,
		tmp._34 + tmp._31,
		tmp._44 + tmp._41
	);

	mPlanes[ToInt(PlaneId::Right)] = Plane(
		tmp._14 - tmp._11,
		tmp._24 - tmp._21,
		tmp._34 - tmp._31,
		tmp._44 - tmp._41
	);

	mPlanes[ToInt(PlaneId::Top)] = Plane(
		tmp._14 - tmp._12,
		tmp._24 - tmp._22,
		tmp._34 - tmp._32,
		tmp._44 - tmp._42
	);

	mPlanes[ToInt(PlaneId::Bottom)] = Plane(
		tmp._14 + tmp._12,
		tmp._24 + tmp._22,
		tmp._34 + tmp._32,
		tmp._44 + tmp._42
	);

	mPlanes[ToInt(PlaneId::Near)] = Plane(
		tmp._13,
		tmp._23,
		tmp._33,
		tmp._43
	);

	mPlanes[ToInt(PlaneId::Far)] = Plane(
		tmp._14 - tmp._13,
		tmp._24 - tmp._23,
		tmp._34 - tmp._33,
		tmp._44 - tmp._43
	);

	for (int i = 0; i < _countof(mPlanes); ++i)
		mPlanes[i].Normalize();
}

/**
 * Function : BuildPositions
 */
void Frustum::BuildPositions(Camera* camera)
{
	using namespace DirectX;

	// It is a copy-past from StaticGeometryBuilder.
	// TODO : Rewrite.

	// clip-space cube.
	XMFLOAT4 csCube[8] =
	{
		XMFLOAT4(-1.0f, -1.0f, 0.0f, 1.0f),
		XMFLOAT4(1.0f, -1.0f, 0.0f, 1.0f),
		XMFLOAT4(-1.0f, 1.0f, 0.0f, 1.0f),
		XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f),

		XMFLOAT4(-1.0f, -1.0f, 1.0f, 1.0f),
		XMFLOAT4(1.0f, -1.0f, 1.0f, 1.0f),
		XMFLOAT4(-1.0f, 1.0f, 1.0f, 1.0f),
		XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f),
	};

	const XMMATRIX& invProj = camera->InverseProjection();
	const XMMATRIX& invView = camera->InverseLookAt();
	XMVECTOR vecPos;
	XMFLOAT4 worldPosition;
	for (int i = 0; i < _countof(csCube); ++i)
	{
		worldPosition = csCube[i];

		vecPos = XMLoadFloat4(&worldPosition);
		vecPos = XMVector4Transform(vecPos, invProj);
		vecPos.m128_f32[0] /= vecPos.m128_f32[3];
		vecPos.m128_f32[1] /= vecPos.m128_f32[3];
		vecPos.m128_f32[2] /= vecPos.m128_f32[3];
		vecPos.m128_f32[3] /= vecPos.m128_f32[3];

		vecPos = XMVector4Transform(vecPos, invView);
		//XMStoreFloat4(&worldPosition, vecPos);
		XMStoreFloat3(&mPoints[i], vecPos);
	}
}

/**
 * Function : Check
 */
bool Frustum::Check(const DirectX::XMFLOAT3& point) const
{
	return Check(DirectX::MakeFloat4(point, 1.0f));
}

/**
 * Function : Check
 */
bool Frustum::Check(const DirectX::XMFLOAT4& point) const
{
	for (auto& plane : mPlanes)
		if (!plane.IsFront(point))
			return false;

	return true;
}

/**
 * Function : Check
 */
bool Frustum::Check(const AABB& bbox) const
{
	using namespace DirectX;

	const XMFLOAT3& min = bbox.Min();
	const XMFLOAT3& max = bbox.Max();

	XMFLOAT4 corners[] =
	{
		XMFLOAT4(min.x, min.y, min.z, 1.0f),
		XMFLOAT4(max.x, min.y, min.z, 1.0f),
		XMFLOAT4(min.x, max.y, min.z, 1.0f),
		XMFLOAT4(max.x, max.y, min.z, 1.0f),
		XMFLOAT4(min.x, min.y, max.z, 1.0f),
		XMFLOAT4(max.x, min.y, max.z, 1.0f),
		XMFLOAT4(min.x, max.y, max.z, 1.0f),
		XMFLOAT4(max.x, max.y, max.z, 1.0f)
	};

	// Check box outside/inside of frustum.
	for (auto& plane : mPlanes)
	{
		int out = 0;
		out += !plane.IsFront(corners[0]);
		out += !plane.IsFront(corners[1]);
		out += !plane.IsFront(corners[2]);
		out += !plane.IsFront(corners[3]);
		out += !plane.IsFront(corners[4]);
		out += !plane.IsFront(corners[5]);
		out += !plane.IsFront(corners[6]);
		out += !plane.IsFront(corners[7]);

		if (out == 8)
			return false;
	}

	// Check frustum outside/inside box.
	int out[6] = { 0, 0, 0, 0, 0, 0 };
	for (int i = 0; i < _countof(mPoints); ++i)
	{
		out[0] += (mPoints[i].x > max.x) ? 1 : 0;
		out[1] += (mPoints[i].x < min.x) ? 1 : 0;
		out[2] += (mPoints[i].y > max.y) ? 1 : 0;
		out[3] += (mPoints[i].y < min.y) ? 1 : 0;
		out[4] += (mPoints[i].z > max.z) ? 1 : 0;
		out[5] += (mPoints[i].z < min.z) ? 1 : 0;
	}

	for (int i = 0; i < _countof(out); ++i)
		if (out[i] == 8)
			return false;

	return true;
}
