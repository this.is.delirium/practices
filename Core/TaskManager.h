#pragma once
#include "StdafxCore.h"
#include "Patterns/Singleton.h"

class TaskManager
{
public:
	class DeviceTask
	{
	public:
		virtual ~DeviceTask() { }

		virtual void Execute() = 0;
	};

	class Task
	{
	public:
		virtual ~Task() { }

		virtual void Execute() = 0;

		virtual DeviceTask* SpawnDeviceStuffTask() { return nullptr; }
	};

public:
	~TaskManager();

	void Add(Task* task);

	template<class T, class... ArgTypes>
	void Add(ArgTypes&&... args) { Add(new T(args)); }

	bool IsEmpty() const { return mTasks.empty(); }
	void Run();

	void LockDeviceTasks() { mDeviceMutex.lock(); }
	std::queue<DeviceTask*>& DeviceTasks() { return mDeviceTasks; }
	void UnlockDeviceTasks() { mDeviceMutex.unlock(); }
	std::mutex& DeviceMutex() { return mDeviceMutex; }


protected:
	TaskManager();
	TaskManager(const TaskManager&) = delete;
	TaskManager& operator=(const TaskManager&) = delete;

	friend class Singleton<TaskManager>;

protected:
	std::queue<Task*>		mTasks;
	mutable std::mutex		mMutex;
	std::condition_variable	mConditionVar;

	//! These tasks must executes from main thread.
	std::queue<DeviceTask*>	mDeviceTasks;
	mutable std::mutex		mDeviceMutex;
};

typedef Singleton<TaskManager> STaskManager;
