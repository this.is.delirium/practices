#pragma once
#include "StdafxCore.h"

class ISerializable
{
public:
	virtual void Serialize(nlohmann::json& js) const = 0;
	virtual void Deserialize(nlohmann::json& js) = 0;
};
