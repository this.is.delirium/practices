#pragma once
#include "StdafxCore.h"

class Camera;
class MeshComponent;
class SceneComponent;
class RHIVertexBuffer;
class SceneObject;

class StaticGeometryBuilder
{
public:
	enum class TypeSplitScheme
	{
		Uniform	= 0,
		Log,
		Practical
	};

public:
	static void LoadPlane(std::shared_ptr<MeshComponent>& mesh, std::shared_ptr<SceneComponent>& sceneComponent);
	static void LoadPlane(SceneObject& obj);

	static void LoadCube(std::shared_ptr<MeshComponent>& mesh, std::shared_ptr<SceneComponent>& sceneComponent);

	/**
	 * Builds sphere at the point (0, 0, 0).
	 * @param pNbOfThetaSplit is number of splittings angle theta. Note that this value must be >= 2.
	 * @param pNbOfPhiSplit is number of splittings angle phi. Note that this value must be >= 2.
	 * It is broken. Don't use please.
	 */
	static void LoadSphere(std::shared_ptr<MeshComponent>& mesh, std::shared_ptr<SceneComponent>& sceneComponent, const uint32_t nbOfThetaSplit, const uint32_t nbOfPhiSplit);

	static void BuildFrustums(Camera* camera, const Camera* lightCamera, const int nbOfSplit, const DirectX::XMFLOAT4* colors, const TypeSplitScheme scheme, const float lambda = 0.0f);

	static void BuildFullScreenQuad(RHIVertexBuffer* buffer, const String& name);
};
