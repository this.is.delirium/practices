#pragma once

#include <algorithm>
#include <atomic>
#include <assert.h>
#include <codecvt>
#include <condition_variable>
#include <filesystem>
#include <fstream>
#include <functional>
#include <future>
#include <map>
#include <memory>
#include <numeric>
#include <queue>
#include <random>
#include <sstream>
#include <stack>
#include <string>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "TypeDefinitions.h"

#ifdef _WIN32
#include <direct.h>
#include <DirectXMath.h>
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

// GLM
#ifndef NoOpenGL
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#endif

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>

// JSON
#include <nlohmann/json.hpp>

// FBX
#define FBXSDK_NEW_API
#define FBXSDK_SHARED
#define isnan
#include <fbxsdk.h>
#undef isnan

// RTTR
#include <rttr/registration.h>
#include <rttr/registration_friend.h>
#include <rttr/rttr_enable.h>
#include <rttr/type.h>
