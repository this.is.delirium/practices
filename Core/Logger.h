#pragma once
#include "StdafxCore.h"
#include "Patterns/Singleton.h"

enum class LogMessageType
{
	Verbose = 0,
	Debug,
	Information,
	Warning,
	Error,
	Count
};

struct LogMessage
{
	LogMessageType	Type;
	time_t			Time;
	String			Report;
};

class Logger
{
public:
	virtual ~Logger();

	void AddMessage(const LogMessageType type, const String& message);
	void AddMessage(const LogMessageType type, CConstString format, ...);

	void SetOutputFile(const String& file) { mFileName = file; }
	void FlushToDisk();

protected:
	Logger();
	Logger(const Logger&) = delete;
	Logger& operator= (const Logger&) = delete;

protected:
	std::vector<LogMessage>	mMessages;
	String					mFileName;
	Ofstream				mOutput;

	using MutexGuard = std::lock_guard<std::mutex>;
	MutexGuard::mutex_type	mMutex;

	friend class Singleton<Logger>;
};

typedef Singleton<Logger> SLogger;
