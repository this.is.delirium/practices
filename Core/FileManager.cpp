#include "StdafxCore.h"
#include "FileManager.h"

/**
 * Function : FileName
 */
CConstString FindData::FileName()
{
#ifdef _MSC_VER
	return Data.cFileName;
#else
	assert(false);
#endif;
}

/**
 * Function : IsDirectory
 */
bool FindData::IsDirectory() const
{
#ifdef _MSC_VER
	return (Data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY);
#else
	assert(false);
#endif
}

/**
 * Function : FindFile
 */
Handle FileManager::Find(const String& fileName, FindData& outData)
{
#ifdef _MSC_VER
	Handle handle = ::FindFirstFile(fileName.c_str(), &outData.Data);
#else
	assert(false);
#endif

	return handle;
}

/**
 * Function : NextFile
 */
bool FileManager::NextFile(Handle handle, FindData& outData)
{
#ifdef _MSC_VER
	return ::FindNextFile(handle, &outData.Data);
#else
	assert(false);
#endif;
}

/**
 * Function : CloseFind
 */
void FileManager::Close(Handle handle)
{
#ifdef _MSC_VER
	::FindClose(handle);
#else
	assert(false);
#endif
}

/**
 * Function : CheckHandle
 */
bool FileManager::CheckHandle(Handle handle)
{
#ifdef _MSC_VER
	return handle != INVALID_HANDLE_VALUE;
#else
	assert(false);
#endif
}

/**
 * Function : FindFilesInFolder
 */
void FileManager::FindFilesInFolder(const String& startFolderRegExp, std::vector<FindData>& result)
{
	FindData findData;
	Handle find = Find(startFolderRegExp, findData);

	std::vector<String> folders;
	if (CheckHandle(find))
	{
		do
		{
			result.push_back(findData);
		} while (NextFile(find, findData));
		Close(find);
	}
}

/**
 * Function : IsExist
 */
bool FileManager::IsExist(const String& path)
{
	return std::experimental::filesystem::exists(path);
}

/**
 * Function : CreateFolder
 */
bool FileManager::CreateFolder(const String& folder)
{
	return std::experimental::filesystem::create_directory(folder);
}

/**
 * Function : DeleteFolder
 */
bool FileManager::DeleteFolder(const String& folder)
{
#ifdef _MSC_VER
	return RemoveDirectory(folder.c_str());
#else
	assert(false);
#endif
}

/**
 * Function : ClearFolder
 */
size_t FileManager::ClearFolder(const String& folder, const bool isRecursive)
{
	size_t numDelObjects = 0;
	FindData findData;
	Handle handle = Find(folder + __TEXT("/*.*"), findData);
	if (CheckHandle(handle))
	{
		do
		{
			if ((StrCmp(findData.FileName(), __TEXT(".")) == 0 || StrCmp(findData.FileName(), __TEXT("..")) == 0))
				continue;

			const String fileName = folder + __TEXT("/") + findData.FileName();
			if (findData.IsDirectory() && isRecursive)
			{
				numDelObjects += ClearFolder(fileName, true);
				DeleteFolder(fileName);
			}
			else
				DeleteFile(fileName.c_str());
		} while (NextFile(handle, findData));
		Close(handle);
	}

	return numDelObjects;
}

/**
 * Function : GetFileName
 */
std::string FileManager::GetFileName(const std::string& path) const
{
	return std::experimental::filesystem::path(path).filename().string();
}

/**
 * Function : RemoveExtension
 */
std::string FileManager::RemoveExtension(const std::string& path) const
{
	return std::experimental::filesystem::path(path).replace_extension("").string();
}

/**
 * Function : ParentPath
 */
std::string FileManager::ParentPath(const std::string& path) const
{
	return std::experimental::filesystem::path(path).parent_path().string();
}
