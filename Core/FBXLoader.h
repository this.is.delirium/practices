#pragma once
#include "StdafxCore.h"
#include "InputLayouts.h"

class MeshComponent;
class SceneComponent;

class FbxLoader
{
public:
	~FbxLoader()
	{
		Release();
	}

	void Release();

	//! Returns the number of loaded objects.
	size_t LoadFile(const String& path, const bool useBatch = true);

	void FillMeshComponent(const size_t loadedObjId, std::shared_ptr<MeshComponent>& mesh);
protected:
	void Clear()
	{
		for (auto& object : mObjects)
			for (auto& meshPart : object.Parts)
				meshPart.Release();

		mObjects.clear();
	}

protected:
	using Vertex = InputLayouts::Main::Vertex;

	struct MeshBatch
	{
		UINT StartIndex;
		UINT NumberElements;
	};

	struct MeshBuffer
	{
		void Release()
		{
			Vertices.clear();
			Indices.clear();
		}

		std::vector<Vertex>		Vertices;
		std::vector<uint32_t>	Indices;

		std::vector<MeshBatch>	VertexBatches;
		std::vector<MeshBatch>	IndexBatches;
	};

	struct Node
	{
		std::string				NamePart;
		std::vector<MeshBuffer>	Parts;
	};

protected:
	Node& AddNewNode(FbxNode* node);
	void AddNewMesh(Node& node, FbxNode* fbxNode, FbxManager* sdkManager, const bool useBatch);

	// TODO: use a tree.
	std::vector<Node> mObjects;
};
