#pragma once
#include <TypeDefinitions.h>

enum class ImageFormat
{
	WicBmp = 1,
	WicJpeg,
	WicPng,
	WicTiff,
	WicGif,
	WicWmp,
	WicIco,
	DDS,
	Unknown
};

String GetExtension(const ImageFormat format);
ImageFormat GetImageFormat(const String& extension);
