#include "StdafxCore.h"
#include "ViewFrustum.h"

#include "InputLayouts.h"
#include "Render/GraphicsCore.h"
#include "StaticGeometryBuilder.h"

#include <RHIUtils.h>

using namespace GraphicsCore;

namespace
{
	using FrustumVertex = InputLayouts::Frustum::Vertex;

	uint32_t cubeIndices[] =
	{
		// near plane
		2, 0, 1,
		2, 1, 3,

		// far plane
		6, 5, 4,
		6, 7, 5,

		// top plane
		6, 2, 3,
		6, 3, 7,

		// bottom plane
		0, 4, 5,
		0, 5, 1,

		// right plane
		3, 1, 5,
		3, 5, 7,

		// left plane
		2, 4, 0,
		2, 6, 4
	};
}

/**
 * Function : Release
 */
void ViewFrustum::Release()
{
	mConstBufferPerFrame.Release();
}

/**
 * Function : Initialize
 */
void ViewFrustum::Initialize(Camera* cameraForFrustum, Camera* cameraForView)
{
	mCameraForFrustum	= cameraForFrustum;
	mCameraForView		= cameraForView;

	// Create a mesh.
	{
		//mMeshComponent.reset(BaseComponent::CreateSubobject<MeshComponent>());
		//mMeshComponent->Initialize(8 * 8, sizeof(FrustumVertex), true, _countof(cubeIndices) * 8);
		const UINT maxVertices	= 8 * 8;
		const UINT vertexStride	= sizeof(FrustumVertex);
		mVertexBuffer.Initialize(__TEXT("MeshComponent:VertexBuffer"), maxVertices * vertexStride, maxVertices, vertexStride);

		const UINT maxIndices		= _countof(cubeIndices) * 8;
		const RHIFormat indexFormat	= RHIFormat::R32Uint;
		const UINT strideIndex		= RHIUtils::GetSize(indexFormat);
		mIndexBuffer.Initialize(__TEXT("MeshComponent:IndexBuffer"), maxIndices * strideIndex, maxIndices, strideIndex);
	}

	// Create a constant buffer for frustum rendering.
	{
		RHISwapChainDesc swapChainDesc;
		gRenderer->SwapChain()->GetDesc(&swapChainDesc);

		mConstBufferPerFrame;

		mConstBufferPerFrame.Data().ViewProjMatrix	= mCameraForView->ViewProjectionMatrix();
		mConstBufferPerFrame.Data().WinSize			= DirectX::XMFLOAT2(swapChainDesc.BufferDesc.Width, swapChainDesc.BufferDesc.Height);

		mConstBufferPerFrame.Initialize(__TEXT("ViewFrustum:CBPerFrame"), RHIUtils::GetConstBufferSize(), 1, sizeof(mConstBufferPerFrame.Data()), &mConstBufferPerFrame.Data());
	}
}

/**
 * Function : UpdateMesh
 */
void ViewFrustum::UpdateMesh()
{
	std::vector<FrustumVertex> vertices;
	std::vector<uint32_t> indices;
	FrustumVertex vertex;
	for (size_t split = 0; split < mCameraForFrustum->NbOfFrustums(); ++split)
	{
		Camera::Frustum& frustum = mCameraForFrustum->GetFrustum(split);
		for (int i = 0; i < 8; ++i)
		{
			vertex.Position = frustum.WorldPosition(i);
			vertex.Color = frustum.Color;

			vertices.push_back(vertex);
		}

		for (int index = 0; index < _countof(cubeIndices); ++index)
		{
			indices.push_back(split * 8 + cubeIndices[index]);
		}
	}

	// Update gpu data.
	{
		RHISubresourceData vertexSubres;
		vertexSubres.Data		= vertices.data();
		vertexSubres.RowPitch	= vertices.size() * sizeof(FrustumVertex);
		vertexSubres.SlicePitch	= vertexSubres.RowPitch;

		RHISubresourceData indexSubres;
		indexSubres.Data		= indices.data();
		indexSubres.RowPitch	= indices.size() * sizeof(uint32_t);
		indexSubres.SlicePitch	= indexSubres.RowPitch;

		gRenderer->UpdateBuffer(&mVertexBuffer, &vertexSubres);
		gRenderer->UpdateBuffer(&mIndexBuffer, &indexSubres);
		//mMeshComponent->Update(&vertexSubres, &indexSubres);
	}
}

/**
 * Function : Update
 */
void ViewFrustum::Update(const float deltaTime)
{
	mConstBufferPerFrame.Data().ViewProjMatrix = mCameraForView->ViewProjectionMatrix();
	mConstBufferPerFrame.UpdateDataOnGPU();
}

/**
 * Function : Draw
 */
void ViewFrustum::Draw(RHICommandList* commandList, const int split)
{
	commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
	commandList->IASetVertexBuffers(0, 1, mVertexBuffer.View());
	commandList->IASetIndexBuffer(mIndexBuffer.View());
	commandList->DrawIndexedInstanced(36, 1, 36 * split, 0, 0);
}

/**
 * Function : DrawAll
 */
void ViewFrustum::DrawAll(RHICommandList* commandList)
{
	commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
	commandList->IASetVertexBuffers(0, 1, mVertexBuffer.View());
	commandList->IASetIndexBuffer(mIndexBuffer.View());
	commandList->DrawIndexedInstanced(36 * mCameraForFrustum->NbOfFrustums(), 1, 0, 0, 0);
}
