#pragma once
#include "StdafxCore.h"

struct WindowDesc
{
	HINSTANCE	hInstance;
	String		className;
	String		title;
	int			nCmdShow;
	uint32_t	width;
	uint32_t	height;
	bool		isFullscreen;
};

struct MouseEventParams
{
	POINT CurrentPos;
	POINT Diff;
};

enum class WindowFlashFlags
{
	FlashStop		= 0,							// Stop flashing. The system restores the window to its original state.
	FlashCaption	= 1,							// Flash the window caption.
	FlashTray		= 2,							// Flash the taskbar button.
	FlashAll		= (FlashCaption | FlashTray),	// Flash both the window caption and taskbar button.
	FlashTimer		= 4,							// Flash continuously, until the FLASHW_STOP flag is set.
	FlashTimerNoFg	= 12							// Flash continuously until the window comes to the foreground.
};

class Window
{
protected:
	using OnCloseHandler		= std::function<void(void)>;
	using OnGuiHandler			= std::function<LRESULT(HWND, UINT, WPARAM, LPARAM)>;
	using OnMouseMoveHandler	= std::function<void(const MouseEventParams&)>;
	using OnMouseWheel			= std::function<void(float)>;

public:
	Window()
		: mHinst(nullptr), mHwnd(nullptr) { }

	~Window();

	bool Initialize(const WindowDesc* desc);
	void Release();

	FORCEINLINE HWND GetHwnd() const { return mHwnd; }
	FORCEINLINE uint32_t Width() const { return mWidth; }
	FORCEINLINE uint32_t Height() const { return mHeight; }
	FORCEINLINE DirectX::XMFLOAT2 Size() const { return DirectX::XMFLOAT2(static_cast<float>(mWidth), static_cast<float>(mHeight)); }
	FORCEINLINE float AspectRatio() const { return static_cast<float>(mWidth) / static_cast<float>(mHeight); }
	FORCEINLINE bool IsWindowed() const { return !mIsFullScreen; }
	FORCEINLINE bool IsFullScreen() const { return mIsFullScreen; }

	void PeekMessages();

	/**
	 * @param timeout - in milliseconds.
	 */
	void Flash(const WindowFlashFlags, const UINT count, const DWORD timeout) const;

	//! Sets gui handler.
	template <class T, class Handler>
	void SetOnGuiHandler(T* pCallee, Handler pHandler)
	{
		using namespace std::placeholders;
		mOnGuiHandler = std::bind(pHandler, pCallee, _1, _2, _3, _4);
	}

	//! Sets mouse-move handler.
	template<class T, class Handler>
	void SetOnMouseMoveHandler(T* pCallee, Handler pHandler)
	{
		using namespace std::placeholders;
		mOnMouseMoveHandler = std::bind(pHandler, pCallee, _1);
	}

	//! Sets mouse-wheel handler.
	template<class T, class Handler>
	void SetOnMouseWheel(T* pCallee, Handler pHandler)
	{
		using namespace std::placeholders;
		mOnMouseWheelHandler = std::bind(pHandler, pCallee, _1);
	}

protected:
	ATOM MyRegisterClass(HINSTANCE hInstance, const String& className);

	static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

public:
	OnCloseHandler		mOnClose;
	OnGuiHandler		mOnGuiHandler;
	OnMouseMoveHandler	mOnMouseMoveHandler;
	OnMouseWheel		mOnMouseWheelHandler;

protected:
	HINSTANCE	mHinst;
	HWND		mHwnd;
	uint32_t	mWidth;
	uint32_t	mHeight;
	String		mClassName;
	bool		mIsFullScreen;

protected: //! static fields
	static std::map<HWND, Window*> mWindows;
};
