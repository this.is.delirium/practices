#pragma once
#include "StdafxCore.h"
#include "HeightMapGenerator.h"
#include "ProgressIndicator.h"

template<class Vertex>
class TerrainBuilder
{
public:
	~TerrainBuilder() { Release(); }
	void Release();

	void Build(const HeightMap& map, const DirectX::XMFLOAT2& size, DirectX::XMFLOAT2& heightRange, ProgressIndicator* progress);

	FORCEINLINE const Vertex* Vertices() const { return mVertices.data(); }
	FORCEINLINE const uint32_t* Indices() const { return mIndices.data(); }

	FORCEINLINE size_t NbOfVertices() const { return mVertices.size(); }
	FORCEINLINE size_t NbOfIndices() const { return mIndices.size(); }

	FORCEINLINE size_t VerticesSizeInBytes() const { return NbOfVertices() * sizeof(Vertex); }
	FORCEINLINE size_t IndicesSizeInBytes() const { return NbOfIndices() * sizeof(uint32_t); }

protected:
	void BuildQuad(const DirectX::XMFLOAT2& lbCorner, const float* heights, const uint32_t vertexId);
	FORCEINLINE float BuildHeight(const float height, const DirectX::XMFLOAT2& range) { return range.x + (range.y - range.x) * height; }

	void BuildNormals(const unsigned int index0, const unsigned int index1, const unsigned int index2);

protected:
	std::vector<Vertex>		mVertices;
	std::vector<uint32_t>	mIndices;

	DirectX::XMFLOAT2		mTriangleSize;
	DirectX::XMFLOAT2		mRange;
};

/**
 * Function : Release
 */
template<class T>
void TerrainBuilder<T>::Release()
{
	mVertices.clear();
	mIndices.clear();
}

/**
 * Function : Build
 */
template<class Vertex>
void TerrainBuilder<Vertex>::Build(const HeightMap& map, const DirectX::XMFLOAT2& size, DirectX::XMFLOAT2& heightRange, ProgressIndicator* progress)
{
	Release();

	mTriangleSize = DirectX::XMFLOAT2(size.x / (float)map.Size().x, size.y / (float)map.Size().y);
	mRange = heightRange;

	const DirectX::XMFLOAT2 startPos(-mTriangleSize.x * map.Size().x * 0.5f, -mTriangleSize.y * map.Size().y * 0.5f);
	DirectX::XMFLOAT2 curPos(startPos);

	uint32_t vertexId = 0;

	if (progress)
	{
		progress->SetMinValue(0);
		progress->SetMaxValue(map.Size().y * 2 - 1);
		progress->SetCurrentValue(0);
		progress->SetStep(1);
	}

	/*for (unsigned int y = 0; y < map.Size().y - 1; ++y)
	{
		for (unsigned int x = 0; x < map.Size().x - 1; ++x)
		{
			const float heights[4] = { map.Value(x, y), map.Value(x + 1, y), map.Value(x, y + 1), map.Value(x + 1, y + 1) };
			BuildQuad(curPos, heights, vertexId);

			curPos.x += mTriangleSize.x;
			vertexId += 4;
		}

		curPos.x = startPos.x;
		curPos.y += mTriangleSize.y;

		if (progress)
		{
			progress->Next();
		}
	}*/

	DirectX::XMFLOAT2 stepUV = DirectX::XMFLOAT2(1.0f / map.Size().x, 1.0f / map.Size().y);
	Vertex vertex;
	for (unsigned int y = 0; y < map.Size().y; ++y)
	{
		for (unsigned int x = 0; x < map.Size().x; ++x)
		{
			vertex.SetVertex(DirectX::XMFLOAT3(curPos.x, curPos.y, BuildHeight(map.Value(x, y), mRange)));
			vertex.SetTexCoords(DirectX::XMFLOAT2(x * stepUV.x, y * stepUV.y));
			mVertices.push_back(vertex);

			curPos.x += mTriangleSize.x;
		}

		curPos.x = startPos.x;
		curPos.y += mTriangleSize.y;

		if (progress)
			progress->Next();
	}

	unsigned int currentRow	= 0;
	unsigned int nextRow	= 0;
	unsigned int index0, index1, index2;
	for (unsigned int y = 0; y < map.Size().y - 1; ++y)
	{
		currentRow	= y;
		nextRow		= y + 1;
		for (unsigned int x = 0; x < map.Size().x - 1; ++x)
		{
			// the first triangle.
			{
				index0 = currentRow * map.Size().x + (x + 0);
				index1 = nextRow * map.Size().x + (x + 0);
				index2 = nextRow * map.Size().x + (x + 1);

				mIndices.push_back(index0);
				mIndices.push_back(index1);
				mIndices.push_back(index2);

				BuildNormals(index0, index1, index2);
			}

			// the second triangle.
			{
				index0 = currentRow * map.Size().x + (x + 0);
				index1 = nextRow * map.Size().x + (x + 1);
				index2 = currentRow * map.Size().x + (x + 1);

				mIndices.push_back(index0);
				mIndices.push_back(index1);
				mIndices.push_back(index2);

				BuildNormals(index0, index1, index2);
			}
		}

		if (progress)
			progress->Next();
	}
}

/**
 * Function : BuildQuad
 */
template<class Vertex>
void TerrainBuilder<Vertex>::BuildQuad(const DirectX::XMFLOAT2& lbCorner, const float* heights, const uint32_t vertexId)
{
	glm::vec3 leftBottom(lbCorner.x, lbCorner.y, BuildHeight(heights[0], mRange));
	glm::vec3 rightBottom(lbCorner.x + mTriangleSize.x, lbCorner.y, BuildHeight(heights[1], mRange));
	glm::vec3 leftTop(lbCorner.x, lbCorner.y + mTriangleSize.y, BuildHeight(heights[2], mRange));
	glm::vec3 rightTop(lbCorner.x + mTriangleSize.x, lbCorner.y + mTriangleSize.y, BuildHeight(heights[3], mRange));
	glm::vec3 firstNormal = glm::cross(glm::normalize(rightBottom - leftBottom), glm::normalize(leftTop - leftBottom));
	glm::vec3 secondNormal = glm::cross(glm::normalize(leftTop - rightTop), glm::normalize(rightBottom - rightTop));
	glm::vec3 internalNormal = (firstNormal + secondNormal) * 0.5f;

	Vertex vertex;
	// the left bottom corner.
	vertex.SetVertex(leftBottom);
	vertex.SetNormal(firstNormal);
	mVertices.push_back(vertex);

	// the right bottom corner.
	vertex.SetVertex(rightBottom);
	vertex.SetNormal(internalNormal);
	mVertices.push_back(vertex);

	// the left top corner.
	vertex.SetVertex(leftTop);
	vertex.SetNormal(internalNormal);
	mVertices.push_back(vertex);

	// the right top corner.
	vertex.SetVertex(rightTop);
	vertex.SetNormal(secondNormal);
	mVertices.push_back(vertex);

	// the first triangle.
	mIndices.push_back(vertexId + 0);
	mIndices.push_back(vertexId + 1);
	mIndices.push_back(vertexId + 2);

	// the second triangle.
	mIndices.push_back(vertexId + 2);
	mIndices.push_back(vertexId + 1);
	mIndices.push_back(vertexId + 3);
}

#include <D3D12/private/DXMathHelper.h>
/**
 * Function : BuildNormals
 */
template<class Vertex>
void TerrainBuilder<Vertex>::BuildNormals(const unsigned int index0, const unsigned int index1, const unsigned int index2)
{
	using namespace DirectX;

	XMFLOAT3& vertex0 = mVertices[index0].Position;
	XMFLOAT3& vertex1 = mVertices[index1].Position;
	XMFLOAT3& vertex2 = mVertices[index2].Position;

	XMFLOAT3 edge0	= XMFloat3Normalize(vertex2 - vertex0);
	XMFLOAT3 edge1	= XMFloat3Normalize(vertex1 - vertex0);
	XMFLOAT3 normal	= XMFloat3Cross(edge0, edge1);

	mVertices[index0].SetNormal(normal);
	mVertices[index1].SetNormal(normal);
	mVertices[index2].SetNormal(normal);
}
