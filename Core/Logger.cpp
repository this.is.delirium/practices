#include "StdafxCore.h"
#include "Logger.h"

namespace
{
	String LogTypeAsString[static_cast<unsigned long long> (LogMessageType::Count)] =
	{
		__TEXT("Verbose"),
		__TEXT("Debug"),
		__TEXT("Information"),
		__TEXT("Warning"),
		__TEXT("Error")
	};
}

/**
 * Function : Logger
 */
Logger::Logger()
{

}

/**
 * Function : ~Logger
 */
Logger::~Logger()
{
	FlushToDisk();
}

/**
 * Function : AddMessage
 */
void Logger::AddMessage(const LogMessageType type, const String& message)
{
	LogMessage log;
	log.Report = message;
	log.Time = time(NULL);
	log.Type = type;

	// Safe-thread add to log.
	{
		MutexGuard guard(mMutex);
		mMessages.push_back(log);
	}

	if (type == LogMessageType::Error)
	{
		FlushToDisk();
		throw;
	}
}

/**
 * Function : AddMessage
 */
void Logger::AddMessage(const LogMessageType type, CConstString format, ...)
{
	//MutexGuard guard(mMutex);

	va_list vlist;
	va_start(vlist, format);

	Char message[2048];
	vsprintfs(message, format, vlist);

	AddMessage(type, String(message));

	va_end(vlist);
}

/**
 * Function : FlushToDisk
 */
void Logger::FlushToDisk()
{
	MutexGuard guard(mMutex);
	if (mMessages.empty())
		return;

	// Get the current directory.
	Char lBuffer[MAX_PATH];
	GetModuleFileName(NULL, lBuffer, MAX_PATH);

	String aPath(lBuffer);
	String::size_type lPos = aPath.find_last_of(__TEXT("\\/"));
	aPath = aPath.substr(0, lPos) + __TEXT("\\") + mFileName;

	// Open the file.
	mOutput.open(aPath, std::ios::out);

	//std::ofstream

	if (!mOutput.is_open())
	{
		return;
	}

	std::sort(mMessages.begin(), mMessages.end(), [](const LogMessage& a, const LogMessage& b)
	{
		return a.Time < b.Time;
	});

	// Save internal data.
	for (const LogMessage& msg : mMessages)
	{
		char formattedLine[32];
		tm timeInfo;
		localtime_s(&timeInfo, &msg.Time);
		sprintf_s(formattedLine, "[%02i.%02i.%04i %02i:%02i:%02i]: ", timeInfo.tm_mday, timeInfo.tm_mon + 1, 1900 + timeInfo.tm_year, timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
		mOutput << formattedLine << LogTypeAsString[ToInt(msg.Type)] << ": " << msg.Report << std::endl;
	}

	// Close the file.
	mOutput.close();
}
