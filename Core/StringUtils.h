#pragma once
#include "StdafxCore.h"

namespace StringUtils
{
//-- ToDo: 
#pragma warning(push)
#pragma warning(disable: 4996)
	class Converter
	{
	protected:
		typedef std::wstring_convert<std::codecvt_utf8<wchar_t>> WideConverter;
		typedef std::wstring_convert<std::codecvt_utf8<wchar_t>> ByteConverter;
		typedef WideConverter::wide_string                       WideString;
		typedef ByteConverter::byte_string                       ByteString;

	public:

		//! Returns byte string from wide string.
		static ByteString WStringToString(const std::wstring& pString)
		{
			ByteConverter lConverter;
			return lConverter.to_bytes(pString);
		}

		//! Returns wide string from byte string.
		static WideString StringToWString(const std::string& pString)
		{
			WideConverter lConverter;
			return lConverter.from_bytes(pString.c_str());
		}

		static void ToLower(String& pString)
		{
			std::transform(pString.begin(), pString.end(), pString.begin(), ::tolower);
		}

		static void ToLower(std::string& pString)
		{
			std::transform(pString.begin(), pString.end(), pString.begin(), ::tolower);
		}

		static std::string ToLower(const std::string& pString)
		{
			std::string result = pString;

			ToLower(result);

			return result;
		}

		static void ToUpper(String& pString)
		{
			std::transform(pString.begin(), pString.end(), pString.begin(), ::toupper);
		}

		static int ToInt(const String& pString)
		{
			return StrToInt(pString.c_str());
		}

		static String FormatString(CConstString pFormat, ...)
		{
			va_list vlist;
			va_start(vlist, pFormat);

			Char lMessage[1024];
			vsprintfs(lMessage, pFormat, vlist);

			va_end(vlist);

			return String(lMessage);
		}
	};

	void Split(const String& source, const String& delimiter, std::vector<String>& tokens);

#pragma warning(pop)
}
