#include "StdafxCore.h"
#include "ImageFormat.h"

/**
 * Function : GetExtension
 */
String GetExtension(const ImageFormat format)
{
	switch (format)
	{
		case ImageFormat::WicBmp:
			return String(__TEXT(".bmp"));

		case ImageFormat::WicGif:
			return String(__TEXT(".gif"));

		case ImageFormat::WicIco:
			return String(__TEXT(".ico"));

		case ImageFormat::WicJpeg:
			return String(__TEXT(".jpg"));

		case ImageFormat::WicPng:
			return String(__TEXT(".png"));

		case ImageFormat::WicTiff:
			return String(__TEXT(".tiff"));

		case ImageFormat::WicWmp:
			return String(__TEXT(".wmp"));

		case ImageFormat::DDS:
			return String(__TEXT(".dds"));

		default:
			assert(false && __TEXT("This format doesn't support."));
			return String();
	}
}

/**
 * Function : GetImageFormat
 */
ImageFormat GetImageFormat(const String& extension)
{
	if (extension == __TEXT(".bmp"))
		return ImageFormat::WicBmp;

	if (extension == __TEXT(".gif"))
		return ImageFormat::WicGif;

	if (extension == __TEXT(".ico"))
		return ImageFormat::WicIco;

	if (extension == __TEXT(".jpeg") || extension == __TEXT(".jpg"))
		return ImageFormat::WicJpeg;

	if (extension == __TEXT(".png"))
		return ImageFormat::WicPng;

	if (extension == __TEXT(".tiff"))
		return ImageFormat::WicTiff;

	if (extension == __TEXT(".wmp"))
		return ImageFormat::WicWmp;

	if (extension == __TEXT(".dds"))
		return ImageFormat::DDS;

	return ImageFormat::Unknown;
}
