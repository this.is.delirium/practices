#include "StdafxCore.h"
#include "FBXLoader.h"
#include "ECS/MeshComponent.h"
#include "ECS/SceneComponent.h"
#include "StringUtils.h"

/**
 * Function : Release
 */
void FbxLoader::Release()
{
	Clear();
}

/**
 * Function : LoadFile
 */
size_t FbxLoader::LoadFile(const String& path, const bool useBatch)
{
	FbxManager* sdkManager = FbxManager::Create();

	FbxImporter* importer = FbxImporter::Create(sdkManager, "");

	std::string asciiString = StringUtils::Converter::WStringToString(path);

	if (importer->Initialize(asciiString.c_str(), -1))
	{
		FbxScene* scene = FbxScene::Create(sdkManager, "myScene");
		SLogger::Instance().AddMessage(LogMessageType::Verbose, __TEXT("Loading StaticMesh from %s..."), path.c_str());
		importer->Import(scene);
		importer->Destroy();

		FbxNode* rootNode = scene->GetRootNode();
		if (rootNode)
		{
			for (int i = 0; i < rootNode->GetChildCount(); ++i)
			{
				FbxNode* node = rootNode->GetChild(i);
				const char* name = node->GetInitialName();
				int childCount = node->GetChildCount();

				if (node->GetMesh() || childCount > 0)
					if (!useBatch || mObjects.empty())
					AddNewNode(node);

				Node& internalNode = mObjects.back();
				// TODO: Rewrite (recursive?).
				if (node->GetMesh())
					AddNewMesh(internalNode, node, sdkManager, useBatch);
				else if (childCount > 0)
				{
					for (int i = 0; i < childCount; ++i)
					{
						FbxNode* child = node->GetChild(i);
						if (child->GetMesh())
							AddNewMesh(internalNode, child, sdkManager, useBatch);
					}
				}
			}
		}
	}
	else
	{
		auto status = importer->GetStatus();
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Failed to load static mesh: %s"), status.GetErrorString());
	}

	sdkManager->Destroy();

	return mObjects.size();
}

/**
 * Function : AddNewNode
 */
FbxLoader::Node& FbxLoader::AddNewNode(FbxNode* fbxNode)
{
	mObjects.push_back(Node());

	Node& node = mObjects.back();
	node.NamePart = std::string(fbxNode->GetName());

	return node;
}

/**
 * Function : AddNewMesh
 */
void FbxLoader::AddNewMesh(Node& node, FbxNode* fbxNode, FbxManager* sdkManager, const bool useBatch)
{
	int vertexCount = 0;
	int triangleCount = 0;

	if (!useBatch || node.Parts.empty())
		node.Parts.push_back(MeshBuffer());

	MeshBuffer& buffer = node.Parts.back();

	if (useBatch)
	{
		buffer.VertexBatches.push_back(MeshBatch());
		buffer.IndexBatches.push_back(MeshBatch());

		buffer.VertexBatches.back().StartIndex	= buffer.Vertices.size();
		buffer.IndexBatches.back().StartIndex	= buffer.Indices.size();
	}

	FbxMesh* mesh = fbxNode->GetMesh();
	if (!mesh->IsTriangleMesh())
	{
		std::unique_ptr<FbxGeometryConverter> geometryConverter(new FbxGeometryConverter(sdkManager));

		FbxNodeAttribute* convertedNode = geometryConverter->Triangulate(mesh, true);
		if (convertedNode != nullptr && convertedNode->GetAttributeType() == FbxNodeAttribute::eMesh)
			mesh = convertedNode->GetNode()->GetMesh();
	}

	//SLogger::Instance().AddMessage(LogMessageType::Verbose, __TEXT("MESH!"));
	vertexCount = mesh->GetPolygonVertexCount();
	triangleCount = mesh->GetPolygonCount();
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Mesh with %i vertices and %i triangles"), vertexCount, triangleCount);

	FbxAMatrix& localTransform = fbxNode->EvaluateLocalTransform();
	const int* polygonVertices = mesh->GetPolygonVertices();
	FbxArray<FbxVector4> normals;
	mesh->GetPolygonVertexNormals(normals);

	int UVCount = mesh->GetUVLayerCount();
	FbxArray<FbxVector2> UVs;

	if (UVCount > 0)
		mesh->GetPolygonVertexUVs(mesh->GetElementUV(0)->GetName(), UVs);

	for (int iPolygonVertex = 0; iPolygonVertex < vertexCount; ++iPolygonVertex)
	{
		Vertex vertex;
		memset(&vertex, 0, sizeof(Vertex)); // debug cleaning
		int vertexIndex = polygonVertices[iPolygonVertex];
		FbxVector4& point = mesh->GetControlPointAt(vertexIndex);
		FbxVector4 test = localTransform.MultT(point);
		FbxVector4& normal = normals[iPolygonVertex];
		vertex.Position = DirectX::XMFLOAT3((float)test[0], (float)test[1], (float)test[2]);
		vertex.Normal = DirectX::XMFLOAT3((float)normal[0], (float)normal[1], (float)normal[2]);

		if (UVCount > 0)
		{
			auto& uv = UVs[iPolygonVertex];
			vertex.TextureUV = DirectX::XMFLOAT2((float)uv.mData[0], 1.0f - (float)uv.mData[1]);
		}
		buffer.Vertices.push_back(vertex);
		buffer.Indices.push_back(iPolygonVertex + (useBatch ? buffer.IndexBatches.back().StartIndex : 0));

		//mBoundingBox.Union(vec4(vertex.mPosition, 1.0f));
	}

	if (useBatch)
	{
		buffer.VertexBatches.back().NumberElements	= buffer.Vertices.size() - buffer.VertexBatches.back().StartIndex;
		buffer.IndexBatches.back().NumberElements	= buffer.Indices.size() - buffer.IndexBatches.back().StartIndex;
	}
}

/**
 * Function : FillMeshComponent
 */
void FbxLoader::FillMeshComponent(const size_t loadedObjId, std::shared_ptr<MeshComponent>& mesh)
{
	Node& node = mObjects[loadedObjId];

	assert(node.Parts.size() == 1);

	MeshBuffer& buffer = node.Parts[0];
	RHIBatchDesc batchDesc = { buffer.Vertices.data(), buffer.Vertices.size() * sizeof(Vertex), sizeof(Vertex), buffer.Indices.data(), buffer.Indices.size() * sizeof(uint32_t), (buffer.Indices.size() ? RHIFormat::R32Uint : RHIFormat::Unknown), RHIPrimitiveTopology::TriangleList };

	for (auto& batch : buffer.VertexBatches)
		batchDesc.VertexPartition.emplace_back(batch.StartIndex, batch.NumberElements);

	for (auto& batch : buffer.IndexBatches)
		batchDesc.IndexPartition.emplace_back(batch.StartIndex, batch.NumberElements);

	mesh->Initialize(batchDesc);
}
