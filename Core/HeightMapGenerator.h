#pragma once
#include "StdafxCore.h"
#include "HeightMapDataWrapper.h"
#include "HeightMapGeneratorAlgo.h"
#include <ProgressIndicator.h>

struct HeightMap
{
	std::vector<float>	mValues;
	DirectX::XMUINT2	mSize;

	FORCEINLINE float Value(const uint32_t x, const uint32_t y) const { return mValues[mSize.x * y + x]; }

	FORCEINLINE const DirectX::XMUINT2& Size() const { return mSize; }

	FORCEINLINE size_t SizeOfType() const { return sizeof(float); }
};

class HeightMapGenerator
{
public:

	void operator() (const DirectX::XMUINT2& sizeMap, ProgressIndicator* progress = nullptr)
	{
	#ifdef _DEBUG
		assert(mAlgo.get() != nullptr);
	#endif

		mMap.mSize = sizeMap;
		mMap.mValues.resize(sizeMap.x * sizeMap.y);

		mAlgo->Apply(mMap.Size(), mMap.mValues, progress);
	}

	FORCEINLINE const DirectX::XMUINT2& SizeMap() const { return mMap.Size(); }
	FORCEINLINE const float* Result() const { return mMap.mValues.data(); }
	FORCEINLINE const HeightMap& Map() const { return mMap; }
	FORCEINLINE size_t SizeOfType() const { return mMap.SizeOfType(); }

	template<class Algo, class... ArgTypes>
	void CreateAlgo(ArgTypes&&... args)
	{
		mAlgo = std::make_shared<Algo>(args...);
	}

	bool IsEmpty() const { return mAlgo.get() == nullptr; }

protected:
	std::shared_ptr<HeightMapGeneratorAlgo>	mAlgo;
	HeightMap								mMap;
};
