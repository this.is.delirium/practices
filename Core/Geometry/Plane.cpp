#include <StdafxCore.h>
#include "Plane.h"

#include <D3D12/private/DXMathHelper.h>
using namespace DirectX;

/**
 * Function : IsFront
 */
bool Plane::IsFront(const DirectX::XMFLOAT4& point) const
{
	mAuxiliaryVec = XMLoadFloat4(&point);
	return XMVectorGetX(XMPlaneDot(mPlaneEquation, mAuxiliaryVec)) >= 0.0f;
}

/**
 * Function : IsFront
 */
bool Plane::IsFront(const DirectX::XMFLOAT3& point) const
{
	return IsFront(MakeFloat4(point, 1.0f));
}
