#include <StdafxCore.h>
#include "AABB.h"

#include <D3D12/private/DXMathHelper.h>
using namespace DirectX;

/**
 * Function : Add
 */
void AABB::Add(const XMFLOAT3& point)
{
	mMin = DirectX::Min(mMin, point);
	mMax = DirectX::Max(mMax, point);

	MarkDirty();
}

/**
 * Function : Add
 */
void AABB::Add(const AABB& bbox)
{
	mMin = DirectX::Min(mMin, bbox.Min());
	mMax = DirectX::Max(mMax, bbox.Max());

	MarkDirty();
}

/**
 * Function : Length
 */
const XMFLOAT3& AABB::Length() const
{
	if (IsDirty())
		mLength = XMFLOAT3(mMax - mMin);

	return mLength;
}

/**
 * Function : Center
 */
XMFLOAT3 AABB::Center() const
{
	return (mMin + mMax) * 0.5f;
}
