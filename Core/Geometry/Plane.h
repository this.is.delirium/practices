#pragma once
#include <StdafxCore.h>

class Plane
{
public:
	Plane(const float A = 0.0f, const float B = 0.0f, const float C = 0.0f, const float D = 0.0f)
	{
		mPlaneEquation = DirectX::XMVectorSet(A, B, C, D);
	}

	Plane(const DirectX::XMFLOAT4& equation)
	{
		mPlaneEquation = DirectX::XMLoadFloat4(&equation);
	}

	FORCEINLINE void Normalize() { mPlaneEquation = DirectX::XMPlaneNormalize(mPlaneEquation); }

	bool IsFront(const DirectX::XMFLOAT4& point) const;
	bool IsFront(const DirectX::XMFLOAT3& point) const;

protected:
	DirectX::XMVECTOR mPlaneEquation;
	mutable DirectX::XMVECTOR mAuxiliaryVec;
};
