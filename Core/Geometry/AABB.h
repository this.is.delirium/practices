#pragma once
#include <StdafxCore.h>

class AABB
{
public:
	AABB(const DirectX::XMFLOAT3& min = DirectX::XMFLOAT3(9999.0f, 9999.0f, 9999.0f), const DirectX::XMFLOAT3& max = DirectX::XMFLOAT3(-9999.0f, -9999.0f, -9999.0f))
		: mMin(min), mMax(max), mIsDirty(true) { }

	void Add(const DirectX::XMFLOAT3& point);
	void Add(const AABB& bbox);

	FORCEINLINE bool IsDirty() const { return mIsDirty; }
	FORCEINLINE void MarkDirty() { mIsDirty = true; }

	const DirectX::XMFLOAT3& Length() const;
	DirectX::XMFLOAT3 Center() const;

	FORCEINLINE const DirectX::XMFLOAT3& Min() const { return mMin; }
	FORCEINLINE const DirectX::XMFLOAT3& Max() const { return mMax; }

protected:
	DirectX::XMFLOAT3			mMin;
	DirectX::XMFLOAT3			mMax;

	bool						mIsDirty;

	mutable DirectX::XMFLOAT3	mLength;
};
