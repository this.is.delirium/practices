#pragma once
#include "StdafxCore.h"
#include <ConstantBuffers.h>
#include <Serialize/ISerializable.h>

class ViewFrustum;

class Camera;
using CameraPtr = std::shared_ptr<Camera>;

class Camera : public ISerializable
{
public:
	enum class MoveType
	{
		Forward,
		Backward,
		Right,
		Left
	};

public:
	enum class InitializeFormat
	{
		FromDirection		= 0,
		FromSphericalAngles	= 1
	};

	enum class ProjectionType
	{
		Perspective		= 0,
		Orthographic	= 1
	};

	struct CommonPart
	{
		DirectX::XMFLOAT4	Position;
		DirectX::XMFLOAT2	ZRange;
	};

	struct PerspectivePart
	{
		float	FoV;
		float	AspectRatio;
	};

	struct OrthographicPart
	{
		DirectX::XMFLOAT4	View; // x - left, y - right, z - bottom, w - top.
	};

	struct DirectionPart
	{
		DirectX::XMFLOAT4	Direction;
		DirectX::XMFLOAT4	Up;
	};

	struct SphericalAnglesPart
	{
		DirectX::XMFLOAT2	SphericalAngles;
		DirectX::XMFLOAT2	AngleXRange;
		DirectX::XMFLOAT2	AngleYRange;
	};

	struct CameraDesc
	{
		CommonPart Common;

		ProjectionType ProjType;
		union
		{
			OrthographicPart	OrthoPart;
			PerspectivePart		PerspPart;
		};

		InitializeFormat Format;
		union
		{
			DirectionPart		DirPart;
			SphericalAnglesPart	AnglesPart;
		};
	};

public:
	struct Frustum
	{
		FORCEINLINE DirectX::XMFLOAT4& WorldPosition(const int i) { return WorldPositions[i]; }
		FORCEINLINE DirectX::XMFLOAT4& ViewPosition(const int i) { return ViewPositions[i]; }

		FORCEINLINE float Near() const { return ZRange.x; }
		FORCEINLINE float Far() const { return ZRange.y; }

		DirectX::XMFLOAT4 WorldPositions[8];
		DirectX::XMFLOAT4 ViewPositions[8];
		DirectX::XMFLOAT4 Center;
		DirectX::XMFLOAT2 ZRange;
		DirectX::XMFLOAT4 Color;
		DirectX::XMMATRIX LightView; // TODO: Think about it and move out.
		DirectX::XMMATRIX Projection;
	};

	struct ViewFrustumDesc
	{
		int					NbOfSplit	= 0;
		DirectX::XMFLOAT4	Colors[4];
		float				Lambda;
		Camera*				ViewCamera	= nullptr;
		Camera*				LightCamera	= nullptr;
	};

public:
	[[deprecated("Use Camera(CameraDesc&)")]]
	Camera();
	Camera(const CameraDesc& desc, const std::string& name);
	Camera(const Camera& camera);
	~Camera();

	[[deprecated("Use Camera(CameraDesc&)")]]
	void Initialize(const CameraDesc& desc);
	void InitializeViewFrustum(const ViewFrustumDesc& desc);

	void UpdateBuffer(ConstBufferPerFrame& cb);

	//void	Move(CameraMove pType, float pDeltaTime);

	/**
	 * Rotate camera using the position of the cursor.
	 * @param pDiffX = (WidthScreen / 2 - MousePos.X).
	 * @param pDiffY - (HeightScren / 2 - MousePos.Y).
	 */
	void RotateFromMouse(int pDiffX, int pDiffY, float pDeltaTime);

	FORCEINLINE const DirectX::XMMATRIX& Projection()
	{
		if (IsDirty())
			UpdateInternalData();

		return mProjection;
	}
	FORCEINLINE const DirectX::XMMATRIX& LookAt()
	{
		if (IsDirty())
			UpdateInternalData();

		return mLookAt;
	}
	FORCEINLINE const DirectX::XMMATRIX& InverseProjection()
	{
		if (IsDirty())
			UpdateInternalData();

		return mInvProjection;
	}
	FORCEINLINE const DirectX::XMMATRIX& InverseLookAt()
	{
		if (IsDirty())
			UpdateInternalData();

		return mInvLookAt;
	}

	FORCEINLINE DirectX::XMMATRIX ViewProjectionMatrix()
	{
		return LookAt() * Projection();
	}

	FORCEINLINE DirectX::XMMATRIX InverseViewProjectionMatrix()
	{
		return InverseLookAt() * InverseProjection();
	}

	FORCEINLINE DirectX::XMFLOAT2& Angles() { return mAngles; }
	FORCEINLINE const DirectX::XMFLOAT2& AngleXRange() const { return mAngleXRange; }
	FORCEINLINE const DirectX::XMFLOAT2& AngleYRange() const { return mAngleYRange; }

	FORCEINLINE DirectX::XMFLOAT4& Position() { return mPosition; }
	FORCEINLINE const DirectX::XMFLOAT4& Position() const { return mPosition; }

	FORCEINLINE DirectX::XMFLOAT4& Direction() { return mDirection; }
	FORCEINLINE const DirectX::XMFLOAT4& Direction() const { return mDirection; }

	FORCEINLINE void SetPosition(const DirectX::XMFLOAT4& position)
	{
		mPosition = position;
		MarkDirty();
	}

	FORCEINLINE const DirectX::XMFLOAT4& UpVector() const { return mUp; }
	FORCEINLINE const DirectX::XMFLOAT4& RightVector() const { return mRight; }

	FORCEINLINE DirectX::XMFLOAT2& ZRange() { return mZRange; }
	FORCEINLINE const DirectX::XMFLOAT2& ZRange() const { return mZRange; }

	FORCEINLINE float FoV() const { return mFoV; }
	FORCEINLINE float& FoV() { return mFoV; }

	FORCEINLINE float AspectRatio() const { return mAspectRatio; }
	FORCEINLINE float& AspectRatio() { return mAspectRatio; }

	FORCEINLINE void MarkDirty() { mIsDirty = true; }
	FORCEINLINE bool IsDirty() const { return mIsDirty; }

	FORCEINLINE void SetZRange(const DirectX::XMFLOAT2& range)
	{
		mZRange = range;
		MarkDirty();
	}

	void Move(const MoveType type, const float value);

public:
	FORCEINLINE void SetName(const std::string& name) { mName = name; }
	FORCEINLINE const std::string& Name() const { return mName; }

public:
	FORCEINLINE std::shared_ptr<ViewFrustum>& GetViewFrustum() { return mViewFrustum; }
	FORCEINLINE const ViewFrustumDesc& GetViewFrustumDesc() const { return mViewFrustumDesc; }
	FORCEINLINE Frustum& GetFrustum(const int id)
	{
		if (IsDirty())
			UpdateInternalData();

		return mFrustums[id];
	}
	FORCEINLINE size_t NbOfFrustums() const { return mFrustums.size(); }

public: //-- ISerializable
	void Serialize(nlohmann::json& js) const override final;
	void Deserialize(nlohmann::json& js) override final;

protected:
	void UpdateInternalData();

	void UpdateFrustums();

	/**
	 * Calculate follow directions: mDirection, mRight, mUp
	 * based on horizontal and vertical angles.
	 */
	void CalculateDirections();

protected:
	using BuildMatricesFunc = std::function<void(void)>;

	void BuildPerspectiveMatrices();
	void BuildOrthographicMatrices();

protected:
	void SetDefaultName();

protected:
	static int						kCameraId;

	ProjectionType					mProjectionType;

	DirectX::XMFLOAT4				mPosition;
	DirectX::XMFLOAT4				mDirection;
	DirectX::XMFLOAT4				mUp;
	DirectX::XMFLOAT4				mRight;
	DirectX::XMMATRIX				mLookAt;
	DirectX::XMMATRIX				mProjection;
	DirectX::XMMATRIX				mInvLookAt;
	DirectX::XMMATRIX				mInvProjection;
	DirectX::XMFLOAT4				mDepthParams;

	bool							mUseAngles;
	DirectX::XMFLOAT2				mAngles;			//<! x - horizontal angle, y - vertical angle.
	DirectX::XMFLOAT2				mAngleXRange;
	DirectX::XMFLOAT2				mAngleYRange;

	DirectX::XMFLOAT2				mZRange;			//<! x - zNear, y - zFar.
	float							mFoV		= 45.f;
	float							mAspectRatio;
	bool							mIsDirty;

	float							mSpeed		= 1.0f;
	float							mSpeedMouse	= 0.005f;

	DirectX::XMFLOAT4				mViewOrtho;

	std::string						mName;

	BuildMatricesFunc				BuildMatrices;

	ViewFrustumDesc					mViewFrustumDesc;
	std::shared_ptr<ViewFrustum>	mViewFrustum;
	std::vector<Frustum>			mFrustums;
};
