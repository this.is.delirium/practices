#pragma once
#include "StdafxCore.h"

class RenderSurface
{
public:
	FORCEINLINE UINT Width() const { return mWidth; }
	FORCEINLINE UINT Height() const { return mHeight; }
	FORCEINLINE HWND WindowHandle() const { return mWindowHandle; }
	FORCEINLINE bool IsFullScreen() const { return mIsFullScreen; }
	FORCEINLINE bool IsWindowed() const { return !mIsFullScreen; }
	FORCEINLINE bool IsEnabledGPUValidation() const { return mIsEnabledGPUValidation; }

public:
	HWND	mWindowHandle			= 0;
	UINT	mWidth					= 0;
	UINT	mHeight					= 0;
	bool	mIsFullScreen			= false;
	bool	mIsEnabledGPUValidation	= false;
};
