#include "StdafxCore.h"
#include "PerlinNoiseAlgo.h"
#include "Interpolation.h"

/**
 * Function : Apply
 */
void PerlinNoiseAlgo::Apply(const DirectX::XMUINT2& sizeMap, std::vector<float>& map, ProgressIndicator* progress) const
{
	if (progress)
	{
		progress->SetMinValue(0);
		progress->SetMaxValue(sizeMap.y);
		progress->SetCurrentValue(0);
		progress->SetStep(1);
	}
	uint32_t x, y;
	std::random_device rndDevice;
	std::default_random_engine engine(rndDevice());
	std::uniform_int_distribution<uint32_t> distr(0, 10);

	for (y = 0; y < sizeMap.y; ++y)
	{
		for (x = 0; x < sizeMap.x; ++x)
		{
			map[sizeMap.x * y + x] = PerlinNoise(x, y, distr(engine));
		}

		if (progress)
		{
			progress->Next();
		}
	}
}

namespace
{
	float clamp(const float value, const float low, const float high)
	{
		return std::min(std::max(value, low), high);
	}
}

/**
 * Function : PerlinNoise
 */
float PerlinNoiseAlgo::PerlinNoise(uint32_t x, uint32_t y, const uint32_t frac) const
{
	float result = 0.0f;

	float frequency = mStartFrequency;
	float amplitude = mStartAmplitude;

	x += frac;
	y += frac;

	for (int i = 0; i < mNbOfOctaves; ++i)
	{
		result += Noise(x * frequency, y * frequency) * amplitude;
		amplitude *= mPersistence;
		frequency *= 2.0f;
	}

	return clamp(result, 0.0f, 1.0f);
}

/**
 * Function : Noise
 */
float PerlinNoiseAlgo::Noise(const float x, const float y) const
{
	const float intX	= floor(x);
	const float intY	= floor(y);
	const float coefX	= x - intX;
	const float coefY	= y - intY;

	const float values[4] = {
		SmoothNoise(intX    , intY    ),
		SmoothNoise(intX + 1, intY    ),
		SmoothNoise(intX    , intY + 1),
		SmoothNoise(intX + 1, intY + 1)
	};

	const float lerps[2] = {
		Interpolation::Quantic(values[0], values[1], coefX),
		Interpolation::Quantic(values[2], values[3], coefX)
	};

	return Interpolation::Quantic(lerps[0], lerps[1], coefY);
}

/**
 * Function : SmoothNoise
 */
float PerlinNoiseAlgo::SmoothNoise(const float x, const float y) const
{
	float corners = (Noise2D(x - 1, y - 1) + Noise2D(x + 1, y - 1) + Noise2D(x - 1, y + 1) + Noise2D(x + 1, y + 1)) * 0.0625f;
	float sides = (Noise2D(x - 1, y) + Noise2D(x + 1, y) + Noise2D(x, y - 1) + Noise2D(x, y + 1)) * 0.125f;

	float center = Noise2D(x, y) * 0.25f;

	return corners + sides + center;
}
