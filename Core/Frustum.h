#pragma once
#include "StdafxCore.h"
#include <Geometry/AABB.h>
#include <Geometry/Plane.h>

class Camera;

//! It is used for the Frustum Culling algorithm.
class Frustum
{
public:
	void Build(Camera* camera);

	//! Returns true if point inside of the frustum.
	bool Check(const DirectX::XMFLOAT3& point) const;

	//! Returns true if point inside of the frustum.
	bool Check(const DirectX::XMFLOAT4& point) const;

	//! Returns false if fully outside, true if inside or intersects.
	bool Check(const AABB& bbox) const;

protected:
	void BuildPlanes(Camera* camera);
	void BuildPositions(Camera* camera);

protected:
	enum class PlaneId
	{
		Left = 0,
		Right,
		Top,
		Bottom,
		Near,
		Far,
		Count
	};

	Plane				mPlanes[ToInt(PlaneId::Count)];
	DirectX::XMFLOAT3	mPoints[8];
};
