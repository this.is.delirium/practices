#include "StdafxCore.h"
#include "SceneComponent.h"

using namespace DirectX;

/**
 * Function : CalculateMatrix
 */
void SceneComponent::CalculateMatrix()
{
	XMMATRIX scaleMatrix		= XMMatrixScaling(mScale.x, mScale.y, mScale.z);
	XMMATRIX translateMatrix	= XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z);

	/**
	 * Angles are measured clockwise when looking along the rotation axis toward the origin. This is a left-handed coordinate system. To use right-handed coordinates, negate all three angles.
	 * The order of transformations is roll first, then pitch, and then yaw. The rotations are all applied in the global coordinate frame.
	 */
	mLocalRotateMatrix			= XMMatrixRotationRollPitchYaw(Pitch(), Yaw(), Roll());

	const XMVECTOR axis			= XMVectorSet(mAxis.x, mAxis.y, mAxis.z, 0.0f);
	const bool isZero			= XMVectorGetX(XMVector3Length(axis)) < 0.01f;
	XMMATRIX axisRotation		= isZero ? XMMatrixIdentity() : XMMatrixRotationAxis(XMVectorSet(mAxis.x, mAxis.y, mAxis.z, 0.0f), mAxis.w);

	mWorldMatrix				= scaleMatrix * mLocalRotateMatrix * translateMatrix * axisRotation;
	mIsDirty					= false;
}
