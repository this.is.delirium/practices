#pragma once
#include "StdafxCore.h"

class BaseComponent
{
public:
	virtual ~BaseComponent() { }

	template<class ReturnType, class... ArgTypes>
	static ReturnType* CreateSubobject(ArgTypes&&... args)
	{
		ReturnType* subobject = new ReturnType(args...);
		//subobject->Initialize();
		return subobject;
	}

	//virtual void Initialize() = 0;

protected:
};
