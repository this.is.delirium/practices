#pragma once
#include "StdafxCore.h"
#include "BaseComponent.h"
#include <RHIBatch.h>

class MeshComponent : BaseComponent
{
public:
	virtual ~MeshComponent();

	virtual void Initialize(RHIBatchDesc& desc);

	/*virtual void Initialize(
		const void*					vertexData,
		const UINT					vertexDataSizeInBytes,
		const UINT					vertexStride,
		const void*					indexData,
		const UINT					indexDataSizeInBytes,
		const RHIFormat				indexFormat,
		const RHIPrimitiveTopology	topology = RHIPrimitiveTopology::TriangleList);

	virtual void Initialize(
		const UINT					maxVertices,
		const UINT					vertexStride,
		const bool					useIndexBuffer	= false,
		const UINT					maxIndices		= 0,
		const RHIFormat				indexFormat		= RHIFormat::R32Uint,
		const RHIPrimitiveTopology	topology		= RHIPrimitiveTopology::TriangleList);

	virtual void Update(RHISubresourceData* vertexData, RHISubresourceData* indexData);*/

	void Draw(RHICommandList* commandList);

public:
	FORCEINLINE const RHIBatch& Batch() const { return mBatch; }

protected:
	RHIBatch mBatch;
};
