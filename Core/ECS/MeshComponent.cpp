#include "StdafxCore.h"
#include "MeshComponent.h"
#include <Render/GraphicsCore.h>
#include <RHIUtils.h>

using namespace GraphicsCore;

/**
 * Function : ~MeshComponent
 */
MeshComponent::~MeshComponent()
{
	mBatch.Release();
}

/**
 * Function : Initialize
 */
void MeshComponent::Initialize(RHIBatchDesc& desc)
{
	mBatch.Initialize(__TEXT("MeshComponentBatch:Batch"), desc);
}

/**
 * Function : Initialize
 */
/*void MeshComponent::Initialize(
	const void*					vertexData,
	const UINT					vertexDataSizeInBytes,
	const UINT					vertexStride,
	const void*					indexData,
	const UINT					indexDataSizeInBytes,
	const RHIFormat				indexFormat,
	const RHIPrimitiveTopology	topology)
{
	mTopology		= topology;
	mHasIndexBuffer	= (indexData != nullptr);

	mVertexBuffer.Initialize(__TEXT("MeshComponent:VertexBuffer"), vertexDataSizeInBytes, vertexDataSizeInBytes / vertexStride, vertexStride, vertexData);

	if (mHasIndexBuffer)
	{
		const UINT strideIndex = RHIUtils::GetSize(indexFormat);
		mIndexBuffer.Initialize(__TEXT("MeshComponent:IndexBuffer"), indexDataSizeInBytes, indexDataSizeInBytes / strideIndex, strideIndex, indexData);
	}
}*/

/**
 * Function : Initialize
 */
/*void MeshComponent::Initialize(
	const UINT					maxVertices,
	const UINT					vertexStride,
	const bool					useIndexBuffer,
	const UINT					maxIndices,
	const RHIFormat				indexFormat,
	const RHIPrimitiveTopology	topology)
{
	mTopology		= topology;
	mHasIndexBuffer	= useIndexBuffer;

	mVertexBuffer.Initialize(__TEXT("MeshComponent:VertexBuffer"), maxVertices * vertexStride, maxVertices, vertexStride);

	if (mHasIndexBuffer)
	{
		const UINT strideIndex = RHIUtils::GetSize(indexFormat);
		mIndexBuffer.Initialize(__TEXT("MeshComponent:IndexBuffer"), maxIndices * strideIndex, maxIndices, strideIndex);
	}
}*/

/**
 * Function : Update
 */
/*void MeshComponent::Update(RHISubresourceData* vertexData, RHISubresourceData* indexData)
{
	gRenderer->UpdateBuffer(&mVertexBuffer, vertexData);

	if (mHasIndexBuffer)
		gRenderer->UpdateBuffer(&mIndexBuffer, indexData);
}*/

/**
 * Function : Draw
 */
void MeshComponent::Draw(RHICommandList* commandList)
{
	commandList->IASetPrimitiveTopology(mBatch.Topology());
	commandList->IASetVertexBuffers(0, 1, mBatch.VertexBufferView());
	if (mBatch.HasIndexBuffer())
	{
		commandList->IASetIndexBuffer(mBatch.IndexBufferView());
		for (auto& batch : mBatch.IndexPartition())
			commandList->DrawIndexedInstanced(batch.NumberElements, 1, batch.StartIndex, 0, 0);
	}
	else
	{
		for (auto& batch : mBatch.VertexPartition())
			commandList->DrawInstanced(batch.NumberElements, 1, batch.StartIndex, 0);
	}
}
