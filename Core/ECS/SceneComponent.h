#pragma once
#include "StdafxCore.h"
#include "BaseComponent.h"

class SceneComponent : public BaseComponent
{
public:
	virtual ~SceneComponent() { }

	//virtual void Initialize() override;

	FORCEINLINE void SetPosition(const DirectX::XMFLOAT3& position)
	{
		mPosition = position;
		MarkDirty();
	}
	FORCEINLINE void SetScale(const DirectX::XMFLOAT3& scale)
	{
		mScale = scale;
		MarkDirty();
	}
	FORCEINLINE void SetRotation(const DirectX::XMFLOAT3& rotation)
	{
		mLocalRotation = rotation;
		MarkDirty();
	}
	// -Y
	FORCEINLINE void AddRotateToPitch(const float add)
	{
		mLocalRotation.y += add;
		MarkDirty();
	}
	// X
	FORCEINLINE void AddRotateToRoll(const float add)
	{
		mLocalRotation.x += add;
		MarkDirty();
	}
	// -Z
	FORCEINLINE void AddRotateToYaw(const float add)
	{
		mLocalRotation.z += add;
		MarkDirty();
	}

	FORCEINLINE float& Roll() { return mLocalRotation.x; }
	FORCEINLINE float& Pitch() { return mLocalRotation.y; }
	FORCEINLINE float& Yaw() { return mLocalRotation.z; }

	FORCEINLINE void SetAxisRotation(const DirectX::XMFLOAT3& axis)
	{
		mAxis = DirectX::XMFLOAT4(axis.x, axis.y, axis.z, 0.0f);
		MarkDirty();
	}

	FORCEINLINE void AddAxisRotation(const float add)
	{
		mAxis.w += add;
		MarkDirty();
	}

	const DirectX::XMMATRIX& WorldMatrix()
	{
		if (IsDirty())
			CalculateMatrix();

		return mWorldMatrix;
	}

	const DirectX::XMMATRIX& LocalRotateMatrix()
	{
		if (IsDirty())
			CalculateMatrix();

		return mLocalRotateMatrix;
	}

	FORCEINLINE void MarkDirty() { mIsDirty = true; }
	FORCEINLINE bool IsDirty() const { return mIsDirty; }

protected:
	void CalculateMatrix();

protected:
	DirectX::XMFLOAT3	mPosition		= DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	DirectX::XMFLOAT3	mScale			= DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f);
	DirectX::XMFLOAT3	mLocalRotation	= DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);	// Roll, Pitch, Yaw.
	DirectX::XMFLOAT4	mAxis			= DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f); // xyz - axis, w - time;

	bool				mIsDirty		= true;
	DirectX::XMMATRIX	mWorldMatrix;
	DirectX::XMMATRIX	mLocalRotateMatrix;

};
