#pragma once
#include "StdafxCore.h"
#include "Patterns/Singleton.h"
#include <RHITexture.h>

class TextureManager : public Singleton<TextureManager>
{
public:
	using Textures = std::unordered_map<String, TextureRef>;

public:
	const Textures& GetTextures() const { return mTextures; }

	TextureRef GetTexture(const String& absolutePath);

protected:
	TextureRef LoadTexture(const String& absolutePath);

protected:
	Textures mTextures;
};
