#include "StdafxCore.h"
#include "IImGui.h"

#include <imgui.h>
#include <imgui_internal.h>

/**
 * Function : FindHoveredWindow
 */
bool IImGui::FindHoveredWindow(ImVec2* pos, bool excludingChilds)
{
	return ImGui::IsMouseHoveringAnyWindow() || ImGui::IsMouseHoveringWindow();
}
