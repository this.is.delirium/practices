#include "StdafxCore.h"

#include <imgui.h>

#include "ImGuiHelper.h"
#include <Scene/SceneObject.h>
#include <Transformation.h>

#include <RHITexture.h>

using namespace rttr;

RTTR_REGISTRATION
{
	registration::class_<std::string>("std::string");
}

#define ShowImGuiControl(ControlName, obj, prop, value, isChanged) \
	if (ImGui::ControlName(prop.get_name().data(), &value)) \
	{ \
		prop.set_value(obj, value); \
		isChanged = true; \
	}

/**
 * Function : ImGuiHelper
 */
ImGuiHelper::ImGuiHelper()
{
	using namespace std::placeholders;

	mDisplayFunctions[type::get<float>().get_id()]				= std::bind(&ImGuiHelper::ShowFloat, this, _1, _2);
	mDisplayFunctions[type::get<int>().get_id()]				= std::bind(&ImGuiHelper::ShowInt, this, _1, _2);
	mDisplayFunctions[type::get<bool>().get_id()]				= std::bind(&ImGuiHelper::ShowBool, this, _1, _2);
	mDisplayFunctions[type::get<std::string>().get_id()]		= std::bind(&ImGuiHelper::ShowString, this, _1, _2);

	mDisplayFunctions[type::get<Transformation>().get_id()]		= std::bind(&ImGuiHelper::ShowTransformation, this, _1, _2);
	mDisplayFunctions[type::get<DirectX::XMFLOAT3>().get_id()]	= std::bind(&ImGuiHelper::ShowFloat3, this, _1, _2);
	mDisplayFunctions[type::get<DirectX::XMVECTOR>().get_id()]	= std::bind(&ImGuiHelper::ShowVector, this, _1, _2);
	mDisplayFunctions[type::get<DirectX::XMFLOAT4>().get_id()]	= std::bind(&ImGuiHelper::ShowFloat4, this, _1, _2);
}

/**
 * Function : ShowInstance
 */
bool ImGuiHelper::ShowInstance(rttr::instance& obj)
{
	#if _DEBUG
	assert(obj.is_valid() && "An instance isn't valid");
	#endif // _DEBUG

	bool result = false;

	type t = obj.get_type();
	for (const auto& prop : t.get_properties())
	{
		result |= ShowProperty(obj, prop);
	}

	return result;
}

/**
 * Function : ShowProperty
 */
bool ImGuiHelper::ShowProperty(rttr::instance& obj, const rttr::property& prop)
{
	uintptr_t id = prop.get_type().get_id();
	#if _DEBUG
	assert(mDisplayFunctions.find(id) != mDisplayFunctions.cend() && "There isn't a display function for the current property.");
	#endif // _DEBUG.

	return mDisplayFunctions[id](obj, prop);
}

/**
 * Function : ShowFloat
 */
bool ImGuiHelper::ShowFloat(rttr::instance& obj, const rttr::property& floatProperty)
{
	#if _DEBUG
	assert(floatProperty.get_type() == type::get<float>() && "floatProperty isn't a float type.");
	#endif

	bool isChanged = false;

	variant var = floatProperty.get_value(obj);
	float& value = var.get_value<float>();

	ShowImGuiControl(DragFloat, obj, floatProperty, value, isChanged)

	return isChanged;
}

/**
 * Function : ShowInt
 */
bool ImGuiHelper::ShowInt(rttr::instance& obj, const rttr::property& intProperty)
{
	#if _DEBUG
	assert(intProperty.get_type() == type::get<int>() && "floatProperty isn't a float type.");
	#endif

	bool isChanged = false;

	variant var = intProperty.get_value(obj);
	int& value = var.get_value<int>();

	ShowImGuiControl(DragInt, obj, intProperty, value, isChanged)

	return isChanged;
}

/**
 * Function : ShowBool
 */
bool ImGuiHelper::ShowBool(rttr::instance& obj, const rttr::property& boolProperty)
{
	#if _DEBUG
	assert(boolProperty.get_type() == type::get<bool>() && "floatProperty isn't a float type.");
	#endif

	bool isChanged = false;

	variant var = boolProperty.get_value(obj);
	bool& value = var.get_value<bool>();

	ShowImGuiControl(Checkbox, obj, boolProperty, value, isChanged)

	return isChanged;
}

/**
 * Function : ShowString
 */
bool ImGuiHelper::ShowString(rttr::instance& obj, const rttr::property& strProperty)
{
	variant value = strProperty.get_value(obj);
	auto& text = value.get_wrapped_value<std::string>();

	ImGui::Text("Name: %s, value: %s", strProperty.get_name().data(), text.data());
	return false;
}

/**
 * Function : ShowSceneObject
 */
bool ImGuiHelper::ShowSceneObject(rttr::instance& obj, const rttr::property& sceneObjProperty)
{
	return false;
}

/**
 * Function : ShowTransformation
 */
bool ImGuiHelper::ShowTransformation(rttr::instance& obj, const rttr::property& transformProperty)
{
	bool isChanged = false;
	variant var = transformProperty.get_value(obj);
	auto& transform = var.get_value<Transformation>();

	rttr::instance objTransform(transform);
	type t = objTransform.get_type();
	for (auto& prop : t.get_properties())
	{
		isChanged |= ShowProperty(objTransform, prop);
	}

	if (isChanged)
	{
		transformProperty.set_value(obj, transform);
	}

	return isChanged;
}

/**
 * Function : ShowFloat3
 */
bool ImGuiHelper::ShowFloat3(rttr::instance& obj, const rttr::property& float3Property)
{
	bool isChanged = false;
	variant value = float3Property.get_value(obj);
	auto& float3 = value.get_value<DirectX::XMFLOAT3>();

	if (ImGui::DragFloat3(float3Property.get_name().data(), &float3.x))
	{
		float3Property.set_value(obj, float3);

		isChanged = true;
	}

	return isChanged;
}

/**
 * Function : ShowFloat4
 */
bool ImGuiHelper::ShowFloat4(rttr::instance& obj, const rttr::property& float4Property)
{
	bool isChanged = false;
	variant value = float4Property.get_value(obj);
	auto float4 = value.get_value<DirectX::XMFLOAT4>();

	if (ImGui::DragFloat4(float4Property.get_name().data(), &float4.x))
	{
		float4Property.set_value(obj, float4);

		isChanged = true;
	}

	return isChanged;
}

/**
 * Function : ShowVector
 */
bool ImGuiHelper::ShowVector(rttr::instance& obj, const rttr::property& vectorProperty)
{
	bool isChanged = false;

	variant value = vectorProperty.get_value(obj);
	auto& vector = value.get_value<DirectX::XMVECTOR>();

	// ToDo: Reconsider later.
	if (ImGui::DragFloat4(vectorProperty.get_name().data(), vector.m128_f32))
	{
		vectorProperty.set_value(obj, vector);

		isChanged = true;
	}

	return isChanged;
}

/**
 * Function : ShowTexture
 */
void ImGuiHelper::ShowTexture(const RHITexture* texture, const DirectX::XMFLOAT2& size, const DirectX::XMFLOAT2& tooltipSize, const float tooltipZoom) const
{
	ImGuiIO& io	= ImGui::GetIO();
	ImVec2 pos	= ImGui::GetCursorScreenPos();

	RHIResourceDesc desc	= texture->Texture()->GetDesc();
	ImTextureID texId		= (void *)texture->SrvHandle().Gpu().ptr;
	const float texWidth	= (float)desc.Width;
	const float texHeight	= (float)desc.Height;

	ImGui::Text("%.0fx%.0f", texWidth, texHeight);
	ImGui::Image(texId, ImVec2(size.x, size.y), ImVec2(0, 0), ImVec2(1, 1), ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 128));

	if (ImGui::IsItemHovered())
	{
		const float regionX = tooltipSize.x / tooltipZoom;
		const float regionY = tooltipSize.y / tooltipZoom;
		float leftCornerX = io.MousePos.x - pos.x - regionX * 0.5f; if (leftCornerX < 0.0f) leftCornerX = 0.0f; else if (leftCornerX > texWidth - regionX) leftCornerX = texWidth - regionX;
		float leftCornerY = io.MousePos.y - pos.y - regionY * 0.5f; if (leftCornerY < 0.0f) leftCornerY = 0.0f; else if (leftCornerY > texHeight - regionY) leftCornerY = texHeight - regionY;
		ImVec2 uv0 = ImVec2((leftCornerX) / size.x, (leftCornerY) / size.y);
		ImVec2 uv1 = ImVec2((leftCornerX + regionX) / size.x, (leftCornerY + regionY) / size.y);

		ImGui::BeginTooltip();
		ImGui::Text("Min: (%.2f, %.2f)", leftCornerX, leftCornerY);
		ImGui::Text("Max: (%.2f, %.2f)", leftCornerX + regionX, leftCornerY + regionY);
		ImGui::Image(texId, ImVec2(tooltipSize.x, tooltipSize.y), uv0, uv1, ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 128));
		ImGui::EndTooltip();
	}
}
