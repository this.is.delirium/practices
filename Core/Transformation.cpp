#include "StdafxCore.h"
#include "Transformation.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<Transformation>("Transformation")
		.constructor<>()
		.property("Scale", &Transformation::mScale, registration::public_access)
		.property("Translation", &Transformation::mTranslation, registration::public_access)
		.property("Rotation", &Transformation::getQuaternion, &Transformation::setQuaternion);
		//.property("Translation", &Transformation::GetTranslation, &Transformation::SetTranslation)
		//.property("Rotation", &Transformation::mRotation, registration::public_access);
}

using namespace DirectX;

/**
 * Function : Transformation
 */
Transformation::Transformation()
	: mScale(1.0f, 1.0f, 1.0f)
	, mTranslation(0.0f, 0.0f, 0.0f)
{
	mRotation = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
}

/**
 * Function : WorldMatrix
 */
void Transformation::WorldMatrix(DirectX::XMMATRIX& out)
{
	out = 
		XMMatrixScaling(mScale.x, mScale.y, mScale.z) * 
		XMMatrixRotationQuaternion(mRotation) * 
		XMMatrixTranslation(mTranslation.x, mTranslation.y, mTranslation.z);
}

/**
 * Function : NormalMatrix
 */
void Transformation::NormalMatrix(DirectX::XMMATRIX& out)
{
	out = XMMatrixRotationQuaternion(mRotation);
}

//-- ToDo: Reconsider later.
const DirectX::XMFLOAT4& Transformation::getQuaternion() const
{
	XMVECTOR axis;
	float angle;
	XMQuaternionToAxisAngle(&axis, &angle, mRotation);

	XMStoreFloat4(&mTest, axis);
	mTest.w = angle;
	//XMStoreFloat4(&mTest, mRotation);

	return mTest;
}

//-- ToDo: Reconsider later.
void Transformation::setQuaternion(const DirectX::XMFLOAT4& quat)
{
	XMFLOAT3 axis3(quat.x, quat.y, quat.z);
	XMVECTOR axis = XMLoadFloat3(&axis3);

	mRotation = XMQuaternionRotationAxis(axis, quat.w);
}
