#pragma once
#include "StdafxCore.h"
#include "IImGui.h"

class IRenderer;
class RHICommandList;
class RHIDescriptorHeap;
class RHIPipelineState;
class RHIResource;
struct RHIRootSignature;

class ImGuiDirectX12 : public IImGui
{
public:
	ImGuiDirectX12(Window* window, IRenderer* renderer)
		: IImGui(window, renderer) { }

	~ImGuiDirectX12() { }

	void Release() override final;
	bool Initialize(const int numFrames) override final;

	void NewFrame() override final;
	void Render(RHICommandList* cmdList) override final;

	bool UpdateMouseCursor() override final;

protected:
	void InvalidateDeviceObjects() override final;
	void CreateFontsTexture() override final;
	bool CreateDeviceObjects() override final;

protected:
	void CreateRootSignature();
	void CreatePSO();

protected:
	struct FrameResources
	{
		void Release();

		RHIResource*	IB					= nullptr;
		RHIResource*	VB					= nullptr;
		int				VertexBufferSize	= 0;
		int				IndexBufferSize		= 0;
	};
	static const size_t kMaxFrames = 8;
	using Frames = std::array<FrameResources, kMaxFrames>;

	RHIRootSignature*		mRootSignature		= nullptr;
	RHIPipelineState*		mPSO				= nullptr;
	RHIResource*			mFontTexture		= nullptr;

	INT64					mTime				= 0;
	INT64					mTicksPerSecond		= 0;

	Frames					mFrameResources;
	int						mNumFrames;
	int						mFrameIndex;
protected:
	struct VertexConstantBuffer
	{
		float mvp[4][4];
	};
};
