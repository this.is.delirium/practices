#pragma once
#include <StdafxCore.h>
#include <RHIShaderDefinition.h>

namespace Pass
{

namespace GeometryPass
{

enum class RootSignatureParams : UINT
{
	PerFrame = 0u,
	PerObject,
	AlbedoTexture,
	NormalTexture,
	SpecularTexture,
	Count
};

static RHIShaderDefinition shader = RHIShaderDefinition(
	"GeometryPass",
	{
		{ RHIShaderType::Vertex, __TEXT("DeferredShading:GPass.vs") },
		{ RHIShaderType::Pixel, __TEXT("DeferredShading:GPass.ps") }
	},
	RHIInputLayoutDesc({ InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) })
);

} // end of namespace GeometryPass.

} // end of namespace Pass.
