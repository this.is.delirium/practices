#include "StdafxCore.h"
#include "RenderCommandContextManager.h"

/**
 * Function : Release
 */
void RenderCommandContextManager::Release()
{
	for (auto& rcc : mRenderCommandContexts)
	{
		rcc.second.Release();
	}
}

/**
 * Function : Initialize
 */
void RenderCommandContextManager::Initialize()
{
	mRenderCommandContexts[RenderCommandContextType::Task0] = RenderCommandContext();
	mRenderCommandContexts[RenderCommandContextType::Task1] = RenderCommandContext();
	mRenderCommandContexts[RenderCommandContextType::Task2] = RenderCommandContext();
	mRenderCommandContexts[RenderCommandContextType::Task3] = RenderCommandContext();
	mRenderCommandContexts[RenderCommandContextType::Task4] = RenderCommandContext();
	mRenderCommandContexts[RenderCommandContextType::Task5] = RenderCommandContext();
	mRenderCommandContexts[RenderCommandContextType::Task6] = RenderCommandContext();
	mRenderCommandContexts[RenderCommandContextType::Task7] = RenderCommandContext();
	mRenderCommandContexts[RenderCommandContextType::Task8] = RenderCommandContext();
	mRenderCommandContexts[RenderCommandContextType::Task9] = RenderCommandContext();

	for (auto& rcc : mRenderCommandContexts)
	{
		rcc.second.Initialize();
	}
}

/**
 * Function : getScopedRCC
 */
RenderCommandContextManager::ScopedRenderCommandContext RenderCommandContextManager::getScopedRCC(const String& name, const RenderCommandContextType type)
{
	return ScopedRenderCommandContext(name, mRenderCommandContexts[type]);
}
