#pragma once
#include "StdafxCore.h"
#include "RenderCommandContext.h"

enum class RenderCommandContextType
{
	Task0 = 0,
	Task1,
	Task2,
	Task3,
	Task4,
	Task5,
	Task6,
	Task7,
	Task8,
	Task9
};

class RenderCommandContextManager
{
public:
	class ScopedRenderCommandContext
	{
	public:
		ScopedRenderCommandContext(const String& name, RenderCommandContext& rcc)
			: mName(name), mRenderCommandContext(rcc) { }

		~ScopedRenderCommandContext()
		{
			mRenderCommandContext.Flush();
		}

		RenderCommandContext& getRCC() { return mRenderCommandContext; }

	protected:
		String mName;
		RenderCommandContext& mRenderCommandContext;
	};

public:

	void Release();
	void Initialize();

	ScopedRenderCommandContext getScopedRCC(const String& name, const RenderCommandContextType type);

protected:
	using RCCContainer = std::map<RenderCommandContextType, RenderCommandContext>;

protected:
	RCCContainer mRenderCommandContexts;
};
