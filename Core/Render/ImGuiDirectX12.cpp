#include "StdafxCore.h"

#include <imgui.h>

#include "ImGuiDirectX12.h"
#include "IRenderer.h"

#include <D3D12/D3D12CommandAllocator.h>
#include <D3D12/D3D12CommandList.h>
#include <D3D12/D3D12DescriptorHeap.h>
#include <D3D12/D3D12Device.h>
#include <D3D12/D3D12Resource.h>
#include <D3D12/D3D12SwapChain.h>

#include "ResourceManager.h"
#include "Window.h"

namespace
{
	ImGuiMouseCursor gLastMouseCursor = ImGuiMouseCursor_COUNT;
	RHIDescriptorHandle gFontSrvHandle;
}

/**
 * Function : Release
 */
void ImGuiDirectX12::FrameResources::Release()
{
	RHISafeRelease(IB);
	RHISafeRelease(VB);
}

/**
 * Function : Release
 */
void ImGuiDirectX12::Release()
{
	InvalidateDeviceObjects();
}

/**
 * Function : InvalidateDeviceObjects
 */
void ImGuiDirectX12::InvalidateDeviceObjects()
{
	RHISafeRelease(mRootSignature);
	RHISafeRelease(mPSO);
	RHISafeRelease(mFontTexture);
	ImGui::GetIO().Fonts->TexID = 0; // We copied g_pFontTextureView to io.Fonts->TexID so let's clear that as well.

	for (auto& frameRes : mFrameResources)
	{
		frameRes.Release();
	}

	gFontSrvHandle.Release();
}

/**
 * Function : Initialize
 */
bool ImGuiDirectX12::Initialize(const int numFrames)
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	mNumFrames	= numFrames;
	mFrameIndex	= UINT_MAX;

	// Initialize frame resources.
	for (int i = 0; i < mNumFrames; i++)
	{
		mFrameResources[i].IB = nullptr;
		mFrameResources[i].VB = nullptr;
		mFrameResources[i].VertexBufferSize = 5000;
		mFrameResources[i].IndexBufferSize = 10000;
	}

	if (!QueryPerformanceFrequency((LARGE_INTEGER *)&mTicksPerSecond))
	{
		return false;
	}

	if (!QueryPerformanceCounter((LARGE_INTEGER *)&mTime))
	{
		return false;
	}

	// Setup back-end capabilities flags
	//ImGuiIO& io = ImGui::GetIO();
	io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;   // We can honor GetMouseCursor() values (optional)
	io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;    // We can honor io.WantSetMousePos requests (optional, rarely used)

															// Keyboard mapping. ImGui will use those indices to peek into the io.KeysDown[] array that we will update during the application lifetime.
	io.KeyMap[ImGuiKey_Tab] = VK_TAB;
	io.KeyMap[ImGuiKey_LeftArrow] = VK_LEFT;
	io.KeyMap[ImGuiKey_RightArrow] = VK_RIGHT;
	io.KeyMap[ImGuiKey_UpArrow] = VK_UP;
	io.KeyMap[ImGuiKey_DownArrow] = VK_DOWN;
	io.KeyMap[ImGuiKey_PageUp] = VK_PRIOR;
	io.KeyMap[ImGuiKey_PageDown] = VK_NEXT;
	io.KeyMap[ImGuiKey_Home] = VK_HOME;
	io.KeyMap[ImGuiKey_End] = VK_END;
	io.KeyMap[ImGuiKey_Insert] = VK_INSERT;
	io.KeyMap[ImGuiKey_Delete] = VK_DELETE;
	io.KeyMap[ImGuiKey_Backspace] = VK_BACK;
	io.KeyMap[ImGuiKey_Space] = VK_SPACE;
	io.KeyMap[ImGuiKey_Enter] = VK_RETURN;
	io.KeyMap[ImGuiKey_Escape] = VK_ESCAPE;
	io.KeyMap[ImGuiKey_A] = 'A';
	io.KeyMap[ImGuiKey_C] = 'C';
	io.KeyMap[ImGuiKey_V] = 'V';
	io.KeyMap[ImGuiKey_X] = 'X';
	io.KeyMap[ImGuiKey_Y] = 'Y';
	io.KeyMap[ImGuiKey_Z] = 'Z';

	io.ImeWindowHandle = mWindow->GetHwnd();

	// Setup style
	//ImGui::StyleColorsDark();
	ImGui::StyleColorsClassic();

	// Load Fonts
	// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them. 
	// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple. 
	// - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
	// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
	// - Read 'misc/fonts/README.txt' for more instructions and details.
	// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
	//io.Fonts->AddFontDefault();
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
	//ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
	//IM_ASSERT(font != NULL);

	return true;
}

/**
 * Function : CreateFontsTexture
 */
void ImGuiDirectX12::CreateFontsTexture()
{
	// Build texture atlas
	ImGuiIO& io = ImGui::GetIO();
	unsigned char* pixels;
	int width, height;
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

	// Upload texture to graphics system
	{
		RHIHeapProperties props		= {};
		props.Type					= RHIHeapType::Default;
		props.CPUPageProperty		= RHICpuPageProperty::Unknown;
		props.MemoryPoolPreference	= RHIMemoryPool::Unknown;
		props.CreationNodeMask		= 1;
		props.VisibleNodeMask		= 1;

		RHIResourceDesc desc = {};
		desc.Dimension			= RHIResourceDimension::Texture2D;
		desc.Alignment			= 0;
		desc.Width				= width;
		desc.Height				= height;
		desc.DepthOrArraySize	= 1;
		desc.MipLevels			= 1;
		desc.Format				= RHIFormat::R8G8B8A8UNorm;
		desc.SampleDesc.Count	= 1;
		desc.SampleDesc.Quality	= 0;
		desc.Layout				= RHITextureLayout::Unknown;
		desc.Flags				= RHIResourceFlag::None;

		RHIResource* pTexture = mRenderer->Device()->CreateCommittedResource(&props, RHIHeapFlag::None, &desc, RHIResourceState::CopyDest, nullptr);

		UINT uploadPitch		= (width * 4 + RHIConstants::kTextureDataPitchAlignment - 1u) & ~(RHIConstants::kTextureDataPitchAlignment - 1u);
		UINT uploadSize			= height * uploadPitch;
		desc.Dimension			= RHIResourceDimension::Buffer;
		desc.Alignment			= 0;
		desc.Width				= uploadSize;
		desc.Height				= 1;
		desc.DepthOrArraySize	= 1;
		desc.MipLevels			= 1;
		desc.Format				= RHIFormat::Unknown;
		desc.SampleDesc.Count	= 1;
		desc.SampleDesc.Quality	= 0;
		desc.Layout				= RHITextureLayout::RowMajor;
		desc.Flags				= RHIResourceFlag::None;

		props.Type					= RHIHeapType::Upload;
		props.CPUPageProperty		= RHICpuPageProperty::Unknown;
		props.MemoryPoolPreference	= RHIMemoryPool::Unknown;

		RHIResource* uploadBuffer = mRenderer->Device()->CreateCommittedResource(&props, RHIHeapFlag::None, &desc, RHIResourceState::GenericRead, nullptr);
		assert(uploadBuffer != nullptr);

		void* mapped = nullptr;
		RHIRange range(0, uploadSize);
		uploadBuffer->Map(0, &range, &mapped);

		for (int y = 0; y < height; y++)
			memcpy((void*)((uintptr_t)mapped + y * uploadPitch), pixels + y * width * 4, width * 4);
		uploadBuffer->Unmap(0, &range);

		RHITextureCopyLocation srcLocation				= {};
		srcLocation.Resource							= uploadBuffer;
		srcLocation.Type								= RHITextureCopyType::PlacedFootprint;
		srcLocation.PlacedFootprint.Footprint.Format	= RHIFormat::R8G8B8A8UNorm;
		srcLocation.PlacedFootprint.Footprint.Width		= width;
		srcLocation.PlacedFootprint.Footprint.Height	= height;
		srcLocation.PlacedFootprint.Footprint.Depth		= 1;
		srcLocation.PlacedFootprint.Footprint.RowPitch	= uploadPitch;

		RHITextureCopyLocation dstLocation	= {};
		dstLocation.Resource				= pTexture;
		dstLocation.Type					= RHITextureCopyType::SubresourceIndex;
		dstLocation.SubresourceIndex		= 0;

		RHIResourceBarrier barrier		= {};
		barrier.Type					= RHIResourceBarrierType::Transition;
		barrier.Flags					= RHIResourceBarrierFlag::None;
		barrier.Transition.Resource		= pTexture;
		barrier.Transition.Subresource	= RHIConstants::kResourceBarrierAllSubresources;
		barrier.Transition.StateBefore	= RHIResourceState::CopyDest;
		barrier.Transition.StateAfter	= RHIResourceState::PixelShaderResource;

		RHIFence* fence = mRenderer->Device()->CreateFence(0, RHIFenceFlags::None);
		assert(fence != nullptr);
		//IM_ASSERT(SUCCEEDED(hr));

		HANDLE event = CreateEvent(0, 0, 0, 0);
		IM_ASSERT(event != NULL);

		RHICommandQueueDesc queueDesc = {};
		queueDesc.Type		= RHICommandListType::Direct;
		queueDesc.Flags		= RHICommandQueueFlags::None;
		queueDesc.NodeMask	= 1;

		RHICommandQueue* cmdQueue = mRenderer->Device()->CreateCommandQueue(&queueDesc);
		assert(cmdQueue != nullptr);

		RHICommandAllocator* cmdAlloc = mRenderer->Device()->CreateCommandAllocator(RHICommandListType::Direct);
		assert(cmdAlloc != nullptr);

		RHICommandList* cmdList = mRenderer->Device()->CreateCommandList(0, RHICommandListType::Direct, cmdAlloc, nullptr);
		assert(cmdAlloc != nullptr);

		cmdList->CopyTextureRegion(&dstLocation, 0, 0, 0, &srcLocation, NULL);
		cmdList->ResourceBarrier(1, &barrier);

		cmdList->Close();

		RHICommandList* cmdLists[] = { cmdList };
		cmdQueue->ExecuteCommandLists(1, cmdLists);
		cmdQueue->Signal(fence, 1);

		fence->SetEventOnCompletion(1, event);
		WaitForSingleObject(event, INFINITE);

		cmdList->Release();
		cmdAlloc->Release();
		cmdQueue->Release();
		CloseHandle(event);
		fence->Release();
		uploadBuffer->Release();

		// Create texture view
		RHIShaderResourceViewDesc srvDesc	= {};
		srvDesc.Format						= RHIFormat::R8G8B8A8UNorm;
		srvDesc.ViewDimension				= RHISRVDimension::Texture2D;
		srvDesc.Texture2D.MipLevels			= desc.MipLevels;
		srvDesc.Texture2D.MostDetailedMip	= 0;
		srvDesc.Shader4ComponentMapping		= D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

		gFontSrvHandle.Initialize(mRenderer->CbvSrvUavHeapManager());

		mRenderer->Device()->CreateShaderResourceView(pTexture, &srvDesc, gFontSrvHandle.Cpu());

		if (mFontTexture != NULL)
			mFontTexture->Release();
		mFontTexture = pTexture;

		// Store our identifier
		static_assert(sizeof(ImTextureID) >= sizeof(gFontSrvHandle.Gpu().ptr), "Can't pack descriptor handle into TexID");
		io.Fonts->TexID = (void *)gFontSrvHandle.Gpu().ptr;
	}
}

/**
 * Function : CreateDeviceObjects
 */
bool ImGuiDirectX12::CreateDeviceObjects()
{
	if (!mRenderer->Device())
	{
		return false;
	}

	if (mPSO)
	{
		InvalidateDeviceObjects();
	}

	CreateRootSignature();
	CreatePSO();

	CreateFontsTexture();

	return true;
}

/**
 * Function : CreateRootSignature
 */
void ImGuiDirectX12::CreateRootSignature()
{
	RHIDescriptorRange descRange = {};
	descRange.RangeType = RHIDescriptorRangeType::SRV;
	descRange.NumDescriptors = 1;
	descRange.BaseShaderRegister = 0;
	descRange.RegisterSpace = 0;
	descRange.OffsetInDescriptorsFromTableStart = 0;

	RHIRootParameter param[2] = {};

	param[0].ParameterType = RHIRootParameterType::Constants32Bit;
	param[0].Constants.ShaderRegister = 0;
	param[0].Constants.RegisterSpace = 0;
	param[0].Constants.Num32BitValues = 16;
	param[0].ShaderVisibility = RHIShaderVisibility::Vertex;

	param[1].ParameterType = RHIRootParameterType::DescriptorTable;
	param[1].DescriptorTable.NumDescriptorRanges = 1;
	param[1].DescriptorTable.DescriptorRanges = &descRange;
	param[1].ShaderVisibility = RHIShaderVisibility::Pixel;

	RHIStaticSamplerDesc staticSampler = {};
	staticSampler.Filter = RHIFilter::MinMagMipLinear;
	staticSampler.AddressU = RHITextureAddressMode::Wrap;
	staticSampler.AddressV = RHITextureAddressMode::Wrap;
	staticSampler.AddressW = RHITextureAddressMode::Wrap;
	staticSampler.MipLODBias = 0.f;
	staticSampler.MaxAnisotropy = 0;
	staticSampler.ComparisonFunc = RHIComparisonFunc::Always;
	staticSampler.BorderColor = RHIStaticBorderColor::TransparentBlack;
	staticSampler.MinLOD = 0.f;
	staticSampler.MaxLOD = 0.f;
	staticSampler.ShaderRegister = 0;
	staticSampler.RegisterSpace = 0;
	staticSampler.ShaderVisibility = RHIShaderVisibility::Pixel;

	RHIRootSignatureDesc desc = {};
	desc.NumParameters = _countof(param);
	desc.Parameters = param;
	desc.NumStaticSamplers = 1;
	desc.StaticSamplers = &staticSampler;
	desc.Flags = GetRootSignatureFlags(true, true, false, false, false, true, false);

	RHIBlobWrapper signature(nullptr);
	RHIBlobWrapper error(nullptr);
	mRenderer->Device()->SerializeRootSignature(&desc, RHIRootSignatureVersion::Ver1, &signature.Blob, &error.Blob);

	if (error.Blob && !error.Blob->IsNull())
	{
		MessageBoxA(NULL, (LPCSTR)error.GetBufferPointer(), "Error", MB_OK | MB_ICONERROR);
		return;
	}

	mRootSignature = mRenderer->Device()->CreateRootSignature(0, signature.GetBufferPointer(), signature.GetBufferSize());
}

/**
 * Function : CreatePSO
 */
void ImGuiDirectX12::CreatePSO()
{
	// Create a pipeline state.
	{
		RHIBlobWrapper vertexShader	(mRenderer->LoadShader(__TEXT("ImGui:Main.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(mRenderer->LoadShader(__TEXT("ImGui:Main.ps"), RHIShaderType::Pixel));

		RHIInputElementDesc inputElementDesc[] =
		{
			{ "POSITION",	0, RHIFormat::R32G32Float,		0, (size_t)(&((ImDrawVert*)0)->pos),	RHIInputClassification::PerVertexData, 0 },
			{ "TEXCOORD",	0, RHIFormat::R32G32Float,		0, (size_t)(&((ImDrawVert*)0)->uv),		RHIInputClassification::PerVertexData, 0 },
			{ "COLOR",		0, RHIFormat::R8G8B8A8UNorm,	0, (size_t)(&((ImDrawVert*)0)->col),	RHIInputClassification::PerVertexData, 0 },
		};

		RHIGraphicsPipelineStateDesc desc			= {};
		desc.RootSignature							= mRootSignature;
		desc.InputLayout							= { inputElementDesc, _countof(inputElementDesc) };
		desc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		desc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		desc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		desc.NodeMask								= 1;
		desc.SampleMask								= UINT_MAX;
		desc.NumRenderTargets						= 1;
		desc.RTVFormats[0]							= mRenderer->RenderTargetFormat();
		desc.SampleDesc.Count						= 1;
		desc.Flags									= RHIPipelineStateFlags::None;

		// Rasterizer state.
		{
			RHIRasterizerDesc& state = desc.RasterizerState;
			state.FillMode				= RHIFillMode::Solid;
			state.CullMode				= RHICullMode::None;
			state.FrontCounterClockwise	= FALSE;
			state.DepthBias				= RHIConstants::kDefaultDepthBias;
			state.DepthBiasClamp		= RHIConstants::kDefaultDepthBiasClamp;
			state.SlopeScaledDepthBias	= RHIConstants::kDefaultSlopeScaledDepthBias;
			state.DepthClipEnable		= TRUE;
			state.MultisampleEnable		= FALSE;
			state.AntialiasedLineEnable	= FALSE;
			state.ForcedSampleCount		= 0;
			state.ConservativeRaster	= RHIConservativeRasterizationMode::Off;
		}

		// Blend state.
		{
			RHIBlendDesc& state = desc.BlendState;
			state.AlphaToCoverageEnable					= false;
			state.IndependentBlendEnable				= false;
			state.RenderTarget[0].BlendEnable			= true;
			state.RenderTarget[0].LogicOpEnable			= false;
			state.RenderTarget[0].SrcBlend				= RHIBlend::SrcAlpha;
			state.RenderTarget[0].DestBlend				= RHIBlend::InvSrcAlpha;
			state.RenderTarget[0].BlendOp				= RHIBlendOp::Add;
			state.RenderTarget[0].SrcBlendAlpha			= RHIBlend::InvSrcAlpha;
			state.RenderTarget[0].DestBlendAlpha		= RHIBlend::Zero;
			state.RenderTarget[0].BlendOpAlpha			= RHIBlendOp::Add;
			state.RenderTarget[0].RenderTargetWriteMask	= ToInt(RHIColorWriteEnable::All);
		}

		// Depth-Stencil state.
		{
			RHIDepthStencilDesc& state = desc.DepthStencilState;

			state.DepthEnable				= FALSE;
			state.StencilEnable				= FALSE;
			state.DepthFunc					= RHIDepthFunc::Always;
			state.DepthWriteMask			= RHIDepthWriteMask::On;
			state.FrontFace.StencilFailOp	= state.FrontFace.StencilDepthFailOp = state.FrontFace.StencilPassOp = RHIStencilOp::Keep;
			state.FrontFace.StencilFunc		= RHIComparisonFunc::Always;
			state.BackFace					= state.FrontFace;
		}

		mPSO = mRenderer->Device()->CreateGraphicsPipelineState(&desc);
	}

	mRenderer->CommandList()->Reset(mRenderer->CommandAllocator(), mPSO);
}

/**
 * Function : UpdateMouseCursor
 */
bool ImGuiDirectX12::UpdateMouseCursor()
{
	ImGuiIO& io = ImGui::GetIO();
	if (io.ConfigFlags & ImGuiConfigFlags_NoMouseCursorChange)
		return false;

	ImGuiMouseCursor imgui_cursor = io.MouseDrawCursor ? ImGuiMouseCursor_None : ImGui::GetMouseCursor();
	if (imgui_cursor == ImGuiMouseCursor_None)
	{
		// Hide OS mouse cursor if imgui is drawing it or if it wants no cursor
		::SetCursor(NULL);
	}
	else
	{
		// Hardware cursor type
		LPTSTR win32_cursor = IDC_ARROW;
		switch (imgui_cursor)
		{
		case ImGuiMouseCursor_Arrow:        win32_cursor = IDC_ARROW; break;
		case ImGuiMouseCursor_TextInput:    win32_cursor = IDC_IBEAM; break;
		case ImGuiMouseCursor_ResizeAll:    win32_cursor = IDC_SIZEALL; break;
		case ImGuiMouseCursor_ResizeEW:     win32_cursor = IDC_SIZEWE; break;
		case ImGuiMouseCursor_ResizeNS:     win32_cursor = IDC_SIZENS; break;
		case ImGuiMouseCursor_ResizeNESW:   win32_cursor = IDC_SIZENESW; break;
		case ImGuiMouseCursor_ResizeNWSE:   win32_cursor = IDC_SIZENWSE; break;
		}
		::SetCursor(::LoadCursor(NULL, win32_cursor));
	}
	return true;
}

/**
 * Function : NewFrame
 */
void ImGuiDirectX12::NewFrame()
{
	if (!mPSO)
	{
		CreateDeviceObjects();
	}

	//g_pd3dCommandList = command_list;

	ImGuiIO& io = ImGui::GetIO();

	// Setup display size (every frame to accommodate for window resizing)
	RECT rect;
	GetClientRect(mWindow->GetHwnd(), &rect);
	io.DisplaySize = ImVec2((float)(rect.right - rect.left), (float)(rect.bottom - rect.top));

	// Setup time step
	INT64 current_time;
	QueryPerformanceCounter((LARGE_INTEGER *)&current_time);
	io.DeltaTime = (float)(current_time - mTime) / mTicksPerSecond;
	mTime = current_time;

	// Read keyboard modifiers inputs
	io.KeyCtrl = (GetKeyState(VK_CONTROL) & 0x8000) != 0;
	io.KeyShift = (GetKeyState(VK_SHIFT) & 0x8000) != 0;
	io.KeyAlt = (GetKeyState(VK_MENU) & 0x8000) != 0;
	io.KeySuper = false;
	// io.KeysDown : filled by WM_KEYDOWN/WM_KEYUP events
	// io.MousePos : filled by WM_MOUSEMOVE events
	// io.MouseDown : filled by WM_*BUTTON* events
	// io.MouseWheel : filled by WM_MOUSEWHEEL events

	// Set OS mouse position if requested (only used when ImGuiConfigFlags_NavEnableSetMousePos is enabled by user)
	if (io.WantSetMousePos)
	{
		POINT pos = { (int)io.MousePos.x, (int)io.MousePos.y };
		ClientToScreen(mWindow->GetHwnd(), &pos);
		SetCursorPos(pos.x, pos.y);
	}

	// Update OS mouse cursor with the cursor requested by imgui
	ImGuiMouseCursor mouse_cursor = io.MouseDrawCursor ? ImGuiMouseCursor_None : ImGui::GetMouseCursor();
	if (gLastMouseCursor != mouse_cursor)
	{
		gLastMouseCursor = mouse_cursor;
		UpdateMouseCursor();
	}

	// Start the frame. This call will update the io.WantCaptureMouse, io.WantCaptureKeyboard flag that you can use to dispatch inputs (or not) to your application.
	ImGui::NewFrame();
}

/**
 * Function : Render
 * This is the main rendering function that you have to implement and provide to ImGui (via setting up 'RenderDrawListsFn' in the ImGuiIO structure)
 * If text or lines are blurry when integrating ImGui in your engine:
 * - in your Render function, try translating your projection matrix by (0.5f,0.5f) or (0.375f,0.375f)
 */
void ImGuiDirectX12::Render(RHICommandList* cmdList)
{
	ImGui::Render();
	ImGuiIO& io = ImGui::GetIO();
	ImDrawData* drawData = ImGui::GetDrawData();
	// NOTE: I'm assuming that this only get's called once per frame!
	// If not, we can't just re-allocate the IB or VB, we'll have to do a proper allocator.
	++mFrameIndex;
	FrameResources* frameResources = &mFrameResources[mFrameIndex % mNumFrames];
	RHIResource* g_pVB = frameResources->VB;
	RHIResource* g_pIB = frameResources->IB;
	int g_VertexBufferSize = frameResources->VertexBufferSize;
	int g_IndexBufferSize = frameResources->IndexBufferSize;

	// Create and grow vertex/index buffers if needed
	if (!g_pVB || g_VertexBufferSize < drawData->TotalVtxCount)
	{
		if (g_pVB) { g_pVB->Release(); g_pVB = NULL; }
		g_VertexBufferSize = drawData->TotalVtxCount + 5000;
		RHIHeapProperties props		= {};
		props.Type					= RHIHeapType::Upload;
		props.CPUPageProperty		= RHICpuPageProperty::Unknown;
		props.MemoryPoolPreference	= RHIMemoryPool::Unknown;
		props.CreationNodeMask		= 1;
		props.VisibleNodeMask		= 1;
		RHIResourceDesc desc		= {};
		desc.Dimension				= RHIResourceDimension::Buffer;
		desc.Width					= g_VertexBufferSize * sizeof(ImDrawVert);
		desc.Height					= 1;
		desc.DepthOrArraySize		= 1;
		desc.MipLevels				= 1;
		desc.Format					= RHIFormat::Unknown;
		desc.SampleDesc.Count		= 1;
		desc.Layout					= RHITextureLayout::RowMajor;
		desc.Flags					= RHIResourceFlag::None;

		g_pVB = mRenderer->Device()->CreateCommittedResource(&props, RHIHeapFlag::None, &desc, RHIResourceState::GenericRead, nullptr);
		if (g_pVB == nullptr)
		{
			return;
		}

		frameResources->VB = g_pVB;
		frameResources->VertexBufferSize = g_VertexBufferSize;
	}
	if (!g_pIB || g_IndexBufferSize < drawData->TotalIdxCount)
	{
		if (g_pIB) { g_pIB->Release(); g_pIB = NULL; }
		g_IndexBufferSize = drawData->TotalIdxCount + 10000;
		RHIHeapProperties props		= {};
		props.Type					= RHIHeapType::Upload;
		props.CPUPageProperty		= RHICpuPageProperty::Unknown;
		props.MemoryPoolPreference	= RHIMemoryPool::Unknown;
		props.CreationNodeMask		= 1;
		props.VisibleNodeMask		= 1;
		RHIResourceDesc desc		= {};
		desc.Dimension				= RHIResourceDimension::Buffer;
		desc.Width					= g_IndexBufferSize * sizeof(ImDrawIdx);
		desc.Height					= 1;
		desc.DepthOrArraySize		= 1;
		desc.MipLevels				= 1;
		desc.Format					= RHIFormat::Unknown;
		desc.SampleDesc.Count		= 1;
		desc.Layout					= RHITextureLayout::RowMajor;
		desc.Flags					= RHIResourceFlag::None;

		g_pIB = mRenderer->Device()->CreateCommittedResource(&props, RHIHeapFlag::None, &desc, RHIResourceState::GenericRead, nullptr);
		if (g_pIB == nullptr)
		{
			return;
		}

		frameResources->IB = g_pIB;
		frameResources->IndexBufferSize = g_IndexBufferSize;
	}

	// Copy and convert all vertices into a single contiguous buffer
	void* vtx_resource, *idx_resource;
	RHIRange range(0, 0);
	g_pVB->Map(0, &range, &vtx_resource);
	//if (g_pVB->Map(0, &range, &vtx_resource) != S_OK)
	//	return;
	g_pIB->Map(0, &range, &idx_resource);
	//if (g_pIB->Map(0, &range, &idx_resource) != S_OK)
	//	return;
	ImDrawVert* vtx_dst = (ImDrawVert*)vtx_resource;
	ImDrawIdx* idx_dst = (ImDrawIdx*)idx_resource;
	for (int n = 0; n < drawData->CmdListsCount; n++)
	{
		const ImDrawList* cmd_list = drawData->CmdLists[n];
		memcpy(vtx_dst, cmd_list->VtxBuffer.Data, cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
		memcpy(idx_dst, cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
		vtx_dst += cmd_list->VtxBuffer.Size;
		idx_dst += cmd_list->IdxBuffer.Size;
	}
	g_pVB->Unmap(0, &range);
	g_pIB->Unmap(0, &range);

	// Setup orthographic projection matrix into our constant buffer
	VertexConstantBuffer vertex_constant_buffer;
	{
		VertexConstantBuffer* constant_buffer = &vertex_constant_buffer;
		float L = 0.0f;
		float R = ImGui::GetIO().DisplaySize.x;
		float B = ImGui::GetIO().DisplaySize.y;
		float T = 0.0f;
		float mvp[4][4] =
		{
			{ 2.0f / (R - L),   0.0f,           0.0f,       0.0f },
			{ 0.0f,         2.0f / (T - B),     0.0f,       0.0f },
			{ 0.0f,         0.0f,           0.5f,       0.0f },
			{ (R + L) / (L - R),  (T + B) / (B - T),    0.5f,       1.0f },
		};
		memcpy(&constant_buffer->mvp, mvp, sizeof(mvp));
	}

	// Setup viewport
	RHIViewport vp = {};
	vp.Width = ImGui::GetIO().DisplaySize.x;
	vp.Height = ImGui::GetIO().DisplaySize.y;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = vp.TopLeftY = 0.0f;
	cmdList->RSSetViewports(1, &vp);

	// Bind shader and vertex buffers
	unsigned int stride = sizeof(ImDrawVert);
	unsigned int offset = 0;
	RHIVertexBufferView vbv = {};
	vbv.BufferLocation = g_pVB->GetGPUVirtualAddress() + offset;
	vbv.SizeInBytes = g_VertexBufferSize * stride;
	vbv.StrideInBytes = stride;
	cmdList->IASetVertexBuffers(0, 1, &vbv);
	RHIIndexBufferView ibv = {};
	ibv.BufferLocation = g_pIB->GetGPUVirtualAddress();
	ibv.SizeInBytes = g_IndexBufferSize * sizeof(ImDrawIdx);
	ibv.Format = sizeof(ImDrawIdx) == 2 ? RHIFormat::R16Uint : RHIFormat::R32Uint;
	cmdList->IASetIndexBuffer(&ibv);
	cmdList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
	cmdList->SetPipelineState(mPSO);
	cmdList->SetGraphicsRootSignature(mRootSignature);
	cmdList->SetGraphicsRoot32BitConstants(0, 16, &vertex_constant_buffer, 0);
	
	RHIDescriptorHeap* heaps[] = { mRenderer->CbvSrvUavHeapManager().Heap() };
	cmdList->SetDescriptorHeaps(_countof(heaps), heaps);
	// Setup render state
	const float blend_factor[4] = { 0.f, 0.f, 0.f, 0.f };
	cmdList->OMSetBlendFactor(blend_factor);

	// Render command lists
	int vtx_offset = 0;
	int idx_offset = 0;
	for (int n = 0; n < drawData->CmdListsCount; n++)
	{
		const ImDrawList* cmd_list = drawData->CmdLists[n];
		for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
		{
			const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
			if (pcmd->UserCallback)
			{
				pcmd->UserCallback(cmd_list, pcmd);
			}
			else
			{
				RHIGPUDescriptorHandle handle((SIZE_T)pcmd->TextureId);
				const RHIRect r = { (LONG)pcmd->ClipRect.x, (LONG)pcmd->ClipRect.y, (LONG)pcmd->ClipRect.z, (LONG)pcmd->ClipRect.w };
				cmdList->SetGraphicsRootDescriptorTable(1, handle);
				cmdList->RSSetScissorRects(1, &r);
				cmdList->DrawIndexedInstanced(pcmd->ElemCount, 1, idx_offset, vtx_offset, 0);
			}
			idx_offset += pcmd->ElemCount;
		}
		vtx_offset += cmd_list->VtxBuffer.Size;
	}
}
