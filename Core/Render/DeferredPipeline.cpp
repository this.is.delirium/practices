#include "StdafxCore.h"
#include "DeferredPipeline.h"
#include "InputLayouts.h"
#include "IRenderer.h"
#include "GraphicsCore.h"
#include "ImGuiDirectX12.h"
#include <Render/Pass/GeometryPass.h>
#include "RenderSystem.h"
#include <RootSignature.h>
#include <Scene/Scene.h>
#include <Scene/SceneObject.h>
#include "Shader.h"
#include "StaticGeometryBuilder.h"
#include "World.h"

using namespace GraphicsCore;

namespace
{

enum class ScreenQuadRootSigParameter
{
	PerFrame = 0,
	SunLight,
	DepthTexture,
	NormalTexture,
	ColorTexture,
	UnusedTexture,
	Count
};

RHIShaderDefinition gScreenQuadRLShader = RHIShaderDefinition(
	"TestScreenQuadRL",
	{
		{ RHIShaderType::Vertex, __TEXT("DeferredShading:ScreenQuadRL.vs") },
		{ RHIShaderType::Pixel, __TEXT("DeferredShading:ScreenQuadRL.ps") }
	},
	RHIInputLayoutDesc({ InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc, _countof(InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc) })
);

}

/**
 * Function : Release
 */
void DeferredPipeline::Release()
{
	mGBuffer.Release();
	mFullScreenQuad.Release();
}

/**
 * Function : Initialize
 */
bool DeferredPipeline::Initialize(const RPDesc& rpDesc)
{
	InitializeMRT();

	InitializeFullScreenQuadPass();
	InitializeGeometryPass();

	switch (rpDesc.gAPI)
	{
		case GraphicsAPI::DirectX12:
		{
			mImGui = std::make_shared<ImGuiDirectX12>(rpDesc.window, &mRenderer); // ToDo: Reconsider later.
			break;
		}
	}

	assert(mImGui != nullptr);

	mImGui->Initialize(mRenderer.FrameCount());

	return true;
}

/**
 * Function : InitializeMRT
 */
void DeferredPipeline::InitializeMRT()
{
	RHISwapChainDesc scDesc = {};
	mRenderer.SwapChain()->GetDesc(&scDesc);

	// ToDo: We can use DepthStencil instead of an additional RT.
	// depth, normal, color, unused.
	RHIFormat formats[]				= { RHIFormat::R32Float, RHIFormat::R16G16Float, RHIFormat::R8G8B8A8UNorm, RHIFormat::R8G8B8A8UNorm };
	//RHIFormat formats[]			= { RHIFormat::R32G32B32A32Float, RHIFormat::R32G32B32A32Float, RHIFormat::R32G32B32A32Float, RHIFormat::R32G32B32A32Float };

	MRTPassDesc mrtDesc				= {};
	mrtDesc.NumRT					= _countof(formats);
	mrtDesc.InputLayout				= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
	mrtDesc.BlendDesc				= RHIBlendDesc(RHIDefault());

	// Describe Render Targets.
	for (UINT i = 0; i < mrtDesc.NumRT; ++i)
	{
		RHIResourceDesc& resDesc	= mrtDesc.Descs[i];
		resDesc.Dimension			= RHIResourceDimension::Texture2D;
		resDesc.Width				= scDesc.BufferDesc.Width;
		resDesc.Height				= scDesc.BufferDesc.Height;
		resDesc.DepthOrArraySize	= 1;
		resDesc.MipLevels			= 1;
		resDesc.SampleDesc.Count	= 1;
		resDesc.Layout				= RHITextureLayout::Unknown;
		resDesc.Flags				= RHIResourceFlag::AllowRenderTarget;
		resDesc.Format				= formats[i];

		{
			RHIClearValue& clearValue	= mrtDesc.ClearValues[i];
			clearValue.Format			= formats[i];
			clearValue.Color[0]			= 0.0f;
			clearValue.Color[1]			= 0.0f;
			clearValue.Color[2]			= 0.0f;
			clearValue.Color[3]			= 0.0f;

			if (i == 0u)
			{
				clearValue.Color[0] = 1.0f;
			}
		}
	}

	// Describe DepthStencil.
	{
		mrtDesc.DepthStencil.Format		= RHIFormat::R24G8Typeless;
		mrtDesc.DepthStencil.ViewFormat	= RHIUtils::Convert::ResourceFormatToDsvFormat(mrtDesc.DepthStencil.Format);

		RHIResourceDesc& dsDesc			= mrtDesc.DepthStencil.Desc;
		dsDesc.Dimension				= RHIResourceDimension::Texture2D;
		dsDesc.Width					= scDesc.BufferDesc.Width;
		dsDesc.Height					= scDesc.BufferDesc.Height;
		dsDesc.DepthOrArraySize			= 1;
		dsDesc.MipLevels				= 1;
		dsDesc.SampleDesc.Count			= 1;
		dsDesc.Layout					= RHITextureLayout::Unknown;
		dsDesc.Flags					= RHIResourceFlag::AllowDepthStencil;
		dsDesc.Format					= mrtDesc.DepthStencil.Format;

		RHIClearValue& clearValue		= mrtDesc.DepthStencil.ClearValue;
		clearValue.Format				= mrtDesc.DepthStencil.ViewFormat;
		clearValue.DepthStencil.Depth	= 1.0f;
		clearValue.DepthStencil.Stencil	= 0;
	}


	mGBuffer.Initialize(mrtDesc);
}

/**
 * Function : InitializeFullScreenQuadPass
 */
void DeferredPipeline::InitializeFullScreenQuadPass()
{
	// RootSignature.
	{
		RHI::BindedResourceViews rvsPerFrame;
		mFullScreenQuad.shader = GraphicsCore::rs().shaderManager().CreateShader(gScreenQuadRLShader, rvsPerFrame);

		RHIUtils::RootSignature rootSignature(ToUnderType(ScreenQuadRootSigParameter::Count), 1u);
		rootSignature[ToUnderType(ScreenQuadRootSigParameter::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToUnderType(ScreenQuadRootSigParameter::SunLight)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
		rootSignature[ToUnderType(ScreenQuadRootSigParameter::DepthTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);
		rootSignature[ToUnderType(ScreenQuadRootSigParameter::NormalTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 1);
		rootSignature[ToUnderType(ScreenQuadRootSigParameter::ColorTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 2);
		rootSignature[ToUnderType(ScreenQuadRootSigParameter::UnusedTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 3);

		RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(0);
		samplerDesc							= RHIStaticSamplerDesc(RHIDefault());
		samplerDesc.Filter					= RHIFilter::MinMagMipLinear;

		mFullScreenQuad.shader->rootSignature() = rootSignature.Finalize(GetRootSignatureFlags(true, false, false, false, false, true, false));
	}

	// PSO.
	{
		RHIGraphicsPipelineStateDesc psoDesc				= {};
		psoDesc.PrimitiveTopologyType						= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask									= UINT_MAX;
		psoDesc.NumRenderTargets							= 1;
		psoDesc.RTVFormats[0]								= gRenderer->RenderTargetFormat();
		psoDesc.SampleDesc.Count							= 1;

		psoDesc.DepthStencilState.DepthEnable				= false;

		psoDesc.RasterizerState								= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode					= RHICullMode::Front;
		psoDesc.RasterizerState.FrontCounterClockwise		= FALSE;

		psoDesc.BlendState									= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable		= TRUE;
		psoDesc.BlendState.RenderTarget[0].SrcBlend			= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend		= RHIBlend::InvSrcAlpha;
		psoDesc.BlendState.RenderTarget[0].SrcBlendAlpha	= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlendAlpha	= RHIBlend::InvSrcAlpha;

		// Fill a pso from a shader.
		RHIUtils::Fill::GraphicsPipelineStateDesc(*mFullScreenQuad.shader, psoDesc);

		mFullScreenQuad.shader->pso() = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// Quad.
	{
		StaticGeometryBuilder::BuildFullScreenQuad(&mFullScreenQuad.quad, __TEXT("DeferredShading:SunPass:VertexBuffer"));
	}
}

/**
 * Function : InitializeGeometryPass
 */
void DeferredPipeline::InitializeGeometryPass()
{
	//-- RootSignature
	{
		// Load shaders.
		{
			RHI::BindedResourceViews rvsPerFrame;
			//rvsPerFrame.emplace_back(ToUnderType(GeometryPassRootSignatureParams::PerFrame), cbPerFrame.SrvHandle().Gpu());

			mGPassShader = GraphicsCore::rs().shaderManager().CreateShader(Pass::GeometryPass::shader, rvsPerFrame);
		}

		RHIUtils::RootSignature rootSignature(ToUnderType(Pass::GeometryPass::RootSignatureParams::Count), 1);
		rootSignature[ToUnderType(Pass::GeometryPass::RootSignatureParams::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToUnderType(Pass::GeometryPass::RootSignatureParams::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
		rootSignature[ToUnderType(Pass::GeometryPass::RootSignatureParams::AlbedoTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);
		rootSignature[ToUnderType(Pass::GeometryPass::RootSignatureParams::NormalTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 1);
		rootSignature[ToUnderType(Pass::GeometryPass::RootSignatureParams::SpecularTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 2);

		RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(0);
		samplerDesc							= RHIStaticSamplerDesc(RHIDefault());
		samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc.ShaderVisibility		= RHIShaderVisibility::Pixel;
		samplerDesc.ShaderRegister			= 0;
		samplerDesc.RegisterSpace			= 0;

		mGPassShader->rootSignature() = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}

	//-- PSO.
	{
		RHIGraphicsPipelineStateDesc psoDesc				= {};
		psoDesc.PrimitiveTopologyType						= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask									= UINT_MAX;
		psoDesc.SampleDesc.Count							= 1;

		psoDesc.DepthStencilState.DepthEnable				= TRUE;
		psoDesc.DepthStencilState.DepthFunc					= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask			= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable				= FALSE;

		psoDesc.RasterizerState								= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode					= RHICullMode::Back;
		psoDesc.RasterizerState.FillMode					= RHIFillMode::Solid;
		psoDesc.RasterizerState.FrontCounterClockwise		= true;

		psoDesc.BlendState									= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable		= TRUE;
		psoDesc.BlendState.RenderTarget[0].SrcBlend			= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend		= RHIBlend::InvSrcAlpha;
		psoDesc.BlendState.RenderTarget[0].SrcBlendAlpha	= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlendAlpha	= RHIBlend::InvSrcAlpha;

		// Fill a psoDesc from a MRTPass.
		// This overload sets PSO and RootSignature, that's why it comes first.
		// ToDo: Think about it.
		RHIUtils::Fill::GraphicsPipelineStateDesc(GraphicsCore::rp()->GBuffer(), &psoDesc);

		// Fill a psoDesc from a shader.
		RHIUtils::Fill::GraphicsPipelineStateDesc(*mGPassShader, psoDesc);

		mGPassShader->pso() = gDevice->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : BeginFrame
 */
void DeferredPipeline::BeginFrame(RenderCommandContext& rcc) const
{
	RHICommandList* cmdList = rcc.CommandList();

	const float testColor[] = { 0.2f, 0.8f, 0.4f, 1.0f };

	{
		cmdList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));
		cmdList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthWrite));

		auto rtvCpuHandle = gRenderer->CurrentRenderTargetCpuHandle();
		auto dsvCpuHandle = gRenderer->CurrentDepthStencilCpuHandle();

		cmdList->OMSetRenderTargets(1, &rtvCpuHandle, false, &dsvCpuHandle);

		cmdList->ClearRenderTargetView(rtvCpuHandle, testColor, 0, nullptr);
		cmdList->ClearDepthStencilView(dsvCpuHandle, RHIClearFlags::Depth | RHIClearFlags::Stencil, 1.0f, 0, 0, nullptr);

		// ToDo: reconsider later.
		cmdList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
		cmdList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present));
	}
}

/**
 * Function : CastShadows
 */
void DeferredPipeline::CastShadows(RenderCommandContext& rcc) const
{

}

/**
 * Function : DrawOpaque
 */
void DeferredPipeline::DrawOpaque(RenderCommandContext& rcc, const IntersectionSet::ModeSet& objects, const World& world) const
{
	RHICommandList* cmdList = rcc.CommandList();

	// ToDo: Think about it.
	{
		mGBuffer.BindRenderTargets(cmdList, true);

		cmdList->RSSetViewports(1, &rcc.Viewport());
		cmdList->RSSetScissorRects(1, &rcc.ScissorRect());

		// ToDo: Think about it.
		RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };
		cmdList->SetDescriptorHeaps(_countof(heaps), heaps);
	}

	const auto& cbPerFrame = world.GetScene().GetConstBufferPerFrame();
	// Render objects.
	{
		Shader* currentShader = nullptr;

		for (const auto& obj : objects)
		{
			Shader* objShader = obj->GetMaterial()->GetShader().get();

			// Set PSO, RootSignature, etc.
			if (currentShader != objShader)
			{
				currentShader = objShader;

				cmdList->SetPipelineState(currentShader->pso());
				cmdList->SetGraphicsRootSignature(currentShader->rootSignature());

				// Set ResourceViews for per frame.
				for (auto& srv : currentShader->bindedResourceViewsPerFrame())
				{
					// ToDo: Handle SetGraphicsRootConstantBufferView.
					//cmdList->SetGraphicsRootDescriptorTable(srv.position, cbPerFrame.SrvHandle().Gpu());
					cmdList->SetGraphicsRootDescriptorTable(srv.position, srv.gpuHandle);
				}
				cmdList->SetGraphicsRootDescriptorTable(0u, cbPerFrame.SrvHandle().Gpu());
			}

			const MaterialRef& material				= obj->GetMaterial();
			const RHIVertexBuffer& vertexBuffer		= obj->GetVertexBuffer();
			const RHIIndexBuffer& indexBuffer		= obj->GetIndexBuffer();
			const ConstBufferPerObject constBuffer	= obj->GetConstBuffer();

			// Set SrvHandles for per object.
			for (auto& srv : material->GetPerObjectSrvHandles())
			{
				cmdList->SetGraphicsRootDescriptorTable(srv.position, srv.gpuHandle);
			}

			cmdList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
			cmdList->IASetVertexBuffers(0, 1, vertexBuffer.View());
			if (indexBuffer.Buffer())
			{
				cmdList->IASetIndexBuffer(indexBuffer.View());
				cmdList->DrawIndexedInstanced(indexBuffer.NumElements(), 1, 0, 0, 0);
			}
			else
			{
				cmdList->DrawInstanced(vertexBuffer.NumElements(), 1, 0, 0);
			}
		}
	}
}

/**
 * Function : ApplyLighting
 */
void DeferredPipeline::ApplyLighting(RenderCommandContext& rcc, const World& world) const
{
	RHICommandList* cmdList = rcc.CommandList();

	//! Setup resources to rendering.
	{
		RHIResourceBarrier barriers[] =
		{
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget),
			//RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthWrite)
		};

		cmdList->ResourceBarrier(_countof(barriers), barriers);

		mGBuffer.BindShaderResources(cmdList);
	}

	cmdList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);

	cmdList->RSSetViewports(1, &rcc.Viewport());
	cmdList->RSSetScissorRects(1, &rcc.ScissorRect());

	// ToDo: Think about it.
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };
	cmdList->SetDescriptorHeaps(_countof(heaps), heaps);

	const auto& scene = world.GetScene();
	const auto& cbPerFrame = scene.GetConstBufferPerFrame();
	const auto& cbSunLight = scene.GetConstBufferSunLight();

	//-- Sun light.
	cmdList->SetPipelineState(mFullScreenQuad.shader->pso());
	cmdList->SetGraphicsRootSignature(mFullScreenQuad.shader->rootSignature());
	cmdList->SetGraphicsRootDescriptorTable(ToUnderType(ScreenQuadRootSigParameter::PerFrame), cbPerFrame.SrvHandle().Gpu());
	cmdList->SetGraphicsRootDescriptorTable(ToUnderType(ScreenQuadRootSigParameter::SunLight), cbSunLight.SrvHandle().Gpu());
	cmdList->SetGraphicsRootDescriptorTable(ToUnderType(ScreenQuadRootSigParameter::DepthTexture), mGBuffer.RenderTarget(0).SrvHandle().Gpu());
	//cmdList->SetGraphicsRootDescriptorTable(ToUnderType(ScreenQuadRootSigParameter::DepthTexture), mGBuffer.DepthStencil().SrvHandle().Gpu());
	cmdList->SetGraphicsRootDescriptorTable(ToUnderType(ScreenQuadRootSigParameter::NormalTexture), mGBuffer.RenderTarget(1).SrvHandle().Gpu());
	cmdList->SetGraphicsRootDescriptorTable(ToUnderType(ScreenQuadRootSigParameter::ColorTexture), mGBuffer.RenderTarget(2).SrvHandle().Gpu());
	cmdList->SetGraphicsRootDescriptorTable(ToUnderType(ScreenQuadRootSigParameter::UnusedTexture), mGBuffer.RenderTarget(3).SrvHandle().Gpu());

	cmdList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
	cmdList->IASetVertexBuffers(0, 1, mFullScreenQuad.quad.View());
	cmdList->DrawInstanced(6, 1, 0, 0);

	auto& lights = scene.GetLights();
	for (auto& light : lights)
	{
		// ToDo: Render them!
	}

	//! Return resources in the previous state.
	{
		RHIResourceBarrier barriers[] =
		{
			// ToDo: Think about it.
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present),

			//RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present)
		};

		cmdList->ResourceBarrier(_countof(barriers), barriers);
	}
}

/**
 * Function : ReceiveShadows
 */
void DeferredPipeline::ReceiveShadows(RenderCommandContext& rcc) const
{

}

/**
 * Function : DrawGUI
 */
void DeferredPipeline::DrawGUI(RenderCommandContext& rcc) const
{
	RHICommandList* cmdList = rcc.CommandList();

	{
		cmdList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));
		cmdList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthWrite));

		auto rtvCpuHandle = gRenderer->CurrentRenderTargetCpuHandle();
		auto dsvCpuHandle = gRenderer->CurrentDepthStencilCpuHandle();

		cmdList->OMSetRenderTargets(1, &rtvCpuHandle, false, &dsvCpuHandle);

		mImGui->Render(cmdList);

		// ToDo: reconsider later.
		cmdList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
		cmdList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present));
	}
}

/**
 * Function : EndFrame
 */
void DeferredPipeline::EndFrame(RenderCommandContext& rcc) const
{
	RHICommandList* cmdList = rcc.CommandList();

	// ToDo: Reconsider later.
	//cmdList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
	//cmdList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present));
}
