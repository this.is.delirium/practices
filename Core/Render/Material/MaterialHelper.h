#pragma once
#include <StdafxCore.h>
#include "Material.h"

namespace Materials
{

class Helper
{
public:
	Helper();

	static MaterialRef CreateMaterial(const std::string& name);

protected:
	using Map = std::unordered_map<std::string, MaterialRef>;

	Map mMapOfMaterials;
};

} // end of namespace Materials.
