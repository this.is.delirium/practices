#include <StdafxCore.h>
#include "MaterialHelper.h"

#include <Render/Material/Predefined/DefaultMaterial.h>
#include <StringUtils.h>

namespace Materials
{

//-----------------------------------------------------------------------------
Helper::Helper()
{
	mMapOfMaterials["default"] = std::make_unique<Predefined::DefaultMaterial>();
}

//-----------------------------------------------------------------------------
MaterialRef Helper::CreateMaterial(const std::string& name)
{
	std::string lowerCaseName = StringUtils::Converter::ToLower(name);

	if (lowerCaseName == "default")
	{
		return std::make_unique<Predefined::DefaultMaterial>();
	}

	assert(false && "Doesn't exist such material.");
	return nullptr;
	//return mMapOfMaterials[StringUtils::Converter::ToLower(name)];
}

} // end of namespace Materials.
