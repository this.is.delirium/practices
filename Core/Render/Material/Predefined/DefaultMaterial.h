#pragma once
#include "StdafxCore.h"
#include <Render/Material/Material.h>
#include <RHITexture.h>

namespace Materials
{

namespace Predefined
{

class DefaultMaterial : public Material
{
public:
	TextureRef& Albedo() { return mAlbedoTexture; }
	TextureRef& Normal() { return mNormalTexture; }
	TextureRef& Specular() { return mSpecularTexture; }
	TextureRef& Gloss() { return mGlossTexture; }

protected:
	TextureRef	mAlbedoTexture;
	TextureRef	mNormalTexture;
	TextureRef	mSpecularTexture;
	TextureRef	mGlossTexture;
};

} // end of namespace Predefined.

} // end of namespace Materials.
