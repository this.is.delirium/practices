#pragma once
#include "StdafxCore.h"
#include <RHI.h>
#include <RHIBindedResourceView.h>
#include <RHIShaderDefinition.h>
#include <Render/ShaderManager.h>

// ToDo: Implement Copy-On-Write pattern.
class Material
{
public:
	virtual ~Material() { }

public:
	void SetShader(const ShaderManager::ShaderPtr& shader) { mShader = shader; }
	const ShaderManager::ShaderPtr& GetShader() const { return mShader; }

public:
	void SetSrvPerObject(const UINT position, const RHIGPUDescriptorHandle& gpuHandle)
	{
		//-- ToDo: Think about it.
		//-- I should create a dummy resource in my heap to allow set a dummy pointer.
		if (gpuHandle.ptr != 0)
		{
			mPerObjectBindedSRVs.emplace_back(position, gpuHandle);
		}
	}

	const RHI::BindedResourceViews& GetPerObjectSrvHandles() const { return mPerObjectBindedSRVs; }

protected:
	ShaderManager::ShaderPtr	mShader;
	RHI::BindedResourceViews	mPerObjectBindedSRVs;
};

using MaterialRef = std::shared_ptr<Material>;
