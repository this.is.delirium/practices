#include "StdafxCore.h"
#include "FrameRenderGraph.h"
#include "GraphicsCore.h"
#include "IRenderPipeline.h"

#include "Mode/OpaqueMode.h"
#include "Scene/IntersectionSet.h"
#include "World.h"

using namespace GraphicsCore;

/**
 * Function : FrameRenderGraph
 */
FrameRenderGraph::FrameRenderGraph()
{

}

/**
 * Function : Evaluate
 */
void FrameRenderGraph::Evaluate(const World& world) const
{
	RenderCommandContextManager& rccManager = rs().RCCManager();

	auto endFrameRCC	= rccManager.getScopedRCC(__TEXT("End frame RCC"), RenderCommandContextType::Task3);
	auto guiRCC			= rccManager.getScopedRCC(__TEXT("GUI RCC"), RenderCommandContextType::Task2);
	auto lightingRCC	= rccManager.getScopedRCC(__TEXT("Lighting RCC"), RenderCommandContextType::Task6);
	auto opaqueRCC		= rccManager.getScopedRCC(__TEXT("Opaque RCC"), RenderCommandContextType::Task4);
	auto shadowRCC		= rccManager.getScopedRCC(__TEXT("Shadow RCC"), RenderCommandContextType::Task5);
	auto cullingRCC		= rccManager.getScopedRCC(__TEXT("Culling RCC"), RenderCommandContextType::Task1);
	auto beginFrameRCC	= rccManager.getScopedRCC(__TEXT("Begin frame RCC"), RenderCommandContextType::Task0);

	auto& scene = world.GetScene();
	IntersectionSet visibleObjects;

	auto& beginFrame = [&]()
	{
		auto& rcc = beginFrameRCC.getRCC();

		rp()->BeginFrame(rcc);
	};

	auto& culling = [&]()
	{
		auto& rcc = cullingRCC.getRCC();

		rs().Culling(rcc, world, visibleObjects);
	};

	auto& shadows = [&]()
	{
		auto& rcc = shadowRCC.getRCC();

		rp()->CastShadows(rcc);
	};

	auto& drawOpaque = [&]()
	{
		auto& rcc = opaqueRCC.getRCC();

		//auto& opaqueMode = scene.Get<OpaqueMode>();
		auto& opaqueObjects = visibleObjects.Get<OpaqueMode>();

		rp()->DrawOpaque(rcc, opaqueObjects, world);
	};

	auto& lighting = [&]()
	{
		auto& rcc = lightingRCC.getRCC();

		// ToDo: Do merge into one method?
		rp()->ApplyLighting(rcc, world);
		rp()->ReceiveShadows(rcc);
	};

	auto& gui = [&]()
	{
		auto& rcc = guiRCC.getRCC();

		rp()->DrawGUI(rcc);
	};

	auto& endFrame = [&]()
	{
		auto& rcc = endFrameRCC.getRCC();

		rp()->EndFrame(rcc);
	};

	// ToDo: Add a tbb flow graph.
	beginFrame();
	culling();
	drawOpaque();
	lighting();
	gui();
	endFrame();
}
