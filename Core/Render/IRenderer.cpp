#include "StdafxCore.h"
#include "IRenderer.h"

#include "GraphicsCore.h"
#include "InputLayouts.h"
#include "RenderSurface.h"
#include <ResourceManager.h>
#include <RHI.h>

/**
 * Function : Release
 */
void IRenderer::Release()
{
	for (UINT i = 0; i < kFrameCount; ++i)
	{
		RHISafeRelease(mRenderTargets[i]);
		RHISafeRelease(mDepthStencils[i]);
	}

	mRtvHeapManager.FreeDescriptors(mRtvDescriptors);
	mCbvSrvUavHeapManager.FreeDescriptors(mDsvTexturesDescriptors);
	mDsvHeapManager.FreeDescriptors(mDsvDescriptors);

	mRtvHeapManager.Release();
	mCbvSrvUavHeapManager.Release();
	mNonShaderCbvSrcUavHeapManager.Release();
	mDsvHeapManager.Release();

	mScreenQuadPart.Release();
	mViewFrustumPart.Release();

	RHISafeRelease(mFence);

	RHISafeRelease(mCommandAllocator);
	RHISafeRelease(mCommandList);

	RHISafeRelease(mResourceBarrierList);
	RHISafeRelease(mResourceBarrierAllocator);

	RHISafeRelease(mUploadCommandAllocator);
	RHISafeRelease(mUploadCommandList);
	RHISafeRelease(mUploadBuffer);

	RHISafeRelease(mCommandQueue);
	RHISafeRelease(mSwapChain);
	RHISafeRelease(mDevice);
}

/**
 * Function : Initialize
 */
bool IRenderer::Initialize(const RenderSurface* renderSurface)
{
	static_assert(kRtvDescriptors >= kFrameCount, "Wrong number of RTV descriptors.");
	static_assert(kDsvDescriptors >= kFrameCount, "Wrong number of DSV descriptors.");

	GraphicsCore::gRenderer	= this;
	GraphicsCore::gDevice	= mDevice;

	LoadPipeline(renderSurface);

	mScreenQuadPart.Initialize(this);
	mViewFrustumPart.Initialize(this);

	CloseAndWait(mCommandList);
	mCommandList->Reset(mCommandAllocator, nullptr);

	return true;
}

/**
 * Function : LoadPipeline
 */
void IRenderer::LoadPipeline(const RenderSurface* renderSurface)
{
	mCommandAllocator	= mDevice->CreateCommandAllocator(RHICommandListType::Direct);
	mCommandList		= mDevice->CreateCommandList(0, RHICommandListType::Direct, mCommandAllocator, nullptr);

	{
		mResourceBarrierAllocator	= mDevice->CreateCommandAllocator(RHICommandListType::Direct);
		mResourceBarrierList		= mDevice->CreateCommandList(0, RHICommandListType::Direct, mResourceBarrierAllocator, nullptr);
		mResourceBarrierList->Close();
	}

	// Initialize an upload part.
	{
		mUploadCommandAllocator	= mDevice->CreateCommandAllocator(RHICommandListType::Direct);
		mUploadCommandList		= mDevice->CreateCommandList(0, RHICommandListType::Direct, mUploadCommandAllocator, nullptr);
		mUploadCommandList->Close();

		mUploadBuffer			= mDevice->CreateCommittedResource(
			&RHIHeapProperties(RHIHeapType::Upload),
			RHIHeapFlag::None,
			&RHIResourceDesc::Buffer(kUploadBufferWidth),
			RHIResourceState::GenericRead,
			nullptr);
	}

	// Create a command queue.
	{
		RHICommandQueueDesc desc = {};
		desc.Flags	= RHICommandQueueFlags::None;
		desc.Type	= RHICommandListType::Direct;

		mCommandQueue = mDevice->CreateCommandQueue(&desc);
	}

	RHISwapChainDesc1			swapChainDesc1		= {};
	RHISwapChainFullscreenDesc	scFullscreenDesc	= {};
	// Create a swap chain.
	{
		mRenderTargetFormat							= RHIFormat::R8G8B8A8UNorm;

		swapChainDesc1.AlphaMode					= RHIAlphaMode::Unspecified;
		swapChainDesc1.BufferCount					= kFrameCount;
		swapChainDesc1.BufferUsage					= RHIUsage::RenderTargetOutput;
		swapChainDesc1.Format						= mRenderTargetFormat;
		swapChainDesc1.Width						= renderSurface->Width();
		swapChainDesc1.Height						= renderSurface->Height();
		swapChainDesc1.SampleDesc.Count				= 1;
		swapChainDesc1.SampleDesc.Quality			= 0;
		swapChainDesc1.Scaling						= RHIScaling::Stretch;
		swapChainDesc1.Stereo						= false;
		swapChainDesc1.SwapEffect					= RHISwapEffect::FlipDiscard;
		swapChainDesc1.Flags						= ToInt(RHISwapChainFlag::FrameLatencyWaitableObject);

		scFullscreenDesc.RefreshRate.Numerator		= 60;
		scFullscreenDesc.RefreshRate.Denominator	= 1;
		scFullscreenDesc.ScanlineOrdering			= RHIModeScanlineOrder::Progressive;
		scFullscreenDesc.Scaling					= RHIModeScaling::Centered;
		scFullscreenDesc.Windowed					= renderSurface->IsWindowed();

		mSwapChain									= mDevice->CreateSwapChain(&swapChainDesc1, &scFullscreenDesc, mCommandQueue);


		mFrameIndex									= mSwapChain->GetCurrentBackBufferIndex();
		mPreviouseFrameIndex						= mFrameIndex;

		mSwapChain->SetMaximumFrameLatency(kFrameCount);
	}

	// Create descriptor heaps.
	{
		RHIDescriptorHeapDesc rtvHeapDesc	= {};
		rtvHeapDesc.NumDescriptors			= kRtvDescriptors;
		rtvHeapDesc.Type					= RHIDescriptorHeapType::RTV;
		rtvHeapDesc.Flags					= RHIDescriptorHeapFlag::None;

		mRtvHeapManager.Initialize(&rtvHeapDesc, __TEXT("Render target heap"));

		RHIDescriptorHeapDesc dsvHeapDesc	= {};
		dsvHeapDesc.NumDescriptors			= kDsvDescriptors;
		dsvHeapDesc.Type					= RHIDescriptorHeapType::DSV;
		dsvHeapDesc.Flags					= RHIDescriptorHeapFlag::None;

		mDsvHeapManager.Initialize(&dsvHeapDesc, __TEXT("Depth stencil heap"));

		RHIDescriptorHeapDesc cbvHeapDesc	= {};
		cbvHeapDesc.NumDescriptors			= kCbvDescriptors;
		cbvHeapDesc.Type					= RHIDescriptorHeapType::CbvSrvUav;
		cbvHeapDesc.Flags					= RHIDescriptorHeapFlag::ShaderVisible;

		mCbvSrvUavHeapManager.Initialize(&cbvHeapDesc, __TEXT("CBV/SRV/UAV heap"));

		cbvHeapDesc.NumDescriptors	= kNonShaderCbvDescriptors;
		cbvHeapDesc.Flags			= RHIDescriptorHeapFlag::None;

		mNonShaderCbvSrcUavHeapManager.Initialize(&cbvHeapDesc, __TEXT("Non shader CBV/SRV/UAV heap"));

		SetDefaultCbvSrvUavHeapManager();
	}

	// Create frame resources.
	{
		mRtvDescriptors = mRtvHeapManager.GetNewDescriptors(kFrameCount);
		// Create a RTV for each frame.
		for (UINT i = 0; i < kFrameCount; ++i)
		{
			mSwapChain->GetBuffer(i, mRenderTargets[i]);
			mDevice->CreateRenderTargetView(mRenderTargets[i], nullptr, mRtvHeapManager.CpuHandle(mRtvDescriptors[i]));
		}

		// Create DSV for each frame.
		{
			mDepthStencilFormat		= RHIFormat::R24G8Typeless;
			mDepthStencilViewFormat = RHIFormat::D24UnormS8Uint;
			RHIResourceDesc resourceDesc(
				RHIResourceDimension::Texture2D, 0,
				swapChainDesc1.Width, swapChainDesc1.Height, 1, 1,
				mDepthStencilFormat, 1, 0,
				RHITextureLayout::Unknown, RHIResourceFlag::AllowDepthStencil);

			RHIHeapProperties heapProps		= {};
			heapProps.Type					= RHIHeapType::Default;
			heapProps.CPUPageProperty		= RHICpuPageProperty::Unknown;
			heapProps.MemoryPoolPreference	= RHIMemoryPool::Unknown;
			heapProps.CreationNodeMask		= 1;
			heapProps.VisibleNodeMask		= 1;

			RHIClearValue clearValue		= {};
			clearValue.Format				= mDepthStencilViewFormat;
			clearValue.DepthStencil.Depth	= 1.0f;
			clearValue.DepthStencil.Stencil	= 0;

			RHIDepthStencilViewDesc dsvDesc	= {};
			dsvDesc.Format					= mDepthStencilViewFormat;
			dsvDesc.ViewDimension			= RHIDSVDimension::Texture2D;

			mDsvDescriptors = mDsvHeapManager.GetNewDescriptors(kFrameCount);
			for (UINT i = 0; i < kFrameCount; ++i)
			{
				mDepthStencils[i] = mDevice->CreateCommittedResource(&heapProps, RHIHeapFlag::None, &resourceDesc, RHIResourceState::Common, &clearValue);
				mDevice->CreateDepthStencilView(mDepthStencils[i], &dsvDesc, mDsvHeapManager.CpuHandle(mDsvDescriptors[i]));
			}
		}

		// Create textures for depth stencils.
		{
			// TODO: Replace D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING on own macro.
			RHIShaderResourceViewDesc srvDesc	= {};
			srvDesc.Shader4ComponentMapping		= D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
			srvDesc.Format						= RHIFormat::R24UnormX8Typeless;
			srvDesc.ViewDimension				= RHISRVDimension::Texture2D;
			srvDesc.Texture2D.MipLevels			= 1;

			mDsvTexturesDescriptors = mCbvSrvUavHeapManager.GetNewDescriptors(kFrameCount);
			for (UINT i = 0; i < kFrameCount; ++i)
			{
				mGpuHandleDSTexures[i] = mCbvSrvUavHeapManager.GpuHandle(mDsvTexturesDescriptors[i]);
				mDevice->CreateShaderResourceView(DepthStencil(i), &srvDesc, mCbvSrvUavHeapManager.CpuHandle(mDsvTexturesDescriptors[i]));
			}
		}
	}

	// Create synchronization objects and wait until assets have been uploaded to the GPU.
	{
		mFence			= mDevice->CreateFence(0, RHIFenceFlags::None);
		mFenceValue		= 1;
		// Create an event handle to use for frame synchronization.
		mFenceEvent		= CreateEvent(nullptr, FALSE, FALSE, nullptr);
		if (mFenceEvent == nullptr)
		{
			ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()), __TEXT("Can't create an event to use for frame synchronization."));
		}

		WaitCommandList();
	}
}

/**
 * Function : ScreenQuadPart::Initialize
 */
void IRenderer::ScreenQuadPart::Initialize(IRenderer* renderer)
{
	// Create a root signature;
	{
		RHIDescriptorRange ranges[3];
		ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);
		ranges[1].Init(RHIDescriptorRangeType::CBV, 1, 1);
		ranges[2].Init(RHIDescriptorRangeType::SRV, 1, 0);

		RHIRootParameter rootParameters[3];
		rootParameters[0].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::All);
		rootParameters[1].InitAsDescriptorTable(1, &ranges[1], RHIShaderVisibility::All);
		rootParameters[2].InitAsDescriptorTable(1, &ranges[2], RHIShaderVisibility::Pixel);

		RHIStaticSamplerDesc samplerDesc	= {};
		samplerDesc.Filter					= RHIFilter::MinMagMipPoint;
		samplerDesc.AddressU				= RHITextureAddressMode::Border;
		samplerDesc.AddressV				= RHITextureAddressMode::Border;
		samplerDesc.AddressW				= RHITextureAddressMode::Border;
		samplerDesc.MipLODBias				= 0;
		samplerDesc.MaxAnisotropy			= 0;
		samplerDesc.ComparisonFunc			= RHIComparisonFunc::Never;
		samplerDesc.BorderColor				= RHIStaticBorderColor::TransparentBlack;
		samplerDesc.MinLOD					= 0.0f;
		samplerDesc.MaxLOD					= RHIConstants::kFloat32Max;
		samplerDesc.ShaderRegister			= 0;
		samplerDesc.RegisterSpace			= 0;
		samplerDesc.ShaderVisibility		= RHIShaderVisibility::Pixel;

		RHIRootSignatureFlags flags = RHIRootSignatureFlags::AllowInputAssemblerInputLayout;
		RHIRootSignatureDesc rootSignatureDesc = {};
		rootSignatureDesc.Init(_countof(rootParameters), &rootParameters[0], 1, &samplerDesc, flags);

		RootSignature = renderer->SerializeAndCreateRootSignature(&rootSignatureDesc, RHIRootSignatureVersion::Ver1);
	}

	// Create a pipeline state.
	{
		RHIBlobWrapper vertexShader		(renderer->LoadShader(__TEXT("ScreenQuad.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper fragmentShader	(renderer->LoadShader(__TEXT("ScreenQuad.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
		{
			return;
		}

		RHIGraphicsPipelineStateDesc psoDesc			= {};
		psoDesc.RootSignature							= RootSignature;
		psoDesc.InputLayout								= { InputLayouts::ScreenQuad::inputElementDesc, _countof(InputLayouts::ScreenQuad::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= renderer->RenderTargetFormat();
		psoDesc.DSVFormat								= renderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= FALSE;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;
		psoDesc.RasterizerState.FrontCounterClockwise	= FALSE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp		= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha	= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend		= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend	= RHIBlend::InvSrcAlpha;

		PSO = renderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : ViewFrustumPart::Initialze
 */
void IRenderer::ViewFrustumPart::Initialize(IRenderer* renderer)
{
	Renderer = renderer;

	// Create a depth stencil.
	{
		RHISwapChainDesc swapChainDesc;
		renderer->SwapChain()->GetDesc(&swapChainDesc);

		DepthStencilFormat		= RHIFormat::R24G8Typeless;
		DepthStencilViewFormat	= RHIFormat::D24UnormS8Uint;
		RHIResourceDesc resourceDesc(
			RHIResourceDimension::Texture2D, 0,
			swapChainDesc.BufferDesc.Width, swapChainDesc.BufferDesc.Height, 1, 1,
			DepthStencilFormat, 1, 0,
			RHITextureLayout::Unknown, RHIResourceFlag::AllowDepthStencil);

		RHIHeapProperties heapProps		= {};
		heapProps.Type					= RHIHeapType::Default;
		heapProps.CPUPageProperty		= RHICpuPageProperty::Unknown;
		heapProps.MemoryPoolPreference	= RHIMemoryPool::Unknown;
		heapProps.CreationNodeMask		= 1;
		heapProps.VisibleNodeMask		= 1;

		RHIClearValue clearValue		= {};
		clearValue.Format				= DepthStencilViewFormat;
		clearValue.DepthStencil.Depth	= 1.0f;

		RHIDepthStencilViewDesc dsvDesc	= {};
		dsvDesc.Format					= DepthStencilViewFormat;
		dsvDesc.ViewDimension			= RHIDSVDimension::Texture2D;

		DsvDescriptors			= renderer->DsvHeapManager().GetNewDescriptors(1);
		DepthStencilCpuHandle	= renderer->DsvHeapManager().CpuHandle(DsvDescriptors[0]);

		DepthStencil = renderer->Device()->CreateCommittedResource(&heapProps, RHIHeapFlag::None, &resourceDesc, RHIResourceState::Common, &clearValue);
		renderer->Device()->CreateDepthStencilView(DepthStencil, &dsvDesc, DepthStencilCpuHandle);
	}

	// Create a root signature.
	{
		RHIDescriptorRange ranges[1];
		ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);

		RHIRootParameter parameters[1];
		parameters[0].InitAsConstantBufferView(0, 0, RHIShaderVisibility::All);

		RHIRootSignatureDesc desc;
		desc.Init(_countof(parameters), parameters, 0, nullptr, GetRootSignatureFlags(true, true, false, false, true, true, false));

		RootSignature = renderer->SerializeAndCreateRootSignature(&desc, RHIRootSignatureVersion::Ver1);
	}

	// Create a pipeline state object.
	{
		RHIBlobWrapper vertexShader		(renderer->LoadShader(__TEXT("ViewFrustum:Main.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper geometryShader	(renderer->LoadShader(__TEXT("ViewFrustum:Main.gs"), RHIShaderType::Geometry));
		RHIBlobWrapper pixelShader		(renderer->LoadShader(__TEXT("ViewFrustum:Main.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || geometryShader.Blob->IsNull() || pixelShader.Blob->IsNull())
		{
			return;
		}

		RHIGraphicsPipelineStateDesc psoDesc			= {};
		psoDesc.RootSignature							= RootSignature;
		psoDesc.InputLayout								= { InputLayouts::Frustum::inputElementDesc, _countof(InputLayouts::Frustum::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.GS										= { geometryShader.GetBufferPointer(), geometryShader.GetBufferSize() };
		psoDesc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= renderer->RenderTargetFormat();
		psoDesc.DSVFormat								= DepthStencilViewFormat;
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= TRUE;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable			= FALSE;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;
		psoDesc.RasterizerState.FrontCounterClockwise	= FALSE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp		= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha	= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend		= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend	= RHIBlend::InvSrcAlpha;

		PSO = renderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : SerializeAndCreateRootSignature
 */
RHIRootSignature* IRenderer::SerializeAndCreateRootSignature(const RHIRootSignatureDesc* desc, const RHIRootSignatureVersion version)
{
	RHIBlobWrapper signature(nullptr);
	RHIBlobWrapper error(nullptr);

	mDevice->SerializeRootSignature(desc, version, &signature.Blob, &error.Blob);

	if (error.Blob && !error.Blob->IsNull())
	{
		MessageBoxA(NULL, (LPCSTR)error.GetBufferPointer(), "Error", MB_OK | MB_ICONERROR);
		return nullptr;
	}

	return mDevice->CreateRootSignature(0, signature.GetBufferPointer(), signature.GetBufferSize());
}

/**
 * Function : WaitForPreviousFrame
 */
void IRenderer::WaitCommandList()
{
	// WAITING FOR THE FRAME TO COMPLETE BEFORE CONTINUING IS NOT BEST PRACTICE.
	// This is code implemented as such for simplicity. More advanced samples 
	// illustrate how to use fences for efficient resource usage.

	// Signal and increment the fence value.
	const UINT64 fence = mFenceValue;
	mCommandQueue->Signal(mFence, fence);
	++mFenceValue;

	// Wait until the previous frame is finished.
	if (mFence->GetCompletedValue() < fence)
	{
		mFence->SetEventOnCompletion(fence, mFenceEvent);
		waitForSingleObject(mFenceEvent, INFINITE);
	}

	//mFrameIndex = mSwapChain->GetCurrentBackBufferIndex();
}

/**
 * Function : StartRendering
 */
void IRenderer::StartRendering(bool isResetBarriers)
{
	UINT frame = FrameIndex();

	mCommandAllocator->Reset();
	mCommandList->Reset(mCommandAllocator, nullptr);
	if (isResetBarriers)
	{
		mCommandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));
		mCommandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthWrite));
	}
	mCommandList->OMSetRenderTargets(1, &mRtvHeapManager.CpuHandle(frame), false, &mDsvHeapManager.CpuHandle(frame));
}

/**
 * Function : StopRendering
 */
void IRenderer::StopRendering()
{
	mCommandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
	mCommandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present));

	CommitCommandList(mCommandList);
}

/**
 * Function : Present
 */
void IRenderer::Present()
{
	mSwapChain->Present(0, 0);

	mPreviouseFrameIndex	= mFrameIndex;
	mFrameIndex				= mSwapChain->GetCurrentBackBufferIndex();

	UploadCommandAllocator()->Reset();
}

/**
 * Function : Clear
 */
void IRenderer::ClearRTV(const float* colorRGBA)
{
	mCommandList->ClearRenderTargetView(mRtvHeapManager.CpuHandle(FrameIndex()), colorRGBA, 0, nullptr);
}

/**
 * Function : ClearDSV
 */
void IRenderer::ClearDSV(const float depth, const UINT8 stencil)
{
	mCommandList->ClearDepthStencilView(mDsvHeapManager.CpuHandle(FrameIndex()), RHIClearFlags::Depth | RHIClearFlags::Stencil, depth, stencil, 0, nullptr);
}

/**
 * Function : CommitCommandList
 */
void IRenderer::CommitCommandList(RHICommandList* commandList)
{
	CloseAndWait(commandList);
}

/**
 * Function : CloseAndWait
 */
void IRenderer::CloseAndWait(RHICommandList* commandList)
{
	commandList->Close();

	RHICommandList* lists[] = { commandList };
	mCommandQueue->ExecuteCommandLists(1, lists);

	WaitCommandList();
}

/**
 * Function : UpdateBuffer
 */
void IRenderer::UpdateBuffer(RHIBuffer* buffer, RHISubresourceData* subresData)
{
	assert(buffer->Buffer() != nullptr);

	if (buffer->HeapType() == RHIHeapType::Upload)
	{
		buffer->Buffer()->InitializeSubData(0, subresData->Data, subresData->RowPitch);
		return;
	}

	assert(subresData->RowPitch < kUploadBufferWidth);

	RHIResource* resource = buffer->Buffer();

	mUploadCommandList->Reset(mUploadCommandAllocator, nullptr);

	mUploadCommandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(resource, buffer->ResourceState(), RHIResourceState::CopyDest));

	mDevice->UpdateSubresources(1, mUploadCommandList, resource, mUploadBuffer, 0, 0, 1, subresData, false);

	mUploadCommandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(resource, RHIResourceState::CopyDest, buffer->ResourceState()));

	CloseAndWait(mUploadCommandList);
}

/**
 * Function : QueueOnUpdateBuffer
 */
void IRenderer::QueueOnUpdateBuffer(RHIBuffer* buffer, RHISubresourceData* subresData)
{
	mUpdateCommands.emplace_back(buffer, *subresData);

	if (buffer->HeapType() != RHIHeapType::Upload)
	{
		mUpdateStartResources.push_back(RHIResourceBarrier::MakeTransition(buffer->Buffer(), buffer->ResourceState(), RHIResourceState::CopyDest));
		mUpdateStopResources.push_back(RHIResourceBarrier::MakeTransition(buffer->Buffer(), RHIResourceState::CopyDest, buffer->ResourceState()));
	}
}

/**
 * Function : ExecuteUpdateQueue
 */
void IRenderer::ExecuteUpdateQueue()
{
	mUploadCommandList->Reset(mUploadCommandAllocator, nullptr);

	if (mUpdateStartResources.size() > 0)
		mUploadCommandList->ResourceBarrier(mUpdateStartResources.size(), mUpdateStartResources.data());

	for (auto& command : mUpdateCommands)
	{
		RHIBuffer*			buffer		= command.first;
		RHISubresourceData*	subresData	= &command.second;

		if (buffer->HeapType() == RHIHeapType::Upload)
		{
			buffer->Buffer()->InitializeSubData(0, subresData->Data, subresData->RowPitch);
			continue;
		}

		assert((subresData->RowPitch < kUploadBufferWidth) && "Row pitch of subresource data bigger than size of upload buffer.");

		mDevice->UpdateSubresources(1, mUploadCommandList, buffer->Buffer(), mUploadBuffer, 0, 0, 1, subresData, false);
	}

	if (mUpdateStopResources.size() > 0)
		mUploadCommandList->ResourceBarrier(mUpdateStopResources.size(), mUpdateStopResources.data());

	CloseAndWait(mUploadCommandList);

	mUpdateCommands.clear();
	mUpdateStartResources.clear();
	mUpdateStopResources.clear();
}

/**
 * Function : ChangeResourceBarrier
 */
void IRenderer::ChangeResourceBarrier(const UINT numBarriers, const RHIResourceBarrier* barriers)
{
	const UINT partition	= std::max(100u, numBarriers / 100u);
	const UINT chunks		= numBarriers / partition + 1u;

	UINT offset = 0;
	for (size_t i = 0; i < chunks; ++i)
	{

		const UINT chunk = (offset + partition > numBarriers) ? numBarriers - offset : partition;

		mResourceBarrierAllocator->Reset();
		mResourceBarrierList->Reset(mResourceBarrierAllocator, nullptr);
		mResourceBarrierList->ResourceBarrier(chunk, barriers + offset);
		CommitCommandList(mResourceBarrierList);

		offset += chunk;
	}
}
