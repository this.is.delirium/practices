#pragma once
#include "StdafxCore.h"

class World;

class FrameRenderGraph
{
public:
	FrameRenderGraph();

	void Evaluate(const World& world) const;

protected:
};
