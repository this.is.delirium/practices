#include "StdafxCore.h"
#include "GraphicsCore.h"


namespace GraphicsCore
{
	IRenderer*			gRenderer	= nullptr;
	RHIRenderingDevice*	gDevice		= nullptr;

	/**
	 * Function : rp
	 */
	IRenderPipeline* rp()
	{
		assert(RenderSystem::Instance().rp());

		return RenderSystem::Instance().rp();
	}

	/**
	 * Function : rs
	 */
	RenderSystem& rs()
	{
		return RenderSystem::Instance();
	}
}
