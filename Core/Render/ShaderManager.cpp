#include "StdafxCore.h"
#include "ShaderManager.h"
#include "GraphicsCore.h"

/**
 * Function : RegisterShader
 */
ShaderManager::ShaderPtr ShaderManager::CreateShader(const RHIShaderDefinition& shader, const RHI::BindedResourceViews& bindedViews)
{
	assert(mShaders.find(shader) == mShaders.cend() && "Such shader already is registered.");
	mShaders[shader] = std::make_shared<Shader>();
	mShaders[shader]->shaderDefinition()			= shader;
	mShaders[shader]->bindedResourceViewsPerFrame()	= bindedViews;

	LoadShaders(shader);

	return mShaders[shader];
}

/**
 * Function : LoadShader
 */
void ShaderManager::LoadShaders(const RHIShaderDefinition& def)
{
	for (const auto& shader : def.shaders())
	{
		auto& wrapper = mShaders[def]->blobWrapper(shader.first);

		wrapper.reset(GraphicsCore::gRenderer->LoadShader(shader.second, shader.first));
	}
}

/**
 * Function : GetShader
 */
ShaderManager::ShaderPtr ShaderManager::GetShader(const std::string& name)
{
	// RHIShaderDefinition::Hash uses only name, that's why we can use it.
	RHIShaderDefinition shader(name, {}, {});

	assert(mShaders.find(shader) != mShaders.cend() && "The shaders wasn't registered.");

	return mShaders[shader];
}
