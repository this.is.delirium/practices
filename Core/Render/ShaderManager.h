#pragma once
#include "StdafxCore.h"
#include "Shader.h"
#include <RHI.h>
#include <RHIBindedResourceView.h>
#include <RHIShaderDefinition.h>

// The class responds for shaders, their layouts.
class ShaderManager
{
public:
	using ShaderPtr = std::shared_ptr<Shader>;

public:
	ShaderPtr CreateShader(const RHIShaderDefinition& shader, const RHI::BindedResourceViews& bindedViews);

	ShaderPtr GetShader(const std::string& name);

protected:
	void LoadShaders(const RHIShaderDefinition& shader);

protected:
	std::unordered_map<RHIShaderDefinition, ShaderPtr> mShaders;
};
