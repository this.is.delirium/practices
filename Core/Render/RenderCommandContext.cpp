#include "StdafxCore.h"
#include "RenderCommandContext.h"
#include "GraphicsCore.h"

using namespace GraphicsCore;

/**
 * Function : Release
 */
void RenderCommandContext::Release()
{
	RHISafeRelease(mCmdAllocator);
	RHISafeRelease(mCmdList);
}

/**
 * Function : Initialize
 */
void RenderCommandContext::Initialize()
{
	mCmdAllocator	= gDevice->CreateCommandAllocator(RHICommandListType::Direct);
	mCmdList		= gDevice->CreateCommandList(0u, RHICommandListType::Direct, mCmdAllocator, nullptr);

	InitializeViewportAndScissorRect();
}

/**
 * Function :
 */
void RenderCommandContext::InitializeViewportAndScissorRect()
{
	RHISwapChainDesc scDesc;
	gRenderer->SwapChain()->GetDesc(&scDesc);

	mViewport			= {};
	mViewport.Width		= static_cast<FLOAT> (scDesc.BufferDesc.Width);
	mViewport.Height	= static_cast<FLOAT> (scDesc.BufferDesc.Height);
	mViewport.MaxDepth	= 1.0f;

	mScissorRect		= {};
	mScissorRect.right	= static_cast<LONG> (scDesc.BufferDesc.Width);
	mScissorRect.bottom	= static_cast<LONG> (scDesc.BufferDesc.Height);
}

/**
 * Function : Flush
 */
void RenderCommandContext::Flush()
{
	// Flush commands.
	gRenderer->CommitCommandList(mCmdList);

	// Reset a command allocator and list.
	mCmdAllocator->Reset();
	mCmdList->Reset(mCmdAllocator, nullptr);
}
