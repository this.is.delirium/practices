#pragma once
#include "StdafxCore.h"
#include "IRenderer.h"
#include "IRenderPipeline.h"
#include "RenderSystem.h"

namespace GraphicsCore
{
	extern IRenderer*			gRenderer;
	extern RHIRenderingDevice*	gDevice;

	IRenderPipeline* rp();
	RenderSystem& rs();
}
