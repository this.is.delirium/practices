#include "StdafxCore.h"
#include "Shader.h"

/**
 * Function : Shader
 */
Shader::~Shader()
{
	RHISafeRelease(mRootSignature);
	RHISafeRelease(mPSO);
}


// Extend RHIUtils.
namespace RHIUtils
{
namespace Fill
{
void GraphicsPipelineStateDesc(Shader& shader, RHIGraphicsPipelineStateDesc& desc)
{
	const auto& shaderInfo = shader.shaderInfo();
	desc.InputLayout	= shaderInfo.shaderDefinition.inputLayout();
	desc.RootSignature	= shader.rootSignature();

	// ToDo: Rewrite?
	auto& vs = shaderInfo.shaders[ToInt(RHIShaderType::Vertex)];
	auto& hs = shaderInfo.shaders[ToInt(RHIShaderType::Hull)];
	auto& gs = shaderInfo.shaders[ToInt(RHIShaderType::Geometry)];
	auto& ds = shaderInfo.shaders[ToInt(RHIShaderType::Domain)];
	auto& ps = shaderInfo.shaders[ToInt(RHIShaderType::Pixel)];

	if (vs && !vs->IsNull())
	{
		desc.VS = { vs->GetBufferPointer(), vs->GetBufferSize() };
	}

	if (hs && !hs->IsNull())
	{
		desc.HS = { hs->GetBufferPointer(), hs->GetBufferSize() };
	}

	if (gs && !gs->IsNull())
	{
		desc.GS = { gs->GetBufferPointer(), gs->GetBufferSize() };
	}

	if (ds && !ds->IsNull())
	{
		desc.DS = { ds->GetBufferPointer(), ds->GetBufferSize() };
	}

	if (ps && !ps->IsNull())
	{
		desc.PS = { ps->GetBufferPointer(), ps->GetBufferSize() };
	}
}

void GraphicsPipelineStateDesc(Shader& shader, RHIComputePipelineStateDesc& desc)
{
	const auto& shaderInfo = shader.shaderInfo();
	desc.RootSignature = shader.rootSignature();

	auto& cs = shaderInfo.shaders[ToInt(RHIShaderType::Compute)];
	desc.CS = { cs->GetBufferPointer(), cs->GetBufferSize() };
}
}
}
