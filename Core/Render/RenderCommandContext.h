#pragma once
#include "StdafxCore.h"
#include "RHI.h"

class RenderCommandContext
{
public:
	void Release();
	void Initialize();

	void Flush();

public:
	RHICommandList* CommandList() { return mCmdList; }
	const RHIViewport& Viewport() const { return mViewport; }
	const RHIRect& ScissorRect() const { return mScissorRect; }

protected:
	void InitializeViewportAndScissorRect();

protected:
	RHICommandAllocator*	mCmdAllocator;
	RHICommandList*			mCmdList;

	RHIViewport				mViewport;
	RHIRect					mScissorRect;
};
