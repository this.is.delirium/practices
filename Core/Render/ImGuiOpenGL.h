#pragma once
#include "StdafxCore.h"
#include "IImGui.h"

class ImGuiOpenGL : public IImGui
{
public:
	ImGuiOpenGL(Window* window, IRenderer* renderer)
		: IImGui(window, renderer) { }

	~ImGuiOpenGL() { }

	void Release() override final;
	void Initialize() override final;

	void NewFrame() override final;
	void Render() override final;

protected:
	void InvalidateDeviceObjects() override final;
	void CreateFontsTexture() override final;
	void CreateDeviceObjects() override final;

protected:
	INT64			mTicksPerSecond = 0;
	INT64			mTime = 0;
	bool			mMousePressed[3] = { false, false, false };
	float			mMouseWheel = 0.0f;
	UINT			mFontTexture = 0;
	int				mShaderHandle = 0, mVertHandle = 0, mFragHandle = 0;
	int				mAttribLocationTex = 0, mAttribLocationProjMtx = 0;
	int				mAttribLocationPosition = 0, mAttribLocationUV = 0, mAttribLocationColor = 0;
	unsigned int	mVboHandle = 0, mVaoHandle = 0, mElementsHandle = 0;
};
