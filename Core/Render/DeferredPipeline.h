#pragma once
#include "StdafxCore.h"
#include "IRenderPipeline.h"
#include <MRTPass.h>
#include "Shader.h"
#include <RHIVertexBuffer.h>

// The class must be stateless to provide multithreading.
class DeferredPipeline : public IRenderPipeline
{
public:
	DeferredPipeline(IRenderer& renderer)
		: IRenderPipeline(renderer) { }

	bool Initialize(const RPDesc& desc) override final;
	void Release() override final;

	void BeginFrame(RenderCommandContext& rcc) const override final;
	void CastShadows(RenderCommandContext& rcc) const override final;
	void DrawOpaque(RenderCommandContext& rcc, const IntersectionSet::ModeSet& objects, const World& world) const override final;
	void ApplyLighting(RenderCommandContext& rcc, const World& world) const override final;
	void ReceiveShadows(RenderCommandContext& rcc) const override final;
	void DrawGUI(RenderCommandContext& rcc) const override final;
	void EndFrame(RenderCommandContext& rcc) const override final;

public:
	//! ToDo: Think about it.
	MRTPass* GBuffer() override final { return &mGBuffer; }

	std::shared_ptr<Shader> GPassShader() override final { return mGPassShader; }
protected:
	void InitializeMRT();
	void InitializeFullScreenQuadPass();
	void InitializeGeometryPass();

protected:
	MRTPass mGBuffer;

	// ToDo: Think about it.
	struct FullScreenQuad
	{
		void Release()
		{
			quad.Release();
		}

		std::shared_ptr<Shader>	shader;
		RHIVertexBuffer			quad;
	} mFullScreenQuad;

	std::shared_ptr<Shader>	mGPassShader;
};
