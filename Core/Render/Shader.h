#pragma once
#include "StdafxCore.h"
#include <RHI.h>
#include <RHIBindedResourceView.h>
#include <RHIShaderDefinition.h>

//! Extend std::default_delete by RHIBlob.
namespace std
{
template<>
struct default_delete<RHIBlob> {
	void operator()(RHIBlob* blob) { blob->Release(); }
};
}

//!
class Shader
{
public:
	using BlobWrapper = std::unique_ptr<RHIBlob>;

	struct ShaderInfo
	{
		RHIShaderDefinition shaderDefinition;
		BlobWrapper			shaders[ToInt(RHIShaderType::Count)];

		bool operator < (const ShaderInfo& rhs) const
		{
			return shaderDefinition.Hash() < rhs.shaderDefinition.Hash();
		}
	};

public:
	~Shader();

	const ShaderInfo& shaderInfo() const { return mShaderInfo; }

	RHIShaderDefinition& shaderDefinition() { return mShaderInfo.shaderDefinition; }
	BlobWrapper& blobWrapper(const RHIShaderType type) { return mShaderInfo.shaders[ToInt(type)]; }

	RHI::BindedResourceViews& bindedResourceViewsPerFrame() { return mPerFrameResourceViews; }

	RHIRootSignature*& rootSignature() { return mRootSignature; }
	RHIPipelineState*& pso() { return mPSO; }

	const RHIRootSignature* rootSignature() const { return mRootSignature; }
	const RHIPipelineState* pso() const { return mPSO; }

protected:
	ShaderInfo					mShaderInfo;

	RHIRootSignature*			mRootSignature			= nullptr;
	RHIPipelineState*			mPSO					= nullptr;

	//! ToDo: Consider case when some objects have got same shader, but different resource views per frame.
	//! ToDo: Do move to ShaderInfo?
	RHI::BindedResourceViews	mPerFrameResourceViews;
};

//! Extend RHIUtils.
namespace RHIUtils
{
namespace Fill
{
void GraphicsPipelineStateDesc(Shader& shaderInfo, RHIGraphicsPipelineStateDesc& desc);
void GraphicsPipelineStateDesc(Shader& shaderInfo, RHIComputePipelineStateDesc& desc);
}
}
