#pragma once
#include "StdafxRHI.h"
#include "RHI.h"
#include "RHIBuffer.h"
#include "RHIDescriptorHeapManager.h"

#include <ImageFormat.h>

class RenderSurface;

/**
 * DO's :
 * Use a couple of worker threads for command list recording.
 *
 * Dont's :
 * Don�t toggle between compute and graphics on the same command queue more than absolutely necessary (This is still a heavyweight switch to make).
 *
 * TODO: Use a delayed changing resources barrier (accumulate and change all resources in same time).
 */

class IRenderer
{
public:
	virtual ~IRenderer() { }

	// Call after device creation.
	virtual bool Initialize(const RenderSurface* renderSurface);
	virtual void Release();

	FORCEINLINE RHIRenderingDevice* Device() { return mDevice; }

	FORCEINLINE RHICommandAllocator* CommandAllocator() { return mCommandAllocator; }
	FORCEINLINE RHICommandList* CommandList() { return mCommandList; }

	FORCEINLINE RHICommandAllocator* UploadCommandAllocator() { return mUploadCommandAllocator; }
	FORCEINLINE RHICommandList* UploadCommandList() { return mUploadCommandList; }
	FORCEINLINE RHIResource* UploadBuffer() { return mUploadBuffer; }

	FORCEINLINE RHICommandQueue* CommandQueue() { return mCommandQueue; }
	FORCEINLINE RHISwapChain* SwapChain() { return mSwapChain; }

	FORCEINLINE RHIDescriptorHeapManager& RtvHeapManager() { return mRtvHeapManager; }
	FORCEINLINE RHIDescriptorHeapManager& DsvHeapManager() { return mDsvHeapManager; }
	FORCEINLINE RHIDescriptorHeapManager& CbvSrvUavHeapManager() { return *mCurrentCbvSrvUavHeapManager; }
	FORCEINLINE RHIDescriptorHeapManager& NonShaderCbvSrvUavHeapManager() { return mNonShaderCbvSrcUavHeapManager; }

	FORCEINLINE void SetDefaultCbvSrvUavHeapManager() { mCurrentCbvSrvUavHeapManager = &mCbvSrvUavHeapManager; }
	FORCEINLINE void SetCbvSrvUavHeapManager(RHIDescriptorHeapManager& heapManager) { mCurrentCbvSrvUavHeapManager = &heapManager; }

	FORCEINLINE UINT FrameCount() const { return kFrameCount; }

	FORCEINLINE RHIResource* CurrentRenderTarget() { return mRenderTargets[FrameIndex()]; }
	FORCEINLINE RHIResource* CurrentDepthStencil() { return mDepthStencils[FrameIndex()]; }

	FORCEINLINE RHIResource* RenderTarget(const UINT id) { return mRenderTargets[id]; }
	FORCEINLINE RHIResource* DepthStencil(const UINT id) { return mDepthStencils[id]; }

	FORCEINLINE RHIGPUDescriptorHandle CurrentDepthStencilGpuHandle() { return mGpuHandleDSTexures[FrameIndex()]; }

	FORCEINLINE RHICPUDescriptorHandle CurrentRenderTargetCpuHandle() const { return mRtvHeapManager.CpuHandle(FrameIndex()); }
	FORCEINLINE RHICPUDescriptorHandle CurrentDepthStencilCpuHandle() const { return mDsvHeapManager.CpuHandle(FrameIndex()); }

	FORCEINLINE UINT FrameIndex() const { return mFrameIndex; }

	FORCEINLINE RHIFormat DepthStencilViewFormat() const { return mDepthStencilViewFormat; }
	FORCEINLINE RHIFormat RenderTargetFormat() const { return mRenderTargetFormat; }

	FORCEINLINE RHIFence* Fence() { return mFence; }

	FORCEINLINE RHIRootSignature* ScreenQuadRootSignature() const { return mScreenQuadPart.RootSignature; }
	FORCEINLINE RHIPipelineState* ScreenQuadPSO() const { return mScreenQuadPart.PSO; }

	FORCEINLINE RHIRootSignature* FrustumRootSignature() const { return mViewFrustumPart.RootSignature; }
	FORCEINLINE RHIPipelineState* FrustumPSO() const { return mViewFrustumPart.PSO; }
	FORCEINLINE RHICPUDescriptorHandle* FrustumDepthStencilCpuHandle() { return &mViewFrustumPart.DepthStencilCpuHandle; }

	void WaitCommandList();

public: //! rendering methods.
	void StartRendering(bool isResetBarriers = true);
	void StopRendering();
	void ClearRTV(const float* colorRGBA);
	void ClearDSV(const float depth = 1.0f, const UINT8 stencil = 0);
	void Present();

public:
	void ChangeResourceBarrier(const UINT numBarriers, const RHIResourceBarrier* barriers);

	void CommitCommandList(RHICommandList* commandList);
	void CloseAndWait(RHICommandList* commandList);

public:
	RHIRootSignature* SerializeAndCreateRootSignature(const RHIRootSignatureDesc* desc, const RHIRootSignatureVersion version);

	virtual String ShaderFolder() const = 0;
	virtual RHIBlob* LoadShader(const String& shader, const RHIShaderType type, const RHIShaderMacro* macroes = nullptr, const bool fromFile = true, const String* initialDirectoryForSources = nullptr) const = 0;

	virtual void DumpRenderTargetToFile(const String& path, const ImageFormat format) = 0;

public:
	void UpdateBuffer(RHIBuffer* buffer, RHISubresourceData* subresData);

	void QueueOnUpdateBuffer(RHIBuffer* buffer, RHISubresourceData* subresData);
	void ExecuteUpdateQueue();

protected:
	virtual void CheckRHI() = 0;

	void LoadPipeline(const RenderSurface* renderSurface);

	virtual DWORD waitForSingleObject(HANDLE handle, DWORD miliSeconds) = 0;

protected:

	using UpdateCommand		= std::pair<RHIBuffer*, RHISubresourceData>;
	using UpdateCommands	= std::vector<UpdateCommand>;
	using UpdateResources	= std::vector<RHIResourceBarrier>;

	static const UINT			kFrameCount						= 3;
	static const UINT			kRtvDescriptors					= 30;
	static const UINT			kCbvDescriptors					= 1000;
	static const UINT			kNonShaderCbvDescriptors		= 30;
	static const UINT			kDsvDescriptors					= 30;

	static const UINT64			kUploadBufferWidth				= 100 * 1024 * 1024;

	std::vector<int>			mRtvDescriptors;
	std::vector<int>			mDsvDescriptors;
	std::vector<int>			mDsvTexturesDescriptors;

	RHIResource*				mRenderTargets[kFrameCount];
	RHIResource*				mDepthStencils[kFrameCount];
	RHIGPUDescriptorHandle		mGpuHandleDSTexures[kFrameCount];

	RHIFormat					mRenderTargetFormat;
	RHIFormat					mDepthStencilFormat;
	RHIFormat					mDepthStencilViewFormat;

	RHIRenderingDevice*			mDevice;
	RHICommandAllocator*		mCommandAllocator;
	RHICommandQueue*			mCommandQueue;
	RHICommandList*				mCommandList;

	RHICommandAllocator*		mResourceBarrierAllocator;
	RHICommandList*				mResourceBarrierList;

	RHICommandAllocator*		mUploadCommandAllocator;
	RHICommandList*				mUploadCommandList;
	RHIResource*				mUploadBuffer;

	UpdateCommands				mUpdateCommands;
	UpdateResources				mUpdateStartResources;
	UpdateResources				mUpdateStopResources;

	RHISwapChain*				mSwapChain;

	RHIDescriptorHeapManager	mRtvHeapManager;
	RHIDescriptorHeapManager	mCbvSrvUavHeapManager;
	RHIDescriptorHeapManager	mNonShaderCbvSrcUavHeapManager;
	RHIDescriptorHeapManager	mDsvHeapManager;

	RHIDescriptorHeapManager*	mCurrentCbvSrvUavHeapManager	= nullptr;

	struct ScreenQuadPart
	{
		void Release()
		{
			RHISafeRelease(PSO);
			RHISafeRelease(RootSignature);
		}

		void Initialize(IRenderer* renderer);

		RHIPipelineState*	PSO				= nullptr;
		RHIRootSignature*	RootSignature	= nullptr;
	} mScreenQuadPart;

	struct ViewFrustumPart
	{
		void Release()
		{
			RHISafeRelease(PSO);
			RHISafeRelease(RootSignature);

			Renderer->DsvHeapManager().FreeDescriptors(DsvDescriptors);
			RHISafeRelease(DepthStencil);
		}

		void Initialize(IRenderer* renderer);

		IRenderer*				Renderer;

		RHIPipelineState*		PSO				= nullptr;
		RHIRootSignature*		RootSignature	= nullptr;

		std::vector<int>		DsvDescriptors;
		RHIResource*			DepthStencil	= nullptr;
		RHICPUDescriptorHandle	DepthStencilCpuHandle;

		RHIFormat				DepthStencilFormat;
		RHIFormat				DepthStencilViewFormat;
	} mViewFrustumPart;

	// Synchronization objects.
	UINT		mFrameIndex;
	UINT		mPreviouseFrameIndex;
	RHIFence*	mFence;
	UINT64		mFenceValue;
	HANDLE		mFenceEvent;
};
