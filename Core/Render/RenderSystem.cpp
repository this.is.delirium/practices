#include "StdafxCore.h"
#include "RenderSystem.h"
#include "Scene/Scene.h"

#include <D3D12/D3D12Renderer.h>
#include "DeferredPipeline.h"
#include "FrameRenderGraph.h"
#include "ImGuiDirectX12.h"
#include "RenderSurface.h"
#include <Window.h>
#include <World.h>

/**
 * Function : RenderSystem
 */
RenderSystem::RenderSystem()
{

}

/**
 * Function : Release
 */
RenderSystem::~RenderSystem()
{
	Release(); // ToDo: Reconsider later.
}

/**
 * Function : Release
 */
void RenderSystem::Release()
{
	mRenderer->Release();
	mRenderPipeline->Release();

	mRCCManager.Release();
}

bool RenderSystem::Initialize(const RSDesc& desc)
{
	mWindow = desc.window;

	switch (desc.gAPI)
	{
		case GraphicsAPI::DirectX12:
		{
			mRenderer	= std::make_shared<D3D12Renderer>();
			break;
		}
	}

	assert(mRenderer != nullptr);

	RenderSurface renderSurface;
	renderSurface.mWidth					= desc.window->Width();
	renderSurface.mHeight					= desc.window->Height();
	renderSurface.mWindowHandle				= desc.window->GetHwnd();
	renderSurface.mIsFullScreen				= desc.window->IsFullScreen();
	renderSurface.mIsEnabledGPUValidation	= false;

	if (!mRenderer->Initialize(&renderSurface))
	{
		return false;
	}

	mRenderer->CommitCommandList(mRenderer->CommandList());

	mRCCManager.Initialize();

	mFrameRenderGraph	= std::make_shared<FrameRenderGraph>();
	mRenderPipeline		= std::make_shared<DeferredPipeline>(*mRenderer);

	{
		IRenderPipeline::RPDesc rpDesc;
		rpDesc.gAPI		= desc.gAPI;
		rpDesc.window	= desc.window;

		if (!mRenderPipeline->Initialize(rpDesc))
		{
			return false;
		}
	}

	return true;
}

/**
 * Function : Culling
 */
void RenderSystem::Culling(RenderCommandContext& rcc, const World& world, IntersectionSet& set) const
{
	// ToDo:
	// 1. Add a culling (for example, frustum culling).
	for (const auto& mode : world.GetScene().GetModeObjects())
	{
		for (const auto& object : mode.second)
		{
			set.AddObject(mode.first, object.get());
		}
	}

	set.Sort();
}

/**
 * Function: AspectRatio
 */
float RenderSystem::AspectRatio() const
{
	return mWindow->AspectRatio();
}
