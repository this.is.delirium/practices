#pragma once
#include "StdafxCore.h"
#include "GraphicsAPI.h"
#include "Patterns/Singleton.h"
#include "RenderCommandContextManager.h"
#include "Scene/IntersectionSet.h"
#include "ShaderManager.h"

class IImGui;
class IRenderer;
class FrameRenderGraph;
class IRenderPipeline;
class Window;
class World;

class RenderSystem : public Singleton<RenderSystem>
{
public:
	struct RSDesc
	{
		GraphicsAPI gAPI;
		Window*		window;
	};

public:
	RenderSystem();
	~RenderSystem();

	bool Initialize(const RSDesc& desc);
	void Release();

	void Culling(RenderCommandContext& rcc, const World& world, IntersectionSet& set) const;

public:
	IRenderer* renderer() { return mRenderer.get(); }

	const FrameRenderGraph& renderGraph() const { return *mFrameRenderGraph; }

	IRenderPipeline* rp() { return mRenderPipeline.get(); }

	RenderCommandContextManager& RCCManager() { return mRCCManager; }

	ShaderManager& shaderManager() { return mShaderManager; }

public:
	[[deprecated("Don't use it. ToDo: Thin about it")]]
	float AspectRatio() const;

protected:
	Window*								mWindow				= nullptr;
	std::shared_ptr<IRenderer>			mRenderer			= nullptr;
	std::shared_ptr<FrameRenderGraph>	mFrameRenderGraph	= nullptr;
	std::shared_ptr<IRenderPipeline>	mRenderPipeline		= nullptr;

	RenderCommandContextManager			mRCCManager;
	ShaderManager						mShaderManager;
};
