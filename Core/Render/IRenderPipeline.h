#pragma once
#include "StdafxCore.h"
#include "GraphicsAPI.h"
#include "IImGui.h"
#include "IRenderer.h"
#include "RenderCommandContext.h"
#include <Scene/IntersectionSet.h>

class Application;
class MRTPass;
class Shader;
class Window;
class World;

// The class must be stateless to provide multithreading.
class IRenderPipeline
{
public:
	struct RPDesc
	{
		GraphicsAPI		gAPI;
		Window*			window;
		Application*	app;
	};

	using DrawCallback = std::function<void(RHICommandList*)>;

public:
	IRenderPipeline(IRenderer& renderer)
		: mRenderer(renderer) { }

	virtual bool Initialize(const RPDesc& desc) = 0;
	virtual void Release() = 0;

	virtual void BeginFrame(RenderCommandContext& rcc) const = 0;
	virtual void CastShadows(RenderCommandContext& rcc) const = 0;
	virtual void DrawOpaque(RenderCommandContext& rcc, const IntersectionSet::ModeSet& objects, const World& world) const = 0;
	// ToDo: Reconsider later. Pass only lights?
	virtual void ApplyLighting(RenderCommandContext& rcc, const World& world) const = 0;
	virtual void ReceiveShadows(RenderCommandContext& rcc) const = 0;
	virtual void DrawGUI(RenderCommandContext& rcc) const = 0;
	virtual void EndFrame(RenderCommandContext& rcc) const = 0;

public:
	IImGui* GUI() { return mImGui.get(); }

public:
	virtual MRTPass* GBuffer() { return nullptr; }

	virtual std::shared_ptr<Shader> GPassShader() { return nullptr; }

protected:
	IRenderer& mRenderer;

	std::shared_ptr<IImGui> mImGui = nullptr; // ToDo: Reconsider later.
};
