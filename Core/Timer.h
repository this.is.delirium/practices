#pragma once
#include "StdafxCore.h"

class Timer
{
	using HighResClock = std::chrono::high_resolution_clock;

public:
	FORCEINLINE void Start()
	{
		mStartTime	= HighResClock::now();
		mIsStopped	= false;
	}

	FORCEINLINE void Stop()
	{
		mStopTime	= HighResClock::now();
		mIsStopped	= true;
	}

	FORCEINLINE bool IsStopped() { return mIsStopped; }

	template<typename Time = std::chrono::milliseconds>
	typename Time::rep Time()
	{
		return std::chrono::duration_cast<Time>((IsStopped() ? mStopTime : HighResClock::now()) - mStartTime).count();
	}

protected:
	using ChronoTime = std::chrono::time_point<std::chrono::steady_clock>;

	ChronoTime	mStartTime;
	ChronoTime	mStopTime;
	bool		mIsStopped = false;
};
