#include "StdafxCore.h"
#include "LightSource.h"

#include "Render/IRenderer.h"
#include "Logger.h"

#include <ECS/MeshComponent.h>
#include <ECS/SceneComponent.h>

#include <RHIUtils.h>

#include <D3D12/private/DXMathHelper.h>

/**
 * Function : Release
 */
void LightSource::Release()
{
	RHISafeRelease(mConstBuffer);
}

/**
 * Function : Initialize
 */
void LightSource::Initialize(const bool createSceneComponent, const bool createMeshComponent)
{
	mConstBuffer = new ConstBufferForLight();
	mConstBuffer->Initialize(__TEXT("LightSource:ConstBuffer"), RHIUtils::GetConstBufferSize(), 1, sizeof(mConstBuffer->Data()));

	if (createSceneComponent)
		mSceneComponent.reset(BaseComponent::CreateSubobject<SceneComponent>());

	if (createMeshComponent)
		mMeshComponent.reset(BaseComponent::CreateSubobject<MeshComponent>());
}

/**
 * Function : SetAttenuation
 */
void LightSource::SetAttenuation(const DirectX::XMFLOAT3& attenuation)
{
	mConstBuffer->Data().Attenuation = DirectX::MakeFloat4(attenuation, mConstBuffer->Data().Attenuation.w);
}

/**
 * Function : SetType
 */
void LightSource::SetType(const LightSourceType type)
{
	mType = type;

	/*switch (mType)
	{
		case LightSourceType::Point:
		{
			mCamera.reset(new Camera());
			break;
		}

		default:
		{
			SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Light source supports only Point type."));
			break;
		}
	}*/
}

/**
 * Function : CalculateDistance
 */
float LightSource::CalculateDistance() const
{
	const DirectX::XMFLOAT4& diffuse	= mConstBuffer->Data().Diffuse;
	const DirectX::XMFLOAT4& atten		= mConstBuffer->Data().Attenuation;

	float C = std::max(std::max(diffuse.x, diffuse.y), diffuse.z);
	return (-atten.y + std::sqrtf(atten.y * atten.y - 4.0f * atten.z * (atten.x - 256.0f * C * atten.w))) / (2.0f * atten.z);
}
