#pragma once
#include "StdafxCore.h"
#include "World.h"

class Window;

class IApp
{
public:
	virtual WorldPtr& AddWorld(const std::string& name) = 0;
	virtual void SetActiveWorld(const WorldPtr& world) = 0;
	virtual WorldPtr& GetActiveWorld() = 0;

	virtual const Window* GetWindow() const = 0;
};
