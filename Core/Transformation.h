#pragma once
#include "StdafxCore.h"

//-- ToDo: Think about it.
#include <RHI.h>

class Transformation
{
public:
	Transformation();

	void WorldMatrix(DirectX::XMMATRIX& out);
	void NormalMatrix(DirectX::XMMATRIX& out);

	DirectX::XMFLOAT3& Scale() { return mScale; }
	DirectX::XMFLOAT3& Translation() { return mTranslation; }

	const DirectX::XMFLOAT3& GetTranslation() const { return mTranslation; }
	void SetTranslation(const DirectX::XMFLOAT3& test) { mTranslation = test; }

	//! Returns a quaternion of rotation.
	DirectX::XMVECTOR& Rotation() { return mRotation; }

	const DirectX::XMFLOAT4& getQuaternion() const;
	void setQuaternion(const DirectX::XMFLOAT4& quat);

protected:
	DirectX::XMFLOAT3 mScale;
	DirectX::XMFLOAT3 mTranslation;
	DirectX::XMVECTOR mRotation;

	mutable DirectX::XMFLOAT4 mTest;

	RTTR_ENABLE()
	RTTR_REGISTRATION_FRIEND
};
