#pragma once
#include "StdafxCore.h"

class AutoGlobalCounter
{
public:
	using Type = UINT;

protected:
	static const Type kMaxValue = 64u;

public:
	AutoGlobalCounter()
	{
		static std::atomic<Type> id = 0;
		mId = id.fetch_add(1, std::memory_order_relaxed);
		assert(mId < kMaxValue);
	}

	operator Type() const
	{
		return mId;
	}

protected:
	Type mId;
};
