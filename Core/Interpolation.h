#pragma once
#include "StdafxCore.h"

namespace Interpolation
{
	FORCEINLINE float Linear(const float a, const float b, const float t)
	{
		return a * (1.0f - t) + b * t;
	}

	FORCEINLINE float Cosine(const float a, const float b, const float t)
	{
		float x = (1.0f - cosf(t * DirectX::XM_PI)) * 0.5f;
		return Linear(a, b, x);
	}

	FORCEINLINE float Quantic(const float a, const float b, const float t)
	{
		float x = t * t * t * (t * (t * 6 - 15) + 10);
		return Linear(a, b, x);
	}
}
