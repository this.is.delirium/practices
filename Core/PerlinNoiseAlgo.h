#pragma once
#include "StdafxCore.h"
#include "HeightMapGeneratorAlgo.h"

class PerlinNoiseAlgo : public HeightMapGeneratorAlgo
{
public:
	~PerlinNoiseAlgo() { }

	PerlinNoiseAlgo(const int nbOfOctaves, const float persistence, const float startFrequency, const float startAmplitude)
		: mNbOfOctaves(nbOfOctaves), mPersistence(persistence), mStartFrequency(startFrequency), mStartAmplitude(startAmplitude) { }

	void Apply(const DirectX::XMUINT2& sizeMap, std::vector<float>& map, ProgressIndicator* progress) const override final;

protected:
	FORCEINLINE float Noise2D(const uint32_t x, const uint32_t y) const
	{
		/*std::seed_seq seeds({ x, y });
		std::mt19937 rng(seeds);
		std::random_device rng;
		std::default_random_engine eng(rng());
		std::uniform_real_distribution<float> distr(0.0f, 1.0f);

		return distr(eng);*/
		uint32_t value = x + y * 57;
		return HeightMapGeneratorAlgo::Noise(value) + 0.3f;
	}

	float PerlinNoise(uint32_t x, uint32_t y, const uint32_t frac) const;
	float Noise(const float x, const float y) const;
	float SmoothNoise(const float x, const float y) const;

protected:
	int		mNbOfOctaves;
	float	mPersistence;
	float	mStartFrequency;
	float	mStartAmplitude;
};
