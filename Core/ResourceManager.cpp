#include "StdafxCore.h"
#include "ResourceManager.h"
#include "FileManager.h"
#include "Logger.h"

/**
 * Function : ResourceManager
 */
ResourceManager::ResourceManager()
	: mFindResult()
{
	mRootFolder.Path	= __TEXT("is not set yet");
	mRootFolder.Parent	= nullptr;
}

/**
 * Function : SetRootFolder
 */
void ResourceManager::SetRootFolder(const String& folder)
{
	mRootFolder.Path = folder;
	mRootFolder.Folders.clear();
}

/**
 * Function : DetectRootFolder
 */
void ResourceManager::FindRootFolder()
{
	Char* buffer;
	if ((buffer = GetCurrentDir(NULL, 0)) != NULL)
	{
		String str(buffer);
		size_t posBuild = str.find(__TEXT("Build"));
		size_t posDebug = str.find(__TEXT("Debug"));

		if (posBuild == String::npos && posDebug == String::npos)
			mRootFolder.Path = str + __TEXT("\\Data");
		else
			mRootFolder.Path = (posBuild != String::npos) ? (str.substr(0, posBuild - 1) + __TEXT("\\Data")) : (str.substr(0, posDebug) + __TEXT("..\\Data"));
	}
	else
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Can't detect path for the current exe file."));
		return;
	}

	mRootFolder.Folders.clear();
}

/**
 * Function : Find
 */
String ResourceManager::Find(const String& name)
{
	mFindResult = __TEXT("");
	Node* currentNode = &mRootFolder;

	size_t pos;
	String strTemlate = name;
	String pattern(__TEXT("*.*"));
	while ((pos = strTemlate.find_first_of(__TEXT(":"), 0)) != String::npos)
	{
		String folderName = strTemlate.substr(0, pos);

		if (currentNode->Folders.find(folderName) == currentNode->Folders.cend())
		{
			String path = Find(currentNode->Path, folderName, pattern);

			if (!path.empty())
			{
				Node* node = new Node();
				node->Path = path;
				node->Parent = currentNode;
				currentNode->Folders[folderName] = node;
			}
			else
			{
				SLogger::Instance().AddMessage(LogMessageType::Warning, __TEXT("Can't find folder '%s' for template '%s' in root folder '%s'."), folderName.c_str(), name.c_str(), mRootFolder.Path.c_str());
				return String(__TEXT(""));
			}
		}

		currentNode = currentNode->Folders[folderName];
		strTemlate = strTemlate.substr(pos + 1); // + 1 - skip ':'.
	}

	pos = strTemlate.find_last_of(__TEXT("."));
	if (pos != String::npos)
	{
		pattern = __TEXT("*") + strTemlate.substr(pos);
		mFindResult = Find(currentNode->Path, strTemlate, pattern);
	}

	if (mFindResult.empty())
	{
		SLogger::Instance().AddMessage(LogMessageType::Warning, __TEXT("Can't find a file with name '%s'"), name.c_str());
	}

	return mFindResult;
}

/**
 * Function : FindFolder
 */
String ResourceManager::Find(const String& startFolder, const String& name, const String& pattern)
{
	String result;
	String startFolderRegExp = startFolder + __TEXT("/") + pattern;

	FindData findData;
	Handle handle = SFileManager::Instance().Find(startFolderRegExp, findData);

	std::vector<String> folders;

	FileManager& fManager = SFileManager::Instance();
	if (fManager.CheckHandle(handle))
	{
		do
		{
			if ((StrCmp(findData.FileName(), __TEXT(".")) != 0 && StrCmp(findData.FileName(), __TEXT("..")) != 0))
			{
				if (StrCmp(findData.FileName(), name.c_str()) == 0)
				{
					result = findData.FileName();
					break;
				}

				if (findData.IsDirectory())
				{
					folders.push_back(findData.FileName());
				}
			}
		} while (fManager.NextFile(handle, findData));
		fManager.Close(handle);

		if (!result.empty())
		{
			result = startFolder + __TEXT("/") + result;
		}
		else
		{
			for (const auto& folder : folders)
			{
				result = Find(startFolder + __TEXT("/") + folder, name, pattern);
				if (!result.empty())
					break;
			}
		}
	}

	return result;
}

/**
 * Function : LoadFile
 */
std::string ResourceManager::LoadFile(const String& file)
{
	std::string		content;
	std::ifstream	fileStream(file.c_str(), std::ios::in);

	if (!fileStream.is_open())
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Could not read file '%s'. File does not exist."), file.c_str());
		return "";
	}

	std::string line = "";

	while (!fileStream.eof())
	{
		std::getline(fileStream, line);
		content.append(line + "\n");
	}

	fileStream.close();
	return content;
}

/**
 * Function : SaveFile
 */
void ResourceManager::SaveFile(const String& fullPath, const std::string& file)
{
	Ofstream output(fullPath.c_str());

	if (!output.is_open())
		return;

	output.clear();

	output << file.c_str();

	output.close();
}
