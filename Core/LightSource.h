#pragma once
#include "StdafxCore.h"
#include "Camera.h"
#include "ConstantBuffers.h"
#include <RHI.h>

class MeshComponent;
class SceneComponent;

enum class LightSourceType
{
	Point = 0,
	Sun,
	SpotLight
};

class LightSource
{
public:
	void SetType(const LightSourceType type);

	std::shared_ptr<Camera>& GetCamera() { return mCamera; }

	void SetAmbient(const DirectX::XMFLOAT4& ambient) { mConstBuffer->Data().Ambient = ambient; }
	void SetDiffuse(const DirectX::XMFLOAT4& diffuse) { mConstBuffer->Data().Diffuse = diffuse; }
	void SetSpecular(const DirectX::XMFLOAT4& specular) { mConstBuffer->Data().Specular = specular; }
	void SetAttenuation(const DirectX::XMFLOAT3& attenuation);
	void SetItensity(const float itensity) { mConstBuffer->Data().Attenuation.w = itensity; }

	const DirectX::XMFLOAT4& Ambient() const { return mConstBuffer->Data().Ambient; }
	const DirectX::XMFLOAT4& Diffuse() const { return mConstBuffer->Data().Diffuse; }
	const DirectX::XMFLOAT4& Specular() const { return mConstBuffer->Data().Specular; }

	void Initialize(const bool createSceneComponent = true, const bool createMeshComponent = true);
	void Release();

	std::shared_ptr<SceneComponent>& GetSceneComponent() { return mSceneComponent; }
	std::shared_ptr<MeshComponent>& GetMeshComponent() { return mMeshComponent; }

	float CalculateDistance() const;

	const ConstBufferForLight& ConstBuffer() const { return *mConstBuffer; }
	ConstBufferForLight& ConstBuffer() { return *mConstBuffer; }

protected:
	LightSourceType					mType;

	std::shared_ptr<Camera>			mCamera;

	ConstBufferForLight*			mConstBuffer	= nullptr;

	std::shared_ptr<MeshComponent>	mMeshComponent;
	std::shared_ptr<SceneComponent>	mSceneComponent;
};
