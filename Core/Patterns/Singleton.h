#pragma once
#include "StdafxCore.h"

template<class T>
class Singleton
{
public:
	static T& Instance()
	{
		static T instance;
		return instance;
	}

protected:
	Singleton() { }
	~Singleton() { }
	Singleton(const Singleton&) = delete;
	Singleton& operator=(const Singleton&) = delete;
};
