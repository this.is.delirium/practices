#include "StdafxCore.h"
#include "StringUtils.h"

namespace StringUtils
{
/**
 * Function : Split
 */
void Split(const String& source, const String& delimiter, std::vector<String>& tokens)
{
	size_t offset = 0;
	while (offset != String::npos)
	{
		size_t pos = source.find(delimiter, offset);

		tokens.push_back(source.substr(offset, pos - offset));

		if (pos != String::npos)
		{
			offset = pos + delimiter.size();
		}
		else
		{
			offset = String::npos;
		}
	}
}
}
