#include "StdafxCore.h"
#include "IntersectionSet.h"
#include "SceneObject.h"

/**
 * Function : Sort
 */
void IntersectionSet::Sort()
{
	for (auto& mode : mSetObjects)
	{
		auto& container = mode.second;
		std::sort(container.begin(), container.end(),
			[](const SceneObject* a, const SceneObject* b) -> bool
		{
			//! ToDo: Consider case when some objects have got same shader, but different resource views per frame.
			return a->GetMaterial()->GetShader()->shaderInfo() < b->GetMaterial()->GetShader()->shaderInfo();
		});
	}
}
