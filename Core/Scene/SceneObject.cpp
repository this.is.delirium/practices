#include "StdafxCore.h"
#include "SceneObject.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<SceneObject>("SceneObject")
		.constructor<const std::string&>()
		.property("Name", &SceneObject::mName, registration::public_access)
		.property("LocalTransformation", &SceneObject::mLocalTransform, registration::public_access);
}


std::atomic<UINT> SceneObject::kUniqueId = 0;

/**
 * Function : SceneObject
 */
SceneObject::SceneObject(const std::string& name)
	: mName(name)
	, mMaterial(std::make_unique<Material>())
{
	constexpr UINT64 size = RHIUtils::GetConstBufferSize();
	mConstBufferTransforms.Initialize(__TEXT("SceneObject::Transforms"), size, 1, sizeof(mConstBufferTransforms.Data()));

	mConstBufferTransforms.Data().WorldMatrix	= DirectX::XMMatrixIdentity();
	mConstBufferTransforms.Data().NormalMatrix	= DirectX::XMMatrixIdentity();

	mConstBufferTransforms.UpdateDataOnGPU();

	++kUniqueId;
}

/**
 * Function : ~SceneObject
 */
SceneObject::~SceneObject()
{
	--kUniqueId;

	mVertexBuffer.Release();
	mIndexBuffer.Release();
}

/**
 * Function : GetUniqueName
 */
std::string SceneObject::GetUniqueName(const std::string& name)
{
	return name + std::to_string(kUniqueId);
}
