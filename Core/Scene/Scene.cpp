#include "StdafxCore.h"
#include "Scene.h"

#include <InputLayouts.h>
#include <Loaders/AssimpLoader.h>
#include "SceneObject.h"
#include <Render/Mode/DrawMode.h>
#include <Render/Mode/OpaqueMode.h>
#include <ResourceManager.h>
#include <StringUtils.h>

/**
 * Function : Initialize
 */
bool Scene::Initialize()
{
	RegisterMode<DrawMode>();
	RegisterMode<OpaqueMode>();

	mConstBufferPerFrame.Initialize(__TEXT("Scene:PerFrameCB"), RHIUtils::GetConstBufferSize(), 1, sizeof(mConstBufferPerFrame.Data()));
	mConstBufferSunLight.Initialize(__TEXT("Scene:SunLightCB"), RHIUtils::GetConstBufferSize(), 1, sizeof(mConstBufferSunLight.Data()));

	return true;
}

/**
 * Function : AddCamera
 */
std::shared_ptr<Camera>& Scene::AddCamera(const Camera::CameraDesc& desc, const std::string& name)
{
	mCameras.emplace_back(std::make_shared<Camera>(desc, name));

	return mCameras.back();
}

/**
 * Function : GetObjectsByShader
 */
void Scene::GetObjectsByShader(const ShaderManager::ShaderPtr& shader, SceneObjectPtrs& objects) const
{
	// ToDo: Optimize it. We can store map<RHIShaderDefinition,  ObjectsContainer> for example instead of a linear vector.
	for (const auto& modeObjects : mSceneObjects)
	{
		for (const auto& object : modeObjects.second)
		{
			if (object->GetMaterial()->GetShader() == shader)
			{
				objects.emplace_back(object.get());
			}
		}
	}
}

/**
 * Function : Tick
 */
void Scene::Tick(const float deltaTime)
{
	if (mActiveCamera)
	{
		mActiveCamera->UpdateBuffer(mConstBufferPerFrame);
		mConstBufferPerFrame.UpdateDataOnGPU();
		mConstBufferSunLight.UpdateDataOnGPU();
	}
}

/**
 * Function : Serialize
 */
void Scene::Serialize(nlohmann::json& js) const
{
	auto& jsCameras = js["cameras"];
	jsCameras = nlohmann::json::array();

	for (const auto& camera : mCameras)
	{
		auto& jsCamera = nlohmann::json::object();

		camera->Serialize(jsCamera);
		if (camera == mActiveCamera)
		{
			jsCamera["active"] = true;
		}

		jsCameras.push_back(jsCamera);
	}

	//-- ToDo: Add serialize objects.
}

/**
 * Function: Deserialize
 */
void Scene::Deserialize(nlohmann::json& js)
{
	// Setup sun light direction.
	{
		auto& jsSunDir = js["sundir"];
		mConstBufferSunLight.Data().LightDir = DirectX::XMFLOAT4(jsSunDir[0], jsSunDir[1], jsSunDir[2], 0.0f);
	}
	
	for (auto jsCamera : js["cameras"])
	{
		//-- Create a new instance.
		mCameras.emplace_back(std::make_shared<Camera>());
		CameraPtr& camera = mCameras.back();

		//-- Deserialize.
		camera->Deserialize(jsCamera);

		if (jsCamera.find("active") != jsCamera.end())
		{
			SetActiveCamera(camera);
		}
	}

	Loaders::AssimpLoader loader;
	for (auto jsObject : js["objects"])
	{
		auto& jsScale		= jsObject["scale"];
		auto& jsPos			= jsObject["pos"];
		auto& jsRotate		= jsObject["rotate"];
		auto& jsMaterial	= jsObject["material"];
		auto& jsShader		= jsObject["shader"];

		DirectX::XMFLOAT3 scale(jsScale[0], jsScale[1], jsScale[2]);
		DirectX::XMFLOAT3 pos(jsPos[0], jsPos[1], jsPos[2]);

		//-- Load a model.
		const bool batch = ((jsObject.find("batch") != jsObject.end()) ? jsObject["batch"] : false);
		String wPath = SResourceManager::Instance().Find(StringUtils::Converter::StringToWString(jsObject["path"]));
		std::string path = StringUtils::Converter::WStringToString(wPath);
		std::list<SceneObjectPtr> loadedObject;

		const std::string matName = jsMaterial;
		const bool isLoaded = loader.LoadModel(path, matName, *this, loadedObject, batch);

		//-- Setup local transformation.
		if (isLoaded)
		{
			auto& obj = loadedObject.front();

			// Setup a shader.
			obj->GetMaterial()->SetShader(GraphicsCore::rs().shaderManager().GetShader(jsShader));

			// Setup transformation.
			Transformation& transform = obj->GetLocalTransform();
			transform.Scale() = scale;
			transform.Translation() = pos;

			const DirectX::XMVECTOR axis = DirectX::XMVectorSet(jsRotate[0], jsRotate[1], jsRotate[2], 0.0f);
			const float angleInRadians = DirectX::XMConvertToRadians(jsRotate[3]);

			transform.Rotation() = DirectX::XMQuaternionRotationAxis(axis, angleInRadians);

			transform.WorldMatrix(obj->GetTransforms().WorldMatrix);
			obj->GetConstBuffer().UpdateDataOnGPU();
		}

	}
}
