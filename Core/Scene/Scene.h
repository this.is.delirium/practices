#pragma once
#include <StdafxCore.h>
#include <AutoGlobalCounter.h>
#include <Camera.h>
#include <LightSource.h>
#include <Patterns/Singleton.h>
#include <Render/ShaderManager.h>
#include "SceneObject.h"
#include <Serialize/ISerializable.h>
#include <RHI.h>

class DrawMode;

class Scene : public ISerializable
{
public:
	using SceneObjectPtrs	= std::vector<const SceneObject*>;
	using IntersectionSet	= SceneObjectPtrs;
	using Modes				= std::unordered_map<AutoGlobalCounter::Type, std::shared_ptr<DrawMode>>;
	using ModesObjectPtrs	= std::unordered_map<AutoGlobalCounter::Type, std::vector<SceneObjectPtr>>;
	using Lights			= std::vector<std::shared_ptr<LightSource>>;

public:
	bool Initialize();

	void GetObjectsByShader(const ShaderManager::ShaderPtr& shader, SceneObjectPtrs& objects) const;

	void Tick(const float deltaTime);

public:
	const ModesObjectPtrs& GetModeObjects() const { return mSceneObjects; }

	const Lights& GetLights() const { return mLights; }

	std::shared_ptr<Camera>& AddCamera(const Camera::CameraDesc& desc, const std::string& name);
	void SetActiveCamera(const std::shared_ptr<Camera>& camera) { mActiveCamera = camera; }
	std::shared_ptr<Camera>& GetActiveCamera() { return mActiveCamera; }

	const ConstBufferPerFrame& GetConstBufferPerFrame() const { return mConstBufferPerFrame; }
	ConstBufferPerFrame& GetConstBufferPerFrame() { return mConstBufferPerFrame; }

	const ConstBufferSunLight& GetConstBufferSunLight() const { return mConstBufferSunLight; }
	ConstBufferSunLight& GetConstBufferSunLight() { return mConstBufferSunLight; }

public: //! template methods.

	template<class Mode>
	void AddObject(const SceneObjectPtr& sceneObject)
	{
		std::lock_guard<std::mutex> guard(mObjectsMutex);

		mSceneObjects[Mode::kIndexType].push_back(sceneObject);
	}

	template<class Mode>
	void RegisterMode()
	{
		assert(!HasMode(Mode::kIndexType));
		mModes[Mode::kIndexType] = std::make_shared<Mode>();
	}

	template<class Mode>
	Mode& Get()
	{
		assert(HasMode(Mode::kIndexType));

		return *reinterpret_cast<Mode*>(mModes[Mode::kIndexType].get());
	}

public: //! ISerializable.
	void Serialize(nlohmann::json& js) const override final;
	void Deserialize(nlohmann::json& js) override final;

protected:
	bool HasMode(AutoGlobalCounter::Type id)
	{
		return mModes.find(id) != mModes.cend();
	}

protected:
	std::mutex				mObjectsMutex;

	Modes					mModes;
	ModesObjectPtrs			mSceneObjects;
	Lights					mLights;

	std::vector<CameraPtr>	mCameras;
	CameraPtr				mActiveCamera;
	ConstBufferPerFrame		mConstBufferPerFrame; //! ToDo: Do move it to Camera?
	ConstBufferSunLight		mConstBufferSunLight;
};
