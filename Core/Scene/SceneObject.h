#pragma once
#include "StdafxCore.h"
#include <ConstantBuffers.h>
#include <Render/Material/Material.h>
#include <RHIVertexBuffer.h>
#include <RHIIndexBuffer.h>
#include <Transformation.h>

class SceneObject
{
public:
	using Transforms = ConstantBuffers::PerObject;

public:
	SceneObject(const std::string& name);
	~SceneObject();

public:
	static std::string GetUniqueName(const std::string& name);

public:
	const MaterialRef& GetMaterial() const { return mMaterial; }
	MaterialRef& GetMaterial() { return mMaterial; }

	const Transforms& GetTransforms() const { return mConstBufferTransforms.Data(); }
	Transforms& GetTransforms() { return mConstBufferTransforms.Data(); }

	Transformation& GetLocalTransform() { return mLocalTransform; }

	const RHIVertexBuffer& GetVertexBuffer() const { return mVertexBuffer; }
	RHIVertexBuffer& GetVertexBuffer() { return mVertexBuffer;}

	const RHIIndexBuffer& GetIndexBuffer() const { return mIndexBuffer; }
	RHIIndexBuffer& GetIndexBuffer() { return mIndexBuffer; }

	const ConstBufferPerObject& GetConstBuffer() const { return mConstBufferTransforms; }
	ConstBufferPerObject& GetConstBuffer() { return mConstBufferTransforms; }

	const std::string& GetName() const { return mName; }

protected:
	static std::atomic<UINT>	kUniqueId;

	std::string					mName;
	MaterialRef					mMaterial;

	RHIVertexBuffer				mVertexBuffer;
	RHIIndexBuffer				mIndexBuffer;
	ConstBufferPerObject		mConstBufferTransforms;
	Transformation				mLocalTransform;

	RTTR_ENABLE()
	RTTR_REGISTRATION_FRIEND
};
using SceneObjectPtr = std::shared_ptr<SceneObject>;
