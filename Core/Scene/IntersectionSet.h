#pragma once
#include "StdafxCore.h"
#include "AutoGlobalCounter.h"

class SceneObject;

class IntersectionSet
{
public:
	using ModeSet = std::vector<const SceneObject*>;
	using Set = std::unordered_map<AutoGlobalCounter::Type, ModeSet>;

public:
	void AddObject(const AutoGlobalCounter::Type mode, const SceneObject* object)
	{
		mSetObjects[mode].push_back(object);
	}

	void Sort();

public: //! template methods.
	template<class Mode>
	const ModeSet& Get()
	{
		return mSetObjects[Mode::kIndexType];
	}

protected:
	Set mSetObjects;
};
