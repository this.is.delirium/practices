#pragma once
#include "StdafxCore.h"
#include "Camera.h"
#include "ConstantBuffers.h"
#include <ECS/MeshComponent.h>
#include "Render/IRenderer.h"
#include <RHI.h>
#include <RHIConstBuffer.h>

//! It is used for rendering the frustums of camera.
class ViewFrustum
{
public:
	using PerFrameBuffer = RHIConstBuffer<ConstantBuffers::Frustum::PerFrame>;

public:
	void Release();

	void Initialize(Camera* cameraForFrustum, Camera* cameraForView);
	void UpdateMesh();

	void Update(const float deltaTime);
	void Draw(RHICommandList* commandList, const int split);
	void DrawAll(RHICommandList* commandList);

public:
	FORCEINLINE void ChangeViewCamera(Camera* camera) { mCameraForView = camera; }

	FORCEINLINE const Camera* FrustumCamera() const { return mCameraForFrustum; }

	FORCEINLINE size_t NbOfSplit() const { return mCameraForFrustum->NbOfFrustums(); }

public:
	const PerFrameBuffer& ConstBufferPerFrame() const { return mConstBufferPerFrame; }

protected:
	Camera*							mCameraForFrustum;
	Camera*							mCameraForView;

	//std::shared_ptr<MeshComponent>	mMeshComponent;
	RHIVertexBuffer					mVertexBuffer;
	RHIIndexBuffer					mIndexBuffer;

	PerFrameBuffer					mConstBufferPerFrame;
};
