#pragma once
#include "StdafxCore.h"
#include <ProgressIndicator.h>

class HeightMapGeneratorAlgo
{
public:
	virtual ~HeightMapGeneratorAlgo() { }

	virtual void Apply(const DirectX::XMUINT2& sizeMap, std::vector<float>& map, ProgressIndicator* progress = nullptr) const = 0;

protected:
	//! Returns pseudo-random number from -1.0 to 1.0.
	FORCEINLINE float Noise(uint32_t value) const
	{
		value = (value << 13) ^ value;
		return (1.0f - ((value * (value * value * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);
	}
protected:
};
