﻿#include "StdafxCore.h"
#include "StaticGeometryBuilder.h"
#include "Camera.h"
#include <ECS/MeshComponent.h>
#include <ECS/SceneComponent.h>
#include "InputLayouts.h"
#include "Render/IRenderer.h"
#include <Scene/SceneObject.h>

using MainVertex	= InputLayouts::Main::Vertex;
using FrustumVertex	= InputLayouts::Frustum::Vertex;

#include <D3D12/private/DXMathHelper.h>
using namespace DirectX;

namespace
{
	FORCEINLINE float Pi() { return DirectX::XM_PI; }

	void CalculateTangentBitangent(MainVertex& out, const MainVertex& p1, const MainVertex& p2)
	{
		const XMFLOAT3 edge1	= p1.Position - out.Position;
		const XMFLOAT3 edge2	= p2.Position - out.Position;
		const XMFLOAT2 deltaUV1	= p1.TextureUV - out.TextureUV;
		const XMFLOAT2 deltaUV2	= p2.TextureUV - out.TextureUV;

		const float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);
		out.Tangent = XMFLOAT3(
			f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x),
			f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y),
			f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z)
		);
		out.Bitangent = XMFLOAT3(
			f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x),
			f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y),
			f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z)
		);

		out.Tangent		= XMFloat3Normalize(out.Tangent);
		out.Bitangent	= XMFloat3Normalize(out.Bitangent);
	}
}

/**
 * Function : LoadPlane
 */
void StaticGeometryBuilder::LoadPlane(std::shared_ptr<MeshComponent>& mesh, std::shared_ptr<SceneComponent>& sceneComponent)
{
	MainVertex vertices[6];

	vertices[0].Position = XMFLOAT3(-1.0f, 1.0f, 0.0f);	vertices[0].Normal = XMFLOAT3(0.0f, 0.0f, 1.0f); vertices[0].TextureUV = XMFLOAT2(0.0f, 1.0f);
	vertices[1].Position = XMFLOAT3(-1.0f, -1.0f, 0.0f);vertices[1].Normal = XMFLOAT3(0.0f, 0.0f, 1.0f); vertices[1].TextureUV = XMFLOAT2(0.0f, 0.0f);
	vertices[2].Position = XMFLOAT3(1.0f, -1.0f, 0.0f);	vertices[2].Normal = XMFLOAT3(0.0f, 0.0f, 1.0f); vertices[2].TextureUV = XMFLOAT2(1.0f, 0.0f);
	vertices[3].Position = vertices[0].Position;		vertices[3].Normal = XMFLOAT3(0.0f, 0.0f, 1.0f); vertices[3].TextureUV = XMFLOAT2(0.0f, 1.0f);
	vertices[4].Position = vertices[2].Position;		vertices[4].Normal = XMFLOAT3(0.0f, 0.0f, 1.0f); vertices[4].TextureUV = XMFLOAT2(1.0f, 0.0f);
	vertices[5].Position = XMFLOAT3(1.0f, 1.0f, 0.0f);	vertices[5].Normal = XMFLOAT3(0.0f, 0.0f, 1.0f); vertices[5].TextureUV = XMFLOAT2(1.0f, 1.0f);

	RHIBatchDesc batchDesc	= { vertices, sizeof(vertices), sizeof(MainVertex), nullptr, 0, RHIFormat::Unknown, RHIPrimitiveTopology::TriangleList };
	batchDesc.VertexPartition.emplace_back(0, _countof(vertices));

	mesh.reset(BaseComponent::CreateSubobject<MeshComponent>());
	mesh->Initialize(batchDesc);
	sceneComponent.reset(BaseComponent::CreateSubobject<SceneComponent>());
}

/**
 * Function : LoadPlane
 */
void StaticGeometryBuilder::LoadPlane(SceneObject& obj)
{
	MainVertex vertices[4];

	vertices[0].Position = XMFLOAT3(-1.0f, 1.0f, 0.0f);	vertices[0].Normal = XMFLOAT3(0.0f, 0.0f, 1.0f); vertices[0].TextureUV = XMFLOAT2(0.0f, 1.0f);
	vertices[1].Position = XMFLOAT3(-1.0f, -1.0f, 0.0f);vertices[1].Normal = XMFLOAT3(0.0f, 0.0f, 1.0f); vertices[1].TextureUV = XMFLOAT2(0.0f, 0.0f);
	vertices[2].Position = XMFLOAT3(1.0f, -1.0f, 0.0f);	vertices[2].Normal = XMFLOAT3(0.0f, 0.0f, 1.0f); vertices[2].TextureUV = XMFLOAT2(1.0f, 0.0f);
	vertices[3].Position = XMFLOAT3(1.0f, 1.0f, 0.0f);	vertices[3].Normal = XMFLOAT3(0.0f, 0.0f, 1.0f); vertices[3].TextureUV = XMFLOAT2(1.0f, 1.0f);

	CalculateTangentBitangent(vertices[0], vertices[1], vertices[2]);
	CalculateTangentBitangent(vertices[3], vertices[0], vertices[2]);

	CalculateTangentBitangent(vertices[1], vertices[0], vertices[2]);
	CalculateTangentBitangent(vertices[2], vertices[1], vertices[0]);

	const std::wstring wname = StringUtils::Converter::StringToWString(obj.GetUniqueName("SceneObject"));
	obj.GetVertexBuffer().Initialize(wname + __TEXT(":VB"), sizeof(vertices), _countof(vertices), sizeof(MainVertex), vertices);

	int indices[6] = { 0, 1, 2, 0, 2, 3 };
	obj.GetIndexBuffer().Initialize(wname + __TEXT(":IB"), sizeof(indices), _countof(indices), sizeof(int), indices);
}

/**
 * Function : LoadCube
 */
void StaticGeometryBuilder::LoadCube(std::shared_ptr<MeshComponent>& mesh, std::shared_ptr<SceneComponent>& sceneComponent)
{
	MainVertex vertices[] =
	{
		{ { 0.5f,  0.5f,  0.5f },{ 0.0f, 0.0f,  1.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, -1.0f, 0.0f },{ 0.0f, 0.0f } },
		{ { 0.5f, -0.5f,  0.5f },{ 0.0f, 0.0f,  1.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, -1.0f, 0.0f },{ 0.0f, 1.0f } },
		{ { -0.5f,  0.5f,  0.5f },{ 0.0f, 0.0f,  1.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, -1.0f, 0.0f },{ 1.0f, 0.0f } },
		{ { -0.5f, -0.5f,  0.5f },{ 0.0f, 0.0f,  1.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, -1.0f, 0.0f },{ 1.0f, 1.0f } },

		// front face -Z
		{ { 0.5f,  0.5f, -0.5f },{ 0.0f, 0.0f, -1.0f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, -1.0f, 0.0f },{ 0.0f, 0.0f } },
		{ { 0.5f, -0.5f, -0.5f },{ 0.0f, 0.0f, -1.0f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, -1.0f, 0.0f },{ 0.0f, 1.0f } },
		{ { -0.5f,  0.5f, -0.5f },{ 0.0f, 0.0f, -1.0f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, -1.0f, 0.0f },{ 1.0f, 0.0f } },
		{ { -0.5f, -0.5f, -0.5f },{ 0.0f, 0.0f, -1.0f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, -1.0f, 0.0f },{ 1.0f, 1.0f } },

		// bottom face -Y
		{ { -0.5f, -0.5f,  0.5f },{ 0.0f, -1.0f, 0.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 1.0f, 0.0f } },
		{ { 0.5f, -0.5f,  0.5f },{ 0.0f, -1.0f, 0.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 0.0f, 0.0f } },
		{ { 0.5f, -0.5f, -0.5f },{ 0.0f, -1.0f, 0.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 0.0f, 1.0f } },
		{ { -0.5f, -0.5f, -0.5f },{ 0.0f, -1.0f, 0.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 1.0f, 1.0f } },

		// top face +Y
		{ { -0.5f,  0.5f,  0.5f },{ 0.0f,  1.0f, 0.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 0.0f, 0.0f } },
		{ { 0.5f,  0.5f,  0.5f },{ 0.0f,  1.0f, 0.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 1.0f, 0.0f } },
		{ { 0.5f,  0.5f, -0.5f },{ 0.0f,  1.0f, 0.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 1.0f, 1.0f } },
		{ { -0.5f,  0.5f, -0.5f },{ 0.0f,  1.0f, 0.0f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 0.0f, 1.0f } },

		// left face -X
		{ { -0.5f,  0.5f,  0.5f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 0.0f, -1.0f, 0.0f },{ 0.0f, 0.0f } },
		{ { -0.5f, -0.5f,  0.5f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 0.0f, -1.0f, 0.0f },{ 0.0f, 1.0f } },
		{ { -0.5f, -0.5f, -0.5f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 0.0f, -1.0f, 0.0f },{ 1.0f, 1.0f } },
		{ { -0.5f,  0.5f, -0.5f },{ -1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, -1.0f },{ 0.0f, -1.0f, 0.0f },{ 1.0f, 0.0f } },

		// right face +X
		{ { 0.5f,  0.5f,  0.5f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, 1.0f },{ 0.0f, -1.0f, 0.0f },{ 1.0f, 0.0f } },
		{ { 0.5f, -0.5f,  0.5f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, 1.0f },{ 0.0f, -1.0f, 0.0f },{ 1.0f, 1.0f } },
		{ { 0.5f, -0.5f, -0.5f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, 1.0f },{ 0.0f, -1.0f, 0.0f },{ 0.0f, 1.0f } },
		{ { 0.5f,  0.5f, -0.5f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f, 1.0f },{ 0.0f, -1.0f, 0.0f },{ 0.0f, 0.0f } }
	};

	uint32_t indices[] =
	{
		//back
		0, 2, 1,
		1, 2, 3,
		//front +4
		4, 5, 7,
		4, 7, 6,
		//left +8
		9, 8, 10,
		10, 8, 11,
		//right +12
		13, 14, 12,
		14, 15, 12,
		//front +16
		17, 16, 19,
		18, 17, 19,
		//back +20
		21, 23, 20,
		22, 23, 21
	};

	RHIBatchDesc desc = { vertices, sizeof(vertices), sizeof(MainVertex), indices, sizeof(indices), RHIFormat::R32Uint, RHIPrimitiveTopology::TriangleList };
	desc.VertexPartition.emplace_back(0, _countof(vertices));
	desc.IndexPartition.emplace_back(0, _countof(indices));

	mesh.reset(BaseComponent::CreateSubobject<MeshComponent>());
	mesh->Initialize(desc);
	sceneComponent.reset(BaseComponent::CreateSubobject<SceneComponent>());
}

namespace
{
	void FromSphericalCoords(MainVertex& vertex, const float theta, const float phi)
	{
		vertex.Position.x = sinf(theta) * cosf(phi);
		vertex.Position.y = sinf(theta) * sinf(phi);
		vertex.Position.z = cosf(phi);

		vertex.Normal = vertex.Position;
	}
}

namespace
{
	XMFLOAT3 GetSphericalCoords(const XMFLOAT3& position)
	{
		XMFLOAT3 result;
		
		result.x = XMVectorGetX(XMVector3Length(XMLoadFloat3(&position))); //glm::length(position);
		result.y = acos(position.y / result.x) / XM_PI;
		result.z = (atan2(position.z, position.x) / XM_PI + 1.0f) * 0.5f;

		return result;
	}
}
/**
 * Function : LoadSphere
 */
void StaticGeometryBuilder::LoadSphere(std::shared_ptr<MeshComponent>& mesh, std::shared_ptr<SceneComponent>& sceneComponent, const uint32_t nbOfThetaSplit, const uint32_t nbOfPhiSplit)
{
	if (nbOfThetaSplit < 2u || nbOfPhiSplit < 2u)
	{
		SLogger::Instance().AddMessage(LogMessageType::Warning, __TEXT("Wrong parameters for creation a sphere: stacks = %d, slices = %d"), nbOfThetaSplit, nbOfPhiSplit);
		return;
	}

	const float R		= 1.0f / (nbOfThetaSplit - 1.0f);
	const float S		= 1.0f / (nbOfPhiSplit - 1.0f);
	const float radius	= 1.0f;
	uint32_t ring, sector;

	std::vector<MainVertex>	vertices;
	std::vector<uint32_t>	indices;

	MainVertex vertex;
	for (ring = 0; ring < nbOfThetaSplit; ++ring)
	{
		for (sector = 0; sector < nbOfPhiSplit; ++sector)
		{
			const float lSinTheta = sinf(Pi() * ring * R);
			const float lPhi = 2.0f * Pi() * sector * S;

			vertex.Position.x = cosf(lPhi) * lSinTheta * radius;
			vertex.Position.y = sinf(lPhi) * lSinTheta * radius;
			vertex.Position.z = cosf(Pi() * ring * R) * radius;

			vertex.Normal = vertex.Position;

			vertex.TextureUV = XMFLOAT2(sector * S, ring * R);

			vertices.push_back(vertex);
		}
	}

	for (ring = 0; ring < nbOfThetaSplit - 1; ++ring)
	{
		for (sector = 0; sector < nbOfPhiSplit; ++sector)
		{
			{
				const uint32_t idx0 = (ring + 0) * nbOfPhiSplit + (sector + 0);
				const uint32_t idx1 = (ring + 1) * nbOfPhiSplit + (sector + 1);
				const uint32_t idx2 = (ring + 1) * nbOfPhiSplit + (sector + 0);

				// Push indices.
				indices.push_back(idx0);
				indices.push_back(idx1);
				indices.push_back(idx2);
			}

			{
				const uint32_t idx0 = (ring + 0) * nbOfPhiSplit + (sector + 0);
				const uint32_t idx1 = (ring + 1) * nbOfPhiSplit + (sector + 1);
				const uint32_t idx2 = (ring + 0) * nbOfPhiSplit + (sector + 1);

				// Push indeices.
				indices.push_back(idx0);
				indices.push_back(idx1);
				indices.push_back(idx2);
			}
		}
	}

	RHIBatchDesc desc = { vertices.data(), sizeof(MainVertex) * vertices.size(), sizeof(MainVertex), indices.data(), indices.size() * sizeof(uint32_t), RHIFormat::R32Uint, RHIPrimitiveTopology::TriangleList };
	desc.VertexPartition.emplace_back(0, vertices.size());
	desc.IndexPartition.emplace_back(0, indices.size());

	mesh.reset(BaseComponent::CreateSubobject<MeshComponent>());
	mesh->Initialize(desc);
	sceneComponent.reset(BaseComponent::CreateSubobject<SceneComponent>());
}

namespace
{
	// clip-space cube.
	XMFLOAT4 csCube[8] =
	{
		XMFLOAT4(-1.0f, -1.0f, 0.0f, 1.0f),
		XMFLOAT4(1.0f, -1.0f, 0.0f, 1.0f),
		XMFLOAT4(-1.0f, 1.0f, 0.0f, 1.0f),
		XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f),

		XMFLOAT4(-1.0f, -1.0f, 1.0f, 1.0f),
		XMFLOAT4(1.0f, -1.0f, 1.0f, 1.0f),
		XMFLOAT4(-1.0f, 1.0f, 1.0f, 1.0f),
		XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f),
	};

	float UniformSplit(const float zNear, const float zFar, const int part, const int nbOfSplit, const float /*lambda*/)
	{
		return zNear + (zFar - zNear) * part / nbOfSplit;
	}

	float LogSplit(const float zNear, const float zFar, const int part, const int nbOfSplit, const float /*lambda*/)
	{
		return zNear * std::powf(zFar / zNear, part / static_cast<float>(nbOfSplit));
	}

	float PracticalSplit(const float zNear, const float zFar, const int part, const int nbOfSplit, const float lambda)
	{
		return lambda * UniformSplit(zNear, zFar, part, nbOfSplit, lambda) + (1.0f - lambda) * LogSplit(zNear, zFar, part, nbOfSplit, lambda);
	}

	using SplitScheme = std::function<float(const float, const float, const int, const int, const float)>;

	void BuildPositionInWorldSpace(XMFLOAT4& worldPosition,  const XMFLOAT4& csPosition, const XMMATRIX& invProj, const XMMATRIX& invView)
	{
		XMVECTOR vecPos;

		worldPosition = csPosition;

		vecPos = XMLoadFloat4(&worldPosition);
		vecPos = XMVector4Transform(vecPos, invProj);
		vecPos.m128_f32[0] /= vecPos.m128_f32[3];
		vecPos.m128_f32[1] /= vecPos.m128_f32[3];
		vecPos.m128_f32[2] /= vecPos.m128_f32[3];
		vecPos.m128_f32[3] /= vecPos.m128_f32[3];

		vecPos = XMVector4Transform(vecPos, invView);
		XMStoreFloat4(&worldPosition, vecPos);
	}

	void BuildPositionsInViewSpace(Camera::Frustum& frustum, const Camera* lightCamera)
	{
		constexpr float fMin = std::numeric_limits<float>::lowest();
		constexpr float fMax = std::numeric_limits<float>::max();

		frustum.LightView = XMMatrixLookToRH(XMLoadFloat4(&frustum.Center), XMLoadFloat4(&lightCamera->Direction()), XMLoadFloat4(&lightCamera->UpVector()));

		XMVECTOR vecPos;
		XMFLOAT3 min(fMax, fMax, fMax);
		XMFLOAT3 max(fMin, fMin, fMin);

		for (int i = 0; i < 8; ++i)
		{
			XMFLOAT4& viewPos = frustum.ViewPosition(i);

			vecPos = XMLoadFloat4(&frustum.WorldPosition(i));
			vecPos = XMVector4Transform(vecPos, frustum.LightView);

			XMStoreFloat4(&viewPos, vecPos);

			min = Min(viewPos, min);
			max = Max(viewPos, max);
		}

		frustum.Projection = XMMatrixOrthographicOffCenterRH(min.x, max.x, min.y, max.y, min.z, max.z);
	}
}

/**
 * Function : BuildUniformFrustumMesh
 */
void StaticGeometryBuilder::BuildFrustums(Camera* camera, const Camera* lightCamera, const int nbOfSplit, const DirectX::XMFLOAT4* colors, const TypeSplitScheme scheme, const float lambda)
{
	Camera helpCamera(*camera);

	// Save old z-range of camera.
	const XMFLOAT2 oldZRange = helpCamera.ZRange();

	SplitScheme splitScheme;
	{
		using namespace std::placeholders;

		switch (scheme)
		{
			case TypeSplitScheme::Uniform:
			{
				splitScheme = std::bind(&UniformSplit, _1, _2, _3, _4, _5);
				break;
			}

			case TypeSplitScheme::Log:
			{
				splitScheme = std::bind(&LogSplit, _1, _2, _3, _4, _5);
				break;
			}

			case TypeSplitScheme::Practical:
			{
				splitScheme = std::bind(&PracticalSplit, _1, _2, _3, _4, _5);
				break;
			}
		}
	}

	// Create ranges.
	std::vector<XMFLOAT2> ranges(nbOfSplit);
	for (int i = 0; i < nbOfSplit; ++i)
	{
		ranges[i].x = splitScheme(oldZRange.x, oldZRange.y, i, nbOfSplit, lambda);
		ranges[i].y = splitScheme(oldZRange.x, oldZRange.y, i + 1, nbOfSplit, lambda);
	}

	constexpr float fMin = std::numeric_limits<float>::lowest();
	constexpr float fMax = std::numeric_limits<float>::max();

	XMFLOAT3 min(fMax, fMax, fMax);
	XMFLOAT3 max(fMin, fMin, fMin);

	for (int split = 0; split < nbOfSplit; ++split)
	{
		helpCamera.SetZRange(ranges[split]);

		const XMMATRIX& invProj = helpCamera.InverseProjection();
		const XMMATRIX& invView = helpCamera.InverseLookAt();

		Camera::Frustum& frustum	= camera->GetFrustum(split);
		frustum.ZRange				= ranges[split];
		frustum.Color				= colors[split];
		for (int i = 0; i < 8; ++i)
		{
			BuildPositionInWorldSpace(frustum.WorldPosition(i), csCube[i], invProj, invView);

			min = Min(frustum.WorldPosition(i), min);
			max = Max(frustum.WorldPosition(i), max);
		}

		frustum.Center = MakeFloat4((min + max) * 0.5, 1.0f);

		if (lightCamera)
			BuildPositionsInViewSpace(frustum, lightCamera);
	}

	// Restore the ZRange.
	helpCamera.SetZRange(oldZRange);
}

/**
 * Function : BuildFullScreenQuad
 */
void StaticGeometryBuilder::BuildFullScreenQuad(RHIVertexBuffer* buffer, const String& name)
{
	using Vertex = InputLayouts::ScreenQuadWithoutMatrix::Vertex;

	const Vertex vertices[] =
	{
		{ XMFLOAT2(-1.0f, 1.0f) },
		{ XMFLOAT2(-1.0f, -1.0f) },
		{ XMFLOAT2(1.0f, -1.0f) },

		{ XMFLOAT2(-1.0f, 1.0f) },
		{ XMFLOAT2(1.0f, -1.0f) },
		{ XMFLOAT2(1.0f, 1.0f) }
	};

	buffer->Initialize(name, sizeof(vertices), _countof(vertices), sizeof(Vertex), vertices);
}
