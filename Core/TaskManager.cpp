#include "StdafxCore.h"
#include "TaskManager.h"

/**
 * Function : TaskManager
 */
TaskManager::TaskManager()
	: mTasks(), mMutex(), mConditionVar()
{
	//
}

/**
 * Function : TaskManager
 */
TaskManager::~TaskManager()
{
	//
}

/**
 * Function : Add
 */
void TaskManager::Add(Task* task)
{
	std::lock_guard<std::mutex> guard(mMutex);

	mTasks.push(task);

	mConditionVar.notify_one();
}

/**
 * Function : Run
 */
void TaskManager::Run()
{
	std::unique_lock<std::mutex> lock(mMutex);
	while (mTasks.empty())
	{
		mConditionVar.wait(lock);
	}

	Task* task = mTasks.front();
	task->Execute();

	{
		std::lock_guard<std::mutex> deviceLock(mDeviceMutex);

		DeviceTask* deviceTask = task->SpawnDeviceStuffTask();
		if (deviceTask)
		{
			mDeviceTasks.push(deviceTask);
		}
	}

	delete task;
	mTasks.pop();
}
