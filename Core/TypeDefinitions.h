#pragma once

#ifdef UNICODE
using Ofstream		= std::wofstream;
using Ifstream		= std::wifstream;
using StringStream	= std::wstringstream;
using String		= std::wstring;
using CConstString	= const wchar_t*;
using CString		= wchar_t*;
using ConstChar		= const wchar_t;
using Char			= wchar_t;

#define vsprintfs	vswprintf_s
#define StrToInt	_wtoi
#define fileopen	_wfopen
#define StrCmp		wcscmp
#define ToString	std::to_wstring

#ifdef _WIN32
#define GetCurrentDir _wgetcwd
#endif

#else
using Ofstream		= std::ofstream;
using Ifstream		= std::ifstream;
using StringStream	= std::stringstream;
using String		= std::string;
using CConstString	= const char*;
using CString		= char*;
using ConstChar		= const wchar;
using Char			= wchar;

#define T(quote)	quote
#define vsprintfs	vsprintf_s
#define StrToInt	atoi
#define fileopen	fopen
#define StrCmp		strcmp
#define ToString	std::to_string

#ifdef _WIN32
#define GetCurrentDir _getcwd
#endif

#endif

#if _MSC_VER >= 1900
	template <typename E>
	constexpr typename std::underlying_type<E>::type ToInt(E e) {
		return static_cast<typename std::underlying_type<E>::type>(e);
	}

	template<typename E>
	constexpr typename std::underlying_type<E>::type ToUnderType(E e)
	{
		return static_cast<typename std::underlying_type<E>::type>(e);
	}

#elif _MSC_VER <= 1800
	#define ToInt(E) static_cast<int>(E);
	#define ToUint(E) static_cast<UINT>(E);
#endif
