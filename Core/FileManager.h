#pragma once
#include "StdafxCore.h"
#include "Patterns/Singleton.h"

#ifdef _MSC_VER
	using Handle = HANDLE;
#else

#endif

class FindData
{
public:
	CConstString FileName();

	bool IsDirectory() const;

public:
#ifdef _MSC_VER
	WIN32_FIND_DATA Data;
#else

#endif
};

class FileManager
{
public:
	FileManager() { }

	void FindFilesInFolder(const String& startFolderRegExp, std::vector<FindData>& result);

public:
	Handle Find(const String& fileName, FindData& outData);
	bool NextFile(Handle handle, FindData& outData);
	void Close(Handle handle);
	bool CheckHandle(Handle handle);

	bool IsExist(const String& path);
	bool CreateFolder(const String& folder);
	bool DeleteFolder(const String& folder);

	//! Returns the number of deleted objects.
	size_t ClearFolder(const String& path, const bool isRecursive = false);

	std::string GetFileName(const std::string& path) const;
	std::string RemoveExtension(const std::string& path) const;
	std::string ParentPath(const std::string& path) const;

protected:
	FileManager(const FileManager&) = delete;
	FileManager& operator=(const FileManager&) = delete;

	friend class Singleton<FileManager>;
};

using SFileManager = Singleton<FileManager>;
