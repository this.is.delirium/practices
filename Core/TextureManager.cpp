#include "StdafxCore.h"
#include "TextureManager.h"

/**
 * Function : GetTexture
 */
TextureRef TextureManager::GetTexture(const String& absolutePath)
{
	TextureRef texture(nullptr);

	if (mTextures.find(absolutePath) == mTextures.cend())
	{
		texture = LoadTexture(absolutePath);
	}

	return texture;
}

/**
 * Function : LoadTexture
 */
TextureRef TextureManager::LoadTexture(const String& absolutePath)
{
	TextureRef texture = std::make_shared<RHITexture>();
	texture->InitializeByPath(absolutePath);

	// ToDo: Add validation.

	mTextures[absolutePath] = texture;

	return texture;
}
