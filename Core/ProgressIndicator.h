#pragma once
#include "StdafxCore.h"

class ProgressIndicator
{
public:
	ProgressIndicator()
		: mMinValue(0), mMaxValue(1), mCurrentValue(0), mStep(1) { }

	ProgressIndicator(const int32_t minValue, const int32_t maxValue, const int32_t startValue = 0, const int32_t step = 1)
		: mMinValue(minValue), mMaxValue(maxValue), mCurrentValue(startValue), mStep(step) { }

	virtual ~ProgressIndicator() { }

	int32_t MinValue() const { return mMinValue; }
	int32_t MaxValue() const { return mMaxValue; }
	virtual int32_t CurrentValue() const { return mCurrentValue; }
	virtual float FloatCurrentValue() const { return static_cast<float>(mCurrentValue - mMinValue) / static_cast<float>(mMaxValue - mMinValue); }
	int32_t Step() const { return mStep; }

	int32_t& CurrentValue() { return mCurrentValue; } // For ImGui

	void SetMinValue(const int32_t minValue) { mMinValue = minValue; }
	void SetMaxValue(const int32_t maxValue) { mMaxValue = maxValue; }
	void SetCurrentValue(const int32_t currentValue) { mCurrentValue = currentValue; }
	void SetStep(const int32_t step) { mStep = step; }

	virtual void Next() { mCurrentValue += mStep; }
protected:
	int32_t	mMinValue;
	int32_t	mMaxValue;
	int32_t	mCurrentValue;
	int32_t	mStep;
};
