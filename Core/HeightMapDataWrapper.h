#pragma once
#include "StdafxCore.h"
/*
template<class T>
class HeightMapDataWrapper
{
public:
	virtual const T& operator[] (const uint32_t x, const uint32_t y) const = 0;
	virtual T& operator[] (const uint32_t x, const uint32_t y) = 0;

	virtual const T& operator[] (const uint32_t index) const = 0;
	virtual T& operator[] (const uint32_t index) = 0;

	virtual const T* Data() const = 0;
	virtual T* Data() = 0;
};

template<class T>
class HeightMapDataVector : public HeightMapDataWrapper<T>
{
public:
	HeightMapDataVector(const glm::uvec2& size)
		: mSize(size)
	{
		mValues.reserve(mSize.x * mSize.y);
	}

	FORCEINLINE const T& operator[] (const uint32_t x, const uint32_t y) const override final { return mValues[mSize.x * y + x]; }
	FORCEINLINE T& operator[] (const uint32_t x, const uint32_t y) override final { return mValues[mSize.x * y + x]; }

	FORCEINLINE const T& operator[] (const uint32_t index) const override final { return mValues[index]; }
	FORCEINLINE T& operator[] (const uint32_t index) override final { return mValues[index]; }

	const T* Data() const override final { return mValues.data(); }
	T* Data() override final { return mValues.data(); }

protected:
	std::vector<T>	mValues;
	glm::uvec2		mSize;
};
*/