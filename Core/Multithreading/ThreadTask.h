#pragma once
#include "StdafxCore.h"
#include "IThreadTask.h"

/**
 * Func - callable type.
 */
template<class Func>
class ThreadTask : public IThreadTask
{
public:
	ThreadTask(Func&& func)
		: mFunc(std::move(func)) { }

	~ThreadTask() override = default;

	ThreadTask(const ThreadTask&) = delete;
	ThreadTask& operator=(const ThreadTask&) = delete;

	ThreadTask(ThreadTask&&) = default;
	ThreadTask& operator=(ThreadTask&&) = default;

	FORCEINLINE void Execute() override { mFunc(); }

protected:
	Func mFunc;
};
