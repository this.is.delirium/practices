#pragma once
#include "StdafxCore.h"

/**
 * It is a wrapper around a std::future that adds the behavior of futures returned from std::async.
 * Specifically, this object will block and wait for execution to finish before going out of scope.
 */
template<typename T>
class TaskFuture
{
public:
	TaskFuture(std::future<T>&& future)
		: mFuture(std::move(future)) { }

	~TaskFuture()
	{
		if (mFuture.valid())
			mFuture.get();
	}

	TaskFuture(const TaskFuture&) = delete;
	TaskFuture& operator=(const TaskFuture&) = delete;

	TaskFuture(TaskFuture&&) = default;
	TaskFuture& operator=(TaskFuture&&) = default;

	FORCEINLINE auto get() { return mFuture.get(); }

protected:
	std::future<T> mFuture;
};
