#pragma once
#include "StdafxCore.h"
#include "LoadingQueue.h"
#include "LoadingStack.h"
#include "TaskFuture.h"
#include "ThreadTask.h"
#include <Patterns/Singleton.h>

//! Keeps the set of threads constantly waiting to execute incoming job.
template<template<class> class Container>
class ThreadPool
{
public:
	explicit ThreadPool(const uint32_t numThreads = std::max(std::thread::hardware_concurrency(), 2u) - 1)
		: mIsDone(false)
	{
		try
		{
			for (uint32_t i = 0; i < numThreads; ++i)
				mThreads.emplace_back(&ThreadPool::Worker, this);
		}
		catch (...)
		{
			Destroy();
			throw;
		}
	}

	ThreadPool(const ThreadPool&) = delete;
	ThreadPool& operator=(const ThreadPool&) = delete;

	~ThreadPool() { Destroy(); }

	template <typename Func, typename... Args>
	auto Submit(Func&& func, Args&&... args)
	{
		auto boundTask = std::bind(std::forward<Func>(func), std::forward<Args>(args)...);
		using ResultType = std::result_of_t<decltype(boundTask)()>;
		using PackagedTask = std::packaged_task<ResultType()>;
		using TaskType = ThreadTask<PackagedTask>;

		PackagedTask task(std::move(boundTask));
		TaskFuture<ResultType> result(task.get_future());
		mContainer.Push(std::make_unique<TaskType>(std::move(task)));
		return result;
	}

	template <typename Func, typename... Args>
	void SubmitDelay(Func&& func, Args&&... args)
	{
		auto boundTask = std::bind(std::forward<Func>(func), std::forward<Args>(args)...);
		using ResultType = std::result_of_t<decltype(boundTask)()>;
		using PackagedTask = std::packaged_task<ResultType()>;
		using TaskType = ThreadTask<PackagedTask>;

		PackagedTask task(std::move(boundTask));
		mContainer.PushDelay(std::make_unique<TaskType>(std::move(task)));
	}

	void NotifyAllThreads()
	{
		mContainer.NotifyAll();
	}

	bool IsEmpty()
	{
		return mContainer.IsEmpty();
	}

protected:
	//! Invalidates the queue and joins all running threads.
	void Destroy()
	{
		mIsDone = true;
		mContainer.Invalidate();

		for (auto& thread : mThreads)
			if (thread.joinable())
				thread.join();
	}

	//! Constantly running function each thread uses to acquire work with items from the queue.
	void Worker()
	{
		while (!mIsDone)
		{
			std::unique_ptr<IThreadTask> task(nullptr);

			if (mContainer.WaitPop(task))
				if (task->IsReady())
					task->Execute();
		}
	}

protected:
	std::atomic_bool						mIsDone;
	Container<std::unique_ptr<IThreadTask>>	mContainer;
	std::vector<std::thread>				mThreads;
};
