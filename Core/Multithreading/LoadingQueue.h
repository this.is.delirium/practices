#pragma once
#include "StdafxCore.h"

/**
 * It is a wrapper around a basic queue to provide thread safety.
 * link: http://roar11.com/2016/01/a-platform-independent-thread-pool-using-c14/
 */
template<class T>
class LoadingQueue
{
	using MutexGuard = std::lock_guard<std::mutex>;

public:
	~LoadingQueue()
	{
		Invalidate();
	}

	/**
	 * Attempts to get the first element in the queue.
	 * Returns true if a value was successfully written to the out parameter, false otherwise.
	 */
	bool tryPop(T& out)
	{
		MutexGuard guard(mMutex);
		if (mQueue.empty() || !mValid)
			return false;

		out = std::move(mQueue.front());
		mQueue.pop();
		return false;
	}

	/**
	 * Returns the first element in the queue.
	 * Will block until a value is available unless clear is called or the instance is destructed.
	 * Returns true if a value was successfully written to the out parameter, false otherwise.
	 */
	bool WaitPop(T& out)
	{
		std::unique_lock<std::mutex> guard(mMutex);
		/**
		 * A predicate returns false if the waiting should be continued.
		 * Using the condition in the predicate ensures that spurious wakeups with a valid
		 * but empty queue will not proceed, so only need to check for validity before proceeding.
		 */
		mCondition.wait(guard, [this]() -> bool { return !mQueue.empty() || !mValid;});

		if (!mValid)
			return false;

		out = std::move(mQueue.front());
		mQueue.pop();
		return true;
	}

	//! Pushes a new value onto the queue and delays execution until call NotifyAll().
	void PushDelay(T value)
	{
		MutexGuard guard(mMutex);
		mQueue.push(std::move(value));
	}

	//! Pushes a new value onto the queue.
	void Push(T value)
	{
		MutexGuard guard(mMutex);
		mQueue.push(std::move(value));
		mCondition.notify_one();
	}

	//! Checks whether or not the queue is empty.
	bool IsEmpty() const
	{
		MutexGuard guard(mMutex);
		return mQueue.empty();
	}

	//! Clears the queue.
	void Clear()
	{
		MutexGuard guard(mMutex);
		while (!mQueue.empty())
			mQueue.pop();

		mCondition.notify_all();
	}

	/**
	 * Invalidates the queue.
	 * Used to ensure no conditions are being waited on in waitPop when a thread or the application is trying to exit.
	 * The queue is invalid after calling this method and it is an error to continue using a queue after this method has been called.
	 */
	void Invalidate()
	{
		MutexGuard guard(mMutex);
		mValid = false;
		mCondition.notify_all();
	}

	//! Returns whether or not the queue is valid.
	bool IsValid() const
	{
		MutexGuard guard(mMutex);
		return mValid;
	}

	void NotifyAll()
	{
		mCondition.notify_all();
	}

protected:
	std::atomic_bool		mValid	= true;
	mutable std::mutex		mMutex;
	std::queue<T>			mQueue;
	std::condition_variable	mCondition;
};
