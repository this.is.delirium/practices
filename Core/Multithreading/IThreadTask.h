#pragma once
#include "StdafxCore.h"

class IThreadTask
{
public:
	IThreadTask() = default;
	virtual ~IThreadTask() = default;

	IThreadTask(const IThreadTask&) = delete;
	IThreadTask& operator=(const IThreadTask&) = delete;

	IThreadTask(IThreadTask&&) = default;
	IThreadTask& operator=(IThreadTask&&) = default;

	//! Runs the task.
	virtual void Execute() = 0;

	virtual bool IsReady() const { return true; }
};
