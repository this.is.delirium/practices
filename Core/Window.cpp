#include "StdafxCore.h"
#include "Window.h"

#include <windowsx.h>

std::map<HWND, Window*> Window::mWindows;

/**
 * Function : ~Window
 */
Window::~Window()
{
	assert(mHwnd == nullptr);
}

/**
 * Function : Release
 */
void Window::Release()
{
	UnregisterClass(mClassName.c_str(), mHinst);
	mHwnd = nullptr;
}

/**
 * Function : Initialize
 */
bool Window::Initialize(const WindowDesc* desc)
{
	if (MyRegisterClass(desc->hInstance, desc->className) == NULL)
	{
		return false;
	}

	mWidth			= desc->width;
	mHeight			= desc->height;
	mHinst			= desc->hInstance;
	mClassName		= desc->className;
	mIsFullScreen	= desc->isFullscreen;

	DWORD exStyle	= WS_EX_APPWINDOW;
	DWORD style		= WS_VISIBLE;

	if (mIsFullScreen)
	{
		exStyle	|= 0;
		style	|= WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
		mWidth	= GetSystemMetrics(SM_CXSCREEN);
		mHeight	= GetSystemMetrics(SM_CYSCREEN);
	}
	else
	{
		exStyle	|= WS_EX_WINDOWEDGE;
		style	|= WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE;
	}

	RECT rect = { 0, 0, mWidth, mHeight };

	AdjustWindowRectEx(&rect, style, FALSE, exStyle);

	mHwnd = CreateWindowExW(exStyle, desc->className.c_str(), desc->title.c_str(), style,
		(GetSystemMetrics(SM_CXSCREEN) - (rect.right - rect.left)) / 2, (GetSystemMetrics(SM_CYSCREEN) - (rect.bottom - rect.top)) / 2,
		rect.right - rect.left, rect.bottom - rect.top,
		nullptr, nullptr, desc->hInstance, reinterpret_cast<void*>(this));

	if (!mHwnd)
	{
		return false;
	}

	ShowWindow(mHwnd, desc->nCmdShow);
	UpdateWindow(mHwnd);

	return true;
}

/**
 * Function : MyRegisterClass
 */
ATOM Window::MyRegisterClass(HINSTANCE hInstance, const String& className)
{
	WNDCLASSEXW wcex{};

	wcex.cbSize			= sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hCursor		= LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName	= nullptr;
	wcex.lpszClassName	= className.c_str();

	return RegisterClassEx(&wcex);
}

/**
 * Function : PeekMessages
 */
void Window::PeekMessages()
{
	MSG msg;
	while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE) != 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

/**
 * Function : Flash
 */
void Window::Flash(const WindowFlashFlags flags, const UINT count, const DWORD timeout) const
{
	FLASHWINFO info = { sizeof(FLASHWINFO), GetHwnd(), static_cast<DWORD>(flags), count, timeout };
	FlashWindowEx(&info);
}

/**
 * Function : WndProc
 */
LRESULT CALLBACK Window::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static POINT lastMousePos;
	static bool isLButtonDown = false;
	static MouseEventParams mouseParams;

	if (uMsg == WM_CREATE)
	{
		mWindows[hWnd] = reinterpret_cast<Window*>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams);
		return 0;
	}

	if (!mWindows.empty())
	{
		Window* window = mWindows[hWnd];

		if (window->mOnGuiHandler)
		{
			if (window->mOnGuiHandler(hWnd, uMsg, wParam, lParam))
				return true;
		}

		switch (uMsg)
		{
			case WM_CLOSE:
			{
				if (window->mOnClose)
					window->mOnClose();
				return true;
			}

			case WM_KEYDOWN:
			{
				if (wParam == VK_ESCAPE)
				{
					if (window->mOnClose)
						window->mOnClose();
					break;
				}
			}

			case WM_MOUSEWHEEL:
			{
				short wheel = GET_WHEEL_DELTA_WPARAM(wParam);
				if (window->mOnMouseWheelHandler && wheel != 0)
					window->mOnMouseWheelHandler(wheel > 0 ? +1.0f : -1.0f);
				break;
			}

			case WM_MOUSEMOVE:
			{
				mouseParams.CurrentPos.x	= GET_X_LPARAM(lParam);
				mouseParams.CurrentPos.y	= GET_Y_LPARAM(lParam);

				if (isLButtonDown)
				{
					if (window->mOnMouseMoveHandler)
					{
						mouseParams.Diff.x = mouseParams.CurrentPos.x - lastMousePos.x;
						mouseParams.Diff.y = mouseParams.CurrentPos.y - lastMousePos.y;

						window->mOnMouseMoveHandler(std::cref(mouseParams));
					}
				}

				lastMousePos.x	= mouseParams.CurrentPos.x;
				lastMousePos.y	= mouseParams.CurrentPos.y;
				break;
			}

			case WM_LBUTTONDOWN:
			{
				mouseParams.CurrentPos.x = GET_X_LPARAM(lParam);
				mouseParams.CurrentPos.y = GET_Y_LPARAM(lParam);
				isLButtonDown = true;
				break;
			}

			case WM_LBUTTONUP:
			{
				isLButtonDown = false;
				break;
			}

			default:
				break;
		}
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}
