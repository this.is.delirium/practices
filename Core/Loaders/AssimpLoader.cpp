#include "StdafxCore.h"
#include "AssimpLoader.h"
#include <InputLayouts.h>
#include <FileManager.h>
#include <Logger.h>
#include <StringUtils.h>

#include <Render/Pass/GeometryPass.h>
#include <Render/Mode/OpaqueMode.h>
#include <Render/Material/MaterialHelper.h>
#include <Render/Material/Predefined/DefaultMaterial.h>

#include <Scene/Scene.h>
#include <Scene/SceneObject.h>

#include <StringUtils.h>
#include <TextureManager.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace Loaders
{

namespace
{

struct ParsedNode
{
	aiString			Name;
	std::vector<int>	IdxMeshes;
	aiMatrix4x4			Transform;
};
using ParsedNodes = std::list<ParsedNode>;

using Vertex = InputLayouts::Main::Vertex;

//! Parses node
void ParseNode(aiNode* node, ParsedNodes& parsedNodes)
{
	if (node->mNumMeshes)
	{
		parsedNodes.emplace_back();
		ParsedNode& parsedNode = parsedNodes.back();

		parsedNode.Name = node->mName;
		parsedNode.IdxMeshes = std::vector<int>(node->mMeshes, node->mMeshes + node->mNumMeshes);
		parsedNode.Transform = node->mParent->mTransformation * node->mTransformation;
	}

	for (UINT i = 0; i < node->mNumChildren; ++i)
	{
		ParseNode(node->mChildren[i], parsedNodes);
	}

	SLogger::Instance().AddMessage(LogMessageType::Debug, __TEXT("File consists of %u meshes"), parsedNodes.size());
}

bool IsValidMesh(const aiMesh* mesh, const bool pushLog)
{
	const bool isValidMesh =
		mesh->HasFaces() |
		mesh->HasNormals() |
		mesh->HasTangentsAndBitangents() |
		mesh->HasTextureCoords(0);

	if (!isValidMesh && pushLog)
	{
		SLogger::Instance().AddMessage(LogMessageType::Warning, __TEXT("Can't load a mesh ('%s'), because it hasn't got normals, tangents,  binormals or texture coordinates."), mesh->mName.C_Str());
	}

	return isValidMesh;
}

void SetRotation(DirectX::XMVECTOR& lhs, const aiQuaternion& rhs)
{
	lhs = DirectX::XMVectorSet(rhs.x, rhs.y, rhs.z, rhs.w);
}

void SetVertex(const aiMesh* mesh, const UINT id, Vertex& vertex, const aiMatrix4x4* transform, aiQuaternion* rotation)
{
	aiVector3D position		= transform ? *transform * mesh->mVertices[id] : mesh->mVertices[id];
	aiVector3D normal		= rotation ? rotation->Rotate(mesh->mNormals[id]) : mesh->mNormals[id];
	aiVector3D bitangent	= rotation ? rotation->Rotate(mesh->mBitangents[id]) : mesh->mBitangents[id];
	aiVector3D tangent		= rotation ? rotation->Rotate(mesh->mTangents[id]) : mesh->mTangents[id];

	vertex.Position		= DirectX::XMFLOAT3(&position.x);
	vertex.Normal		= DirectX::XMFLOAT3(&normal.x);
	vertex.Bitangent	= DirectX::XMFLOAT3(&bitangent.x);
	vertex.Tangent		= DirectX::XMFLOAT3(&tangent.x);
	vertex.TextureUV	= DirectX::XMFLOAT2(&mesh->mTextureCoords[0][id].x);
}

void ParseMeshesIntoOne(const aiScene* assimpScene, const ParsedNodes& nodes, const std::string& name, Scene& scene, std::list<SceneObjectPtr>& loadedObjects)
{
	aiMesh** meshes = assimpScene->mMeshes;

	//-- Calculate number of vertices and indices.
	size_t numVertices = 0;
	size_t numIndices = 0;
	for (auto& node : nodes)
	{
		for (auto id : node.IdxMeshes)
		{
			aiMesh* mesh = meshes[id];

			if (!IsValidMesh(mesh, false))
			{
				continue;
			}

			numVertices += mesh->mNumVertices;
			numIndices += mesh->mNumFaces * 3;
		}
	}

	//-- Reserves memory using the calculated values.
	std::vector<Vertex> vertices;
	std::vector<int> indices;
	vertices.reserve(numVertices);
	indices.reserve(numIndices);

	//-- Parse meshes.
	for (auto& node : nodes)
	{
		aiVector3D scale, translate;
		aiQuaternion rotation;

		node.Transform.Decompose(scale, rotation, translate);

		for (auto& id : node.IdxMeshes)
		{
			aiMesh* mesh = meshes[id];

			if (!IsValidMesh(mesh, true))
			{
				continue;
			}

			for (UINT i = 0; i < mesh->mNumVertices; ++i)
			{
				Vertex vertex;
				SetVertex(mesh, i, vertex, &node.Transform, &rotation);

				vertices.push_back(vertex);
			}

			for (UINT i = 0; i < mesh->mNumFaces; ++i)
			{
				aiFace& face = mesh->mFaces[i];

				#if _DEBUG
				if (face.mNumIndices != 3)
				{
					SLogger::Instance().AddMessage(LogMessageType::Warning, __TEXT("A mesh ('%s') has inconsistent face (id = %d, indices = %d)"), mesh->mName.C_Str(), i, face.mNumIndices);
					continue;
				}
				#endif // _DEBUG.

				indices.insert(indices.end(), face.mIndices, face.mIndices + face.mNumIndices);
			}
		}
	}

	//-- Create a vertex and index buffers.
	{
		std::shared_ptr<SceneObject> obj = std::make_shared<SceneObject>(SceneObject::GetUniqueName(name));
		loadedObjects.push_back(obj);

		String wname = StringUtils::Converter::StringToWString(name);

		obj->GetVertexBuffer().Initialize(wname + __TEXT(":VB"), sizeof(Vertex) * vertices.size(), vertices.size(), sizeof(Vertex), vertices.data());
		obj->GetIndexBuffer().Initialize(wname + __TEXT(":IB"), sizeof(int) * indices.size(), indices.size(), sizeof(int), indices.data());

		//-- Setup local transforms.
		{
			obj->GetLocalTransform().WorldMatrix(obj->GetTransforms().WorldMatrix);
			obj->GetConstBuffer().UpdateDataOnGPU();
		}
	}
}

} //-- the end of unnamed namespace.

/**
 * Function : AssimpLoader
 */
AssimpLoader::AssimpLoader()
{
	auto& id = typeid(Materials::Predefined::DefaultMaterial);
	std::string name = id.name();
	size_t hash = id.hash_code();

	using namespace std::placeholders;

	mMaterialSetters[id.hash_code()] = std::bind(&AssimpLoader::FillDefaultMaterial, this, _1, _2, _3);
}

/**
 * Function : LoadFile
 */
bool AssimpLoader::LoadModel(const std::string& file, const std::string& matName, Scene& scene, std::list<SceneObjectPtr>& loadedObjects, const bool batchIntoOneMesh)
{
	bool result = false;
	Assimp::Importer importer;

	const UINT flags = aiProcessPreset_TargetRealtime_Fast;
	const aiScene* assimpScene = importer.ReadFile(file, flags);

	result = (assimpScene != nullptr);
	if (!result)
	{
		SLogger::Instance().AddMessage(LogMessageType::Warning, __TEXT("Can't load the file '%s'"), file.c_str());
		return result;
	}

	ParsedNodes parsedNodes;
	ParseNode(assimpScene->mRootNode, parsedNodes);

	if (!batchIntoOneMesh)
	{
		assert(parsedNodes.size() == 1 && "Model has many meshes instead one.");

		ParseMeshes(assimpScene, &parsedNodes.front(), file, matName, scene, loadedObjects);
	}
	//-- ToDo: Reconsider later.
	else
	{
		std::string name = SFileManager::Instance().GetFileName(file);
		ParseMeshesIntoOne(assimpScene, parsedNodes, name, scene, loadedObjects);
	}

	return true;
}

/**
 * Function : FillMaterial
 */
void AssimpLoader::FillMaterial(SceneObjectPtr& obj, const std::string& file, const std::string& matName, aiMaterial* assimpMaterial)
{
	auto& material = obj->GetMaterial();
	material = Materials::Helper::CreateMaterial(matName);

	auto setter = mMaterialSetters.find(typeid(*material).hash_code());
	if (setter == mMaterialSetters.cend())
	{
		assert(false && "Doesn't exist such a material setter.");
	}

	setter->second(obj, assimpMaterial, file);
}

/**
 * Function : LoadTexture
 */
TextureRef AssimpLoader::LoadTexture(const String& parentPath, aiMaterial* material, const int type)
{
	const aiTextureType textureType = static_cast<aiTextureType>(type);
	const UINT count = material->GetTextureCount(textureType);

	assert(count < 2 && "There are too many textures.");

	if (count == 1)
	{
		aiString aiPath;
		material->GetTexture(textureType, 0, &aiPath);

		std::string filename = SFileManager::Instance().GetFileName(std::string(aiPath.C_Str()));
		String path = parentPath + StringUtils::Converter::StringToWString(filename);

		return TextureManager::Instance().GetTexture(path);
	}

	return nullptr;
}

/**
 * Function : FillDefaultMaterial
 */
void AssimpLoader::FillDefaultMaterial(SceneObjectPtr& obj, aiMaterial* assimpMaterial, const std::string& file)
{
	std::string parentPath = SFileManager::Instance().ParentPath(file);
	String path = StringUtils::Converter::StringToWString(parentPath) + __TEXT("/");

	auto& material = obj->GetMaterial();
	auto* defaultMat = reinterpret_cast<Materials::Predefined::DefaultMaterial*>(material.get());

	defaultMat->Albedo()	= LoadTexture(path, assimpMaterial, aiTextureType_DIFFUSE);
	defaultMat->Normal()	= LoadTexture(path, assimpMaterial, aiTextureType_NORMALS);
	defaultMat->Specular()	= LoadTexture(path, assimpMaterial, aiTextureType_SPECULAR);
	defaultMat->Gloss()		= LoadTexture(path, assimpMaterial, aiTextureType_SHININESS);

	{
		using namespace Pass::GeometryPass;
		//material->SetSrvPerObject(ToUnderType(GeometryPassRootSignatureParams::PerObject), obj->GetConstBuffer().SrvHandle().Gpu());
		material->SetSrvPerObject(ToUnderType(RootSignatureParams::PerObject), obj->GetConstBuffer().SrvHandle().Gpu());

		RHIGPUDescriptorHandle dummyHandle;
		material->SetSrvPerObject(ToUnderType(RootSignatureParams::AlbedoTexture), defaultMat->Albedo() ? defaultMat->Albedo()->SrvHandle().Gpu() : dummyHandle);
		material->SetSrvPerObject(ToUnderType(RootSignatureParams::NormalTexture), defaultMat->Normal() ? defaultMat->Normal()->SrvHandle().Gpu() : dummyHandle);
		material->SetSrvPerObject(ToUnderType(RootSignatureParams::SpecularTexture), defaultMat->Specular() ? defaultMat->Specular()->SrvHandle().Gpu() : dummyHandle);
	}
}

/**
 * Function :
 */
void AssimpLoader::ParseMeshes(const aiScene* assimpScene, const ParsedNode* node, const std::string& file, const std::string& matName, Scene& scene, std::list<SceneObjectPtr>& loadedObjects)
{
	aiMesh** meshes = assimpScene->mMeshes;

	//-- Calculate number of vertices and indices.
	size_t numVertices = 0;
	size_t numIndices = 0;
	for (auto id : node->IdxMeshes)
	{
		aiMesh* mesh = meshes[id];

		if (!IsValidMesh(mesh, false))
		{
			continue;
		}

		numVertices += mesh->mNumVertices;
		numIndices += mesh->mNumFaces * 3;
	}

	std::vector<Vertex> vertices;
	std::vector<int> indices;
	vertices.reserve(numVertices);
	indices.reserve(numIndices);

	for (auto& id : node->IdxMeshes)
	{
		aiMesh* mesh = meshes[id];

		if (!IsValidMesh(mesh, true))
		{
			continue;
		}

		const UINT idxOffset = static_cast<UINT>(vertices.size());
		for (UINT i = 0; i < mesh->mNumVertices; ++i)
		{
			Vertex vertex;
			SetVertex(mesh, i, vertex, nullptr, nullptr);

			vertices.push_back(vertex);
		}

		for (UINT i = 0; i < mesh->mNumFaces; ++i)
		{
			aiFace& face = mesh->mFaces[i];

			#if _DEBUG
			if (face.mNumIndices != 3)
			{
				SLogger::Instance().AddMessage(LogMessageType::Warning, __TEXT("A mesh ('%s') has inconsistent face (id = %d, indices = %d)"), mesh->mName.C_Str(), i, face.mNumIndices);
				continue;
			}
			#endif // _DEBUG.

			for (UINT j = 0; j < face.mNumIndices; ++j)
			{
				indices.push_back(idxOffset + face.mIndices[j]);
			}
			//indices.insert(indices.end(), face.mIndices, face.mIndices + face.mNumIndices);
		}
	}

	//-- Create a vertex, index buffers, setup a material and transformation.
	{
		std::string name(node->Name.C_Str());

		std::shared_ptr<SceneObject> obj = std::make_shared<SceneObject>(SceneObject::GetUniqueName(name));
		loadedObjects.push_back(obj);

		String wname = StringUtils::Converter::StringToWString(name);

		obj->GetVertexBuffer().Initialize(wname + __TEXT(":VB"), sizeof(Vertex) * vertices.size(), vertices.size(), sizeof(Vertex), vertices.data());
		obj->GetIndexBuffer().Initialize(wname + __TEXT(":IB"), sizeof(int) * indices.size(), indices.size(), sizeof(int), indices.data());

		//-- Setup and fill a material.
		{
			aiMesh* mesh = meshes[node->IdxMeshes[0]];
			auto* mat = assimpScene->mMaterials[mesh->mMaterialIndex];
			FillMaterial(obj, file, matName, mat);
		}

		//-- Setup local transforms.
		{
			aiVector3D position, scaling;
			aiQuaternion rotation;

			node->Transform.Decompose(scaling, rotation, position);

			obj->GetLocalTransform().Scale() = DirectX::XMFLOAT3(&scaling.x);
			obj->GetLocalTransform().Translation() = DirectX::XMFLOAT3(&position.x);

			SetRotation(obj->GetLocalTransform().Rotation(), rotation);

			obj->GetLocalTransform().WorldMatrix(obj->GetTransforms().WorldMatrix);
			obj->GetConstBuffer().UpdateDataOnGPU();
		}

		//-- Add an object to a scene.
		//-- ToDo: Reconsider later.
		scene.AddObject<OpaqueMode>(obj);
	}
}

} // end of namespace Loaders.
