#pragma once
#include "StdafxCore.h"
#include <Scene/SceneObject.h>
#include <RHITexture.h>

struct aiMaterial;
struct aiScene;

class Scene;

namespace Loaders
{

namespace
{
class ParsedNode;
} // end of unnamed namespace.

class AssimpLoader
{
public:
	AssimpLoader();

	//-- Loads a file using MainLayout::Vertex.
	bool LoadModel(const std::string& file, const std::string& matName, Scene& scene, std::list<SceneObjectPtr>& loadedObjects, const bool batchIntoOneMesh);

protected:
	void FillMaterial(SceneObjectPtr& obj, const std::string& file, const std::string& matName, aiMaterial* assimpMaterial);

	TextureRef LoadTexture(const String& path, aiMaterial* material, const int type);

	void ParseMeshes(const aiScene* assimpScene, const ParsedNode* node, const std::string& file, const std::string& matName, Scene& scene, std::list<SceneObjectPtr>& loadedObjects);

protected:
	void FillDefaultMaterial(SceneObjectPtr& material, aiMaterial* assimpMaterial, const std::string& file);

protected:
	using MaterialSetter	= std::function<void(SceneObjectPtr&, aiMaterial*, const std::string&)>;
	using MaterialSetters	= std::unordered_map<size_t, MaterialSetter>;

	MaterialSetters mMaterialSetters;
};

} // end of namespace Loaders.
