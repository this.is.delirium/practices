#pragma once
#include "StdafxCore.h"
#include "Patterns/Singleton.h"

class ResourceManager
{
public:
	void SetRootFolder(const String& rootFolder);
	void FindRootFolder();

	const String& RootFolder() const
	{
		return mRootFolder.Path;
	}

	std::string LoadFile(const String& fullPath);
	void SaveFile(const String& fullPath, const std::string& file);

	/**
	 * Returns full path to an object with given name.
	 * @param name may be the following contents:
	 *	only name of file.
	 *	folder:name - folder will be keep
	 */
	String Find(const String& name);

protected:
	String Find(const String& startFolder, const String& name, const String& pattern);

protected:
	ResourceManager();
	ResourceManager(const ResourceManager&) = delete;
	ResourceManager& operator=(const ResourceManager&) = delete;

	friend class Singleton<ResourceManager>;

protected:
	struct Node;
	using MapOfFolders = std::unordered_map<String, Node*>;

	struct Node
	{
		~Node()
		{
			for (auto& child : Folders)
			{
				delete child.second;
			}
		}

		String			Path;
		MapOfFolders	Folders;
		Node*			Parent;
	};

	Node	mRootFolder;
	String	mFindResult;
};

typedef Singleton<ResourceManager> SResourceManager;
