#pragma once
#include "StdafxCore.h"
#include "RHI.h"

namespace InputLayouts
{
	namespace Main
	{
		struct Vertex
		{
			DirectX::XMFLOAT3 Position;
			DirectX::XMFLOAT3 Normal;
			DirectX::XMFLOAT3 Bitangent;
			DirectX::XMFLOAT3 Tangent;
			DirectX::XMFLOAT2 TextureUV;

			FORCEINLINE void SetVertex(const DirectX::XMFLOAT3& pos) { Position = pos; }
			FORCEINLINE void SetNormal(const DirectX::XMFLOAT3& normal) { Normal = normal; }
			FORCEINLINE void SetTexCoords(const DirectX::XMFLOAT2& uv) { TextureUV = uv; }
		};

		static const RHIInputElementDesc inputElementDesc[] =
		{
			{ "POSITION",	0, RHIFormat::R32G32B32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "NORMAL",		0, RHIFormat::R32G32B32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "BITANGENT",	0, RHIFormat::R32G32B32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "TANGENT",	0, RHIFormat::R32G32B32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "TEXCOORD",	0, RHIFormat::R32G32Float,		0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 }
		};
	}

	namespace ScreenQuadWithoutMatrix
	{
		struct Vertex
		{
			Vertex(const DirectX::XMFLOAT2& position)
				: pos(position) { }

			DirectX::XMFLOAT2 pos;
		};

		static const RHIInputElementDesc inputElementDesc[] =
		{
			{ "POSITION", 0, RHIFormat::R32G32Float, 0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 }
		};
	}

	namespace ParticlesUpdate
	{
		struct Vertex
		{

		};

		static const RHIInputElementDesc inputElementDesc[] =
		{
			{ "POSITION",	0, RHIFormat::R32G32B32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "VELOCITY",	0, RHIFormat::R32G32B32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "COLOR",		0, RHIFormat::R32G32B32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "SETTINGS",	0, RHIFormat::R32G32Float,		0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "TYPE",		0, RHIFormat::R32Sint,			0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 }
		};
	}

	namespace ParticlesRender
	{
		struct Vertex
		{
			DirectX::XMFLOAT4	CurrPosition;
			DirectX::XMFLOAT4	PrevPosition;
		};

		static const RHIInputElementDesc inputElementDesc[] =
		{
			{ "POSITION",		0, RHIFormat::R32G32B32A32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "PREVPOSITION",	0, RHIFormat::R32G32B32A32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 }
		};
	}

	namespace ScreenQuad
	{
		struct Vertex
		{
			Vertex() { }
			Vertex(const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& tex) : Position(pos), Texcoord(tex) { }
			Vertex(const float x, const float y, const float tx, const float ty) : Position(x, y), Texcoord(tx, ty) { }

			DirectX::XMFLOAT2 Position;
			DirectX::XMFLOAT2 Texcoord;
		};

		static const RHIInputElementDesc inputElementDesc[] =
		{
			{ "POSITION", 0, RHIFormat::R32G32Float, 0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "TEXCOORD", 0, RHIFormat::R32G32Float, 0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 }
		};
	}

	namespace Tessellation
	{
		struct Vertex
		{
			DirectX::XMFLOAT3 Position;
		};

		static const RHIInputElementDesc inputElementDesc[] =
		{
			{ "POSITION", 0, RHIFormat::R32G32B32Float, 0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 }
		};
	}

	namespace RenderToTexture
	{
		struct Vertex
		{
			DirectX::XMFLOAT3 Position;
		};

		static const RHIInputElementDesc inputElementDesc[] =
		{
			{ "POSITION", 0, RHIFormat::R32G32B32Float, 0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 }
		};
	}

	namespace StreamOutput
	{
		struct Vertex
		{
			DirectX::XMFLOAT3	Position;
			DirectX::XMFLOAT3	Velocity;
			DirectX::XMFLOAT3	Color;
			DirectX::XMFLOAT2	Settings;
			int					Type;
		};

		static const RHIInputElementDesc inputElementDesc[] =
		{
			{ "POSITION",	0, RHIFormat::R32G32B32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "VELOCITY",	0, RHIFormat::R32G32B32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "COLOR",		0, RHIFormat::R32G32B32Float,	0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "SETTINGS",	0, RHIFormat::R32G32Float,		0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "TYPE",		0, RHIFormat::R32Sint,			0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 }
		};
	}

	namespace Frustum
	{
		struct Vertex
		{
			DirectX::XMFLOAT4 Position;
			DirectX::XMFLOAT4 Color;
		};

		static const RHIInputElementDesc inputElementDesc[] =
		{
			{ "POSITION", 0, RHIFormat::R32G32B32A32Float, 0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 },
			{ "COLOR", 0, RHIFormat::R32G32B32A32Float, 0, RHIConstants::kAppendAlignedElement, RHIInputClassification::PerVertexData, 0 }
		};
	}
}
