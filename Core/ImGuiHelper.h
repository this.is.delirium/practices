#pragma once
#include "StdafxCore.h"
#include <Patterns/Singleton.h>

class IImGui;
class Transformation;
class RHITexture;

class ImGuiHelper : public Singleton<ImGuiHelper>
{
public:
	ImGuiHelper();

	bool ShowProperty(rttr::instance& obj, const rttr::property& prop);

	//! Returns true if a property was changed.
	bool ShowInstance(rttr::instance& obj);

public:
	//-- ToDo: Reconsider later.
	void ShowTexture(const RHITexture* texture, const DirectX::XMFLOAT2& size, const DirectX::XMFLOAT2& tooltipSize, const float tooltipZoom) const;

protected:
	bool ShowFloat(rttr::instance& obj, const rttr::property& prop);
	bool ShowInt(rttr::instance& obj, const rttr::property& prop);
	bool ShowBool(rttr::instance& obj, const rttr::property& prop);

	bool ShowString(rttr::instance& obj, const rttr::property& prop);

	bool ShowSceneObject(rttr::instance& obj, const rttr::property& prop);
	bool ShowTransformation(rttr::instance& obj, const rttr::property& prop);

	bool ShowFloat3(rttr::instance& obj, const rttr::property& prop);
	bool ShowFloat4(rttr::instance& obj, const rttr::property& prop);
	bool ShowVector(rttr::instance& obj, const rttr::property& prop);

protected:
	using ShowPropertyFunc = std::function<bool(rttr::instance& obj, const rttr::property& prop)>;

	std::unordered_map<uintptr_t, ShowPropertyFunc> mDisplayFunctions;
};
