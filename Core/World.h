#pragma once
#include "StdafxCore.h"
#include <Serialize/ISerializable.h>
#include <Scene/Scene.h>

class World;
using WorldPtr = std::shared_ptr<World>;

class World : public ISerializable
{
public:
	World(const std::string& name)
		: mName(name), mScene() { }

	const Scene& GetScene() const { return mScene; }
	Scene& GetScene() { return mScene; }

public: //-- ISerializable
	void Serialize(nlohmann::json& js) const override final;
	void Deserialize(nlohmann::json& js) override final;

protected:
	std::string mName;
	Scene mScene;
};
