#include "StdafxCore.h"
#include "World.h"

/**
 * Function : Serialize
 */
void World::Serialize(nlohmann::json& js) const
{
	auto& jsScene = js["scene"];
	jsScene = nlohmann::json::object();

	mScene.Serialize(jsScene);

	js["name"] = mName;

}

/**
 * Function : Deserialize
 */
void World::Deserialize(nlohmann::json& js)
{
	GetScene().Deserialize(js);
}
