require 'fileutils'
include FileUtils

def prepare_third_party()
# cmake -DBOOST_ROOT="D:/SDK/boost_1_60_0" -DLIBBSON_DIR="d:/SDK/MongoDB" -DLIBMONGOC_DIR="D:/SDK/MongoDB" -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 14 Win64" -DCMAKE_INSTALL_PREFIX=D:\SDK\MongoDB ..
end

BUILD_DIR = "Build"

if not Dir.exist?(BUILD_DIR) then
	mkdir(BUILD_DIR)
end

cd(BUILD_DIR)

system('cmake ../ -G "Visual Studio 15 Win64"')
