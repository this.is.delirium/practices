#include "StdafxRHI.h"
#include "RHITexture.h"
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include "RHIUtils.h"

using namespace GraphicsCore;

/**
 * Function : Release
 */
void RHITexture::Release()
{
	RHISafeRelease(mTexture);
	mRtvHandle.Release();
	mDsvHandle.Release();
	mSrvHandle.Release();
	mUavHandle.Release();
}

/**
 * Function : InitializeByName
 */
void RHITexture::InitializeByName(const String& textureName)
{
	InitializeByPath(SResourceManager::Instance().Find(textureName));
}

/**
 * Function : InitializeByPath
 */
void RHITexture::InitializeByPath(const String& path)
{
	mTexture = gDevice->CreateTextureFromFile(path);

	mSrvHandle.Initialize(gRenderer->CbvSrvUavHeapManager());

	RHIResourceDesc				texDesc	= mTexture->GetDesc();
	RHIShaderResourceViewDesc	srvDesc	= {};

	RHIUtils::Fill::ShaderResourceViewDesc(&texDesc, &srvDesc);

	gDevice->CreateShaderResourceView(mTexture, &srvDesc, mSrvHandle.Cpu());

	gRenderer->ChangeResourceBarrier(1, &RHIResourceBarrier::MakeTransition(Texture(), RHIResourceState::CopyDest, RHIResourceState::PixelShaderResource));
}

/**
 * Function : InitializeCubeMap
 */
void RHITexture::InitializeCubeMap(const String& folder, const String& extension)
{
	const String names[6] =
	{
		__TEXT(":posx"),
		__TEXT(":negx"),
		__TEXT(":posy"),
		__TEXT(":negy"),
		__TEXT(":posz"),
		__TEXT(":negz")
	};

	RHIResourceDesc imageDescs[6];
	RHISubresourceData subresources[6];

	for (int i = 0; i < _countof(names); ++i)
	{
		String name = folder + names[i] + extension;
		String path = SResourceManager::Instance().Find(name);
		if (path.empty())
		{
			SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Can't load the image '%s'"), name.c_str());
			return;
		}
		
		gDevice->LoadTextureFromFile(path, &imageDescs[i], &subresources[i]);
	}

	assert(imageDescs[0].MipLevels == 1);

	// Create & load texture.
	RHIResourceDesc desc = {};
	{
		desc.DepthOrArraySize	= 6;
		desc.Dimension			= RHIResourceDimension::Texture2D;
		desc.Format				= imageDescs[0].Format;
		desc.Height				= imageDescs[0].Height;
		desc.MipLevels			= imageDescs[0].MipLevels;
		desc.SampleDesc.Count	= 1;
		desc.Width				= imageDescs[0].Width;

		mTexture = gDevice->CreateCommittedResource(
			&RHIHeapProperties(RHIHeapType::Default),
			RHIHeapFlag::None,
			&desc,
			RHIResourceState::Common,
			nullptr);

		gDevice->UploadResource(mTexture, 0, _countof(subresources), subresources);
	}

	// Create SRV.
	{
		mSrvHandle.Initialize(gRenderer->CbvSrvUavHeapManager());

		RHIShaderResourceViewDesc srvDesc = {};
		RHIUtils::Fill::ShaderResourceViewDesc(&desc, &srvDesc);

		gDevice->CreateShaderResourceView(mTexture, &srvDesc, mSrvHandle.Cpu());
	}

	gRenderer->ChangeResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mTexture, RHIResourceState::CopyDest, RHIResourceState::PixelShaderResource));
}

/**
 * Function : Initialize
 */
void RHITexture::Initialize(const RHIResourceDesc* desc, const RHIClearValue* clearValue, RHISubresourceData* subresourceData)
{
	mTexture = gDevice->CreateCommittedResource(
		&RHIHeapProperties(RHIHeapType::Default),
		RHIHeapFlag::None,
		desc,
		RHIResourceState::Common,
		clearValue);

	// Create SRV
	if (!(desc->Flags & RHIResourceFlag::DenyShaderResource))
	{
		mSrvHandle.Initialize(gRenderer->CbvSrvUavHeapManager());

		RHIShaderResourceViewDesc srvDesc = {};
		RHIUtils::Fill::ShaderResourceViewDesc(desc, &srvDesc);

		gDevice->CreateShaderResourceView(Texture(), &srvDesc, mSrvHandle.Cpu());
	}

	// Create UAV.
	if (desc->Flags & RHIResourceFlag::AllowUnorderedAccess)
	{
		mUavHandle.Initialize(gRenderer->CbvSrvUavHeapManager());

		RHIUnorderedAccessViewDesc uavDesc = {};
		RHIUtils::Fill::UnorderedAccessViewDesc(desc, &uavDesc);

		gDevice->CreateUnorderedAccessView(Texture(), nullptr, &uavDesc, mUavHandle.Cpu());
	}

	// Create RTV.
	if (desc->Flags & RHIResourceFlag::AllowRenderTarget)
	{
		mRtvHandle.Initialize(gRenderer->RtvHeapManager());

		gDevice->CreateRenderTargetView(Texture(), nullptr, mRtvHandle.Cpu());
	}

	// Create DSV.
	if (desc->Flags & RHIResourceFlag::AllowDepthStencil)
	{
		mDsvHandle.Initialize(gRenderer->DsvHeapManager());

		RHIDepthStencilViewDesc dsvDesc = {};
		RHIUtils::Fill::DepthStencilViewDesc(desc, &dsvDesc);

		gDevice->CreateDepthStencilView(Texture(), &dsvDesc, mDsvHandle.Cpu());
	}

	// Upload data.
	if (subresourceData)
	{
		UINT64 uploadSizeBuffer = gDevice->GetRequiredIntermediateSize(Texture(), 0, 1);
		RHIResource* textureUploadHeap = gDevice->CreateCommittedResource(
			&RHIHeapProperties(RHIHeapType::Upload),
			RHIHeapFlag::None,
			&RHIResourceDesc::Buffer(uploadSizeBuffer),
			RHIResourceState::GenericRead,
			nullptr);

		gRenderer->UploadCommandList()->Reset(gRenderer->UploadCommandAllocator(), nullptr);
		gDevice->UpdateSubresources(gRenderer->UploadCommandList(), Texture(), textureUploadHeap, 0ui64, 0, 1, subresourceData);
		gRenderer->CommitCommandList(gRenderer->UploadCommandList());

		RHISafeRelease(textureUploadHeap);
		/*RHIBox box	= {};
		box.left	= 0;
		box.top		= 0;
		box.right	= (UINT)desc->Width;
		box.bottom	= (UINT)desc->Height;
		box.front	= 0u;
		box.back	= 1u;

		Texture()->Map(0, nullptr, nullptr);
		Texture()->WriteToSubresource(0, &box, subresourceData->Data, subresourceData->RowPitch, subresourceData->SlicePitch);
		Texture()->Unmap(0, nullptr);*/
	}
}
