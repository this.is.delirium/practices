#include "StdafxRHI.h"
#include "RHI.h"

/**
 * Function : ~RHICommandList
 */
RHICommandList::~RHICommandList()
{
	ClearCommands();
}

/**
 * Function : ClearCommands
 */
void RHICommandList::ClearCommands()
{
	/*for (RHICommand*& command : mCommands)
	{
		delete command;
		command = nullptr;
	}*/
	mCommands.clear();

	/*for (RHICommand*& delayCommand : mDelayedCommands)
	{
		delete delayCommand;
		delayCommand = nullptr;
	}*/
	mDelayedCommands.clear();
}

RHIRootSignatureFlags GetRootSignatureFlags(const bool ia, const bool vertex, const bool hull, const bool domain, const bool geometry, const bool pixel, const bool so)
{
	RHIRootSignatureFlags flags = RHIRootSignatureFlags::None;

	if (ia)
		flags |= RHIRootSignatureFlags::AllowInputAssemblerInputLayout;
	if (!vertex)
		flags |= RHIRootSignatureFlags::DenyVertexShaderRootAccess;
	if (!hull)
		flags |= RHIRootSignatureFlags::DenyHullShaderRootAccess;
	if (!domain)
		flags |= RHIRootSignatureFlags::DenyDomainShaderRootAccess;
	if (!geometry)
		flags |= RHIRootSignatureFlags::DenyGeometryShaderRootAccess;
	if (!pixel)
		flags |= RHIRootSignatureFlags::DenyPixelShaderRootAccess;
	if (so)
		flags |= RHIRootSignatureFlags::AllowStreamOutput;

	return flags;
}
