#pragma once
#include "StdafxRHI.h"
#include "RHIBuffer.h"

class RHIByteAddressBuffer : public RHIBuffer
{
public:
	RHIByteAddressBuffer()
		: RHIBuffer(RHIResourceFlag::AllowUnorderedAccess) { }

	void Release() override final;

public:
	FORCEINLINE RHIResourceState ResourceState() override final { return RHIResourceState::Common; }

protected:
	void CreateDerivedViews() override final;
};
