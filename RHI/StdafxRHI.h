#pragma once

#include <array>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <map>
#include <memory>
#include <stack>
#include <stdexcept>
#include <stdint.h>
#include <string>
#include <vector>

#include <TypeDefinitions.h>

#include <Logger.h>

#ifndef NoOpenGL
// glm
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// gli
#include <gli/gli.hpp>

// glew
#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/wglew.h>
#endif

#include <d3d12.h>
#include <dxgi1_5.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>


#ifdef WIN32
#include <roapi.h>
#include <Windows.h>
#include <wrl.h>
using Microsoft::WRL::ComPtr;

#include <DirectXTex.h>
#else
typedef void*            HWND;

typedef unsigned int     UINT;
typedef unsigned char    BYTE;
typedef const char*      LPCSTR;
typedef int              BOOL;
typedef unsigned __int64 SIZE_T;
typedef float            FLOAT;
#endif

inline void ThrowIfFailed(HRESULT hr, const String& message)
{
	if (FAILED(hr))
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, message);
		SLogger::Instance().FlushToDisk();
		throw message;
	}
}

inline void ThrowIfFailed(HRESULT hr, CConstString format, ...)
{
	if (FAILED(hr))
	{
		va_list args;
		va_start(args, format);

		Char message[1024];
		vsprintfs(message, format, args);
		SLogger::Instance().AddMessage(LogMessageType::Error, format, args);
		SLogger::Instance().FlushToDisk();

		va_end(args);

		throw message;
	}
}

#define AddFileAndLine(message) ("File: %s; Line: %d\n"##message), __FILE__, __LINE__

/*
 * Concatenate preprocessor tokens A and B without expanding macro definitions
 * (however, if invoked from a macro, macro arguments are expanded).
 */
#define PPCAT_NX(A, B) A ## B

/*
 * Concatenate preprocessor tokens A and B after macro-expanding them.
 */
#define PPCAT(A, B) PPCAT_NX(A, B)

/*
 * Turn A into a string literal without expanding macro definitions
 * (however, if invoked from a macro, macro arguments are expanded).
 */
#define STRINGIZE_NX(A) #A

/*
 * Turn A into a string literal after macro-expanding it.
 */
#define STRINGIZE(A) STRINGIZE_NX(A)

#define TEST \ doesnt match \ 
#define STATIC_ASSERT(D3D12Structure, RHIStructure) static_assert(sizeof(D3D12Structure) == sizeof(RHIStructure), STRINGIZE(PPCAT(PPCAT(RHIStructure, TEST), D3D12Structure)));

#define STATIC_VALIDATE_PARAMS(EnumType) static_assert(std::is_enum<EnumType>::value, "Template parameter is not an enum type.");

template<typename Enum>
Enum operator |(const Enum lhs, const Enum rhs)
{
	STATIC_VALIDATE_PARAMS(Enum)
	using underlying = typename std::underlying_type<Enum>::type;

	return static_cast<Enum> (static_cast<underlying>(lhs) | static_cast<underlying>(rhs));
}

template<typename Enum>
Enum& operator |=(Enum& a, Enum b)
{
	STATIC_VALIDATE_PARAMS(Enum)
	using underlying = typename std::underlying_type<Enum>::type;

	return a = a | b;
}

template<typename Enum>
int operator &(const Enum lhs, const Enum rhs)
{
	STATIC_VALIDATE_PARAMS(Enum)
	using underlying = typename std::underlying_type<Enum>::type;

	return static_cast<int> (static_cast<underlying>(lhs) & static_cast<underlying>(rhs));
}

template<class D3D12Structure, class RHIStructure>
inline const D3D12Structure* ToD3D12Structure(const RHIStructure* structure)
{
	return reinterpret_cast<const D3D12Structure*> (structure);
}

template<class D3D12Structure, class RHIStructure>
inline D3D12Structure* ToD3D12Structure(RHIStructure* structure)
{
	return reinterpret_cast<D3D12Structure*>(structure);
}
