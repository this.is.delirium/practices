#include "StdafxRHI.h"
#include "D3D12CommandQueue.h"
#include "D3D12Fence.h"

/**
 * Function : Signal
 */
void D3D12CommandQueue::Signal(RHIFence* fence, UINT64 value)
{
	D3D12Fence* d3d12Fence = ToD3D12Structure<D3D12Fence>(fence);

	ThrowIfFailed(mCommandQueue->Signal(d3d12Fence->Fence().Get(), value), __TEXT("Can't get a signal."));
}
