#include "StdafxRHI.h"
#include "D3D12SwapChain.h"

#include "D3D12Resource.h"

/**
 * Function : GetBuffer
 */
void D3D12SwapChain::GetBuffer(const UINT buffer, RHIResource*& surface)
{
	D3D12Resource* resource = new D3D12Resource();

	ThrowIfFailed(mSwapChain->GetBuffer(buffer, IID_PPV_ARGS(&resource->Resource())), __TEXT("Can't get a buffer."));

	surface = resource;
}

/**
 * Function : GetDesc
 */
bool D3D12SwapChain::GetDesc(RHISwapChainDesc* desc) const
{
	DXGI_SWAP_CHAIN_DESC* d3d12Desc = ToD3D12Structure<DXGI_SWAP_CHAIN_DESC>(desc);

	return mSwapChain->GetDesc(d3d12Desc) == S_OK;
}
