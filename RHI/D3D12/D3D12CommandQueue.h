#pragma once
#include "StdafxRHI.h"
#include "D3D12CommandList.h"
#include <RHI.h>

class D3D12CommandQueue : public RHICommandQueue
{
public:
	virtual void Release() { }

	ComPtr<ID3D12CommandQueue>& Queue() { return mCommandQueue; }

public:
	void ExecuteCommandLists(const UINT count, RHICommandList** commandLists)
	{
		std::vector<ID3D12CommandList*> lists(count);
		for (UINT i = 0; i < count; ++i)
		{
			lists[i] = reinterpret_cast<D3D12CommandList*>(commandLists[i])->List().Get();
		}
		mCommandQueue->ExecuteCommandLists(count, lists.data());
	}

	void Signal(RHIFence* fence, UINT64 value) override final;

protected:
	ComPtr<ID3D12CommandQueue>	mCommandQueue;
};
