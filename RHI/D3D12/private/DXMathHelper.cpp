#include <StdafxRHI.h>
#include "DXMathHelper.h"

namespace DirectX
{
	/**
	 * Function : XMFloat4Normalize
	 */
	XMFLOAT4 XMFloat4Normalize(const XMFLOAT4& vec)
	{
		XMFLOAT4 result;

		XMStoreFloat4(&result, XMVector4Normalize(XMLoadFloat4(&vec)));

		return result;
	}

	/**
	 * Function : XMFloat3Normalize
	 */
	XMFLOAT3 XMFloat3Normalize(const XMFLOAT3& vec)
	{
		XMFLOAT3 result;

		XMStoreFloat3(&result, XMVector3Normalize(XMLoadFloat3(&vec)));

		return result;
	}

	/**
	 * Function : XMFloat3Cross
	 */
	XMFLOAT3 XMFloat3Cross(const XMFLOAT3& a, const XMFLOAT3& b)
	{
		XMFLOAT3 result;

		XMStoreFloat3(&result, XMVector3Cross(XMLoadFloat3(&a), XMLoadFloat3(&b)));

		return result;
	}

	/**
	 * Function : XMFloat3Length
	 */
	float XMFloat3Length(const XMFLOAT4& a, const XMFLOAT4& b)
	{
		XMFLOAT4 tmp = b - a;
		return sqrtf(XMFloat3Dot(tmp, tmp));
	}

	/**
	 * Function : XMFloat3Dot
	 */
	float XMFloat3Dot(const XMFLOAT3& a, const XMFLOAT3& b)
	{
		return a.x * b.x + a.y * b.y + a.z * b.z;
	}

	/**
	 * Function : XMFloat3Dot
	 */
	float XMFloat3Dot(const XMFLOAT4& a, const XMFLOAT4& b)
	{
		return a.x * b.x + a.y * b.y + a.z * b.z;
	}
}
