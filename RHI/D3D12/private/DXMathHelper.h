#pragma once
#include "StdafxRHI.h"

namespace DirectX
{
	// XMFLOAT4 operators
	FORCEINLINE XMFLOAT4 MakeFloat4(const XMFLOAT2& other, const float z, const float w)
	{
		return XMFLOAT4(other.x, other.y, z, w);
	}

	FORCEINLINE XMFLOAT4 MakeFloat4(const XMFLOAT3& other, const float w)
	{
		return XMFLOAT4(other.x, other.y, other.z, w);
	}

	FORCEINLINE XMFLOAT4 operator+(const XMFLOAT4& a, const XMFLOAT4& b)
	{
		return XMFLOAT4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
	}

	FORCEINLINE XMFLOAT4 operator-(const XMFLOAT4& a, const XMFLOAT4& b)
	{
		return XMFLOAT4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.z);
	}

	FORCEINLINE XMFLOAT4 operator*(const XMFLOAT4& vec, const float value)
	{
		return XMFLOAT4(vec.x * value, vec.y * value, vec.z * value, vec.w * value);
	}

	FORCEINLINE XMFLOAT4 operator-(const XMFLOAT4& vec)
	{
		return XMFLOAT4(-vec.x, -vec.y, -vec.z, vec.w);
	}

	XMFLOAT4 XMFloat4Normalize(const XMFLOAT4& vec);

	// XMFLOAT3 operators
	FORCEINLINE XMFLOAT3 MakeFloat3(const XMFLOAT4& other)
	{
		return XMFLOAT3(other.x, other.y, other.z);
	}

	FORCEINLINE XMFLOAT3 MakeFloat3(const float value)
	{
		return XMFLOAT3(value, value, value);
	}

	FORCEINLINE XMFLOAT3 operator+(const XMFLOAT3& a, const XMFLOAT3& b)
	{
		return XMFLOAT3(a.x + b.x, a.y + b.y, a.z + b.z);
	}

	FORCEINLINE XMFLOAT3 operator-(const XMFLOAT3& a, const XMFLOAT3& b)
	{
		return XMFLOAT3(a.x - b.x, a.y - b.y, a.z - b.z);
	}

	FORCEINLINE XMFLOAT3& operator+=(XMFLOAT3& a, const XMFLOAT3& b)
	{
		a.x += b.x;
		a.y += b.y;
		a.z += b.z;

		return a;
	}

	FORCEINLINE XMFLOAT3& operator*=(XMFLOAT3& vec, const float value)
	{
		vec.x *= value;
		vec.y *= value;
		vec.z *= value;

		return vec;
	}

	FORCEINLINE XMFLOAT3 operator*(const XMFLOAT3& vec, const float value)
	{
		return XMFLOAT3(vec.x * value, vec.y * value, vec.z * value);
	}

	FORCEINLINE XMFLOAT3 operator*(const float value, const XMFLOAT3& vec)
	{
		return vec * value;
	}

	FORCEINLINE XMFLOAT3 operator/(const XMFLOAT3& vec, const float value)
	{
		return XMFLOAT3(vec.x / value, vec.y / value, vec.z / value);
	}

	FORCEINLINE bool IsEqual(const XMFLOAT3& a, const XMFLOAT3& b)
	{
		return fabs(a.x - b.x) < 1e-3f
			&& fabs(a.y - b.y) < 1e-3f
			&& fabs(a.z - b.z) < 1e-3f;
	}

	XMFLOAT3 XMFloat3Normalize(const XMFLOAT3& vec);

	XMFLOAT3 XMFloat3Cross(const XMFLOAT3& a, const XMFLOAT3& b);

	float XMFloat3Length(const XMFLOAT4& a, const XMFLOAT4& b);

	float XMFloat3Dot(const XMFLOAT3& a, const XMFLOAT3& b);
	float XMFloat3Dot(const XMFLOAT4& a, const XMFLOAT4& b);

	FORCEINLINE XMFLOAT3 Min(const XMFLOAT3& a, const XMFLOAT3& b) { return XMFLOAT3((std::min)(a.x, b.x), (std::min)(a.y, b.y), (std::min)(a.z, b.z)); }
	FORCEINLINE XMFLOAT3 Max(const XMFLOAT3& a, const XMFLOAT3& b) { return XMFLOAT3((std::max)(a.x, b.x), (std::max)(a.y, b.y), (std::max)(a.z, b.z)); }

	// XMFLOAT2 operators
	FORCEINLINE XMFLOAT2 operator*(const XMFLOAT2& a, const XMFLOAT2& b)
	{
		return XMFLOAT2(a.x * b.x, a.y * b.y);
	}

	FORCEINLINE XMFLOAT2 operator-(const XMFLOAT2& a, const XMFLOAT2& b)
	{
		return XMFLOAT2(a.x - b.x, a.y - b.y);
	}

	// Different functions.

	// Ignores the w-component of the first vector.
	FORCEINLINE XMFLOAT3 Min(const XMFLOAT4& a, const XMFLOAT3& b) { return XMFLOAT3((std::min)(a.x, b.x), (std::min)(a.y, b.y), (std::min)(a.z, b.z)); }
	// Ignores the w-component of the first vector.
	FORCEINLINE XMFLOAT3 Max(const XMFLOAT4& a, const XMFLOAT3& b) { return XMFLOAT3((std::max)(a.x, b.x), (std::max)(a.y, b.y), (std::max)(a.z, b.z)); }
}
