#pragma once
#include "StdafxRHI.h"
#include <RHI.h>

class IRenderer;
class D3D12Device : public RHIRenderingDevice
{
public:
	D3D12Device(IRenderer* renderer)
		: mRenderer(renderer), mRunTimeInitializerWrapper(RO_INIT_MULTITHREADED) { }
	~D3D12Device() { }

	void Release() override final;

	ComPtr<ID3D12Device>& Device() { return mDevice; }

public:

#ifdef WIN32
	virtual bool Initialize(HWND hWnd) override final;
#endif

	bool CheckFeatureSupport(
		RHIFeature	feature,
		void*		featureSupportData,
		const UINT	featureSupportDataSize) override final;

	RHICommandQueue* CreateCommandQueue(const RHICommandQueueDesc* desc) override final;

	RHISwapChain* CreateSwapChain(RHISwapChainDesc* desc, RHICommandQueue* commandQueue) override final;
	RHISwapChain* CreateSwapChain(RHISwapChainDesc1* desc, RHISwapChainFullscreenDesc* fullscreenDesc, RHICommandQueue* commandQueue) override final;

	RHICommandAllocator* CreateCommandAllocator(const RHICommandListType listType) override final;

	RHICommandList* CreateCommandList(const UINT nodeMask, const RHICommandListType type, RHICommandAllocator* allocator, RHIPipelineState* pso) override final;

	void SerializeRootSignature(
		const RHIRootSignatureDesc*		desc,
		const RHIRootSignatureVersion	version,
		RHIBlob**						blob,
		RHIBlob**						errorBlob) override final;

	void SerializeVersionedRootSignature(
		const RHIVersionedRootSignatureDesc*	desc,
		const RHIRootSignatureVersion			maxVersion,
		RHIBlob**								blob,
		RHIBlob**								errorBlob) override final;

	RHIRootSignature* CreateRootSignature(
		const UINT					nodeMask,
		const void*					blobWithRootSignature,
		const SIZE_T				blobLengthInBytes) override final;

	RHIPipelineState* CreateGraphicsPipelineState(RHIGraphicsPipelineStateDesc* psoDesc) override final;
	RHIPipelineState* CreateComputePipelineState(RHIComputePipelineStateDesc* psoDesc) override final;

	RHIDescriptorHeap* CreateDescriptorHeap(const RHIDescriptorHeapDesc* desc) override final;

	UINT GetDescriptorHandleIncrementSize(RHIDescriptorHeapType type) override final;

	void CreateRenderTargetView(
		RHIResource*					resource,
		const RHIRenderTargetViewDesc*	desc,
		RHICPUDescriptorHandle			destDescriptor) override final;

	void CreateDepthStencilView(
		RHIResource*					resource,
		const RHIDepthStencilViewDesc*	desc,
		RHICPUDescriptorHandle			destDescriptor) override final;

	void CreateShaderResourceView(
		RHIResource*						resource,
		const RHIShaderResourceViewDesc*	desc,
		RHICPUDescriptorHandle				destDescriptor) override final;

	void CreateConstantBufferView(const RHIConstantBufferViewDesc* desc, const RHICPUDescriptorHandle destDescriptor) override final;

	void CreateUnorderedAccessView(
		RHIResource*				resource,
		RHIResource*				counterResource,
		RHIUnorderedAccessViewDesc*	desc,
		RHICPUDescriptorHandle		destDescriptor) override final;

	RHIFence* CreateFence(const UINT64 initialValue, const RHIFenceFlags flags) override final;

	RHIResource* CreateCommittedResource(
		const RHIHeapProperties*	heapProperties,
		const RHIHeapFlag			heapFlags,
		const RHIResourceDesc*		desc,
		const RHIResourceState		initialResourceStates,
		const RHIClearValue*		optimizedClearValue) override final;

	RHIResource* CreateTextureFromFile(const String& path) override final;
	void LoadTextureFromFile(const String& fullPath, RHIResourceDesc* desc, RHISubresourceData* subresData) override final;

	void CopyDescriptorsSimple(
		const UINT				numDescriptors,
		RHICPUDescriptorHandle	destDescriptorRangeStart,
		RHICPUDescriptorHandle	srcDescriptorRangeStart,
		RHIDescriptorHeapType	descriptorHeapsType) override final;

	void GetCopyableFootprints(
		const RHIResourceDesc*			desc,
		const UINT						firstSubresource,
		const UINT						numSubresource,
		const UINT64					baseOffset,
		RHIPlacedSubresourceFootprint*	layouts,
		UINT*							numRows,
		UINT64*							rowSizeInBytes,
		UINT64*							totalBytes) override final;

public:
	// Uses UpdateSubresources and allocates a new upload buffer.
	void UploadResource(RHIResource* destResource, const UINT firstSubresource, const UINT numSubresources, RHISubresourceData* subresources) override final;

public:
	UINT64 GetRequiredIntermediateSize(RHIResource* destinationResource, const UINT firstSubresource, const UINT numSubresources) override final;

	UINT64 UpdateSubresources(
		RHICommandList*		cmdList,
		RHIResource*		destResource,
		RHIResource*		intermediate,
		const UINT64		intermediateOffset,
		const UINT			firstSubresource,
		const UINT			numSubresources,
		RHISubresourceData*	srcData) override final;

	UINT64 UpdateSubresources(
		const UINT			maxSubresource,
		RHICommandList*		cmdList,
		RHIResource*		destResource,
		RHIResource*		intermediate,
		const UINT64		intermediateOffset,
		const UINT			firstSubresource,
		const UINT			numSubresources,
		RHISubresourceData*	srcData,
		const bool			useFullBuffer) override final;

	UINT64 UpdateSubresources(
		RHICommandList*							cmdList,
		RHIResource*							destResource,
		RHIResource*							intermediate,
		const UINT								firstSubresource,
		const UINT								numSubresources,
		const UINT64							requiredSize,
		const RHIPlacedSubresourceFootprint*	layouts,
		const UINT*								numRows,
		const UINT64*							rowSizeInBytes,
		const RHISubresourceData*				srcData) override final;

protected:
	void GetHardwareAdapter(IDXGIFactory5* factory, IDXGIAdapter1** outAdapter);
	void CreateFactory();

protected:
	void LoadMetaData(const String& path, DirectX::TexMetadata& metadata, std::vector<D3D12_SUBRESOURCE_DATA>& subresources);

protected:
	void ClearTmpImages();

protected:
	ComPtr<ID3D12Device>							mDevice;
	ComPtr<IDXGIFactory5>							mFactory;

	D3D_FEATURE_LEVEL								mFeatureLevel;

	HWND											mWndHandle;
	IRenderer*										mRenderer;

	std::vector<DirectX::ScratchImage*>				mTmpImages;
	static const size_t								kMaxNumTmpImages = 128;

protected:
	Microsoft::WRL::Wrappers::RoInitializeWrapper	mRunTimeInitializerWrapper;
};
