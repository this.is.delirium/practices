#pragma once
#include "StdafxRHI.h"
#include "D3D12PipelineState.h"

class D3D12GraphicsPipelineState : public D3D12PipelineState
{
public:
	D3D12GraphicsPipelineState(const RHIGraphicsPipelineStateDesc* psoDesc)
		: D3D12PipelineState() { }

	void Release() override final { };

/*public:
	ComPtr<ID3D12PipelineState>& PSO() { return mPSO; }

protected:
	ComPtr<ID3D12PipelineState> mPSO;*/
};
