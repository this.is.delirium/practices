#include "StdafxRHI.h"
#include "D3D12Resource.h"

/**
 * Function : Initialize
 */
void D3D12Resource::InitializeSubData(const UINT subresource, const void* data, const size_t size)
{
	RHIRange range(0, 0);
	UINT8* dataBegin;
	Map(subresource, &range, reinterpret_cast<void**>(&dataBegin));
	memcpy(dataBegin, data, size);
	Unmap(subresource, &range);
}

/**
 * Function : WriteToSubresource
 */
void D3D12Resource::WriteToSubresource(UINT dstSubresource, const RHIBox* dstBox, const void* srcData, const UINT srcRowPitch, const UINT srcDepthPitch)
{
	const D3D12_BOX* d3d12Box = ToD3D12Structure<D3D12_BOX>(dstBox);
	mResource->WriteToSubresource(dstSubresource, d3d12Box, srcData, srcRowPitch, srcDepthPitch);
}
