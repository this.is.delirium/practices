#pragma once
#include "StdafxRHI.h"
#include <RHI.h>

class D3D12Fence : public RHIFence
{
public:
	inline void Release() override final { }

	inline ComPtr<ID3D12Fence>& Fence() { return mFence; }

public:
	UINT64 GetCompletedValue() override final
	{
		return mFence->GetCompletedValue();
	}

	void SetEventOnCompletion(const UINT64 value, HANDLE event) override final
	{
		ThrowIfFailed(mFence->SetEventOnCompletion(value, event), __TEXT("Can't set an event on completion."));
	}

protected:
	ComPtr<ID3D12Fence>	mFence;
};
