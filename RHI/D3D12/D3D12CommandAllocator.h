#pragma once
#include "StdafxRHI.h"
#include "RHI.h"

class D3D12CommandAllocator : public RHICommandAllocator
{
public:
	void Release() override final { }

	ComPtr<ID3D12CommandAllocator>& Allocator() { return mCommandAllocator; }

public:
	void Reset() override final
	{
		ThrowIfFailed(mCommandAllocator->Reset(), __TEXT("Can't reset a command allocator."));
	}

protected:
	ComPtr<ID3D12CommandAllocator> mCommandAllocator;
};
