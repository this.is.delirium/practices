#include "StdafxRHI.h"
#include "D3D12Device.h"

#include "D3D12CommandAllocator.h"
#include "D3D12CommandList.h"
#include "D3D12CommandQueue.h"
#include "D3D12ComputePipelineState.h"
#include "D3D12DescriptorHeap.h"
#include "D3D12Fence.h"
#include "D3D12GraphicsPipelineState.h"
#include "D3D12Renderer.h"
#include "D3D12Resource.h"
#include "D3D12RootSignature.h"
#include "D3D12SwapChain.h"
#include <Logger.h>
#include "private/d3dx12.h"
#include <StringUtils.h>

/**
 * Function : Release
 */
void D3D12Device::Release()
{
	ClearTmpImages();
}

/**
 * Function : ClearTmpImages
 */
void D3D12Device::ClearTmpImages()
{
	for (auto& image : mTmpImages)
		RHISafeRelease(image);

	mTmpImages.clear();
}

/**
 * Function : Initialize
 */
bool D3D12Device::Initialize(HWND hWnd)
{
	mWndHandle = hWnd;

	CreateFactory();

	ComPtr<IDXGIAdapter1> adapter;
	GetHardwareAdapter(mFactory.Get(), &adapter);

	ThrowIfFailed(D3D12CreateDevice(adapter.Get(), mFeatureLevel, IID_PPV_ARGS(&mDevice)), __TEXT("Can't create a device for DX12"));

	if (FAILED(mRunTimeInitializerWrapper))
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Can't initialize a thread to use Windows Runtime APIs."));

	return true;
}

/**
 * Function : CreateFactory
 */
void D3D12Device::CreateFactory()
{
#ifdef _DEBUG
	UINT flag = DXGI_CREATE_FACTORY_DEBUG;
#else
	UINT flag = 0;
#endif

	ThrowIfFailed(CreateDXGIFactory2(flag, IID_PPV_ARGS(&mFactory)), __TEXT("Can't create a DXGI factory."));
}

/**
 * Function : GetHardwareAdapter
 */
void D3D12Device::GetHardwareAdapter(IDXGIFactory5* factory, IDXGIAdapter1** outAdapter)
{
	ComPtr<IDXGIAdapter1> adapter;
	*outAdapter = nullptr;

	mFeatureLevel = D3D_FEATURE_LEVEL_9_1;
	D3D_FEATURE_LEVEL levels[] =
	{
		D3D_FEATURE_LEVEL_12_1,
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0
	};

	DXGI_ADAPTER_DESC1 desc;
	for (UINT adapterIndex = 0; factory->EnumAdapters1(adapterIndex, &adapter) != DXGI_ERROR_NOT_FOUND; ++adapterIndex)
	{
		adapter->GetDesc1(&desc);

		if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
			continue;

		for (int i = 0; i < _countof(levels); ++i)
		{
			if (SUCCEEDED(D3D12CreateDevice(nullptr, levels[i], _uuidof(ID3D12Device), nullptr)))
			{
				mFeatureLevel = levels[i];
				break;
			}
		}

		if (mFeatureLevel != D3D_FEATURE_LEVEL_9_1)
			break;
	}

#ifdef _DEBUG
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Adapter desc: %s"), desc.Description);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Dedicated memory: in bytes - %u, in kbytes - %u, in mbytes - %u"), desc.DedicatedVideoMemory, desc.DedicatedVideoMemory / 1024, desc.DedicatedVideoMemory / 1048576);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Shared memory: in bytes - %u, in kbytes - %u, in mbytes - %u"), desc.SharedSystemMemory, desc.SharedSystemMemory / 1024, desc.SharedSystemMemory / 1048576);
#endif

	*outAdapter = adapter.Detach();
}

/**
 * Function : CheckFeatureSuppor
 */
bool D3D12Device::CheckFeatureSupport(
	RHIFeature	feature,
	void*		featureSupportData,
	const UINT	featureSupportDataSize)
{
	D3D12_FEATURE d3d12Feature = static_cast<D3D12_FEATURE> (feature);

	return mDevice->CheckFeatureSupport(d3d12Feature, featureSupportData, featureSupportDataSize) == S_OK;
}

/**
 * Function : CreateCommandQueue
 */
RHICommandQueue* D3D12Device::CreateCommandQueue(const RHICommandQueueDesc* desc)
{
	D3D12CommandQueue* queue = new D3D12CommandQueue();

	const D3D12_COMMAND_QUEUE_DESC* d3d12Desc = ToD3D12Structure<const D3D12_COMMAND_QUEUE_DESC> (desc);

	ThrowIfFailed(mDevice->CreateCommandQueue(d3d12Desc, IID_PPV_ARGS(&queue->Queue())), __TEXT("Can't create a command queue."));

	return queue;
}

/**
 * Function : CreateSwapChain
 */
RHISwapChain* D3D12Device::CreateSwapChain(RHISwapChainDesc* desc, RHICommandQueue* commandQueue)
{
	D3D12CommandQueue*		queue		= ToD3D12Structure<D3D12CommandQueue>(commandQueue);
	D3D12SwapChain*			swapChain	= new D3D12SwapChain();
	DXGI_SWAP_CHAIN_DESC*	dxgiDesc	= ToD3D12Structure<DXGI_SWAP_CHAIN_DESC>(desc);

	ComPtr<IDXGISwapChain> dxgiSwapChain;
	ThrowIfFailed(mFactory->CreateSwapChain(queue->Queue().Get(), dxgiDesc, &dxgiSwapChain), __TEXT("Can't create a swap chain."));

	ThrowIfFailed(dxgiSwapChain.As(&swapChain->SwapChain()), __TEXT("Can't query interface for IDXGISwapChain4."));

	ThrowIfFailed(mFactory->MakeWindowAssociation(desc->OutputWindow, DXGI_MWA_NO_ALT_ENTER), __TEXT("Can't make window association"));

	return swapChain;
}

/**
 * Function : CreateSwapChain
 */
RHISwapChain* D3D12Device::CreateSwapChain(RHISwapChainDesc1* desc, RHISwapChainFullscreenDesc* fullscreenDesc, RHICommandQueue* commandQueue)
{
	ID3D12CommandQueue*					d3d12Queue			= ToD3D12Structure<D3D12CommandQueue>(commandQueue)->Queue().Get();
	D3D12SwapChain*						swapChain			= new D3D12SwapChain();
	DXGI_SWAP_CHAIN_DESC1*				dxgiDesc1			= ToD3D12Structure<DXGI_SWAP_CHAIN_DESC1>(desc);
	DXGI_SWAP_CHAIN_FULLSCREEN_DESC*	d3d12FullscreenDesc	= ToD3D12Structure<DXGI_SWAP_CHAIN_FULLSCREEN_DESC>(fullscreenDesc);

	ComPtr<IDXGISwapChain1> dxgiSwapChain;
	ThrowIfFailed(mFactory->CreateSwapChainForHwnd(d3d12Queue, mWndHandle, dxgiDesc1, d3d12FullscreenDesc, NULL, &dxgiSwapChain), __TEXT("Can't create a swap chain 1."));

	ThrowIfFailed(dxgiSwapChain.As(&swapChain->SwapChain()), __TEXT("Can't query interface for IDXGISwapChain4."));

	ThrowIfFailed(mFactory->MakeWindowAssociation(mWndHandle, DXGI_MWA_NO_ALT_ENTER), __TEXT("Can't make window association."));

	return swapChain;
}

/**
 * Function : CreateCommandAllocator
 */
RHICommandAllocator* D3D12Device::CreateCommandAllocator(const RHICommandListType listType)
{
	D3D12CommandAllocator* commandAllocator = new D3D12CommandAllocator();

	D3D12_COMMAND_LIST_TYPE type = static_cast<D3D12_COMMAND_LIST_TYPE>(listType);
	ThrowIfFailed(mDevice->CreateCommandAllocator(type, IID_PPV_ARGS(&commandAllocator->Allocator())), __TEXT("Can't create a command Allocator."));

	return commandAllocator;
}

/**
 * Function : CreateCommandList
 */
RHICommandList* D3D12Device::CreateCommandList(const UINT nodeMask, const RHICommandListType type, RHICommandAllocator* allocator, RHIPipelineState* pso)
{
	D3D12CommandList* list = new D3D12CommandList();
	D3D12CommandAllocator* d3d12Allocator = ToD3D12Structure<D3D12CommandAllocator>(allocator);
	D3D12_COMMAND_LIST_TYPE d3d12Type = static_cast<D3D12_COMMAND_LIST_TYPE>(type);
	ThrowIfFailed(mDevice->CreateCommandList(nodeMask, d3d12Type, d3d12Allocator->Allocator().Get(), nullptr, IID_PPV_ARGS(&list->List())), __TEXT("Can't create a command list."));

	//list->Close();

	return list;
}

/**
 * Function : SerializeRootSignature
 */
void D3D12Device::SerializeRootSignature(
	const RHIRootSignatureDesc*		desc,
	const RHIRootSignatureVersion	version,
	RHIBlob**						blob,
	RHIBlob**						errorBlob)
{
	*blob		= new D3D12Blob();
	*errorBlob	= new D3D12Blob();

	const D3D12_ROOT_SIGNATURE_DESC*	d3d12Desc		= ToD3D12Structure<D3D12_ROOT_SIGNATURE_DESC>(desc);
	ID3DBlob**							d3dBlob			= &static_cast<D3D12Blob*> (*blob)->mBlob;
	ID3DBlob**							d3dErrorBlob	= &static_cast<D3D12Blob*> (*errorBlob)->mBlob;
	D3D_ROOT_SIGNATURE_VERSION			d3d12Version	= static_cast<D3D_ROOT_SIGNATURE_VERSION>(version);

	HRESULT hr = D3D12SerializeRootSignature(d3d12Desc, d3d12Version, d3dBlob, d3dErrorBlob);
	if (FAILED(hr))
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Can't serialize a root signature."));
		std::string message((char*)(*d3dErrorBlob)->GetBufferPointer());
#ifdef _UNICODE
		SLogger::Instance().AddMessage(LogMessageType::Information, StringUtils::Converter::StringToWString(message));
#else
		SLogger::Instance().AddMessage(LogMessageType::Information, message);
#endif
	}
}

/**
 * Function : SerializeVersionedRootSignature
 */
void D3D12Device::SerializeVersionedRootSignature(
	const RHIVersionedRootSignatureDesc*	desc,
	const RHIRootSignatureVersion			maxVersion,
	RHIBlob**								blob,
	RHIBlob**								errorBlob)
{
	*blob		= new D3D12Blob();
	*errorBlob	= new D3D12Blob();

	const D3D12_VERSIONED_ROOT_SIGNATURE_DESC*	d3d12Desc		= ToD3D12Structure<D3D12_VERSIONED_ROOT_SIGNATURE_DESC>(desc);
	ID3DBlob**									d3dBlob			= &static_cast<D3D12Blob*> (*blob)->mBlob;
	ID3DBlob**									d3dErrorBlob	= &static_cast<D3D12Blob*> (*errorBlob)->mBlob;
	D3D_ROOT_SIGNATURE_VERSION					d3d12MaxVersion	= static_cast<D3D_ROOT_SIGNATURE_VERSION>(maxVersion);

	HRESULT hr = D3DX12SerializeVersionedRootSignature(d3d12Desc, d3d12MaxVersion, d3dBlob, d3dErrorBlob);
	if (FAILED(hr))
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Can't serialize a versioned root signature."));
		std::string message((char*)(*d3dErrorBlob)->GetBufferPointer());
#ifdef _UNICODE
		SLogger::Instance().AddMessage(LogMessageType::Information, StringUtils::Converter::StringToWString(message));
#else
		SLogger::Instance().AddMessage(LogMessageType::Information, message);
#endif
	}
}

/**
 * Function : CreateRootSignature
 */
RHIRootSignature* D3D12Device::CreateRootSignature(
	const UINT					nodeMask,
	const void*					blobWithRootSignature,
	const SIZE_T				blobLengthInBytes)
{
	D3D12RootSignature* rootSignature = new D3D12RootSignature();

	HRESULT hr = mDevice->CreateRootSignature(nodeMask, blobWithRootSignature, blobLengthInBytes, IID_PPV_ARGS(&rootSignature->RootSignature()));
	if (FAILED(hr))
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Can't create a root signature."));
		return nullptr;
	}

	return rootSignature;
}

/**
 * Function : CreateGraphicsPipelineState
 */
RHIPipelineState* D3D12Device::CreateGraphicsPipelineState(RHIGraphicsPipelineStateDesc* psoDesc)
{
	D3D12GraphicsPipelineState* pso = new D3D12GraphicsPipelineState(psoDesc);
	D3D12_GRAPHICS_PIPELINE_STATE_DESC d3d12Desc = *ToD3D12Structure<D3D12_GRAPHICS_PIPELINE_STATE_DESC> (psoDesc);
	d3d12Desc.pRootSignature = ToD3D12Structure<D3D12RootSignature> (psoDesc->RootSignature)->RootSignature().Get();

	ThrowIfFailed(mDevice->CreateGraphicsPipelineState(&d3d12Desc, IID_PPV_ARGS(&pso->PSO())), __TEXT("Can't create a graphics pipeline state."));

	return pso;
}

/**
 * Function : CreateComputePipelineState
 */
RHIPipelineState* D3D12Device::CreateComputePipelineState(RHIComputePipelineStateDesc* psoDesc)
{
	D3D12ComputePipelineState* pso = new D3D12ComputePipelineState(psoDesc);
	D3D12_COMPUTE_PIPELINE_STATE_DESC d3d12Desc = *ToD3D12Structure<D3D12_COMPUTE_PIPELINE_STATE_DESC>(psoDesc);
	d3d12Desc.pRootSignature = ToD3D12Structure<D3D12RootSignature>(psoDesc->RootSignature)->RootSignature().Get();

	ThrowIfFailed(mDevice->CreateComputePipelineState(&d3d12Desc, IID_PPV_ARGS(&pso->PSO())), __TEXT("Can't create a compute pipeline state."));

	return pso;
}

/**
 * Function : CreateDescriptorHeap
 */
RHIDescriptorHeap* D3D12Device::CreateDescriptorHeap(const RHIDescriptorHeapDesc* desc)
{
	D3D12DescriptorHeap* heap = new D3D12DescriptorHeap();
	const D3D12_DESCRIPTOR_HEAP_DESC* d3d12Desc = ToD3D12Structure<D3D12_DESCRIPTOR_HEAP_DESC>(desc);

	ThrowIfFailed(mDevice->CreateDescriptorHeap(d3d12Desc, IID_PPV_ARGS(&heap->Heap())), __TEXT("Can't create a descriptor heap."));

	return heap;
}

/**
 * Function : GetDescriptorHandleIncrementSize
 */
UINT D3D12Device::GetDescriptorHandleIncrementSize(RHIDescriptorHeapType type)
{
	D3D12_DESCRIPTOR_HEAP_TYPE d3d12Type = static_cast<D3D12_DESCRIPTOR_HEAP_TYPE> (type);
	return mDevice->GetDescriptorHandleIncrementSize(d3d12Type);
}

/**
 * Function : CreateFence
 */
RHIFence* D3D12Device::CreateFence(const UINT64 initialValue, const RHIFenceFlags flags)
{
	D3D12Fence* fence = new D3D12Fence();

	D3D12_FENCE_FLAGS d3d12Flags = static_cast<D3D12_FENCE_FLAGS>(flags);

	ThrowIfFailed(mDevice->CreateFence(initialValue, d3d12Flags, IID_PPV_ARGS(&fence->Fence())), __TEXT("Can't create a fence."));

	return fence;
}

/**
 * Function : CreateCommittedResource
 */
RHIResource* D3D12Device::CreateCommittedResource(
	const RHIHeapProperties*	heapProperties,
	const RHIHeapFlag			heapFlags,
	const RHIResourceDesc*		desc,
	const RHIResourceState		initialResourceStates,
	const RHIClearValue*		optimizedClearValue)
{
	D3D12Resource* d3d12Resource = new D3D12Resource();

	const D3D12_HEAP_PROPERTIES*	d3d12HeapProperties	= reinterpret_cast<const D3D12_HEAP_PROPERTIES*>(heapProperties);
	const D3D12_HEAP_FLAGS			d3d12HeapFlags		= static_cast<D3D12_HEAP_FLAGS>(heapFlags);
	const D3D12_RESOURCE_DESC*		d3d12ResourceDesc	= reinterpret_cast<const D3D12_RESOURCE_DESC*>(desc);
	const D3D12_RESOURCE_STATES		d3d12ResourceStates	= static_cast<D3D12_RESOURCE_STATES>(initialResourceStates);
	const D3D12_CLEAR_VALUE*		d3d12ClearValue		= reinterpret_cast<const D3D12_CLEAR_VALUE*>(optimizedClearValue);

	ThrowIfFailed(mDevice->CreateCommittedResource(
		d3d12HeapProperties,
		d3d12HeapFlags,
		d3d12ResourceDesc,
		d3d12ResourceStates,
		d3d12ClearValue,
		IID_PPV_ARGS(&d3d12Resource->Resource())), __TEXT("Can't create a committed resource."));

	return d3d12Resource;
}

/**
 * Function : CreateConstantBufferView
 */
void D3D12Device::CreateConstantBufferView(const RHIConstantBufferViewDesc* desc, const RHICPUDescriptorHandle destDescriptor)
{
	const D3D12_CONSTANT_BUFFER_VIEW_DESC* d3d12Desc = ToD3D12Structure<D3D12_CONSTANT_BUFFER_VIEW_DESC>(desc);
	D3D12_CPU_DESCRIPTOR_HANDLE d3d12DestDescriptor; d3d12DestDescriptor.ptr = destDescriptor.ptr;

	mDevice->CreateConstantBufferView(d3d12Desc, d3d12DestDescriptor);
}

/**
 * Function : CreateRenderTargetView
 */
void D3D12Device::CreateRenderTargetView(
	RHIResource*					resource,
	const RHIRenderTargetViewDesc*	desc,
	RHICPUDescriptorHandle			destDescriptor)
{
	D3D12Resource* d3d12Resource = ToD3D12Structure<D3D12Resource>(resource);
	const D3D12_RENDER_TARGET_VIEW_DESC* d3d12Desc = ToD3D12Structure<D3D12_RENDER_TARGET_VIEW_DESC>(desc);
	D3D12_CPU_DESCRIPTOR_HANDLE d3d12DestDescriptor;
	d3d12DestDescriptor.ptr = destDescriptor.ptr;

	mDevice->CreateRenderTargetView(d3d12Resource->Resource().Get(), d3d12Desc, d3d12DestDescriptor);
}

/**
 * Function : CreateDepthStencilView
 */
void D3D12Device::CreateDepthStencilView(
	RHIResource*					resource,
	const RHIDepthStencilViewDesc*	desc,
	RHICPUDescriptorHandle			destDescriptor)
{
	D3D12Resource* d3d12Resource = ToD3D12Structure<D3D12Resource>(resource);
	const D3D12_DEPTH_STENCIL_VIEW_DESC* d3d12Desc = ToD3D12Structure<D3D12_DEPTH_STENCIL_VIEW_DESC>(desc);
	D3D12_CPU_DESCRIPTOR_HANDLE d3d12DestDescriptor;
	d3d12DestDescriptor.ptr = destDescriptor.ptr;

	mDevice->CreateDepthStencilView(d3d12Resource->Resource().Get(), d3d12Desc, d3d12DestDescriptor);
}

/**
 * Function : CreateShaderResourceView
 */
void D3D12Device::CreateShaderResourceView(
	RHIResource*						resource,
	const RHIShaderResourceViewDesc*	desc,
	RHICPUDescriptorHandle				destDescriptor)
{
	D3D12Resource* d3d12Resource = ToD3D12Structure<D3D12Resource>(resource);
	const D3D12_SHADER_RESOURCE_VIEW_DESC* d3d12Desc = ToD3D12Structure<D3D12_SHADER_RESOURCE_VIEW_DESC>(desc);
	D3D12_CPU_DESCRIPTOR_HANDLE d3d12DestDescriptor; d3d12DestDescriptor.ptr = destDescriptor.ptr;

	mDevice->CreateShaderResourceView(d3d12Resource->Resource().Get(), d3d12Desc, d3d12DestDescriptor);
}

/**
 * Function : CreateUnorderedAccessView
 */
void D3D12Device::CreateUnorderedAccessView(
	RHIResource*				resource,
	RHIResource*				counterResource,
	RHIUnorderedAccessViewDesc*	desc,
	RHICPUDescriptorHandle		destDescriptor)
{
	ID3D12Resource*						d3d12Resource			= ToD3D12Structure<D3D12Resource>(resource)->Resource().Get();
	ID3D12Resource*						d3d12CounterResource	= (counterResource) ? ToD3D12Structure<D3D12Resource>(counterResource)->Resource().Get() : nullptr;
	D3D12_UNORDERED_ACCESS_VIEW_DESC*	d3d12Desc				= ToD3D12Structure<D3D12_UNORDERED_ACCESS_VIEW_DESC>(desc);
	D3D12_CPU_DESCRIPTOR_HANDLE			d3d12Handle; d3d12Handle.ptr = destDescriptor.ptr;

	mDevice->CreateUnorderedAccessView(d3d12Resource, d3d12CounterResource, d3d12Desc, d3d12Handle);
}

/**
 * Function : CopyDescriptorSimple
 */
void D3D12Device::CopyDescriptorsSimple(
	const UINT				numDescriptors,
	RHICPUDescriptorHandle	destDescriptorRangeStart,
	RHICPUDescriptorHandle	srcDescriptorRangeStart,
	RHIDescriptorHeapType	descriptorHeapsType)
{
	D3D12_CPU_DESCRIPTOR_HANDLE	d3d12DestHandle;	d3d12DestHandle.ptr	= destDescriptorRangeStart.ptr;
	D3D12_CPU_DESCRIPTOR_HANDLE	d3d12SrcHandle;		d3d12SrcHandle.ptr	= srcDescriptorRangeStart.ptr;
	D3D12_DESCRIPTOR_HEAP_TYPE	d3d12DescrType = static_cast<D3D12_DESCRIPTOR_HEAP_TYPE>(descriptorHeapsType);

	mDevice->CopyDescriptorsSimple(numDescriptors, d3d12DestHandle, d3d12SrcHandle, d3d12DescrType);
}

/**
 * Function : GetCopyableFootprints
 */
void D3D12Device::GetCopyableFootprints(
	const RHIResourceDesc*			desc,
	const UINT						firstSubresource,
	const UINT						numSubresource,
	const UINT64					baseOffset,
	RHIPlacedSubresourceFootprint*	layouts,
	UINT*							numRows,
	UINT64*							rowSizeInBytes,
	UINT64*							totalBytes)
{
	const D3D12_RESOURCE_DESC* d3d12Desc = ToD3D12Structure<D3D12_RESOURCE_DESC>(desc);
	D3D12_PLACED_SUBRESOURCE_FOOTPRINT* d3d12Layouts = ToD3D12Structure<D3D12_PLACED_SUBRESOURCE_FOOTPRINT>(layouts);

	mDevice->GetCopyableFootprints(d3d12Desc, firstSubresource, numSubresource, baseOffset,
		d3d12Layouts, numRows, rowSizeInBytes, totalBytes);
}

/**
 * Function : UploadResource
 */
void D3D12Device::UploadResource(RHIResource* destResource, const UINT firstSubresource, const UINT numSubresources, RHISubresourceData* subresources)
{
	const UINT64	uploadBufferSize = GetRequiredIntermediateSize(destResource, 0, numSubresources) + D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT;
	RHIResource*	uploadResource = CreateCommittedResource(
		&RHIHeapProperties(RHIHeapType::Upload),
		RHIHeapFlag::None,
		&RHIResourceDesc::Buffer(uploadBufferSize),
		RHIResourceState::GenericRead,
		nullptr);

	uploadResource->SetName(__TEXT("Upload buffer"));

	mRenderer->UploadCommandList()->Reset(mRenderer->UploadCommandAllocator(), nullptr);

	ID3D12Resource* d3d12Resource = ToD3D12Structure<D3D12Resource>(destResource)->Resource().Get();
	ID3D12GraphicsCommandList* cmdList = ToD3D12Structure<D3D12CommandList>(mRenderer->UploadCommandList())->List().Get();
	D3D12_SUBRESOURCE_DATA* d3d12Subresources = ToD3D12Structure<D3D12_SUBRESOURCE_DATA>(subresources);
	::UpdateSubresources(cmdList, d3d12Resource, ToD3D12Structure<D3D12Resource>(uploadResource)->Resource().Get(), 0, 0, numSubresources, d3d12Subresources);

	mRenderer->CommitCommandList(mRenderer->UploadCommandList());

	RHISafeRelease(uploadResource);
}

/**
 * Function : GetRequiredIntermediateSize
 */
UINT64 D3D12Device::GetRequiredIntermediateSize(RHIResource* destinationResource, const UINT firstSubresource, const UINT numSubresources)
{
	ID3D12Resource* d3d12Resource = ToD3D12Structure<D3D12Resource>(destinationResource)->Resource().Get();
	D3D12_RESOURCE_DESC desc = d3d12Resource->GetDesc();
	UINT64 requiredSize = 0;

	ID3D12Device* device;
	d3d12Resource->GetDevice(__uuidof(*device), reinterpret_cast<void**>(&device));
	device->GetCopyableFootprints(&desc, firstSubresource, numSubresources, 0, nullptr, nullptr, nullptr, &requiredSize);
	device->Release();

	return requiredSize;
}

/**
 * Function : UpdateSubresources
 */
UINT64 D3D12Device::UpdateSubresources(
	RHICommandList*		cmdList,
	RHIResource*		destResource,
	RHIResource*		intermediate,
	const UINT64		intermediateOffset,
	const UINT			firstSubresource,
	const UINT			numSubresources,
	RHISubresourceData*	srcData)
{
	UINT64 RequiredSize = 0;
	UINT64 MemToAlloc = static_cast<UINT64>(sizeof(D3D12_PLACED_SUBRESOURCE_FOOTPRINT) + sizeof(UINT) + sizeof(UINT64)) * numSubresources;
	if (MemToAlloc > SIZE_MAX)
	{
		return 0;
	}
	void* pMem = HeapAlloc(GetProcessHeap(), 0, static_cast<SIZE_T>(MemToAlloc));
	if (pMem == NULL)
	{
		return 0;
	}
	D3D12_PLACED_SUBRESOURCE_FOOTPRINT* pLayouts = reinterpret_cast<D3D12_PLACED_SUBRESOURCE_FOOTPRINT*>(pMem);
	UINT64* pRowSizesInBytes = reinterpret_cast<UINT64*>(pLayouts + numSubresources);
	UINT* pNumRows = reinterpret_cast<UINT*>(pRowSizesInBytes + numSubresources);

	ID3D12Resource* d3d12Resource = ToD3D12Structure<D3D12Resource>(destResource)->Resource().Get();
	D3D12_RESOURCE_DESC Desc = d3d12Resource->GetDesc();
	ID3D12Device* pDevice;
	d3d12Resource->GetDevice(__uuidof(*pDevice), reinterpret_cast<void**>(&pDevice));
	pDevice->GetCopyableFootprints(&Desc, firstSubresource, numSubresources, intermediateOffset, pLayouts, pNumRows, pRowSizesInBytes, &RequiredSize);
	pDevice->Release();

	RHIPlacedSubresourceFootprint* layouts = reinterpret_cast<RHIPlacedSubresourceFootprint*>(pLayouts);
	UINT64 Result = UpdateSubresources(cmdList, destResource, intermediate, firstSubresource, numSubresources, RequiredSize, layouts, pNumRows, pRowSizesInBytes, srcData);
	HeapFree(GetProcessHeap(), 0, pMem);
	return Result;
}

/**
 * Function : UpdateSubresource
 */
UINT64 D3D12Device::UpdateSubresources(
	const UINT			maxSubresource,
	RHICommandList*		cmdList,
	RHIResource*		destResource,
	RHIResource*		intermediate,
	const UINT64		intermediateOffset,
	const UINT			firstSubresource,
	const UINT			numSubresources,
	RHISubresourceData*	srcData,
	const bool			useFullBuffer)
{
	UINT64 requiredSize = 0;

	std::vector<RHIPlacedSubresourceFootprint> layouts(maxSubresource);
	std::vector<UINT> numRows(maxSubresource);
	std::vector<UINT64> rowSizesInBytes(maxSubresource);

	D3D12_PLACED_SUBRESOURCE_FOOTPRINT* d3d12Layouts = ToD3D12Structure<D3D12_PLACED_SUBRESOURCE_FOOTPRINT>(&layouts[0]);

	ID3D12Resource* d3d12DestRes = ToD3D12Structure<D3D12Resource>(destResource)->Resource().Get();
	D3D12_RESOURCE_DESC desc = d3d12DestRes->GetDesc();

	if (!useFullBuffer)
		desc.Width = srcData->RowPitch;

	ID3D12Device* device;
	d3d12DestRes->GetDevice(__uuidof(*device), reinterpret_cast<void**>(&device));
	device->GetCopyableFootprints(&desc, firstSubresource, numSubresources, intermediateOffset, d3d12Layouts, &numRows[0], &rowSizesInBytes[0], &requiredSize);
	device->Release();

	return UpdateSubresources(cmdList, destResource, intermediate, firstSubresource, numSubresources, requiredSize, layouts.data(), numRows.data(), rowSizesInBytes.data(), srcData);
}

/**
 * Function : UpdateSubresource
 */
UINT64 D3D12Device::UpdateSubresources(
	RHICommandList*							cmdList,
	RHIResource*							destResource,
	RHIResource*							intermediate,
	const UINT								firstSubresource,
	const UINT								numSubresources,
	const UINT64							requiredSize,
	const RHIPlacedSubresourceFootprint*	layouts,
	const UINT*								numRows,
	const UINT64*							rowSizeInBytes,
	const RHISubresourceData*				srcData)
{
	ID3D12GraphicsCommandList*					pCmdList				= ToD3D12Structure<D3D12CommandList>(cmdList)->List().Get();
	ID3D12Resource*								pIntermediate			= ToD3D12Structure<D3D12Resource>(intermediate)->Resource().Get();
	ID3D12Resource*								pDestinationResource	= ToD3D12Structure<D3D12Resource>(destResource)->Resource().Get();
	const D3D12_PLACED_SUBRESOURCE_FOOTPRINT*	pLayouts				= ToD3D12Structure<D3D12_PLACED_SUBRESOURCE_FOOTPRINT>(layouts);
	const D3D12_SUBRESOURCE_DATA*				pSrcData				= ToD3D12Structure<D3D12_SUBRESOURCE_DATA>(srcData);

	return ::UpdateSubresources(pCmdList, pDestinationResource, pIntermediate, firstSubresource, numSubresources, requiredSize, pLayouts, numRows, rowSizeInBytes, pSrcData);
}

/**
 * Function : LoadMetaData
 */
void D3D12Device::LoadMetaData(const String& path, DirectX::TexMetadata& metadata, std::vector<D3D12_SUBRESOURCE_DATA>& subresources)
{
	if (mTmpImages.size() >= kMaxNumTmpImages)
		ClearTmpImages();

	mTmpImages.push_back(new DirectX::ScratchImage());

	DirectX::ScratchImage&	image = *mTmpImages.back();
	DWORD					flags = DirectX::CP_FLAGS::CP_FLAGS_NONE;

	size_t dotPos = path.find_last_of(__TEXT('.'));
	assert(dotPos != String::npos);

	String extension = path.substr(dotPos);
	ImageFormat format = GetImageFormat(extension);

	if (format <= ImageFormat::WicIco)
		ThrowIfFailed(DirectX::LoadFromWICFile(path.c_str(), flags, &metadata, image), __TEXT("Can't load the wic texture form the file '%s'"), path.c_str());
	else if (format == ImageFormat::DDS)
		ThrowIfFailed(DirectX::LoadFromDDSFile(path.c_str(), flags, &metadata, image), __TEXT("Can't load the dds texture from the file '%s'"), path.c_str());

	ThrowIfFailed(DirectX::PrepareUpload(mDevice.Get(), image.GetImages(), image.GetImageCount(), image.GetMetadata(), subresources), __TEXT("Can't prepare to upload image."));
}

/**
 * Function : CreateTextureFromFile
 */
RHIResource* D3D12Device::CreateTextureFromFile(const String& path)
{
	ClearTmpImages();

	D3D12Resource* texture = new D3D12Resource();
	ID3D12Resource* d3d12Texture = nullptr;

	DirectX::TexMetadata				metadata;
	std::vector<D3D12_SUBRESOURCE_DATA>	subresources;

	LoadMetaData(path, metadata, subresources);

	// ToDo: Reconsider later.
	ThrowIfFailed(DirectX::CreateTexture(mDevice.Get(), metadata, &d3d12Texture), __TEXT("Can't create a texture for the file '%s'"), path.c_str());

	texture->Resource().Attach(d3d12Texture);

	UploadResource(texture, 0, subresources.size(), reinterpret_cast<RHISubresourceData*> (subresources.data()));

	return texture;
}

/**
 * Function : LoadTextureFromFile
 */
void D3D12Device::LoadTextureFromFile(const String& path, RHIResourceDesc* desc, RHISubresourceData* subresData)
{
	DirectX::TexMetadata				metadata;
	std::vector<D3D12_SUBRESOURCE_DATA>	subresources;

	LoadMetaData(path, metadata, subresources);

	if (subresources.size() == 1)
		*subresData = *reinterpret_cast<RHISubresourceData*>(&subresources[0]);

	desc->DepthOrArraySize	= metadata.arraySize;
	desc->Dimension			= static_cast<RHIResourceDimension>(metadata.dimension);
	desc->Width				= metadata.width;
	desc->Height			= metadata.height;
	desc->Format			= static_cast<RHIFormat> (metadata.format);
	desc->MipLevels			= metadata.mipLevels;
}
