#pragma once
#include "StdafxRHI.h"
#include "RHI.h"

class D3D12CommandList : public RHICommandList
{
public:
	~D3D12CommandList() { }
	void Release() { }

	ComPtr<ID3D12GraphicsCommandList>& List() { return mCommandList; }

	void ClearRenderTargetView(
		RHICPUDescriptorHandle	renderTargetView,
		const FLOAT*			colorRGBA,
		const UINT				numRects,
		const RHIRect*			rects) override final;

	void ClearDepthStencilView(
		RHICPUDescriptorHandle	depthStencilView,
		RHIClearFlags			clearFlags,
		const FLOAT				depth,
		const UINT8				stencil,
		const UINT				numRects,
		const RHIRect*			rects) override final;

	void ClearUnorderedAccessViewUint(
		const RHIGPUDescriptorHandle	viewGPUHandleInCurrentHeap,
		const RHICPUDescriptorHandle	viewCPUHandle,
		RHIResource*					resource,
		const UINT						values[4],
		const UINT						numRects,
		const RHIRect*					rects) override final;

	void RSSetViewports(const UINT numViewports, const RHIViewport* viewports) override final;
	void RSSetScissorRects(const UINT numRects, const RHIRect* rects) override final;

	void SetPipelineState(RHIPipelineState* pso) override final;

	void SetGraphicsRootSignature(RHIRootSignature* rootSignature) override final;
	void SetGraphicsRootDescriptorTable(const UINT rootParameterIndex, const RHIGPUDescriptorHandle baseDescriptor) override final;
	void SetGraphicsRootConstantBufferView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation) override final;
	void SetGraphicsRoot32BitConstants(const UINT rootParameterIndex, const UINT num32VitValues, const void* data, const UINT destOffsetIn32BitValues) override final;

	void SetComputeRootSignature(RHIRootSignature* rootSignature) override final;
	void SetComputeRootDescriptorTable(const UINT rootParameterIndex, const RHIGPUDescriptorHandle baseDescirptor) override final;
	void SetComputeRootConstantBufferView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation) override final;
	void SetComputeRootUnorderedAccessView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation) override final;

	void SetDescriptorHeaps(const UINT numDescriptorHeaps, RHIDescriptorHeap** heaps) override final;

	void IASetIndexBuffer(const RHIIndexBufferView* view) override final;
	void IASetPrimitiveTopology(const RHIPrimitiveTopology topology) override final;
	void IASetVertexBuffers(const UINT startSlot, const UINT numViews, const RHIVertexBufferView* pViews) override final;

	void ResourceBarrier(const UINT numBarriers, const RHIResourceBarrier* barriers) override final;

	void CopyResource(RHIResource* dstResource, RHIResource* srcResource) override final;

	void CopyTextureRegion(
		const RHITextureCopyLocation*	dst,
		const UINT						dstX,
		const UINT						dstY,
		const UINT						dstZ,
		const RHITextureCopyLocation*	src,
		const RHIBox*					srcBox) override final;

	void CopyBufferRegion(
		RHIResource*	dstBuffer,
		UINT64			dstOffset,
		RHIResource*	srcBuffer,
		UINT64			srcOffset,
		UINT64			numBytes) override final;

	void OMSetBlendFactor(const FLOAT blendFactor[4]) override final;
	void OMSetRenderTargets(
		const UINT						numRenderTargetDescriptors,
		const RHICPUDescriptorHandle*	renderTargetDescriptors,
		const BOOL						RTsSingleHandleToDescriptorRange,
		const RHICPUDescriptorHandle*	depthStencilDescriptor) override final;

	void DrawInstanced(
		const uint32_t vertexCountPerIntsance,
		const uint32_t instancCount,
		const uint32_t startVertexLocation,
		const uint32_t startInstanceLocation) override final;

	void DrawIndexedInstanced(
		const uint32_t	indexCountPerInstance,
		const uint32_t	instanceCount,
		const uint32_t	startIndexLocation,
		const int32_t	baseVertexLocation,
		const uint32_t	startInstanceLocation) override final;

	void Dispatch(const UINT threadGroupCountX, const UINT threadGroupCountY, const UINT threadGroupCountZ) override final;

	void ExecuteBundle(RHICommandList* commandList) override final;

	void SOSetTargets(const UINT startSlot, const UINT numViews, const RHIStreamOutputBufferView* views) override final;
	void DrawStreamOutput(const RHIResource* streamOuput) override final { assert(false); }

	void Reset(RHICommandAllocator*, RHIPipelineState* pso) override final;
	void Close() override final;

protected:
	ComPtr<ID3D12GraphicsCommandList> mCommandList;
};
