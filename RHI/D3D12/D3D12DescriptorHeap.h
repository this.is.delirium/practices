#pragma once
#include "StdafxRHI.h"
#include <RHI.h>

class D3D12DescriptorHeap : public RHIDescriptorHeap
{
public:
	inline void Release() override final { }

	inline void SetName(CString name) override final { mHeap->SetName(name); }

	inline ComPtr<ID3D12DescriptorHeap>& Heap() { return mHeap; }

public:
	inline RHICPUDescriptorHandle GetCPUDescriptorHandleForHeapStart() override final
	{
		return RHICPUDescriptorHandle(mHeap->GetCPUDescriptorHandleForHeapStart().ptr);
	}

	inline RHIGPUDescriptorHandle GetGPUDescriptorHandleForHeapStart() override final
	{
		return RHIGPUDescriptorHandle(mHeap->GetGPUDescriptorHandleForHeapStart().ptr);
	}

protected:
	ComPtr<ID3D12DescriptorHeap> mHeap;
};
