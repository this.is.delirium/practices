#pragma once
#include <StdafxRHI.h>
#include <RHI.h>

class D3D12RootSignature : public RHIRootSignature
{
public:
	void Release() override final { }

	ComPtr<ID3D12RootSignature>& RootSignature() { return mRootSignature; }

protected:
	ComPtr<ID3D12RootSignature> mRootSignature;
};
