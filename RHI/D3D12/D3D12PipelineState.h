#pragma once
#include "RHI.h"

class D3D12PipelineState : public RHIPipelineState
{
public:
	ComPtr<ID3D12PipelineState>& PSO() { return mPSO; }

protected:
	ComPtr<ID3D12PipelineState> mPSO;
};
