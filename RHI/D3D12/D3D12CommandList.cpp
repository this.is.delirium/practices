#include "StdafxRHI.h"
#include "D3D12CommandList.h"
#include "D3D12CommandAllocator.h"
#include "D3D12DescriptorHeap.h"
#include "D3D12PipelineState.h"
#include "D3D12Resource.h"
#include "D3D12RootSignature.h"
#include <D3D12/private/d3dx12.h>

/**
 * Function : ClearRenderTargetView
 */
void D3D12CommandList::ClearRenderTargetView(
	RHICPUDescriptorHandle	renderTargetView,
	const FLOAT*			colorRGBA,
	const UINT				numRects,
	const RHIRect*			rects)
{
	D3D12_CPU_DESCRIPTOR_HANDLE d3d12RTView; d3d12RTView.ptr = renderTargetView.ptr;
	const D3D12_RECT* d3d12Rects = ToD3D12Structure<D3D12_RECT>(rects);

	mCommandList->ClearRenderTargetView(d3d12RTView, colorRGBA, numRects, d3d12Rects);
}

/**
 * Function : ClearDepthStencilView
 */
void D3D12CommandList::ClearDepthStencilView(
	RHICPUDescriptorHandle	depthStencilView,
	RHIClearFlags			clearFlags,
	const FLOAT				depth,
	const UINT8				stencil,
	const UINT				numRects,
	const RHIRect*			rects)
{
	D3D12_CPU_DESCRIPTOR_HANDLE d3d12Handle; d3d12Handle.ptr = depthStencilView.ptr;
	D3D12_CLEAR_FLAGS d3d12Flags = static_cast<D3D12_CLEAR_FLAGS>(clearFlags);
	const D3D12_RECT* d3d12Rects = ToD3D12Structure<D3D12_RECT>(rects);

	mCommandList->ClearDepthStencilView(d3d12Handle, d3d12Flags, depth, stencil, numRects, d3d12Rects);
}

/**
 * Function : ClearUnorderedAccessViewUint
 */
void D3D12CommandList::ClearUnorderedAccessViewUint(
	const RHIGPUDescriptorHandle	viewGPUHandleInCurrentHeap,
	const RHICPUDescriptorHandle	viewCPUHandle,
	RHIResource*					resource,
	const UINT						values[4],
	const UINT						numRects,
	const RHIRect*					rects)
{
	D3D12_GPU_DESCRIPTOR_HANDLE d3d12GpuHandle; d3d12GpuHandle.ptr = viewGPUHandleInCurrentHeap.ptr;
	D3D12_CPU_DESCRIPTOR_HANDLE d3d12CpuHandle; d3d12CpuHandle.ptr = viewCPUHandle.ptr;
	ID3D12Resource* d3d12Resource = ToD3D12Structure<D3D12Resource>(resource)->Resource().Get();
	const D3D12_RECT* d3d12Rects = ToD3D12Structure<D3D12_RECT>(rects);

	mCommandList->ClearUnorderedAccessViewUint(d3d12GpuHandle, d3d12CpuHandle, d3d12Resource, values, numRects, d3d12Rects);
}

/**
 * Function : RSSetViewports
 */
void D3D12CommandList::RSSetViewports(const UINT numViewports, const RHIViewport* viewports)
{
	const D3D12_VIEWPORT* d3d12Viewports = ToD3D12Structure<D3D12_VIEWPORT>(viewports);
	mCommandList->RSSetViewports(numViewports, d3d12Viewports);
}

/**
 * Function : RSSetScissorRects
 */
void D3D12CommandList::RSSetScissorRects(const UINT numRects, const RHIRect* rects)
{
	const D3D12_RECT* d3d12Rects = ToD3D12Structure<D3D12_RECT>(rects);
	mCommandList->RSSetScissorRects(numRects, d3d12Rects);
}

/**
 * Function : SetPipelineState
 */
void D3D12CommandList::SetPipelineState(RHIPipelineState* pso)
{
	D3D12PipelineState* d3d12PSO = ToD3D12Structure<D3D12PipelineState>(pso);

	mCommandList->SetPipelineState(d3d12PSO->PSO().Get());
}

/**
 * Function : Reset
 */
void D3D12CommandList::Reset(RHICommandAllocator* commandAllocator, RHIPipelineState* pso)
{
	D3D12CommandAllocator*	d3d12CommandAllocator	= ToD3D12Structure<D3D12CommandAllocator>(commandAllocator);
	D3D12PipelineState*		d3d12PSO				= ToD3D12Structure<D3D12PipelineState>(pso);
	ID3D12PipelineState*	targetPSO				= (d3d12PSO) ? d3d12PSO->PSO().Get() : nullptr;
	ThrowIfFailed(mCommandList->Reset(d3d12CommandAllocator->Allocator().Get(), targetPSO), __TEXT("Can't reset a command list."));
}

/**
 * Function : SetGraphicsRootConstantBufferView
 */
void D3D12CommandList::SetGraphicsRootConstantBufferView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation)
{
	mCommandList->SetGraphicsRootConstantBufferView(rootParameterIndex, bufferLocation);
}

/**
 * Function : SetGraphicsRoot32BitConstants
 */
void D3D12CommandList::SetGraphicsRoot32BitConstants(const UINT rootParameterIndex, const UINT num32VitValues, const void* data, const UINT destOffsetIn32BitValues)
{
	mCommandList->SetGraphicsRoot32BitConstants(rootParameterIndex, num32VitValues, data, destOffsetIn32BitValues);
}

/**
* Function : SetGraphicsRootDescriptorTable
*/
void D3D12CommandList::SetGraphicsRootDescriptorTable(const UINT rootParameterIndex, const RHIGPUDescriptorHandle baseDescriptor)
{
	D3D12_GPU_DESCRIPTOR_HANDLE d3d12BaseDescriptor; d3d12BaseDescriptor.ptr = baseDescriptor.ptr;

	mCommandList->SetGraphicsRootDescriptorTable(rootParameterIndex, d3d12BaseDescriptor);
}

/**
 * Function : SetGraphicsRootSignature
 */
void D3D12CommandList::SetGraphicsRootSignature(RHIRootSignature* rootSignature)
{
	D3D12RootSignature* d3d12RootSignature = ToD3D12Structure<D3D12RootSignature>(rootSignature);

	mCommandList->SetGraphicsRootSignature(d3d12RootSignature->RootSignature().Get());
}

/**
 * Function : SetComputeRootSignature
 */
void D3D12CommandList::SetComputeRootSignature(RHIRootSignature* rootSignature)
{
	D3D12RootSignature* d3d12RootSignature = ToD3D12Structure<D3D12RootSignature>(rootSignature);

	mCommandList->SetComputeRootSignature(d3d12RootSignature->RootSignature().Get());
}

/**
 * Function : SetComputeRootDescriptorTable
 */
void D3D12CommandList::SetComputeRootDescriptorTable(const UINT rootParameterIndex, const RHIGPUDescriptorHandle baseDescirptor)
{
	D3D12_GPU_DESCRIPTOR_HANDLE d3d12BaseDescriptor; d3d12BaseDescriptor.ptr = baseDescirptor.ptr;

	mCommandList->SetComputeRootDescriptorTable(rootParameterIndex, d3d12BaseDescriptor);
}

/**
 * Function : SetComputeRootConstantBufferView
 */
void D3D12CommandList::SetComputeRootConstantBufferView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation)
{
	mCommandList->SetComputeRootConstantBufferView(rootParameterIndex, bufferLocation);
}

/**
 * Function : SetComputeRootUnorderedAccessView
 */
void D3D12CommandList::SetComputeRootUnorderedAccessView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation)
{
	mCommandList->SetComputeRootUnorderedAccessView(rootParameterIndex, bufferLocation);
}

/**
 * Function : SetDescriptorHeaps
 */
void D3D12CommandList::SetDescriptorHeaps(const UINT numDescriptorHeaps, RHIDescriptorHeap** heaps)
{
	std::vector<ID3D12DescriptorHeap*> d3d12Heaps(numDescriptorHeaps);
	for (UINT i = 0; i < numDescriptorHeaps; ++i)
		d3d12Heaps[i] = ToD3D12Structure<D3D12DescriptorHeap>(heaps[i])->Heap().Get();

	mCommandList->SetDescriptorHeaps(numDescriptorHeaps, d3d12Heaps.data());
}

/**
 * Function : IASetIndexBuffer
 */
void D3D12CommandList::IASetIndexBuffer(const RHIIndexBufferView* view)
{
	const D3D12_INDEX_BUFFER_VIEW* d3d12View = ToD3D12Structure<D3D12_INDEX_BUFFER_VIEW>(view);

	mCommandList->IASetIndexBuffer(d3d12View);
}

/**
 * Function : IASetPrimitiveTopology
 */
void D3D12CommandList::IASetPrimitiveTopology(const RHIPrimitiveTopology topology)
{
	D3D12_PRIMITIVE_TOPOLOGY d3d12Topology = static_cast<D3D12_PRIMITIVE_TOPOLOGY>(topology);

	mCommandList->IASetPrimitiveTopology(d3d12Topology);
}

/**
 * Function :IASetVertexBuffers
 */
void D3D12CommandList::IASetVertexBuffers(const UINT startSlot, const UINT numViews, const RHIVertexBufferView* views)
{
	const D3D12_VERTEX_BUFFER_VIEW* d3d12Views = ToD3D12Structure<D3D12_VERTEX_BUFFER_VIEW>(views);

	mCommandList->IASetVertexBuffers(startSlot, numViews, d3d12Views);
}

/**
 * Function : ResourceBarrier
 */
void D3D12CommandList::ResourceBarrier(const UINT numBarriers, const RHIResourceBarrier* barriers)
{
	std::vector<D3D12_RESOURCE_BARRIER> d3d12ResourceBarriers(numBarriers);
	for (UINT i = 0; i < numBarriers; ++i)
	{
		memcpy(&d3d12ResourceBarriers[i], &barriers[i], sizeof(D3D12_RESOURCE_BARRIER));
		switch (barriers[i].Type)
		{
			case RHIResourceBarrierType::Transition:
			{
				d3d12ResourceBarriers[i].Transition.pResource = ToD3D12Structure<D3D12Resource>(barriers[i].Transition.Resource)->Resource().Get();
				break;
			}

			case RHIResourceBarrierType::Aliasing:
			{
				d3d12ResourceBarriers[i].Aliasing.pResourceAfter	= ToD3D12Structure<D3D12Resource>(barriers[i].Aliasing.ResourceAfter)->Resource().Get();
				d3d12ResourceBarriers[i].Aliasing.pResourceBefore	= ToD3D12Structure<D3D12Resource>(barriers[i].Aliasing.ResourceBefore)->Resource().Get();
				break;
			}

			case RHIResourceBarrierType::UAV:
			{
				d3d12ResourceBarriers[i].UAV.pResource = ToD3D12Structure<D3D12Resource>(barriers[i].UAV.Resource)->Resource().Get();
				break;
			}
		}
	}

	mCommandList->ResourceBarrier(numBarriers, d3d12ResourceBarriers.data());
}

/**
 * Function : CopyResource
 */
void D3D12CommandList::CopyResource(RHIResource* dstResource, RHIResource* srcResource)
{
	ID3D12Resource* d3d12Dst = ToD3D12Structure<D3D12Resource>(dstResource)->Resource().Get();
	ID3D12Resource* d3d12Src = ToD3D12Structure<D3D12Resource>(srcResource)->Resource().Get();

	mCommandList->CopyResource(d3d12Dst, d3d12Src);
}

/**
 * Function : CopyTextureRegion
 */
void D3D12CommandList::CopyTextureRegion(
	const RHITextureCopyLocation*	dst,
	const UINT						dstX,
	const UINT						dstY,
	const UINT						dstZ,
	const RHITextureCopyLocation*	src,
	const RHIBox*					srcBox)
{
	D3D12_TEXTURE_COPY_LOCATION d3d12Dst = *ToD3D12Structure<D3D12_TEXTURE_COPY_LOCATION>(dst);
	D3D12_TEXTURE_COPY_LOCATION d3d12Src = *ToD3D12Structure<D3D12_TEXTURE_COPY_LOCATION>(src);
	const D3D12_BOX* d3d12SrcBox = ToD3D12Structure<D3D12_BOX>(srcBox);

	d3d12Dst.pResource = ToD3D12Structure<D3D12Resource>(dst->Resource)->Resource().Get();
	d3d12Src.pResource = ToD3D12Structure<D3D12Resource>(src->Resource)->Resource().Get();

	mCommandList->CopyTextureRegion(&d3d12Dst, dstX, dstY, dstZ, &d3d12Src, d3d12SrcBox);
}

/**
 * Function : CopyBufferRegion
 */
void D3D12CommandList::CopyBufferRegion(
	RHIResource*	dstBuffer,
	UINT64			dstOffset,
	RHIResource*	srcBuffer,
	UINT64			srcOffset,
	UINT64			numBytes)
{
	ID3D12Resource* d3d12DstBuffer	= ToD3D12Structure<D3D12Resource>(dstBuffer)->Resource().Get();
	ID3D12Resource* d3d12SrcBuffer	= ToD3D12Structure<D3D12Resource>(srcBuffer)->Resource().Get();

	mCommandList->CopyBufferRegion(d3d12DstBuffer, dstOffset, d3d12SrcBuffer, srcOffset, numBytes);
}

/**
 * Function : OMSetBlendFactor
 */
void D3D12CommandList::OMSetBlendFactor(const FLOAT blendFactor[4])
{
	mCommandList->OMSetBlendFactor(blendFactor);
}

/**
 * Function : OMSetRenderTargets
 */
void D3D12CommandList::OMSetRenderTargets(
	const UINT						numRenderTargetDescriptors,
	const RHICPUDescriptorHandle*	renderTargetDescriptors,
	const BOOL						RTsSingleHandleToDescriptorRange,
	const RHICPUDescriptorHandle*	depthStencilDescriptor)
{
	std::vector<D3D12_CPU_DESCRIPTOR_HANDLE> d3d12RTDescriptors(numRenderTargetDescriptors);
	for (UINT i = 0; i < numRenderTargetDescriptors; ++i)
	{
		d3d12RTDescriptors[i].ptr = renderTargetDescriptors[i].ptr;
	}

	const D3D12_CPU_DESCRIPTOR_HANDLE* d3d12DSDescriptor = ToD3D12Structure<D3D12_CPU_DESCRIPTOR_HANDLE>(depthStencilDescriptor);

	mCommandList->OMSetRenderTargets(numRenderTargetDescriptors, d3d12RTDescriptors.data(), RTsSingleHandleToDescriptorRange, d3d12DSDescriptor);
}

/**
 * Function : DrawInstanced
 */
void D3D12CommandList::DrawInstanced(
	const uint32_t vertexCountPerIntsance,
	const uint32_t instanceCount,
	const uint32_t startVertexLocation,
	const uint32_t startInstanceLocation
)
{
	mCommandList->DrawInstanced(vertexCountPerIntsance, instanceCount, startVertexLocation, startInstanceLocation);
}

/**
 * Function : DrawIndexedInstanced
 */
void D3D12CommandList::DrawIndexedInstanced(
	const uint32_t	indexCountPerInstance,
	const uint32_t	instanceCount,
	const uint32_t	startIndexLocation,
	const int32_t	baseVertexLocation,
	const uint32_t	startInstanceLocation)
{
	mCommandList->DrawIndexedInstanced(indexCountPerInstance, instanceCount, startIndexLocation, baseVertexLocation, startInstanceLocation);
}

/**
 * Function :
 */
void D3D12CommandList::Dispatch(const UINT threadGroupCountX, const UINT threadGroupCountY, const UINT threadGroupCountZ)
{
	mCommandList->Dispatch(threadGroupCountX, threadGroupCountY, threadGroupCountZ);
}

/**
 * Function : ExecuteBundle
 */
void D3D12CommandList::ExecuteBundle(RHICommandList* commandList)
{
	D3D12CommandList* d3d12CmdList = ToD3D12Structure<D3D12CommandList>(commandList);

	mCommandList->ExecuteBundle(d3d12CmdList->List().Get());
}

/**
 * Function : SOSetTargets
 */
void D3D12CommandList::SOSetTargets(const UINT startSlot, const UINT numViews, const RHIStreamOutputBufferView* views)
{
	const D3D12_STREAM_OUTPUT_BUFFER_VIEW* d3d12Views = ToD3D12Structure<D3D12_STREAM_OUTPUT_BUFFER_VIEW>(views);
	mCommandList->SOSetTargets(startSlot, numViews, d3d12Views);
}

/**
 * Function : Close
 */
void D3D12CommandList::Close()
{
	ThrowIfFailed(mCommandList->Close(), __TEXT("Can't close a command list."));
}
