#pragma once
#include "StdafxRHI.h"
#include <Render/IRenderer.h>

struct D3D12Blob : public RHIBlob
{
	void Release() override final
	{
		if (mBlob) mBlob->Release();
		//delete mBlob;
	}

	void* GetBufferPointer() override final { return mBlob->GetBufferPointer(); }
	SIZE_T GetBufferSize() override final { return mBlob->GetBufferSize(); }

	bool IsNull() override final { return mBlob == nullptr; }

	ID3DBlob* mBlob;
};

class D3D12Renderer : public IRenderer
{
public:
	D3D12Renderer();
	~D3D12Renderer();

	bool Initialize(const RenderSurface* renderSurface) override final;
	void Release() override final;

public:
	String ShaderFolder() const override final { return String(__TEXT("Shaders:HLSL:")); }
	RHIBlob* LoadShader(const String& shader, const RHIShaderType type, const RHIShaderMacro* macroes = nullptr, const bool fromFile = true, const String* initialDirectoryForSources = nullptr) const override final;

	void DumpRenderTargetToFile(const String& path, const ImageFormat format) override final;

protected:
	void DumpFeatures();

	void CheckRHI() override final;

	DWORD waitForSingleObject(HANDLE handle, DWORD miliSeconds) override final
	{
		return ::WaitForSingleObject(handle, miliSeconds);
	}

	void CreateDebugController(const bool enableGPUValidation);
};
