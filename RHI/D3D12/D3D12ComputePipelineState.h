#pragma once
#include "StdafxRHI.h"
#include "D3D12PipelineState.h"

class D3D12ComputePipelineState : public D3D12PipelineState
{
public:
	D3D12ComputePipelineState(const RHIComputePipelineStateDesc* desc)
		: D3D12PipelineState() { }

	void Release() override final { }
};
