#pragma once
#include "StdafxRHI.h"
#include "RHI.h"

class D3D12Resource : public RHIResource
{
public:
	void Release() override final { }

	FORCEINLINE ComPtr<ID3D12Resource>& Resource() { return mResource; }

	FORCEINLINE void SetName(CConstString name) override final { mResource->SetName(name); }

public:

	FORCEINLINE RHIGPUVirtualAddress GetGPUVirtualAddress() const override final { return mResource->GetGPUVirtualAddress(); }

	FORCEINLINE RHIResourceDesc GetDesc() const override final
	{
		D3D12_RESOURCE_DESC desc = mResource->GetDesc();
		return RHIResourceDesc(reinterpret_cast<RHIResourceDesc&>(desc));
	}

	FORCEINLINE void Map(const UINT subresource, const RHIRange* readRange, void** data) override final
	{
		mResource->Map(subresource, reinterpret_cast<const D3D12_RANGE*>(readRange), data);
	}

	FORCEINLINE void Unmap(const UINT subresource, const RHIRange* writtenRange) override final
	{
		mResource->Unmap(subresource, reinterpret_cast<const D3D12_RANGE*>(writtenRange));
	}

	void InitializeSubData(const UINT subresource, const void* data, const size_t size) override final;
	void WriteToSubresource(UINT dstSubresource, const RHIBox* dstBox, const void* srcData, const UINT srcRowPitch, const UINT srcDepthPitch) override final;

protected:
	ComPtr<ID3D12Resource>	mResource;
};
