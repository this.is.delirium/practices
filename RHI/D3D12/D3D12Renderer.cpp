#include "StdafxRHI.h"
#include "D3D12Renderer.h"

#include "D3D12CommandQueue.h"
#include "D3D12Device.h"
#include "D3D12Resource.h"
#include <RenderSurface.h>
#include <ResourceManager.h>
#include <StringUtils.h>

#define D3D_COMPILE_STANDARD_FILE_INCLUDE ((ID3DInclude*)(UINT_PTR)1)

/**
 * Function : D3D12Renderer
 */
D3D12Renderer::D3D12Renderer()
	: IRenderer() { }

/**
 * Function : ~D3D12Renderer
 */
D3D12Renderer::~D3D12Renderer()
{

}

/**
 * Function : Release
 */
void D3D12Renderer::Release()
{
	IRenderer::Release();
}

/**
 * Function : Initialize
 */
bool D3D12Renderer::Initialize(const RenderSurface* renderSurface)
{
	CreateDebugController(renderSurface->IsEnabledGPUValidation());

	mDevice = new D3D12Device(this);

	bool isInitialized = true;
	isInitialized &= mDevice->Initialize(renderSurface->WindowHandle());

	assert(isInitialized);

	isInitialized &= IRenderer::Initialize(renderSurface);

	DumpFeatures();

	return isInitialized;
}

/**
 * Function : CreateDebugController
 */
void D3D12Renderer::CreateDebugController(const bool enableGPUValidation)
{
	ID3D12Debug1* debugController;
	if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
	{
		debugController->EnableDebugLayer();
		debugController->SetEnableGPUBasedValidation(enableGPUValidation);
	}
}

/**
 * Function : DumpFeatures
 */
void D3D12Renderer::DumpFeatures()
{
	RHIFeatureDataArchitecture archData = {};
	mDevice->CheckFeatureSupport(RHIFeature::Architecture, &archData, sizeof(RHIFeatureDataArchitecture));
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Hardware and driver support for cache-coherent UMA: %d"), archData.CacheCoherentUNA);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Hardware and driver support for UMA: %d"), archData.UMA);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Tile-base rendering support: %d"), archData.TileBasedRenderer);

	RHIFeatureDataOptions opts = {};
	mDevice->CheckFeatureSupport(RHIFeature::RHIOptions, &opts, sizeof(RHIFeatureDataOptions));
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Conservative Rasterization Tier: %d"), opts.ConservativeRasterizationTier);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Cross Adapter Row Major Texture Supported: %d"), opts.CrossAdapterRowMajorTextureSupported);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Cross Node Sharing Tier: %d"), opts.CrossNodeSharingTier);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Double Precision Float Shader Operations: %d"), opts.DoublePrecisionFloatShaderOps);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Min Precision Support: %d"), opts.MinPrecisionSupport);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Output Merger Logic Operations: %d"), opts.OutputMergerLogicOp);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("PS Specified Stencil Ref Supported: %d"), opts.PSSpecifiedStencilRefSupported);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Resource Binding Tier: %d"), opts.ResourceBindingTier);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Resource Heap Tier: %d"), opts.ResourceHeapTier);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("ROVs Supported: %d"), opts.ROVsSupported);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Standard Swizzle 64KB Supported: %d"), opts.StandardSwizzle64KBSupported);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Tiled Resources Tier: %d"), opts.TiledResourcesTier);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Typed UAV Load Additional Formats: %d"), opts.TypedUAVLoadAdditionalFormats);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("VP And RT Array Index From Any Shader Feeding Rasterizer Supported Without GS Emulation: %d"), opts.VPAndRTArrayIndexFromAnyShaderFeedingRasterizerSupportedWithoutGSEmulation);

	RHIFeatureDataGpuVirtualAddressSupport vaSupport = {};
	mDevice->CheckFeatureSupport(RHIFeature::GpuVirtualAddressSupport, &vaSupport, sizeof(RHIFeatureDataGpuVirtualAddressSupport));
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Max GPU Virtual Address Bits Per Process: %d"), vaSupport.MaxGPUVirtualAddressBitsPerProcess);
	SLogger::Instance().AddMessage(LogMessageType::Information, __TEXT("Max GPU Virtual Address Bits Per Resource: %d"), vaSupport.MaxGPUVirtualAddressBitsPerResource);

	SLogger::Instance().FlushToDisk();
}

/**
 * Function : LoadShader
 */
RHIBlob* D3D12Renderer::LoadShader(const String& shader, const RHIShaderType type, const RHIShaderMacro* macroes, const bool fromFile, const String* initialDirectoryForSources) const
{
	D3D12Blob* blob = new D3D12Blob();

	UINT compileFlags = 0;
#if defined(_DEBUG)
	compileFlags |= D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	static const char* entryPoints[] =
	{
		"VSMain",
		"HSMain",
		"GSMain",
		"DSMain",
		"PSMain",
		"CSMain"
	};

	static const char* targets[] =
	{
		"vs_5_1",
		"hs_5_1",
		"gs_5_1",
		"ds_5_1",
		"ps_5_1",
		"cs_5_1"
	};

	HRESULT hr = S_OK;
	String path;
	const int shaderType = ToInt(type);
	ID3DBlob* errorBlob = nullptr;

	const D3D_SHADER_MACRO* d3d12Macroes = ToD3D12Structure<D3D_SHADER_MACRO>(macroes);

	if (fromFile)
	{
		path = SResourceManager::Instance().Find(ShaderFolder() + shader);
		hr = D3DCompileFromFile(path.c_str(), d3d12Macroes, D3D_COMPILE_STANDARD_FILE_INCLUDE, entryPoints[shaderType], targets[shaderType], compileFlags, 0, &blob->mBlob, &errorBlob);
	}
	else
	{
#ifdef UNICODE
		std::string asciiShader			= StringUtils::Converter::WStringToString(shader);
		std::string initialDirectory	= StringUtils::Converter::WStringToString(*initialDirectoryForSources);
#else
		const std::string& asciiShader		= shader;
		const std::string& initialDirectory	= *initialDirectoryForSources;
#endif
		hr = D3DCompile(asciiShader.c_str(), asciiShader.length() * sizeof(char), initialDirectory.c_str(), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, entryPoints[shaderType], targets[shaderType], compileFlags, 0, &blob->mBlob, &errorBlob);
	}

	if (FAILED(hr))
	{
		const String& printShader = (fromFile) ? path : shader;
		if (errorBlob)
		{
			std::string message((char*)errorBlob->GetBufferPointer());
#ifdef _UNICODE
			String printMessage(StringUtils::Converter::StringToWString(message));
			SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("The shader '%s' has errors: %s"), printShader.c_str(), printMessage.c_str());
#else
			SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("The shader '%s' has errors: %s"), printShader.c_str(), message.c_str());
#endif
			errorBlob->Release();
		}
		else
		{
			Char buffer[256];
			FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), buffer, 256, NULL);
			SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Can't compile the shader '%s', because it has errors: \n%s"), printShader.c_str(), buffer);
		}

		return blob;
	}

	return blob;
}

/**
 * Function : CheckRHI
 */
#pragma warning(push)
#pragma warning(disable : 4129)
void D3D12Renderer::CheckRHI()
{
	STATIC_ASSERT(D3D12_COMMAND_QUEUE_DESC, RHICommandQueueDesc);

	STATIC_ASSERT(DXGI_SWAP_CHAIN_DESC, RHISwapChainDesc);
	STATIC_ASSERT(DXGI_SWAP_CHAIN_DESC1, RHISwapChainDesc1);

	STATIC_ASSERT(D3D12_ROOT_PARAMETER, RHIRootParameter);
	STATIC_ASSERT(D3D12_STATIC_SAMPLER_DESC, RHIStaticSamplerDesc);
	STATIC_ASSERT(D3D12_ROOT_SIGNATURE_DESC, RHIRootSignatureDesc);

	STATIC_ASSERT(D3D12_GRAPHICS_PIPELINE_STATE_DESC, RHIGraphicsPipelineStateDesc);
	STATIC_ASSERT(D3D12_COMPUTE_PIPELINE_STATE_DESC, RHIComputePipelineStateDesc);

	STATIC_ASSERT(D3D12_HEAP_PROPERTIES, RHIHeapProperties);
	STATIC_ASSERT(D3D12_RESOURCE_DESC, RHIResourceDesc);
	STATIC_ASSERT(D3D12_CLEAR_VALUE, RHIClearValue);
	STATIC_ASSERT(D3D12_DESCRIPTOR_RANGE, RHIDescriptorRange);
	STATIC_ASSERT(D3D12_ROOT_DESCRIPTOR_TABLE, RHIRootDescriptorTable);
	STATIC_ASSERT(D3D12_ROOT_PARAMETER, RHIRootParameter);
	STATIC_ASSERT(D3D12_DESCRIPTOR_HEAP_DESC, RHIDescriptorHeapDesc);
	STATIC_ASSERT(D3D12_RENDER_TARGET_BLEND_DESC, RHIRenderTargetBlendDesc);
	STATIC_ASSERT(D3D12_RENDER_TARGET_VIEW_DESC, RHIRenderTargetViewDesc);
	STATIC_ASSERT(D3D12_RANGE, RHIRange);
	STATIC_ASSERT(D3D12_CONSTANT_BUFFER_VIEW_DESC, RHIConstantBufferViewDesc);
	STATIC_ASSERT(D3D12_VIEWPORT, RHIViewport);
	STATIC_ASSERT(D3D12_RECT, RHIRect);
	STATIC_ASSERT(D3D12_RESOURCE_BARRIER, RHIResourceBarrier);
	STATIC_ASSERT(D3D12_DEPTH_STENCIL_VIEW_DESC, RHIDepthStencilViewDesc);
	STATIC_ASSERT(D3D12_TEXTURE_COPY_LOCATION, RHITextureCopyLocation);
	STATIC_ASSERT(D3D12_SHADER_RESOURCE_VIEW_DESC, RHIShaderResourceViewDesc);
	STATIC_ASSERT(D3D12_INDEX_BUFFER_VIEW, RHIIndexBufferView);
	STATIC_ASSERT(D3D12_VERTEX_BUFFER_VIEW, RHIVertexBufferView);
	STATIC_ASSERT(D3D12_STREAM_OUTPUT_DESC, RHIStreamOutputDesc);
	STATIC_ASSERT(D3D12_SO_DECLARATION_ENTRY, RHIStreamOutputDeclarationEntry);
	STATIC_ASSERT(D3D12_STREAM_OUTPUT_BUFFER_VIEW, RHIStreamOutputBufferView);
	STATIC_ASSERT(D3D12_SUBRESOURCE_DATA, RHISubresourceData);
	STATIC_ASSERT(D3D12_UNORDERED_ACCESS_VIEW_DESC, RHIUnorderedAccessViewDesc);

	STATIC_ASSERT(D3D12_DESCRIPTOR_RANGE1, RHIDescriptorRange1);
	STATIC_ASSERT(D3D12_ROOT_DESCRIPTOR_TABLE1, RHIRootDescriptorTable1);
	STATIC_ASSERT(D3D12_ROOT_DESCRIPTOR1, RHIRootDescriptor1);
	STATIC_ASSERT(D3D12_ROOT_PARAMETER1, RHIRootParameter1);

	STATIC_ASSERT(D3D12_VERSIONED_ROOT_SIGNATURE_DESC, RHIVersionedRootSignatureDesc);

	STATIC_ASSERT(D3D12_FEATURE_DATA_ROOT_SIGNATURE, RHIFeatureDataRootSignature);
	STATIC_ASSERT(D3D12_FEATURE_DATA_ARCHITECTURE, RHIFeatureDataArchitecture);
	STATIC_ASSERT(D3D12_FEATURE_DATA_D3D12_OPTIONS, RHIFeatureDataOptions);
	STATIC_ASSERT(D3D12_FEATURE_DATA_GPU_VIRTUAL_ADDRESS_SUPPORT, RHIFeatureDataGpuVirtualAddressSupport);

	STATIC_ASSERT(D3D_SHADER_MACRO, RHIShaderMacro);
}
#pragma warning(pop)

/**
 * Function : DumpRenderTargetToFile
 */
void D3D12Renderer::DumpRenderTargetToFile(const String& path, const ImageFormat format)
{
	ID3D12CommandQueue*		d3d12CommandQueue	= ToD3D12Structure<D3D12CommandQueue>(mCommandQueue)->Queue().Get();
	ID3D12Resource*			d3d12Resource		= ToD3D12Structure<D3D12Resource>(mRenderTargets[mPreviouseFrameIndex])->Resource().Get();

	DirectX::ScratchImage	scratchImage;
	DWORD					flags = 0;

	DirectX::CaptureTexture(d3d12CommandQueue, d3d12Resource, false, scratchImage, D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_PRESENT);

	HRESULT hr;
	if (format <= ImageFormat::WicIco)
		hr = DirectX::SaveToWICFile(scratchImage.GetImages(), scratchImage.GetImageCount(), flags, DirectX::GetWICCodec(static_cast<DirectX::WICCodecs>(format)), path.c_str());
	else
		assert(false);
}
