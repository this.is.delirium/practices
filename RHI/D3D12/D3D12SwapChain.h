#pragma once
#include "StdafxRHI.h"
#include <RHI.h>

class D3D12SwapChain : public RHISwapChain
{
public:
	FORCEINLINE virtual void Release() { }

	FORCEINLINE ComPtr<IDXGISwapChain3>& SwapChain() { return mSwapChain; }

public:

	FORCEINLINE void Present(const UINT syncInterval, const UINT flags)
	{
		mSwapChain->Present(syncInterval, flags);
	}

	bool GetDesc(RHISwapChainDesc* desc) const override final;

	FORCEINLINE UINT GetCurrentBackBufferIndex() override final
	{
		return mSwapChain->GetCurrentBackBufferIndex();
	}

	void GetBuffer(const UINT buffer, RHIResource*& surface) override final;

	FORCEINLINE void SetMaximumFrameLatency(const UINT latency) override final
	{
		ThrowIfFailed(mSwapChain->SetMaximumFrameLatency(latency), __TEXT("Can't set a maximum frame latency."));
	}

	FORCEINLINE HANDLE GetFrameLatencyWaitableObject() override final
	{
		return mSwapChain->GetFrameLatencyWaitableObject();
	}

protected:
	ComPtr<IDXGISwapChain3>	mSwapChain;
};
