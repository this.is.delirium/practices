#include "StdafxRHI.h"
#include "RHIUtils.h"
#include "MRTPass.h"
#include <Render/GraphicsCore.h>
#include <Logger.h>
#include <ResourceManager.h>

/**
 * Function : Convert::ResourceDimToDsvDim
 */
RHIDSVDimension RHIUtils::Convert::ResourceDimToDsvDim(const RHIResourceDimension dim)
{
	//SLogger::Instance().AddMessage(LogMessageType::Warning, __TEXT("Converting a resource dimension to DSV dimension doesn't support the next values: Texture1DArray, Texture2DArray, Texture2DMS, Texture2DMSArray"));

	switch (dim)
	{
		case RHIResourceDimension::Texture1D:
			return RHIDSVDimension::Texture1D;

		case RHIResourceDimension::Texture2D:
			return RHIDSVDimension::Texture2D;

		case RHIResourceDimension::Texture3D:
			assert(false);

		case RHIResourceDimension::Buffer:
			assert(false);

		default:
			return RHIDSVDimension::Unknown;
	}
}

/**
 * Function : Convert::ResourceDimToSrvDim
 */
RHISRVDimension RHIUtils::Convert::ResourceDimToSrvDim(const RHIResourceDimension dim, const UINT16 depthOrArraySize)
{
	//SLogger::Instance().AddMessage(LogMessageType::Warning, __TEXT("Converting a resource dimension to SRV dimension doesn't support the next values: Texture1DArray, Texture2DArray, Texture2DMS, Texture2DMSArray, TextureCubeArray"));

	switch (dim)
	{
		case RHIResourceDimension::Texture1D:
			return RHISRVDimension::Texture1D;

		case RHIResourceDimension::Texture2D:
		{
			if (depthOrArraySize == 6)
				return RHISRVDimension::TextureCube;
			else
				return RHISRVDimension::Texture2D;
		}

		case RHIResourceDimension::Texture3D:
			return RHISRVDimension::Texture3D;

		case RHIResourceDimension::Buffer:
			return RHISRVDimension::Buffer;

		default:
			return RHISRVDimension::Unknown;
	}
}

/**
 * Function : Convert::ResourceDimToUavDim
 */
RHIUAVDimension RHIUtils::Convert::ResourceDimToUavDim(const RHIResourceDimension dim)
{
	//SLogger::Instance().AddMessage(LogMessageType::Warning, __TEXT("Converting a resource dimension to UAV dimension doesn't support the next values: Texture1DArray, Texture2DArray"));

	switch (dim)
	{
		case RHIResourceDimension::Texture1D:
			return RHIUAVDimension::Texture1D;

		case RHIResourceDimension::Texture2D:
			return RHIUAVDimension::Texture2D;

		case RHIResourceDimension::Texture3D:
			return RHIUAVDimension::Texture3D;

		case RHIResourceDimension::Buffer:
			return RHIUAVDimension::Buffer;

		default:
			return RHIUAVDimension::Unknown;
	}
}

/**
 * Function : Convert::ResourceFormatToDsvFormat
 */
RHIFormat RHIUtils::Convert::ResourceFormatToDsvFormat(const RHIFormat format)
{
	switch (format)
	{
		case RHIFormat::R24G8Typeless:
			return RHIFormat::D24UnormS8Uint;

		default:
			assert(false);
			return RHIFormat::Unknown;
	}
}

/**
 * Function : Fill::ShadersArray::Push
 */
void RHIUtils::Fill::ShadersArray::Push(const String& name, const RHIShaderType type)
{
	ShaderInfo& info = Shaders[ToInt(type)];
	info.Name	= name;
	info.Type	= type;
	info.Blob	= GraphicsCore::gRenderer->LoadShader(name, type);
}

/**
 * Function : Fill::ResourceFormatToSrvFormat
 */
RHIFormat RHIUtils::Convert::ResourceFormatToSrvFormat(const RHIFormat format)
{
	switch (format)
	{
		case RHIFormat::R24G8Typeless:
			return RHIFormat::R24UnormX8Typeless;

		default:
			return format;
	}
}

/**
 * Function : Fill::DepthStencilViewDesc
 */
void RHIUtils::Fill::DepthStencilViewDesc(const RHIResourceDesc* resDesc, RHIDepthStencilViewDesc* dsvDesc)
{
	dsvDesc->ViewDimension	= RHIUtils::Convert::ResourceDimToDsvDim(resDesc->Dimension);
	dsvDesc->Format			= RHIUtils::Convert::ResourceFormatToDsvFormat(resDesc->Format);
}

/**
 * Function : Fill::ShaderResourceViewDesc
 */
void RHIUtils::Fill::ShaderResourceViewDesc(const RHIResourceDesc* resDesc, RHIShaderResourceViewDesc* srvDesc)
{
	srvDesc->Shader4ComponentMapping	= D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc->Format						= RHIUtils::Convert::ResourceFormatToSrvFormat(resDesc->Format);
	srvDesc->ViewDimension				= RHIUtils::Convert::ResourceDimToSrvDim(resDesc->Dimension, resDesc->DepthOrArraySize);

	switch (srvDesc->ViewDimension)
	{
		case RHISRVDimension::Texture2D:
		{
			srvDesc->Texture2D.MipLevels = 1;
			break;
		}

		case RHISRVDimension::TextureCube:
		{
			srvDesc->TextureCube.MipLevels = 1;
			break;
		}

		default: assert(false);
	}
}

/**
 * Function : Fill::UnorderedAccessViewDesc
 */
void RHIUtils::Fill::UnorderedAccessViewDesc(const RHIResourceDesc* resDesc, RHIUnorderedAccessViewDesc* uavDesc)
{
	uavDesc->Format			= resDesc->Format;
	uavDesc->ViewDimension	= RHIUtils::Convert::ResourceDimToUavDim(resDesc->Dimension);

	switch (uavDesc->ViewDimension)
	{
		case RHIUAVDimension::Texture2D:
		{
			uavDesc->Texture2D.MipSlice		= 0;
			uavDesc->Texture2D.PlaneSlice	= 0;
			break;
		}
	}
}

/**
 * Function : Fill::GraphicsPipelineStateDesc
 */
void RHIUtils::Fill::GraphicsPipelineStateDesc(MRTPass* mrtPass, RHIGraphicsPipelineStateDesc* psoDesc)
{
	psoDesc->RootSignature		= mrtPass->RootSignature();
	psoDesc->InputLayout		= mrtPass->InputLayoutDesc();

	psoDesc->NumRenderTargets	= mrtPass->NumRT();
	for (int i = 0; i < mrtPass->NumRT(); ++i)
	{
		psoDesc->RTVFormats[i] = mrtPass->Format(i);
	}
	psoDesc->DSVFormat = mrtPass->DepthStencilViewFormat();

	psoDesc->BlendState = mrtPass->BlendState();
}

/**
 * Function : Fill::GraphicsPipelinStateDesc
 */
void RHIUtils::Fill::GraphicsPipelineStateDesc(const ShadersArray& shaders, RHIGraphicsPipelineStateDesc* psoDesc)
{
	for (auto& shader : shaders.Shaders)
	{
		if (shader.Type == RHIShaderType::Unknown)
			continue;

		if (shader.Blob->IsNull())
		{
			SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Blob for '%s' shader is null"), shader.Name.c_str());
			return;
		}

		RHIShaderBytecode* psoShader = GetShaderBytecode(psoDesc, shader.Type);
		*psoShader = { shader.Blob->GetBufferPointer(), shader.Blob->GetBufferSize() };
	}
}

/**
 * Function : GetSize
 */
size_t RHIUtils::GetSize(const RHIFormat format)
{
	switch (format)
	{
		case RHIFormat::R32Uint:
			return sizeof(uint32_t);

		case RHIFormat::R16Uint:
			return sizeof(uint16_t);

		default:
			assert(false);
			return 0;
	}
}

/**
 * Function : GetSubresource
 */
RHISubresourceData RHIUtils::GetSubresource(const void* data, const size_t size)
{
	RHISubresourceData subres;
	subres.Data			= data;
	subres.RowPitch		= size;
	subres.SlicePitch	= subres.RowPitch;

	return subres;
}

/**
 * Function : GetShaderBytecode
 */
RHIShaderBytecode* RHIUtils::GetShaderBytecode(RHIGraphicsPipelineStateDesc* psoDesc, const RHIShaderType type)
{
	switch (type)
	{
		case RHIShaderType::Vertex:
			return &psoDesc->VS;

		case RHIShaderType::Pixel:
			return &psoDesc->PS;

		case RHIShaderType::Domain:
			return &psoDesc->DS;

		case RHIShaderType::Geometry:
			return &psoDesc->GS;

		case RHIShaderType::Hull:
			return &psoDesc->HS;

		default:
			return nullptr;
	}
}
