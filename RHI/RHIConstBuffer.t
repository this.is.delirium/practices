#include <Render/GraphicsCore.h>
#include <RHIUtils.h>

/**
 * Function : Release
 */
template<typename ConstBuffer>
void RHIConstBuffer<ConstBuffer>::Release()
{
	RHIBuffer::Release();
	mSrvHandle.Release();
}

/**
 * Function : SetHeapDescriptor
 */
template<class ConstBuffer>
void RHIConstBuffer<ConstBuffer>::SetHeapDescriptor()
{
	mSrvHandle.Initialize(GraphicsCore::gRenderer->CbvSrvUavHeapManager());
}

/**
 * Function : CreateDerivedViews
 */
template<class ConstBuffer>
void RHIConstBuffer<ConstBuffer>::CreateDerivedViews()
{
	SetHeapDescriptor();

	RHIConstantBufferViewDesc cbvDesc;
	cbvDesc.BufferLocation	= mBuffer->GetGPUVirtualAddress();
	cbvDesc.SizeInBytes		= AlignTo256Bytes(mNumElements * mStrideInBytes);

	GraphicsCore::gDevice->CreateConstantBufferView(&cbvDesc, mSrvHandle.Cpu());
}

/**
 * Function : UpdateDataOnGPU
 */
template<class ConstBuffer>
void RHIConstBuffer<ConstBuffer>::UpdateDataOnGPU()
{
	GraphicsCore::gRenderer->UpdateBuffer(this, &RHIUtils::GetSubresource(&mData, sizeof(mData)));
	mIsDirty = false;
}
