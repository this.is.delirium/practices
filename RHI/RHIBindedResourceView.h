#pragma once
#include "StdafxRHI.h"
#include "RHI.h"

namespace RHI
{

struct BindedResourceView
{
	BindedResourceView(const UINT pos, const RHIGPUDescriptorHandle& handle)
		: position(pos)
		, gpuHandle(handle) { }

	UINT					position;
	RHIGPUDescriptorHandle	gpuHandle;
};

using BindedResourceViews = std::vector<BindedResourceView>;

} // end of namespace RHI
