#pragma once
#include "StdafxRHI.h"
#include "RHI.h"

class RHIDescriptorHeapManager
{
public:
	static const UINT kNullDescriptor = (UINT)-1;

public:
	void Release();

	void Initialize(const RHIDescriptorHeapDesc* desc, CString name = nullptr);
	FORCEINLINE RHIDescriptorHeap* Heap() { return mHeap; }

	FORCEINLINE bool HasDescriptors(const UINT numDescriptors) const { return numDescriptors < mFreeDescriptors.size(); }

	// Returns vector of descriptors.
	std::vector<int> GetNewDescriptors(const UINT numDescriptors);

	//! Returns the start descriptor of continuous descriptors in the heap.
	int GetStartContinuousDescriptors(const UINT numDescriptors);

	int GetNewDescriptor();

	void FreeDescriptors(std::vector<int>& descriptors);
	void FreeDescriptors(int& startDescriptor, const UINT numDescriptors);
	void FreeDescriptor(int& descriptor);

	FORCEINLINE RHICPUDescriptorHandle CpuHandle(const int descriptor) const { return RHICPUDescriptorHandle(mHeap->GetCPUDescriptorHandleForHeapStart(), descriptor, mDescriptorSize); }
	FORCEINLINE RHIGPUDescriptorHandle GpuHandle(const int descriptor) const { return RHIGPUDescriptorHandle(mHeap->GetGPUDescriptorHandleForHeapStart(), descriptor, mDescriptorSize); }

protected:
	RHIDescriptorHeap*	mHeap = nullptr;
	UINT				mMaxDescriptors;
	UINT				mDescriptorSize;

	std::stack<int>		mFreeDescriptors;	// TODO: Think about it. It is possible that stack isn't the best container.

	String				mName;
};
