#include "StdafxRHI.h"
#include "RHIStructuredBuffer.h"
#include <Render/GraphicsCore.h>

using namespace GraphicsCore;

/**
 * Function : Release
 */
void RHIStructuredBuffer::Release()
{
	RHIBuffer::Release();
	mSrvHandle.Release();
	mUavHandle.Release();
	mCounter.Release();
}

/**
 * Function : CreateDerivedViews
 */
void RHIStructuredBuffer::CreateDerivedViews()
{
	RHIShaderResourceViewDesc srvDesc	= {};
	srvDesc.ViewDimension				= RHISRVDimension::Buffer;
	srvDesc.Format						= RHIFormat::Unknown;
	srvDesc.Shader4ComponentMapping		= D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Buffer.NumElements			= mNumElements;
	srvDesc.Buffer.StructureByteStride	= mStrideInBytes;
	srvDesc.Buffer.Flags				= RHIBufferSRVFlags::None;

	mSrvHandle.Initialize(gRenderer->CbvSrvUavHeapManager());
	gDevice->CreateShaderResourceView(mBuffer, &srvDesc, mSrvHandle.Cpu());

	RHIUnorderedAccessViewDesc uavDesc	= {};
	uavDesc.ViewDimension				= RHIUAVDimension::Buffer;
	uavDesc.Format						= RHIFormat::Unknown;
	uavDesc.Buffer.CounterOffsetInBytes	= 0;
	uavDesc.Buffer.NumElements			= mNumElements;
	uavDesc.Buffer.StructureByteStride	= mStrideInBytes;
	uavDesc.Buffer.Flags				= RHIBufferUAVFlags::None;

	mCounter.Initialize(__TEXT("StructuredBuffer:Counter"), 4, 1, 4);
	mUavHandle.Initialize(gRenderer->CbvSrvUavHeapManager());
	gDevice->CreateUnorderedAccessView(mBuffer, mCounter.Buffer(), &uavDesc, mUavHandle.Cpu());
}
