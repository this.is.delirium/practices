#pragma once
#include "StdafxRHI.h"
#include "RHIIndexBuffer.h"
#include "RHIVertexBuffer.h"

struct RHIBatchPartition
{
	RHIBatchPartition(const UINT startIndex, const UINT numberElements)
		: StartIndex(startIndex), NumberElements(numberElements) { }

	UINT StartIndex;
	UINT NumberElements;
};

struct RHIBatchDesc
{
	RHIBatchDesc& operator= (RHIBatchDesc& o)
	{
		VertexData				= o.VertexData;
		VertexDataSizeInBytes	= o.VertexDataSizeInBytes;
		VertexStride			= o.VertexStride;

		IndexData				= o.IndexData;
		IndexDataSizeInBytes	= o.IndexDataSizeInBytes;
		IndexFormat				= o.IndexFormat;

		PrimitiveTopology		= o.PrimitiveTopology;

		VertexPartition			= std::move(o.VertexPartition);
		IndexPartition			= std::move(o.IndexPartition);

		return *this;
	}

	void*							VertexData;
	UINT64							VertexDataSizeInBytes;
	UINT							VertexStride;

	void*							IndexData;
	UINT64							IndexDataSizeInBytes;
	RHIFormat						IndexFormat;

	RHIPrimitiveTopology			PrimitiveTopology;

	std::vector<RHIBatchPartition>	VertexPartition;
	std::vector<RHIBatchPartition>	IndexPartition;
};

class RHIBatch
{
public:
	void Release();
	void Initialize(const String& name, RHIBatchDesc& desc);

	FORCEINLINE RHIPrimitiveTopology Topology() const { return mDesc.PrimitiveTopology; }
	FORCEINLINE bool HasIndexBuffer() const { return mHasIndexBuffer; }

	FORCEINLINE size_t Count() const { return (mHasIndexBuffer ? mDesc.IndexPartition.size() : mDesc.VertexPartition.size()); }

	FORCEINLINE const RHIVertexBufferView* VertexBufferView() { return mVertexBuffer.View(); }
	FORCEINLINE const RHIIndexBufferView* IndexBufferView() { return mIndexBuffer.View(); }

	FORCEINLINE const std::vector<RHIBatchPartition>& VertexPartition() const { return mDesc.VertexPartition; }
	FORCEINLINE const std::vector<RHIBatchPartition>& IndexPartition() const { return mDesc.IndexPartition; }

protected:
	bool			mHasIndexBuffer	= false;
	RHIVertexBuffer	mVertexBuffer;
	RHIIndexBuffer	mIndexBuffer;

	RHIBatchDesc	mDesc;
};
