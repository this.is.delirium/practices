#include "StdafxRHI.h"
#include "RHIByteAddressBuffer.h"
#include <Render/GraphicsCore.h>

using namespace GraphicsCore;

/**
 * Function : Release
 */
void RHIByteAddressBuffer::Release()
{
	RHIBuffer::Release();
	mSrvHandle.Release();
	mUavHandle.Release();
}

/**
 * Function : CreateDerivedViews
 */
void RHIByteAddressBuffer::CreateDerivedViews()
{
	RHIShaderResourceViewDesc srvDesc	= {};
	srvDesc.ViewDimension				= RHISRVDimension::Buffer;
	srvDesc.Format						= RHIFormat::R32Typeless;
	srvDesc.Shader4ComponentMapping		= D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Buffer.NumElements			= mNumElements * mStrideInBytes / 4;
	srvDesc.Buffer.Flags				= RHIBufferSRVFlags::Raw;

	mSrvHandle.Initialize(gRenderer->CbvSrvUavHeapManager());
	gDevice->CreateShaderResourceView(mBuffer, &srvDesc, mSrvHandle.Cpu());

	RHIUnorderedAccessViewDesc uavDesc	= {};
	uavDesc.ViewDimension				= RHIUAVDimension::Buffer;
	uavDesc.Format						= RHIFormat::R32Typeless;
	uavDesc.Buffer.NumElements			= srvDesc.Buffer.NumElements;
	uavDesc.Buffer.Flags				= RHIBufferUAVFlags::Raw;

	mUavHandle.Initialize(gRenderer->CbvSrvUavHeapManager());
	gDevice->CreateUnorderedAccessView(mBuffer, nullptr, &uavDesc, mUavHandle.Cpu());
}
