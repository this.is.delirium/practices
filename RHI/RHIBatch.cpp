#include "StdafxRHI.h"
#include "RHIBatch.h"
#include "RHIUtils.h"

/**
 * Function : Release
 */
void RHIBatch::Release()
{
	mVertexBuffer.Release();
	mIndexBuffer.Release();
}

/**
 * Function : Initialize
 */
void RHIBatch::Initialize(const String& name, RHIBatchDesc& desc)
{
	const UINT countVertices = desc.VertexDataSizeInBytes / desc.VertexStride;
	mVertexBuffer.Initialize(name + __TEXT(":VB"), desc.VertexDataSizeInBytes, countVertices, desc.VertexStride, desc.VertexData);

	if (mHasIndexBuffer = (desc.IndexData != nullptr))
	{
		const size_t stride		= RHIUtils::GetSize(desc.IndexFormat);
		const UINT countIndices = desc.IndexDataSizeInBytes / stride;
		mIndexBuffer.Initialize(name + __TEXT(":IB"), desc.IndexDataSizeInBytes, countIndices, stride, desc.IndexData);
	}

	mDesc = desc;
}
