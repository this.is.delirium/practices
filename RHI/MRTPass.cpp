#include "StdafxRHI.h"
#include "MRTPass.h"
#include <Render/GraphicsCore.h>
using namespace GraphicsCore;

/**
 * Function : Release
 */
void MRTPass::Release()
{
	RHISafeRelease(mRootSignature);
	RHISafeRelease(mPSO);

	for (UINT i = 0; i < mRenderTargets.size(); ++i)
		mRenderTargets[i].Release();

	mDepthStencil.Release();

	mRtvCpuHandles.clear();
}

/**
 * Function : Initialize
 */
void MRTPass::Initialize(const MRTPassDesc& desc)
{
	mNumRT			= desc.NumRT;
	mShowTexture	= true;

	// Create render targets.
	mRenderTargets.resize(mNumRT);
	mRtvCpuHandles.resize(mNumRT);
	mFormats.resize(mNumRT);
	mClearValues.resize(mNumRT);

	std::vector<RHIResourceBarrier> barriers;
	for (UINT i = 0; i < mNumRT; ++i)
	{
		const RHIResourceDesc& resDesc = desc.Descs[i];
		mFormats[i]			= resDesc.Format;
		mClearValues[i]		= desc.ClearValues[i];

		mRenderTargets[i].Initialize(&resDesc, &desc.ClearValues[i]);
		mRtvCpuHandles[i] = mRenderTargets[i].RtvHandle().Cpu();

		if (!(resDesc.Flags & RHIResourceFlag::DenyShaderResource))
			barriers.push_back(RHIResourceBarrier::MakeTransition(mRenderTargets[i].Texture(), RHIResourceState::Common, RHIResourceState::PixelShaderResource));
	}

	// Create a depth stencil.
	mDepthStencilViewFormat = desc.DepthStencil.ViewFormat;
	if (mDepthStencilViewFormat != RHIFormat::Unknown)
	{
		mDepthStencilClearValue = desc.DepthStencil.ClearValue;

		mDepthStencil.Initialize(&desc.DepthStencil.Desc, &mDepthStencilClearValue);
		mDepthStencilIsShaderResources = !(desc.DepthStencil.Desc.Flags & RHIResourceFlag::DenyShaderResource);
		if (mDepthStencilIsShaderResources)
		{
			barriers.push_back(RHIResourceBarrier::MakeTransition(mDepthStencil.Texture(), RHIResourceState::Common, RHIResourceState::PixelShaderResource));
		}
	}

	if (!barriers.empty())
		gRenderer->ChangeResourceBarrier(barriers.size(), barriers.data());

	// Fill barriers to farther bindings.
	{
		int i = 0;
		for (; i < mRenderTargets.size(); ++i)
		{
			mBindToRenderTargetBarriers[i]		= RHIResourceBarrier::MakeTransition(mRenderTargets[i].Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget);
			mBindToShaderResourceBarriers[i]	= RHIResourceBarrier::MakeTransition(mRenderTargets[i].Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource);
		}

		if (mDepthStencilIsShaderResources)
		{
			// ToDo: What will happen if pso depth mask is write off?
			mBindToRenderTargetBarriers[i]		= RHIResourceBarrier::MakeTransition(mDepthStencil.Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::DepthWrite);
			mBindToShaderResourceBarriers[i]	= RHIResourceBarrier::MakeTransition(mDepthStencil.Texture(), RHIResourceState::DepthWrite, RHIResourceState::PixelShaderResource);
		}
		else
		{
			mBindToRenderTargetBarriers[i]		= RHIResourceBarrier::MakeTransition(mDepthStencil.Texture(), RHIResourceState::Present, RHIResourceState::DepthWrite);
			mBindToShaderResourceBarriers[i]	= RHIResourceBarrier::MakeTransition(mDepthStencil.Texture(), RHIResourceState::DepthWrite, RHIResourceState::Present);
		}
	}

	mBlendDesc				= desc.BlendDesc;
	mInputLayoutDesc		= desc.InputLayout;
}

/**
 * Function : BindRenderTargets
 */
void MRTPass::BindRenderTargets(RHICommandList* cmdList, const bool clear) const
{
	// ToDo: Combine these barriers with outer barriers.
	cmdList->ResourceBarrier(mNumRT + 1, mBindToRenderTargetBarriers.data());

	const RHICPUDescriptorHandle* dsvCpuHandle = mDepthStencil.Texture() ? &mDepthStencil.DsvHandle().Cpu() : nullptr;

	cmdList->OMSetRenderTargets(mNumRT, RtvCpuHandles(), false, dsvCpuHandle);

	if (clear)
	{
		for (int i = 0; i < mNumRT; ++i)
		{
			cmdList->ClearRenderTargetView(mRenderTargets[i].RtvHandle().Cpu(), mClearValues[i].Color, 0, nullptr);
		}

		if (dsvCpuHandle)
		{
			const auto& clear = mDepthStencilClearValue.DepthStencil;
			cmdList->ClearDepthStencilView(*dsvCpuHandle, RHIClearFlags::Depth | RHIClearFlags::Stencil, clear.Depth, clear.Stencil, 0, nullptr);
		}
	}
}

/**
 * Function : BindShaderResources
 */
void MRTPass::BindShaderResources(RHICommandList* cmdList) const
{
	// ToDo: Combine these barriers with outer barriers.
	cmdList->ResourceBarrier(mNumRT + 1, mBindToShaderResourceBarriers.data());
}
