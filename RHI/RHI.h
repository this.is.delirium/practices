#pragma once
#include "StdafxRHI.h"

using RHIGPUVirtualAddress	= UINT64;
using LongPointer			= long long;

namespace RHIConstants
{
	static const UINT kAppendAlignedElement				= 0xffffffff;

	static const UINT kDescriptorRangeOffsetAppend		= 0xffffffff;

	static const UINT kResourceBarrierAllSubresources	= 0xffffffff;

	static const UINT kSignalIgnore						= 0;
	static const UINT kSignalInfinite					= 0xffffffff;

	static const FLOAT kFloat32Max						= 3.402823466e+38f;

	//-- ToDo: Reconsider later.
	static const INT kDefaultDepthBias					= D3D12_DEFAULT_DEPTH_BIAS;
	static const FLOAT kDefaultDepthBiasClamp			= D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
	static const FLOAT kDefaultSlopeScaledDepthBias		= D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;

	static const UINT kTextureDataPitchAlignment		= D3D12_TEXTURE_DATA_PITCH_ALIGNMENT;
}

struct RHIDefault { };

inline UINT AlignTo256Bytes(const UINT size)
{
	return ((size + 255) & ~255);
}

template<class T>
void RHISafeRelease(T*& rhiObject)
{
	if (rhiObject)
	{
		rhiObject->Release();
		delete rhiObject;
		rhiObject = nullptr;
	}
}

struct RHIViewport
{
	FLOAT TopLeftX;
	FLOAT TopLeftY;
	FLOAT Width;
	FLOAT Height;
	FLOAT MinDepth;
	FLOAT MaxDepth;
};

struct RHIRect
{
	LONG left;
	LONG top;
	LONG right;
	LONG bottom;
};

struct RHIBox
{
	UINT left;
	UINT top;
	UINT front;
	UINT right;
	UINT bottom;
	UINT back;
};

struct RHIRational
{
	UINT	Numerator;
	UINT	Denominator;
};

struct RHIFeatureDataArchitecture
{
	UINT	NodeIndex;
	BOOL	TileBasedRenderer;
	BOOL	UMA;
	BOOL	CacheCoherentUNA;
};

enum class RHIShaderMinPrecisionSupport
{
	None	= 0,
	Bits10	= 0x1,
	Bits16	= 0x2
};

enum class RHITiledResourcesTier
{
	Supported	= 0,
	Tier1		= 1,
	Tier2		= 2,
	Tier3		= 3
};

enum class RHIResourceBindingTier
{
	Tier1	= 1,
	Tier2	= 2,
	Tier3	= 3
};

enum class RHIConservativeRasterizationTier
{
	Supported	= 0,
	Tier1		= 1,
	Tier2		= 2,
	Tier3		= 3
};

enum class RHICrossNodeSharingTier
{
	Supported		= 0,
	Tier1Emulated	= 1,
	Tier1			= 2,
	Tier2			= 3,
};

enum class RHIResourceHeapTier
{
	Tier1	= 1,
	Tier2	= 2,
};

struct RHIFeatureDataOptions
{
	BOOL								DoublePrecisionFloatShaderOps;
	BOOL								OutputMergerLogicOp;
	RHIShaderMinPrecisionSupport		MinPrecisionSupport;
	RHITiledResourcesTier				TiledResourcesTier;
	RHIResourceBindingTier				ResourceBindingTier;
	BOOL								PSSpecifiedStencilRefSupported;
	BOOL								TypedUAVLoadAdditionalFormats;
	BOOL								ROVsSupported;
	RHIConservativeRasterizationTier	ConservativeRasterizationTier;
	UINT								MaxGPUVirtualAddressBitsPerResource;
	BOOL								StandardSwizzle64KBSupported;
	RHICrossNodeSharingTier				CrossNodeSharingTier;
	BOOL								CrossAdapterRowMajorTextureSupported;
	BOOL								VPAndRTArrayIndexFromAnyShaderFeedingRasterizerSupportedWithoutGSEmulation;
	RHIResourceHeapTier					ResourceHeapTier;
};

struct RHIFeatureDataGpuVirtualAddressSupport
{
	UINT MaxGPUVirtualAddressBitsPerResource;
	UINT MaxGPUVirtualAddressBitsPerProcess;
};

enum class RHIFeature
{
	RHIOptions					= 0,
	Architecture				= 1,
	FeatureLevels				= 2,
	FormatSupport				= 3,
	MultisampleQualityLevels	= 4,
	FormatInfo					= 5,
	GpuVirtualAddressSupport	= 6,
	ShaderModel					= 7,
	RHIOptions1					= 8,
	RootSignature				= 12
};

enum class RHIModeScanlineOrder
{
	Unspecified = 0,
	Progressive,
	UpperFieldFirst,
	LowerFieldFirst
};

enum class RHIModeScaling
{
	Unspecified = 0,
	Centered,
	Stretched
};


enum class RHIFormat : uint32_t
{
	Unknown					= 0,
	R32G32B32A32Typeless	= 1,
	R32G32B32A32Float		= 2,
	R32G32B32A32Uint		= 3,
	R32G32B32A32Sint		= 4,
	R32G32B32Typeless		= 5,
	R32G32B32Float			= 6,
	R32G32B32Uint			= 7,
	R32G32B32Sint			= 8,
	R16G16B16A16Typeless	= 9,
	R16G16B16A16Float		= 10,
	R16G16B16A16Unorm		= 11,
	R16G16B16A16Uint		= 12,
	R16G16B16A16Snorm		= 13,
	R16G16B16A16Sint		= 14,
	R32G32Typeless			= 15,
	R32G32Float				= 16,
	R32G32Uint				= 17,
	R32G32Sint				= 18,
	R32G8X24Typeless		= 19,
	D32FloatS8X24Uint		= 20,
	R32FloatX8X24Typeless	= 21,
	X32TypelessG8X24uint	= 22,
	R10G10B10A2Typeless		= 23,
	R10G10B10A2Unorm		= 24,
	R10G10B10A2Uint			= 25,
	R11G11B10Float			= 26,
	R8G8B8A8Typeless		= 27,
	R8G8B8A8UNorm			= 28,
	R8G8B8A8UNormSRGB		= 29,
	R8G8B8A8UInt			= 30,
	R8G8B8A8SNorm			= 31,
	R8G8B8A8SInt			= 32,
	R16G16Typeless			= 33,
	R16G16Float				= 34,
	R16G16Unorm				= 35,
	R16G16Uint				= 36,
	R16G16Snorm				= 37,
	R16G16Sint				= 38,
	R32Typeless				= 39,
	D32Float				= 40,
	R32Float				= 41,
	R32Uint					= 42,
	R32Sint					= 43,
	R24G8Typeless			= 44,
	D24UnormS8Uint			= 45,
	R24UnormX8Typeless		= 46,
	X24TypelessG8Uint		= 47,
	R8G8Typeless			= 48,
	R8G8Unorm				= 49,
	R8G8Uint				= 50,
	R8G8Snorm				= 51,
	R8G8Sint				= 52,
	R16Typeless				= 53,
	R16Float				= 54,
	D16Unorm				= 55,
	R16Unorm				= 56,
	R16Uint					= 57,
	R16Snorm				= 58,
	R16Sint					= 59,
	R8Typeless				= 60,
	R8Unorm					= 61,
	R8Uint					= 62,
	R8Snorm					= 63,
	R8Sint					= 64,
	A8Unorm					= 65,
	R1Unorm					= 66,
	R9G9B9E5Sharedxp		= 67,
	R8G8B8G8Unorm			= 68,
	G8R8G8B8Unorm			= 69,
	BC1Typeless				= 70,
	BC1Unorm				= 71,
	BC1Unorm_SRGB			= 72,
	BC2Typeless				= 73,
	BC2Unorm				= 74,
	BC2UnormSRGB			= 75,
	BC3Typeless				= 76,
	BC3Unorm				= 77,
	BC3UnormSRGB			= 78,
	BC4Typeless				= 79,
	BC4Unorm				= 80,
	BC4Snorm				= 81,
	BC5Typeless				= 82,
	BC5Unorm				= 83,
	BC5Snorm				= 84,
	B5G6R5Unorm				= 85,
	B5G5R5A1Unorm			= 86,
	B8G8R8A8Unorm			= 87,
	B8G8R8X8Unorm			= 88,
	R10G10B10XRBiasA2Unorm	= 89,
	B8G8R8A8Typeless		= 90,
	B8G8R8A8UnormSRGB		= 91,
	B8G8R8X8Typeless		= 92,
	B8G8R8X8UnormSRGB		= 93,
	BC6HTypeless			= 94,
	BC6HUF16				= 95,
	BC6HSF16				= 96,
	BC7Typeless				= 97,
	BC7Unorm				= 98,
	BC7UnormSRGB			= 99,
	AYUV					= 100,
	Y410					= 101,
	Y416					= 102,
	NV12					= 103,
	P010					= 104,
	P016					= 105,
	Opaque420				= 106,
	YUY2					= 107,
	Y210					= 108,
	Y216					= 109,
	NV11					= 110,
	AI44					= 111,
	IA44					= 112,
	P8						= 113,
	A8P8					= 114,
	B4G4R4A4Unorm			= 115,
	P208					= 130,
	V208					= 131,
	V408					= 132,
	ForceUint				= 0xffffffff
};

struct RHIModeDesc
{
	UINT					Width;
	UINT					Height;
	RHIRational				RefreshRate;
	RHIFormat				Format;
	RHIModeScanlineOrder	ScanlineOrdering;
	RHIModeScaling			Scaling;
};

struct RHISampleDesc
{
	UINT	Count;
	UINT	Quality;
};

// TODO: remove
enum class RHIBufferType
{
	Vertex = 0x01L,
	Index = 0x02L,
	Constant = 0x04L,
	StreamOutput = 0x10L
};

// TODO: remove
enum class RHIBufferUsage
{
	StaticDraw = 0,
	DynamicDraw,
	StreamDraw,
	StaticRead,
	DynamicRead,
	StreamRead
};

enum class RHIUsage
{
	ShaderInput			= 0x00000010UL,
	RenderTargetOutput	= 0x00000020UL,
	BackBuffer			= 0x00000040UL,
	Shared				= 0x00000080UL,
	ReadOnly			= 0x00000100UL,
	DiscardOnPresent	= 0x00000200UL,
	UnorderedAccess		= 0x00000400UL
};

enum class RHISwapEffect
{
	Discard			= 0,
	Sequential		= 1,
	FlipSequential	= 3,
	FlipDiscard		= 4
};

enum class RHISwapChainFlag
{
	NonPreRotated					= 1,
	AllowModeSwitch					= 2,
	GdiCompatible					= 4,
	RestrictedContent				= 8,
	RestrictSharedResourceDriver	= 16,
	DisplayOnly						= 32,
	FrameLatencyWaitableObject		= 64,
	ForegroundLayer					= 128,
	FullscreenVideo					= 256,
	YuvVideo						= 512,
	HwProtected						= 1024,
	AllowTearing					= 2048
};

struct RHISwapChainDesc
{
	RHIModeDesc		BufferDesc;
	RHISampleDesc	SampleDesc;
	RHIUsage		BufferUsage;
	UINT			BufferCount;
	HWND			OutputWindow;
	BOOL			Windowed;
	RHISwapEffect	SwapEffect;
	UINT			Flags;
};

enum class RHIScaling
{
	Stretch = 0,
	None = 1,
	AspectRatioStretch = 2
};

enum class RHIAlphaMode : uint32_t
{
	Unspecified		= 0,
	Premultiplied	= 1,
	Straight		= 2,
	Ignore			= 3,
	ForceDword		= 0xffffffff
};

struct RHISwapChainDesc1
{
	UINT			Width;
	UINT			Height;
	RHIFormat		Format;
	BOOL			Stereo;
	RHISampleDesc	SampleDesc;
	RHIUsage		BufferUsage;
	UINT			BufferCount;
	RHIScaling		Scaling;
	RHISwapEffect	SwapEffect;
	RHIAlphaMode	AlphaMode;
	UINT			Flags;
};

struct RHISwapChainFullscreenDesc
{
	RHIRational				RefreshRate;
	RHIModeScanlineOrder	ScanlineOrdering;
	RHIModeScaling			Scaling;
	BOOL					Windowed;
};

enum class RHIComparisonFunc
{
	Never = 1,
	Less,
	Equal,
	LEqual,
	Greater,
	NotEqual,
	GEqual,
	Always
};

enum class RHIFilter
{
	MinMagMipPoint							= 0,
	MinMagPointMipLinear					= 0x1,
	MinPointMagLinearMipPoint				= 0x4,
	MinPointMagMipLinear					= 0x5,
	MinLinearMagMipPoint					= 0x10,
	MinLinearMagPointMipLinear				= 0x11,
	MinMagLinearMipPoint					= 0x14,
	MinMagMipLinear							= 0x15,
	Anisotropic								= 0x55,
	ComparisonMinMagMipPoint				= 0x80,
	ComparisonMinMagPointMipLinear			= 0x81,
	ComparisonMinPointMagLinearMipPoint		= 0x84,
	ComparisonMinPointMagMipLinear			= 0x85,
	ComparisonMinLinearMagMipPoint			= 0x90,
	ComparisonMinLinearMagPointMipLinear	= 0x91,
	ComparisonMinMagLinearMipPoint			= 0x94,
	ComparisonMinMagMipLinear				= 0x95,
	ComparisonAnisotropic					= 0xd5,
	MinimumMinMagMipPoint					= 0x100,
	MinimumMinMagPointMipLinear				= 0x101,
	MinimumMinPointMagLinearMipPoint		= 0x104,
	MinimumMinPointMagMipLinear				= 0x105,
	MinimumMinLinearMagMipPoint				= 0x110,
	MinimumMinLinearMagPointMipLinear		= 0x111,
	MinimumMinMagLinearMipPoint				= 0x114,
	MinimumMinMagMipLinear					= 0x115,
	MinimumAnisotropic						= 0x155,
	MaximumMinMagMipPoint					= 0x180,
	MaximumMinMagPointMipLinear				= 0x181,
	MaximumMinPointMagLinearMipPoint		= 0x184,
	MaximumMinPointMagMipLinear				= 0x185,
	MaximumMinLinearMagMipPoint				= 0x190,
	MaximumMinLinearMagPointMipLinear		= 0x191,
	MaximumMinMagLinearMipPoint				= 0x194,
	MaximumMinMagMipLinear					= 0x195,
	MaximumAnisotropic						= 0x1d5
};

enum class RHITextureAddressMode
{
	Wrap = 1,	// GL_REPEAT in OpenGL
	Mirror,		// GL_MIRRORED_REPEAT in OpenGL
	Clamp,		// GL_CLAMP_TO_EDGE
	Border,		// GL_CLAMP_TO_BORDER
	MirrorOnce	//  GL_MIRROR_CLAMP_TO_EDGE (?)
};

enum class RHIRootParameterType
{
	DescriptorTable	= 0,
	Constants32Bit	= (DescriptorTable + 1),
	CBV				= (Constants32Bit + 1),
	SRV				= (CBV + 1),
	UAV				= (SRV + 1)
};

enum class RHIDescriptorRangeType
{
	SRV		= 0,
	UAV		= (SRV + 1),
	CBV		= (UAV + 1),
	Sampler	= (CBV + 1)
};

enum class RHIDescriptorRangeFlags
{
	None						= 0,
	DescriptorsVolatile			= 0x1,
	DataVolatile				= 0x2,
	DataStaticWhileSetAtExecute	= 0x4,
	DataStatic					= 0x8
};

enum class RHIRootDescriptorFlags
{
	None						= 0,
	DataVolatile				= 0x2,
	DataStaticWhileSetAtExecute	= 0x4,
	DataStatic					= 0x8
};

struct RHIDescriptorRange
{
	inline void Init(
		RHIDescriptorRangeType	type,
		const UINT				numDescriptors,
		const UINT				baseShaderRegister,
		const UINT				registerSpace = 0,
		const UINT				offsetInDescriptorsFromTableStart = RHIConstants::kDescriptorRangeOffsetAppend)
	{
		Init(*this, type, numDescriptors, baseShaderRegister, registerSpace, offsetInDescriptorsFromTableStart);
	}

	static inline void Init(
		RHIDescriptorRange&		range,
		RHIDescriptorRangeType	type,
		const UINT				numDescriptors,
		const UINT				baseShaderRegister,
		const UINT				registerSpace = 0,
		const UINT				offsetInDescriptorsFromTableStart = RHIConstants::kDescriptorRangeOffsetAppend)
	{
		range.RangeType							= type;
		range.NumDescriptors					= numDescriptors;
		range.BaseShaderRegister				= baseShaderRegister;
		range.RegisterSpace						= registerSpace;
		range.OffsetInDescriptorsFromTableStart	= offsetInDescriptorsFromTableStart;
	}

	RHIDescriptorRangeType	RangeType;
	UINT					NumDescriptors;
	UINT					BaseShaderRegister;
	UINT					RegisterSpace;
	UINT					OffsetInDescriptorsFromTableStart;
};

struct RHIRootDescriptorTable
{
	static inline void Init(
		RHIRootDescriptorTable&		table,
		const UINT					numDescriptorRanges,
		const RHIDescriptorRange*	ranges)
	{
		table.NumDescriptorRanges	= numDescriptorRanges;
		table.DescriptorRanges		= ranges;
	}

	UINT						NumDescriptorRanges;
	const RHIDescriptorRange*	DescriptorRanges;

	/**
	 * NumDescriptorRange - the number of ranges this descriptor table will contain.
	 * We can put combinations of srv, cbv, and uav ranges together here.
	 * Samplers cannot be combined with the others.
	 */
};

struct RHIRootConstants
{
	UINT ShaderRegister;
	UINT RegisterSpace;
	UINT Num32BitValues;
};

struct RHIRootDescriptor
{
	static inline void Init(RHIRootDescriptor& descr, const UINT shaderRegister, const UINT registerSpace = 0)
	{
		descr.ShaderRegister	= shaderRegister;
		descr.RegisterSpace		= registerSpace;
	}

	UINT ShaderRegister;
	UINT RegisterSpace;
};

enum class RHIShaderVisibility
{
	All			= 0,
	Vertex		= 1,
	Hull		= 2,
	Domain		= 3,
	Geometry	= 4,
	Pixel		= 5
};

struct RHIRootParameter
{

	static inline void InitAsDescriptorTable(
		RHIRootParameter&			parameter,
		const UINT					numDescriptorRanges,
		const RHIDescriptorRange*	ranges,
		const RHIShaderVisibility	visibility = RHIShaderVisibility::All)
	{
		parameter.ParameterType		= RHIRootParameterType::DescriptorTable;
		parameter.ShaderVisibility	= visibility;
		RHIRootDescriptorTable::Init(parameter.DescriptorTable, numDescriptorRanges, ranges);
	}

	static inline void InitAsConstantBufferView(
		RHIRootParameter&	param,
		const UINT			shaderRegister,
		const UINT			registerSpace = 0,
		RHIShaderVisibility	visibility = RHIShaderVisibility::All)
	{
		param.ParameterType = RHIRootParameterType::CBV;
		param.ShaderVisibility = visibility;
		RHIRootDescriptor::Init(param.Descriptor, shaderRegister, registerSpace);
	}

	inline void InitAsConstantBufferView(
		const UINT			shaderRegister,
		const UINT			registerSpace = 0,
		RHIShaderVisibility	visibility = RHIShaderVisibility::All)
	{
		InitAsConstantBufferView(*this, shaderRegister, registerSpace, visibility);
	}
	inline void InitAsDescriptorTable(
		const UINT					numDescriptorRanges,
		const RHIDescriptorRange*	ranges,
		const RHIShaderVisibility	visibility = RHIShaderVisibility::All)
	{
		InitAsDescriptorTable(*this, numDescriptorRanges, ranges, visibility);
	}

	RHIRootParameterType ParameterType;
	union
	{
		RHIRootDescriptorTable	DescriptorTable;
		RHIRootConstants		Constants;
		RHIRootDescriptor		Descriptor;
	};
	RHIShaderVisibility ShaderVisibility;
};

enum class RHIRootSignatureFlags
{
	None							= 0,
	AllowInputAssemblerInputLayout	= 0x1,
	DenyVertexShaderRootAccess		= 0x2,
	DenyHullShaderRootAccess		= 0x4,
	DenyDomainShaderRootAccess		= 0x8,
	DenyGeometryShaderRootAccess	= 0x10,
	DenyPixelShaderRootAccess		= 0x20,
	AllowStreamOutput				= 0x40
};

RHIRootSignatureFlags GetRootSignatureFlags(const bool ia, const bool vertex, const bool hull, const bool domain, const bool geometry, const bool pixel, const bool so);

enum class RHIStaticBorderColor
{
	TransparentBlack	= 0,
	OpaqueBlack			= (TransparentBlack + 1),
	OpaqueWhite			= (OpaqueBlack + 1)
};

struct RHIStaticSamplerDesc
{
	RHIStaticSamplerDesc() {}

	explicit RHIStaticSamplerDesc(const RHIDefault&)
	{
		Filter				= RHIFilter::MinMagMipPoint;
		AddressU			= RHITextureAddressMode::Wrap;
		AddressV			= RHITextureAddressMode::Wrap;
		AddressW			= RHITextureAddressMode::Wrap;
		MipLODBias			= 0.0f;
		MaxAnisotropy		= 0;
		ComparisonFunc		= RHIComparisonFunc::Never;
		BorderColor			= RHIStaticBorderColor::TransparentBlack;
		MinLOD				= 0.0f;
		MaxLOD				= RHIConstants::kFloat32Max;
		ShaderRegister		= 0;
		RegisterSpace		= 0;
		ShaderVisibility	= RHIShaderVisibility::Pixel;
	}

	RHIFilter				Filter;
	RHITextureAddressMode	AddressU;
	RHITextureAddressMode	AddressV;
	RHITextureAddressMode	AddressW;
	FLOAT					MipLODBias;
	UINT					MaxAnisotropy;
	RHIComparisonFunc		ComparisonFunc;
	RHIStaticBorderColor	BorderColor;
	FLOAT					MinLOD;
	FLOAT					MaxLOD;
	UINT					ShaderRegister;
	UINT					RegisterSpace;
	RHIShaderVisibility		ShaderVisibility;
};

struct RHIRootSignatureDesc
{
	RHIRootSignatureDesc() { }

	explicit RHIRootSignatureDesc(const RHIRootSignatureDesc& desc)
	{
		Init(desc.NumParameters, desc.Parameters, desc.NumStaticSamplers, desc.StaticSamplers, desc.Flags);
	}

	RHIRootSignatureDesc(
		const UINT					numParameters,
		const RHIRootParameter*		parameters,
		const UINT					numStaticSamplers,
		const RHIStaticSamplerDesc*	staticSamplers,
		const RHIRootSignatureFlags	flags = RHIRootSignatureFlags::None)
	{
		Init(numParameters, parameters, numStaticSamplers, staticSamplers, flags);
	}

	RHIRootSignatureDesc(const RHIDefault&)
	{
		Init(0, NULL, 0, NULL, RHIRootSignatureFlags::None);
	}

	inline void Init(
		const UINT					numParameters,
		const RHIRootParameter*		parameters,
		const UINT					numStaticSamplers,
		const RHIStaticSamplerDesc*	staticSamplers,
		const RHIRootSignatureFlags	flags = RHIRootSignatureFlags::None)
	{
		Init(*this, numParameters, parameters, numStaticSamplers, staticSamplers, flags);
	}

	static inline void Init(
		RHIRootSignatureDesc& desc,
		const UINT					numParameters,
		const RHIRootParameter*		parameters,
		const UINT					numStaticSamplers,
		const RHIStaticSamplerDesc*	staticSamplers,
		const RHIRootSignatureFlags	flags = RHIRootSignatureFlags::None)
	{
		desc.NumParameters		= numParameters;
		desc.Parameters			= parameters;
		desc.NumStaticSamplers	= numStaticSamplers;
		desc.StaticSamplers		= staticSamplers;
		desc.Flags				= flags;
	}

	UINT						NumParameters;
	const RHIRootParameter*		Parameters;
	UINT						NumStaticSamplers;
	const RHIStaticSamplerDesc*	StaticSamplers;
	RHIRootSignatureFlags		Flags;
};

struct RHIDescriptorRange1
{
	RHIDescriptorRange1()
	{}
	RHIDescriptorRange1(const RHIDescriptorRange1& o)
	{
		memcpy(this, &o, sizeof(RHIDescriptorRange1));
	}

	static inline void Init(
		RHIDescriptorRange1&	range,
		RHIDescriptorRangeType	rangeType,
		const UINT				numDescriptors,
		const UINT				baseShaderRegister,
		const UINT				registerSpace = 0,
		RHIDescriptorRangeFlags	flags = RHIDescriptorRangeFlags::None,
		const UINT				offsetInDescriptorsFromTableStart = RHIConstants::kDescriptorRangeOffsetAppend)
	{
		range.RangeType							= rangeType;
		range.NumDescriptors					= numDescriptors;
		range.BaseShaderRegister				= baseShaderRegister;
		range.RegisterSpace						= registerSpace;
		range.Flags								= flags;
		range.OffsetInDescriptorsFromTableStart	= offsetInDescriptorsFromTableStart;
	}

	inline void Init(
		RHIDescriptorRangeType	rangeType,
		const UINT				numDescriptors,
		const UINT				baseShaderRegister,
		const UINT				registerSpace = 0,
		RHIDescriptorRangeFlags	flags = RHIDescriptorRangeFlags::None,
		const UINT				offsetInDescriptorsFromTableStart = RHIConstants::kDescriptorRangeOffsetAppend)
	{
		Init(*this, rangeType, numDescriptors, baseShaderRegister, registerSpace, flags, offsetInDescriptorsFromTableStart);
	}

	RHIDescriptorRangeType	RangeType;
	UINT					NumDescriptors;
	UINT					BaseShaderRegister;
	UINT					RegisterSpace;
	RHIDescriptorRangeFlags	Flags;
	UINT					OffsetInDescriptorsFromTableStart;
};

struct RHIRootDescriptorTable1
{
	static inline void Init(
		RHIRootDescriptorTable1&	rootDescriptorTable,
		const UINT					numDescriptorRanges,
		const RHIDescriptorRange1*	descriptorRanges)
	{
		rootDescriptorTable.NumDescriptorRanges	= numDescriptorRanges;
		rootDescriptorTable.DescriptorRanges	= descriptorRanges;
	}

	inline void Init(const UINT numDescriptorRanges, const RHIDescriptorRange1* descriptorRanges)
	{
		Init(*this, numDescriptorRanges, descriptorRanges);
	}

	UINT						NumDescriptorRanges;
	const RHIDescriptorRange1*	DescriptorRanges;
};

struct RHIRootDescriptor1
{
	UINT					ShaderRegister;
	UINT					RegisterSpace;
	RHIRootDescriptorFlags	Flags;
};

struct RHIRootParameter1
{
	static inline void InitAsDescriptorTable(
		RHIRootParameter1&			rootParam,
		const UINT					numDescriptorRanges,
		const RHIDescriptorRange1*	descriptorRanges,
		const RHIShaderVisibility	visibility = RHIShaderVisibility::All)
	{
		rootParam.ParameterType		= RHIRootParameterType::DescriptorTable;
		rootParam.ShaderVisibility	= visibility;
		RHIRootDescriptorTable1::Init(rootParam.DescriptorTable, numDescriptorRanges, descriptorRanges);
	}

	inline void InitAsDescriptorTable(
		const UINT					numDescriptorRanges,
		const RHIDescriptorRange1*	descriptorRanges,
		const RHIShaderVisibility	visibility = RHIShaderVisibility::All)
	{
		InitAsDescriptorTable(*this, numDescriptorRanges, descriptorRanges, visibility);
	}

	RHIRootParameterType ParameterType;
	union
	{
		RHIRootDescriptorTable1	DescriptorTable;
		RHIRootConstants		Constants;
		RHIRootDescriptor1		Descriptor;
	};
	RHIShaderVisibility ShaderVisibility;
};

struct RHIRootSignatureDesc1
{
	UINT						NumParameters;
	const RHIRootParameter1*	Parameters;
	UINT						NumStaticSamplers;
	const RHIStaticSamplerDesc*	StaticSamplers;
	RHIRootSignatureFlags		Flags;
};

enum class RHIRootSignatureVersion
{
	Ver1	= 0x1,
	Ver1_0	= 0x1,
	Ver1_1	= 0x2
};

struct RHIVersionedRootSignatureDesc
{
	RHIVersionedRootSignatureDesc()
	{}

	static inline void Init_1_1(
		RHIVersionedRootSignatureDesc&	desc,
		const UINT						numParameters,
		const RHIRootParameter1*		parameters,
		const UINT						numStaticSamplers = 0,
		const RHIStaticSamplerDesc*		staticSamplers = nullptr,
		const RHIRootSignatureFlags		flags = RHIRootSignatureFlags::None)
	{
		desc.Version					= RHIRootSignatureVersion::Ver1_1;
		desc.Desc_1_1.NumParameters		= numParameters;
		desc.Desc_1_1.Parameters		= parameters;
		desc.Desc_1_1.NumStaticSamplers	= numStaticSamplers;
		desc.Desc_1_1.StaticSamplers	= staticSamplers;
		desc.Desc_1_1.Flags				= flags;
	}

	inline void Init_1_1(
		const UINT					numParameters,
		const RHIRootParameter1*	parameters,
		const UINT					numStaticSamplers = 0,
		const RHIStaticSamplerDesc*	staticSamplers = nullptr,
		const RHIRootSignatureFlags	flags = RHIRootSignatureFlags::None)
	{
		Init_1_1(*this, numParameters, parameters, numStaticSamplers, staticSamplers, flags);
	}

	RHIRootSignatureVersion Version;
	union
	{
		RHIRootSignatureDesc	Desc_1_0;
		RHIRootSignatureDesc1	Desc_1_1;
	};
};

struct RHIFeatureDataRootSignature
{
	RHIRootSignatureVersion	HighestVersion;
};

struct RHIRootSignature
{
	virtual void Release() = 0;
};

enum class RHIDescriptorHeapType
{
	CbvSrvUav	= 0,
	Sampler		= (CbvSrvUav + 1),
	RTV			= (Sampler + 1),
	DSV			= (RTV + 1),
	NumTypes	= (DSV + 1)
};

enum class RHIDescriptorHeapFlag
{
	None			= 0,
	ShaderVisible	= 0x1
};

struct RHIDescriptorHeapDesc
{
	RHIDescriptorHeapType	Type;
	UINT					NumDescriptors;
	RHIDescriptorHeapFlag	Flags;
	UINT					NodeMask;
};

enum class RHIShaderType
{
	Vertex = 0,
	Hull,
	Geometry,
	Domain,
	Pixel,
	Compute,
	Count,
	Unknown
};

// Specifies how the pipeline interprets geometry or hull shader input primitives.
enum class RHIPrimitiveTopologyType
{
	Undefined	= 0,
	Point		= 1,
	Line		= 2,
	Triangle	= 3,
	Patch		= 4
};

/**
* Values that indicate how the pipeline interprets vertex data that is bound to the input - assembler stage.
* These primitive topology values determine how the vertex data is rendered on screen.
*/
enum class RHIPrimitiveTopology
{
	Undefined					= 0,
	PointList					= 1,
	LineList					= 2,
	LineStrip					= 3,
	TriangleList				= 4,
	TriangleStrip				= 5,
	Control_Point_Patchlist_1	= 33,
	Control_Point_Patchlist_3	= 35,
	Control_Point_Patchlist_4	= 36,
	Control_Point_Patchlist_16  = 48
};

enum class RHIInputClassification
{
	PerVertexData = 0,
	PerInstanceData
};

struct RHIShaderBytecode
{
	RHIShaderBytecode()
		: ShaderBytecode(nullptr), Length(0) { }

	RHIShaderBytecode(const void* shader, const SIZE_T length)
		: ShaderBytecode(shader), Length(length) { }

	const void*	ShaderBytecode; // for opengl this field is simple const char*.
	SIZE_T		Length;
};

struct RHIShaderMacro
{
	LPCSTR Name;
	LPCSTR Definition;
};

struct RHIInputElementDesc
{
	LPCSTR					SemanticName;
	UINT					SemanticIndex;
	RHIFormat				Format;
	UINT					InputSlot;
	UINT					AlignedByteOffset;
	RHIInputClassification	InputSlotClass;
	UINT					InstanceDataStepRate;
};

struct RHIInputLayoutDesc
{
	const RHIInputElementDesc*	InputElementDescs;
	UINT						NumElements;
};

struct RHIStreamOutputDeclarationEntry
{
	UINT	Stream;
	LPCSTR	SemanticName;
	UINT	SemanticIndex;
	BYTE	StartComponent;
	BYTE	ComponentCount;
	BYTE	OutputSlot;
};

struct RHIStreamOutputDesc
{
	const RHIStreamOutputDeclarationEntry*	SODeclaration;
	UINT									NumElements;
	const UINT*								BufferStrides;
	UINT									NumStrides;
	UINT									RasterizedStream;
};

enum class RHIDepthWriteMask
{
	Off = 0,
	On
};

enum class RHIDepthFunc
{
	Never		= 1,
	Less		= 2,
	Equal		= 3,
	LEqual		= 4,
	Greater		= 5,
	NotEqual	= 6,
	GEqual		= 7,
	Always		= 8
};

enum class RHIStencilOp
{
	Keep	= 1,
	Zero	= 2,
	Replace	= 3,
	IncrSat	= 4,
	DecrSat	= 5,
	Invert	= 6,
	Incr	= 7,
	Decr	= 8
};

struct RHIDepthStencilOpDesc
{
	RHIStencilOp		StencilFailOp;
	RHIStencilOp		StencilDepthFailOp;
	RHIStencilOp		StencilPassOp;
	RHIComparisonFunc	StencilFunc;
};

struct RHIDepthStencilDesc
{
	BOOL					DepthEnable;
	RHIDepthWriteMask		DepthWriteMask;
	RHIDepthFunc			DepthFunc;
	BOOL					StencilEnable;
	UINT8					StencilReadMask;
	UINT8					StencilWriteMask;
	RHIDepthStencilOpDesc	FrontFace;
	RHIDepthStencilOpDesc	BackFace;
};

enum class RHICullMode
{
	None			= 1,
	Front			= 2,
	Back			= 3,
	FrontAndBack	= 5
};

enum class RHIFillMode
{
	Wireframe	= 2,
	Solid		= 3
};

enum class RHIConservativeRasterizationMode
{
	Off	= 0,
	On	= 1
};

struct RHIRasterizerDesc
{
	RHIRasterizerDesc()
	{}
	explicit RHIRasterizerDesc(const RHIRasterizerDesc& desc)
	{
		memcpy(this, &desc, sizeof(RHIRasterizerDesc));
	}
	explicit RHIRasterizerDesc(const RHIDefault&)
	{
		FillMode				= RHIFillMode::Solid;
		CullMode				= RHICullMode::Back;
		FrontCounterClockwise	= FALSE;
		DepthBias				= 0;
		DepthBiasClamp			= 0.0f;
		SlopeScaledDepthBias	= 0.0f;
		DepthClipEnable			= TRUE;
		MultisampleEnable		= FALSE;
		AntialiasedLineEnable	= FALSE;
		ForcedSampleCount		= 0;
		ConservativeRaster		= RHIConservativeRasterizationMode::Off;
	}

	RHIFillMode							FillMode;
	RHICullMode							CullMode;
	BOOL								FrontCounterClockwise;
	INT									DepthBias;
	FLOAT								DepthBiasClamp;
	FLOAT								SlopeScaledDepthBias;
	BOOL								DepthClipEnable;
	BOOL								MultisampleEnable;
	BOOL								AntialiasedLineEnable;
	UINT								ForcedSampleCount;
	RHIConservativeRasterizationMode	ConservativeRaster;
};

enum class RHILogicOp
{
	Clear = 0,		// 0
	Set,			// 1
	Copy,			// s
	CopyInverted,	// ~s
	Noop,			// d
	Invert,			// ~d
	And,			// s & d
	Nand,			// ~(s & d)
	Or,				// s | d
	Nor,			// ~(s | d)
	Xor,			// s ^ d
	Equiv,			// ~(s ^ d)
	AndReverse,		// s & ~d
	AndInverted,	// ~s & d
	OrReverse,		// s | ~d
	OrInverted		// ~s | d
};

enum class RHIBlend
{
	Zero				= 1,
	One					= 2,
	SrcColor			= 3,
	InvSrcColor			= 4,
	SrcAlpha			= 5,
	InvSrcAlpha			= 6,
	DestAlpha			= 7,
	InvDestAlpha		= 8,
	DestColor			= 9,
	InvDestColor		= 10,
	SrcAlphaSat			= 11,
	BlendFactor			= 14,
	InvBlendFactor		= 15,
	Src1Color			= 16,
	InvSrc1Color		= 17,
	Src1Alpha			= 18,
	InvSrc1Alpha		= 19,

	// Only OpenGL ?
	BlendAlpha,
	InvBlendAlpha
};

enum class RHIBlendOp
{
	Add = 1,
	Subtract,
	RevSubtract,
	Min,
	Max
};

enum class RHIColorWriteEnable
{
	Red		= 1,
	Green	= 2,
	Blue	= 4,
	Alpha	= 8,
	All		= (((Red | Green) | Blue) | Alpha)
};

struct RHIRenderTargetBlendDesc
{
	BOOL		BlendEnable;
	BOOL		LogicOpEnable;
	RHIBlend	SrcBlend;
	RHIBlend	DestBlend;
	RHIBlendOp	BlendOp;
	RHIBlend	SrcBlendAlpha;
	RHIBlend	DestBlendAlpha;
	RHIBlendOp	BlendOpAlpha;
	RHILogicOp	LogicOp;
	UINT8		RenderTargetWriteMask;
};

struct RHIBlendDesc
{
	RHIBlendDesc()
	{}
	RHIBlendDesc(const RHIBlendDesc& desc)
	{
		memcpy(this, &desc, sizeof(RHIBlendDesc));
	}
	explicit RHIBlendDesc(const RHIDefault&)
	{
		AlphaToCoverageEnable	= FALSE;
		IndependentBlendEnable	= FALSE;
		const RHIRenderTargetBlendDesc defaultDesc =
		{
			FALSE, FALSE,
			RHIBlend::One, RHIBlend::Zero, RHIBlendOp::Add,
			RHIBlend::One, RHIBlend::Zero, RHIBlendOp::Add,
			RHILogicOp::Noop,
			(UINT8)RHIColorWriteEnable::All
		};
		for (UINT i = 0; i < 8; ++i)
			RenderTarget[i] = defaultDesc;
	}

	void SetBlendFunc(const UINT rt, const RHIBlend srcFactor, const RHIBlend destFactor)
	{
		RenderTarget[rt].SrcBlend		= srcFactor;
		RenderTarget[rt].SrcBlendAlpha	= srcFactor;

		RenderTarget[rt].DestBlend		= destFactor;
		RenderTarget[rt].DestBlendAlpha	= destFactor;
	}

	void SetBlendFuncSeparate(const UINT rt, const RHIBlend srcFactor, const RHIBlend destFactor, const RHIBlend srcAlphaFactor, const RHIBlend destAlphaFactor)
	{
		RenderTarget[rt].SrcBlend		= srcFactor;
		RenderTarget[rt].SrcBlendAlpha	= srcAlphaFactor;

		RenderTarget[rt].DestBlend		= destFactor;
		RenderTarget[rt].DestBlendAlpha	= destAlphaFactor;
	}

	BOOL						AlphaToCoverageEnable;
	BOOL						IndependentBlendEnable;
	RHIRenderTargetBlendDesc	RenderTarget[8];
};

enum class RHIIndexBufferStripCutValue
{
	Disabled = 0,
	Value0xFFFF,
	Value0xFFFFFFFF
};

struct RHICachedPipelineState
{
	const void*	CachedBlob;
	SIZE_T		CachedBlobSizeInBytes;
};

enum class RHIPipelineStateFlags
{
	None		= 0,
	ToolDebug	= 0x1
};

struct RHIGraphicsPipelineStateDesc
{
	RHIRootSignature*			RootSignature;
	RHIShaderBytecode			VS;
	RHIShaderBytecode			PS;
	RHIShaderBytecode			DS;
	RHIShaderBytecode			HS;
	RHIShaderBytecode			GS;
	RHIStreamOutputDesc			StreamOutput;
	RHIBlendDesc				BlendState;
	UINT						SampleMask;
	RHIRasterizerDesc			RasterizerState;
	RHIDepthStencilDesc			DepthStencilState;
	RHIInputLayoutDesc			InputLayout;
	RHIIndexBufferStripCutValue	IBStripCutValue;
	RHIPrimitiveTopologyType	PrimitiveTopologyType;
	UINT						NumRenderTargets;
	RHIFormat					RTVFormats[8];
	RHIFormat					DSVFormat;
	RHISampleDesc				SampleDesc;
	UINT						NodeMask;
	RHICachedPipelineState		CachedPSO;
	RHIPipelineStateFlags		Flags;
};

struct RHIComputePipelineStateDesc
{
	RHIRootSignature*		RootSignature;
	RHIShaderBytecode		CS;
	UINT					NodeMask;
	RHICachedPipelineState	CachedPSO;
	RHIPipelineStateFlags	Flags;
};

//! A helper structure for creating and working with graphics and compute pipeline states through a combined interface.
struct RHIPipelineStateStream
{
	RHIPipelineStateStream() {}
	RHIPipelineStateStream(const RHIGraphicsPipelineStateDesc& desc)
	{
		RootSignature			= desc.RootSignature;
		VS						= desc.VS;
		PS						= desc.PS;
		DS						= desc.DS;
		HS						= desc.HS;
		GS						= desc.GS;
		StreamOutput			= desc.StreamOutput;
		BlendState				= desc.BlendState;
		SampleMask				= desc.SampleMask;
		RasterizerState			= desc.RasterizerState;
		DepthStencilState		= desc.DepthStencilState;
		InputLayout				= desc.InputLayout;
		IBStripCutValue			= desc.IBStripCutValue;
		PrimitiveTopologyType	= desc.PrimitiveTopologyType;
		NumRenderTargets		= desc.NumRenderTargets;
		//RTVFormats[8];
		memcpy(RTVFormats, desc.RTVFormats, NumRenderTargets * sizeof(RTVFormats[0]));
		DSVFormat				= desc.DSVFormat;
		SampleDesc				= desc.SampleDesc;
		NodeMask				= desc.NodeMask;
		CachedPSO				= desc.CachedPSO;
		Flags					= desc.Flags;
	}
	RHIPipelineStateStream(const RHIComputePipelineStateDesc& desc)
	{
		RootSignature	= desc.RootSignature;
		CS				= desc.CS;
		NodeMask		= desc.NodeMask;
		CachedPSO		= desc.CachedPSO;
		Flags			= desc.Flags;
	}

	bool IsGraphicsPipeline() const { return CS.ShaderBytecode == nullptr; }

	RHIGraphicsPipelineStateDesc GraphicsDesc()
	{
		RHIGraphicsPipelineStateDesc desc = {};

		desc.RootSignature			= RootSignature;
		desc.VS						= VS;
		desc.PS						= PS;
		desc.DS						= DS;
		desc.HS						= HS;
		desc.GS						= GS;
		desc.StreamOutput			= StreamOutput;
		desc.BlendState				= BlendState;
		desc.SampleMask				= SampleMask;
		desc.RasterizerState		= RasterizerState;
		desc.DepthStencilState		= DepthStencilState;
		desc.InputLayout			= InputLayout;
		desc.IBStripCutValue		= IBStripCutValue;
		desc.PrimitiveTopologyType	= PrimitiveTopologyType;
		desc.NumRenderTargets		= NumRenderTargets;
		//desc.RTVFormats				= RTVFormats;
		memcpy(desc.RTVFormats, RTVFormats, NumRenderTargets * sizeof(RTVFormats[0]));
		desc.DSVFormat				= DSVFormat;
		desc.SampleDesc				= SampleDesc;
		desc.NodeMask				= NodeMask;
		desc.CachedPSO				= CachedPSO;
		desc.Flags					= Flags;

		return desc;
	}

	RHIComputePipelineStateDesc ComputeDesc()
	{
		RHIComputePipelineStateDesc desc = {};

		desc.RootSignature	= RootSignature;
		desc.CS				= CS;
		desc.NodeMask		= NodeMask;
		desc.CachedPSO		= CachedPSO;
		desc.Flags			= Flags;

		return desc;
	}

	RHIRootSignature*			RootSignature;
	RHIShaderBytecode			VS;
	RHIShaderBytecode			PS;
	RHIShaderBytecode			DS;
	RHIShaderBytecode			HS;
	RHIShaderBytecode			GS;
	RHIShaderBytecode			CS;
	RHIStreamOutputDesc			StreamOutput;
	RHIBlendDesc				BlendState;
	UINT						SampleMask;
	RHIRasterizerDesc			RasterizerState;
	RHIDepthStencilDesc			DepthStencilState;
	RHIInputLayoutDesc			InputLayout;
	RHIIndexBufferStripCutValue	IBStripCutValue;
	RHIPrimitiveTopologyType	PrimitiveTopologyType;
	UINT						NumRenderTargets;
	RHIFormat					RTVFormats[8];
	RHIFormat					DSVFormat;
	RHISampleDesc				SampleDesc;
	UINT						NodeMask;
	RHICachedPipelineState		CachedPSO;
	RHIPipelineStateFlags		Flags;
};

class RHICommandList;
class RHIPipelineState
{
public:
	virtual ~RHIPipelineState() { }

	virtual void Release() = 0;

};

struct RHISamplerDesc
{
	RHISamplerDesc()
	{
		Filter			= RHIFilter::MinMagMipLinear;

		AddressU		= RHITextureAddressMode::Wrap;
		AddressV		= RHITextureAddressMode::Wrap;
		AddressW		= RHITextureAddressMode::Wrap;

		MipLODBias		= 0.0f;

		MaxAnisotropy	= 32;

		ComparisonFunc	= RHIComparisonFunc::LEqual;

		MinLOD			= -1000.0f;
		MaxLOD			= 1000.0f;
	}

	RHIFilter				Filter;
	RHITextureAddressMode	AddressU;
	RHITextureAddressMode	AddressV;
	RHITextureAddressMode	AddressW;
	FLOAT					MipLODBias;
	UINT					MaxAnisotropy;
	RHIComparisonFunc		ComparisonFunc;
	FLOAT					BorderColor[4];
	FLOAT					MinLOD;
	FLOAT					MaxLOD;
};

enum class RHIResourceDimension
{
	Unknown = 0,
	Buffer,
	Texture1D,
	Texture2D,
	Texture3D
};

/**
 * Discribes a memory range. When Begin equals End, the range is empty. The size of the range is (End - Begin).
 */
struct RHIRange
{
	explicit RHIRange(const SIZE_T begin, const SIZE_T end)
	: Begin(begin), End(end) { }

	SIZE_T Begin;	//!< The offset, in bytes, denoting the begging of a memory range.
	SIZE_T End;		//!< The offset, in bytes, denoting the end of a memory range. End is one-past-the-end.
};

struct RHIVertexBufferView
{
	RHIGPUVirtualAddress	BufferLocation;
	UINT					SizeInBytes;
	UINT					StrideInBytes;
};

struct RHIIndexBufferView
{
	RHIGPUVirtualAddress	BufferLocation;
	UINT					SizeInBytes;
	RHIFormat				Format;
};

struct RHIConstantBufferViewDesc
{
	RHIGPUVirtualAddress	BufferLocation;
	UINT					SizeInBytes;
};

struct RHIStreamOutputBufferView
{
	RHIGPUVirtualAddress	BufferLocation;
	UINT64					SizeInBytes;
	RHIGPUVirtualAddress	BufferFilledSizeLocation;
};

enum class RHIHeapType
{
	Default = 1,
	Upload,
	Readback,
	Custom
};

enum class RHICpuPageProperty
{
	Unknown = 0,
	NotAvailable,
	WriteCombine,
	WriteBack
};

enum class RHIMemoryPool
{
	Unknown = 0,
	L0,
	L1
};

struct RHIHeapProperties
{
	RHIHeapProperties() { }

	explicit RHIHeapProperties(const RHIHeapProperties& properties) = default;

	RHIHeapProperties(
		const RHICpuPageProperty	cpuPageProperty,
		const RHIMemoryPool			memoryPoolPreference,
		const UINT					creationNodeMask = 1,
		const UINT					nodeMask = 1)
	:	Type(RHIHeapType::Custom),
		CPUPageProperty(cpuPageProperty),
		MemoryPoolPreference(memoryPoolPreference),
		CreationNodeMask(creationNodeMask),
		VisibleNodeMask(nodeMask) { }

	explicit RHIHeapProperties(const RHIHeapType type, const UINT creationNodeMask = 1, const UINT nodeMask = 1)
	:	Type(type),
		CPUPageProperty(RHICpuPageProperty::Unknown),
		MemoryPoolPreference(RHIMemoryPool::Unknown),
		CreationNodeMask(creationNodeMask),
		VisibleNodeMask(nodeMask) { }

	RHIHeapType			Type;
	RHICpuPageProperty	CPUPageProperty;
	RHIMemoryPool		MemoryPoolPreference;
	UINT				CreationNodeMask;
	UINT				VisibleNodeMask;
};

enum class RHIHeapFlag
{
	None						= 0,
	Shared						= 0x1,
	DenyBuffers					= 0x4,
	AllowDisplay				= 0x8,
	SharedCrossAdapter			= 0x20,
	DenyRtDsTextures			= 0x40,
	DenyNonRtDsTextures			= 0x80,
	HardwareProtected			= 0x100,
	AllowAllBuffersAndTextures	= 0,
	AllowOnlyBuffers			= 0xc0,
	AllowOnlyNonRtDsTextures	= 0x44,
	AllowOnlyRtDsTextures		= 0x84
};

enum class RHITextureLayout
{
	Unknown = 0,
	RowMajor,
	UndefinedSwizzle64KB,
	StandardSwizzle64KB
};

enum class RHIResourceFlag
{
	None					= 0,
	AllowRenderTarget		= 0x1,
	AllowDepthStencil		= 0x2,
	AllowUnorderedAccess	= 0x4,
	DenyShaderResource		= 0x8,
	AllowCrossAdapter		= 0x10,
	AllowSimultaneousAccess	= 0x20
};

struct RHIResourceDesc
{
	RHIResourceDesc()
	{
		ZeroMemory(this, sizeof(RHIResourceDesc));
	}
	RHIResourceDesc(const RHIResourceDesc& desc)
	{
		memcpy(this, &desc, sizeof(RHIResourceDesc));
	}
	RHIResourceDesc(
		const RHIResourceDimension	dimension,
		const UINT64				alignment,
		const UINT64				width,
		const UINT					height,
		const UINT16				depthOrArraySize,
		const UINT16				mipLevels,
		const RHIFormat				format,
		const UINT					sampleCount,
		const UINT					sampleQuality,
		const RHITextureLayout		layout,
		const RHIResourceFlag		flags)
	{
		Dimension			= dimension;
		Alignment			= alignment;
		Width				= width;
		Height				= height;
		DepthOrArraySize	= depthOrArraySize;
		MipLevels			= mipLevels;
		Format				= format;
		SampleDesc.Count	= sampleCount;
		SampleDesc.Quality	= sampleQuality;
		Layout				= layout;
		Flags				= flags;
	}

	static inline RHIResourceDesc Buffer(
		const UINT64			width,
		const RHIResourceFlag	flags = RHIResourceFlag::None,
		const UINT64			alignment = 0
	)
	{
		return RHIResourceDesc(RHIResourceDimension::Buffer, alignment, width, 1, 1, 1,
			RHIFormat::Unknown, 1, 0, RHITextureLayout::RowMajor, flags);
	}

	RHIResourceDimension	Dimension;
	UINT64					Alignment;
	UINT64					Width;
	UINT					Height;
	UINT16					DepthOrArraySize;
	UINT16					MipLevels;
	RHIFormat				Format;
	RHISampleDesc			SampleDesc;
	RHITextureLayout		Layout;
	RHIResourceFlag			Flags;
};

enum class RHIResourceState
{
	Common					= 0,
	VertexAndConstantBuffer	= 0x1,
	IndexBuffer				= 0x2,
	RenderTarget			= 0x4,
	UnorderedAccess			= 0x8,
	DepthWrite				= 0x10,
	DepthRead				= 0x20,
	NonPixelShaderResource	= 0x40,
	PixelShaderResource		= 0x80,
	StreamOut				= 0x100,
	IndirectArgument		= 0x200,
	CopyDest				= 0x400,
	CopySource				= 0x800,
	ResolveDest				= 0x1000,
	ResolveSource			= 0x2000,
	GenericRead				= (((((0x1 | 0x2) | 0x40) | 0x80) | 0x200) | 0x800),
	Present					= 0,
	Predication				= 0x200
};

struct RHIDepthStencilValue
{
	FLOAT Depth;
	UINT8 Stencil;
};

struct RHIClearValue
{
	RHIFormat Format;
	union
	{
		FLOAT Color[4];
		RHIDepthStencilValue DepthStencil;
	};
};

enum class RHIRTVDimension
{
	Unknown				= 0,
	Buffer				= 1,
	Texture1D			= 2,
	Texture1DArray		= 3,
	Texture2D			= 4,
	Texture2DArray		= 5,
	Texture2DMS			= 6,
	Texture2DMSArray	= 7,
	Texture3D			= 8
};

struct RHIBufferRTV
{
	UINT64	FirstElement;
	UINT	NumElements;
};

struct RHITex1DRTV
{
	UINT MipSlice;
};

struct RHITex1DArrayRTV
{
	UINT MipSlice;
	UINT FirstArraySlice;
	UINT ArraySize;
};

struct RHITex2DRTV
{
	UINT MipSlice;
	UINT PlaneSlice;
};

struct RHITex2DMSRTV
{
	UINT UnusedField;
};

struct RHITex2DArrayRTV
{
	UINT MipSlice;
	UINT FrstArraySlice;
	UINT ArraySize;
	UINT PlaneSlice;
};

struct RHITex2DMSArrayRTV
{
	UINT FirstArraySlice;
	UINT ArraySize;
};

struct RHITex3DRTV
{
	UINT MipSlice;
	UINT FirstWSlice;
	UINT WSize;
};

struct RHIRenderTargetViewDesc
{
	RHIFormat		Format;
	RHIRTVDimension	ViewDimension;
	union
	{
		RHIBufferRTV		Buffer;
		RHITex1DRTV			Texture1D;
		RHITex1DArrayRTV	Texture1DArray;
		RHITex2DRTV			Texture2D;
		RHITex2DArrayRTV	Texture2DArray;
		RHITex2DMSRTV		Texture2DMS;
		RHITex2DMSArrayRTV	Texture2DMSArray;
		RHITex3DRTV			Texture3D;
	};
};

enum class RHIDSVDimension
{
	Unknown				= 0,
	Texture1D			= 1,
	Texture1DArray		= 2,
	Texture2D			= 3,
	Texture2DArray		= 4,
	Texture2DMS			= 5,
	Texture2DMSArray	= 6
};

enum class RHIDSVFlags
{
	None			= 0,
	ReadOnlyDepth	= 0x1,
	ReadOnlyStencil	= 0x2
};

struct RHITex1DDSV
{
	UINT MipSlice;
};

struct RHITex1DArrayDSV
{
	UINT MipSlice;
	UINT FirstArraySlice;
	UINT ArraySize;
};

struct RHITex2DDSV
{
	UINT MipSlice;
};

struct RHITex2DArrayDSV
{
	UINT MipSlice;
	UINT FirstArraySlice;
	UINT ArraySize;
};

struct RHITex2DMSDSV
{
	UINT UnusedField;
};

struct RHITex2DMSArrayDSV
{
	UINT FirstArraySlice;
	UINT ArraySize;
};

struct RHIDepthStencilViewDesc
{
	RHIFormat		Format;
	RHIDSVDimension	ViewDimension;
	RHIDSVFlags		Flags;
	union
	{
		RHITex1DDSV			Texture1D;
		RHITex1DArrayDSV	Texture1DArray;
		RHITex2DDSV			Texture2D;
		RHITex2DArrayDSV	Texture2DArray;
		RHITex2DMSDSV		Texture2DMS;
		RHITex2DMSArrayDSV	Texture2DMSArray;
	};
};

enum class RHISRVDimension
{
	Unknown				= 0,
	Buffer				= 1,
	Texture1D			= 2,
	Texture1DArray		= 3,
	Texture2D			= 4,
	Texture2DArray		= 5,
	Texture2DMS			= 6,
	Texture2DMSarray	= 7,
	Texture3D			= 8,
	TextureCube			= 9,
	TextureCubeArray	= 10
};

enum class RHIBufferSRVFlags
{
	None	= 0,
	Raw		= 0x1
};

struct RHIBufferSRV
{
	UINT64				FirstElement;
	UINT				NumElements;
	UINT				StructureByteStride;
	RHIBufferSRVFlags	Flags;
};

struct RHITex1DSRV
{
	UINT	MostDetailedMip;
	UINT	MipLevels;
	FLOAT	ResourceMinLODClamp;
};

struct RHITex1DArraySRV
{
	UINT	MostDetailedMip;
	UINT	MipLevels;
	UINT	FirstArraySlice;
	UINT	ArraySize;
	FLOAT	ResourceMinLODClamp;
};

struct RHITex2DSRV
{
	UINT	MostDetailedMip;
	UINT	MipLevels;
	UINT	PlaneSlice;
	FLOAT	ResourceMinLODClamp;
};

struct RHITex2DArraySRV
{
	UINT	MostDetailedMip;
	UINT	MipLevels;
	UINT	FirstArraySlice;
	UINT	ArraySize;
	UINT	PlaneSlice;
	FLOAT	ResourceMinLODClamp;
};

struct RHITex2DMSSRV
{
	UINT UnusedField;
};

struct RHITex2DMSArraySRV
{
	UINT FirstArraySlice;
	UINT ArraySize;
};

struct RHITex3DSRV
{
	UINT	MostDetailedMip;
	UINT	MipLevels;
	FLOAT	ResourceMinLODClamp;
};

struct RHITexCubeSRV
{
	UINT	MostDetailedMip;
	UINT	MipLevels;
	FLOAT	ResourceMinLODClamp;
};

struct RHITexCubeArraySRV
{
	UINT	MostDetailedMip;
	UINT	MipLevels;
	UINT	First2DArrayFace;
	UINT	NumCubes;
	FLOAT	ResourceMinLODClamp;
};

struct RHIShaderResourceViewDesc
{
	RHIFormat		Format;
	RHISRVDimension	ViewDimension;
	UINT			Shader4ComponentMapping;
	union
	{
		RHIBufferSRV		Buffer;
		RHITex1DSRV			Texture1D;
		RHITex1DArraySRV	Texture1DArray;
		RHITex2DSRV			Texture2D;
		RHITex2DArraySRV	Texture2DArray;
		RHITex2DMSSRV		Texture2DMS;
		RHITex2DMSArraySRV	Texture2DMSArray;
		RHITex3DSRV			Texture3D;
		RHITexCubeSRV		TextureCube;
		RHITexCubeArraySRV	TextureCubeArray;
	};
};

enum class RHIUAVDimension
{
	Unknown			= 0,
	Buffer			= 1,
	Texture1D		= 2,
	Texture1DArray	= 3,
	Texture2D		= 4,
	Texture2DArray	= 5,
	Texture3D		= 8
};

enum class RHIBufferUAVFlags
{
	None	= 0,
	Raw		= 0x1
};

struct RHIBufferUAV
{
	UINT64				FirstElement;
	UINT				NumElements;
	UINT				StructureByteStride;
	UINT64				CounterOffsetInBytes;
	RHIBufferUAVFlags	Flags;
};

struct RHITex1DUAV
{
	UINT MipSlice;
};

struct RHITex1DArrayUAV
{
	UINT MipSlice;
	UINT FirstArraySlice;
	UINT ArraySize;
};

struct RHITex2DUAV
{
	UINT MipSlice;
	UINT PlaneSlice;
};

struct RHITex2DArrayUAV
{
	UINT MipSlice;
	UINT FirstArraySlice;
	UINT ArraySize;
	UINT PlaneSlice;
};

struct RHITex3DUAV
{
	UINT MipSlice;
	UINT FirstWSlice;
	UINT WSize;
};

struct RHIUnorderedAccessViewDesc
{
	RHIFormat		Format;
	RHIUAVDimension	ViewDimension;
	union
	{
		RHIBufferUAV		Buffer;
		RHITex1DUAV			Texture1D;
		RHITex1DArrayUAV	Texture1DArray;
		RHITex2DUAV			Texture2D;
		RHITex2DArrayUAV	Texture2DArray;
		RHITex3DUAV			Texture3D;
	};
};

enum class RHITextureCopyType
{
	SubresourceIndex	= 0,
	PlacedFootprint		= 1
};

struct RHISubresourceFootprint
{
	RHIFormat	Format;
	UINT		Width;
	UINT		Height;
	UINT		Depth;
	UINT		RowPitch;
};

struct RHIPlacedSubresourceFootprint
{
	UINT64					Offset;
	RHISubresourceFootprint	Footprint;
};

class RHIResource;
struct RHITextureCopyLocation
{
	RHIResource*		Resource;
	RHITextureCopyType	Type;
	union
	{
		RHIPlacedSubresourceFootprint	PlacedFootprint;
		UINT							SubresourceIndex;
	};
};

class RHIResource
{
public:
	virtual ~RHIResource() { }

	virtual void Release() = 0;

	virtual void SetName(CConstString name) = 0;

	virtual RHIGPUVirtualAddress GetGPUVirtualAddress() const = 0;

	virtual RHIResourceDesc GetDesc() const = 0;

	virtual void Map(const UINT subresource, const RHIRange* readRange, void** data) = 0;
	virtual void Unmap(const UINT subresource, const RHIRange* writtenRange) = 0;

	virtual void InitializeSubData(const UINT subresource, const void* data, const size_t size) = 0;

	virtual void WriteToSubresource(UINT dstSubresource, const RHIBox* dstBox, const void* srcData, const UINT srcRowPitch, const UINT srcDepthPitch) = 0;
};

class RHISwapChain
{
public:
	virtual void Release() = 0;

	virtual void Present(const UINT syncInterval, const UINT flags) = 0;

	virtual bool GetDesc(RHISwapChainDesc* desc) const = 0;

	virtual void GetBuffer(const UINT buffer, RHIResource*& surface) = 0;

	virtual UINT GetCurrentBackBufferIndex() = 0;

	virtual void SetMaximumFrameLatency(const UINT latency) = 0;
	virtual HANDLE GetFrameLatencyWaitableObject() = 0;

protected:

};

struct RHIBlob
{
	virtual void Release() = 0;

	virtual void* GetBufferPointer() = 0;
	virtual SIZE_T GetBufferSize() = 0;

	virtual bool IsNull() = 0;
};

struct RHIBlobWrapper
{
	explicit RHIBlobWrapper(RHIBlob* blob)
		: Blob(blob) { }

	~RHIBlobWrapper()
	{
		RHISafeRelease(Blob);
	}

	void* GetBufferPointer() { return Blob->GetBufferPointer(); }
	SIZE_T GetBufferSize() { return Blob->GetBufferSize(); }

	RHIBlob* Blob;
};

struct RHISubresourceData
{
	const void*	Data;
	LongPointer	RowPitch;
	LongPointer	SlicePitch;
};

class RHICommandAllocator
{
public:
	virtual void Release() = 0;
	virtual void Reset() = 0;
protected:
};

struct RHICPUDescriptorHandle
{
	RHICPUDescriptorHandle() : ptr(0)
	{}
	explicit RHICPUDescriptorHandle(SIZE_T pointer) : ptr(pointer)
	{}
	RHICPUDescriptorHandle(const RHICPUDescriptorHandle& other, INT offsetInDescriptors, UINT descriptorIncrementSize)
	{
		InitOffsetted(other, offsetInDescriptors, descriptorIncrementSize);
	}

	RHICPUDescriptorHandle& Offset(INT offsetInDescriptor, UINT descriptorIncrementSize)
	{
		ptr += offsetInDescriptor * descriptorIncrementSize;
		return *this;
	}

	inline void InitOffsetted(const RHICPUDescriptorHandle& base, INT offsetInDescriptors, UINT descriptorIncrementSize)
	{
		InitOffsetted(*this, base, offsetInDescriptors, descriptorIncrementSize);
	}

	static inline void InitOffsetted(RHICPUDescriptorHandle& handle, const RHICPUDescriptorHandle& base, INT offsetInDescriptors, UINT descriptorIncrementSize)
	{
		handle.ptr = base.ptr + offsetInDescriptors * descriptorIncrementSize;
	}

	SIZE_T ptr;
};

struct RHIGPUDescriptorHandle
{
	RHIGPUDescriptorHandle() : ptr(0)
	{}
	explicit RHIGPUDescriptorHandle(SIZE_T pointer) : ptr(pointer)
	{}
	RHIGPUDescriptorHandle(const RHIGPUDescriptorHandle& other, INT offsetInDescriptors, UINT descriptorIncrementSize)
	{
		InitOffsetted(other, offsetInDescriptors, descriptorIncrementSize);
	}

	RHIGPUDescriptorHandle& Offset(INT offsetInDescriptor, UINT descriptorIncrementSize)
	{
		ptr += offsetInDescriptor * descriptorIncrementSize;
		return *this;
	}

	inline void InitOffsetted(const RHIGPUDescriptorHandle& base, INT offsetInDescriptors, UINT descriptorIncrementSize)
	{
		InitOffsetted(*this, base, offsetInDescriptors, descriptorIncrementSize);
	}

	static inline void InitOffsetted(RHIGPUDescriptorHandle& handle, const RHIGPUDescriptorHandle& base, INT offsetInDescriptors, UINT descriptorIncrementSize)
	{
		handle.ptr = base.ptr + offsetInDescriptors * descriptorIncrementSize;
	}
	UINT64 ptr;
};

class RHIDescriptorHeap
{
public:
	virtual void Release() = 0;

	virtual void SetName(CString name) = 0;

	virtual RHICPUDescriptorHandle GetCPUDescriptorHandleForHeapStart() = 0;
	virtual RHIGPUDescriptorHandle GetGPUDescriptorHandleForHeapStart() = 0;
};

enum class RHIClearFlags
{
	Depth	= 0x1,
	Stencil	= 0x2
};

enum class RHIFenceFlags
{
	None				= 0,
	Shared				= 0x1,
	SharedCrossAdapter	= 0x2
};

class RHIFence
{
public:
	virtual void Release() = 0;

	virtual UINT64 GetCompletedValue() = 0;
	virtual void SetEventOnCompletion(const UINT64 value, HANDLE event) = 0;
};

enum class RHIResourceBarrierType
{
	Transition	= 0,
	Aliasing	= (Transition + 1),
	UAV			= (Aliasing + 1)
};

enum class RHIResourceBarrierFlag
{
	None		= 0,
	BeginOnly	= 0x1,
	BeginEnd	= 0x2
};

struct RHIResourceTransitionBarrier
{
	RHIResource*		Resource;
	UINT				Subresource;
	RHIResourceState	StateBefore;
	RHIResourceState	StateAfter;
};

struct RHIResourceAliasingBarrier
{
	RHIResource*	ResourceBefore;
	RHIResource*	ResourceAfter;
};

struct RHIResourceUAVBarrier
{
	RHIResource* Resource;
};

struct RHIResourceBarrier
{
	RHIResourceBarrier()
	{
		ZeroMemory(this, sizeof(RHIResourceBarrier));
	}
	RHIResourceBarrier(const RHIResourceBarrier& o)
	{
		memcpy(this, &o, sizeof(RHIResourceBarrier));
	}
	static inline RHIResourceBarrier MakeTransition(
		RHIResource*					resource,
		const RHIResourceState			stateBefore,
		const RHIResourceState			stateAfter,
		const UINT						subresource = RHIConstants::kResourceBarrierAllSubresources,
		const RHIResourceBarrierFlag	flags = RHIResourceBarrierFlag::None)
	{
		RHIResourceBarrier result		= {};

		result.Type						= RHIResourceBarrierType::Transition;
		result.Flags					= flags;
		result.Transition.Resource		= resource;
		result.Transition.StateBefore	= stateBefore;
		result.Transition.StateAfter	= stateAfter;
		result.Transition.Subresource	= subresource;

		return result;
	}

	RHIResourceBarrierType	Type;
	RHIResourceBarrierFlag	Flags;
	union
	{
		RHIResourceTransitionBarrier	Transition;
		RHIResourceAliasingBarrier		Aliasing;
		RHIResourceUAVBarrier			UAV;
	};
};

enum class RHICommandListType
{
	Direct = 0,
	Bundle,
	Compute,
	Copy
};

enum class RHICommandQueueFlags
{
	None				= 0,
	DisableGPUTimeout	= 0x1
};

struct RHICommandQueueDesc
{
	RHICommandListType		Type;
	INT						Priority;
	RHICommandQueueFlags	Flags;
	UINT					NodeMask;
};

class RHICommandQueue
{
public:
	virtual void Release() = 0;

	virtual void ExecuteCommandLists(const UINT count, RHICommandList** commandLists) = 0;

	virtual void Signal(RHIFence* fence, UINT64 value) = 0;
protected:

};

struct RHICommand
{
	virtual ~RHICommand() { }
	virtual void Execute() = 0;
};

class RHICommandList
{
public:
	virtual ~RHICommandList();
	virtual void Release() = 0;

	virtual void ClearRenderTargetView(
		RHICPUDescriptorHandle	renderTargetView,
		const FLOAT*			colorRGBA,
		const UINT				numRects,
		const RHIRect*			rects) = 0;

	virtual void ClearDepthStencilView(
		RHICPUDescriptorHandle	depthStencilView,
		RHIClearFlags			clearFlags,
		const FLOAT				depth,
		const UINT8				stencil,
		const UINT				numRects,
		const RHIRect*			rects) = 0;

	virtual void ClearUnorderedAccessViewUint(
		const RHIGPUDescriptorHandle	viewGPUHandleInCurrentHeap,
		const RHICPUDescriptorHandle	viewCPUHandle,
		RHIResource*					resource,
		const UINT						values[4],
		const UINT						numRects,
		const RHIRect*					rects) = 0;

	virtual void RSSetViewports(const UINT numViewports, const RHIViewport* viewports) = 0;
	virtual void RSSetScissorRects(const UINT numRects, const RHIRect* rects) = 0;

	virtual void SetPipelineState(RHIPipelineState* pso) = 0;

	virtual void SetGraphicsRootSignature(RHIRootSignature* rootSignature) = 0;
	virtual void SetGraphicsRootDescriptorTable(const UINT rootParameterIndex, const RHIGPUDescriptorHandle baseDescriptor) = 0;
	virtual void SetGraphicsRootConstantBufferView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation) = 0;
	virtual void SetGraphicsRoot32BitConstants(const UINT rootParameterIndex, const UINT num32VitValues, const void* data, const UINT destOffsetIn32BitValues) = 0;

	virtual void SetComputeRootSignature(RHIRootSignature* rootSignature) = 0;
	virtual void SetComputeRootDescriptorTable(const UINT rootParameterIndex, const RHIGPUDescriptorHandle baseDescirptor) = 0;
	virtual void SetComputeRootConstantBufferView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation) = 0;
	virtual void SetComputeRootUnorderedAccessView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation) = 0;

	virtual void SetDescriptorHeaps(const UINT numDescriptorHeaps, RHIDescriptorHeap** heaps) = 0;

	virtual void IASetIndexBuffer(const RHIIndexBufferView* view) = 0;
	virtual void IASetPrimitiveTopology(const RHIPrimitiveTopology primitiveTopology) = 0;
	virtual void IASetVertexBuffers(const UINT startSlot, const UINT numViews, const RHIVertexBufferView* pViews) = 0;

	virtual void ResourceBarrier(const UINT numBarriers, const RHIResourceBarrier* barriers) = 0;

	virtual void CopyResource(RHIResource* dstResource, RHIResource* srcResource) = 0;

	virtual void CopyTextureRegion(
		const RHITextureCopyLocation*	dst,
		const UINT						dstX,
		const UINT						dstY,
		const UINT						dstZ,
		const RHITextureCopyLocation*	src,
		const RHIBox*					srcBox) = 0;

	virtual void CopyBufferRegion(
		RHIResource*	dstBuffer,
		UINT64			dstOffset,
		RHIResource*	srcBuffer,
		UINT64			srcOffset,
		UINT64			numBytes) = 0;

	virtual void OMSetBlendFactor(const FLOAT blendFactor[4]) = 0;
	virtual void OMSetRenderTargets(
		const UINT						numRenderTargetDescriptors,
		const RHICPUDescriptorHandle*	renderTargetDescriptors,
		const BOOL						RTsSingleHandleToDescriptorRange,
		const RHICPUDescriptorHandle*	depthStencilDescriptor) = 0;

	virtual void DrawInstanced(
		const uint32_t vertexCountPerInstance,
		const uint32_t instanceCount,
		const uint32_t startVertexLocation,
		const uint32_t startInstanceLocation) = 0;

	virtual void DrawIndexedInstanced(
		const uint32_t indexCountPerInstance,
		const uint32_t instanceCount,
		const uint32_t startIndexLocation,
		const int32_t baseVertexLocation,
		const uint32_t startInstanceLocation) = 0;

	virtual void Dispatch(const UINT threadGroupCountX, const UINT threadGroupCountY, const UINT threadGroupCountZ) = 0;

	virtual void ExecuteBundle(RHICommandList* commandList) = 0;

	virtual void SOSetTargets(const UINT startSlot, const UINT numViews, const RHIStreamOutputBufferView* views) = 0;
	virtual void DrawStreamOutput(const RHIResource* streamOutput) = 0;

	virtual void Reset(RHICommandAllocator* commandAllocator, RHIPipelineState* pso) = 0;
	virtual void Close() = 0;

	const std::vector<std::shared_ptr<RHICommand>> Commands() const { return mCommands; }

protected:
	void ClearCommands();

protected:
	std::vector<std::shared_ptr<RHICommand>>	mCommands;
	std::vector<std::shared_ptr<RHICommand>>	mDelayedCommands; // call this commands before draw-call (e.g. glEnableVertexPointer and etc..)
};

class RHIRenderingDevice
{
public:
	virtual ~RHIRenderingDevice() { }

	virtual void Release() = 0;

#ifdef WIN32
	virtual bool Initialize(HWND hWnd) = 0;
#endif

	virtual bool CheckFeatureSupport(
		RHIFeature	feature,
		void*		featureSupportData,
		const UINT	featureSupportDataSize) = 0;

	virtual RHICommandQueue* CreateCommandQueue(const RHICommandQueueDesc* desc) = 0;

	virtual RHISwapChain* CreateSwapChain(RHISwapChainDesc* desc, RHICommandQueue* commandQueue) = 0;
	virtual RHISwapChain* CreateSwapChain(RHISwapChainDesc1* desc, RHISwapChainFullscreenDesc* fullscreenDesc, RHICommandQueue* commandQueue) = 0;

	virtual RHICommandAllocator* CreateCommandAllocator(const RHICommandListType listType) = 0;

	virtual RHICommandList* CreateCommandList(const UINT nodeMask, const RHICommandListType type, RHICommandAllocator* allocator, RHIPipelineState* pipelineState) = 0;

	virtual void SerializeRootSignature(
		const RHIRootSignatureDesc*		desc,
		const RHIRootSignatureVersion	version,
		RHIBlob**						blob,
		RHIBlob**						errorBlob) = 0;

	virtual void SerializeVersionedRootSignature(
		const RHIVersionedRootSignatureDesc*	desc,
		const RHIRootSignatureVersion			maxVersion,
		RHIBlob**								blob,
		RHIBlob**								errorBlob) = 0;

	virtual RHIRootSignature* CreateRootSignature(
		const UINT					nodeMask,
		const void*					blobWithRootSignature,
		const SIZE_T				blobLengthInBytes) = 0;

	virtual RHIPipelineState* CreateGraphicsPipelineState(RHIGraphicsPipelineStateDesc* psoDesc) = 0;
	virtual RHIPipelineState* CreateComputePipelineState(RHIComputePipelineStateDesc* psoDesc) = 0;

	virtual RHIDescriptorHeap* CreateDescriptorHeap(const RHIDescriptorHeapDesc* desc) = 0;

	virtual UINT GetDescriptorHandleIncrementSize(RHIDescriptorHeapType type) = 0;

	virtual void CreateRenderTargetView(
		RHIResource*					resource,
		const RHIRenderTargetViewDesc*	desc,
		RHICPUDescriptorHandle			destDescriptor) = 0;

	virtual void CreateDepthStencilView(
		RHIResource*					resource,
		const RHIDepthStencilViewDesc*	desc,
		RHICPUDescriptorHandle			destDescriptor) = 0;

	virtual void CreateShaderResourceView(
		RHIResource*						resource,
		const RHIShaderResourceViewDesc*	desc,
		RHICPUDescriptorHandle				destDescriptor) = 0;

	virtual void CreateConstantBufferView(const RHIConstantBufferViewDesc* desc, const RHICPUDescriptorHandle destDescriptor) = 0;

	virtual void CreateUnorderedAccessView(
		RHIResource*				resource,
		RHIResource*				counterResource,
		RHIUnorderedAccessViewDesc*	desc,
		RHICPUDescriptorHandle		destDescriptor) = 0;

	virtual RHIFence* CreateFence(const UINT64 initialValue, const RHIFenceFlags flags) = 0;

	virtual RHIResource* CreateCommittedResource(
		const RHIHeapProperties*	heapProperties,
		const RHIHeapFlag			heapFlags,
		const RHIResourceDesc*		desc,
		const RHIResourceState		initialResourceStates,
		const RHIClearValue*		optimizedClearValue) = 0;

	virtual RHIResource* CreateTextureFromFile(const String& fullPath) = 0;
	virtual void LoadTextureFromFile(const String& fullPath, RHIResourceDesc* desc, RHISubresourceData* subresData) = 0;

	virtual void CopyDescriptorsSimple(
		const UINT				numDescriptors,
		RHICPUDescriptorHandle	destDescriptorRangeStart,
		RHICPUDescriptorHandle	srcDescriptorRangeStart,
		RHIDescriptorHeapType	descriptorHeapsType) = 0;

	virtual void GetCopyableFootprints(
		const RHIResourceDesc*			desc,
		const UINT						firstSubresource,
		const UINT						numSubresource,
		const UINT64					baseOffset,
		RHIPlacedSubresourceFootprint*	layouts,
		UINT*							numRows,
		UINT64*							rowSizeInBytes,
		UINT64*							totalBytes) = 0;


public:
	// Uses UpdateSubresources and allocates a new upload buffer.
	virtual void UploadResource(RHIResource* destResource, const UINT firstSubresource, const UINT numSubresources, RHISubresourceData* subresources) = 0;

public:
	// Returns required size of a buffer to be used for data upload/
	virtual UINT64 GetRequiredIntermediateSize(RHIResource* destinationResource, const UINT firstSubresource, const UINT numSubresources) = 0;

	// Heap-allocating UpdateSubresources implementation
	virtual UINT64 UpdateSubresources(
		RHICommandList*		cmdList,
		RHIResource*		destResource,
		RHIResource*		intermediate,
		const UINT64		intermediateOffset,
		const UINT			firstSubresource,
		const UINT			numSubresources,
		RHISubresourceData*	srcData) = 0;

	// Stack-allocating UpdateSubresources implementation.
	virtual UINT64 UpdateSubresources(
		const UINT			maxSubresource,
		RHICommandList*		cmdList,
		RHIResource*		destResource,
		RHIResource*		intermediate,
		const UINT64		intermediateOffset,
		const UINT			firstSubresource,
		const UINT			numSubresources,
		RHISubresourceData*	srcData,
		const bool			useFullBuffer = true) = 0;

	// All arrays must be populated (e.g. by calling GetCopyableFootprints).
	virtual UINT64 UpdateSubresources(
		RHICommandList*							cmdList,
		RHIResource*							destResource,
		RHIResource*							intermediate,
		const UINT								firstSubresource,
		const UINT								numSubresources,
		const UINT64							requiredSize,
		const RHIPlacedSubresourceFootprint*	layouts,
		const UINT*								numRows,
		const UINT64*							rowSizeInBytes,
		const RHISubresourceData*				srcData) = 0;
};
