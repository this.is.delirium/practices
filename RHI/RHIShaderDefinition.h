#pragma once
#include "StdafxRHI.h"
#include "RHI.h"

#include <StringUtils.h>

class RHIShaderDefinition
{
public:
	using Shaders			= std::map<RHIShaderType, String>;
	using InitializerList	= std::initializer_list<Shaders::value_type>;

	RHIShaderDefinition()
		: mHashTypes(0u)
		, mHashNames(0u)
	{
		//
	}

	RHIShaderDefinition(const std::string& name, const InitializerList& list, const RHIInputLayoutDesc& inputLayout)
		: mName(StringUtils::Converter::ToLower(name)), mShaders(list), mInputLayout(inputLayout)
	{
		ComputeHashTypes();
		ComputeHashNames();
	}

	bool operator==(const RHIShaderDefinition& rhs) const
	{
		return Hash() == rhs.Hash();
	}

	size_t Hash() const { return std::hash_value(mName); }

	const Shaders& shaders() const { return mShaders; }

	const RHIInputLayoutDesc& inputLayout() const { return mInputLayout; }

protected:
	void ComputeHashTypes()
	{
		mHashTypes = 0;
		for (auto& shader : mShaders)
		{
			mHashTypes |= 1ull << static_cast<size_t>(shader.first);
		}
	}

	void ComputeHashNames()
	{
		String totalName;
		for (auto& shader : mShaders)
		{
			totalName += shader.second;
		}

		std::hash<String> strHash;
		mHashNames = strHash(totalName);
	}

protected:
	std::string			mName;
	RHIInputLayoutDesc	mInputLayout;
	Shaders				mShaders;
	size_t				mHashTypes;
	size_t				mHashNames;
};

// Extend std::hash by RHIShaderDefinition.
namespace std
{
template<>
class hash<RHIShaderDefinition>
{
public:
	size_t operator()(const RHIShaderDefinition& shader) const
	{
		return shader.Hash();
	}
};
}

