#pragma once
#include "StdafxRHI.h"
#include "RHIBuffer.h"
#include "RHIDescriptorHandle.h"

template<class ConstBuffer>
class RHIConstBuffer : public RHIBuffer
{
public:
	using Type = ConstBuffer;

public:
	void Release() override final;

	void UpdateDataOnGPU();

public:
	FORCEINLINE void MarkDirty() { mIsDirty = true; }
	FORCEINLINE bool IsDirty() const { return mIsDirty == true; }

	FORCEINLINE RHIResourceState ResourceState() override final { return RHIResourceState::VertexAndConstantBuffer; }

	FORCEINLINE ConstBuffer& Data() { return mData; }
	FORCEINLINE const ConstBuffer& Data() const { return mData; }

protected:
	void SetHeapDescriptor();

	void CreateDerivedViews() override final;

protected:
	ConstBuffer	mData;
	bool		mIsDirty = true;
};

#include "RHIConstBuffer.t"
