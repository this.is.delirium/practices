#pragma once
#include "StdafxRHI.h"
#include "RHI.h"

class IRenderer;

namespace RHIUtils
{
	class RootParameter
	{
	public:
		RootParameter& InitAsDescriptorTable(const int numRanges, const RHIShaderVisibility visibility);
		void InitAsConstantBufferView(const UINT shaderRegister, const UINT registerSpace = 0, const RHIShaderVisibility visibility = RHIShaderVisibility::All);

		FORCEINLINE RootParameter& InitRange(
			const int				index,
			RHIDescriptorRangeType	type,
			const UINT				numDescriptors,
			const UINT				baseShaderRegister,
			const UINT				registerSpace = 0,
			const UINT				offsetInDescriptorsFromTableStart = RHIConstants::kDescriptorRangeOffsetAppend)
		{
			mRanges[index].Init(type, numDescriptors, baseShaderRegister, registerSpace, offsetInDescriptorsFromTableStart);
			return *this;
		}

		FORCEINLINE RHIDescriptorRange& operator[](const int index) { return mRanges[index]; }
		FORCEINLINE operator RHIRootParameter&() { return mParameter; }

	protected:
		std::unique_ptr<RHIDescriptorRange[]>	mRanges;
		RHIRootParameter						mParameter;
	};

	class RootSignature
	{
	public:
		RootSignature() { }

		RootSignature(const UINT nbOfParameters, const UINT nbOfStaticSamplers = 0)
		{
			Init(nbOfParameters, nbOfStaticSamplers);
		}

		void Init(const UINT nbOfParameters, const UINT nbOfStaticSamplers = 0);
		RHIRootSignature* Finalize(const RHIRootSignatureFlags flags = RHIRootSignatureFlags::None);

		FORCEINLINE RootParameter& operator[](const int index) { return mParameters[index]; }
		FORCEINLINE RHIStaticSamplerDesc& SamplerDesc(const int index) { return mStaticSamplerDescs[index]; }

		FORCEINLINE const RHIRootSignatureDesc* Desc() const { return &mDesc; }

	protected:
		RHIRootSignatureDesc					mDesc;
		std::unique_ptr<RootParameter[]>		mParameters;
		std::unique_ptr<RHIStaticSamplerDesc[]>	mStaticSamplerDescs;
	};
}
