#pragma once
#include "StdafxRHI.h"
#include "RHI.h"
#include "RHIDescriptorHandle.h"

class RHIBuffer
{
public:
	RHIBuffer(const RHIResourceFlag flag = RHIResourceFlag::None)
		: mFlags(flag) { }

	FORCEINLINE virtual void Release()
	{
		RHISafeRelease(mBuffer);
	}

	virtual void Initialize(const String& name, const UINT64 maxWidth, const UINT numElements, const UINT strideInBytes, const void* data = nullptr, const RHIHeapType heapType = RHIHeapType::Default);

public:
	virtual RHIResourceState ResourceState() = 0;

	FORCEINLINE UINT NumElements() const { return mNumElements; }
	FORCEINLINE RHIResource* Buffer() const { return mBuffer; }

	FORCEINLINE bool IsNull() const { return mBuffer == nullptr; }

	FORCEINLINE RHIHeapType HeapType() const { return mHeapType; }

	FORCEINLINE void SetNumElements(const UINT count) { mNumElements = count; }

	FORCEINLINE RHIGPUVirtualAddress GpuVirtualAddress() const { return mBuffer->GetGPUVirtualAddress(); }
	FORCEINLINE const RHIDescriptorHandle& SrvHandle() const { return mSrvHandle; }
	FORCEINLINE const RHIDescriptorHandle& UavHandle() const { return mUavHandle; }

protected:
	virtual void CreateDerivedViews() = 0;

protected:
	RHIHeapType			mHeapType;

	RHIResourceFlag		mFlags;
	RHIResource*		mBuffer			= nullptr;
	UINT				mNumElements	= 0;
	UINT				mStrideInBytes	= 0;

	RHIDescriptorHandle	mSrvHandle;
	RHIDescriptorHandle	mUavHandle;
};
