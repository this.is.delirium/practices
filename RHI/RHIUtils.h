#pragma once
#include "StdafxRHI.h"
#include "RHI.h"

class MRTPass;

namespace RHIUtils
{
	namespace Convert
	{
		RHIDSVDimension ResourceDimToDsvDim(const RHIResourceDimension dim);
		RHISRVDimension ResourceDimToSrvDim(const RHIResourceDimension dim, const UINT16 depthOrArraySize);
		RHIUAVDimension ResourceDimToUavDim(const RHIResourceDimension dim);

		RHIFormat ResourceFormatToDsvFormat(const RHIFormat format);
		RHIFormat ResourceFormatToSrvFormat(const RHIFormat format);
	}

	namespace Fill
	{
		struct ShaderInfo
		{
			String			Name;
			RHIShaderType	Type	= RHIShaderType::Unknown;
			RHIBlob*		Blob	= nullptr;
		};

		struct ShadersArray
		{
			void Push(const String& name, const RHIShaderType type);

			ShaderInfo Shaders[ToInt(RHIShaderType::Count)];
		};

		void DepthStencilViewDesc(const RHIResourceDesc* resDesc, RHIDepthStencilViewDesc* dsvDesc);
		void ShaderResourceViewDesc(const RHIResourceDesc* resDesc, RHIShaderResourceViewDesc* srvDesc);
		void UnorderedAccessViewDesc(const RHIResourceDesc* resDesc, RHIUnorderedAccessViewDesc* uavDesc);
		void GraphicsPipelineStateDesc(MRTPass* mrtPass, RHIGraphicsPipelineStateDesc* psoDesc);
		void GraphicsPipelineStateDesc(const ShadersArray& shaders, RHIGraphicsPipelineStateDesc* psoDesc);
	}

	FORCEINLINE constexpr UINT64 GetConstBufferSize() { return 1024 * 4; }

	size_t GetSize(const RHIFormat format);

	RHISubresourceData GetSubresource(const void* data, const size_t size);

	RHIShaderBytecode* GetShaderBytecode(RHIGraphicsPipelineStateDesc* psoDesc, const RHIShaderType type);
}
