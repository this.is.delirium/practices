#include "StdafxRHI.h"
#include "RHIDescriptorHeapManager.h"
#include <Render/GraphicsCore.h>
#include <Logger.h>

/**
 * Function : Release
 */
void RHIDescriptorHeapManager::Release()
{
	RHISafeRelease(mHeap);
}

/**
 * Function : CreateHeap
 */
void RHIDescriptorHeapManager::Initialize(const RHIDescriptorHeapDesc* desc, CString name)
{
	using namespace GraphicsCore;

	mHeap = gDevice->CreateDescriptorHeap(desc);
	if (name)
	{
		mName = name;
		mHeap->SetName(name);
	}

	mMaxDescriptors	= desc->NumDescriptors;
	mDescriptorSize	= gDevice->GetDescriptorHandleIncrementSize(desc->Type);

	for (UINT i = 0; i < mMaxDescriptors; ++i)
		mFreeDescriptors.push(mMaxDescriptors - i - 1);
}

/**
 * Function : GetNewDescriptor
 */
std::vector<int> RHIDescriptorHeapManager::GetNewDescriptors(const UINT numDescriptors)
{
	std::vector<int> descriptors;
	if (!HasDescriptors(numDescriptors))
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Can't get %d new descriptors in '%s' heap"), numDescriptors, mName.c_str());
		return descriptors;
	}

	for (UINT i = 0; i < numDescriptors; ++i)
	{
		descriptors.push_back(mFreeDescriptors.top());
		mFreeDescriptors.pop();
	}

	return descriptors;
}

/**
 * Function : GetStartContinuousDescriptors
 */
int RHIDescriptorHeapManager::GetStartContinuousDescriptors(const UINT numDescriptors)
{
	if (!HasDescriptors(numDescriptors))
		return kNullDescriptor;

	std::vector<int> test;

	int startDesc	= mFreeDescriptors.top();
	int prevDesc	= startDesc;
	mFreeDescriptors.pop();
	test.push_back(prevDesc);

	for (UINT i = 1; i < numDescriptors; ++i)
	{
		int currDesc = mFreeDescriptors.top();

		if (currDesc != (prevDesc + 1))
			break;

		test.push_back(currDesc);
		mFreeDescriptors.pop();
		prevDesc = currDesc;
	}

	if (test.size() != numDescriptors)
	{
		for (auto& it = test.rbegin(); it != test.rend(); ++it)
			mFreeDescriptors.push(*it);

		return kNullDescriptor;
	}

	return startDesc;
}

/**
 * Function : GetNewDescriptor
 */
int RHIDescriptorHeapManager::GetNewDescriptor()
{
	if (!HasDescriptors(1))
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Can't get a new descriptor in '%s' heap"), mName.c_str());
		return kNullDescriptor;
	}

	int descriptor = mFreeDescriptors.top();
	mFreeDescriptors.pop();

	return descriptor;
}

/**
 * Function : FreeDescriptors
 */
void RHIDescriptorHeapManager::FreeDescriptors(std::vector<int>& descriptors)
{
	for (auto& it = descriptors.rbegin(); it != descriptors.rend(); ++it)
		mFreeDescriptors.push(*it);

	descriptors.clear();
}

/**
 * Function : FreeDescriptors
 */
void RHIDescriptorHeapManager::FreeDescriptors(int& startDescriptor, const UINT numDescriptors)
{
	if (startDescriptor < 0)
		return;

	for (int i = 0; i < numDescriptors; ++i)
		mFreeDescriptors.push(startDescriptor + i);

	startDescriptor = kNullDescriptor;
}

/**
 * Function : FreeDescriptor
 */
void RHIDescriptorHeapManager::FreeDescriptor(int& descriptor)
{
	if (descriptor >= 0)
		mFreeDescriptors.push(descriptor);

	descriptor = kNullDescriptor;
}
