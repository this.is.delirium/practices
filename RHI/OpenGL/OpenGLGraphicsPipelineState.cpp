#include "StdafxRHI.h"
#include "OpenGLGraphicsPipelineState.h"
#include "OpenGLCommandList.h"
#include "private/OpenGLUtils.h"
#include <RHI.h>
#include <Logger.h>

/**
 * Function : Release
 */
void OpenGLGraphicsPipelineState::Release()
{
	mProgram.Release();
}

/**
 * Function : Initialize
 */
void OpenGLGraphicsPipelineState::Initialize()
{
	if (mPsoDesc.VS.ShaderBytecode)
		mProgram.AttachShader(mPsoDesc.VS, RHIShaderType::Vertex);

	if (mPsoDesc.PS.ShaderBytecode)
		mProgram.AttachShader(mPsoDesc.PS, RHIShaderType::Pixel);

	if (mPsoDesc.GS.ShaderBytecode)
		mProgram.AttachShader(mPsoDesc.GS, RHIShaderType::Geometry);

	if (mPsoDesc.HS.ShaderBytecode)
		mProgram.AttachShader(mPsoDesc.HS, RHIShaderType::Hull);

	if (mPsoDesc.DS.ShaderBytecode)
		mProgram.AttachShader(mPsoDesc.DS, RHIShaderType::Domain);

	if ((mPsoDesc.HS.ShaderBytecode || mPsoDesc.DS.ShaderBytecode) && (mPsoDesc.PrimitiveTopologyType != RHIPrimitiveTopologyType::Patch))
	{
		SLogger::Instance().AddMessage(LogMessageType::Warning, __TEXT("For Hull or Domain shader primitive topology must be as 'patch'."));
	}

	if (mPsoDesc.StreamOutput.SODeclaration)
		mProgram.SetFeedback(mPsoDesc.StreamOutput);

	mProgram.Create();

	mProgram.Bind();
	GLint stride = 0;
	for (size_t idx = 0; idx < mPsoDesc.InputLayout.NumElements; ++idx)
	{
		const RHIInputElementDesc& desc = mPsoDesc.InputLayout.InputElementDescs[idx];
		mShaderInputAttributes.push_back(ParseInputElementDesc(desc));

		if (desc.AlignedByteOffset == RHIConstants::kAppendAlignedElement)
		{
			mShaderInputAttributes[idx].mPointer = (const GLvoid*)stride;
		}

		stride += mShaderInputAttributes[idx].mStride;
	}
	for (size_t idx = 0; idx < mPsoDesc.InputLayout.NumElements; ++idx)
	{
		mShaderInputAttributes[idx].mStride = stride;
	}
	mProgram.Unbind();

	// Cache pipeline states.
	{
		mDepthMask		= GL_FALSE + ToInt(mPsoDesc.DepthStencilState.DepthWriteMask);
		mDepthFunc		= GL_NEVER + ToInt(mPsoDesc.DepthStencilState.DepthFunc) - 1;

		mCullFace		= GL_FRONT + (ToInt(mPsoDesc.RasterizerState.CullMode) - 1);
		mFrontFace		= GL_CW + mPsoDesc.RasterizerState.FrontCounterClockwise;

		mSrcBlend		= OpenGLUtils::ParseBlend(mPsoDesc.BlendState.RenderTarget[0].SrcBlend);
		mDestBlend		= OpenGLUtils::ParseBlend(mPsoDesc.BlendState.RenderTarget[0].DestBlend);
		mBlendOp		= OpenGLUtils::ParseBlendOp(mPsoDesc.BlendState.RenderTarget[0].BlendOp);
		mSrcBlendAlpha	= OpenGLUtils::ParseBlend(mPsoDesc.BlendState.RenderTarget[0].SrcBlendAlpha);
		mDestBlendAlpha	= OpenGLUtils::ParseBlend(mPsoDesc.BlendState.RenderTarget[0].DestBlendAlpha);
		mBlendOpAlpha	= OpenGLUtils::ParseBlendOp(mPsoDesc.BlendState.RenderTarget[0].BlendOpAlpha);
		mLogicOp		= OpenGLUtils::ParseLogicOp(mPsoDesc.BlendState.RenderTarget[0].LogicOp);
	}
}

/**
 * Function : ParseInputElementDesc
 */
OpenGLShaderInputAttributes OpenGLGraphicsPipelineState::ParseInputElementDesc(const RHIInputElementDesc& desc) const
{
	OpenGLShaderInputAttributes attributes;
	attributes.mAttribIndex	= glGetAttribLocation(mProgram.Id(), desc.SemanticName);
	attributes.mNormalized	= (desc.Format == RHIFormat::R8G8B8A8UInt) ? GL_TRUE : GL_FALSE; // TODO: rewrite.
	attributes.mPointer		= (const GLvoid*)desc.AlignedByteOffset;

	OpenGLUtils::ParseFormat(desc.Format, attributes.mSize, attributes.mType, attributes.mStride);

	return attributes;
}

/**
 * Function : Set
 */
void OpenGLGraphicsPipelineState::Set(RHICommandList* commandList) const
{
	if (mProgram.IsValid())
	{
		OpenGLCommandList* list = reinterpret_cast<OpenGLCommandList*> (commandList);
		list->BindProgram(mProgram.Id());

		for (const auto& attributes : mShaderInputAttributes)
		{
			if (attributes.mAttribIndex != (GLuint)-1) // To avoid opengl error. You can throw this code.
			{
				list->VertexAttrib(&attributes);
			}
		}
	}

	ProcessDepthTest();

	ProcessCullMode();

	ProcessBlending();

	ProcessFeedback(commandList);

	if (mPsoDesc.HS.ShaderBytecode || mPsoDesc.DS.ShaderBytecode)
		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::Control_Point_Patchlist_1);
}

/**
 * Function : ProcessDepthTest
 */
void OpenGLGraphicsPipelineState::ProcessDepthTest() const
{
	if (mPsoDesc.DepthStencilState.DepthEnable)
	{
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(mDepthFunc);
		glDepthMask(mDepthMask);
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
	}
}

/**
 * Function : ProcessCullMode
 */
void OpenGLGraphicsPipelineState::ProcessCullMode() const
{
	if (mPsoDesc.RasterizerState.CullMode != RHICullMode::None)
	{
		glEnable(GL_CULL_FACE);
		glCullFace(mCullFace);
		glFrontFace(mFrontFace);
	}
	else
	{
		glDisable(GL_CULL_FACE);
	}
}

/**
 * Function : ProcessBlending
 */
void OpenGLGraphicsPipelineState::ProcessBlending() const
{
	if (mPsoDesc.BlendState.RenderTarget[0].BlendEnable)
	{
		glEnable(GL_BLEND);
		glBlendFuncSeparate(mSrcBlend, mDestBlend, mSrcBlendAlpha, mDestBlendAlpha);
		glBlendEquationSeparate(mBlendOp, mBlendOpAlpha);
	}
	else
	{
		glDisable(GL_BLEND);
	}

	if (mPsoDesc.BlendState.RenderTarget[0].LogicOpEnable)
	{
		glEnable(GL_COLOR_LOGIC_OP);
		glLogicOp(mLogicOp);
	}
	else
	{
		glDisable(GL_COLOR_LOGIC_OP);
	}
}

/**
 * Function : ProcessFeedback
 */
void OpenGLGraphicsPipelineState::ProcessFeedback(RHICommandList* commandList) const
{
	if (mProgram.IsFeedback())
	{
		glEnable(GL_RASTERIZER_DISCARD);
	}
	else
	{
		glDisable(GL_RASTERIZER_DISCARD);
	}
}
