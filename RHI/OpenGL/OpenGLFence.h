#pragma once
#include "StdafxRHI.h"
#include <RHI.h>

class OpenGLFence : public RHIFence
{
public:
	inline void Release() override final { }

public:
	UINT64 GetCompletedValue() override final
	{
		return 0;
	}

	void SetEventOnCompletion(const UINT64 value, HANDLE event) override final
	{
		//
	}

protected:
};
