#pragma once
#include "StdafxRHI.h"
#include "IRenderer.h"
#include <OpenGL/private/OpenGLUtils.h>

struct OpenGLBlob : public RHIBlob
{
	void Release() override final { }

	void* GetBufferPointer() override final { return &mShader.front(); }
	SIZE_T GetBufferSize() override final { return mShader.length(); }

	bool IsNull() override final { return mShader.empty(); }

	std::string mShader;
};

class OpenGLRenderer : public IRenderer
{
public:
	OpenGLRenderer();
	~OpenGLRenderer();

	bool Initialize(const RenderSurface* renderSurface) override final;
	void Release() override final;

protected:
	void CheckRHI() override final { }

	DWORD waitForSingleObject(HANDLE handle, DWORD miliSeconds) override final { return 0; }

public:
	String ShaderFolder() const override final { return String(__TEXT("Shaders:GLSL:")); }
	RHIBlob* LoadShader(const String& shader, const RHIShaderType type, const bool fromFile = true, const String* initialDirectoryForSources = nullptr) const override final;

	void DumpRenderTargetToFile(const String& path, const ImageFormat format) override final { assert(false); }
};
