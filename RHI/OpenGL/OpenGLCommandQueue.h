#pragma once
#include <RHI.h>

class OpenGLCommandQueue : public RHICommandQueue
{
public:
	void Release() override final { }

	void ExecuteCommandLists(const UINT count, RHICommandList** commandLists) override final
	{
		for (UINT id = 0; id < count; ++id)
		{
			for (std::shared_ptr<RHICommand> command : commandLists[id]->Commands())
			{
				command->Execute();
			}
		}
	}

	void Signal(RHIFence* fence, UINT64 value) override final { }
};
