#include "StdafxRHI.h"
#include "OpenGLDevice.h"

#include <Logger.h>
#include "OpenGLBuffer.h"
#include "OpenGLCommandAllocator.h"
#include "OpenGLCommandList.h"
#include "OpenGLCommandQueue.h"
#include "OpenGLDescriptorHeap.h"
#include "OpenGLFence.h"
#include "OpenGLGraphicsPipelineState.h"
#include "OpenGLRenderer.h"
#include "OpenGLRootSignature.h"
#include "OpenGLSwapChain.h"
#include "OpenGLTexture.h"
#include "OpenGLTransformFeedback.h"
#include <RHI.h>
#include <StringUtils.h>

/**
 * Function : OpenGLDevice
 */
OpenGLDevice::OpenGLDevice()
	: mContext(NULL), mHdc(NULL)
{
	//
}

/**
 * Function : ~OpenGLDevice
 */
OpenGLDevice::~OpenGLDevice()
{
}

/**
 * Function : Release
 */
void OpenGLDevice::Release()
{
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(mContext);
}

/**
 * Function : Initialize
 */
bool OpenGLDevice::Initialize(HWND hWnd)
{
	bool isInitialized = false;

	isInitialized = InitializeContext(hWnd);

	return isInitialized;
}

/**
 * Function : InitializeContext
 */
bool OpenGLDevice::InitializeContext(HWND hWnd)
{
	bool isInitialized = false;

	PIXELFORMATDESCRIPTOR pfd = { 0 };
	pfd.nSize			= sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion		= 1;
	pfd.dwFlags			= PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType		= PFD_TYPE_RGBA;
	pfd.cColorBits		= 32;
	pfd.cDepthBits		= 24;
	pfd.cStencilBits	= 8;
	pfd.iLayerType		= PFD_MAIN_PLANE;

	mHdc = GetDC(hWnd);

	int pixelFormat = ChoosePixelFormat(mHdc, &pfd);

	if (!pixelFormat)
	{
		return isInitialized;
	}

	if (!SetPixelFormat(mHdc, pixelFormat, &pfd))
	{
		return isInitialized;
	}

	HGLRC tempContext = wglCreateContext(mHdc);
	wglMakeCurrent(mHdc, tempContext);

	GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Can't initalize GLEW"));
		return isInitialized;
	}

	mContext = tempContext;

	isInitialized = (mContext != 0);

	return isInitialized;
}

/**
 * Function : CreateCommandQueue
 */
RHICommandQueue* OpenGLDevice::CreateCommandQueue(const RHICommandQueueDesc* desc)
{
	return new OpenGLCommandQueue();
}

/**
 * Function : CreateSwapChain
 */
RHISwapChain* OpenGLDevice::CreateSwapChain(RHISwapChainDesc* desc, RHICommandQueue* commandQueue)
{
	return new OpenGLSwapChain(mHdc);
}

/**
 * Function : CreateSwapChain
 */
RHISwapChain* OpenGLDevice::CreateSwapChain(RHISwapChainDesc1* desc, RHISwapChainFullscreenDesc* fullscreenDesc, RHICommandQueue* commandQueue)
{
	return new OpenGLSwapChain(mHdc);
}

/**
 * Function : CreateCommandAllocator
 */
RHICommandAllocator* OpenGLDevice::CreateCommandAllocator(const RHICommandListType listType)
{
	return new OpenGLCommandAllocator();
}

/**
 * Function : CreateCommandList
 */
RHICommandList* OpenGLDevice::CreateCommandList(const UINT nodeMask, const RHICommandListType type, RHICommandAllocator* allocator, RHIPipelineState* pipelineState)
{
	// TODO: rewrite.
	return new OpenGLCommandList();
}

/**
 * Function : SerializeRootSignature
 */
void OpenGLDevice::SerializeRootSignature(
	const RHIRootSignatureDesc*		desc,
	const RHIRootSignatureVersion	version,
	RHIBlob**						blob,
	RHIBlob**						errorBlob)
{
	// Stub for Device::CreateRootSignature.
	//OpenGLBlob* stub = new OpenGLBlob();
	//stub->mShader = "stub";
	//
	//*blob = stub;

	assert(false);
}

/**
 * Function : SerializeVersionedRootSignature
 */
void OpenGLDevice::SerializeVersionedRootSignature(
	const RHIVersionedRootSignatureDesc*	desc,
	const RHIRootSignatureVersion			maxVersion,
	RHIBlob**								blob,
	RHIBlob**								errorBlob)
{
	assert(false);
}

RHIRootSignature* OpenGLDevice::CreateRootSignature(
	const UINT					nodeMask,
	const void*					blobWithRootSignature,
	const SIZE_T				blobLengthInBytes)
{
	return new OpenGLRootSignature();
}

/**
 * Function : CreateGraphicsPipelineState
 */
RHIPipelineState* OpenGLDevice::CreateGraphicsPipelineState(RHIGraphicsPipelineStateDesc* psoDesc)
{
	// TODO: check it.
	OpenGLGraphicsPipelineState* pso = new OpenGLGraphicsPipelineState(psoDesc);
	pso->Initialize();

	return pso;
}

/**
 * Function : CreateDescriptorHeap
 */
RHIDescriptorHeap* OpenGLDevice::CreateDescriptorHeap(const RHIDescriptorHeapDesc* desc)
{
	return new OpenGLDescriptorHeap();
}

/**
 * Function : CreateFence
 */
RHIFence* OpenGLDevice::CreateFence(const UINT64 initialValue, const RHIFenceFlags flags)
{
	return new OpenGLFence();
}

/**
 * Function : CreateTexturueFromFile
 */
RHIResource* OpenGLDevice::CreateTextureFromFile(const String& fullPath)
{
	gli::texture gliTexture = gli::load(StringUtils::Converter::WStringToString(fullPath));
	if (gliTexture.empty())
	{
		return nullptr;
	}

	GLuint textureId;
	glGenTextures(1, &textureId);

	OpenGLTexture* texture = new OpenGLTexture(textureId);
	texture->InitializedFromFile(gliTexture);

	return texture;
}
