#pragma once
#include "StdafxRHI.h"
#include "OpenGLResource.h"
#include <RHI.h>

class OpenGLBuffer : public OpenGLResource
{
public:
	OpenGLBuffer(const GLuint resourceId)
		: OpenGLResource(resourceId), mUsage(GL_DYNAMIC_DRAW) { }

	~OpenGLBuffer();

	void Release() override final;

	// TODO: Think about it.
	void Initialize(RHIBufferType target, GLsizeiptr size, const GLvoid * data, RHIBufferUsage usage);
	void InitializeSubData(const UINT subresource, const void* data, const size_t size) override final;

	FORCEINLINE void Bind() const override final;
	FORCEINLINE void Unbind() const { glBindBuffer(mTarget, kNullResource); }

	void Map(const UINT subresource, const RHIRange* readRange, void** data) override final;
	void Unmap(const UINT subresource, const RHIRange* writtenRange) override final;

protected:
	GLenum mUsage;
};
