#include "StdafxRHI.h"
#include "OpenGLRenderer.h"
#include <OpenGL/OpenGLDevice.h>
#include <OpenGL/private/OpenGLUtils.h>
#include <RenderSurface.h>
#include <ResourceManager.h>

/**
 * Function : OpenGLRenderer
 */
OpenGLRenderer::OpenGLRenderer()
	: IRenderer() { }

/**
 * Function : ~CoreGLRenderer
 */
OpenGLRenderer::~OpenGLRenderer()
{

}

/**
 * Function : Release
 */
void OpenGLRenderer::Release()
{
	IRenderer::Release();
}

/**
 * Function : Initialize
 */
bool OpenGLRenderer::Initialize(const RenderSurface* renderSurface)
{
	mDevice = new OpenGLDevice();

	bool isInitialized = true;
	isInitialized &= mDevice->Initialize(renderSurface->WindowHandle());

	assert(isInitialized);

	isInitialized &= IRenderer::Initialize(renderSurface);

	return isInitialized;
}

/**
 * Function : LoadShader
 */
RHIBlob* OpenGLRenderer::LoadShader(const String& shader, const RHIShaderType type, const bool fromFile, const String* initialDirectoryForSources) const
{
	if (!fromFile)
		assert(false);

	OpenGLBlob* blob	= new OpenGLBlob();
	blob->mShader		= SResourceManager::Instance().LoadFile(SResourceManager::Instance().Find(ShaderFolder() + shader));

	return blob;
}
