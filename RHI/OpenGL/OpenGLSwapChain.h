#pragma once
#include <RHI.h>
#include "OpenGLBuffer.h"

class OpenGLSwapChain : public RHISwapChain
{
public:
	OpenGLSwapChain(const HDC& hdc)
		: RHISwapChain(), mHdc(hdc) { }

	FORCEINLINE void Release() override final { }

	FORCEINLINE void Present(const UINT syncInterval, const UINT flags)
	{
		::SwapBuffers(mHdc);
	}

	FORCEINLINE UINT GetCurrentBackBufferIndex() override final { return 0; }

	FORCEINLINE bool GetDesc(RHISwapChainDesc* desc) const override final { return false; }

	FORCEINLINE void GetBuffer(const UINT buffer, RHIResource*& surface)
	{
		// Stub.
		surface = new OpenGLBuffer(0);
	}

	void SetMaximumFrameLatency(const UINT latency) override final { }
	HANDLE GetFrameLatencyWaitableObject() override final { return NULL; }

protected:
	HDC mHdc;
};
