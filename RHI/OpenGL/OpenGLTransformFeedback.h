#pragma once
#include "StdafxRHI.h"
#include "OpenGLResource.h"

class OpenGLTransformFeedback : public OpenGLResource
{
public:
	OpenGLTransformFeedback(const GLuint resource)
		: OpenGLResource(resource), mAttachedVertexBuffer(0)
	{
		mTarget = GL_TRANSFORM_FEEDBACK;
	}

	void Release() override final;

	void Bind() const override final { }
	void Unbind() const override final { }

	void Map(const UINT subresource, const RHIRange* readRange, void** data) override final { }
	void Unmap(const UINT subresource, const RHIRange* writtenRange) override final { }

	void InitializeSubData(const UINT subresource, const void* data, const size_t size) override final { }

	void AttachVertexBuffer(const GLuint resourceId) { mAttachedVertexBuffer = resourceId; }
	GLuint AttachedBuffer() const { return mAttachedVertexBuffer; }
protected:
	GLuint mAttachedVertexBuffer;
};
