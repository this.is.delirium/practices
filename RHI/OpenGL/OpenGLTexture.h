#pragma once
#include "StdafxRHI.h"
#include "OpenGLResource.h"

class OpenGLDevice;

class OpenGLTexture : public OpenGLResource
{
public:
	OpenGLTexture(const GLuint resourceId);

	~OpenGLTexture() final;

	void Initialize(const RHIResourceDesc* desc);
	void Release() override final;

	FORCEINLINE void Bind() const override final;
	FORCEINLINE void Unbind() const override final { glBindTexture(mTarget, kNullResource); }

	// TODO: rewrire.
	FORCEINLINE void BindToTextureSampler(const GLuint sampler) { mTextureSampler = sampler; }
	FORCEINLINE GLuint TextureUnit() const { return mTextureSampler; }

	void Map(const UINT subresource, const RHIRange* readRange, void** data) override final { }
	void Unmap(const UINT subresource, const RHIRange* writtenRange) override final { }

	// TODO: rewrite.
	FORCEINLINE void SetLocation(const GLint location) { mLocation = location; }

	void InitializeSubData(const UINT subresource, const void* data, const size_t size) override final { }

	void InitializeSubData(const GLint offsetX, const GLint offsetY, const GLsizei width, const GLsizei height, const GLvoid* data);

protected:
	friend class OpenGLDevice;
	void InitializedFromFile(gli::texture& texture);

protected:
	GLuint	mTextureSampler;
	GLint	mLocation;

	GLsizei	mWidth;
	GLsizei mHeight;
	GLint	mInternalFormat;
	GLenum	mPixelFormat;
	GLenum	mPixelType;
};
