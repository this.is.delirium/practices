#pragma once
#include "StdafxRHI.h"
#include <RHI.h>

class OpenGLDescriptorHeap : public RHIDescriptorHeap
{
public:
	inline void Release() override final { }

	inline void SetName(CString name) override final { }

	inline RHICPUDescriptorHandle GetCPUDescriptorHandleForHeapStart() override final
	{
		return RHICPUDescriptorHandle();
	}

	inline RHIGPUDescriptorHandle GetGPUDescriptorHandleForHeapStart() override final
	{
		return RHIGPUDescriptorHandle();
	}
};
