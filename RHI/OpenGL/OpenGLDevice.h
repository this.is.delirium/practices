#pragma once
#include "StdafxRHI.h"
#include "RHI.h"

class OpenGLDevice : public RHIRenderingDevice
{
public:
	OpenGLDevice();
	virtual ~OpenGLDevice();

	virtual void Release();

	bool Initialize(HWND hWnd) override final;

	bool CheckFeatureSupport(
		RHIFeature	feature,
		void*		featureSupportData,
		const UINT	featureSupportDataSize) override final { return false; }

	RHICommandQueue* CreateCommandQueue(const RHICommandQueueDesc* desc) override final;

	RHISwapChain* CreateSwapChain(RHISwapChainDesc* desc, RHICommandQueue* commandQueue) override final;
	RHISwapChain* CreateSwapChain(RHISwapChainDesc1* desc, RHISwapChainFullscreenDesc* fullscreenDesc, RHICommandQueue* commandQueue) override final;

	RHICommandAllocator* CreateCommandAllocator(const RHICommandListType listType) override final;

	RHICommandList* CreateCommandList(
		const UINT nodeMask,
		const RHICommandListType type,
		RHICommandAllocator* allocator,
		RHIPipelineState* pipelineState) override final;

	void SerializeRootSignature(
		const RHIRootSignatureDesc*		desc,
		const RHIRootSignatureVersion	version,
		RHIBlob**						blob,
		RHIBlob**						errorBlob) override final;

	void SerializeVersionedRootSignature(
		const RHIVersionedRootSignatureDesc*	desc,
		const RHIRootSignatureVersion			maxVersion,
		RHIBlob**								blob,
		RHIBlob**								errorBlob) override final;

	RHIRootSignature* CreateRootSignature(
		const UINT					nodeMask,
		const void*					blobWithRootSignature,
		const SIZE_T				blobLengthInBytes
	) override final;

	RHIPipelineState* CreateGraphicsPipelineState(RHIGraphicsPipelineStateDesc* psoDesc) override final;
	RHIPipelineState* CreateComputePipelineState(RHIComputePipelineStateDesc* psoDesc) override final { assert(false); return nullptr; }

	RHIDescriptorHeap* CreateDescriptorHeap(const RHIDescriptorHeapDesc* desc) override final;

	UINT GetDescriptorHandleIncrementSize(RHIDescriptorHeapType type) override final
	{
		return 0;
	}

	RHIResource* CreateCommittedResource(
		const RHIHeapProperties*	heapProperties,
		const RHIHeapFlags			heapFlags,
		const RHIResourceDesc*		desc,
		const RHIResourceStates		initialResourceStates,
		const RHIClearValue*		optimizedClearValue) override final { return nullptr; }

	void CreateConstantBufferView(const RHIConstantBufferViewDesc* desc, const RHICPUDescriptorHandle destDescriptor) override final { }

	void CreateRenderTargetView(
		RHIResource*					resource,
		const RHIRenderTargetViewDesc*	desc,
		RHICPUDescriptorHandle			destDescriptor) override final { }

	void CreateDepthStencilView(
		RHIResource*					resource,
		const RHIDepthStencilViewDesc*	desc,
		RHICPUDescriptorHandle			destDescriptor) override final { }

	void CreateShaderResourceView(
		RHIResource*						resource,
		const RHIShaderResourceViewDesc*	desc,
		RHICPUDescriptorHandle				destDescriptor) override final { }

	void CreateUnorderedAccessView(
		RHIResource*				resource,
		RHIResource*				counterResource,
		RHIUnorderedAccessViewDesc*	desc,
		RHICPUDescriptorHandle		destDescriptor) override final { }

	RHIFence* CreateFence(const UINT64 initialValue, const RHIFenceFlags flags) override final;

	RHIResource* CreateTextureFromFile(const String& fullPath) override final;
	void LoadTextureFromFile(const String& fullPath, RHIResourceDesc* desc, RHISubresourceData* subresData) override final { assert(false); }

	void CopyDescriptorsSimple(
		const UINT				numDescriptors,
		RHICPUDescriptorHandle	destDescriptorRangeStart,
		RHICPUDescriptorHandle	srcDescriptorRangeStart,
		RHIDescriptorHeapType	descriptorHeapsType) override final { }

	void GetCopyableFootprints(
		const RHIResourceDesc*			desc,
		const UINT						firstSubresource,
		const UINT						numSubresource,
		const UINT64					baseOffset,
		RHIPlacedSubresourceFootprint*	layouts,
		UINT*							numRows,
		UINT64*							rowSizeInBytes,
		UINT64*							totalBytes) override final { }

public:
	// Uses UpdateSubresources and allocates a new upload buffer.
	void UploadResource(RHIResource* destResource, const UINT firstSubresource, const UINT numSubresources, RHISubresourceData* subresources) override final { assert(false); }

public:
	UINT64 GetRequiredIntermediateSize(RHIResource* destinationResource, const UINT firstSubresource, const UINT numSubresources) override final { assert(false); return 0; }

	UINT64 UpdateSubresources(
		RHICommandList*		cmdList,
		RHIResource*		destResource,
		RHIResource*		intermediate,
		const UINT64		intermediateOffset,
		const UINT			firstSubresource,
		const UINT			numSubresources,
		RHISubresourceData*	srcData) override final { assert(false); return 0; }

	UINT64 UpdateSubresources(
		const UINT			maxSubresource,
		RHICommandList*		cmdList,
		RHIResource*		destResource,
		RHIResource*		intermediate,
		const UINT64		intermediateOffset,
		const UINT			firstSubresource,
		const UINT			numSubresources,
		RHISubresourceData*	srcData,
		const bool			useFullBuffer) override final { assert(false); return 0; }

	UINT64 UpdateSubresources(
		RHICommandList*							cmdList,
		RHIResource*							destResource,
		RHIResource*							intermediate,
		const UINT								firstSubresource,
		const UINT								numSubresources,
		const UINT64							requiredSize,
		const RHIPlacedSubresourceFootprint*	layouts,
		const UINT*								numRows,
		const UINT64*							rowSizeInBytes,
		const RHISubresourceData*				srcData) override final { assert(false); return 0; }

protected:
	bool InitializeContext(HWND hWnd);

protected:
	HGLRC	mContext; // OpenGL Rendering Context;
	HDC		mHdc;
};
