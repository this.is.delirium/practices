#pragma once
#include "StdafxRHI.h"
#include <RHI.h>
#include "private/OpenGLShaderProgram.h"

struct OpenGLShaderInputAttributes
{
	GLuint			mAttribIndex;
	GLint			mSize;
	GLenum			mType;
	GLboolean		mNormalized;
	GLsizei			mStride;
	const GLvoid*	mPointer;
};

class OpenGLGraphicsPipelineState : public RHIPipelineState
{
public:
	OpenGLGraphicsPipelineState(const RHIGraphicsPipelineStateDesc* psoDesc)
		: mPsoDesc(*psoDesc) { }

	~OpenGLGraphicsPipelineState() { }

	void Release() override final;

	void Set(RHICommandList* commandList) const /*override final*/;

	FORCEINLINE const OpenGLShaderProgram& Program() const { return mProgram; }

protected:
	void Initialize() /*override final*/;

	OpenGLShaderInputAttributes ParseInputElementDesc(const RHIInputElementDesc& desc) const;

protected:
	void ProcessDepthTest() const;
	void ProcessCullMode() const;
	void ProcessBlending() const;
	void ProcessFeedback(RHICommandList* commandList) const;

protected:
	OpenGLShaderProgram							mProgram;
	std::vector<OpenGLShaderInputAttributes>	mShaderInputAttributes;

protected:
	friend class OpenGLDevice;

protected: //! cached values;
	RHIGraphicsPipelineStateDesc	mPsoDesc;

	GLenum							mDepthMask;
	GLenum							mDepthFunc;

	GLenum							mCullFace;
	GLenum							mFrontFace;

	GLenum							mSrcBlend;
	GLenum							mDestBlend;
	GLenum							mBlendOp;
	GLenum							mSrcBlendAlpha;
	GLenum							mDestBlendAlpha;
	GLenum							mBlendOpAlpha;
	GLenum							mLogicOp;
};
