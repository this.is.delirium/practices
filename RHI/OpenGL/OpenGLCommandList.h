#pragma once
#include "StdafxRHI.h"
#include <RHI.h>

struct OpenGLShaderInputAttributes;
class OpenGLCommandList : public RHICommandList
{
public:
	void Release() { Close(); ClearCommands(); }

	void ClearRenderTargetView(
		RHICPUDescriptorHandle	renderTargetView,
		const FLOAT*			colorRGBA,
		const UINT				numRects,
		const RHIRect*			rects) override final;

	void ClearDepthStencilView(
		RHICPUDescriptorHandle	depthStencilView,
		RHIClearFlags			clearFlags,
		const FLOAT				depth,
		const UINT8				stencil,
		const UINT				numRects,
		const RHIRect*			rects) override final { assert(false); }

	void ClearUnorderedAccessViewUint(
		const RHIGPUDescriptorHandle	viewGPUHandleInCurrentHeap,
		const RHICPUDescriptorHandle	viewCPUHandle,
		RHIResource*					resource,
		const UINT						values[4],
		const UINT						numRects,
		const RHIRect*					rects) override final { assert(false); }

	void RSSetViewports(const UINT numViewports, const RHIViewport* viewports) override final;
	void RSSetScissorRects(const UINT numRects, const RHIRect* rects) override final;

	void SetPipelineState(RHIPipelineState* pso) override final;

	void SetGraphicsRootSignature(RHIRootSignature* rootSignature) override final { assert(false); }
	void SetGraphicsRootConstantBufferView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation) override final { assert(false); }
	void SetGraphicsRootDescriptorTable(const UINT rootParameterIndex, const RHIGPUDescriptorHandle baseDescriptor) override final { assert(false); }

	void SetComputeRootSignature(RHIRootSignature* rootSignature) override final { assert(false); }
	void SetComputeRootDescriptorTable(const UINT rootParameterIndex, const RHIGPUDescriptorHandle baseDescirptor) override final { assert(false); }
	void SetComputeRootConstantBufferView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation) override final { assert(false); }
	void SetComputeRootUnorderedAccessView(const UINT rootParameterIndex, RHIGPUVirtualAddress bufferLocation) override final { assert(false); }

	void SetDescriptorHeaps(const UINT numDescriptorHeaps, RHIDescriptorHeap** heaps) override final { assert(false); }

	void IASetIndexBuffer(const RHIIndexBufferView* view) override final { assert(false); }
	void IASetPrimitiveTopology(const RHIPrimitiveTopology primitiveTopology) override final;
	void IASetVertexBuffers(const UINT startSlot, const UINT numViews, const RHIVertexBufferView* views) override final;

	void ResourceBarrier(const UINT numBarriers, const RHIResourceBarrier* barriers) override final { assert(false); }

	void CopyResource(RHIResource* dstResource, RHIResource* srcResource) override final { assert(false); }

	void CopyTextureRegion(
		const RHITextureCopyLocation*	dst,
		const UINT						dstX,
		const UINT						dstY,
		const UINT						dstZ,
		const RHITextureCopyLocation*	src,
		const RHIBox*					srcBox) override final { assert(false); }

	void CopyBufferRegion(
		RHIResource*	dstBuffer,
		UINT64			dstOffset,
		RHIResource*	srcBuffer,
		UINT64			srcOffset,
		UINT64			numBytes) override final { assert(false); }

	void OMSetBlendFactor(const FLOAT blendFactor[4]) override final { assert(false); };
	void OMSetRenderTargets(
		const UINT						numRenderTargetDescriptors,
		const RHICPUDescriptorHandle*	renderTargetDescriptors,
		const BOOL						RTsSingleHandleToDescriptorRange,
		const RHICPUDescriptorHandle*	depthStencilDescriptor) override final { assert(false); }

	void DrawInstanced(
		const uint32_t vertexCountPerInstance,
		const uint32_t instanceCount,
		const uint32_t startVertexLocation,
		const uint32_t startInstanceLocation) override final;

	void DrawIndexedInstanced(
		const uint32_t indexCountPerInstance,
		const uint32_t instanceCount,
		const uint32_t startIndexLocation,
		const int32_t baseVertexLocation,
		const uint32_t startInstanceLocation) override final;

	void Dispatch(const UINT threadGroupCountX, const UINT threadGroupCountY, const UINT threadGroupCountZ) override final { assert(false); }

	void ExecuteBundle(RHICommandList* commandList) override final { assert(false); }

	void SOSetTargets(const UINT startSlot, const UINT numViews, const RHIStreamOutputBufferView* views) override final { assert(false); }
	void DrawStreamOutput(const RHIResource* streamOutput) override final;

	void Close() override final { assert(false); }

	void Reset(RHICommandAllocator* commandAllocator, RHIPipelineState* pso) override final;

	void BindProgram(const GLuint program);
	void VertexAttrib(const OpenGLShaderInputAttributes* attributes);

protected:
	GLuint					mProgramId;
	RHIPrimitiveTopology	mPrimitiveTopology;
};
