#include "StdafxRHI.h"
#include "OpenGLBuffer.h"
#include "OpenGL/private/OpenGLUtils.h"

/**
 * Function : ~OpenGLBuffer
 */
OpenGLBuffer::~OpenGLBuffer()
{
	assert(mResourceId == kNullResource);
}

/**
 * Function : Release
 */
void OpenGLBuffer::Release()
{
	if (mResourceId == kNullResource)
	{
		return;
	}

	glDeleteBuffers(1, &mResourceId);
	mResourceId = kNullResource;
}

/**
 * Function : Initialize
 */
void OpenGLBuffer::Initialize(RHIBufferType target, GLsizeiptr size, const GLvoid * data, RHIBufferUsage usage)
{
	mTarget = OpenGLUtils::ParseBufferType(target);
	mUsage = OpenGLUtils::ParseBufferUsage(usage);

	Bind();

	glBufferData(mTarget, size, data, mUsage);

	Unbind();
}

/**
 * Function : InitializeSubData
 */
void OpenGLBuffer::InitializeSubData(const UINT subresource, const void* data, const size_t size)
{
	Bind();

	glBufferSubData(mTarget, subresource, size, data);

	Unbind();
}

/**
 * Function : Bind
 */
void OpenGLBuffer::Bind() const
{
	glBindBuffer(mTarget, mResourceId);
}

/**
 * Function : Map
 */
void OpenGLBuffer::Map(const UINT subresource, const RHIRange* readRange, void** data)
{

}

/**
 * Function : Unmap
 */
void OpenGLBuffer::Unmap(const UINT subresource, const RHIRange* writtenRange)
{

}
