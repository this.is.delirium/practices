#pragma once
#include "StdafxRHI.h"
#include <RHI.h>

class OpenGLResource : public RHIResource
{
public:
	OpenGLResource(const GLuint resourceId)
		: RHIResource(), mResourceId(resourceId) { }

	virtual ~OpenGLResource()
	{
		assert(mResourceId == kNullResource);
	}

	FORCEINLINE void SetName(CConstString name) override final { }

	FORCEINLINE RHIGPUVirtualAddress GetGPUVirtualAddress() const override final { return ResourceId(); }

	FORCEINLINE RHIResourceDesc GetDesc() const override final { return RHIResourceDesc(); }

	FORCEINLINE GLuint ResourceId() const { return mResourceId; }
	FORCEINLINE GLenum Target() const { return mTarget; }

	virtual void Bind() const = 0;
	virtual void Unbind() const = 0;

	void WriteToSubresource(UINT dstSubresource, const RHIBox* dstBox, const void* srcData, const UINT srcRowPitch, const UINT srcDepthPitch) override final { }

protected:
	static const GLuint kNullResource = 0;

	GLuint mResourceId;
	GLenum mTarget;
};
