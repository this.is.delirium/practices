#include "StdafxRHI.h"
#include "OpenGLCommandList.h"
#include "OpenGLBuffer.h"
#include "OpenGLCommands.h"
#include "OpenGLTransformFeedback.h"
#include "OpenGLTexture.h"

/**
 * Function : ClearColor
 */
void OpenGLCommandList::ClearRenderTargetView(
	RHICPUDescriptorHandle	renderTargetView,
	const FLOAT*			colorRGBA,
	const UINT				numRects,
	const RHIRect*			rects)
{
	mCommands.push_back(std::make_shared<OpenGLCommandClearColor>(colorRGBA));
}

/**
 * Function : SetViewport
 */
void OpenGLCommandList::RSSetViewports(const UINT numViewports, const RHIViewport* viewports)
{
	mCommands.push_back(std::make_shared<OpenGLCommandViewport>(&viewports[0]));
}

/**
 * Function : SetScissor
 */
void OpenGLCommandList::RSSetScissorRects(const UINT numRects, const RHIRect* rects)
{
	mCommands.push_back(std::make_shared<OpenGLCommandScissor>(&rects[0]));
}

/**
 * Function : IASetPrimitiveTopology
 */
void OpenGLCommandList::IASetPrimitiveTopology(const RHIPrimitiveTopology primitiveTopology)
{
	mPrimitiveTopology = primitiveTopology;
}

/**
 * Function : IASetVertexBuffer
 */
void OpenGLCommandList::IASetVertexBuffers(const UINT startSlot, const UINT numView, const RHIVertexBufferView* views)
{
	mCommands.push_back(std::make_shared<OpenGLCommandSetVertexBuffers>(startSlot, numView, views));
}

/**
 * Function : DrawInstanced
 */
void OpenGLCommandList::DrawInstanced(
	const uint32_t vertexCountPerInstance,
	const uint32_t instanceCount,
	const uint32_t startVertexLocation,
	const uint32_t startInstanceLocation)
{
	for (auto& delayCommand : mDelayedCommands)
	{
		mCommands.push_back(delayCommand);
	}
	//mDelayedCommands.clear();

	mCommands.push_back(
		std::make_shared<OpenGLCommandDrawInstanced>(mPrimitiveTopology, vertexCountPerInstance, instanceCount, startVertexLocation, startInstanceLocation)
	);
}

/**
 * Function : DrawInstanced
 */
void OpenGLCommandList::DrawIndexedInstanced(
	const uint32_t indexCountPerInstance,
	const uint32_t instanceCount,
	const uint32_t startIndexLocation,
	const int32_t baseVertexLocation,
	const uint32_t startInstanceLocation)
{
	for (auto& delayCommand : mDelayedCommands)
	{
		mCommands.push_back(delayCommand);
	}
	//mDelayedCommands.clear();

	mCommands.push_back(
		std::make_shared<OpenGLCommandDrawIndexedInstanced>(mPrimitiveTopology, indexCountPerInstance, true, nullptr)
	);
}

/**
 * Function : DrawStreamOutput
 */
void OpenGLCommandList::DrawStreamOutput(const RHIResource* streamOutput)
{
	for (auto& delayCommand : mDelayedCommands)
	{
		mCommands.push_back(delayCommand);
	}

	const OpenGLResource* feedback = reinterpret_cast<const OpenGLTransformFeedback*>(streamOutput);
	mCommands.push_back(std::make_shared<OpenGLCommandDrawStreamOutput>(feedback, mPrimitiveTopology));
}

/**
 * Function : BindProgram
 */
void OpenGLCommandList::BindProgram(const GLuint program)
{
	mProgramId = program;
	mCommands.push_back(std::make_shared<OpenGLCommandBindProgram>(program));

}

/**
 * Function : VertexAttrib
 */
void OpenGLCommandList::VertexAttrib(const OpenGLShaderInputAttributes* attributes)
{
	mDelayedCommands.push_back(std::make_shared<OpenGLCommandVertexAttrib>(attributes));
}

/**
 * Function : Reset
 */
void OpenGLCommandList::Reset(RHICommandAllocator* commandAllocator, RHIPipelineState* pso)
{
	ClearCommands();
	assert(false);
	//pso->Set(this);
}

/**
 * Function : Set
 */
void OpenGLCommandList::SetPipelineState(RHIPipelineState* pso)
{
	assert(false);
	//pso->Set(this);
}
