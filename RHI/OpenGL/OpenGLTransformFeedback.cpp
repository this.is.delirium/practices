#include "StdafxRHI.h"
#include "OpenGLTransformFeedback.h"

/**
 * Function : Release
 */
void OpenGLTransformFeedback::Release()
{
	glDeleteTransformFeedbacks(1, &mResourceId);
	mResourceId = kNullResource;
}
