#include "StdafxRHI.h"
#include "OpenGLTexture.h"
#include <OpenGL/private/OpenGLUtils.h>

/**
 * Function : OpenGLTexture
 */
OpenGLTexture::OpenGLTexture(const GLuint resourceId)
	: OpenGLResource(resourceId), mTextureSampler(0), mLocation(-1),
	mWidth(0), mHeight(0), mInternalFormat(GL_NONE), mPixelFormat(GL_NONE), mPixelType(GL_NONE) { }

/**
 * Function : ~OpenGLTexture
 */
OpenGLTexture::~OpenGLTexture()
{
	assert(mResourceId == kNullResource);
}

/**
 * Function : Release
 */
void OpenGLTexture::Release()
{
	glDeleteTextures(1, &mResourceId);
	mResourceId = kNullResource;
}

/**
 * Function : Iniitialize
 */
void OpenGLTexture::Initialize(const RHIResourceDesc* desc)
{
	bool hasMipMaps = desc->MipLevels > 1;

	const GLint	border	= 0;
	const GLint	level	= 0;

	mWidth	= desc->Height;
	mHeight	= desc->Height;
	OpenGLUtils::ParseFormatForTexture(desc->Format, mInternalFormat, mPixelFormat, mPixelType);

	switch (desc->Dimension)
	{
		case RHIResourceDimension::Texture1D:
		{
			mTarget = GL_TEXTURE_1D;
			assert(false);
			break;
		}

		case RHIResourceDimension::Texture2D:
		{
			mTarget = GL_TEXTURE_2D;

			Bind();

			glTexImage2D(GL_PROXY_TEXTURE_2D, level, mInternalFormat, (GLsizei)desc->Width, (GLsizei)desc->Height, border, mPixelFormat, mPixelType, nullptr);

			GLint testWidth, testHeight;
			glGetTexLevelParameteriv(GL_PROXY_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &testWidth);
			glGetTexLevelParameteriv(GL_PROXY_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &testHeight);

			assert(testWidth != 0 && testHeight != 0);

			glTexImage2D(mTarget, level, mInternalFormat, desc->Width, desc->Height, border, mPixelFormat, mPixelType, nullptr);

			break;
		}

		case RHIResourceDimension::Texture3D:
		{
			assert(false);
			mTarget = GL_TEXTURE_3D;
			break;
		}
	}

	Unbind();
}

/**
 * Function : InitializedFromFile
 */
void OpenGLTexture::InitializedFromFile(gli::texture& texture)
{

	gli::gl GL(gli::gl::profile::PROFILE_GL33);
	gli::gl::format const Format = GL.translate(texture.format(), texture.swizzles());

	glm::tvec3<GLsizei> const Extent(texture.extent());

	mWidth			= Extent.x;
	mHeight			= Extent.y;
	mTarget			= GL.translate (texture.target());
	mInternalFormat	= Format.Internal;
	mPixelFormat	= Format.External;
	mPixelType		= Format.Type;

	Bind();

	glTexParameteri(mTarget, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(mTarget, GL_TEXTURE_MAX_LEVEL, static_cast<GLint>(texture.levels() - 1));
	glTexParameteri(mTarget, GL_TEXTURE_SWIZZLE_R, Format.Swizzles[0]);
	glTexParameteri(mTarget, GL_TEXTURE_SWIZZLE_G, Format.Swizzles[1]);
	glTexParameteri(mTarget, GL_TEXTURE_SWIZZLE_B, Format.Swizzles[2]);
	glTexParameteri(mTarget, GL_TEXTURE_SWIZZLE_A, Format.Swizzles[3]);
	GLsizei const FaceTotal = static_cast<GLsizei>(texture.layers() * texture.faces());

	switch (texture.target())
	{
		case gli::TARGET_2D:
		{
			glTexStorage2D(mTarget, static_cast<GLint>(texture.levels()), mInternalFormat,
				mWidth, /*texture.target() == gli::TARGET_2D ?*/ mHeight /*: FaceTotal*/);
			break;
		}

		default:
		{
			break;
		}
	}

	for (std::size_t Layer = 0; Layer < texture.layers(); ++Layer)
	for (std::size_t Face = 0; Face < texture.faces(); ++Face)
	for (std::size_t Level = 0; Level < texture.levels(); ++Level)
	{
		GLsizei const LayerGL = static_cast<GLsizei>(Layer);
		glm::tvec3<GLsizei> Extent(texture.extent(Level));
		GLenum Target = gli::is_target_cube(texture.target())
			? static_cast<GLenum>(GL_TEXTURE_CUBE_MAP_POSITIVE_X + Face)
			: mTarget;

		switch (texture.target())
		{
			case gli::TARGET_2D:
			{
				if (gli::is_compressed(texture.format()))
				{
					glCompressedTexSubImage2D(
						mTarget, static_cast<GLint>(Level),
						0, 0,
						mWidth,
						/*texture.target() == gli::TARGET_1D_ARRAY ? LayerGL :*/ mHeight,
						mInternalFormat, static_cast<GLsizei>(texture.size(Level)),
						texture.data(Layer, Face, Level));
				}
				else
				{
					glTexSubImage2D(
						Target, static_cast<GLint>(Level),
						0, 0,
						Extent.x,
						/*texture.target() == gli::TARGET_1D_ARRAY ? LayerGL :*/ Extent.y,
						mPixelFormat, mPixelType,
						texture.data(Layer, Face, Level));
				}
				break;
			}

			default:
			{
				assert(0);
				break;
			}
		}

	}

	Unbind();
	GLenum error = glGetError(); // To avoid opengl errors.
}

/**
 * Function : InitializeSubData
 */
void OpenGLTexture::InitializeSubData(const GLint offsetX, const GLint offsetY, const GLsizei width, const GLsizei height, const GLvoid* data)
{
	Bind();

	glTexSubImage2D(mTarget, 0, offsetX, offsetY, width, height, mPixelFormat, mPixelType, data);

	Unbind();
}

/**
 * Function : Bind
 */
void OpenGLTexture::Bind() const
{
	glActiveTexture(GL_TEXTURE0 + mTextureSampler);
	glBindTexture(mTarget, mResourceId);

	if (mLocation != -1) // To avoid opengl errors.
	{
		glUniform1i(mLocation, mTextureSampler);
	}
}
