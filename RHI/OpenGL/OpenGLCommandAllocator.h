#pragma once
#include "StdafxRHI.h"
#include <RHI.h>

class OpenGLCommandAllocator : public RHICommandAllocator
{
public:
	void Release() override final { }
	void Reset() override final { }
};
