#include "StdafxRHI.h"
#include "OpenGLShaderProgram.h"
#include "OpenGLUtils.h"
#include <Logger.h>

/**
 * Function : OpenGLShaderProgram
 */
OpenGLShaderProgram::OpenGLShaderProgram()
	: mShaderProgram(kNullObject)
{
	//
}

/**
 * Function : ~OpenGLShaderProgram
 */
OpenGLShaderProgram::~OpenGLShaderProgram()
{
	bool result = mShaderProgram == kNullObject;
	for (const auto& shader : mShaders)
	{
		result &= shader.second == kNullObject;
	}
	// OpenGL resource wasn't release! Possible GPU memory leakage...
	assert(result);
}

/**
 * Function : Release
 */
void OpenGLShaderProgram::Release()
{
	for (const auto& shader : mShaders)
	{
		glDetachShader(mShaderProgram, shader.second);
		glDeleteShader(shader.second);
	}
	mShaders.clear();

	glDeleteProgram(mShaderProgram);
	mShaderProgram = kNullObject;
}

/**
 * Function : AttachShader
 */
void OpenGLShaderProgram::AttachShader(const RHIShaderBytecode& shaderBytecode, const RHIShaderType type)
{
	GLuint shader = loadShader(OpenGLUtils::ParseShaderType(type), shaderBytecode);
	if (shader != kNullObject)
	{
		mShaders[type] = shader;
	}
	else
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Error: cann't create a shader."));
	}
}

/**
 * Function : SetFeedback
 */
void OpenGLShaderProgram::SetFeedback(const RHIStreamOutputDesc& desc)
{
	for (UINT i = 0; i < desc.NumElements; ++i)
	{
		mFeedbackVaryings.push_back(desc.SODeclaration[i].SemanticName);
	}
}

/**
 * Function : Create
 */
bool OpenGLShaderProgram::Create()
{
	mShaderProgram = glCreateProgram();

	if (mShaderProgram == 0)
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Cann't create shader program."));
		return false;
	}

	for (const auto& shader : mShaders)
	{
		glAttachShader(mShaderProgram, shader.second);
	}

	if (mFeedbackVaryings.size() > 0)
	{
		glTransformFeedbackVaryings(mShaderProgram, mFeedbackVaryings.size(), mFeedbackVaryings.data(), GL_INTERLEAVED_ATTRIBS);
	}

	glLinkProgram(mShaderProgram);

	GLint linked;
	glGetProgramiv(mShaderProgram, GL_LINK_STATUS, &linked);

	if (!linked)
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Linking program."));
		return false;
	}

	// Check on correctness the shader
	GLint status;
	glValidateProgram(mShaderProgram);
	glGetProgramiv(mShaderProgram, GL_VALIDATE_STATUS, &status);

	if (status != GL_TRUE)
	{
		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Main shader program is not validate."));
		return false;
	}

	return true;
}

/**
 * Function : loadShader
 */
GLuint OpenGLShaderProgram::loadShader(const GLenum type, const RHIShaderBytecode& shaderBytecode)
{
	GLuint			shader = glCreateShader(type);
	const GLint		length = shaderBytecode.Length;
	const GLchar*	source = (const GLchar*)shaderBytecode.ShaderBytecode;

	// Replaces & compile the source code in a shader object.
	glShaderSource(shader, 1, &source, &length);
	glCompileShader(shader);

	// Check
	GLint compileStatus;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);

	if (compileStatus != GL_TRUE)
	{
		GLint size;
		GLchar infoLog[1024];
		glGetShaderInfoLog(shader, 1024, &size, infoLog);

		SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("Shader isn't compiled %s"), infoLog);
		return kNullObject;
	}

	return shader;
}
