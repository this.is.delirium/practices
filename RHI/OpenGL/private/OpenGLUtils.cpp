#include "StdafxRHI.h"
#include "OpenGLUtils.h"
#include <Logger.h>

/**
 * Function : ParseShaderType
 */
GLenum OpenGLUtils::ParseShaderType(const RHIShaderType shader)
{
	switch (shader)
	{
		case RHIShaderType::Vertex:
			return GL_VERTEX_SHADER;

		case RHIShaderType::Pixel:
			return GL_FRAGMENT_SHADER;

		case RHIShaderType::Geometry:
			return GL_GEOMETRY_SHADER;

		case RHIShaderType::Hull:
			return GL_TESS_CONTROL_SHADER;

		case RHIShaderType::Domain:
			return GL_TESS_EVALUATION_SHADER;

		default:
			return GL_ZERO;
	}
}

/**
 * Function : ParseFormat
 */
void OpenGLUtils::ParseFormat(const RHIFormat format, GLint& size, GLenum& type, GLint& sizeInBytes)
{
	switch (format)
	{
		case RHIFormat::R32G32B32A32Float:
		{
			size		= 4;
			type		= GL_FLOAT;
			sizeInBytes	= sizeof(float) * size;
			break;
		}

		case RHIFormat::R32G32B32Float:
		{
			size		= 3;
			type		= GL_FLOAT;
			sizeInBytes	= sizeof(float) * size;
			break;
		}

		case RHIFormat::R32G32Float:
		{
			size		= 2;
			type		= GL_FLOAT;
			sizeInBytes	= sizeof(float) * size;
			break;
		}

		case RHIFormat::R8G8B8A8UInt:
		{
			size		= 4;
			type		= GL_UNSIGNED_BYTE;
			sizeInBytes	= sizeof(unsigned char) * size;
			break;
		}

		case RHIFormat::R32Float:
		{
			size		= 1;
			type		= GL_FLOAT;
			sizeInBytes	= sizeof(float) * size;
			break;
		}

		case RHIFormat::R32Sint:
		{
			size = 1;
			type = GL_INT;
			sizeInBytes = sizeof(int) * size;
			break;
		}

		default:
			break;
	}
}

/**
 * Function : ParseFormatForTexture
 * See for details: https://www.opengl.org/sdk/docs/man/html/glTexImage2D.xhtml
 */
void OpenGLUtils::ParseFormatForTexture(const RHIFormat format, GLint& internalFormat, GLenum& pixelFormat, GLenum& pixelType)
{
	switch (format)
	{
		case RHIFormat::R32G32B32A32Float:
		{
			internalFormat	= GL_RGBA32F;
			pixelFormat		= GL_RGBA;
			pixelType		= GL_FLOAT;
			break;
		}

		case RHIFormat::R32G32B32Float:
		{
			internalFormat	= GL_RGB32F;
			pixelFormat		= GL_RGB;
			pixelType		= GL_FLOAT;
			break;
		}

		case RHIFormat::R32G32Float:
		{
			internalFormat	= GL_RG32F;
			pixelFormat		= GL_RG;
			pixelType		= GL_FLOAT;
			break;
		}

		case RHIFormat::R8G8B8A8UInt:
		{
			internalFormat	= GL_RGBA;
			pixelFormat		= GL_RGBA;
			pixelType		= GL_UNSIGNED_BYTE;
			break;
		}

		case RHIFormat::R32Float:
		{
			internalFormat	= GL_R32F;
			pixelFormat		= GL_RED;
			pixelType		= GL_FLOAT;
			break;
		}

		case RHIFormat::R32Sint:
		{
			internalFormat	= GL_R32I;
			pixelFormat		= GL_RED;
			pixelType		= GL_INT;
			break;
		}

		default:
		{
			internalFormat	= -1;
			pixelFormat		= GL_NONE;
			pixelType		= GL_NONE;
		}
	}
}

/**
 * Function : ParseBufferType
 */
GLenum OpenGLUtils::ParseBufferType(const RHIBufferType& type)
{
	switch (type)
	{
		case RHIBufferType::Constant:
			return GL_UNIFORM_BUFFER;

		case RHIBufferType::Index:
			return GL_ELEMENT_ARRAY_BUFFER;

		case RHIBufferType::Vertex:
			return GL_ARRAY_BUFFER;

		default:
			return 0;
	}
}

/**
 * Function : ParseBufferUsage
 */
GLenum OpenGLUtils::ParseBufferUsage(const RHIBufferUsage& usage)
{
	switch (usage)
	{
		case RHIBufferUsage::StaticDraw:
			return GL_STATIC_DRAW;

		case RHIBufferUsage::DynamicDraw:
			return GL_DYNAMIC_DRAW;

		case RHIBufferUsage::StreamDraw:
			return GL_STREAM_DRAW;

		case RHIBufferUsage::StaticRead:
			return GL_STATIC_READ;

		case RHIBufferUsage::DynamicRead:
			return GL_DYNAMIC_READ;

		case RHIBufferUsage::StreamRead:
			return GL_STREAM_READ;

		default:
			return 0;
	}
}

/**
 * Function : ParseFilter
 */
void OpenGLUtils::ParseFilter(const RHIFilter filter, GLenum& minFilter, GLenum& magFilter)
{
	switch (filter)
	{
		case RHIFilter::MinMagMipPoint:
		{
			minFilter = GL_NEAREST;
			magFilter = GL_NEAREST;
			break;
		}

		case RHIFilter::MinPointMagMipLinear:
		{
			minFilter = GL_NEAREST;
			magFilter = GL_LINEAR;
			break;
		}

		case RHIFilter::MinLinearMagMipPoint:
		{
			minFilter = GL_LINEAR;
			magFilter = GL_NEAREST;
			break;
		}

		case RHIFilter::MinMagMipLinear:
		{
			minFilter = GL_LINEAR;
			magFilter = GL_LINEAR;
			break;
		}
	}
}

/**
 * Function : ParseTextureAddresMode
 */
GLenum OpenGLUtils::ParseTextureAddresMode(const RHITextureAddressMode mode)
{
	switch (mode)
	{
		case RHITextureAddressMode::Wrap:
			return GL_REPEAT;

		case RHITextureAddressMode::Border:
			return GL_CLAMP_TO_BORDER;

		case RHITextureAddressMode::Clamp:
			return GL_CLAMP_TO_EDGE;

		case RHITextureAddressMode::Mirror:
			return GL_MIRRORED_REPEAT;

		case RHITextureAddressMode::MirrorOnce:
			return GL_MIRROR_CLAMP_TO_EDGE;

		default:
			return GL_NONE;
	}
}

/**
 * Function : ParseLogicOp
 */
GLenum OpenGLUtils::ParseLogicOp(const RHILogicOp logicOp)
{
	switch (logicOp)
	{
		case RHILogicOp::Clear:
			return GL_CLEAR;

		case RHILogicOp::Set:
			return GL_SET;

		case RHILogicOp::Copy:
			return GL_COPY;

		case RHILogicOp::CopyInverted:
			return GL_COPY_INVERTED;

		case RHILogicOp::Noop:
			return GL_NOOP;

		case RHILogicOp::Invert:
			return GL_INVERT;

		case RHILogicOp::And:
			return GL_AND;

		case RHILogicOp::Nand:
			return GL_NAND;

		case RHILogicOp::Or:
			return GL_OR;

		case RHILogicOp::Nor:
			return GL_NOR;

		case RHILogicOp::Xor:
			return GL_XOR;

		case RHILogicOp::Equiv:
			return GL_EQUIV;

		case RHILogicOp::AndReverse:
			return GL_AND_REVERSE;

		case RHILogicOp::AndInverted:
			return GL_AND_INVERTED;

		case RHILogicOp::OrReverse:
			return GL_OR_REVERSE;

		case RHILogicOp::OrInverted:
			return GL_OR_INVERTED;

		default:
			return GL_CLEAR;
	}
}

/**
 * Function : ParseBlend
 */
GLenum OpenGLUtils::ParseBlend(const RHIBlend blend)
{
	switch (blend)
	{
		case RHIBlend::Zero:
			return GL_ZERO;

		case RHIBlend::One:
			return GL_ONE;

		case RHIBlend::SrcColor:
			return GL_SRC_COLOR;

		case RHIBlend::InvSrcColor:
			return GL_ONE_MINUS_SRC_COLOR;

		case RHIBlend::DestColor:
			return GL_DST_COLOR;

		case RHIBlend::InvDestColor:
			return GL_ONE_MINUS_DST_COLOR;

		case RHIBlend::SrcAlpha:
			return GL_SRC_ALPHA;

		case RHIBlend::InvSrcAlpha:
			return GL_ONE_MINUS_SRC_ALPHA;

		case RHIBlend::DestAlpha:
			return GL_DST_ALPHA;

		case RHIBlend::InvDestAlpha:
			return GL_ONE_MINUS_DST_ALPHA;

		case RHIBlend::BlendFactor:
			return GL_CONSTANT_COLOR;

		case RHIBlend::InvBlendFactor:
			return GL_ONE_MINUS_CONSTANT_COLOR;

		case RHIBlend::SrcAlphaSat:
			return GL_SRC_ALPHA_SATURATE;

		case RHIBlend::Src1Color:
			return GL_SRC1_COLOR;

		case RHIBlend::InvSrc1Color:
			return GL_ONE_MINUS_SRC1_COLOR;

		case RHIBlend::Src1Alpha:
			return GL_SRC1_ALPHA;

		case RHIBlend::InvSrc1Alpha:
			return GL_ONE_MINUS_SRC1_ALPHA;

		case RHIBlend::BlendAlpha:
			return GL_CONSTANT_ALPHA;

		case RHIBlend::InvBlendAlpha:
			return GL_ONE_MINUS_CONSTANT_ALPHA;

		default:
			return GL_ZERO;
	}
}

/**
 * Function : ParseBlendOp
 */
GLenum OpenGLUtils::ParseBlendOp(const RHIBlendOp blendOp)
{
	switch (blendOp)
	{
		case RHIBlendOp::Add:
			return GL_FUNC_ADD;

		case RHIBlendOp::Subtract:
			return GL_FUNC_SUBTRACT;

		case RHIBlendOp::RevSubtract:
			return GL_FUNC_REVERSE_SUBTRACT;

		case RHIBlendOp::Min:
			return GL_MIN;

		case RHIBlendOp::Max:
			return GL_MAX;

		default:
			return GL_FUNC_ADD;
	}
}

/**
 * Function : ParsePrimitiveTopology
 */
GLenum OpenGLUtils::ParsePrimitiveTopology(const RHIPrimitiveTopology topology)
{
	switch (topology)
	{
		case RHIPrimitiveTopology::PointList:
		{
			return GL_POINTS;
		}

		case RHIPrimitiveTopology::TriangleList:
		{
			return GL_TRIANGLES;
		}

		case RHIPrimitiveTopology::Control_Point_Patchlist_1:
		{
			return GL_PATCHES;
		}

		default:
		{
			assert(false && "Undefined primitive topology.");
		}
	}

	return GL_NONE;
}
