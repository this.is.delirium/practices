#pragma once
#include "StdafxRHI.h"
#include <RHI.h>

class OpenGLShaderProgram
{
protected:
	static const GLuint kNullObject = 0;

public:
	OpenGLShaderProgram();
	~OpenGLShaderProgram();

	void Release();

	void AttachShader(const RHIShaderBytecode& shader, const RHIShaderType type);
	bool Create();

	FORCEINLINE bool IsValid() const { return mShaderProgram != kNullObject; }

	FORCEINLINE void Bind() const { glUseProgram(mShaderProgram); }
	FORCEINLINE void Unbind() const { glUseProgram(kNullObject); }

	FORCEINLINE GLuint Id() const { return mShaderProgram; }

	bool IsActive() const
	{
		GLint id;
		glGetIntegerv(GL_CURRENT_PROGRAM, &id);

		return id == mShaderProgram;
	}

	// I don't know how it looks in directx.
	// TODO: rewrite.
	void SetFeedback(const RHIStreamOutputDesc& outputDesc);
	bool IsFeedback() const { return mFeedbackVaryings.size() > 0; }

public: // OpenGL getters
	FORCEINLINE GLuint GetUniformBlockIndex(const char* blockName) const { return glGetUniformBlockIndex(mShaderProgram, blockName); }
	FORCEINLINE GLint GetUniformLocation(const char* uniformName) const { return glGetUniformLocation(mShaderProgram, uniformName); }

protected:
	GLuint loadShader(const GLenum type, const RHIShaderBytecode& shader);

	GLuint mShaderProgram;
	std::map<RHIShaderType, GLuint> mShaders;

	std::vector<const char*> mFeedbackVaryings;
};
