#pragma once
#include "StdafxRHI.h"
#include <RHI.h>

namespace OpenGLUtils
{
	GLenum ParseShaderType(const RHIShaderType type);
	void ParseFormat(const RHIFormat format, GLint& size, GLenum& type, GLint& sizeInBytes);
	void ParseFormatForTexture(const RHIFormat format, GLint& internalFormat, GLenum& pixelFormat, GLenum& pixelType); // nice name, I know it.
	GLenum ParseBufferType(const RHIBufferType& type);
	GLenum ParseBufferUsage(const RHIBufferUsage& usage);

	void ParseFilter(const RHIFilter filter, GLenum& minFilter, GLenum& magFilter);
	GLenum ParseTextureAddresMode(const RHITextureAddressMode mode);

	GLenum ParseLogicOp(const RHILogicOp logicOp);
	GLenum ParseBlend(const RHIBlend blend);
	GLenum ParseBlendOp(const RHIBlendOp blendOp);

	GLenum ParsePrimitiveTopology(const RHIPrimitiveTopology topology);
}
