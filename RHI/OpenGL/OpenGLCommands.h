#pragma once
#include "StdafxRHI.h"
#include <RHI.h>
#include "private/OpenGLUtils.h"

struct OpenGLCommandBindProgram : public RHICommand
{
	OpenGLCommandBindProgram(const GLuint program)
		: mProgram(program) { }

	void Execute() override final
	{
		glUseProgram(mProgram);
	}

	~OpenGLCommandBindProgram()
	{
		glUseProgram(0);
	}

	GLuint mProgram;
};

struct OpenGLShaderInputAttributes;
struct OpenGLCommandVertexAttrib : public RHICommand
{
	OpenGLCommandVertexAttrib(const OpenGLShaderInputAttributes* attributes)
		: mAttributes(attributes) { }

	~OpenGLCommandVertexAttrib();

	void Execute() override final;

	const OpenGLShaderInputAttributes* mAttributes;
};

struct OpenGLCommandClearColor : public RHICommand
{
	OpenGLCommandClearColor(const FLOAT* color)
		: RHICommand(), mColor(color[0], color[1], color[2], color[3]) { }

	void Execute() override final
	{
		glClearColor(mColor.r, mColor.g, mColor.b, mColor.a);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // TODO: Rewrite. Set up PSO.
	}

	glm::vec4 mColor;
};

struct OpenGLCommandViewport : public RHICommand
{
	OpenGLCommandViewport(const RHIViewport* viewport)
		: RHICommand(), mViewport(*viewport) { }

	void Execute() override final { glViewport((GLint)mViewport.TopLeftX, (GLint)mViewport.TopLeftY, (GLsizei)mViewport.Width, (GLsizei)mViewport.Height); }

	const RHIViewport mViewport;
};

struct OpenGLCommandScissor : public RHICommand
{
	OpenGLCommandScissor(const RHIRect* scissor)
		: RHICommand(), mScissor(*scissor) { }

	~OpenGLCommandScissor() { glDisable(GL_SCISSOR_TEST); }

	void Execute() override final
	{
		glEnable(GL_SCISSOR_TEST);
		glScissor(mScissor.left, mScissor.top, mScissor.right, mScissor.bottom);
	}

	const RHIRect mScissor;
};

class OpenGLResource;
struct OpenGLCommandSetResource : public RHICommand
{
	OpenGLCommandSetResource(const OpenGLResource* resource)
		: RHICommand(), mResource(resource) { }

	~OpenGLCommandSetResource();

	void Execute() override final;

	const OpenGLResource* mResource;
};

struct OpenGLCommandSetVertexBuffers : public RHICommand
{
	OpenGLCommandSetVertexBuffers(const UINT startSlot, const UINT numView, const RHIVertexBufferView* views)
	: RHICommand(), mStartSlot(startSlot), mNumViews(numView), mViews(views) { }

	~OpenGLCommandSetVertexBuffers();

	void Execute() override final;

	const UINT					mStartSlot;
	const UINT					mNumViews;
	const RHIVertexBufferView*	mViews;
};

class OpenGLBuffer;
struct OpenGLCommandSetConstantBuffer : public RHICommand
{
	OpenGLCommandSetConstantBuffer(const GLuint programId, const UINT slot, const UINT numBuffers, const OpenGLBuffer* buffer)
		: mProgramId(programId), mSlot(slot), mNumBuffers(numBuffers), mBuffer(buffer) { }

	~OpenGLCommandSetConstantBuffer();

	void Execute() override final;

	UINT mSlot;
	UINT mNumBuffers;
	GLuint mProgramId;
	const OpenGLBuffer* mBuffer;
};

struct OpenGLCommandSetSampler : public RHICommand
{
	OpenGLCommandSetSampler(const GLuint samplerId, const GLuint textureUnit)
		: RHICommand(), mSamplerId(samplerId), mTextureUnit(textureUnit) { }

	~OpenGLCommandSetSampler() { glBindSampler(mTextureUnit, 0); }

	void Execute() override final { glBindSampler(mTextureUnit, mSamplerId); }

	GLuint mSamplerId;
	GLuint mTextureUnit;
};

struct OpenGLCommandDrawInstanced : public RHICommand
{
	OpenGLCommandDrawInstanced(
		const RHIPrimitiveTopology primitiveTopology,
		const uint32_t vertexCountPerInstance,
		const uint32_t instanceCount,
		const uint32_t startVertexLocation,
		const uint32_t startInstanceLocation)
		: RHICommand(),
		mVertexCountPerInstance(vertexCountPerInstance),
		mInstanceCount(instanceCount),
		mStartVertexLocation(startVertexLocation),
		mStartInstanceLocation(mStartInstanceLocation),
		mPrimitiveTopology(primitiveTopology){ }

	void Execute() override final
	{
		glDrawArrays(OpenGLUtils::ParsePrimitiveTopology(mPrimitiveTopology), mStartVertexLocation, mVertexCountPerInstance);
	}

	uint32_t				mVertexCountPerInstance;
	uint32_t				mInstanceCount;
	uint32_t				mStartVertexLocation;
	uint32_t				mStartInstanceLocation;
	RHIPrimitiveTopology	mPrimitiveTopology;
};

struct OpenGLCommandDrawIndexedInstanced : public RHICommand
{
	OpenGLCommandDrawIndexedInstanced(
		const RHIPrimitiveTopology primitiveTopology,
		const uint32_t indexCountPerInstance/*,
		const uint32_t instanceCount,
		const uint32_t startIndexLocation,
		/*const int32_t baseVertexLocation,
		const uint32_t startInstanceLocation*/,
		const bool isUnsignedInt,
		const void* offset)
		: RHICommand(),
		mIndexCountPerInstance(indexCountPerInstance),
		/*mInstanceCount(instanceCount),
		mStartIndexLocation(startIndexLocation),
		mBaseVertexLocation(baseVertexLocation),
		mStartInstanceLocation(startInstanceLocation),*/
		mPrimitiveTopology(primitiveTopology),
		mOffset(offset)
	{
		mType = (isUnsignedInt) ? GL_UNSIGNED_INT : GL_UNSIGNED_SHORT;
	}

	void Execute() override final
	{
		glDrawElements(OpenGLUtils::ParsePrimitiveTopology(mPrimitiveTopology), mIndexCountPerInstance, mType, mOffset);
	}

	uint32_t				mIndexCountPerInstance;
	/*uint32_t				mInstanceCount;*/
	uint32_t				mStartIndexLocation;
	/*int32_t					mBaseVertexLocation;
	uint32_t				mStartInstanceLocation;*/
	RHIPrimitiveTopology	mPrimitiveTopology;
	GLenum					mType;
	const GLvoid*			mOffset;
};

class OpenGLResource;
struct OpenGLCommandDrawStreamOutput : public RHICommand
{
	OpenGLCommandDrawStreamOutput(const OpenGLResource* transformFeedback, const RHIPrimitiveTopology topology)
		: RHICommand(), mTransFeedback(transformFeedback), mPrimitiveTopology(topology) { }

	void Execute() override final;

	const OpenGLResource*	mTransFeedback;
	RHIPrimitiveTopology	mPrimitiveTopology;
};

class OpenGLTransformFeedback;
struct OpenGLCommandSetShaderOutput : public RHICommand
{
	OpenGLCommandSetShaderOutput(const OpenGLTransformFeedback* buffer, const RHIPrimitiveTopology topology)
		: mFeedback(buffer), mPrimitiveTopology(topology) { }

	~OpenGLCommandSetShaderOutput();

	void Execute() override final;

	const OpenGLTransformFeedback*	mFeedback;
	RHIPrimitiveTopology			mPrimitiveTopology;
};
