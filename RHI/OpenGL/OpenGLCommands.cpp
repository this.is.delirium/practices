#include "StdafxRHI.h"
#include "OpenGLCommands.h"
#include "OpenGLBuffer.h"
#include "OpenGLGraphicsPipelineState.h"
#include "OpenGLResource.h"
#include "OpenGLTransformFeedback.h"
#include <RHI.h>

/**
 * Function : OpenGLCommandVertexAttrib::Execute
 */
void OpenGLCommandVertexAttrib::Execute()
{
	glEnableVertexAttribArray(mAttributes->mAttribIndex);
	glVertexAttribPointer(
		mAttributes->mAttribIndex,
		mAttributes->mSize,
		mAttributes->mType,
		mAttributes->mNormalized,
		mAttributes->mStride,
		mAttributes->mPointer
	);
}

/**
 * Function : OpenGLCommandVertexAttrib::~OpenGLCommandVertexAttrib
 */
OpenGLCommandVertexAttrib::~OpenGLCommandVertexAttrib()
{
	glDisableVertexAttribArray(mAttributes->mAttribIndex);
}

/**
 * Function : ~OpenGLCommandSetResource
 */
OpenGLCommandSetResource::~OpenGLCommandSetResource()
{
	mResource->Unbind();
}

/**
 * Function : OpenGLCommandSetResource::Execute
 */
void OpenGLCommandSetResource::Execute()
{
	mResource->Bind();
}

/**
 * Function : OpenGLCommandSetVertexBuffer
 */
OpenGLCommandSetVertexBuffers::~OpenGLCommandSetVertexBuffers()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

/**
 * Function : OpenGLCommandSetVertexBuffer::Execute
 */
void OpenGLCommandSetVertexBuffers::Execute()
{
	glBindBuffer(GL_ARRAY_BUFFER, (GLuint)mViews[0].BufferLocation);
}

/**
 * Function : ~OpenGLCommandSetConstantBuffer
 */
OpenGLCommandSetConstantBuffer::~OpenGLCommandSetConstantBuffer()
{
	mBuffer->Unbind();
}

/**
 * Function : OpenGLCommandSetConstantBuffer::Execute
 */
void OpenGLCommandSetConstantBuffer::Execute()
{
	glUniformBlockBinding(mProgramId, mSlot, mSlot);
	glBindBufferBase(mBuffer->Target(), mSlot, mBuffer->ResourceId());
}

/**
 * Function : ~OpenGLCommandSetShaderOutput
 */
OpenGLCommandSetShaderOutput::~OpenGLCommandSetShaderOutput()
{
	glEndTransformFeedback();
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
}

/**
 * Function : OpenGLCommandSetShaderOutput::Execute
 */
void OpenGLCommandSetShaderOutput::Execute()
{
	// Implemented only GL_INTERLEAVED_ATTRIBS mode at this moment.
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, mFeedback->ResourceId());
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, mFeedback->AttachedBuffer());
	glBeginTransformFeedback(OpenGLUtils::ParsePrimitiveTopology(mPrimitiveTopology));
}

/**
 * Function : OpenGLCommandDrawStreamOutput::Execute()
 */
void OpenGLCommandDrawStreamOutput::Execute()
{
	glDrawTransformFeedback (OpenGLUtils::ParsePrimitiveTopology(mPrimitiveTopology), mTransFeedback->ResourceId());
}
