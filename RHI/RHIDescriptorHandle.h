#pragma once
#include "StdafxRHI.h"
#include "RHI.h"
#include "RHIDescriptorHeapManager.h"

class RHIDescriptorHandle
{
public:
	void Release();
	void Initialize(RHIDescriptorHeapManager& heapManager);

	//! Requests the continuous set of descriptors in the heap. Returns false if it is impossible.
	bool InitializeMultipleDescriptors(RHIDescriptorHeapManager& heapManager, const UINT numDescriptors);

	FORCEINLINE const RHICPUDescriptorHandle& Cpu() const { return mCpu; }
	FORCEINLINE const RHIGPUDescriptorHandle& Gpu() const { return mGpu; }

	FORCEINLINE bool IsMultipleDescriptors() const { return mNumDescriptors > 1; }
	FORCEINLINE UINT NumDescriptors() const { return mNumDescriptors; }
	FORCEINLINE int Descriptor() const { return mDescriptor; }

protected:
	UINT					mNumDescriptors	= 0;
	int						mDescriptor		= RHIDescriptorHeapManager::kNullDescriptor;
	RHICPUDescriptorHandle	mCpu;
	RHIGPUDescriptorHandle	mGpu;
};
