#pragma once
#include "StdafxRHI.h"
#include "RHI.h"
#include "RHITexture.h"

struct MRTPassDesc
{
	struct DepthStencil
	{
		RHIResourceDesc	Desc;
		RHIClearValue	ClearValue;
		RHIFormat		Format;
		RHIFormat		ViewFormat;
	};

	UINT					NumRT;
	RHIResourceDesc			Descs[8];
	RHIClearValue			ClearValues[8];

	DepthStencil			DepthStencil;

	RHIInputLayoutDesc		InputLayout;
	RHIBlendDesc			BlendDesc;
};

class MRTPass
{
public:
	virtual void Release();
	virtual void Initialize(const MRTPassDesc& desc);

	void BindRenderTargets(RHICommandList* cmdList, const bool clear) const;
	void BindShaderResources(RHICommandList* cmdList) const;

public:
	FORCEINLINE bool& ShowTexture() { return mShowTexture; }
	FORCEINLINE int& ShowTextureId() { return mShowTextureId; }

public: // setters.
	FORCEINLINE void SetRootSignature(RHIRootSignature* rootSignature) { mRootSignature = rootSignature; }
	FORCEINLINE void SetPSO(RHIPipelineState* pso) { mPSO = pso; }

public: // getters.
	FORCEINLINE int NumRT() const { return mNumRT; }

	FORCEINLINE RHIRootSignature* RootSignature() { return mRootSignature; }
	FORCEINLINE RHIPipelineState* PSO() { return mPSO; }

	FORCEINLINE RHITexture& RenderTarget(const int id) { return mRenderTargets[id]; }
	FORCEINLINE const RHITexture& RenderTarget(const int id) const { return mRenderTargets[id]; }

	FORCEINLINE const RHITexture& DepthStencil() const { return mDepthStencil; }

	FORCEINLINE const RHICPUDescriptorHandle* RtvCpuHandles() const { return mRtvCpuHandles.data(); }

	FORCEINLINE const RHIClearValue& ClearValue(const int id) const { return mClearValues[id]; }

	FORCEINLINE RHIFormat Format(const int id) const { return mFormats[id]; }
	FORCEINLINE RHIFormat DepthStencilViewFormat() const { return mDepthStencilViewFormat; }

	FORCEINLINE const RHIInputLayoutDesc& InputLayoutDesc() const { return mInputLayoutDesc; }

	FORCEINLINE const RHIBlendDesc& BlendState() const { return mBlendDesc; }

protected:
	RHIRootSignature*								mRootSignature			= nullptr;
	RHIPipelineState*								mPSO					= nullptr;

	std::vector<RHITexture>							mRenderTargets;
	std::vector<RHICPUDescriptorHandle>				mRtvCpuHandles;

	RHITexture										mDepthStencil;
	RHIFormat										mDepthStencilViewFormat	= RHIFormat::Unknown;
	RHIClearValue									mDepthStencilClearValue;
	bool											mDepthStencilIsShaderResources;

	std::vector<RHIClearValue>						mClearValues;
	std::vector<RHIFormat>							mFormats;

	UINT											mNumRT;

	RHIInputLayoutDesc								mInputLayoutDesc;
	RHIBlendDesc									mBlendDesc;

	bool											mShowTexture;
	int												mShowTextureId			= 0;

	static const int								kBarriersSize			= _countof(MRTPassDesc::Descs) + 1; // +1 to insert a barrier of depthstencil.
	std::array<RHIResourceBarrier, kBarriersSize>	mBindToRenderTargetBarriers;
	std::array<RHIResourceBarrier, kBarriersSize>	mBindToShaderResourceBarriers;
};
