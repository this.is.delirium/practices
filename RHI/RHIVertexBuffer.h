#pragma once
#include "StdafxRHI.h"
#include "RHIBuffer.h"

class RHIVertexBuffer : public RHIBuffer
{
public:
	FORCEINLINE RHIResourceState ResourceState() override final { return RHIResourceState::VertexAndConstantBuffer; }
	FORCEINLINE RHIVertexBufferView* View() { return &mVertexBufferView; }
	FORCEINLINE const RHIVertexBufferView* View() const { return &mVertexBufferView; }

protected:
	void CreateDerivedViews() override final;

protected:
	RHIVertexBufferView	mVertexBufferView;
};
