#include "StdafxRHI.h"
#include "RHIIndexBuffer.h"

/**
* Function : CreateDerivedViews
*/
void RHIIndexBuffer::CreateDerivedViews()
{
	mIndexBufferView.BufferLocation		= mBuffer->GetGPUVirtualAddress();
	mIndexBufferView.SizeInBytes		= mNumElements * mStrideInBytes;
	mIndexBufferView.Format				= (mStrideInBytes == 2) ? RHIFormat::R16Uint : RHIFormat::R32Uint;
}
