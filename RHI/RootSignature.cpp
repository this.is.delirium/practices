#include "StdafxRHI.h"
#include "RootSignature.h"
#include <Render/GraphicsCore.h>

using namespace GraphicsCore;

namespace RHIUtils
{
	RootParameter& RootParameter::InitAsDescriptorTable(const int numRanges, const RHIShaderVisibility visibility)
	{
		mRanges.reset(new RHIDescriptorRange[numRanges]);

		mParameter.ParameterType	= RHIRootParameterType::DescriptorTable;
		mParameter.ShaderVisibility	= visibility;
		RHIRootDescriptorTable::Init(mParameter.DescriptorTable, numRanges, mRanges.get());

		return *this;
	}

	void RootParameter::InitAsConstantBufferView(const UINT shaderRegister, const UINT registerSpace, const RHIShaderVisibility visibility)
	{
		mParameter.InitAsConstantBufferView(shaderRegister, registerSpace, visibility);
	}

	void RootSignature::Init(const UINT nbOfParameters, const UINT nbOfStaticSamplers)
	{
		mParameters.reset(new RootParameter[nbOfParameters]);

		if (nbOfStaticSamplers > 0)
		{
			mStaticSamplerDescs.reset(new RHIStaticSamplerDesc[nbOfStaticSamplers]);

			for (UINT i = 0; i < nbOfStaticSamplers; ++i)
				mStaticSamplerDescs[i] = RHIStaticSamplerDesc(RHIDefault());
		}

		mDesc					= {};
		mDesc.NumParameters		= nbOfParameters;
		mDesc.NumStaticSamplers	= nbOfStaticSamplers;
	}

	RHIRootSignature* RootSignature::Finalize(const RHIRootSignatureFlags flags)
	{
		std::unique_ptr<RHIRootParameter[]> parameters;
		parameters.reset(new RHIRootParameter[mDesc.NumParameters]);
		for (UINT i = 0; i < mDesc.NumParameters; ++i)
			parameters[i] = mParameters[i];

		mDesc.Flags				= flags;
		mDesc.StaticSamplers	= mStaticSamplerDescs.get();
		mDesc.Parameters		= parameters.get();

		return gRenderer->SerializeAndCreateRootSignature(&mDesc, RHIRootSignatureVersion::Ver1);
	}
}
