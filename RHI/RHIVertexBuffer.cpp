#include "StdafxRHI.h"
#include "RHIVertexBuffer.h"

/**
 * Function : CreateDerivedViews
 */
void RHIVertexBuffer::CreateDerivedViews()
{
	mVertexBufferView.BufferLocation	= mBuffer->GetGPUVirtualAddress();
	mVertexBufferView.StrideInBytes		= mStrideInBytes;
	mVertexBufferView.SizeInBytes		= mNumElements * mStrideInBytes;
}
