#pragma once
#include "StdafxRHI.h"
#include "RHI.h"
#include "RHIDescriptorHandle.h"

class IRenderer;

class RHITexture
{
public:
	void Release();

	void InitializeByName(const String& textureName);
	void InitializeByPath(const String& path);
	void InitializeCubeMap(const String& folder, const String& extension);
	void Initialize(const RHIResourceDesc* desc, const RHIClearValue* clearValue = nullptr, RHISubresourceData* subresourceData = nullptr);

	FORCEINLINE RHIResource* Texture() { return mTexture; }
	FORCEINLINE RHIResource* Texture() const { return mTexture; }

	FORCEINLINE const RHIDescriptorHandle& RtvHandle() const { return mRtvHandle; }
	FORCEINLINE const RHIDescriptorHandle& DsvHandle() const { return mDsvHandle; }
	FORCEINLINE const RHIDescriptorHandle& SrvHandle() const { return mSrvHandle; }
	FORCEINLINE const RHIDescriptorHandle& UavHandle() const { return mUavHandle; }

	FORCEINLINE RHIGPUVirtualAddress Location() { return mTexture->GetGPUVirtualAddress(); }

protected:
	RHIResource*		mTexture	= nullptr;

	RHIDescriptorHandle	mRtvHandle;
	RHIDescriptorHandle mDsvHandle;
	RHIDescriptorHandle	mSrvHandle;
	RHIDescriptorHandle	mUavHandle;
};

using TextureRef = std::shared_ptr<RHITexture>;
