#pragma once
#include "StdafxRHI.h"
#include "RHIBuffer.h"

class RHIIndexBuffer : public RHIBuffer
{
public:
	FORCEINLINE RHIResourceState ResourceState() override final { return RHIResourceState::IndexBuffer; }
	FORCEINLINE RHIIndexBufferView* View() { return &mIndexBufferView; }
	FORCEINLINE const RHIIndexBufferView* View() const { return &mIndexBufferView; }

protected:
	void CreateDerivedViews() override final;

protected:
	RHIIndexBufferView	mIndexBufferView;
};
