#include "StdafxRHI.h"
#include "RHIBuffer.h"
#include <Render/GraphicsCore.h>

using namespace GraphicsCore;

/**
 * Function : Initialize
 */
void RHIBuffer::Initialize(const String& name, const UINT64 maxWidth, const UINT numElements, const UINT strideInBytes, const void* data, const RHIHeapType heapType)
{
	mHeapType		= heapType;
	mNumElements	= numElements;
	mStrideInBytes	= strideInBytes;

	RHIResourceState state = (mHeapType == RHIHeapType::Default) ? ResourceState() : RHIResourceState::GenericRead;
	mBuffer		= gRenderer->Device()->CreateCommittedResource(
		&RHIHeapProperties(mHeapType),
		RHIHeapFlag::None,
		&RHIResourceDesc::Buffer(maxWidth, mFlags),
		state,
		nullptr);
	mBuffer->SetName(name.c_str());

	CreateDerivedViews();

	if (!data)
		return;

	RHISubresourceData subresData	= {};
	subresData.Data					= data;
	subresData.RowPitch				= strideInBytes * numElements;
	subresData.SlicePitch			= subresData.RowPitch;

	gRenderer->UpdateBuffer(this, &subresData);
}
