#pragma once
#include "StdafxRHI.h"
#include "RHIBuffer.h"
#include "RHIByteAddressBuffer.h"

class RHIStructuredBuffer : public RHIBuffer
{
public:
	RHIStructuredBuffer()
		: RHIBuffer(RHIResourceFlag::AllowUnorderedAccess) { }

	void Release() override final;

public:
	FORCEINLINE RHIResourceState ResourceState() override final { return RHIResourceState::Common; }
	FORCEINLINE RHIByteAddressBuffer& Counter() { return mCounter; }

protected:
	void CreateDerivedViews() override final;

protected:

	RHIByteAddressBuffer mCounter;
};
