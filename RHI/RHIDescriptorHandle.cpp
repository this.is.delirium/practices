#include "StdafxRHI.h"
#include "RHIDescriptorHandle.h"
#include <Render/GraphicsCore.h>
#include <Render/IRenderer.h>

using namespace GraphicsCore;

/**
 * Function : Release
 */
void RHIDescriptorHandle::Release()
{
	if (IsMultipleDescriptors())
		gRenderer->CbvSrvUavHeapManager().FreeDescriptors(mDescriptor, mNumDescriptors);
	else
		gRenderer->CbvSrvUavHeapManager().FreeDescriptor(mDescriptor);

	mCpu = RHICPUDescriptorHandle(0);
	mGpu = RHIGPUDescriptorHandle(0);
}

/**
 * Function : Initialize
 */
void RHIDescriptorHandle::Initialize(RHIDescriptorHeapManager& heapManager)
{
	mNumDescriptors	= 1;
	mDescriptor		= heapManager.GetNewDescriptor();
	mCpu			= heapManager.CpuHandle(mDescriptor);
	mGpu			= heapManager.GpuHandle(mDescriptor);
}

/**
 * Function : InitializeMultipleHandles
 */
bool RHIDescriptorHandle::InitializeMultipleDescriptors(RHIDescriptorHeapManager& heapManager, const UINT numDescriptors)
{
	if (!heapManager.HasDescriptors(numDescriptors))
		return false;

	mNumDescriptors	= numDescriptors;
	mDescriptor		= heapManager.GetStartContinuousDescriptors(numDescriptors);
	mCpu			= heapManager.CpuHandle(mDescriptor);
	mGpu			= heapManager.GpuHandle(mDescriptor);

	return true;
}
