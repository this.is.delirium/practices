#include "StdafxSamples.h"
#include "PSOEditor.h"
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHIUtils.h>
#include <StringUtils.h>

/**
 * Function : Release
 */
void PSOEditor::Release()
{
	for (auto& registeredPSOs : mRegisteredPSOs)
		for (auto& registeredPSO : registeredPSOs.second)
			for (auto& registeredShader : registeredPSO.Shaders)
				RHISafeRelease(registeredShader.Blob);
}

/**
 * Function : Load
 */
void PSOEditor::LoadShader(const String& hierarchy)
{
	mIsReady = true;
	mShaderBuffer = SResourceManager::Instance().LoadFile(SResourceManager::Instance().Find(hierarchy));
	mShaderBuffer.resize(mShaderBuffer.size() * 2);
}

/**
 * Function : Save
 */
void PSOEditor::SaveShader(const String& hierarchy)
{
	mIsReady = false;

	ResourceManager& resManager = SResourceManager::Instance();
	resManager.SaveFile(resManager.Find(hierarchy), mShaderBuffer);

	mIsReady = true;
}

/**
 * Function : RegisterPSO
 */
PSOEditor::PSOInfo& PSOEditor::RegisterPSO(const String& modeName, const String& psoName, RHIPipelineState** pso, const RHIPipelineStateStream& psoDesc)
{
	mRegisteredPSOs[modeName].emplace_back();

	PSOInfo& info = mRegisteredPSOs[modeName].back();
	info.Name		= psoName;
	info.PSO		= pso;
	info.PSODesc	= psoDesc;

	return mRegisteredPSOs[modeName].back();
}

/**
 * Function : AttachShaders
 */
void PSOEditor::PSOInfo::AttachShaders(const RHIUtils::Fill::ShadersArray& shaders)
{
	for (int i = 0; i < _countof(shaders.Shaders); ++i)
	{
		const RHIUtils::Fill::ShaderInfo& shader = shaders.Shaders[i];

		if (shader.Type == RHIShaderType::Unknown)
			continue;

		RegisterShader(shader.Name, shader.Type, shader.Blob);
	}
}

/**
 * Function : RegisterShader
 */
void PSOEditor::PSOInfo::RegisterShader(const String& name, const RHIShaderType type, RHIBlob* blob)
{
	String modeName(__TEXT("Common"));
	ShaderInfo& info = Shaders[ToInt(type)];
	size_t pos = name.find_first_of(__TEXT(":"));

	if (pos != String::npos)
	{
		modeName = name.substr(0, pos);
		info.Name = name.substr(pos + 1);
	}
	else
		info.Name = name;

	info.Type		= type;
	info.Hierarchy	= name;
	info.Blob		= blob;
}

/**
 * Function : RebuildShader
 */
void PSOEditor::RebuildShader(PSOInfo* psoInfo, const ShaderInfo* shaderInfo)
{
	if (psoInfo == nullptr || shaderInfo == nullptr)
		return;

	RHIGraphicsPipelineStateDesc psoDesc = psoInfo->PSODesc.GraphicsDesc();

	String initialDirectory = SResourceManager::Instance().Find(shaderInfo->Hierarchy);
#ifdef UNICODE
	String wString = StringUtils::Converter::StringToWString(mShaderBuffer);
	RHIBlobWrapper shader(GraphicsCore::gRenderer->LoadShader(wString, shaderInfo->Type, nullptr, false, &initialDirectory));
#else
	RHIBlobWrapper shader(GraphicsCore::gRenderer->LoadShader(mShaderBuffer, shaderInfo->Type, nullptr, false, &initialDirectory));
#endif

	RHIShaderBytecode* bytecode = RHIUtils::GetShaderBytecode(&psoDesc, shaderInfo->Type);
	*bytecode = { shader.GetBufferPointer(), shader.GetBufferSize() };

	RHIPipelineState* pso = GraphicsCore::gDevice->CreateGraphicsPipelineState(&psoDesc);

	RHISafeRelease(*psoInfo->PSO);

	*psoInfo->PSO = pso;
}
