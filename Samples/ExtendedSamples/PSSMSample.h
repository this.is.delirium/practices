#pragma once
#include "StdafxSamples.h"
#include "Sample.h"
#include <HeightMapGenerator.h>
#include <ProgressIndicator.h>
#include <RHIIndexBuffer.h>
#include <RHITexture.h>
#include <RHIVertexBuffer.h>
#include <StaticObject.h>
#include <TerrainBuilder.h>

//! Parallel Split Shadow Mapping
class PSSMSample : public Sample
{
public:
	PSSMSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor, true, DirectX::XMFLOAT2(300.0f, 300.0f)) { }

	void Release() override final;

	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	bool IsUseOwnRendering() const override final { return true; }

	void Activate() override final;

protected:
	void SetCameras() override final;

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

	void AllocateDescriptorHandles();
	void CreateDepthTextures();

	void BuildHeightMap();
	void BuildTerrain();

	void LoadContent();
	void GenerateScene();

protected:
	std::shared_ptr<Camera>				mAuxillaryCamera;

	RHIVertexBuffer						mTerrainVB;
	RHIIndexBuffer						mTerrainIB;
	RHITexture							mTerrainTexture;

	DirectX::XMFLOAT2					mTerrainSizeInWorld;
	DirectX::XMFLOAT2					mTerrainHeightRange;
	DirectX::XMUINT2					mSizeMap;

	std::shared_ptr<ProgressIndicator>	mBuildHeightMapProgress;
	std::shared_ptr<ProgressIndicator>	mBuildTerrainProgress;

	using Vertex = InputLayouts::Main::Vertex;
	TerrainBuilder<Vertex>				mTerrainBuilder;
	HeightMapGenerator					mHeightMapGenerator;

	StaticObject						mSpheres[5];
	RHITexture							mSphereTexture;

	ConstBufferRanges					mConstBufferFrustumRanges;

	bool								mExistSamplels	= false;

	struct SkydomePart
	{
		void Release();

		RHIRootSignature*		RootSignature	= nullptr;
		RHIPipelineState*		PSO				= nullptr;

		StaticObject			Mesh;
		RHITexture				Texture;

		ConstBufferPerObject	CB;
	} mSkydome;

	struct DepthPass
	{
		static const int kMaxTextures				= 4;

		void Release();

		RHIRootSignature*		RootSignature		= nullptr;
		RHIPipelineState*		PSO					= nullptr;

		RHIFormat				DsvFormat			= RHIFormat::R24G8Typeless;
		RHIFormat				ViewFormat			= RHIFormat::D24UnormS8Uint;
		RHIFormat				SrvFormat			= RHIFormat::R24UnormX8Typeless;
		RHIResource*			TextureArray		= nullptr;
		RHICPUDescriptorHandle	TextureCpuHandlesDsv[kMaxTextures];
		RHICPUDescriptorHandle	TextureCpuHandlesSrv[kMaxTextures];
		RHIGPUDescriptorHandle	TextureGpuHandlesSrv[kMaxTextures];
		std::vector<int>		DsvTextureDescriptors;
		std::vector<int>		SrvTextureDescriptors;

		DirectX::XMFLOAT2		TextureSize;
		RHIViewport				Viewport;
		RHIRect					ScissorRect;

		ConstBufferPerFrame		CB[kMaxTextures];

		bool					ShowTextures		= true;
		int						TextureId			= 0;
	} mDepthPass;

	struct RenderPass
	{
		void Release();

		int							DepthTextureArrayDescriptor;
		RHICPUDescriptorHandle		DTADCpuHandle;
		RHIGPUDescriptorHandle		DTADGpuHandle;

		ConstBufferCascadeMatrices	CB;
	} mRenderPass;
};
