#pragma once
#include "StdafxSamples.h"
#include <Sample.h>
#include <MRTPass.h>
#include <StaticObject.h>

class SSLRSample : public Sample
{
public:
	SSLRSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor, true, DirectX::XMFLOAT2(300.0f, 300.0f)) { }

	void Release() override final;
	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	bool IsUseOwnRendering() const override final { return true; }

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

	void CreateRenderTargets();

protected:
	struct Room
	{
		void Release();
		void Initialize(IRenderer* renderer, const bool isRandom);

		enum class ID
		{
			Floor = 0,
			Ceiling,
			LeftWall,
			RightWall,
			BackWall,
			Cube1,
			Cube2,
			Cube3,
			Teapot1,
			Teapot2,
			//Sphere1,
			Count
		};

		StaticObject Objects[ToInt(ID::Count)];
	} mRoom;

	MRTPass mGPass;

	struct SSLRPass
	{
		void Release();

		RHIRootSignature*		RootSignature	= nullptr;
		RHIPipelineState*		PSO				= nullptr;

		RHIVertexBuffer			FullScreenQuad;
		ConstBufferSSLRParams	Params;
	} mSSLRPass;
};
