#pragma once
#include <StdafxSamples.h>
#include <Sample.h>
#include <MRTPass.h>
#include <StaticObject.h>
#include <RHIStructuredBuffer.h>
#include <RHITexture.h>

class OITSample : public Sample
{
protected:
	using RenderMethod = std::function<void(RHICommandList*)>;

public:
	OITSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor, true, DirectX::XMFLOAT2(300.0f, 300.0f)) { }

	void Release() override final;
	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	bool IsUseOwnRendering() const override final { return true; }

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

	void CreateWBResources();
	void CreateRenderTargets();
	void CreateFullScreenQuad();

	void CreateLLResources();

	void CreateScene();
	void CreateTestScene();

	void WeightedBlendedRender(RHICommandList* commandList);
	void LinkedListsRender(RHICommandList* commandList);

protected:
	MRTPass mWBFirstPass;

	struct WeightedBlendedSecondPass
	{
		void Release();

		RHIRootSignature*	RootSignature		= nullptr;
		RHIPipelineState*	PSO					= nullptr;
		RHITexture			OpaquePassTexture;
		RHIVertexBuffer		VertexBuffer;
	} mWBSecondPass;

protected:
	static const UINT64 kMaxElementsPerPixel = 8;
	struct ListNode
	{
		UINT PackedColor;
		UINT DepthAndCoverage;
		UINT Next;
	};

	RHITexture		mOpaquePassResult;
	RHIVertexBuffer	mQuadVertexBuffer;

	struct LinkedListsClearPass
	{
		void Release();

		RHIRootSignature*					RootSignature	= nullptr;
		RHIPipelineState*					PSO				= nullptr;

		RHIConstBuffer<DirectX::XMUINT4>	ClearValueCB;
	} mLinkedListsClearPass;

	struct LinkedListsFirstPass
	{
		void Release(IRenderer* renderer);

		RHIRootSignature*	RootSignature	= nullptr;
		RHIPipelineState*	PSO				= nullptr;

		RHITexture			HeadsOfLists;
		RHIStructuredBuffer	LinkedLists;
	} mLinkedListsFirstPass;

	struct LinkedListsSecondPass
	{
		void Release();

		RHIRootSignature*	RootSignature	= nullptr;
		RHIPipelineState*	PSO				= nullptr;
	} mLinkedListsSecondPass;

protected:
	enum class Method
	{
		WeightedBlended = 0,
		LinkedLists,
		Count
	};

	void SetMethod(const Method method);

protected:
	int							mMethod;
	RenderMethod				mRenderMethod;
	bool						mIsRandomize;

	std::vector<StaticObject>	mOpaqueObjects;
	std::vector<StaticObject>	mTransparentObjects;
};
