#include <StdafxSamples.h>
#include "SSAOSample.h"
#include <Application.h>
#include <Render/GraphicsCore.h>
#include <PSOEditor.h>
#include <RootSignature.h>
#include <StaticGeometryBuilder.h>

#include <ECS/MeshComponent.h>
#include <ECS/SceneComponent.h>

#include <D3D12/private/DXMathHelper.h>
using namespace DirectX;
using namespace GraphicsCore;

namespace
{
	enum class GeometryPassRootSignature
	{
		PerFrame = 0,
		PerObject,
		Count
	};

	enum class SSAOPassRootSignature
	{
		PerFrame = 0,
		Kernel,
		Settings,
		Noise,
		ColorTexture,
		NormalTexture,
		DepthTexture,
		Count
	};

	enum class BlurPassRootSignature
	{
		Settings = 0,
		GaussKernel,
		ColorTexture,
		SSAOTexture,
		Count
	};
}

/**
 * Function : Release
 */
void SSAOSample::Room::Release()
{
	for (auto& object : Objects)
		object.Release();
}

/**
 * Function : Release
 */
void SSAOSample::SSAOPass::Release()
{
	RHISafeRelease(RootSignature);
	RHISafeRelease(PSO);

	Result.Release();

	KernelBuffer.Release();
	NoiseTexture.Release();
	Params.Release();
}

/**
 * Function : Release
 */
void SSAOSample::Release()
{
	Sample::Release();
	mRoom.Release();

	mFullScreenQuad.Release();

	mGPass.Release();
	mSSAOPass.Release();
	mBlurPass.Release();
}

/**
 * Function : Initialize
 */
void SSAOSample::Initialize()
{
	Sample::Initialize();
	mScreenQuad->SetTexture(mGPass.RenderTarget(0).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);

	mCamera->SetPosition(XMFLOAT4(15.0f, 0.0f, 5.0f, 1.0f));
	mCamera->SetZRange(XMFLOAT2(0.1f, 30.0f));
	mCamera->Angles() = XMFLOAT2(XM_PI, -XM_PIDIV4 * 0.3f);
}
namespace
{
	float lerp(const float low, const float high, const float value)
	{
		return low + value * (high - low);
	}

	std::vector<XMFLOAT4> GenerateNoise(const int noiseSize)
	{
		std::mt19937 uid(time(NULL));
		std::uniform_real_distribution<float> rnd(0.0f, 1.0f);

		std::vector<XMFLOAT4> data(noiseSize * noiseSize);
		XMVECTOR vector;
		for (int y = 0; y < noiseSize; ++y)
		{
			for (int x = 0; x < noiseSize; ++x)
			{
				vector = XMVectorSet(rnd(uid), rnd(uid), 0.0f, 1.0f);
				vector = XMVector3Normalize(vector);

				XMStoreFloat4(&data[x + y * noiseSize], vector);
			}
		}

		return data;
	}

	float GaussDistribution(const float sigma, const int x, const int y)
	{
		const float sigmaPow2mul2 = sigma * sigma * 2.0f;
		return expf(-(x * x + y * y) / sigmaPow2mul2) / (XM_PI * sigmaPow2mul2);
	}

	void GenerateGaussian(const float sigma, const int rows, const int columns, ConstantBuffers::SSAO::GaussKernel& kernel)
	{
		const int rowsDiv2		= rows / 2;
		const int columnsDiv2	= columns / 2;

		int y, x;
		float sum = 0.0f;
		for (y = 0; y < rows; ++y)
		{
			for (x = 0; x < columns; ++x)
			{
				float gauss = GaussDistribution(sigma, x - columnsDiv2, y - rowsDiv2);
				kernel.Values[x + y * rows].x = gauss;

				sum += gauss;
			}
		}

		for (y = 0; y < rows; y++)
		{
			for (x = 0; x < columns; ++x)
			{
				kernel.Values[x + y * rows].x /= sum;
			}
		}
	}
}

/**
 * Function : InitializeRenderingResources
 */
void SSAOSample::InitializeRenderingResources()
{
	mRoom.Initialize(mIsRandomScene);

	CreateRenderTargets();

	// Create the screen quad.
	StaticGeometryBuilder::BuildFullScreenQuad(&mFullScreenQuad, __TEXT("SSAO:ScreenQuad"));

	ConstantBuffers::SSAO::Params& params = mSSAOPass.Params.Data();
	mSSAOPass.Params.Data().ScreenWidth() = mApp->GetWindow()->Width();
	mSSAOPass.Params.Data().ScreenHeight() = mApp->GetWindow()->Height();
	mSSAOPass.Initialize();

	// Create a constant buffer for gauss filter.
	ConstantBuffers::SSAO::GaussKernel& gaussKernel = mBlurPass.GaussKernelCB.Data();
	GenerateGaussian(mBlurPass.Sigma, params.NoiseSize(), params.NoiseSize(), gaussKernel);
	mBlurPass.GaussKernelCB.Initialize(__TEXT("SSAO:GaussKernel"), AlignTo256Bytes(sizeof(gaussKernel)), 1, sizeof(gaussKernel), &mBlurPass.GaussKernelCB.Data());

	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, false);
}

/**
 * Function : CreateRenderTargets
 */
void SSAOSample::CreateRenderTargets()
{
	RHISwapChainDesc scDesc = {};
	gRenderer->SwapChain()->GetDesc(&scDesc);

	RHIFormat formats[]				= { RHIFormat::R8G8B8A8UNorm, RHIFormat::R16G16Float };
	MRTPassDesc desc				= {};
	desc.NumRT						= _countof(formats);
	desc.DepthStencil.ViewFormat	= gRenderer->DepthStencilViewFormat(); // ToDo: Describe DepthStencil.
	desc.InputLayout				= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
	desc.BlendDesc					= RHIBlendDesc(RHIDefault());

	for (int i = 0; i < _countof(formats); ++i)
	{
		RHIResourceDesc& resDesc	= desc.Descs[i];
		resDesc.Dimension			= RHIResourceDimension::Texture2D;
		resDesc.Format				= formats[i];
		resDesc.Width				= scDesc.BufferDesc.Width;
		resDesc.Height				= scDesc.BufferDesc.Height;
		resDesc.DepthOrArraySize	= 1;
		resDesc.MipLevels			= 1;
		resDesc.SampleDesc.Count	= 1;
		resDesc.Layout				= RHITextureLayout::Unknown;
		resDesc.Flags				= RHIResourceFlag::AllowRenderTarget;

		{
			RHIClearValue& clearValue	= desc.ClearValues[i];
			clearValue.Format			= formats[i];
			clearValue.Color[0]			= 0.0f;
			clearValue.Color[1]			= 0.0f;
			clearValue.Color[2]			= 0.0f;
			clearValue.Color[3]			= 0.0f;
		}
	}

	mGPass.Initialize(desc);
}

/**
 * Function : Initialize
 */
void SSAOSample::Room::Initialize(const bool isRandom)
{
	for (auto& object : Objects)
		object.Initialize();

	// Create walls.
	{
		for (int i = 0; i < ToInt(ID::Cube1); ++i)
			StaticGeometryBuilder::LoadPlane(Objects[i].GetMeshComponent(), Objects[i].GetSceneComponent());

		const float scale = 7.0f;
		StaticObject& floor = Objects[ToInt(ID::Floor)];
		floor.GetSceneComponent()->SetScale(MakeFloat3(scale));
		floor.GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, 0.0f, 0.0f));

		StaticObject& ceiling = Objects[ToInt(ID::Ceiling)];
		ceiling.GetSceneComponent()->SetScale(MakeFloat3(scale));
		ceiling.GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, 0.0f, 7.0f));
		ceiling.GetSceneComponent()->AddRotateToPitch(XM_PI);

		StaticObject& backWall = Objects[ToInt(ID::BackWall)];
		backWall.GetSceneComponent()->SetScale(XMFLOAT3(scale * 0.5f, scale, 1.0f));
		backWall.GetSceneComponent()->SetPosition(XMFLOAT3(-scale * 0.5f, 0.0f, scale * 0.5f));
		backWall.GetSceneComponent()->AddRotateToYaw(XM_PIDIV2);

		StaticObject& leftWall = Objects[ToInt(ID::LeftWall)];
		leftWall.GetSceneComponent()->SetScale(XMFLOAT3(scale, scale * 0.5f, 1.0f));
		leftWall.GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, -scale, scale * 0.5f));
		leftWall.GetSceneComponent()->AddRotateToPitch(XM_PIDIV2);

		StaticObject& rightWall = Objects[ToInt(ID::RightWall)];
		rightWall.GetSceneComponent()->SetScale(XMFLOAT3(scale, scale * 0.5f, 1.0f));
		rightWall.GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, scale, scale * 0.5f));
		rightWall.GetSceneComponent()->AddRotateToPitch(-XM_PIDIV2);
	}

	// Create cubes.
	if (isRandom)
	{

	}
	else
	{
		for (int i = ToInt(ID::Cube1); i < ToInt(ID::Count); ++i)
			StaticGeometryBuilder::LoadCube(Objects[i].GetMeshComponent(), Objects[i].GetSceneComponent());

		StaticObject& cube1 = Objects[ToInt(ID::Cube1)];
		cube1.GetSceneComponent()->SetScale(MakeFloat3(2.0f));
		cube1.GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, -2.0f, 1.5f));
		cube1.GetSceneComponent()->AddRotateToPitch(XM_PIDIV4 * 0.3f);

		StaticObject& cube2 = Objects[ToInt(ID::Cube2)];
		cube2.GetSceneComponent()->SetScale(MakeFloat3(2.0f));
		cube2.GetSceneComponent()->SetPosition(XMFLOAT3(1.0f, -3.0f, 2.0f));
		cube2.GetSceneComponent()->AddRotateToRoll(XM_PIDIV2 * 0.7f);

		StaticObject& cube3 = Objects[ToInt(ID::Cube3)];
		cube3.GetSceneComponent()->SetScale(MakeFloat3(2.3f));
		cube3.GetSceneComponent()->SetPosition(XMFLOAT3(1.0f, -5.0f, 1.5f));
		cube3.GetSceneComponent()->AddRotateToYaw(XM_PIDIV2 * 0.54f);
		cube3.GetSceneComponent()->AddRotateToPitch(-XM_PI * 0.14f);

		StaticObject& cube4 = Objects[ToInt(ID::Cube4)];
		cube4.GetSceneComponent()->SetScale(MakeFloat3(3.0f));
		cube4.GetSceneComponent()->SetPosition(XMFLOAT3(-0.7f, -2.5f, 3.2f));
		cube4.GetSceneComponent()->AddRotateToRoll(XM_PI * 0.2f);
		cube4.GetSceneComponent()->AddRotateToPitch(XM_PIDIV4);

		StaticObject& cube5 = Objects[ToInt(ID::Cube5)];
		cube5.GetSceneComponent()->SetScale(MakeFloat3(1.5f));
		cube5.GetSceneComponent()->SetPosition(XMFLOAT3(1.0f, -1.0f, 0.5f));
		cube5.GetSceneComponent()->AddRotateToRoll(-XM_PIDIV4);
		cube5.GetSceneComponent()->AddRotateToYaw(XM_PIDIV4 * 0.3f);

	}
}

/**
 * Function : Initialize
 */
void SSAOSample::SSAOPass::Initialize()
{
	ConstantBuffers::SSAO::Params& params = Params.Data();
	Params.Initialize(__TEXT("SSAO:Params"), RHIUtils::GetConstBufferSize(), 1, sizeof(params), &params);

	// Create a render target.
	{
		RHIResourceDesc desc	= {};
		desc.Dimension			= RHIResourceDimension::Texture2D;
		desc.Format				= RtFormat;
		desc.Width				= params.ScreenWidth();
		desc.Height				= params.ScreenHeight();
		desc.MipLevels			= 1;
		desc.DepthOrArraySize	= 1;
		desc.SampleDesc.Count	= 1;
		desc.Flags				= RHIResourceFlag::AllowRenderTarget;

		RHIClearValue clearValue	= {};
		clearValue.Format			= RtFormat;
		clearValue.Color[0] = clearValue.Color[1] = clearValue.Color[2] = clearValue.Color[3] = 0.0f;

		Result.Initialize(&desc, &clearValue);

		GraphicsCore::gRenderer->ChangeResourceBarrier(1, &RHIResourceBarrier::MakeTransition(Result.Texture(), RHIResourceState::Common, RHIResourceState::PixelShaderResource));
	}

	// Create the kernel buffer.
	{
		KernelBuffer.Initialize(__TEXT("SSAO:KernelBuffer"), AlignTo256Bytes(sizeof(KernelBuffer.Data())), 1, sizeof(KernelBuffer.Data()));
		FillKernel(params.KernelSize(), KernelBuffer.Data());
		KernelBuffer.UpdateDataOnGPU();
	}

	// Initialize the noise texture.
	FillNoise(params.NoiseSize(), NoiseTexture);
	GraphicsCore::gRenderer->ChangeResourceBarrier(1, &RHIResourceBarrier::MakeTransition(NoiseTexture.Texture(), RHIResourceState::CopyDest, RHIResourceState::PixelShaderResource));
}

/**
 * Function : FillKernel
 */
void SSAOSample::SSAOPass::FillKernel(const int kernelSize, ConstantBuffers::SSAO::Kernel& kernel)
{
	std::mt19937 uid(time(NULL));
	std::uniform_real_distribution<float> rnd(-1.0f, 1.0f);

	XMVECTOR vector;
	for (int i = 0; i < kernelSize; ++i)
	{
		vector = XMVectorSet(rnd(uid), rnd(uid), rnd(uid) * 0.5f + 0.5f, 0.0f);
		vector = XMVector3Normalize(vector);

		float factor = i / (float)kernelSize;
		vector *= rnd(uid) * 0.5f + 0.5f;
		vector *= lerp(0.1f, 1.0f, factor);

		XMStoreFloat4(&kernel.Vectors[i], vector);
	}
}

/**
 * Function : FillNoise
 */
void SSAOSample::SSAOPass::FillNoise(const int noiseSize, RHITexture& noise)
{
	bool release = false;
	if (noise.Texture() != nullptr)
	{
		RHIResourceDesc desc = noise.Texture()->GetDesc();
		if (desc.Width != noiseSize || desc.Height != noiseSize)
			release = true;
	}

	if (release)
		noise.Release();

	RHIResourceDesc desc	= {};
	desc.Dimension			= RHIResourceDimension::Texture2D;
	desc.Format				= RHIFormat::R32G32B32A32Float;
	desc.MipLevels			= 1;
	desc.Width				= noiseSize;
	desc.Height				= noiseSize;
	desc.Flags				= RHIResourceFlag::None;
	desc.DepthOrArraySize	= 1;
	desc.SampleDesc.Count	= 1;
	desc.SampleDesc.Quality	= 0;

	std::vector<XMFLOAT4> data	= GenerateNoise(noiseSize);
	RHISubresourceData subRes	= {};
	subRes.Data					= data.data();
	subRes.RowPitch				= noiseSize * sizeof(XMFLOAT4);
	subRes.SlicePitch			= noiseSize * subRes.RowPitch;

	noise.Initialize(&desc, nullptr, &subRes);
	noise.Texture()->SetName(__TEXT("SSAOPass:NoiseTexture"));
}

/**
 * Function : CreateRootSignatures
 */
void SSAOSample::CreateRootSignatures()
{
	// Geometry pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(GeometryPassRootSignature::Count));
		rootSignature[ToInt(GeometryPassRootSignature::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(GeometryPassRootSignature::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);

		mGPass.SetRootSignature(rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, false, true)));
	}

	// SSAO pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(SSAOPassRootSignature::Count), 2);
		rootSignature[ToInt(SSAOPassRootSignature::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(SSAOPassRootSignature::Kernel)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
		rootSignature[ToInt(SSAOPassRootSignature::Settings)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 2);

		rootSignature[ToInt(SSAOPassRootSignature::ColorTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);
		rootSignature[ToInt(SSAOPassRootSignature::NormalTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 1);
		rootSignature[ToInt(SSAOPassRootSignature::DepthTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 2);
		rootSignature[ToInt(SSAOPassRootSignature::Noise)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 3);

		RHIStaticSamplerDesc& samplerDesc0	= rootSignature.SamplerDesc(0);
		samplerDesc0.Filter					= RHIFilter::MinMagMipLinear; //RHIFilter::ComparisonMinMagMipLinear;
		// We require a clamp based sampler when sampling the depth buffer so that it doesn't wrap around and sample incorrect information.
		samplerDesc0.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc0.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc0.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc0.ComparisonFunc			= RHIComparisonFunc::Never; //RHIComparisonFunc::LEqual;
		samplerDesc0.BorderColor			= RHIStaticBorderColor::TransparentBlack;
		samplerDesc0.ShaderRegister			= 0;

		RHIStaticSamplerDesc& samplerDesc1	= rootSignature.SamplerDesc(1);
		samplerDesc1.Filter					= RHIFilter::MinMagMipLinear;
		samplerDesc1.AddressU				= RHITextureAddressMode::Mirror;
		samplerDesc1.AddressV				= RHITextureAddressMode::Mirror;
		samplerDesc1.AddressW				= RHITextureAddressMode::Mirror;
		samplerDesc1.ShaderRegister			= 1;

		mSSAOPass.RootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}

	// Blur pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(BlurPassRootSignature::Count), 1);
		rootSignature[ToInt(BlurPassRootSignature::GaussKernel)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(BlurPassRootSignature::Settings)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 2);
		rootSignature[ToInt(BlurPassRootSignature::ColorTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);
		rootSignature[ToInt(BlurPassRootSignature::SSAOTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 1);

		RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(0);
		samplerDesc.Filter					= RHIFilter::MinMagMipLinear;
		samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc.ShaderRegister			= 0;

		mBlurPass.RootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}
}

/**
 * Function : CreatePipelineStates
 */
void SSAOSample::CreatePipelineStates()
{
	// Geometry pass.
	{
		RHIGraphicsPipelineStateDesc psoDesc			= {};

		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= true;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable			= false;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise	= true;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;

		RHIUtils::Fill::GraphicsPipelineStateDesc(&mGPass, &psoDesc);

		RHIUtils::Fill::ShadersArray shaders;
		shaders.Push(__TEXT("SSAO:GeometryPass.vs"), RHIShaderType::Vertex);
		shaders.Push(__TEXT("SSAO:GeometryPass.ps"), RHIShaderType::Pixel);
		RHIUtils::Fill::GraphicsPipelineStateDesc(shaders, &psoDesc);

		mGPass.SetPSO(gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc));

		RHIPipelineState* GPassPso = mGPass.PSO();
		mApp->GetPSOEditor()->RegisterPSO(Name(), __TEXT("Geometry pass"), &GPassPso, RHIPipelineStateStream(psoDesc)).AttachShaders(shaders);
	}

	// SSAO pass.
	{
		RHIGraphicsPipelineStateDesc psoDesc			= {};
		psoDesc.RootSignature							= mSSAOPass.RootSignature;
		psoDesc.InputLayout								= { InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc, _countof(InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc) };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= mSSAOPass.RtFormat;
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= false;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode				= RHICullMode::Back;
		psoDesc.RasterizerState.FrontCounterClockwise	= true;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= TRUE;
		psoDesc.BlendState.SetBlendFunc(0, RHIBlend::SrcAlpha, RHIBlend::InvSrcAlpha);

		RHIUtils::Fill::ShadersArray shaders;
		shaders.Push(__TEXT("SSAO:SSAOPass.vs"), RHIShaderType::Vertex);
		shaders.Push(__TEXT("SSAO:SSAOPass.ps"), RHIShaderType::Pixel);
		RHIUtils::Fill::GraphicsPipelineStateDesc(shaders, &psoDesc);

		mSSAOPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);

		mApp->GetPSOEditor()->RegisterPSO(Name(), __TEXT("SSAO pass"), &mSSAOPass.PSO, RHIPipelineStateStream(psoDesc)).AttachShaders(shaders);
	}

	// Blur pass.
	{
		RHIGraphicsPipelineStateDesc psoDesc			= {};
		psoDesc.RootSignature							= mBlurPass.RootSignature;
		psoDesc.InputLayout								= { InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc, _countof(InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc) };

		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= false;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode				= RHICullMode::Back;
		psoDesc.RasterizerState.FrontCounterClockwise	= true;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= TRUE;
		psoDesc.BlendState.SetBlendFunc(0, RHIBlend::SrcAlpha, RHIBlend::InvSrcAlpha);

		RHIUtils::Fill::ShadersArray shaders;
		shaders.Push(__TEXT("SSAO:BlurPass.vs"), RHIShaderType::Vertex);
		shaders.Push(__TEXT("SSAO:BlurPass.ps"), RHIShaderType::Pixel);
		RHIUtils::Fill::GraphicsPipelineStateDesc(shaders, &psoDesc);

		mBlurPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);

		mApp->GetPSOEditor()->RegisterPSO(Name(), __TEXT("Blur pass"), &mBlurPass.PSO, RHIPipelineStateStream(psoDesc)).AttachShaders(shaders);
	}
}

/**
 * Function : RenderImGui
 */
void SSAOSample::RenderImGui()
{
	ConstantBuffers::SSAO::Params& params = mSSAOPass.Params.Data();

	ImGui::Text("Screen space ambient occlusion Sample");
	if (ImGui::SliderInt("Kernel size", &params.KernelSize(), ConstantBuffers::SSAO::kMinKernelSize, ConstantBuffers::SSAO::kMaxKernelSize))
	{
		mSSAOPass.FillKernel(params.KernelSize(), mSSAOPass.KernelBuffer.Data());
		mSSAOPass.KernelBuffer.MarkDirty();
	}

	if (ImGui::SliderInt("Noize size", &params.NoiseSize(), ConstantBuffers::SSAO::kMinNoiseSize, ConstantBuffers::SSAO::kMaxNoiseSize))
		mSSAOPass.FillNoise(params.NoiseSize(), mSSAOPass.NoiseTexture);

	if (ImGui::SliderFloat("SSAO Radius", &params.Radius(), 0.1f, 5.0f))
		mSSAOPass.Params.UpdateDataOnGPU();

	if (ImGui::SliderFloat("SSAO Power", &params.Power(), 0.1f, 5.0f))
		mSSAOPass.Params.UpdateDataOnGPU();

	//if (ImGui::SliderFloat("SSAO Scale", &params.Scale(), 0.1f, 5.0f))
	//	mSSAOPass.Params.UpdateDataOnGPU();

	if (ImGui::SliderFloat("SSAO Bias", &params.Bias(), 0.1f, 5.0f))
		mSSAOPass.Params.UpdateDataOnGPU();

	if (ImGui::SliderFloat("Gauss sigma", &mBlurPass.Sigma, 1.0f, 10.0f))
	{
		GenerateGaussian(mBlurPass.Sigma, params.NoiseSize(), params.NoiseSize(), mBlurPass.GaussKernelCB.Data());
		mBlurPass.GaussKernelCB.MarkDirty();
	}

	ImGui::Separator();
	ImGui::Checkbox("Show texture", &mGPass.ShowTexture());
	if (mGPass.ShowTexture())
	{
		ImGui::Text("Show a render target:");
		if (ImGui::RadioButton("Diffuse color", &mGPass.ShowTextureId(), 0))
			mScreenQuad->SetTexture(mGPass.RenderTarget(0).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);

		if (ImGui::RadioButton("Normals", &mGPass.ShowTextureId(), 1))
			mScreenQuad->SetTexture(mGPass.RenderTarget(1).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);

		if (ImGui::RadioButton("Depth", &mGPass.ShowTextureId(), mGPass.NumRT()))
			mScreenQuad->SetTexture(gRenderer->CurrentDepthStencilGpuHandle(), ScreenQuadPart::TextureType::Depth);

		if (ImGui::RadioButton("Noise", &mGPass.ShowTextureId(), mGPass.NumRT() + 1))
			mScreenQuad->SetTexture(mSSAOPass.NoiseTexture.SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);

		if (ImGui::RadioButton("SSAO result", &mGPass.ShowTextureId(), mGPass.NumRT() + 2))
			mScreenQuad->SetTexture(mSSAOPass.Result.SrvHandle().Gpu(), ScreenQuadPart::TextureType::Depth);
	}
}

/**
 * Function : Update
 */
void SSAOSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	if (mSSAOPass.KernelBuffer.IsDirty())
		mSSAOPass.KernelBuffer.UpdateDataOnGPU();

	if (mBlurPass.GaussKernelCB.IsDirty())
		mBlurPass.GaussKernelCB.UpdateDataOnGPU();

	for (auto& object : mRoom.Objects)
		object.Update(deltaTime);
}

/**
 * Function : Render
 */
void SSAOSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };

	gRenderer->CommandAllocator()->Reset();
	commandList->Reset(gRenderer->CommandAllocator(), nullptr);

	// Geometry pass.
	{
		RHIResourceBarrier startBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(0).Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(1).Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthWrite),
		};

		RHIResourceBarrier stopBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(0).Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(1).Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present)
		};
		commandList->ResourceBarrier(_countof(startBarriers), startBarriers);
		commandList->OMSetRenderTargets(mGPass.NumRT(), mGPass.RtvCpuHandles(), false, &gRenderer->CurrentDepthStencilCpuHandle());

		for (int i = 0; i < mGPass.NumRT(); ++i)
			commandList->ClearRenderTargetView(mGPass.RenderTarget(i).RtvHandle().Cpu(), mGPass.ClearValue(i).Color, 0, nullptr);
		commandList->ClearDepthStencilView(gRenderer->CurrentDepthStencilCpuHandle(), RHIClearFlags::Depth | RHIClearFlags::Stencil, 1.0f, 0, 0, nullptr);

		commandList->SetPipelineState(mGPass.PSO());
		commandList->SetGraphicsRootSignature(mGPass.RootSignature());
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		commandList->SetGraphicsRootDescriptorTable(ToInt(GeometryPassRootSignature::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());

		for (auto& object : mRoom.Objects)
			object.Draw(commandList, ToInt(GeometryPassRootSignature::PerObject));

		commandList->ResourceBarrier(_countof(stopBarriers), stopBarriers);
	}

	// SSAO pass.
	{
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mSSAOPass.Result.Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget));

		float color[] = { 0.0f, 0.0f, 0.0f, 0.0f };
		commandList->OMSetRenderTargets(1, &mSSAOPass.Result.RtvHandle().Cpu(), false, nullptr);
		commandList->ClearRenderTargetView(mSSAOPass.Result.RtvHandle().Cpu(), color, 0, nullptr);

		commandList->SetPipelineState(mSSAOPass.PSO);
		commandList->SetGraphicsRootSignature(mSSAOPass.RootSignature);

		commandList->SetGraphicsRootDescriptorTable(ToInt(SSAOPassRootSignature::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SSAOPassRootSignature::Kernel), mSSAOPass.KernelBuffer.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SSAOPassRootSignature::Settings), mSSAOPass.Params.SrvHandle().Gpu());

		commandList->SetGraphicsRootDescriptorTable(ToInt(SSAOPassRootSignature::ColorTexture), mGPass.RenderTarget(0).SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SSAOPassRootSignature::NormalTexture), mGPass.RenderTarget(1).SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SSAOPassRootSignature::DepthTexture), gRenderer->CurrentDepthStencilGpuHandle());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SSAOPassRootSignature::Noise), mSSAOPass.NoiseTexture.SrvHandle().Gpu());

		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
		commandList->IASetVertexBuffers(0, 1, mFullScreenQuad.View());
		commandList->DrawInstanced(mFullScreenQuad.NumElements(), 1, 0, 0);

		RHIResourceBarrier barriers[] =
		{
			RHIResourceBarrier::MakeTransition(mSSAOPass.Result.Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget)
		};
		commandList->ResourceBarrier(_countof(barriers), barriers);
	}

	// Blur pass.
	{
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);
		commandList->ClearRenderTargetView(gRenderer->CurrentRenderTargetCpuHandle(), &mBGColor.x, 0, nullptr);

		commandList->SetPipelineState(mBlurPass.PSO);
		commandList->SetGraphicsRootSignature(mBlurPass.RootSignature);

		commandList->SetGraphicsRootDescriptorTable(ToInt(BlurPassRootSignature::GaussKernel), mBlurPass.GaussKernelCB.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(BlurPassRootSignature::Settings), mSSAOPass.Params.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(BlurPassRootSignature::ColorTexture), mGPass.RenderTarget(0).SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(BlurPassRootSignature::SSAOTexture), mSSAOPass.Result.SrvHandle().Gpu());

		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
		commandList->IASetVertexBuffers(0, 1, mFullScreenQuad.View());
		commandList->DrawInstanced(mFullScreenQuad.NumElements(), 1, 0, 0);
	}

	//
	if (mGPass.ShowTexture() && mUseScreenQuad)
	{
		DrawScreenQuad(commandList, heaps, _countof(heaps));

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
	}

	gRenderer->CommitCommandList(commandList);
}
