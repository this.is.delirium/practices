#include <StdafxSamples.h>
#include "SSLRSample.h"
#include <Application.h>
#include <ECS/MeshComponent.h>
#include <ECS/SceneComponent.h>
#include <FBXLoader.h>
#include <PSOEditor.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RootSignature.h>
#include <StaticGeometryBuilder.h>

#include <D3D12/private/DXMathHelper.h>
using namespace DirectX;
using namespace GraphicsCore;

namespace
{
	enum class GeometryPassRootSignature
	{
		PerFrame = 0,
		PerObject,
		Count
	};

	enum class SSLRPassRootSignature
	{
		PerFrame = 0,
		Params,
		ColorTexture,
		NormalTexture,
		DepthTeture,
		Count
	};
}

/**
 * Function : Release
 */
void SSLRSample::Room::Release()
{
	for (auto& object : Objects)
		object.Release();
}

/**
 * Function : Release
 */
void SSLRSample::SSLRPass::Release()
{
	RHISafeRelease(RootSignature);
	RHISafeRelease(PSO);
	FullScreenQuad.Release();
	Params.Release();
}

/**
 * Function : Release
 */
void SSLRSample::Release()
{
	Sample::Release();

	mRoom.Release();

	mGPass.Release();
}

/**
 * Function : Initialize
 */
void SSLRSample::Initialize()
{
	Sample::Initialize();
	mScreenQuad->SetTexture(mGPass.RenderTarget(mGPass.ShowTextureId()).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);

	mCamera->SetPosition(XMFLOAT4(15.0f, 0.0f, 5.0f, 1.0f));
	mCamera->SetZRange(XMFLOAT2(0.1f, 30.0f));
	mCamera->Angles() = XMFLOAT2(XM_PI, -XM_PIDIV4 * 0.3f);
}

/**
 * Function : InitializeRenderingResources
 */
void SSLRSample::InitializeRenderingResources()
{
	mRoom.Initialize(gRenderer, false);

	CreateRenderTargets();

	StaticGeometryBuilder::BuildFullScreenQuad(&mSSLRPass.FullScreenQuad, __TEXT("SSLR:FullScreenQuad"));

	mSSLRPass.Params.Initialize(__TEXT("SSLR:Params"), RHIUtils::GetConstBufferSize(), 1, sizeof(mSSLRPass.Params.Data()), &mSSLRPass.Params.Data());

	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, false);
}

/**
 * Function : Initialize
 */
void SSLRSample::Room::Initialize(IRenderer* renderer, const bool isRandom)
{
	for (int i = 0; i < ToInt(ID::Count); ++i)
		Objects[i].Initialize();

	// Create walls.
	{
		for (int i = 0; i < ToInt(ID::Cube1); ++i)
			StaticGeometryBuilder::LoadPlane(Objects[i].GetMeshComponent(), Objects[i].GetSceneComponent());

		const float scale = 7.0f;
		StaticObject& floor = Objects[ToInt(ID::Floor)];
		floor.GetSceneComponent()->SetScale(MakeFloat3(scale));
		floor.GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, 0.0f, 0.0f));

		StaticObject& ceiling = Objects[ToInt(ID::Ceiling)];
		ceiling.GetSceneComponent()->SetScale(MakeFloat3(scale));
		ceiling.GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, 0.0f, 7.0f));
		ceiling.GetSceneComponent()->AddRotateToPitch(XM_PI);

		StaticObject& backWall = Objects[ToInt(ID::BackWall)];
		backWall.GetSceneComponent()->SetScale(XMFLOAT3(scale * 0.5f, scale, 1.0f));
		backWall.GetSceneComponent()->SetPosition(XMFLOAT3(-scale * 0.5f, 0.0f, scale * 0.5f));
		backWall.GetSceneComponent()->AddRotateToYaw(XM_PIDIV2);

		StaticObject& leftWall = Objects[ToInt(ID::LeftWall)];
		leftWall.GetSceneComponent()->SetScale(XMFLOAT3(scale, scale * 0.5f, 1.0f));
		leftWall.GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, -scale, scale * 0.5f));
		leftWall.GetSceneComponent()->AddRotateToPitch(-XM_PIDIV2);

		StaticObject& rightWall = Objects[ToInt(ID::RightWall)];
		rightWall.GetSceneComponent()->SetScale(XMFLOAT3(scale, scale * 0.5f, 1.0f));
		rightWall.GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, scale, scale * 0.5f));
		rightWall.GetSceneComponent()->AddRotateToPitch(XM_PIDIV2);
	}

	// Create cubes.
	if (isRandom)
	{

	}
	else
	{
		for (int i = ToInt(ID::Cube1); i < ToInt(ID::Count); ++i)
			StaticGeometryBuilder::LoadCube(Objects[i].GetMeshComponent(), Objects[i].GetSceneComponent());

		StaticObject& cube1 = Objects[ToInt(ID::Cube1)];
		cube1.GetSceneComponent()->SetScale(MakeFloat3(2.0f));
		cube1.GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, -2.0f, 1.5f));
		cube1.GetSceneComponent()->AddRotateToPitch(XM_PIDIV4 * 0.3f);

		StaticObject& cube2 = Objects[ToInt(ID::Cube2)];
		cube2.GetSceneComponent()->SetScale(MakeFloat3(2.0f));
		cube2.GetSceneComponent()->SetPosition(XMFLOAT3(1.0f, -3.0f, 2.0f));
		cube2.GetSceneComponent()->AddRotateToRoll(XM_PIDIV2 * 0.7f);

		StaticObject& cube3 = Objects[ToInt(ID::Cube3)];
		cube3.GetSceneComponent()->SetScale(MakeFloat3(2.3f));
		cube3.GetSceneComponent()->SetPosition(XMFLOAT3(1.0f, -5.0f, 1.5f));
		cube3.GetSceneComponent()->AddRotateToYaw(XM_PIDIV2 * 0.54f);
		cube3.GetSceneComponent()->AddRotateToPitch(-XM_PI * 0.14f);

		FbxLoader loader;
		loader.LoadFile(SResourceManager::Instance().Find(__TEXT("Samplels:teapot.fbx")));

		StaticObject& teapot1 = Objects[ToInt(ID::Teapot1)];
		loader.FillMeshComponent(0, teapot1.GetMeshComponent());
		teapot1.GetSceneComponent()->SetScale(MakeFloat3(0.03f));
		teapot1.GetSceneComponent()->SetPosition(XMFLOAT3(-3.0f, 2.0f, 0.0f));
		teapot1.GetSceneComponent()->AddRotateToPitch(XM_PIDIV2);

		StaticObject& teapot2 = Objects[ToInt(ID::Teapot2)];
		loader.FillMeshComponent(0, teapot2.GetMeshComponent());
		teapot2.GetSceneComponent()->SetScale(MakeFloat3(0.03f));
		teapot2.GetSceneComponent()->SetPosition(XMFLOAT3(-1.0f, 4.0f, 0.0f));
		teapot2.GetSceneComponent()->AddRotateToPitch(-3.044f);
		teapot2.GetSceneComponent()->AddRotateToRoll(-4.469f);
		teapot2.GetSceneComponent()->AddRotateToYaw(2.332f);

		/*loader.LoadFile(SResourceManager::Instance().Find(__TEXT("Samplels:Sphere.fbx")));
		StaticObject& sphere1 = Objects[ToInt(ID::Sphere1)];
		loader.FillMeshComponent(sphere1.GetMeshComponent());
		sphere1.GetSceneComponent()->SetScale(MakeFloat3(0.7f));
		sphere1.GetSceneComponent()->SetPosition(XMFLOAT3(-1.0f, 3.0f, 0.0f));*/
	}
}

/**
 * Function : CreateRenderTargets
 */
void SSLRSample::CreateRenderTargets()
{
	RHISwapChainDesc scDesc;
	gRenderer->SwapChain()->GetDesc(&scDesc);

	// Color, Normal
	RHIFormat formats[]				= { RHIFormat::R8G8B8A8UNorm, RHIFormat::R16G16Float };
	MRTPassDesc mrtDesc				= {};
	mrtDesc.NumRT					= _countof(formats);
	mrtDesc.DepthStencil.ViewFormat	= gRenderer->DepthStencilViewFormat(); // ToDo: Describe DepthStencil.
	mrtDesc.InputLayout				= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
	mrtDesc.BlendDesc				= RHIBlendDesc(RHIDefault());

	for (UINT i = 0; i < mrtDesc.NumRT; ++i)
	{
		RHIResourceDesc& resDesc	= mrtDesc.Descs[i];
		resDesc.Dimension			= RHIResourceDimension::Texture2D;
		resDesc.Format				= formats[i];
		resDesc.Width				= scDesc.BufferDesc.Width;
		resDesc.Height				= scDesc.BufferDesc.Height;
		resDesc.DepthOrArraySize	= 1;
		resDesc.MipLevels			= 1;
		resDesc.SampleDesc.Count	= 1;
		resDesc.Layout				= RHITextureLayout::Unknown;
		resDesc.Flags				= RHIResourceFlag::AllowRenderTarget;

		{
			RHIClearValue& clearValue	= mrtDesc.ClearValues[i];
			clearValue.Format			= formats[i];
			clearValue.Color[0]			= 0.0f;
			clearValue.Color[1]			= 0.0f;
			clearValue.Color[2]			= 0.0f;
			clearValue.Color[3]			= 0.0f;
		}
	}

	mGPass.Initialize(mrtDesc);
}

/**
 * Function : CreateRootSignatures
 */
void SSLRSample::CreateRootSignatures()
{
	// Geometry pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(GeometryPassRootSignature::Count));
		rootSignature[ToInt(GeometryPassRootSignature::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(GeometryPassRootSignature::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);

		mGPass.SetRootSignature(rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false)));
	}

	// SSLR pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(SSLRPassRootSignature::Count), 1);
		rootSignature[ToInt(SSLRPassRootSignature::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(SSLRPassRootSignature::Params)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
		rootSignature[ToInt(SSLRPassRootSignature::ColorTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);
		rootSignature[ToInt(SSLRPassRootSignature::NormalTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 1);
		rootSignature[ToInt(SSLRPassRootSignature::DepthTeture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 2);

		RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(0);
		samplerDesc.Filter					= RHIFilter::MinMagMipLinear;
		samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc.ShaderRegister			= 0;

		mSSLRPass.RootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}
}

/**
 * Function : CreatePipelineStates
 */
void SSLRSample::CreatePipelineStates()
{
	// Geometry pass.
	{
		RHIGraphicsPipelineStateDesc psoDesc			= {};

		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= true;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable			= false;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise	= true;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;

		RHIUtils::Fill::GraphicsPipelineStateDesc(&mGPass, &psoDesc);

		RHIUtils::Fill::ShadersArray shaders;
		shaders.Push(__TEXT("SSLR:GeometryPass.vs"), RHIShaderType::Vertex);
		shaders.Push(__TEXT("SSLR:GeometryPass.ps"), RHIShaderType::Pixel);
		RHIUtils::Fill::GraphicsPipelineStateDesc(shaders, &psoDesc);

		mGPass.SetPSO(gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc));

		RHIPipelineState* GPassPso = mGPass.PSO();
		mApp->GetPSOEditor()->RegisterPSO(Name(), __TEXT("Geometry pass"), &GPassPso, RHIPipelineStateStream(psoDesc)).AttachShaders(shaders);
	}

	// SSLR pass.
	{
		RHIGraphicsPipelineStateDesc psoDesc			= {};
		psoDesc.RootSignature							= mSSLRPass.RootSignature;
		psoDesc.InputLayout								= { InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc, _countof(InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc) };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= false;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode				= RHICullMode::Back;
		psoDesc.RasterizerState.FrontCounterClockwise	= true;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= TRUE;
		psoDesc.BlendState.SetBlendFunc(0, RHIBlend::SrcAlpha, RHIBlend::InvSrcAlpha);

		RHIUtils::Fill::ShadersArray shaders;
		shaders.Push(__TEXT("SSLR:SSLRPass.vs"), RHIShaderType::Vertex);
		shaders.Push(__TEXT("SSLR:SSLRPass.ps"), RHIShaderType::Pixel);
		RHIUtils::Fill::GraphicsPipelineStateDesc(shaders, &psoDesc);

		mSSLRPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);

		mApp->GetPSOEditor()->RegisterPSO(Name(), __TEXT("SSLR pass"), &mSSLRPass.PSO, RHIPipelineStateStream(psoDesc)).AttachShaders(shaders);
	}
}

/**
 * Function : RenderImGui
 */
void SSLRSample::RenderImGui()
{
	ImGui::Text("The implementation of Screen Space Local Reflection.");
	ImGui::Separator();

	/*std::shared_ptr<SceneComponent>& teapot2 = mRoom.Objects[ToInt(Room::ID::Teapot2)].GetSceneComponent();
	if (ImGui::SliderFloat("Roll", &teapot2->Roll(), -XM_2PI, XM_2PI))
		teapot2->MarkDirty();

	if (ImGui::SliderFloat("Yaw", &teapot2->Yaw(), -XM_2PI, XM_2PI))
		teapot2->MarkDirty();

	if (ImGui::SliderFloat("Pitch", &teapot2->Pitch(), -XM_2PI, XM_2PI))
		teapot2->MarkDirty();
	ImGui::Separator();*/

	// SSLR settings.
	{
		using namespace ConstantBuffers::SSLR;
		Params& params = mSSLRPass.Params.Data();
		if (ImGui::SliderFloat("Start L", &params.StartL(), kMinStartL, kMaxStartL))
			mSSLRPass.Params.MarkDirty();

		if (ImGui::SliderFloat("Reflection distance", &params.ReflectionDistance(), kMinReflectionDistance, kMaxReflectionDistance))
			mSSLRPass.Params.MarkDirty();
	}
	ImGui::Separator();

	ImGui::Checkbox("Show texture", &mGPass.ShowTexture());
	if (mGPass.ShowTexture())
	{
		ImGui::Text("Show a render target:");
		if (ImGui::RadioButton("Diffuse color", &mGPass.ShowTextureId(), 0))
			mScreenQuad->SetTexture(mGPass.RenderTarget(0).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);

		if (ImGui::RadioButton("Normals", &mGPass.ShowTextureId(), 1))
			mScreenQuad->SetTexture(mGPass.RenderTarget(1).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);
	}
}

/**
 * Function : Update
 */
void SSLRSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	for (auto& object : mRoom.Objects)
		object.Update(deltaTime);

	if (mSSLRPass.Params.IsDirty())
		mSSLRPass.Params.UpdateDataOnGPU();
}

/**
 * Function : Render
 */
void SSLRSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };

	gRenderer->CommandAllocator()->Reset();
	commandList->Reset(gRenderer->CommandAllocator(), nullptr);

	// Geometry pass.
	{
		RHIResourceBarrier startBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(0).Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(1).Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthWrite),
		};

		RHIResourceBarrier stopBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(0).Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(1).Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget)
		};

		commandList->ResourceBarrier(_countof(startBarriers), startBarriers);
		commandList->OMSetRenderTargets(mGPass.NumRT(), mGPass.RtvCpuHandles(), false, &gRenderer->CurrentDepthStencilCpuHandle());

		for (int i = 0; i < mGPass.NumRT(); ++i)
			commandList->ClearRenderTargetView(mGPass.RenderTarget(i).RtvHandle().Cpu(), mGPass.ClearValue(i).Color, 0, nullptr);
		commandList->ClearDepthStencilView(gRenderer->CurrentDepthStencilCpuHandle(), RHIClearFlags::Depth | RHIClearFlags::Stencil, 1.0f, 0, 0, nullptr);

		commandList->SetPipelineState(mGPass.PSO());
		commandList->SetGraphicsRootSignature(mGPass.RootSignature());
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		commandList->SetGraphicsRootDescriptorTable(ToInt(GeometryPassRootSignature::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());

		for (auto& object : mRoom.Objects)
			object.Draw(commandList, ToInt(GeometryPassRootSignature::PerObject));

		commandList->ResourceBarrier(_countof(stopBarriers), stopBarriers);
	}

	// SSLR pass.
	{
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);
		commandList->ClearRenderTargetView(gRenderer->CurrentRenderTargetCpuHandle(), &mBGColor.x, 0, nullptr);

		commandList->SetPipelineState(mSSLRPass.PSO);
		commandList->SetGraphicsRootSignature(mSSLRPass.RootSignature);

		commandList->SetGraphicsRootDescriptorTable(ToInt(SSLRPassRootSignature::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SSLRPassRootSignature::Params), mSSLRPass.Params.SrvHandle().Gpu());

		commandList->SetGraphicsRootDescriptorTable(ToInt(SSLRPassRootSignature::ColorTexture), mGPass.RenderTarget(0).SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SSLRPassRootSignature::NormalTexture), mGPass.RenderTarget(1).SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SSLRPassRootSignature::DepthTeture), gRenderer->CurrentDepthStencilGpuHandle());

		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
		commandList->IASetVertexBuffers(0, 1, mSSLRPass.FullScreenQuad.View());
		commandList->DrawInstanced(mSSLRPass.FullScreenQuad.NumElements(), 1, 0, 0);
	}

	if (mUseScreenQuad && mGPass.ShowTexture())
	{
		DrawScreenQuad(commandList, heaps, _countof(heaps));

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
	}

	gRenderer->CommitCommandList(commandList);
}
