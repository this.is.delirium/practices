#pragma once
#include "StdafxSamples.h"
#include <InputLayouts.h>
#include <Sample.h>
#include <RHITexture.h>

class RHIResource;
class RHIPipelineState;
class Window;

// SEE: Sources/StandEpp
class ParticlesSample : public Sample
{
public:
	ParticlesSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor) { }

	void Release() override final;

	void Initialize() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	void RenderImGui() override final;

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

	void InitializeMainRenderingResources();
	void InitializeParticlesRenderingResources();
	void InitializeInitialStateParticles();

	DirectX::XMFLOAT3 BuildAttractor(Camera* camera, const int absoluteMouseX, const int absoluteMouseY, const int absoluteMouseZ);

protected:
	__declspec(align(16))
	struct ParticleGeneratorSettings
	{
		DirectX::XMFLOAT4	Position;
		DirectX::XMFLOAT4	MinVelocity;
		DirectX::XMFLOAT4	MaxVelocity;
		DirectX::XMFLOAT4	GravityVector;
		DirectX::XMFLOAT4	Color;
		DirectX::XMFLOAT4	Seed;
		DirectX::XMFLOAT4 	Settings; // x - minLifeTime, y - maxLifeTime, z - size, w - timePassed
		int					Count;
	};

protected:
	RHIRootSignature*				mParticlesRenderRootSignature	= nullptr;
	RHIPipelineState*				mParticlesRenderPSO				= nullptr;

	RHIRootSignature*				mParticlesUpdateRootSignature	= nullptr;
	RHIPipelineState*				mParticlesUpdatePSO				= nullptr;

	RHIVertexBuffer					mParticlesBuffer;

	ConstBufferParticlesConstVars	mParticlesConstVarsBuffer;
	ConstBufferParticlesDynamVars	mParticlesDynamVarsBuffer;
	ConstBufferParticlesTimer		mParticlesTimer;

	RHITexture						mParticleTexture;

	struct ParticlesBufferUavSrv
	{
		void Release(IRenderer* renderer);

		RHIResource*			Buffer = nullptr;

		std::vector<int>		Descriptors;

		RHIGPUDescriptorHandle	UavGpuHandle;
		RHICPUDescriptorHandle	UavCpuHandle;

		RHIGPUDescriptorHandle	SrvGpuHandle;
		RHICPUDescriptorHandle	SrvCpuHandle;
	} mParticlesUavSrv;

	ParticleGeneratorSettings	mConstBufferGenSettingsData;

protected: // a part of particles.
	using ParticleVertex = InputLayouts::ParticlesRender::Vertex;
	size_t						mMaxParticles			= 3000000;
	size_t						mCurrentNbOfParticles	= 100000;
	UINT						mRenderNbOfParticles;
	std::vector<ParticleVertex>	mParticles;
	float						mInitialRadius			= 100.0f;
};
