#pragma once
#include <StdafxSamples.h>
#include <LightSource.h>
#include <Sample.h>
#include <ScreenQuadPart.h>
#include <StaticObject.h>
#include <ViewFrustum.h>
#include <RHITexture.h>

class ShadowMapSample : public Sample
{
public:
	ShadowMapSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor, true, DirectX::XMFLOAT2(300.0f, 300.0f)) { }
	~ShadowMapSample() { }

	void Release() override final;
	void Initialize() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* list) override final;
	void RenderImGui() override final;

	bool IsUseOwnRendering() const override final { return true; }

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

	void SetCameras() override final;

protected:
	enum class TextureQuality
	{
		Low		= 0,
		Medium	= 1,
		High	= 2
	};

	void CreateDepthTexture();
	glm::uvec2 ParseTextureQuality(const TextureQuality quality);

protected:
	std::shared_ptr<StaticObject>				mFloor;
	std::vector<std::shared_ptr<StaticObject>>	mCubes;
	StaticObject								mLightCube;

	ConstBufferPerFrame							mLightConstBufferPerFrame;
	LightSource									mLightPoint;

	RHIRootSignature*							mDepthPassRootSignature	= nullptr;
	RHIPipelineState*							mDepthPassPSO			= nullptr;
	RHITexture									mDepthTexture;
	glm::uvec2									mDepthTextureSize;
	TextureQuality								mDepthTextureQuality	= TextureQuality::Medium;
	RHIViewport									mDepthViewport;
	RHIRect										mDepthScissorRect;

protected:
	bool										mIsShowLight			= false;
};
