#include "StdafxSamples.h"
#include "PBRSample.h"
#include <ECS/SceneComponent.h>
#include <FBXLoader.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RootSignature.h>
#include <StaticGeometryBuilder.h>

#include <D3D12/private/DXMathHelper.h>
using namespace DirectX;
using namespace GraphicsCore;

namespace
{
	enum class CubeMapPassRootSignature
	{
		PerFrame = 0,
		PerObject,
		CubeMapTexture,
		Count
	};

	enum class PBRPassRootSignature
	{
		PerFrame = 0,
		PerObject,
		PerLight,
		Material,
		Count
	};
}

/**
 * Function : Release
 */
void PBRSample::Release()
{
	Sample::Release();
	mLightPoint.Release();
	mShaderBall.Release();
	mCubeMapPass.Release();
	mTestMaterial.Release();
}

/**
 * Function : Initialize
 */
void PBRSample::Initialize()
{
	Sample::Initialize();
	mCamera->SetZRange(XMFLOAT2(0.1f, 30.0f));
}

/**
 * Function : InitializeRenderingResources
 */
void PBRSample::InitializeRenderingResources()
{
	// Load a sphere.
	{
		FbxLoader fbxLoader;
		//const size_t nbOfObjects = fbxLoader.LoadFile(SResourceManager::Instance().Find(__TEXT("Samplels:Shader Ball-V1.fbx")), true);
		fbxLoader.LoadFile(SResourceManager::Instance().Find(__TEXT("Samplels:Sphere.fbx")));

		mShaderBall.Initialize();
		fbxLoader.FillMeshComponent(0, mShaderBall.GetMeshComponent());
		mShaderBall.GetSceneComponent()->SetScale(MakeFloat3(3.0f));

		//mShaderBall.GetSceneComponent()->SetScale(MakeFloat3(0.01f));
		//mShaderBall.GetSceneComponent()->AddRotateToPitch(XM_PIDIV2);
		//
		//mShaderBall.GetSceneComponent()->SetAxisRotation(XMFLOAT3(0.0f, 0.0f, 1.0f));
		//mShaderBall.GetSceneComponent()->AddAxisRotation(XM_PI);
	}

	// Initialize a light point.
	{
		mLightPoint.Initialize(true, false);
		mLightPoint.SetType(LightSourceType::Point);
		mLightPoint.SetAmbient(XMFLOAT4(100.0f, 100.0f, 100.0f, 1.0f));
		mLightPoint.SetDiffuse(XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
		mLightPoint.SetSpecular(XMFLOAT4(1.0f, 1.0f, 1.0f, 64.0f));

		mLightPoint.ConstBuffer().Data().Position = XMFLOAT4(5.0f, 0.0f, 5.0f, 1.0f);
		mLightPoint.ConstBuffer().UpdateDataOnGPU();
	}

	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, false);

	mCubeMapPass.Initialize(__TEXT("Textures:EnvMaps:Yokohama2"));

	{
		mTestMaterial.Initialize(__TEXT("PBR:TestMaterial"), RHIUtils::GetConstBufferSize(), 1, sizeof(mTestMaterial.Data()), &mTestMaterial.Data());
	}
}

/**
 * Function : Initialize
 */
void PBRSample::CubeMapPass::Initialize(const String& folder)
{
	Map.InitializeCubeMap(folder, __TEXT(".jpg"));

	Cube.Initialize();
	StaticGeometryBuilder::LoadCube(Cube.GetMeshComponent(), Cube.GetSceneComponent());

	Cube.GetSceneComponent()->AddRotateToPitch(XM_PIDIV2);
}

/**
 * Function : CreateRootSignatures
 */
void PBRSample::CreateRootSignatures()
{
	// Cubemap pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(CubeMapPassRootSignature::Count), 1);
		rootSignature[ToInt(CubeMapPassRootSignature::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(CubeMapPassRootSignature::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
		rootSignature[ToInt(CubeMapPassRootSignature::CubeMapTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);

		RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(0);
		samplerDesc							= RHIStaticSamplerDesc(RHIDefault());
		samplerDesc.Filter					= RHIFilter::MinMagMipLinear;

		mCubeMapPass.RootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}

	// Main pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(PBRPassRootSignature::Count));
		rootSignature[ToInt(PBRPassRootSignature::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(PBRPassRootSignature::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
		rootSignature[ToInt(PBRPassRootSignature::PerLight)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::CBV, 1, 2);
		rootSignature[ToInt(PBRPassRootSignature::Material)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 3);

		mRootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}
}

/**
 * Function : CreatePipelineStates
 */
void PBRSample::CreatePipelineStates()
{
	// Cubemap pass.
	{
		RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("PBR:CubeMapPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("PBR:CubeMapPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc			= {};
		psoDesc.RootSignature							= mCubeMapPass.RootSignature;
		psoDesc.InputLayout								= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= false;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;
		psoDesc.RasterizerState.FrontCounterClockwise	= true;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.SetBlendFunc(0, RHIBlend::SrcAlpha, RHIBlend::InvSrcAlpha);

		mCubeMapPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// Main pass.
	{
		RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("PBR:Main.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("PBR:Main.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc			= {};
		psoDesc.RootSignature							= mRootSignature;
		psoDesc.InputLayout								= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.DSVFormat								= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= TRUE;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable			= FALSE;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode				= RHICullMode::Back;
		psoDesc.RasterizerState.FrontCounterClockwise	= true;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.SetBlendFunc(0, RHIBlend::SrcAlpha, RHIBlend::InvSrcAlpha);

		mPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : Update
 */
void PBRSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	mShaderBall.Update(deltaTime);

	{
		mCubeMapPass.Cube.GetSceneComponent()->SetPosition(MakeFloat3(mCamera->Position()));
		mCubeMapPass.Cube.Update(deltaTime);
	}

	if (mRotateLight)
	{
		ConstBufferForLight& cbLight = mLightPoint.ConstBuffer();

		XMMATRIX rotation = XMMatrixRotationAxis(XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f), deltaTime);
		XMVECTOR position = XMLoadFloat4(&cbLight.Data().Position);
		position = XMVector4Transform(position, rotation);
		XMStoreFloat4(&cbLight.Data().Position, position);

		cbLight.MarkDirty();
	}

	if (mLightPoint.ConstBuffer().IsDirty())
		mLightPoint.ConstBuffer().UpdateDataOnGPU();

	if (mTestMaterial.IsDirty())
		mTestMaterial.UpdateDataOnGPU();
}

/**
 * Function : Render
 */
void PBRSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };

	gRenderer->CommandAllocator()->Reset();
	commandList->Reset(gRenderer->CommandAllocator(), nullptr);

	// Cubemap pass.
	{
		RHIResourceBarrier startBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget)
		};

		commandList->ResourceBarrier(_countof(startBarriers), startBarriers);
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);
		commandList->ClearRenderTargetView(gRenderer->CurrentRenderTargetCpuHandle(), &mBGColor.x, 0, nullptr);

		commandList->SetPipelineState(mCubeMapPass.PSO);
		commandList->SetGraphicsRootSignature(mCubeMapPass.RootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		commandList->SetGraphicsRootDescriptorTable(ToInt(CubeMapPassRootSignature::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(CubeMapPassRootSignature::CubeMapTexture), mCubeMapPass.Map.SrvHandle().Gpu());

		mCubeMapPass.Cube.Draw(commandList, ToInt(CubeMapPassRootSignature::PerObject));
	}

	// Main pass.
	{
		RHIResourceBarrier startBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthWrite)
		};

		RHIResourceBarrier stopBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present)
		};

		commandList->ResourceBarrier(_countof(startBarriers), startBarriers);
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, &gRenderer->CurrentDepthStencilCpuHandle());
		commandList->ClearDepthStencilView(gRenderer->CurrentDepthStencilCpuHandle(), RHIClearFlags::Depth | RHIClearFlags::Stencil, 1.0f, 0, 0, nullptr);

		commandList->SetGraphicsRootSignature(mRootSignature);
		commandList->SetPipelineState(mPSO);

		commandList->SetGraphicsRootDescriptorTable(ToInt(PBRPassRootSignature::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(PBRPassRootSignature::PerLight), mLightPoint.ConstBuffer().SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(PBRPassRootSignature::Material), mTestMaterial.SrvHandle().Gpu());

		mShaderBall.Draw(commandList, ToInt(PBRPassRootSignature::PerObject));

		commandList->ResourceBarrier(_countof(stopBarriers), stopBarriers);
	}

	gRenderer->CommitCommandList(commandList);
}

/**
* Function : RenderImGui
*/
void PBRSample::RenderImGui()
{
	if (ImGui::CollapsingHeader("Material settings", ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_DefaultOpen))
		MaterialSettings();

	if (ImGui::CollapsingHeader("Light settings", ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_DefaultOpen))
		LightSettings();
}

/**
 * Function : MaterialSettings
 */
void PBRSample::MaterialSettings()
{
	Material& mat = mTestMaterial.Data();

	if (ImGui::SliderFloat3("Albedo", &mat.Albedo.x, 0.0f, 1.0f))
		mTestMaterial.MarkDirty();

	if (ImGui::SliderFloat3("Base Fresnel", &mat.BaseFresnel.x, 0.0f, 1.0f))
		mTestMaterial.MarkDirty();

	if (ImGui::SliderFloat("Metallic", &mat.Metallic(), 0.0f, 1.0f))
		mTestMaterial.MarkDirty();

	if (ImGui::SliderFloat("Roughness", &mat.Roughness(), 0.0f, 1.0f))
		mTestMaterial.MarkDirty();

	if (ImGui::SliderFloat("AO", &mat.AO(), 0.0f, 1.0f))
		mTestMaterial.MarkDirty();
}

/**
 * Function : LightSettings
 */
void PBRSample::LightSettings()
{
	ConstBufferForLight& cbLight		= mLightPoint.ConstBuffer();
	ConstBufferForLight::Type& cbData	= mLightPoint.ConstBuffer().Data();

	ImGui::Checkbox("Rotate the light point", &mRotateLight);
	ImGui::Separator();
	if (ImGui::SliderFloat("Specular", &cbData.Specular.w, 8.0f, 256.0f))
		cbLight.MarkDirty();
	if (ImGui::SliderFloat3("Ambient", &cbData.Ambient.x, 0.0f, 100.0f))
		cbLight.MarkDirty();
}
