#include "StdafxSamples.h"
#include "DeferredShadingSample.h"

#include <Application.h>
#include <ECS/MeshComponent.h>
#include <ECS/SceneComponent.h>
#include <FBXLoader.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHIUtils.h>
#include <StaticGeometryBuilder.h>
#include <Window.h>

#include <D3D12/private/DXMathHelper.h>
using namespace DirectX;
using namespace GraphicsCore;

namespace
{
	enum class GeometryPassRootParameters
	{
		PerFrame	= 0,
		PerObject	= 1,
		Texture		= 2
	};

	enum class SunPassRootParameters
	{
		Texture	= 0
	};

	enum class FirstLightPassRootParameters
	{
		PerFrame	= 0,
		PerObject	= 1
	};

	enum class SecondLightPassRootParameters
	{
		PerFrame		= 0,
		PerObject		= 1,
		DiffuseTexture	= 2,
		NormalTexture	= 3,
		DepthTexture	= 4,
		WTexture		= 5
	};
}

/**
 * Function : Release
 */
void DeferredShadingSample::Release()
{
	Sample::Release();

	mGPass.Release();
	mSunPass.Release();
	mFirstLightPass.Release();
	mSecondLightPass.Release();

	if (mFloor.get())
		mFloor->Release();

	for (auto& object : mSpheres)
		object.Release();
	mSpheres.clear();

	for (auto& light : mLightPoints)
		light.Release();
	mLightPoints.clear();

	mFloorTexture.Release();
	mSphereTexture.Release();
}

/**
 * Function : Initialize
 */
void DeferredShadingSample::Initialize()
{
	Sample::Initialize();

	mScreenQuad->SetTexture(mGPass.RenderTarget(0).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);
	mCamera->SetPosition(XMFLOAT4(50.0f, 00.0f, 30.0f, 1.0f));
}

/**
 * Function : InitializeRenderingResources
 */
void DeferredShadingSample::InitializeRenderingResources()
{
	CreateRenderTargets();
	CreateScene();
	CreateLights();
	CreateSunQuad();
	LoadTextures();

	// Initialize constant buffers.
	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, true);
}

/**
 * Function : CreateRenderTargets
 */
void DeferredShadingSample::CreateRenderTargets()
{
	RHIFormat formats[]				= { RHIFormat::R8G8B8A8UNorm, RHIFormat::R16G16Float, RHIFormat::R32Float };
	MRTPassDesc mrtDesc				= {};
	mrtDesc.NumRT					= _countof(formats);
	mrtDesc.DepthStencil.ViewFormat	= gRenderer->DepthStencilViewFormat(); // ToDo: Describe DepthStencil.
	mrtDesc.InputLayout				= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
	mrtDesc.BlendDesc				= RHIBlendDesc(RHIDefault());
	for (UINT i = 0; i < mrtDesc.NumRT; ++i)
	{
		RHIResourceDesc& desc	= mrtDesc.Descs[i];
		desc.Dimension			= RHIResourceDimension::Texture2D;
		desc.Width				= mApp->GetWindow()->Width();
		desc.Height				= mApp->GetWindow()->Height();
		desc.DepthOrArraySize	= 1;
		desc.MipLevels			= 1;
		desc.SampleDesc.Count	= 1;
		desc.Layout				= RHITextureLayout::Unknown;
		desc.Flags				= RHIResourceFlag::AllowRenderTarget;
		desc.Format				= formats[i];

		{
			RHIClearValue& clearValue = mrtDesc.ClearValues[i];
			clearValue.Format = formats[i];
			clearValue.Color[0] = 0.0f;
			clearValue.Color[1] = 0.0f;
			clearValue.Color[2] = 0.0f;
			clearValue.Color[3] = 1.0f;
		}
	}

	mGPass.Initialize(mrtDesc);
}

/**
 * Function : CreateScene
 */
void DeferredShadingSample::CreateScene()
{
	mFloor = std::make_shared<StaticObject>();
	mFloor->Initialize(false, false);
	StaticGeometryBuilder::LoadPlane(mFloor->GetMeshComponent(), mFloor->GetSceneComponent());

	mFloor->GetSceneComponent()->SetScale(XMFLOAT3(300.0f, 300.0f, 300.0f));
	mFloor->GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, 0.0f, 0.0f));

	const float		distance		= 3.0f;
	const float		diameter		= 6.0f;
	const float		radius			= diameter * 0.5f;
	const float		tmp				= distance + diameter;
	const int		numX			= 20;
	const int		numY			= 20;
	const uint32_t	nbOfThetaSplit	= 15;
	const uint32_t	nbOfPhiSplit	= 15;
	
	const XMFLOAT3 startPosition(-tmp * numX * 0.5f, -tmp * numY * 0.5f, radius);
	XMStoreFloat3(&mSceneSize, XMVectorAbs(XMLoadFloat3(&startPosition)));
	mSceneSize *= 2.0f;

	std::default_random_engine eng(time(NULL));
	std::uniform_real_distribution<float> distr(-0.5f, 0.5f);

	std::default_random_engine rotateEng(time(NULL));
	std::uniform_real_distribution<float> rotateDistr(0.0f, 2.0f * glm::pi<float>());

	FbxLoader loader;
	loader.LoadFile(SResourceManager::Instance().Find(__TEXT("Samplels:Sphere.fbx")));

	for (int y = 0; y < numY; ++y)
	{
		for (int x = 0; x < numX; ++x)
		{
			mSpheres.push_back(StaticObject());

			XMFLOAT3 offset(0.0f, 0.0f, 0.0f);
			if (mIsRandomScene)
				offset = XMFLOAT3(distr(eng) * radius, distr(eng) * radius, 0.0f);

			StaticObject& sphere = mSpheres.back();
			sphere.Initialize();
			loader.FillMeshComponent(0, sphere.GetMeshComponent());

			sphere.GetSceneComponent()->SetScale(XMFLOAT3(radius, radius, radius));
			sphere.GetSceneComponent()->SetPosition(startPosition + XMFLOAT3(tmp * x, tmp * y, 0.0f) + offset);

			if (mIsRandomScene)
			{
				sphere.GetSceneComponent()->AddRotateToPitch(rotateDistr(rotateEng));
				sphere.GetSceneComponent()->AddRotateToRoll(rotateDistr(rotateEng));
			}
		}
	}

	mStartPosition = startPosition;
}

/**
 * Function : CreateLights
 */
void DeferredShadingSample::CreateLights()
{
	FbxLoader loader;
	loader.LoadFile(SResourceManager::Instance().Find(__TEXT("Samplels:LightSphere.fbx")));

	const int maxX = 16;
	const int maxY = 16;
	const int maxLights = maxX * maxY;
	std::default_random_engine engPos(time(NULL));
	std::uniform_real_distribution<float> distrPos(-1.0f, 1.0f);

	unsigned int colorSeed = mIsRandomScene ? time(NULL) : 0xFFF;
	std::default_random_engine engColor(colorSeed);
	std::uniform_real_distribution<float> distrColor(0.3f, 1.0f);

	unsigned int defSeed = mIsRandomScene ? time(NULL) : 0xABC;
	std::default_random_engine engDef(defSeed);
	std::uniform_real_distribution<float> distrDef(0.0f, 1.0f);

	XMFLOAT2 step(mSceneSize.x / maxX, mSceneSize.y / maxY);

	for (int y = 0; y < maxY; ++y)
	{
		for (int x = 0; x < maxX; ++x)
		{
			mLightPoints.push_back(LightSource());

			LightSource& lightPoint = mLightPoints.back();
			lightPoint.Initialize();
			lightPoint.SetType(LightSourceType::Point);
			lightPoint.SetAmbient(XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f));
			lightPoint.SetDiffuse(XMFLOAT4(distrColor(engColor), distrColor(engColor), distrColor(engColor), 1.0f));
			lightPoint.SetAttenuation(XMFLOAT3(1.0f, 0.7f, 1.0f) + XMFLOAT3(distrDef(engDef) * 0.5f, distrDef(engDef) * 1.0f, distrDef(engDef) * 0.8f));
			lightPoint.SetItensity(distrDef(engDef) * 10.0f);
			lightPoint.SetSpecular(XMFLOAT4(distrColor(engColor), distrColor(engColor), distrColor(engColor), 1.0f));

			loader.FillMeshComponent(0, lightPoint.GetMeshComponent());

			float scale = lightPoint.CalculateDistance();

			ConstBufferForLight& cbLight = lightPoint.ConstBuffer();
			if (mIsRandomScene)
				cbLight.Data().Position = XMFLOAT4(mSceneSize.x * distrPos(engPos), mSceneSize.y * distrPos(engPos), mSceneSize.z * (1.0f + distrPos(engPos)), scale);
			else
				cbLight.Data().Position = XMFLOAT4(mStartPosition.x + step.x * x, mStartPosition.y + step.y * y, mSceneSize.z * 1.1f, scale);

			lightPoint.GetSceneComponent()->SetPosition(XMFLOAT3(cbLight.Data().Position.x, cbLight.Data().Position.y, cbLight.Data().Position.z));
			lightPoint.GetSceneComponent()->SetScale(XMFLOAT3(scale, scale, scale));
			cbLight.Data().Matrix = lightPoint.GetSceneComponent()->WorldMatrix();
			cbLight.UpdateDataOnGPU();
		}
	}
}

/**
 * Function : CreateSunQuad
 */
void DeferredShadingSample::CreateSunQuad()
{
	StaticGeometryBuilder::BuildFullScreenQuad(&mSunPass.VertexBuffer, __TEXT("DeferredShading:SunPass:VertexBuffer"));
}

/**
 * Function : LoadTextures
 */
void DeferredShadingSample::LoadTextures()
{
	mFloorTexture.InitializeByName(__TEXT("Textures:floor.dds"));
	mSphereTexture.InitializeByName(__TEXT("Textures:lion.dds"));
}

/**
 * Function : CreateRootSignatures
 */
void DeferredShadingSample::CreateRootSignatures()
{
	// Geometry pass.
	{
		RHIDescriptorRange ranges[3];
		ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);
		ranges[1].Init(RHIDescriptorRangeType::CBV, 1, 1);
		ranges[2].Init(RHIDescriptorRangeType::SRV, 1, 0);

		RHIRootParameter rootParameters[3];
		rootParameters[ToInt(GeometryPassRootParameters::PerFrame)].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::All);
		rootParameters[ToInt(GeometryPassRootParameters::PerObject)].InitAsDescriptorTable(1, &ranges[1], RHIShaderVisibility::Vertex);
		rootParameters[ToInt(GeometryPassRootParameters::Texture)].InitAsDescriptorTable(1, &ranges[2], RHIShaderVisibility::Pixel);

		RHIStaticSamplerDesc samplerDesc	= {};
		samplerDesc.Filter					= RHIFilter::MinMagMipLinear;
		samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc.MipLODBias				= 0;
		samplerDesc.MaxAnisotropy			= 0;
		samplerDesc.ComparisonFunc			= RHIComparisonFunc::Never;
		samplerDesc.BorderColor				= RHIStaticBorderColor::TransparentBlack;
		samplerDesc.MinLOD					= 0.0f;
		samplerDesc.MaxLOD					= RHIConstants::kFloat32Max;
		samplerDesc.ShaderRegister			= 0;
		samplerDesc.RegisterSpace			= 0;
		samplerDesc.ShaderVisibility		= RHIShaderVisibility::Pixel;

		RHIRootSignatureDesc rsDesc = {};
		rsDesc.Init(_countof(rootParameters), &rootParameters[0], 1, &samplerDesc, GetRootSignatureFlags(true, true, false, false, false, true, false));

		mGPass.SetRootSignature(gRenderer->SerializeAndCreateRootSignature(&rsDesc, RHIRootSignatureVersion::Ver1));
	}

	// Sun pass.
	{
		RHIDescriptorRange ranges[1];
		ranges[0].Init(RHIDescriptorRangeType::SRV, 1, 0); // a diffuse texture.

		RHIRootParameter rootParameters[1];
		rootParameters[ToInt(SunPassRootParameters::Texture)].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::Pixel);

		RHIStaticSamplerDesc samplerDesc	= {};
		samplerDesc.Filter					= RHIFilter::MinMagMipLinear;
		samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc.MipLODBias				= 0;
		samplerDesc.MaxAnisotropy			= 0;
		samplerDesc.ComparisonFunc			= RHIComparisonFunc::Never;
		samplerDesc.BorderColor				= RHIStaticBorderColor::TransparentBlack;
		samplerDesc.MinLOD					= 0.0f;
		samplerDesc.MaxLOD					= RHIConstants::kFloat32Max;
		samplerDesc.ShaderRegister			= 0;
		samplerDesc.RegisterSpace			= 0;
		samplerDesc.ShaderVisibility		= RHIShaderVisibility::Pixel;

		RHIRootSignatureDesc desc = {};
		desc.Init(_countof(rootParameters), &rootParameters[0], 1, &samplerDesc, GetRootSignatureFlags(true, true, false, false, false, true, false));

		mSunPass.RootSignature = gRenderer->SerializeAndCreateRootSignature(&desc, RHIRootSignatureVersion::Ver1);
	}

	// The first light pass.
	{
		RHIDescriptorRange ranges[2];
		ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);
		ranges[1].Init(RHIDescriptorRangeType::CBV, 1, 1);

		RHIRootParameter rootParameters[2];
		rootParameters[ToInt(FirstLightPassRootParameters::PerFrame)].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::Vertex);
		rootParameters[ToInt(FirstLightPassRootParameters::PerObject)].InitAsDescriptorTable(1, &ranges[1], RHIShaderVisibility::Vertex);

		RHIRootSignatureDesc desc = {};
		desc.Init(_countof(rootParameters), &rootParameters[0], 0, nullptr, GetRootSignatureFlags(true, true, false, false, false, false, false));

		mFirstLightPass.RootSignature = gRenderer->SerializeAndCreateRootSignature(&desc, RHIRootSignatureVersion::Ver1);
	}

	// The second light pass.
	{
		RHIDescriptorRange ranges[6];
		ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0); // per frame.
		ranges[1].Init(RHIDescriptorRangeType::CBV, 1, 1); // per light.
		ranges[2].Init(RHIDescriptorRangeType::SRV, 1, 0); // a diffuse texture.
		ranges[3].Init(RHIDescriptorRangeType::SRV, 1, 1); // a normal texture.
		ranges[4].Init(RHIDescriptorRangeType::SRV, 1, 2); // a depth texture.
		ranges[5].Init(RHIDescriptorRangeType::SRV, 1, 3); // a w-component texture.

		// It will be possible if DescriptorHeapManager::GetNewDescriptors returns values one after another.
		//ranges[0].Init(RHIDescriptorRangeType::CBV, 2, 0);
		//ranges[1].Init(RHIDescriptorRangeType::SRV, 3, 0);

		RHIRootParameter rootParameters[6];
		rootParameters[ToInt(SecondLightPassRootParameters::PerFrame)].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::All);
		rootParameters[ToInt(SecondLightPassRootParameters::PerObject)].InitAsConstantBufferView(1, 0, RHIShaderVisibility::All);
		rootParameters[ToInt(SecondLightPassRootParameters::DiffuseTexture)].InitAsDescriptorTable(1, &ranges[2], RHIShaderVisibility::Pixel);
		rootParameters[ToInt(SecondLightPassRootParameters::NormalTexture)].InitAsDescriptorTable(1, &ranges[3], RHIShaderVisibility::Pixel);
		rootParameters[ToInt(SecondLightPassRootParameters::DepthTexture)].InitAsDescriptorTable(1, &ranges[4], RHIShaderVisibility::Pixel);
		rootParameters[ToInt(SecondLightPassRootParameters::WTexture)].InitAsDescriptorTable(1, &ranges[5], RHIShaderVisibility::Pixel);

		RHIStaticSamplerDesc samplerDesc	= {};
		samplerDesc.Filter					= RHIFilter::MinMagMipLinear;
		samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc.MipLODBias				= 0;
		samplerDesc.MaxAnisotropy			= 0;
		samplerDesc.ComparisonFunc			= RHIComparisonFunc::Never;
		samplerDesc.BorderColor				= RHIStaticBorderColor::TransparentBlack;
		samplerDesc.MinLOD					= 0.0f;
		samplerDesc.MaxLOD					= RHIConstants::kFloat32Max;
		samplerDesc.ShaderRegister			= 0;
		samplerDesc.RegisterSpace			= 0;
		samplerDesc.ShaderVisibility		= RHIShaderVisibility::All;

		RHIRootSignatureDesc rsDesc = {};
		rsDesc.Init(_countof(rootParameters), &rootParameters[0], 1, &samplerDesc, GetRootSignatureFlags(true, true, false, false, false, true, false));

		mSecondLightPass.RootSignature = gRenderer->SerializeAndCreateRootSignature(&rsDesc, RHIRootSignatureVersion::Ver1);
	}
}

/**
 * Function : CreatePipelineStates
 */
void DeferredShadingSample::CreatePipelineStates()
{
	// Geometry pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("DeferredShading:GeometryPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("DeferredShading:GeometryPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(psoDesc));

		psoDesc.VS												= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS												= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType							= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask										= UINT_MAX;
		psoDesc.SampleDesc.Count								= 1;

		psoDesc.DepthStencilState.DepthEnable					= TRUE;
		psoDesc.DepthStencilState.DepthFunc						= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask				= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable					= false;

		psoDesc.RasterizerState									= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise			= TRUE;
		psoDesc.RasterizerState.FillMode						= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode						= RHICullMode::Back;

		RHIUtils::Fill::GraphicsPipelineStateDesc(&mGPass, &psoDesc);

		mGPass.SetPSO(gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc));
	}

	// Sun pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("DeferredShading:SunPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("DeferredShading:SunPass.ps"), RHIShaderType::Pixel));
		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc				= {};
		psoDesc.RootSignature								= mSunPass.RootSignature;
		psoDesc.InputLayout									= { InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc, _countof(InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc) };
		psoDesc.VS											= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS											= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType						= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask									= UINT_MAX;
		psoDesc.NumRenderTargets							= 1;
		psoDesc.RTVFormats[0]								= gRenderer->RenderTargetFormat();
		//psoDesc.DSVFormat									= mDepthStencilViewFormat;
		psoDesc.SampleDesc.Count							= 1;

		psoDesc.DepthStencilState.DepthEnable				= false;

		psoDesc.RasterizerState								= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode					= RHICullMode::Back;
		psoDesc.RasterizerState.FrontCounterClockwise		= TRUE;

		psoDesc.BlendState									= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable		= TRUE;
		psoDesc.BlendState.RenderTarget[0].SrcBlend			= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend		= RHIBlend::InvSrcAlpha;
		psoDesc.BlendState.RenderTarget[0].SrcBlendAlpha	= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlendAlpha	= RHIBlend::InvSrcAlpha;

		mSunPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// The first light pass.
	{
		RHIBlobWrapper vertexShader(gRenderer->LoadShader(__TEXT("DeferredShading:FirstLightPass.vs"), RHIShaderType::Vertex));
		if (vertexShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc					= {};
		psoDesc.RootSignature									= mFirstLightPass.RootSignature;
		psoDesc.InputLayout										= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS												= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType							= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask										= UINT_MAX;
		psoDesc.NumRenderTargets								= 0;
		psoDesc.DSVFormat										= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count								= 1;

		psoDesc.DepthStencilState.DepthEnable					= true;
		psoDesc.DepthStencilState.DepthFunc						= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask				= RHIDepthWriteMask::Off;
		psoDesc.DepthStencilState.StencilEnable					= true;
		psoDesc.DepthStencilState.StencilReadMask				= 0;
		psoDesc.DepthStencilState.StencilWriteMask				= 0xff;
		psoDesc.DepthStencilState.FrontFace.StencilFailOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.FrontFace.StencilDepthFailOp	= RHIStencilOp::IncrSat;
		psoDesc.DepthStencilState.FrontFace.StencilPassOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.FrontFace.StencilFunc			= RHIComparisonFunc::Always;
		psoDesc.DepthStencilState.BackFace.StencilFailOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.BackFace.StencilDepthFailOp	= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.BackFace.StencilPassOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.BackFace.StencilFunc			= RHIComparisonFunc::Never;

		psoDesc.RasterizerState									= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise			= TRUE;
		psoDesc.RasterizerState.FillMode						= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode						= RHICullMode::Back;

		psoDesc.BlendState										= RHIBlendDesc(RHIDefault());

		mFirstLightPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// The second light pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("DeferredShading:SecondLightPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("DeferredShading:SecondLightPass.ps"), RHIShaderType::Pixel));
		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(psoDesc));

		psoDesc.RootSignature									= mSecondLightPass.RootSignature;
		psoDesc.InputLayout										= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS												= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS												= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType							= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask										= UINT_MAX;
		psoDesc.NumRenderTargets								= 1;
		psoDesc.RTVFormats[0]									= gRenderer->RenderTargetFormat();
		psoDesc.DSVFormat										= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count								= 1;

		psoDesc.DepthStencilState.DepthEnable					= true;
		psoDesc.DepthStencilState.DepthFunc						= RHIDepthFunc::GEqual;
		psoDesc.DepthStencilState.DepthWriteMask				= RHIDepthWriteMask::Off;
		psoDesc.DepthStencilState.StencilEnable					= true;
		psoDesc.DepthStencilState.StencilReadMask				= 0;
		psoDesc.DepthStencilState.StencilWriteMask				= 0xff;
		psoDesc.DepthStencilState.FrontFace.StencilFailOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.FrontFace.StencilDepthFailOp	= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.FrontFace.StencilPassOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.FrontFace.StencilFunc			= RHIComparisonFunc::Never;
		psoDesc.DepthStencilState.BackFace.StencilFailOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.BackFace.StencilDepthFailOp	= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.BackFace.StencilPassOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.BackFace.StencilFunc			= RHIComparisonFunc::Equal;

		psoDesc.RasterizerState									= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise			= TRUE;
		psoDesc.RasterizerState.FillMode						= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode						= RHICullMode::Back;

		psoDesc.RasterizerState									= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise			= FALSE;
		psoDesc.RasterizerState.FillMode						= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode						= RHICullMode::Front;

		psoDesc.BlendState										= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable			= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp				= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha			= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend				= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend			= RHIBlend::InvSrcAlpha;

		mSecondLightPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : RenderImGui
 */
void DeferredShadingSample::RenderImGui()
{
	ImGui::Text("A simple deferred shading.");

	ImGui::Checkbox("Show texture", &mGPass.ShowTexture());
	if (mGPass.ShowTexture())
	{
		ImGui::Text("Show a render target:");
		if (ImGui::RadioButton("Diffuse color", &mGPass.ShowTextureId(), 0))
			mScreenQuad->SetTexture(mGPass.RenderTarget(0).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);

		if (ImGui::RadioButton("Normals", &mGPass.ShowTextureId(), 1))
			mScreenQuad->SetTexture(mGPass.RenderTarget(1).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);

		if (ImGui::RadioButton("Depth", &mGPass.ShowTextureId(), mGPass.NumRT()))
			mScreenQuad->SetTexture(gRenderer->CurrentDepthStencilGpuHandle(), ScreenQuadPart::TextureType::Depth);
	}
}

/**
 * Function : Update
 */
void DeferredShadingSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	mFloor->Update(deltaTime);

	for (auto& sphere : mSpheres)
		sphere.Update(deltaTime);
}

#include <D3D12/D3D12CommandList.h>
/**
 * Function : Render
 */
void DeferredShadingSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };
	ID3D12GraphicsCommandList* d3d12CmdList = ToD3D12Structure<D3D12CommandList>(commandList)->List().Get();

	// Geometry pass.
	{
		RHIResourceBarrier startBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(0).Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(1).Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(2).Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthWrite)
		};

		RHIResourceBarrier stopBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(0).Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(1).Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(mGPass.RenderTarget(2).Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present)
		};

		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Prepare the geometry pass"));

		commandList->ResourceBarrier(_countof(startBarriers), startBarriers);
		commandList->OMSetRenderTargets(mGPass.NumRT(), mGPass.RtvCpuHandles(), false, &gRenderer->CurrentDepthStencilCpuHandle());
		for (int i = 0; i < mGPass.NumRT(); ++i)
			commandList->ClearRenderTargetView(mGPass.RenderTarget(i).RtvHandle().Cpu(), mGPass.ClearValue(i).Color, 0, nullptr);
		commandList->ClearDepthStencilView(gRenderer->CurrentDepthStencilCpuHandle(), RHIClearFlags::Depth | RHIClearFlags::Stencil, 1.0f, 0, 0, nullptr);
		PIXEndEvent(d3d12CmdList);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Set up the geometry pass"));
		commandList->SetPipelineState(mGPass.PSO());
		commandList->SetGraphicsRootSignature(mGPass.RootSignature());
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetGraphicsRootDescriptorTable(ToInt(GeometryPassRootParameters::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(GeometryPassRootParameters::PerObject), mConstBufferPerObject.SrvHandle().Gpu());

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);
		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
		PIXEndEvent(d3d12CmdList);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Draw the floor"));
		commandList->SetGraphicsRootDescriptorTable(ToInt(GeometryPassRootParameters::Texture), mFloorTexture.SrvHandle().Gpu());
		mFloor->Draw(commandList, ToInt(GeometryPassRootParameters::PerObject));
		PIXEndEvent(d3d12CmdList);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Draw spheres"));
		commandList->SetGraphicsRootDescriptorTable(ToInt(GeometryPassRootParameters::Texture), mSphereTexture.SrvHandle().Gpu());
		for (auto& sphere : mSpheres)
		{
			sphere.Draw(commandList, ToInt(GeometryPassRootParameters::PerObject));
		}
		PIXEndEvent(d3d12CmdList);

		PIXSetMarker(d3d12CmdList, 0, __TEXT("Restore resource barriers (Geometry pass)"));
		commandList->ResourceBarrier(_countof(stopBarriers), stopBarriers);

		PIXSetMarker(d3d12CmdList, 0, __TEXT("Commit command list (Geometry pass)"));
		gRenderer->CommitCommandList(commandList);
	}

	// Sun pass.
	{
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Prepare the sun pass"));

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);
		commandList->ClearRenderTargetView(gRenderer->CurrentRenderTargetCpuHandle(), &mBGColor.x, 0, nullptr);
		PIXEndEvent(d3d12CmdList);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Set up the sun pass"));
		commandList->SetPipelineState(mSunPass.PSO);
		commandList->SetGraphicsRootSignature(mSunPass.RootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetGraphicsRootDescriptorTable(ToInt(SunPassRootParameters::Texture), mGPass.RenderTarget(0).SrvHandle().Gpu());

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);
		PIXEndEvent(d3d12CmdList);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Draw the sun quad"));
		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
		commandList->IASetVertexBuffers(0, 1, mSunPass.VertexBuffer.View());
		commandList->DrawInstanced(6, 1, 0, 0);
		PIXEndEvent(d3d12CmdList);

		PIXSetMarker(d3d12CmdList, 0, __TEXT("Restore resource barriers (Sun pass)"));
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
		PIXSetMarker(d3d12CmdList, 0, __TEXT("Commit command list (Sun pass)"));
		gRenderer->CommitCommandList(commandList);
	}

	// The first light pass.
	{
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Prepare the first light pass"));

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthWrite));
		commandList->OMSetRenderTargets(0, nullptr, false, &gRenderer->CurrentDepthStencilCpuHandle());
		commandList->ClearDepthStencilView(gRenderer->CurrentDepthStencilCpuHandle(), RHIClearFlags::Stencil, 1.0f, 0, 0, nullptr);
		PIXEndEvent(d3d12CmdList);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Set up the first light pass"));
		commandList->SetPipelineState(mFirstLightPass.PSO);
		commandList->SetGraphicsRootSignature(mFirstLightPass.RootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetGraphicsRootDescriptorTable(ToInt(FirstLightPassRootParameters::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		PIXEndEvent(d3d12CmdList);

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Draw spheres in the first light pass"));
		for (auto& lightPoint : mLightPoints)
		{
			commandList->SetGraphicsRootDescriptorTable(ToInt(FirstLightPassRootParameters::PerObject), lightPoint.ConstBuffer().SrvHandle().Gpu());
			mLightPoints[0].GetMeshComponent()->Draw(commandList);
		}
		PIXEndEvent(d3d12CmdList);

		PIXSetMarker(d3d12CmdList, 0, __TEXT("Restore resource barriers (the first light pass)"));
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present));
		PIXSetMarker(d3d12CmdList, 0, __TEXT("Commit command list (the first light pass)"));
		gRenderer->CommitCommandList(commandList);
	}

	// TODO: Copy the depth buffer into another texture.
	// The second light pass.
	{
		gRenderer->StartRendering();
		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Prepare the second light pass"));
		PIXEndEvent(d3d12CmdList);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Set up the second light pass"));
		commandList->SetPipelineState(mSecondLightPass.PSO);
		commandList->SetGraphicsRootSignature(mSecondLightPass.RootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		PIXEndEvent(d3d12CmdList);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Set textures"));
		commandList->SetGraphicsRootDescriptorTable(ToInt(SecondLightPassRootParameters::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SecondLightPassRootParameters::DiffuseTexture), mGPass.RenderTarget(0).SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SecondLightPassRootParameters::NormalTexture), mGPass.RenderTarget(1).SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SecondLightPassRootParameters::DepthTexture), gRenderer->CurrentDepthStencilGpuHandle());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SecondLightPassRootParameters::WTexture), mGPass.RenderTarget(2).SrvHandle().Gpu());
		PIXEndEvent(d3d12CmdList);

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);
		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);

		PIXBeginEvent(d3d12CmdList, 0, __TEXT("Draw spheres in the second light pass"));
		for (auto& lightPoint : mLightPoints)
		{
			//commandList->SetGraphicsRootDescriptorTable(1, lightPoint.GpuHandleCB());
			commandList->SetGraphicsRootConstantBufferView(ToInt(SecondLightPassRootParameters::PerObject), lightPoint.ConstBuffer().GpuVirtualAddress());
			commandList->ClearDepthStencilView(gRenderer->CurrentDepthStencilCpuHandle(), RHIClearFlags::Stencil, 1.0f, 0, 0, nullptr);
			mLightPoints[0].GetMeshComponent()->Draw(commandList);
		}
		PIXEndEvent(d3d12CmdList);

		PIXSetMarker(d3d12CmdList, 0, __TEXT("Restore resource barriers and commit command list (The second light pass)"));
		gRenderer->StopRendering();
	}

	// Draw a screen quad.
	if (mGPass.ShowTexture())
	{
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);

		DrawScreenQuad(commandList, heaps, _countof(heaps));

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
		gRenderer->CommitCommandList(commandList);
	}
}
