#pragma once

#include "StdafxSamples.h"
#include <Sample.h>
#include <LightSource.h>
#include <RHITexture.h>
#include <StaticObject.h>

class VarianceShadowMappingSample : public Sample
{
public:
	VarianceShadowMappingSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor, true, DirectX::XMFLOAT2(256.0f, 256.0f)) { }

	void Release() override final;
	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	bool IsUseOwnRendering() const override final { return true; }

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

	void SetCameras() override final;

protected:
	enum class Type
	{
		SimpleSM = 0,
		VarianceSM
	};

	struct VarianceParams
	{
		int& Type() { return Params.x; }

		DirectX::XMINT4 Params;
	};

	RHIConstBuffer<VarianceParams> mVarianceCB;

	struct DepthPass
	{
		void Release()
		{
			RHISafeRelease(RootSignature);
			RHISafeRelease(PSO);

			RT.Release();
		}

		RHIRootSignature*	RootSignature	= nullptr;
		RHIPipelineState*	PSO				= nullptr;

		RHIFormat			Format			= RHIFormat::R16G16Float;
		RHIClearValue		ClearValue;
		RHITexture			RT;
	} mDepthPass;

	struct SceneObjects
	{
		void Release()
		{
			if (Floor)
				Floor->Release();

			for (auto& cube : Cubes)
				cube->Release();

			LightConstBufferPerFrame.Release();
			LightPoint.Release();
		}

		std::shared_ptr<StaticObject>				Floor;
		std::vector<std::shared_ptr<StaticObject>>	Cubes;

		ConstBufferPerFrame							LightConstBufferPerFrame;
		LightSource									LightPoint;

		bool										Rotate = true;
	} mObjects;
};
