#include "StdafxSamples.h"
#include "PSSMSample.h"
#include <Application.h>
#include <BuildTasks.h>
#include <FBXLoader.h>
#include <PerlinNoiseAlgo.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHIUtils.h>
#include <RootSignature.h>
#include <TaskManager.h>

#include <ECS/MeshComponent.h>
#include <ECS/SceneComponent.h>

#include <D3D12/private/DXMathHelper.h>

using Vertex = InputLayouts::Main::Vertex;
using namespace DirectX;
using namespace GraphicsCore;

namespace
{
	enum class DepthPassRootParameters
	{
		PerFrame = 0,
		PerObject,
		Count
	};

	enum class SkydomeRootParameters
	{
		PerFrame = 0,
		PerObject,
		Texture,
		Count
	};

	enum class RenderPassRootParameters
	{
		PerFrame = 0,
		PerObject,
		FrustumRanges,
		CascadeMatrices,
		DiffuseTexture,
		DepthTextures,
		Count
	};
}

/**
 * Function : Release
 */
void PSSMSample::SkydomePart::Release()
{
	RHISafeRelease(RootSignature);
	RHISafeRelease(PSO);
	Mesh.Release();
	Texture.Release();
	CB.Release();
}

/**
 * Function : Release
 */
void PSSMSample::DepthPass::Release()
{
	RHISafeRelease(RootSignature);
	RHISafeRelease(PSO);

	RHISafeRelease(TextureArray);

	gRenderer->DsvHeapManager().FreeDescriptors(DsvTextureDescriptors);
	gRenderer->CbvSrvUavHeapManager().FreeDescriptors(SrvTextureDescriptors);

	for (int i = 0; i < _countof(CB); ++i)
		CB[i].Release();
}

/**
 * Function : Release
 */
void PSSMSample::RenderPass::Release()
{
	CB.Release();

	gRenderer->CbvSrvUavHeapManager().FreeDescriptor(DepthTextureArrayDescriptor);
}

/**
 * Function : Release
 */
void PSSMSample::Release()
{
	Sample::Release();

	mTerrainVB.Release();
	mTerrainIB.Release();

	mTerrainTexture.Release();

	mSkydome.Release();
	mDepthPass.Release();
	mRenderPass.Release();

	for (int i = 0; i < _countof(mSpheres); ++i)
		mSpheres[i].Release();

	mSphereTexture.Release();

	mConstBufferFrustumRanges.Release();
}

/**
 * Function : Initialize
 */
void PSSMSample::Initialize()
{
	mExistSamplels			= !SResourceManager::Instance().Find(__TEXT("Samplels:powerplant:powerplant.obj")).empty();

	mBuildHeightMapProgress	= std::make_shared<ProgressIndicator>(0, 1);
	mBuildTerrainProgress	= std::make_shared<ProgressIndicator>(0, 1);

	Sample::Initialize();

	mScreenQuad->SetTexture(mDepthPass.TextureGpuHandlesSrv[mDepthPass.TextureId], ScreenQuadPart::TextureType::Depth);

	mCamera->SetPosition(XMFLOAT4(160.0f, 0.0f, 85.0f, 1.0f));
}

/**
 * Function : SetCameras
 */
void PSSMSample::SetCameras()
{
	Sample::SetCameras();

	// Add an auxillary camera.
	{
		Camera::CameraDesc desc;
		desc.Common.Position			= XMFLOAT4(0.0f, 300.0f, 150.0f, 1.0f);
		desc.Common.ZRange				= XMFLOAT2(0.1f, 500.0f);

		desc.ProjType					= Camera::ProjectionType::Perspective;
		desc.PerspPart.FoV				= 45.0f;
		desc.PerspPart.AspectRatio		= mApp->GetWindow()->AspectRatio();

		desc.Format						= Camera::InitializeFormat::FromSphericalAngles;
		desc.AnglesPart.AngleXRange		= XMFLOAT2(-XM_PI, XM_PI);
		desc.AnglesPart.AngleYRange		= XMFLOAT2(-XM_PIDIV2, XM_PIDIV2);
		desc.AnglesPart.SphericalAngles	= XMFLOAT2(-XM_PIDIV2, -XM_PIDIV4);

		mAuxillaryCamera = std::make_shared<Camera>();
		mAuxillaryCamera->Initialize(desc);

		mAuxillaryCamera->SetName("Auxillary camera");
		mApp->AddCamera(mAuxillaryCamera);
	}
}

/**
 * Function : Activate
 */
void PSSMSample::Activate()
{
	Initialize();

	{
		BuildHeightMap();
		BuildTerrain();
	}
}

/**
 * Function : InitializeRenderingResources
 */
void PSSMSample::InitializeRenderingResources()
{
	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, true);

	if (mExistSamplels)
		LoadContent();
	else
		GenerateScene();

	// Initialize a skydome.
	{
		FbxLoader loader;
		loader.LoadFile(SResourceManager::Instance().Find(__TEXT("Samplels:skydome.fbx")));

		mSkydome.Mesh.Initialize();
		loader.FillMeshComponent(0, mSkydome.Mesh.GetMeshComponent());

		mSkydome.Texture.InitializeByName(__TEXT("Textures:sky.dds"));

		mSkydome.CB.Initialize(__TEXT("PSSM:SkydomeCB"), RHIUtils::GetConstBufferSize(), 1, sizeof(mSkydome.CB.Data()));
	}

	// Initialize a view frustum.
	{
		XMFLOAT4 colors[] =
		{
			XMFLOAT4(1.0f, 0.0f, 0.0f, 0.5f),
			XMFLOAT4(0.0f, 1.0f, 0.0f, 0.5f),
			XMFLOAT4(0.0f, 0.0f, 1.0f, 0.5f),
			XMFLOAT4(1.0f, 1.0f, 0.0f, 0.5f)
		};

		Camera::ViewFrustumDesc desc;
		desc.Lambda			= 0.5f;
		desc.ViewCamera		= mCamera.get();
		desc.LightCamera	= mAuxillaryCamera.get();
		desc.NbOfSplit		= _countof(colors);
		memcpy(desc.Colors, colors, sizeof(colors));

		mCamera->InitializeViewFrustum(desc);
		mApp->AddViewFrustum(mCamera->GetViewFrustum());
	}

	AllocateDescriptorHandles();
	CreateDepthTextures();

	// Create a constant buffer for the depth pass.
	for (int i = 0; i < _countof(mDepthPass.CB); ++i)
		mDepthPass.CB[i].Initialize(__TEXT("PSSM:DepthPass:CBi"), RHIUtils::GetConstBufferSize(), 1, sizeof(mDepthPass.CB[i].Data()), nullptr, RHIHeapType::Upload);

	// Create a constant buffer for frustum ranges.
	{
		float* zFar = &mConstBufferFrustumRanges.Data().ZFars.x;
		for (size_t i = 0; i < mCamera->NbOfFrustums(); ++i, ++zFar)
			*zFar = mCamera->GetFrustum(i).ZRange.y;
		mConstBufferFrustumRanges.Data().NbOfFrustums = mCamera->NbOfFrustums();

		mConstBufferFrustumRanges.Initialize(__TEXT("PSSM:CBFrustumRanges"), RHIUtils::GetConstBufferSize(), 1, sizeof(mConstBufferFrustumRanges.Data()), &mConstBufferFrustumRanges.Data());
	}

	// Create the constant buffer of cascade matrices for the render pass.
	{
		mRenderPass.CB.Initialize(__TEXT("PSSM:RenderPassCB"), RHIUtils::GetConstBufferSize(), 1, sizeof(mRenderPass.CB.Data()), nullptr, RHIHeapType::Upload);
	}
}

/**
 * Function : LoadContent
 */
void PSSMSample::LoadContent()
{

}

/**
 * Function : GenerateScene
 */
void PSSMSample::GenerateScene()
{
	mSizeMap			= DirectX::XMUINT2(512, 512);
	mTerrainHeightRange	= DirectX::XMFLOAT2(-5.0f, 30.0f);
	mTerrainSizeInWorld	= DirectX::XMFLOAT2(500.0f, 500.0f);

	const UINT maxVertices = mSizeMap.x * mSizeMap.y;

	// Create a vertex buffer.
	mTerrainVB.Initialize(__TEXT("PSSM:TerratinVB"), sizeof(Vertex) * maxVertices, 0, sizeof(Vertex));

	// Create an index buffer.
	mTerrainIB.Initialize(__TEXT("PSSM:TerrainIB"), sizeof(uint32_t) * maxVertices * 6, 0, sizeof(uint32_t));

	mTerrainTexture.InitializeByName(__TEXT("Textures:floor.dds"));

	// Create spheres.
	{
		FbxLoader loader;
		loader.LoadFile(SResourceManager::Instance().Find(__TEXT("Samplels:Sphere.fbx")));

		const float radius = 5.0f;
		for (int i = 0; i < _countof(mSpheres); ++i)
		{
			StaticObject& sphere = mSpheres[i];

			sphere.Initialize();
			loader.FillMeshComponent(0, sphere.GetMeshComponent());

			sphere.GetSceneComponent()->SetScale(XMFLOAT3(radius, radius, radius));
			sphere.GetSceneComponent()->SetPosition(XMFLOAT3(30.0f * i, 0.0, 50.0f));
			sphere.GetSceneComponent()->SetAxisRotation(XMFLOAT3(0.0f, 0.0f, 1.0f));
		}

		mSphereTexture.InitializeByName(__TEXT("Textures:lion.dds"));
	}
}

/**
 * Function : CreateRootSignatures
 */
void PSSMSample::CreateRootSignatures()
{
	// Depth pass root signature.
	{
		RHIUtils::RootSignature rootSignature(ToInt(DepthPassRootParameters::Count));
		rootSignature[ToInt(DepthPassRootParameters::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(DepthPassRootParameters::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);

		mDepthPass.RootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, false, false));
	}

	{
		RHIUtils::RootSignature rootSignature(ToInt(RenderPassRootParameters::Count), 2);
		rootSignature[ToInt(RenderPassRootParameters::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(RenderPassRootParameters::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
		rootSignature[ToInt(RenderPassRootParameters::FrustumRanges)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 2);
		rootSignature[ToInt(RenderPassRootParameters::CascadeMatrices)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 3);
		rootSignature[ToInt(RenderPassRootParameters::DiffuseTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);
		rootSignature[ToInt(RenderPassRootParameters::DepthTextures)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 1);

		// Sample from a diffuse texture.
		RHIStaticSamplerDesc& samplerDesc0	= rootSignature.SamplerDesc(0);
		samplerDesc0.Filter					= RHIFilter::MinMagMipLinear;
		samplerDesc0.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc0.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc0.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc0.MipLODBias				= 0;
		samplerDesc0.MaxAnisotropy			= 0;
		samplerDesc0.ComparisonFunc			= RHIComparisonFunc::Never;
		samplerDesc0.BorderColor			= RHIStaticBorderColor::TransparentBlack;
		samplerDesc0.MinLOD					= 0.0f;
		samplerDesc0.MaxLOD					= RHIConstants::kFloat32Max;
		samplerDesc0.ShaderRegister			= 0;
		samplerDesc0.RegisterSpace			= 0;
		samplerDesc0.ShaderVisibility		= RHIShaderVisibility::Pixel;
		// Sample from a depth texture.
		RHIStaticSamplerDesc& samplerDesc1	= rootSignature.SamplerDesc(1);
		samplerDesc1.Filter					= RHIFilter::ComparisonMinMagMipLinear;
		samplerDesc1.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc1.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc1.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc1.MipLODBias				= 0;
		samplerDesc1.MaxAnisotropy			= 0;
		samplerDesc1.ComparisonFunc			= RHIComparisonFunc::Less;
		samplerDesc1.BorderColor			= RHIStaticBorderColor::TransparentBlack;
		samplerDesc1.MinLOD					= 0.0f;
		samplerDesc1.MaxLOD					= RHIConstants::kFloat32Max;
		samplerDesc1.ShaderRegister			= 1;
		samplerDesc1.RegisterSpace			= 0;
		samplerDesc1.ShaderVisibility		= RHIShaderVisibility::Pixel;

		mRootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}

	// Skydome root signature.
	{
		RHIUtils::RootSignature rootSignature(ToInt(SkydomeRootParameters::Count), 1);
		rootSignature[ToInt(SkydomeRootParameters::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(SkydomeRootParameters::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
		rootSignature[ToInt(SkydomeRootParameters::Texture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);

		RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(0);
		samplerDesc.Filter					= RHIFilter::MinMagMipLinear;
		samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc.MipLODBias				= 0;
		samplerDesc.MaxAnisotropy			= 0;
		samplerDesc.ComparisonFunc			= RHIComparisonFunc::Never;
		samplerDesc.BorderColor				= RHIStaticBorderColor::TransparentBlack;
		samplerDesc.MinLOD					= 0.0f;
		samplerDesc.MaxLOD					= RHIConstants::kFloat32Max;
		samplerDesc.ShaderRegister			= 0;
		samplerDesc.RegisterSpace			= 0;
		samplerDesc.ShaderVisibility		= RHIShaderVisibility::Pixel;

		mSkydome.RootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}
}

/**
 * Function : CreatePipelineStates
 */
void PSSMSample::CreatePipelineStates()
{
	// Depth pass pipeline state.
	{
		RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("PSSM:DepthPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("PSSM:DepthPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc				= {};
		psoDesc.RootSignature								= mDepthPass.RootSignature;
		psoDesc.InputLayout									= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS											= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS											= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType						= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask									= UINT_MAX;
		psoDesc.NumRenderTargets							= 0;
		psoDesc.DSVFormat									= mDepthPass.ViewFormat;
		psoDesc.SampleDesc.Count							= 1;

		psoDesc.DepthStencilState.DepthEnable				= TRUE;
		psoDesc.DepthStencilState.DepthFunc					= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask			= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable				= FALSE;

		psoDesc.RasterizerState								= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode					= RHICullMode::None;
		psoDesc.RasterizerState.FrontCounterClockwise		= TRUE;

		psoDesc.BlendState									= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable		= FALSE;

		mDepthPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// Render pass pipeline state.
	{
		RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("PSSM:RenderPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("PSSM:RenderPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc				= {};
		psoDesc.RootSignature								= mRootSignature;
		psoDesc.InputLayout									= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS											= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS											= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType						= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask									= UINT_MAX;
		psoDesc.NumRenderTargets							= 1;
		psoDesc.RTVFormats[0]								= gRenderer->RenderTargetFormat();
		psoDesc.DSVFormat									= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count							= 1;

		psoDesc.DepthStencilState.DepthEnable				= TRUE;
		psoDesc.DepthStencilState.DepthFunc					= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask			= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable				= FALSE;

		psoDesc.RasterizerState								= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode					= RHICullMode::None;
		psoDesc.RasterizerState.FrontCounterClockwise		= TRUE;

		psoDesc.BlendState									= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable		= TRUE;
		psoDesc.BlendState.RenderTarget[0].SrcBlend			= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend		= RHIBlend::InvSrcAlpha;
		psoDesc.BlendState.RenderTarget[0].SrcBlendAlpha	= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlendAlpha	= RHIBlend::InvSrcAlpha;

		mPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// Skydome pipeline state.
	{
		RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("PSSM:Skydome.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("PSSM:Skydome.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc				= {};
		psoDesc.RootSignature								= mSkydome.RootSignature;
		psoDesc.InputLayout									= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS											= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS											= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType						= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask									= UINT_MAX;
		psoDesc.NumRenderTargets							= 1;
		psoDesc.RTVFormats[0]								= gRenderer->RenderTargetFormat();
		psoDesc.SampleDesc.Count							= 1;

		psoDesc.DepthStencilState.DepthEnable				= FALSE;
		psoDesc.DepthStencilState.StencilEnable				= FALSE;

		psoDesc.RasterizerState								= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode					= RHICullMode::None;
		psoDesc.RasterizerState.FrontCounterClockwise		= TRUE;

		psoDesc.BlendState									= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable		= FALSE;

		mSkydome.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : AllocateDescriptorHandles
 */
void PSSMSample::AllocateDescriptorHandles()
{
	mDepthPass.DsvTextureDescriptors = gRenderer->DsvHeapManager().GetNewDescriptors(mDepthPass.kMaxTextures);
	mDepthPass.SrvTextureDescriptors = gRenderer->CbvSrvUavHeapManager().GetNewDescriptors(mDepthPass.kMaxTextures);

	for (size_t i = 0; i < mDepthPass.kMaxTextures; ++i)
	{
		mDepthPass.TextureCpuHandlesDsv[i] = gRenderer->DsvHeapManager().CpuHandle(mDepthPass.DsvTextureDescriptors[i]);
		mDepthPass.TextureCpuHandlesSrv[i] = gRenderer->CbvSrvUavHeapManager().CpuHandle(mDepthPass.SrvTextureDescriptors[i]);
		mDepthPass.TextureGpuHandlesSrv[i] = gRenderer->CbvSrvUavHeapManager().GpuHandle(mDepthPass.SrvTextureDescriptors[i]);
	}

	mRenderPass.DepthTextureArrayDescriptor		= gRenderer->CbvSrvUavHeapManager().GetNewDescriptor();
	mRenderPass.DTADCpuHandle					= gRenderer->CbvSrvUavHeapManager().CpuHandle(mRenderPass.DepthTextureArrayDescriptor);
	mRenderPass.DTADGpuHandle					= gRenderer->CbvSrvUavHeapManager().GpuHandle(mRenderPass.DepthTextureArrayDescriptor);
}

/**
 * Function : CreateDepthTextures
 */
void PSSMSample::CreateDepthTextures()
{
	mDepthPass.TextureSize			= XMFLOAT2(1024, 1024);
	mDepthPass.Viewport				= {};
	mDepthPass.Viewport.Width		= mDepthPass.TextureSize.x;
	mDepthPass.Viewport.Height		= mDepthPass.TextureSize.y;
	mDepthPass.Viewport.MaxDepth	= 1.0f;

	mDepthPass.ScissorRect			= {};
	mDepthPass.ScissorRect.right	= mDepthPass.TextureSize.x;
	mDepthPass.ScissorRect.bottom	= mDepthPass.TextureSize.y;

	RHISafeRelease(mDepthPass.TextureArray);

	if (mCamera->IsDirty())
		mCamera->Projection();

	RHIResourceDesc desc;
	ZeroMemory(&desc, sizeof(desc));

	desc.Dimension			= RHIResourceDimension::Texture2D;
	desc.Format				= mDepthPass.DsvFormat;
	desc.Width				= mDepthPass.TextureSize.x;
	desc.Height				= mDepthPass.TextureSize.y;
	desc.DepthOrArraySize	= mDepthPass.kMaxTextures;
	desc.MipLevels			= 1;
	desc.Alignment			= 0;
	desc.SampleDesc.Count	= 1;
	desc.SampleDesc.Quality	= 0;
	desc.Layout				= RHITextureLayout::Unknown;
	desc.Flags				= RHIResourceFlag::AllowDepthStencil;

	RHIClearValue clearValue;
	clearValue.Format				= mDepthPass.ViewFormat;
	clearValue.DepthStencil.Depth	= 1.0f;
	clearValue.DepthStencil.Stencil	= 0;

	// Create the array of depth stencil textures.
	mDepthPass.TextureArray = gRenderer->Device()->CreateCommittedResource(
		&RHIHeapProperties(RHIHeapType::Default),
		RHIHeapFlag::None,
		&desc,
		RHIResourceState::Common,
		&clearValue
	);

	RHIDepthStencilViewDesc dsvDesc		= {};
	dsvDesc.Format						= mDepthPass.ViewFormat;
	dsvDesc.ViewDimension				= RHIDSVDimension::Texture2DArray;
	dsvDesc.Texture2DArray.ArraySize	= 1;
	dsvDesc.Texture2DArray.MipSlice		= 0;

	RHIShaderResourceViewDesc srvDesc	= {};
	srvDesc.Shader4ComponentMapping		= D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Format						= mDepthPass.SrvFormat;
	srvDesc.ViewDimension				= RHISRVDimension::Texture2DArray;
	srvDesc.Texture2DArray.MipLevels	= 1;
	srvDesc.Texture2DArray.ArraySize	= 1;

	for (size_t i = 0; i < mCamera->NbOfFrustums(); ++i)
	{
		// Create a DSV.
		{
			dsvDesc.Texture2DArray.FirstArraySlice = i;
			gRenderer->Device()->CreateDepthStencilView(mDepthPass.TextureArray, &dsvDesc, mDepthPass.TextureCpuHandlesDsv[i]);
		}
	
		// Create a SRV.
		{
			srvDesc.Texture2DArray.FirstArraySlice = i;
			gRenderer->Device()->CreateShaderResourceView(mDepthPass.TextureArray, &srvDesc, mDepthPass.TextureCpuHandlesSrv[i]);
		}
	}

	// Create a SRV for the render pass.
	{
		srvDesc.Texture2DArray.ArraySize		= mDepthPass.kMaxTextures;
		srvDesc.Texture2DArray.FirstArraySlice	= 0;
		gRenderer->Device()->CreateShaderResourceView(mDepthPass.TextureArray, &srvDesc, mRenderPass.DTADCpuHandle);
	}
}

/**
 * Function : BuildHeightMap
 */
void PSSMSample::BuildHeightMap()
{
	mHeightMapGenerator.CreateAlgo<PerlinNoiseAlgo>(5, 1.0f, 0.01f, 0.5f);

	BuildTasks::HeightMapTask* task = new BuildTasks::HeightMapTask(nullptr, nullptr, nullptr, nullptr,
		&mHeightMapGenerator, mSizeMap, mBuildHeightMapProgress.get());

	STaskManager::Instance().Add(task);
}

/**
 * Function : BuildTerrain
 */
void PSSMSample::BuildTerrain()
{
	BuildTasks::TerrainTask* task = new BuildTasks::TerrainTask(&mTerrainVB, &mTerrainIB,
		&mTerrainBuilder, std::cref(mHeightMapGenerator.Map()), std::cref(mTerrainSizeInWorld), std::cref(mTerrainHeightRange), mBuildTerrainProgress.get());

	STaskManager::Instance().Add(task);
}

/**
 * Function : RenderImGui
 */
void PSSMSample::RenderImGui()
{
	ImGui::Text("Parallel Split Shadow Mapping");

	if (ImGui::Button("Build height map"))
	{
		BuildHeightMap();
	}
	ImGui::SameLine();
	ImGui::ProgressBar(mBuildHeightMapProgress->FloatCurrentValue());

	if (ImGui::Button("Build terrain"))
	{
		BuildTerrain();
	}
	ImGui::SameLine();
	ImGui::ProgressBar(mBuildTerrainProgress->FloatCurrentValue());

	ImGui::Separator();

	ImGui::Checkbox("Show depth textures", &mDepthPass.ShowTextures);
	if (mDepthPass.ShowTextures)
	{
		for (size_t i = 0; i < mCamera->NbOfFrustums(); ++i)
		{
			std::string name("Depth map "); name += std::to_string(i);
			if (ImGui::RadioButton(name.c_str(), &mDepthPass.TextureId, i))
				mScreenQuad->SetTexture(mDepthPass.TextureGpuHandlesSrv[mDepthPass.TextureId], ScreenQuadPart::TextureType::ColorRGBA);
		}
	}
}

/**
 * Function : Update
 */
void PSSMSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	// Move a skydome sphere to a camera position.
	{
		mSkydome.Mesh.GetSceneComponent()->SetPosition(DirectX::MakeFloat3(mCamera->Position()));

		mSkydome.CB.Data().WorldMatrix = mSkydome.Mesh.GetSceneComponent()->WorldMatrix();
		mSkydome.CB.UpdateDataOnGPU();
	}

	for (int i = 0; i < _countof(mSpheres); ++i)
	{
		mSpheres[i].GetSceneComponent()->AddAxisRotation(deltaTime * ((i % 2 == 0) ? 1.0f : -1.0f));
		mSpheres[i].Update(deltaTime);
	}

	// Update constant buffer for the depth pass.
	for (int i = 0; i < mCamera->NbOfFrustums(); ++i)
	{
		Camera::Frustum& frustum = mCamera->GetFrustum(i);
		mDepthPass.CB[i].Data().ViewProjMatrix	= frustum.LightView * frustum.Projection;
		mRenderPass.CB.Data().LightVP[i]		= mDepthPass.CB[i].Data().ViewProjMatrix;
		
		mDepthPass.CB[i].UpdateDataOnGPU();
		//gRenderer->QueueOnUpdateBuffer(&mDepthPass.CB[i], &RHIUtils::GetSubresource(&mDepthPass.CBData[i], sizeof(mDepthPass.CBData[i])));
	}
	mRenderPass.CB.UpdateDataOnGPU();

	//gRenderer->ExecuteUpdateQueue();
}

/**
 * Function : Render
 */
void PSSMSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };

	// Depth pass.
	if (mTerrainIB.NumElements() > 0)
	{
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mDepthPass.TextureArray, RHIResourceState::Present, RHIResourceState::DepthWrite));

		for (size_t i = 0; i < mCamera->NbOfFrustums(); ++i)
			commandList->ClearDepthStencilView(mDepthPass.TextureCpuHandlesDsv[i], RHIClearFlags::Depth, 1.0f, 0, 0, nullptr);

		commandList->SetPipelineState(mDepthPass.PSO);
		commandList->SetGraphicsRootSignature(mDepthPass.RootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);

		commandList->RSSetViewports(1, &mDepthPass.Viewport);
		commandList->RSSetScissorRects(1, &mDepthPass.ScissorRect);

		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);

		for (size_t i = 0; i < mCamera->NbOfFrustums(); ++i)
		{
			commandList->OMSetRenderTargets(0, nullptr, false, &mDepthPass.TextureCpuHandlesDsv[i]);

			commandList->SetGraphicsRootDescriptorTable(ToInt(DepthPassRootParameters::PerFrame), mDepthPass.CB[i].SrvHandle().Gpu());

			commandList->SetGraphicsRootDescriptorTable(ToInt(DepthPassRootParameters::PerObject), mConstBufferPerObject.SrvHandle().Gpu());
			commandList->IASetVertexBuffers(0, 1, mTerrainVB.View());
			commandList->IASetIndexBuffer(mTerrainIB.View());
			commandList->DrawIndexedInstanced(mTerrainIB.NumElements(), 1, 0, 0, 0);

			for (int i = 0; i < _countof(mSpheres); ++i)
				mSpheres[i].Draw(commandList, ToInt(DepthPassRootParameters::PerObject));
		}

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mDepthPass.TextureArray, RHIResourceState::DepthWrite, RHIResourceState::Present));
		gRenderer->CommitCommandList(commandList);
	}

	// Draw a skydome.
	{
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);
		commandList->ClearRenderTargetView(gRenderer->CurrentRenderTargetCpuHandle(), &mBGColor.x, 0, nullptr);
	
		commandList->SetPipelineState(mSkydome.PSO);
		commandList->SetGraphicsRootSignature(mSkydome.RootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetGraphicsRootDescriptorTable(ToInt(SkydomeRootParameters::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SkydomeRootParameters::PerObject), mSkydome.CB.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(SkydomeRootParameters::Texture), mSkydome.Texture.SrvHandle().Gpu());
		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);
	
		mSkydome.Mesh.Draw(commandList, -1);
	
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
		gRenderer->CommitCommandList(commandList);
	}

	if (mTerrainIB.NumElements() > 0)
	{
		gRenderer->StartRendering();
		gRenderer->ClearDSV();

		commandList->SetPipelineState(mPSO);
		commandList->SetGraphicsRootSignature(mRootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::PerObject), mConstBufferPerObject.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::FrustumRanges), mConstBufferFrustumRanges.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::CascadeMatrices), mRenderPass.CB.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::DiffuseTexture), mTerrainTexture.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::DepthTextures), mRenderPass.DTADGpuHandle);
		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);
		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
		commandList->IASetVertexBuffers(0, 1, mTerrainVB.View());
		commandList->IASetIndexBuffer(mTerrainIB.View());
		commandList->DrawIndexedInstanced(mTerrainIB.NumElements(), 1, 0, 0, 0);

		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::DiffuseTexture), mSphereTexture.SrvHandle().Gpu());
		for (auto& sphere : mSpheres)
			sphere.Draw(commandList, ToInt(RenderPassRootParameters::PerObject));

		gRenderer->StopRendering();
	}

	if (mDepthPass.ShowTextures)
	{
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);

		DrawScreenQuad(commandList, heaps, _countof(heaps));

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
		gRenderer->CommitCommandList(commandList);
	}
}
