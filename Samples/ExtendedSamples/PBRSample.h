#pragma once
#include "StdafxSamples.h"
#include "Sample.h"
#include <LightSource.h>
#include <RHITexture.h>
#include "StaticObject.h"

class PBRSample : public Sample
{
public:
	PBRSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor) { }

	void Release() override final;
	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	bool IsUseOwnRendering() const { return true; }

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

protected:
	void LightSettings();
	void MaterialSettings();

protected:
	LightSource		mLightPoint;
	StaticObject	mShaderBall;

	struct CubeMapPass
	{
		void Release()
		{
			RHISafeRelease(RootSignature);
			RHISafeRelease(PSO);
			Cube.Release();
			Map.Release();
		}

		void Initialize(const String& folder);

		RHIRootSignature*	RootSignature	= nullptr;
		RHIPipelineState*	PSO				= nullptr;

		RHITexture		Map;
		StaticObject	Cube;
	} mCubeMapPass;

	struct Material
	{
		FORCEINLINE float& Metallic() { return Params.x; }
		FORCEINLINE float& Roughness() { return Params.y; }
		FORCEINLINE float& AO() { return Params.z; }

		DirectX::XMFLOAT4 Albedo		= DirectX::XMFLOAT4(0.8f, 0.2f, 0.2f, 1.0f);
		DirectX::XMFLOAT4 BaseFresnel	= DirectX::XMFLOAT4(0.04f, 0.04f, 0.04f, 1.0f);
		DirectX::XMFLOAT4 Params		= DirectX::XMFLOAT4(0.04f, 1.0f, 0.0f, 1.0f); //! x - metallic, y - roughness, z - ao.
	};

	RHIConstBuffer<Material> mTestMaterial;

protected:
	bool			mRotateLight = false;
};
