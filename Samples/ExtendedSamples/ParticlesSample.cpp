#include "StdafxSamples.h"
#include "ParticlesSample.h"
#include <Application.h>
#include <Logger.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHI.h>
#include <RHIUtils.h>
#include <RootSignature.h>
#include <Window.h>

#include <D3D12/private/DXMathHelper.h>
using namespace DirectX;
using namespace GraphicsCore;

namespace
{
	enum class ComputePassRootSignature
	{
		ConstantVariables = 0,
		DynamicVariables,
		Particles,
		Count
	};

	enum class RenderPassRootSignature
	{
		PerFrame = 0,
		ParticlesBuffer,
		ParticleTexture,
		Timer,
		Count
	};
}

/**
 * Function : Release
 */
void ParticlesSample::ParticlesBufferUavSrv::Release(IRenderer* renderer)
{
	RHISafeRelease(Buffer);
	renderer->CbvSrvUavHeapManager().FreeDescriptors(Descriptors);
}

/**
 * Function : Release
 */
void ParticlesSample::Release()
{
	Sample::Release();

	RHISafeRelease(mParticlesRenderPSO);
	RHISafeRelease(mParticlesRenderRootSignature);

	RHISafeRelease(mParticlesUpdatePSO);
	RHISafeRelease(mParticlesUpdateRootSignature);

	mParticlesBuffer.Release();

	mParticlesUavSrv.Release(gRenderer);

	mParticlesConstVarsBuffer.Release();
	mParticlesDynamVarsBuffer.Release();

	mParticleTexture.Release();
}

/**
 * Function : Initialize
 */
void ParticlesSample::Initialize()
{
	{
		mConstBufferGenSettingsData.Position		= XMFLOAT4(0.0f, 0.0f, 2.0f, 0.0f);
		mConstBufferGenSettingsData.MinVelocity		= XMFLOAT4(-5.0f, -5.0f, 0.0f, 0.0f);
		mConstBufferGenSettingsData.MaxVelocity		= XMFLOAT4(5.0f, 5.0f, 10.0f, 0.0f);
		mConstBufferGenSettingsData.GravityVector	= XMFLOAT4(0.0f, 0.0f, -5.0f, 0.0f);
		mConstBufferGenSettingsData.Color			= XMFLOAT4(0.2f, 0.4f, 0.8f, 0.0f);
		mConstBufferGenSettingsData.Settings		= XMFLOAT4(1.5f, 3.0f, 0.3f, 0.0f);
		mConstBufferGenSettingsData.Count			= 40;
		const float everySec = 0.2f;
	}

	Sample::Initialize();

	mCamera->SetPosition(XMFLOAT4(200.0f, 0.0f, 0.0f, 1.0f));
	mCamera->Angles().y = 0.0f;
}

/**
 * Function : InitializeRenderingResources
 */
void ParticlesSample::InitializeRenderingResources()
{
	InitializeMainRenderingResources();
	InitializeParticlesRenderingResources();
}

/**
 * Function : CreateRootSignatures
 */
void ParticlesSample::CreateRootSignatures()
{
	// Compute root signature.
	{
		RHIUtils::RootSignature rootSignature(3);
		rootSignature[ToInt(ComputePassRootSignature::ConstantVariables)].InitAsConstantBufferView(0);
		rootSignature[ToInt(ComputePassRootSignature::DynamicVariables)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
		rootSignature[ToInt(ComputePassRootSignature::Particles)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::UAV, 1, 0);

		mParticlesUpdateRootSignature = rootSignature.Finalize(GetRootSignatureFlags(false, false, false, false, false, false, false));
	}

	// Render root signature.
	{
		RHIUtils::RootSignature rootSignature(ToInt(RenderPassRootSignature::Count), 1);
		rootSignature[ToInt(RenderPassRootSignature::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Geometry).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(RenderPassRootSignature::ParticlesBuffer)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);
		rootSignature[ToInt(RenderPassRootSignature::ParticleTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 1);
		rootSignature[ToInt(RenderPassRootSignature::Timer)].InitAsConstantBufferView(1);

		RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(0);
		samplerDesc.Filter					= RHIFilter::MinMagMipLinear;
		samplerDesc.AddressU				= RHITextureAddressMode::Wrap;
		samplerDesc.AddressV				= RHITextureAddressMode::Wrap;
		samplerDesc.AddressW				= RHITextureAddressMode::Wrap;
		samplerDesc.MipLODBias				= 0;
		samplerDesc.MaxAnisotropy			= 0;
		samplerDesc.ComparisonFunc			= RHIComparisonFunc::Always;
		samplerDesc.BorderColor				= RHIStaticBorderColor::TransparentBlack;
		samplerDesc.MinLOD					= 0.0f;
		samplerDesc.MaxLOD					= RHIConstants::kFloat32Max;
		samplerDesc.ShaderRegister			= 0;
		samplerDesc.RegisterSpace			= 0;
		samplerDesc.ShaderVisibility		= RHIShaderVisibility::Pixel;

		mParticlesRenderRootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, true, true, false));
	}
}

/**
 * Function : CreatePipelineStates
 */
void ParticlesSample::CreatePipelineStates()
{
	// Compute pso.
	{
		RHIBlobWrapper computeShader(gRenderer->LoadShader(__TEXT("Particles:ParticlesUpdate.compute"), RHIShaderType::Compute));

		if (computeShader.Blob->IsNull())
			return;

		RHIComputePipelineStateDesc psoDesc	= {};
		psoDesc.RootSignature				= mParticlesUpdateRootSignature;
		psoDesc.CS							= { computeShader.GetBufferPointer(), computeShader.GetBufferSize() };

		mParticlesUpdatePSO = gRenderer->Device()->CreateComputePipelineState(&psoDesc);
	}

	// Render pso.
	{
		RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("Particles:ParticlesRender.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("Particles:ParticlesRender.ps"), RHIShaderType::Pixel));
		RHIBlobWrapper geometryShader	(gRenderer->LoadShader(__TEXT("Particles:ParticlesRender.gs"), RHIShaderType::Geometry));

		if (vertexShader.Blob->IsNull() || fragmentShader.Blob->IsNull() || geometryShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

		psoDesc.RootSignature										= mParticlesRenderRootSignature;
		psoDesc.InputLayout											= { InputLayouts::ParticlesRender::inputElementDesc, _countof(InputLayouts::ParticlesRender::inputElementDesc) };
		psoDesc.InputLayout											= { nullptr, 0 };
		psoDesc.VS													= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS													= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
		psoDesc.GS													= { geometryShader.GetBufferPointer(), geometryShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType								= RHIPrimitiveTopologyType::Point;
		psoDesc.SampleMask											= UINT_MAX;
		psoDesc.NumRenderTargets									= 1;
		psoDesc.RTVFormats[0]										= gRenderer->RenderTargetFormat();
		psoDesc.DSVFormat											= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count									= 1;

		psoDesc.DepthStencilState.DepthEnable						= TRUE;
		psoDesc.DepthStencilState.DepthFunc							= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask					= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable						= FALSE;

		psoDesc.RasterizerState										= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode							= RHICullMode::None;
		psoDesc.RasterizerState.FrontCounterClockwise				= FALSE;
		psoDesc.RasterizerState.FillMode							= RHIFillMode::Solid;

		psoDesc.BlendState											= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.IndependentBlendEnable					= false;
		psoDesc.BlendState.RenderTarget[0].BlendEnable				= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp					= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha				= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend					= RHIBlend::One;
		psoDesc.BlendState.RenderTarget[0].DestBlend				= RHIBlend::One;
		psoDesc.BlendState.RenderTarget[0].SrcBlendAlpha			= RHIBlend::One;
		psoDesc.BlendState.RenderTarget[0].DestBlendAlpha			= RHIBlend::Zero;
		psoDesc.BlendState.RenderTarget[0].RenderTargetWriteMask	= (UINT8)RHIColorWriteEnable::All;

		mParticlesRenderPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : InitializeRenderingResource
 */
void ParticlesSample::InitializeMainRenderingResources()
{
	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, false);
}

/**
 * Function : InitializeParticlesRenderingResources
 */
void ParticlesSample::InitializeParticlesRenderingResources()
{
	const UINT64 particlesSizeInBytes = mMaxParticles * sizeof(ParticleVertex);
	// Create vertices buffer.
	{
		InitializeInitialStateParticles();
		mParticlesBuffer.Initialize(__TEXT("Particles:ParticlesBuffer"), particlesSizeInBytes, mParticles.size(), sizeof(ParticleVertex), mParticles.data());
	}

	// Load a particle texture.
	mParticleTexture.InitializeByName(__TEXT("Textures:Particle.png"));

	// Create an unoredered access view.
	{
		const UINT64 test = mParticles.size() * sizeof(ParticleVertex);
		ParticlesBufferUavSrv& p = mParticlesUavSrv;
		p.Buffer = gRenderer->Device()->CreateCommittedResource(
			&RHIHeapProperties(RHIHeapType::Default),
			RHIHeapFlag::None,
			&RHIResourceDesc::Buffer(mParticles.size() * sizeof(ParticleVertex), RHIResourceFlag::AllowUnorderedAccess),
			RHIResourceState::CopyDest,
			nullptr);
		p.Buffer->SetName(__TEXT("UAV/SRV particles buffer"));

		RHISubresourceData data	= {};
		data.Data				= mParticles.data();
		data.RowPitch			= mParticles.size() * sizeof(ParticleVertex);
		data.SlicePitch			= data.RowPitch;

		gRenderer->UploadCommandList()->Reset(gRenderer->UploadCommandAllocator(), nullptr);
		gRenderer->Device()->UpdateSubresources(1, gRenderer->UploadCommandList(), p.Buffer, gRenderer->UploadBuffer(), 0, 0, 1, &data);
		gRenderer->CloseAndWait(gRenderer->UploadCommandList());

		gRenderer->ChangeResourceBarrier(1, &RHIResourceBarrier::MakeTransition(p.Buffer, RHIResourceState::CopyDest, RHIResourceState::NonPixelShaderResource));

		p.Descriptors	= gRenderer->CbvSrvUavHeapManager().GetNewDescriptors(2);
		p.SrvCpuHandle	= gRenderer->CbvSrvUavHeapManager().CpuHandle(p.Descriptors[0]);
		p.SrvGpuHandle	= gRenderer->CbvSrvUavHeapManager().GpuHandle(p.Descriptors[0]);
		p.UavCpuHandle	= gRenderer->CbvSrvUavHeapManager().CpuHandle(p.Descriptors[1]);
		p.UavGpuHandle	= gRenderer->CbvSrvUavHeapManager().GpuHandle(p.Descriptors[1]);

		RHIShaderResourceViewDesc srvDesc	= {};
		srvDesc.Shader4ComponentMapping		= D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.Format						= RHIFormat::Unknown;
		srvDesc.ViewDimension				= RHISRVDimension::Buffer;
		srvDesc.Buffer.FirstElement			= 0;
		srvDesc.Buffer.NumElements			= mParticles.size();
		srvDesc.Buffer.StructureByteStride	= sizeof(ParticleVertex);
		srvDesc.Buffer.Flags				= RHIBufferSRVFlags::None;

		gRenderer->Device()->CreateShaderResourceView(p.Buffer, &srvDesc, p.SrvCpuHandle);

		RHIUnorderedAccessViewDesc uavDesc	= {};
		uavDesc.Format						= RHIFormat::Unknown;
		uavDesc.ViewDimension				= RHIUAVDimension::Buffer;
		uavDesc.Buffer.FirstElement			= 0;
		uavDesc.Buffer.NumElements			= mParticles.size();
		uavDesc.Buffer.StructureByteStride	= sizeof(ParticleVertex);
		uavDesc.Buffer.CounterOffsetInBytes	= 0;
		uavDesc.Buffer.Flags				= RHIBufferUAVFlags::None;

		gRenderer->Device()->CreateUnorderedAccessView(p.Buffer, nullptr, &uavDesc, p.UavCpuHandle);
	}

	mParticlesConstVarsBuffer.Initialize(__TEXT("Particles:CBConstVarsBuffer"), RHIUtils::GetConstBufferSize(), 1, sizeof(mParticlesConstVarsBuffer.Data()));
	mParticlesDynamVarsBuffer.Initialize(__TEXT("Particles:CBDynamVarsBuffer"), RHIUtils::GetConstBufferSize(), 1, sizeof(mParticlesDynamVarsBuffer.Data()));
	mParticlesTimer.Initialize(__TEXT("Particles:Timer"), RHIUtils::GetConstBufferSize(), 1, sizeof(mParticlesTimer.Data()));

	mParticlesConstVarsBuffer.Data().Params.x = mParticles.size();
	mParticlesConstVarsBuffer.UpdateDataOnGPU();

	mParticlesTimer.Data().Time = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);

	mParticles.clear();
}

/**
 * Function : InitializeInitialStateParticles
 */
void ParticlesSample::InitializeInitialStateParticles()
{
	mParticles.resize(mCurrentNbOfParticles);

	std::default_random_engine rndEngine(time(NULL));
	std::uniform_real_distribution<float> distr(-mInitialRadius, mInitialRadius);

	std::default_random_engine colorEng(time(NULL));
	std::uniform_real_distribution<float> colorDistr(0.2f, 1.0f);

	for (UINT id = 0; id < mCurrentNbOfParticles; ++id)
	{
		mParticles[id].CurrPosition = mConstBufferGenSettingsData.Position + XMFLOAT4(distr(rndEngine), distr(rndEngine), distr(rndEngine), 1.0f);
		mParticles[id].PrevPosition = mParticles[id].CurrPosition;
	}

	mRenderNbOfParticles = mCurrentNbOfParticles;
}

/**
 * Function : Update
 */
void ParticlesSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	// Update the attractor info.
	{
		mParticlesDynamVarsBuffer.Data().Attractor = MakeFloat4(BuildAttractor(mCamera.get(), 0, 0, 1), deltaTime);
		mParticlesDynamVarsBuffer.UpdateDataOnGPU();
	}

	// Update the timer.
	{
		mParticlesTimer.Data().Time.x += deltaTime;
		mParticlesTimer.UpdateDataOnGPU();
	}
}

/**
 * Function : Render
 */
void ParticlesSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };

	// Update particles.
	commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mParticlesUavSrv.Buffer, RHIResourceState::NonPixelShaderResource, RHIResourceState::UnorderedAccess));
	commandList->SetPipelineState(mParticlesUpdatePSO);
	commandList->SetComputeRootSignature(mParticlesUpdateRootSignature);
	commandList->SetDescriptorHeaps(_countof(heaps), heaps);
	commandList->SetComputeRootConstantBufferView(ToInt(ComputePassRootSignature::ConstantVariables), mParticlesConstVarsBuffer.GpuVirtualAddress());
	commandList->SetComputeRootDescriptorTable(ToInt(ComputePassRootSignature::DynamicVariables), mParticlesDynamVarsBuffer.SrvHandle().Gpu());
	commandList->SetComputeRootDescriptorTable(ToInt(ComputePassRootSignature::Particles), mParticlesUavSrv.UavGpuHandle);
	commandList->Dispatch(mCurrentNbOfParticles / 128 + 1, 1, 1);
	commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mParticlesUavSrv.Buffer, RHIResourceState::UnorderedAccess, RHIResourceState::NonPixelShaderResource));

	// Render particles.
	const float blendFactor[4] = { 1.25f, 1.25f, 1.25f, 0.0f };
	commandList->OMSetBlendFactor(blendFactor);
	commandList->SetPipelineState(mParticlesRenderPSO);
	commandList->SetGraphicsRootSignature(mParticlesRenderRootSignature);
	commandList->SetDescriptorHeaps(_countof(heaps), heaps);
	commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootSignature::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
	commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootSignature::ParticlesBuffer), mParticlesUavSrv.SrvGpuHandle);
	commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootSignature::ParticleTexture), mParticleTexture.SrvHandle().Gpu());
	commandList->SetGraphicsRootConstantBufferView(ToInt(RenderPassRootSignature::Timer), mParticlesTimer.GpuVirtualAddress());
	commandList->RSSetViewports(1, &mViewport);
	commandList->RSSetScissorRects(1, &mScissorRect);
	commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::PointList);
	commandList->IASetVertexBuffers(0, 0, nullptr);
	commandList->DrawInstanced(mRenderNbOfParticles, 1, 0, 0);
}

/**
 * Function : RenderImGui
 */
void ParticlesSample::RenderImGui()
{
	ImGui::Text("Particles: %d", mCurrentNbOfParticles);
	/*ImGui::Text("Max particles: %u", mMaxParticles);
	if (ImGui::Button("Rebuild"))
	{
		InitializeInitialStateParticles();
	}
	ImGui::SameLine();
	//ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.5f);
	ImGui::SliderInt("Particles", &mCurrentNbOfParticles, 10000, mMaxParticles);*/
}

/**
 * Function : BuildAttractor
 */
DirectX::XMFLOAT3 ParticlesSample::BuildAttractor(Camera* camera, const int absoluteMouseX, const int absoluteMouseY, const int absoluteMouseZ)
{
	XMVECTOR lookTo, up, right, pos, attractor;
	attractor = XMVectorSet(0, 0, 0, 0);
	lookTo = XMLoadFloat4(&camera->Direction());
	//lookTo = XMVectorSet(1.0f, 0.0f, -1.0f, 1.0f);
	up = XMLoadFloat4(&camera->UpVector());
	right = XMVector3Cross(up, lookTo);
	pos = XMLoadFloat4(&camera->Position());

	XMVECTOR topleft, topright, bottomleft, bottomright;

	XMMATRIX rotY = XMMatrixRotationAxis(up, (float)(XM_PIDIV4 / 1.7));
	XMMATRIX rotX = XMMatrixRotationAxis(right, (float)(XM_PIDIV4 / 2.));
	XMMATRIX rot_Y = XMMatrixRotationAxis(up, (float)(-XM_PIDIV4 / 1.7));
	XMMATRIX rot_X = XMMatrixRotationAxis(right, (float)(-XM_PIDIV4 / 2.));

	topright = XMVector3Transform(lookTo, rotY);
	topright = XMVector3Transform(topright, rot_X);

	topleft = XMVector3Transform(lookTo, rot_Y);
	topleft = XMVector3Transform(topleft, rot_X);

	bottomright = XMVector3Transform(lookTo, rotY);
	bottomright = XMVector3Transform(bottomright, rotX);

	bottomleft = XMVector3Transform(lookTo, rot_Y);
	bottomleft = XMVector3Transform(bottomleft, rotX);

	topright	*= (float)absoluteMouseZ;
	topleft		*= (float)absoluteMouseZ;
	bottomright	*= (float)absoluteMouseZ;
	bottomleft	*= (float)absoluteMouseZ;

	right	= topleft - topright;
	up		= topleft - bottomleft;

	float normMouseX = (float)(-absoluteMouseX) / ((float)(mApp->GetWindow()->Width()));
	float normMouseY = (float)(-absoluteMouseY) / ((float)(mApp->GetWindow()->Height()));

	attractor += pos + topleft + normMouseX*right + normMouseY*up;

	XMFLOAT3 result;
	XMStoreFloat3(&result, attractor);

	return result;
}
