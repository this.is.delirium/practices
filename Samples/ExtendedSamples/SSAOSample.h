#pragma once
#include <StdafxSamples.h>
#include <Sample.h>
#include <MRTPass.h>
#include <RHITexture.h>
#include <StaticObject.h>

// TODO: start SSAO -> OIT -> Deferred Shading (Bug: the texture of normals is null).
// TODO: start SSAO -> Deferred Shading (Bug: The screen squad is a mad).
// TODO: start SSAO -> Shadow mapping (Bug: the screen squad is a mad).

/**
 * Positions and normals need to be in a view space.
 * The occlusion contribution of each occluder depends on two factors:
 *  0. Distance 'd' to the occludee.
 *  1. Angle between the occludee's normal 'N' and the vector between occluder and occludee V.
 *  Simple formula: Occlusion = max(0.0f, dot(N, V)) * 1.0f / (1.0f + d); - linear attenuation.

 */
class SSAOSample : public Sample
{
public:
	SSAOSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor, true, DirectX::XMFLOAT2(300.0f, 300.0f)) { }

	void Release() override final;
	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	bool IsUseOwnRendering() const { return true; }

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

	void CreateRenderTargets();

protected:
	bool mIsRandomScene = false;

	struct Room
	{
		void Release();
		void Initialize(const bool isRandom);

		enum class ID
		{
			Floor = 0,
			Ceiling,
			LeftWall,
			RightWall,
			BackWall,
			Cube1,
			Cube2,
			Cube3,
			Cube4,
			Cube5,
			Count
		};

		StaticObject Objects[ToInt(ID::Count)];
	} mRoom;

	MRTPass			mGPass;
	RHIVertexBuffer	mFullScreenQuad;

	struct SSAOPass
	{
		void Release();

		// Creates all rendering resources except RS and PSO.
		void Initialize();

		void FillKernel(const int kernelSize, ConstantBuffers::SSAO::Kernel& kernel);
		void FillNoise(const int noiseSize, RHITexture& noise);

		RHIRootSignature*		RootSignature	= nullptr;
		RHIPipelineState*		PSO				= nullptr;

		RHIFormat				RtFormat		= RHIFormat::R32G32B32A32Float;
		RHITexture				Result;

		ConstBufferSSAOKernel	KernelBuffer;
		RHITexture				NoiseTexture;

		ConstBufferSSAOParams	Params;
	} mSSAOPass;

	struct BlurPass
	{
		void Release()
		{
			RHISafeRelease(RootSignature);
			RHISafeRelease(PSO);

			GaussKernelCB.Release();
		}

		RHIRootSignature*			RootSignature	= nullptr;
		RHIPipelineState*			PSO				= nullptr;

		ConstBufferSSAOGaussKernel	GaussKernelCB;
		float						Sigma			= 1.0f;
	} mBlurPass;
};
