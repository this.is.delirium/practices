#include <StdafxSamples.h>
#include "ShadowMapSample.h"

#include <Application.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHIUtils.h>
#include <StaticGeometryBuilder.h>

#include <ECS/MeshComponent.h>
#include <ECS/SceneComponent.h>

#include <D3D12/private/DXMathHelper.h>
using namespace DirectX;
using namespace GraphicsCore;

namespace
{
	enum class DepthPassRootParameters
	{
		PerFrame = 0,
		PerObject = 1
	};

	enum class RenderPassRootParameters
	{
		PerFrame = 0,
		PerObject = 1,
		Light = 2,
		DepthTexture = 3
	};
}

/**
 * Function : Release
 */
void ShadowMapSample::Release()
{
	Sample::Release();

	if (mFloor.get())
		mFloor->Release();

	mLightCube.Release();

	for (auto& cube : mCubes)
	{
		cube->Release();
	}

	RHISafeRelease(mDepthPassPSO);
	RHISafeRelease(mDepthPassRootSignature);

	mLightConstBufferPerFrame.Release();
	mDepthTexture.Release();

	mLightPoint.Release();
}

/**
 * Function : Initialize
 */
void ShadowMapSample::Initialize()
{
	Sample::Initialize();

	mCamera->SetPosition(XMFLOAT4(300.0f, 10.0f, 70.0f, 1.0f));

	mApp->AddViewFrustum(mLightPoint.GetCamera()->GetViewFrustum());

	mScreenQuad->SetTexture(mDepthTexture.SrvHandle().Gpu(), ScreenQuadPart::TextureType::Depth);
}

/**
 * Function : SetCameras
 */
void ShadowMapSample::SetCameras()
{
	Sample::SetCameras();

	{
		Camera::CameraDesc desc;
		desc.Common.Position			= XMFLOAT4(0.0f, 50.0f, 100.0f, 1.0f);
		desc.Common.ZRange				= XMFLOAT2(0.1f, 300.0f);

		desc.ProjType					= Camera::ProjectionType::Perspective;
		desc.PerspPart.FoV				= 45.0f;
		desc.PerspPart.AspectRatio		= mApp->GetWindow()->AspectRatio();

		desc.Format						= Camera::InitializeFormat::FromSphericalAngles;
		desc.AnglesPart.AngleXRange		= XMFLOAT2(-XM_PI, XM_PI);
		desc.AnglesPart.AngleYRange		= XMFLOAT2(-XM_PIDIV2, XM_PIDIV2);
		desc.AnglesPart.SphericalAngles	= XMFLOAT2(-XM_PIDIV2, -XM_PIDIV2 * 0.5f);

		mLightPoint.GetCamera() = std::make_shared<Camera>();
		mLightPoint.GetCamera()->Initialize(desc);

		mLightPoint.GetCamera()->SetName("Light point camera");
		mApp->AddCamera(mLightPoint.GetCamera());
	}
}

/**
 * Function : InitializeRenderingResources
 */
void ShadowMapSample::InitializeRenderingResources()
{
	// Create constants buffer for a light source.
	{
		const UINT size = 1024 * 4;
		mLightConstBufferPerFrame.Initialize(__TEXT("ShadowMap:LightCBPerFrame"), size, 1, sizeof(mLightConstBufferPerFrame.Data()));

		mLightPoint.Initialize();

		mLightPoint.SetType(LightSourceType::Point);
		mLightPoint.SetAmbient(XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
		mLightPoint.SetDiffuse(XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
		mLightPoint.SetSpecular(XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));

		ConstBufferForLight& cbLight	= mLightPoint.ConstBuffer();
		cbLight.Data().Position			= mLightPoint.GetCamera()->Position();
		cbLight.Data().ZRange			= MakeFloat4(mLightPoint.GetCamera()->ZRange(), 0.0f, 0.0f);
		cbLight.Data().Matrix			= mLightPoint.GetCamera()->ViewProjectionMatrix();
		cbLight.UpdateDataOnGPU();
	}

	// Create a floor.
	{
		mFloor = std::make_shared<StaticObject>();
		mFloor->Initialize(false, false);
		StaticGeometryBuilder::LoadPlane(mFloor->GetMeshComponent(), mFloor->GetSceneComponent());
		mFloor->GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, 0.0f, -5.0f));
		mFloor->GetSceneComponent()->SetScale(XMFLOAT3(300.0f, 300.0f, 300.0f));
	}

	// Create a light frustum.
	{
		XMFLOAT4 colors[] =
		{
			XMFLOAT4(1.0f, 0.0f, 0.0f, 0.5f),
			XMFLOAT4(0.0f, 1.0f, 0.0f, 0.5f),
			XMFLOAT4(0.0f, 0.0f, 1.0f, 0.5f),
			XMFLOAT4(1.0f, 1.0f, 0.0f, 0.5f)
		};

		Camera::ViewFrustumDesc viewFrustumDesc;
		viewFrustumDesc.Lambda		= 0.5f;
		viewFrustumDesc.ViewCamera	= mCamera.get();
		viewFrustumDesc.NbOfSplit	= _countof(colors);
		memcpy(viewFrustumDesc.Colors, colors, sizeof(colors));

		mLightPoint.GetCamera()->InitializeViewFrustum(viewFrustumDesc);
	}

	// Create a cube for represent light point in a scene.
	{
		mLightCube.Initialize(false, false);
		StaticGeometryBuilder::LoadCube(mLightCube.GetMeshComponent(), mLightCube.GetSceneComponent());

		const XMFLOAT4& pos = mLightPoint.GetCamera()->Position();
		mLightCube.GetSceneComponent()->SetPosition(XMFLOAT3(pos.x, pos.y, pos.z));
	}

	// Create cubes.
	{
		const int numX			= 5;
		const int numY			= 5;
		const int numZ			= 5;
		const float scale		= 3.0f;
		const float distance	= 9.0f;
		const float tmp			= scale + distance;
		const XMFLOAT3 startPosition(-numX * tmp * 0.5f, -numY * tmp * 0.5f, 0.0f);
		for (int z = 0; z < numZ; ++z)
		{
			for (int y = 0; y < numY; ++y)
			{
				for (int x = 0; x < numX; ++x)
				{
					std::shared_ptr<StaticObject> cube = std::make_shared<StaticObject>();

					cube->Initialize(false, false);
					StaticGeometryBuilder::LoadCube(cube->GetMeshComponent(), cube->GetSceneComponent());

					cube->GetSceneComponent()->SetPosition(startPosition + XMFLOAT3(x * tmp, y * tmp, z * tmp));
					cube->GetSceneComponent()->SetScale(XMFLOAT3(scale, scale, scale));

					mCubes.push_back(cube);
				}
			}
		}
	}

	// Create constant buffers.
	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, false);

	// Create a depth texture.
	CreateDepthTexture();
}

/**
 * Function : CreateRootSignatures
 */
void ShadowMapSample::CreateRootSignatures()
{
	// Create a root signature for a depth pass.
	{
		RHIDescriptorRange ranges[2];
		ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);
		ranges[1].Init(RHIDescriptorRangeType::CBV, 1, 1);

		RHIRootParameter rootParameters[2];
		rootParameters[ToInt(DepthPassRootParameters::PerFrame)].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::Vertex);
		rootParameters[ToInt(DepthPassRootParameters::PerObject)].InitAsDescriptorTable(1, &ranges[1], RHIShaderVisibility::Vertex);

		RHIRootSignatureDesc rootSignatureDesc = {};
		rootSignatureDesc.Init(_countof(rootParameters), &rootParameters[0], 0, nullptr, RHIRootSignatureFlags::AllowInputAssemblerInputLayout);

		mDepthPassRootSignature = gRenderer->SerializeAndCreateRootSignature(&rootSignatureDesc, RHIRootSignatureVersion::Ver1);
	}

	// Create a root signature for a render pass.
	{
		RHIDescriptorRange ranges[4];
		ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);
		ranges[1].Init(RHIDescriptorRangeType::CBV, 1, 1);
		ranges[2].Init(RHIDescriptorRangeType::CBV, 1, 2);
		ranges[3].Init(RHIDescriptorRangeType::SRV, 1, 0);

		RHIRootParameter rootParameters[4];
		rootParameters[ToInt(RenderPassRootParameters::PerFrame)].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::All);
		rootParameters[ToInt(RenderPassRootParameters::PerObject)].InitAsDescriptorTable(1, &ranges[1], RHIShaderVisibility::Vertex);
		rootParameters[ToInt(RenderPassRootParameters::Light)].InitAsDescriptorTable(1, &ranges[2], RHIShaderVisibility::All);
		rootParameters[ToInt(RenderPassRootParameters::DepthTexture)].InitAsDescriptorTable(1, &ranges[3], RHIShaderVisibility::Pixel);

		RHIStaticSamplerDesc samplerDesc	= {};
		samplerDesc.Filter					= RHIFilter::ComparisonMinMagMipLinear;
		// We require a clamp based sampler when sampling the depth buffer so that it doesn't wrap around and sample incorrect information.
		samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc.MipLODBias				= 0;
		samplerDesc.MaxAnisotropy			= 0;
		samplerDesc.ComparisonFunc			= RHIComparisonFunc::Less;
		samplerDesc.BorderColor				= RHIStaticBorderColor::TransparentBlack;
		samplerDesc.MinLOD					= 0.0f;
		samplerDesc.MaxLOD					= RHIConstants::kFloat32Max;
		samplerDesc.ShaderRegister			= 0;
		samplerDesc.RegisterSpace			= 0;
		samplerDesc.ShaderVisibility		= RHIShaderVisibility::Pixel;

		RHIRootSignatureDesc rootSignatureDesc = {};
		rootSignatureDesc.Init(_countof(rootParameters), &rootParameters[0], 1, &samplerDesc, RHIRootSignatureFlags::AllowInputAssemblerInputLayout);

		mRootSignature = gRenderer->SerializeAndCreateRootSignature(&rootSignatureDesc, RHIRootSignatureVersion::Ver1);
	}
}

/**
 * Function : CreatePipelineStates
 */
void ShadowMapSample::CreatePipelineStates()
{
	// Create a pso for a depth pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("ShadowMapping:DepthPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("ShadowMapping:DepthPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

		psoDesc.RootSignature									= mDepthPassRootSignature;
		psoDesc.InputLayout										= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS												= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS												= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType							= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask										= UINT_MAX;
		psoDesc.NumRenderTargets								= 0;
		psoDesc.DSVFormat										= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count								= 1;

		psoDesc.DepthStencilState.DepthEnable					= TRUE;
		psoDesc.DepthStencilState.DepthFunc						= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask				= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable					= TRUE;
		psoDesc.DepthStencilState.StencilReadMask				= 0;
		psoDesc.DepthStencilState.StencilWriteMask				= 0xff;
		psoDesc.DepthStencilState.FrontFace.StencilFailOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.FrontFace.StencilDepthFailOp	= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.FrontFace.StencilPassOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.FrontFace.StencilFunc			= RHIComparisonFunc::Always;
		psoDesc.DepthStencilState.BackFace.StencilFailOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.BackFace.StencilDepthFailOp	= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.BackFace.StencilPassOp		= RHIStencilOp::Zero;
		psoDesc.DepthStencilState.BackFace.StencilFunc			= RHIComparisonFunc::Always;

		psoDesc.RasterizerState									= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise			= false;
		psoDesc.RasterizerState.FillMode						= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode						= RHICullMode::None;

		psoDesc.BlendState										= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable			= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp				= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha			= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend				= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend			= RHIBlend::InvSrcAlpha;

		mDepthPassPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// Create a pso for a render pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("ShadowMapping:RenderPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("ShadowMapping:RenderPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

		psoDesc.RootSignature							= mRootSignature;
		psoDesc.InputLayout								= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.DSVFormat								= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= TRUE;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable			= FALSE;
		psoDesc.DepthStencilState.BackFace.StencilFunc	= RHIComparisonFunc::Always;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise	= FALSE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp		= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha	= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend		= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend	= RHIBlend::InvSrcAlpha;

		mPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : CreateDepthTexture
 */
void ShadowMapSample::CreateDepthTexture()
{
	mDepthTextureSize			= ParseTextureQuality(mDepthTextureQuality);

	mDepthViewport				= {};
	mDepthViewport.Width		= mDepthTextureSize.x;
	mDepthViewport.Height		= mDepthTextureSize.y;
	mDepthViewport.MaxDepth		= 1.0f;

	mDepthScissorRect			= {};
	mDepthScissorRect.right		= mDepthTextureSize.x;
	mDepthScissorRect.bottom	= mDepthTextureSize.y;

	mDepthTexture.Release();

	// Describe a resource.
	RHIFormat format		= RHIFormat::R24G8Typeless;
	RHIFormat viewFormat	= RHIFormat::D24UnormS8Uint;
	RHIFormat srvFormat		= RHIFormat::R24UnormX8Typeless;

	RHIResourceDesc desc;
	ZeroMemory(&desc, sizeof(desc));

	desc.Dimension			= RHIResourceDimension::Texture2D;
	desc.Format				= format;
	desc.Width				= mDepthTextureSize.x;
	desc.Height				= mDepthTextureSize.y;
	desc.DepthOrArraySize	= 1;
	desc.MipLevels			= 1;
	desc.Alignment			= 0;
	desc.SampleDesc.Count	= 1;
	desc.SampleDesc.Quality	= 0;
	desc.Layout				= RHITextureLayout::Unknown;
	desc.Flags				= RHIResourceFlag::AllowDepthStencil;

	RHIClearValue clearValue;
	clearValue.Format				= viewFormat;
	clearValue.DepthStencil.Depth	= 1.0f;
	clearValue.DepthStencil.Stencil	= 0;

	// Create a resource.
	mDepthTexture.Initialize(&desc, &clearValue);
}

/**
 * Function : ParseTextureQuality
 */
glm::uvec2 ShadowMapSample::ParseTextureQuality(const TextureQuality quality)
{
	switch (quality)
	{
		case TextureQuality::Low:
		{
			return glm::uvec2(512, 512);
		}

		case TextureQuality::Medium:
		{
			return glm::uvec2(1024, 1024);
		}

		case TextureQuality::High:
		{
			return glm::uvec2(2048, 2048);
		}

		default:
		{
			SLogger::Instance().AddMessage(LogMessageType::Error, __TEXT("ParseTextureQuality doesn't support current Sample %d"), quality);
			return glm::uvec2(1, 1);
		}
	}
}

/**
 * Function : RenderImGui
 */
void ShadowMapSample::RenderImGui()
{
	ImGui::Text("A simple shadow mapping.");

	static const char* texts[] = { "low", "medium", "high" };
	if (ImGui::Combo("Texture quality", reinterpret_cast<int *>(&mDepthTextureQuality), texts, _countof(texts)))
	{
		CreateDepthTexture();
	}
	ImGui::Text("Depth texture size (%u, %u)", mDepthTextureSize.x, mDepthTextureSize.y);
	ImGui::Checkbox("Show light", &mIsShowLight);
}

/**
 * Function : Update
 */
void ShadowMapSample::Update(const float deltaTime)
{
	UpdateCameraResource();
	mScreenQuad->UpdateDepthParams(MakeFloat4(mLightPoint.GetCamera()->ZRange(), 0.0f, 0.0f));

	if (mLightPoint.GetCamera()->IsDirty())
	{
		mLightConstBufferPerFrame.Data().DepthParams	= MakeFloat4(mLightPoint.GetCamera()->ZRange(), 0.0f, 0.0f);
		mLightConstBufferPerFrame.Data().ViewMatrix		= mLightPoint.GetCamera()->LookAt();
		mLightConstBufferPerFrame.Data().ViewProjMatrix	= mLightPoint.GetCamera()->ViewProjectionMatrix();
		mLightConstBufferPerFrame.UpdateDataOnGPU();
	}

	mFloor->Update(deltaTime);
	mLightCube.Update(deltaTime);

	for (int i = 0; i < mCubes.size(); ++i)
	{
		const int rest = i % 5;
		if (rest == 0 || rest == 2 || rest == 4)
		{
			mCubes[i]->GetSceneComponent()->AddRotateToPitch(deltaTime);
		}
		else if (rest == 1)
		{
			mCubes[i]->GetSceneComponent()->AddRotateToRoll(deltaTime);
		}
		else
		{
			mCubes[i]->GetSceneComponent()->AddRotateToYaw(deltaTime);
		}

		mCubes[i]->Update(deltaTime);
	}
}

/**
 * Function : Render
 */
void ShadowMapSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };

	RHIResourceBarrier barriers[] =
	{
		RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget),
		RHIResourceBarrier::MakeTransition(mDepthTexture.Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::DepthWrite),

		RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present),
		RHIResourceBarrier::MakeTransition(mDepthTexture.Texture(), RHIResourceState::DepthWrite, RHIResourceState::PixelShaderResource)
	};

	// Depth pass.
	{
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &barriers[1]);
		commandList->OMSetRenderTargets(0, nullptr, false, &mDepthTexture.DsvHandle().Cpu());
		commandList->ClearDepthStencilView(mDepthTexture.DsvHandle().Cpu(), RHIClearFlags::Depth, 1.0f, 0, 0, nullptr);

		commandList->SetPipelineState(mDepthPassPSO);
		commandList->SetGraphicsRootSignature(mDepthPassRootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetGraphicsRootDescriptorTable(ToInt(DepthPassRootParameters::PerFrame), mLightConstBufferPerFrame.SrvHandle().Gpu());
		commandList->RSSetViewports(1, &mDepthViewport);
		commandList->RSSetScissorRects(1, &mDepthScissorRect);

		mFloor->Draw(commandList, ToInt(DepthPassRootParameters::PerObject));

		for (auto& cube : mCubes)
			cube->Draw(commandList, ToInt(DepthPassRootParameters::PerObject));

		commandList->ResourceBarrier(1, &barriers[3]);
		gRenderer->CommitCommandList(commandList);
	}

	// Render pass.
	{
		gRenderer->StartRendering();
		gRenderer->ClearRTV(&mBGColor.x);
		gRenderer->ClearDSV();

		commandList->SetPipelineState(mPSO);
		commandList->SetGraphicsRootSignature(mRootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::Light), mLightPoint.ConstBuffer().SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::DepthTexture), mDepthTexture.SrvHandle().Gpu());
		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		mFloor->Draw(commandList, ToInt(RenderPassRootParameters::PerObject));

		if (mIsShowLight)
			mLightCube.Draw(commandList, ToInt(RenderPassRootParameters::PerObject));

		for (auto& cube : mCubes)
			cube->Draw(commandList, ToInt(RenderPassRootParameters::PerObject));

		gRenderer->StopRendering();
	}

	// Draw a screen quad.
	{
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &barriers[0]);
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);

		DrawScreenQuad(commandList, heaps, _countof(heaps));

		commandList->ResourceBarrier(1, &barriers[2]);
		gRenderer->CommitCommandList(commandList);
	}
}
