#pragma once
#include "StdafxSamples.h"
#include <Sample.h>
#include <LightSource.h>
#include <MRTPass.h>
#include <StaticObject.h>
#include <RHITexture.h>
#include <RHIVertexBuffer.h>

/**
 * See more: http://kayru.org/articles/deferred-stencil/
 */
class DeferredShadingSample : public Sample
{
public:
	DeferredShadingSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor, true, DirectX::XMFLOAT2(300.0f, 300.0f)) { }

	void Release() override final;

	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	bool IsUseOwnRendering() const { return true; }

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

	void CreateRenderTargets();
	void CreateScene();
	void CreateLights();
	void CreateSunQuad();
	void LoadTextures();

protected:
	MRTPass mGPass;

	struct SunPass
	{
		void Release()
		{
			RHISafeRelease(RootSignature);
			RHISafeRelease(PSO);
			VertexBuffer.Release();
		}

		RHIRootSignature*	RootSignature	= nullptr;
		RHIPipelineState*	PSO				= nullptr;
		RHIVertexBuffer		VertexBuffer;
	} mSunPass;

	// This pass creates a Stencil mask for the areas of the light volume that are not occluded by scene geometry.
	struct FirstLightPass
	{
		void Release()
		{
			RHISafeRelease(RootSignature);
			RHISafeRelease(PSO);
		}

		RHIRootSignature*	RootSignature	= nullptr;
		RHIPipelineState*	PSO				= nullptr;
	} mFirstLightPass;

	// This pass is where lighting actually happens. Every pixel that passes Z and Stencil tests is then added to light accumulation buffer.
	struct SecondLightPass
	{
		void Release()
		{
			RHISafeRelease(RootSignature);
			RHISafeRelease(PSO);
		}

		RHIRootSignature*	RootSignature	= nullptr;
		RHIPipelineState*	PSO				= nullptr;
	} mSecondLightPass;

	RHITexture						mFloorTexture;
	RHITexture						mSphereTexture;

	std::vector<LightSource>		mLightPoints;
	std::shared_ptr<StaticObject>	mFloor;
	std::vector<StaticObject>		mSpheres;
	DirectX::XMFLOAT3				mSceneSize;
	DirectX::XMFLOAT3				mStartPosition;
	bool							mIsRandomScene = false;
};
