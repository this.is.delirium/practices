#include "StdafxSamples.h"
#include "VarianceShadowMappingSample.h"

#include <Application.h>
#include <Render/GraphicsCore.h>
#include <RootSignature.h>
#include <StaticGeometryBuilder.h>
#include <Window.h>

#include <ECS/MeshComponent.h>
#include <ECS/SceneComponent.h>

#include <D3D12/private/DXMathHelper.h>
using namespace DirectX;
using namespace GraphicsCore;

namespace
{
	enum class DepthPassRootParameters
	{
		PerFrame = 0,
		PerObject,
		Count
	};

	enum class RenderPassRootParameters
	{
		PerFrame = 0,
		PerObject,
		Light,
		VarianceParams,
		DepthTexture,
		Count
	};
}

/**
 * Function : Release
 */
void VarianceShadowMappingSample::Release()
{
	Sample::Release();

	mDepthPass.Release();
	mObjects.Release();
}

/**
 * Function : Initialize
 */
void VarianceShadowMappingSample::Initialize()
{
	Sample::Initialize();

	mCamera->SetPosition(XMFLOAT4(200.0f, 10.0f, 70.0f, 1.0f));

	mScreenQuad->SetTexture(mDepthPass.RT.SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);
}

/**
 * Function : InitializeRenderingResources
 */
void VarianceShadowMappingSample::InitializeRenderingResources()
{
	// Create a RT.
	{
		RHISwapChainDesc scDesc;
		gRenderer->SwapChain()->GetDesc(&scDesc);
		XMUINT2 size(scDesc.BufferDesc.Width, scDesc.BufferDesc.Height);

		RHIResourceDesc desc	= {};
		desc.Dimension			= RHIResourceDimension::Texture2D;
		desc.Width				= size.x;
		desc.Height				= size.y;
		desc.DepthOrArraySize	= 1;
		desc.MipLevels			= 1;
		desc.SampleDesc.Count	= 1;
		desc.Layout				= RHITextureLayout::Unknown;
		desc.Flags				= RHIResourceFlag::AllowRenderTarget;
		desc.Format				= mDepthPass.Format;

		RHIClearValue& clearValue	= mDepthPass.ClearValue;
		clearValue					= {};
		clearValue.Format			= mDepthPass.Format;
		clearValue.Color[0]			= 0.0f;
		clearValue.Color[1]			= 0.0f;
		clearValue.Color[2]			= 0.0f;
		clearValue.Color[3]			= 1.0f;

		mDepthPass.RT.Initialize(&desc, &clearValue);

		gRenderer->ChangeResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mDepthPass.RT.Texture(), RHIResourceState::Common, RHIResourceState::PixelShaderResource));
	}

	// Create a variance params CB.
	{
		mVarianceCB.Data().Type() = ToInt(Type::VarianceSM);
		mVarianceCB.Initialize(__TEXT("VarianceCB"), RHIUtils::GetConstBufferSize(), 1, sizeof(mVarianceCB.Data()), &mVarianceCB.Data());
	}

	// Create constants buffer for a light source.
	{
		const UINT size = 1024 * 4;
		mObjects.LightConstBufferPerFrame.Initialize(__TEXT("ShadowMap:LightCBPerFrame"), size, 1, sizeof(mObjects.LightConstBufferPerFrame.Data()));

		LightSource& lightPoint = mObjects.LightPoint;
		lightPoint.Initialize();

		lightPoint.SetType(LightSourceType::Point);
		lightPoint.SetAmbient(XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
		lightPoint.SetDiffuse(XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
		lightPoint.SetSpecular(XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));

		ConstBufferForLight& cbLight	= lightPoint.ConstBuffer();
		cbLight.Data().Position			= lightPoint.GetCamera()->Position();
		cbLight.Data().ZRange			= MakeFloat4(lightPoint.GetCamera()->ZRange(), 0.0f, 0.0f);
		cbLight.Data().Matrix			= lightPoint.GetCamera()->ViewProjectionMatrix();
		cbLight.UpdateDataOnGPU();
	}

	// Create a floor.
	{
		mObjects.Floor = std::make_shared<StaticObject>();
		mObjects.Floor->Initialize(false, false);
		StaticGeometryBuilder::LoadPlane(mObjects.Floor->GetMeshComponent(), mObjects.Floor->GetSceneComponent());
		mObjects.Floor->GetSceneComponent()->SetPosition(XMFLOAT3(0.0f, 0.0f, -5.0f));
		mObjects.Floor->GetSceneComponent()->SetScale(XMFLOAT3(300.0f, 300.0f, 300.0f));
	}

	mObjects.LightPoint.GetCamera()->MarkDirty();

	// Create cubes.
	{
		const int numX			= 5;
		const int numY			= 5;
		const int numZ			= 5;
		const float scale		= 3.0f;
		const float distance	= 9.0f;
		const float tmp			= scale + distance;
		const XMFLOAT3 startPosition(-numX * tmp * 0.5f, -numY * tmp * 0.5f, 0.0f);
		for (int z = 0; z < numZ; ++z)
		{
			for (int y = 0; y < numY; ++y)
			{
				for (int x = 0; x < numX; ++x)
				{
					std::shared_ptr<StaticObject> cube = std::make_shared<StaticObject>();

					cube->Initialize(false, false);
					StaticGeometryBuilder::LoadCube(cube->GetMeshComponent(), cube->GetSceneComponent());

					cube->GetSceneComponent()->SetPosition(startPosition + XMFLOAT3(x * tmp, y * tmp, z * tmp));
					cube->GetSceneComponent()->SetScale(XMFLOAT3(scale, scale, scale));

					mObjects.Cubes.push_back(cube);
				}
			}
		}
	}

	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, false);
}

/**
 * Function : SetCameras
 */
void VarianceShadowMappingSample::SetCameras()
{
	Sample::SetCameras();

	{
		Camera::CameraDesc desc;
		desc.Common.Position			= XMFLOAT4(0.0f, 50.0f, 100.0f, 1.0f);
		desc.Common.ZRange				= XMFLOAT2(0.1f, 300.0f);

		desc.ProjType					= Camera::ProjectionType::Perspective;
		desc.PerspPart.FoV				= 45.0f;
		desc.PerspPart.AspectRatio		= mApp->GetWindow()->AspectRatio();

		desc.Format						= Camera::InitializeFormat::FromSphericalAngles;
		desc.AnglesPart.AngleXRange		= XMFLOAT2(-XM_PI, XM_PI);
		desc.AnglesPart.AngleYRange		= XMFLOAT2(-XM_PIDIV2, XM_PIDIV2);
		desc.AnglesPart.SphericalAngles	= XMFLOAT2(-XM_PIDIV2, -XM_PIDIV2 * 0.5f);

		mObjects.LightPoint.GetCamera() = std::make_shared<Camera>();
		mObjects.LightPoint.GetCamera()->Initialize(desc);

		mObjects.LightPoint.GetCamera()->SetName("Light point camera");
		mApp->AddCamera(mObjects.LightPoint.GetCamera());
	}
}

/**
 * Function : CreateRootSignatures
 */
void VarianceShadowMappingSample::CreateRootSignatures()
{
	// Depth pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(DepthPassRootParameters::Count));
		rootSignature[ToInt(DepthPassRootParameters::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(DepthPassRootParameters::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);

		mDepthPass.RootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}

	// Render pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(RenderPassRootParameters::Count), 2);
		rootSignature[ToInt(RenderPassRootParameters::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(RenderPassRootParameters::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
		rootSignature[ToInt(RenderPassRootParameters::Light)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::CBV, 1, 2);
		rootSignature[ToInt(RenderPassRootParameters::VarianceParams)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 3);
		rootSignature[ToInt(RenderPassRootParameters::DepthTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);

		// Set the first static sampler.
		{
			RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(0);
			samplerDesc.Filter					= RHIFilter::MinMagMipLinear;
			samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
			samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
			samplerDesc.ShaderRegister			= 0;
			samplerDesc.RegisterSpace			= 0;
			samplerDesc.ShaderVisibility		= RHIShaderVisibility::Pixel;
		}

		// Set the second static sampler.
		{
			RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(1);
			samplerDesc.Filter					= RHIFilter::ComparisonMinMagMipLinear;
			// We require a clamp based sampler when sampling the depth buffer so that it doesn't wrap around and sample incorrect information.
			samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
			samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
			samplerDesc.AddressW				= RHITextureAddressMode::Clamp;
			samplerDesc.MipLODBias				= 0;
			samplerDesc.MaxAnisotropy			= 0;
			samplerDesc.ComparisonFunc			= RHIComparisonFunc::Less;
			samplerDesc.BorderColor				= RHIStaticBorderColor::TransparentBlack;
			samplerDesc.MinLOD					= 0.0f;
			samplerDesc.MaxLOD					= RHIConstants::kFloat32Max;
			samplerDesc.ShaderRegister			= 1;
			samplerDesc.RegisterSpace			= 0;
			samplerDesc.ShaderVisibility		= RHIShaderVisibility::Pixel;
		}

		mRootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}
}

/**
 * Function : CreatePipelineStates
 */
void VarianceShadowMappingSample::CreatePipelineStates()
{
	// Depth pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("VarianceShadowMapping:DepthPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("VarianceShadowMapping:DepthPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

		psoDesc.RootSignature									= mDepthPass.RootSignature;
		psoDesc.InputLayout										= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS												= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS												= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType							= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask										= UINT_MAX;
		psoDesc.NumRenderTargets								= 1;
		psoDesc.RTVFormats[0]									= mDepthPass.Format;
		psoDesc.SampleDesc.Count								= 1;

		psoDesc.DepthStencilState.DepthEnable					= false;

		psoDesc.RasterizerState									= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise			= FALSE;
		psoDesc.RasterizerState.FillMode						= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode						= RHICullMode::None;

		psoDesc.BlendState										= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable			= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp				= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha			= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend				= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend			= RHIBlend::InvSrcAlpha;

		mDepthPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// Render pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("VarianceShadowMapping:RenderPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("VarianceShadowMapping:RenderPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

		psoDesc.RootSignature							= mRootSignature;
		psoDesc.InputLayout								= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.DSVFormat								= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= TRUE;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable			= FALSE;
		psoDesc.DepthStencilState.BackFace.StencilFunc	= RHIComparisonFunc::Always;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise	= FALSE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp		= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha	= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend		= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend	= RHIBlend::InvSrcAlpha;

		mPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : Update
 */
void VarianceShadowMappingSample::Update(const float deltaTime)
{
	UpdateCameraResource();
	mScreenQuad->UpdateDepthParams(MakeFloat4(mObjects.LightPoint.GetCamera()->ZRange(), 0.0f, 0.0f));

	if (mObjects.LightPoint.GetCamera()->IsDirty())
	{
		mObjects.LightConstBufferPerFrame.Data().DepthParams	= MakeFloat4(mObjects.LightPoint.GetCamera()->ZRange(), 0.0f, 0.0f);
		mObjects.LightConstBufferPerFrame.Data().ViewMatrix		= mObjects.LightPoint.GetCamera()->LookAt();
		mObjects.LightConstBufferPerFrame.Data().ViewProjMatrix	= mObjects.LightPoint.GetCamera()->ViewProjectionMatrix();
		mObjects.LightConstBufferPerFrame.UpdateDataOnGPU();
	}

	mObjects.Floor->Update(deltaTime);

	for (int i = 0; i < mObjects.Cubes.size(); ++i)
	{
		if (mObjects.Rotate)
		{
			const int rest = i % 5;
			if (rest == 0 || rest == 2 || rest == 4)
			{
				mObjects.Cubes[i]->GetSceneComponent()->AddRotateToPitch(deltaTime);
			}
			else if (rest == 1)
			{
				mObjects.Cubes[i]->GetSceneComponent()->AddRotateToRoll(deltaTime);
			}
			else
			{
				mObjects.Cubes[i]->GetSceneComponent()->AddRotateToYaw(deltaTime);
			}
		}

		mObjects.Cubes[i]->Update(deltaTime);
	}

	if (mVarianceCB.IsDirty())
		mVarianceCB.UpdateDataOnGPU();
}

/**
 * Function : RenderImGui
 */
void VarianceShadowMappingSample::RenderImGui()
{
	ImGui::Text("Variance shadow mapping.");

	ImGui::Checkbox("Rotate objects", &mObjects.Rotate);

	if (ImGui::RadioButton("SimpleSM", &mVarianceCB.Data().Type(), ToInt(Type::SimpleSM)))
		mVarianceCB.MarkDirty();

	if (ImGui::RadioButton("VarianceSM", &mVarianceCB.Data().Type(), ToInt(Type::VarianceSM)))
		mVarianceCB.MarkDirty();
}

/**
 * Function : Render
 */
void VarianceShadowMappingSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };

	RHIResourceBarrier barriers[] =
	{
		RHIResourceBarrier::MakeTransition(mDepthPass.RT.Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),

		RHIResourceBarrier::MakeTransition(mDepthPass.RT.Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),

		RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget),
		RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present)
	};

	// Depth pass.
	{
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &barriers[0]);
		commandList->OMSetRenderTargets(1, &mDepthPass.RT.RtvHandle().Cpu(), false, nullptr);
		commandList->ClearRenderTargetView(mDepthPass.RT.RtvHandle().Cpu(), mDepthPass.ClearValue.Color, 0, nullptr);

		commandList->SetPipelineState(mDepthPass.PSO);
		commandList->SetGraphicsRootSignature(mDepthPass.RootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);

		commandList->SetGraphicsRootDescriptorTable(ToInt(DepthPassRootParameters::PerFrame), mObjects.LightConstBufferPerFrame.SrvHandle().Gpu());
		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		mObjects.Floor->Draw(commandList, ToInt(DepthPassRootParameters::PerObject));

		for (auto& cube : mObjects.Cubes)
			cube->Draw(commandList, ToInt(DepthPassRootParameters::PerObject));

		commandList->ResourceBarrier(1, &barriers[1]);
		gRenderer->CommitCommandList(commandList);
	}

	// Render pass.
	{
		gRenderer->StartRendering();
		gRenderer->ClearRTV(&mBGColor.x);
		gRenderer->ClearDSV();

		commandList->SetPipelineState(mPSO);
		commandList->SetGraphicsRootSignature(mRootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::Light), mObjects.LightPoint.ConstBuffer().SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::VarianceParams), mVarianceCB.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(RenderPassRootParameters::DepthTexture), mDepthPass.RT.SrvHandle().Gpu());
		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		mObjects.Floor->Draw(commandList, ToInt(RenderPassRootParameters::PerObject));

		for (auto& cube : mObjects.Cubes)
			cube->Draw(commandList, ToInt(RenderPassRootParameters::PerObject));

		gRenderer->StopRendering();
	}

	// Draw a screen quad.
	if (mUseScreenQuad)
	{
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &barriers[2]);
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);

		DrawScreenQuad(commandList, heaps, _countof(heaps));

		commandList->ResourceBarrier(1, &barriers[3]);
		gRenderer->CommitCommandList(commandList);
	}
}
