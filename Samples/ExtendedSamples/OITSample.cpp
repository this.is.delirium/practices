#include <StdafxSamples.h>
#include "OITSample.h"
#include <Application.h>
#include <ECS/MeshComponent.h>
#include <ECS/SceneComponent.h>
#include <FBXLoader.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHIUtils.h>
#include <RootSignature.h>
#include <StaticGeometryBuilder.h>

#include <D3D12/private/DXMathHelper.h>
using namespace DirectX;
using namespace GraphicsCore;

namespace
{
	static const char* methods[] =
	{
		"Weighted blended OIT",
		"OIT with linked litst"
	};

	enum class TestPassRootSignature
	{
		PerFrame = 0,
		PerObject,
		Count
	};

	enum class WBFirstPassRootSignature
	{
		PerFrame = 0,
		PerObject,
		Count
	};

	enum class WBSecondPassRootSignature
	{
		Accumulate = 0,
		Revealage,
		OpaquePass,
		Count
	};

	enum class LLClearPassRootSignature
	{
		ClearValue = 0,
		Count
	};

	enum class LLFirstPassRootSignature
	{
		PerFrame = 0,
		PerObject,
		StartOffsets,
		FragmentAndLinkBuffer,
		Count
	};

	enum class LLSecondPassRootSignature
	{
		OpaquePassResult = 0,
		StartOffsets,
		FragmentAndLinkBuffer,
		Count
	};
}

/**
 * Function : Release
 */
void OITSample::WeightedBlendedSecondPass::Release()
{
	RHISafeRelease(RootSignature);
	RHISafeRelease(PSO);
	OpaquePassTexture.Release();
	VertexBuffer.Release();
}

/**
 * Function : Release
 */
void OITSample::LinkedListsClearPass::Release()
{
	RHISafeRelease(RootSignature);
	RHISafeRelease(PSO);
	ClearValueCB.Release();
}

/**
 * Function : Release
 */
void OITSample::LinkedListsFirstPass::Release(IRenderer* renderer)
{
	RHISafeRelease(RootSignature);
	RHISafeRelease(PSO);
	HeadsOfLists.Release();
	LinkedLists.Release();
}

/**
 * Function : Release
 */
void OITSample::LinkedListsSecondPass::Release()
{
	RHISafeRelease(RootSignature);
	RHISafeRelease(PSO);
}

/**
 * Function : Release
 */
void OITSample::Release()
{
	Sample::Release();

	mWBFirstPass.Release();
	mWBSecondPass.Release();

	mQuadVertexBuffer.Release();
	mOpaquePassResult.Release();

	mLinkedListsClearPass.Release();
	mLinkedListsFirstPass.Release(gRenderer);
	mLinkedListsSecondPass.Release();

	for (auto& object : mOpaqueObjects)
		object.Release();

	for (auto& object : mTransparentObjects)
		object.Release();
}

/**
 * Function : Initialize
 */
void OITSample::Initialize()
{
	SetMethod(Method::LinkedLists);
	mIsRandomize = false;
	Sample::Initialize();

	mScreenQuad->SetTexture(mWBFirstPass.RenderTarget(0).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);
}

/**
 * Function : InitializeRenderingResources
 */
void OITSample::InitializeRenderingResources()
{
	CreateWBResources();
	CreateLLResources();

	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, true);

	//CreateScene();
	CreateTestScene();
}

/**
 * Function : CreateScene
 */
void OITSample::CreateScene()
{
	FbxLoader fbxLoader;
	fbxLoader.LoadFile(SResourceManager::Instance().Find(__TEXT("Samplels:teapot.fbx")));

	mOpaqueObjects.emplace_back();
	StaticObject& floor = mOpaqueObjects.back();
	floor.Initialize(false, false);
	StaticGeometryBuilder::LoadPlane(floor.GetMeshComponent(), floor.GetSceneComponent());
	floor.GetSceneComponent()->SetScale(XMFLOAT3(300.0f, 300.0f, 300.0f));

	const int maxX			= 7;
	const int maxY			= 7;
	const int idOpaque		= 4;
	const float distance	= 10.0f;
	const float size		= 8.0f;
	const float tmp			= distance + size;
	const XMFLOAT3 startPosition(-tmp * maxX * 0.5f, -tmp * maxY * 0.5f, 0.0f);
	for (int y = 0; y < maxY; ++y)
	{
		for (int x = 0; x < maxX; ++x)
		{
			const bool isOpaque = ((y * maxX + x) % idOpaque) == 0;
			StaticObject* object = nullptr;

			if (isOpaque)
			{
				mOpaqueObjects.emplace_back();
				object = &mOpaqueObjects.back();
			}
			else
			{
				mTransparentObjects.emplace_back();
				object = &mTransparentObjects.back();
			}

			object->Initialize();
			object->GetSceneComponent()->SetScale(XMFLOAT3(0.1f, 0.1f, 0.1f));
			object->GetSceneComponent()->SetPosition(startPosition + XMFLOAT3(tmp * x, tmp * y, 0.0f));
			object->GetSceneComponent()->AddRotateToPitch(XM_PIDIV2);

			fbxLoader.FillMeshComponent(0, object->GetMeshComponent());
			if (!isOpaque)
				object->SetColor(XMFLOAT4(0.2f, 0.3f, 0.6f, 0.4f));
		}
	}

	mCamera->Angles().x = 2.27f;
	mCamera->Angles().y = -0.315f;
	mCamera->SetPosition(XMFLOAT4(31.0f, -27, 10.0f, 1.0f));
}

/**
 * Function : CreateTestScene
 */
void OITSample::CreateTestScene()
{
	mTransparentObjects.resize(3);
	float step			= 2.0f;
	float startOffset	= 0.0f;

	const XMFLOAT4 colors[3] =
	{
		XMFLOAT4(1.0f, 0.0f, 0.0f, 0.75f),
		XMFLOAT4(1.0f, 1.0f, 0.0f, 0.75f),
		XMFLOAT4(0.0f, 0.0f, 1.0f, 0.75f)
	};

	for (int i = 0; i < mTransparentObjects.size(); ++i)
	{
		StaticObject& object = mTransparentObjects[i];

		object.Initialize(false, false);
		StaticGeometryBuilder::LoadPlane(object.GetMeshComponent(), object.GetSceneComponent());
		object.GetSceneComponent()->SetScale(XMFLOAT3(3.0f, 3.0f, 3.0f));
		object.GetSceneComponent()->AddRotateToYaw(XM_PIDIV2);
		object.GetSceneComponent()->SetPosition(XMFLOAT3(startOffset, startOffset * 0.7f, 10.0f + startOffset * 0.5f));
		object.SetColor(colors[i]);
		startOffset += step;
	}

	mCamera->Angles().y = 0.0f;
	mCamera->SetZRange(XMFLOAT2(0.1f, 30.0f));
	mBGColor = DirectX::XMFLOAT4(0.75f, 0.75f, 0.75f, 1.0f);
}

/**
 * Function : CreateWBResources
 */
void OITSample::CreateWBResources()
{
	CreateRenderTargets();
	CreateFullScreenQuad();
}

/**
 * Function : CreateRenderTargets
 */
void OITSample::CreateRenderTargets()
{
	// The first render target must have at least RGBA16F precision and the second must have at least R8 precision.
	RHIFormat formats[]				= { RHIFormat::R16G16B16A16Float, RHIFormat::R16G16B16A16Float };
	MRTPassDesc mrtDesc				= {};
	mrtDesc.NumRT					= _countof(formats);
	mrtDesc.DepthStencil.ViewFormat	= gRenderer->DepthStencilViewFormat(); // ToDo: Describe DepthStencil.
	mrtDesc.InputLayout				= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };

	for (UINT i = 0; i < mrtDesc.NumRT; ++i)
	{
		RHIResourceDesc& resDesc	= mrtDesc.Descs[i];
		resDesc.Dimension			= RHIResourceDimension::Texture2D;
		resDesc.Width				= mApp->GetWindow()->Width();
		resDesc.Height				= mApp->GetWindow()->Height();
		resDesc.DepthOrArraySize	= 1;
		resDesc.MipLevels			= 1;
		resDesc.SampleDesc.Count	= 1;
		resDesc.Layout				= RHITextureLayout::Unknown;
		resDesc.Flags				= RHIResourceFlag::AllowRenderTarget;
		resDesc.Format				= formats[i];

		{
			RHIClearValue& clearValue = mrtDesc.ClearValues[i];
			clearValue.Format = formats[i];

			// Clear the first render target to vec4(0) and the second render target to 1.
			float value = (i == 0) ? 0.0f : 1.0f;
			clearValue.Color[0] = value;
			clearValue.Color[1] = 0.0f;
			clearValue.Color[2] = 0.0f;
			clearValue.Color[3] = 0.0f;
		}
	}

	RHIBlendDesc& blendState = mrtDesc.BlendDesc;
	blendState = RHIBlendDesc(RHIDefault());

	blendState.RenderTarget[0].BlendEnable = true;
	blendState.RenderTarget[1].BlendEnable = true;

	if (true)
	{
		blendState.SetBlendFunc(0, RHIBlend::One, RHIBlend::One);
		blendState.SetBlendFunc(1, RHIBlend::Zero, RHIBlend::InvSrcAlpha);
	}
	else
	{
		blendState.SetBlendFuncSeparate(0, RHIBlend::One, RHIBlend::One, RHIBlend::Zero, RHIBlend::InvSrcAlpha);
		blendState.SetBlendFuncSeparate(1, RHIBlend::One, RHIBlend::One, RHIBlend::Zero, RHIBlend::InvSrcAlpha);
	}

	mWBFirstPass.Initialize(mrtDesc);

	//
	{
		RHIResourceDesc desc	= mrtDesc.Descs[0];
		desc.Flags				= RHIResourceFlag::None;
		desc.Format				= gRenderer->RenderTargetFormat();

		mWBSecondPass.OpaquePassTexture.Initialize(&desc);

		gRenderer->ChangeResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mWBSecondPass.OpaquePassTexture.Texture(), RHIResourceState::Common, RHIResourceState::PixelShaderResource));
	}
}

/**
 * Function : CreateFullScreenQuad
 */
void OITSample::CreateFullScreenQuad()
{
	StaticGeometryBuilder::BuildFullScreenQuad(&mWBSecondPass.VertexBuffer, __TEXT("OITSample:WBSecondPass:VertexBuffer"));
}

/**
 * Function : CreateLLResources
 */
void OITSample::CreateLLResources()
{
	// Create a render-target for opaque pass.
	{
		RHISwapChainDesc scDesc;
		gRenderer->SwapChain()->GetDesc(&scDesc);

		RHIResourceDesc desc	= {};
		desc.Dimension			= RHIResourceDimension::Texture2D;
		desc.Format				= gRenderer->RenderTargetFormat();
		desc.Width				= scDesc.BufferDesc.Width;
		desc.Height				= scDesc.BufferDesc.Height;
		desc.DepthOrArraySize	= 1;
		desc.MipLevels			= 1;
		desc.SampleDesc.Count	= 1;
		desc.Layout				= RHITextureLayout::Unknown;
		desc.Flags				= RHIResourceFlag::AllowRenderTarget;

		mOpaquePassResult.Initialize(&desc);
	}

	// Create a full-screen quad.
	{
		using QuadVertex = InputLayouts::ScreenQuadWithoutMatrix::Vertex;

		const QuadVertex vertices[] =
		{
			{ DirectX::XMFLOAT2(-1.0f, 1.0f) },
			{ DirectX::XMFLOAT2(-1.0f, -1.0f) },
			{ DirectX::XMFLOAT2(1.0f, -1.0f) },

			{ DirectX::XMFLOAT2(-1.0f, 1.0f) },
			{ DirectX::XMFLOAT2(1.0f, -1.0f) },
			{ DirectX::XMFLOAT2(1.0f, 1.0f) }
		};

		mQuadVertexBuffer.Initialize(__TEXT("OITwLL:QuadVB"), sizeof(vertices), _countof(vertices), sizeof(QuadVertex), vertices);
	}

	// Create a clear-value constant buffer.
	{
		XMUINT4 clearValue(0xFFFFFFFF, 0, 0, 0);
		RHIConstBuffer<XMUINT4>& cb = mLinkedListsClearPass.ClearValueCB;
		cb.Initialize(__TEXT("OITwLL:ClearValueCB"), RHIUtils::GetConstBufferSize(), 1, sizeof(cb.Data()), &clearValue);
	}

	uint32_t width	= mApp->GetWindow()->Width();
	uint32_t height	= mApp->GetWindow()->Height();
	// Create a texture for head elements of lists.
	{
		RHIResourceDesc desc	= {};
		desc.Dimension			= RHIResourceDimension::Texture2D;
		desc.Format				= RHIFormat::R32Uint;
		desc.Width				= width;
		desc.Height				= height;
		desc.DepthOrArraySize	= 1;
		desc.MipLevels			= 1;
		desc.SampleDesc.Count	= 1;
		desc.Flags				= RHIResourceFlag::AllowUnorderedAccess | RHIResourceFlag::AllowRenderTarget;

		mLinkedListsFirstPass.HeadsOfLists.Initialize(&desc);
	}

	// Create the buffer of linked lists.
	{
		const UINT64 widthBuffer = width * height * kMaxElementsPerPixel * sizeof(ListNode);
		const UINT numElements = width * height;
		mLinkedListsFirstPass.LinkedLists.Initialize(__TEXT("OIT:LinkedLists"), widthBuffer, numElements, sizeof(ListNode), nullptr, RHIHeapType::Default);
	}

	RHIResourceBarrier barriers[] = 
	{
		RHIResourceBarrier::MakeTransition(mLinkedListsFirstPass.HeadsOfLists.Texture(), RHIResourceState::Common, RHIResourceState::PixelShaderResource),
		RHIResourceBarrier::MakeTransition(mLinkedListsFirstPass.LinkedLists.Buffer(), RHIResourceState::Common, RHIResourceState::PixelShaderResource),
		RHIResourceBarrier::MakeTransition(mOpaquePassResult.Texture(), RHIResourceState::Common, RHIResourceState::PixelShaderResource)
	};

	gRenderer->ChangeResourceBarrier(_countof(barriers), barriers);
}

/**
 * Function : CreateRootSignatures
 */
void OITSample::CreateRootSignatures()
{
	// Test root signature.
	{
		RHIUtils::RootSignature rootSignature(ToInt(TestPassRootSignature::Count));
		rootSignature[ToInt(TestPassRootSignature::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(TestPassRootSignature::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);

		mRootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}

	// Weighted blended first pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(WBFirstPassRootSignature::Count));
		rootSignature[ToInt(WBFirstPassRootSignature::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(WBFirstPassRootSignature::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);

		mWBFirstPass.SetRootSignature(rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false)));
	}

	// Weighted blended second pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(WBSecondPassRootSignature::Count), 1);
		rootSignature[ToInt(WBSecondPassRootSignature::Accumulate)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);
		rootSignature[ToInt(WBSecondPassRootSignature::Revealage)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 1);
		rootSignature[ToInt(WBSecondPassRootSignature::OpaquePass)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 2);

		RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(0);
		samplerDesc.Filter					= RHIFilter::MinMagMipLinear;
		samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc.MipLODBias				= 0;
		samplerDesc.MaxAnisotropy			= 0;
		samplerDesc.ComparisonFunc			= RHIComparisonFunc::Never;
		samplerDesc.BorderColor				= RHIStaticBorderColor::TransparentBlack;
		samplerDesc.MinLOD					= 0.0f;
		samplerDesc.MaxLOD					= RHIConstants::kFloat32Max;
		samplerDesc.ShaderRegister			= 0;
		samplerDesc.RegisterSpace			= 0;
		samplerDesc.ShaderVisibility		= RHIShaderVisibility::Pixel;

		mWBSecondPass.RootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}

	// OITwLL clear pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(LLClearPassRootSignature::Count));
		rootSignature[ToInt(LLClearPassRootSignature::ClearValue)].InitAsConstantBufferView(0, 0, RHIShaderVisibility::Pixel);

		mLinkedListsClearPass.RootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}

	// OITwLL first pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(LLFirstPassRootSignature::Count));
		rootSignature[ToInt(LLFirstPassRootSignature::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
		rootSignature[ToInt(LLFirstPassRootSignature::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
		rootSignature[ToInt(LLFirstPassRootSignature::StartOffsets)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::UAV, 1, 0);
		rootSignature[ToInt(LLFirstPassRootSignature::FragmentAndLinkBuffer)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::UAV, 1, 1);

		mLinkedListsFirstPass.RootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}

	// OITwLL second pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(LLSecondPassRootSignature::Count), 1);
		rootSignature[ToInt(LLSecondPassRootSignature::StartOffsets)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);
		rootSignature[ToInt(LLSecondPassRootSignature::FragmentAndLinkBuffer)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 1);
		rootSignature[ToInt(LLSecondPassRootSignature::OpaquePassResult)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 2);

		RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(0);
		samplerDesc.Filter					= RHIFilter::MinMagMipLinear;
		samplerDesc.AddressU				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressV				= RHITextureAddressMode::Clamp;
		samplerDesc.AddressW				= RHITextureAddressMode::Clamp;
		samplerDesc.MipLODBias				= 0;
		samplerDesc.MaxAnisotropy			= 0;
		samplerDesc.ComparisonFunc			= RHIComparisonFunc::Never;
		samplerDesc.BorderColor				= RHIStaticBorderColor::TransparentBlack;
		samplerDesc.MinLOD					= 0.0f;
		samplerDesc.MaxLOD					= RHIConstants::kFloat32Max;
		samplerDesc.ShaderRegister			= 0;
		samplerDesc.RegisterSpace			= 0;
		samplerDesc.ShaderVisibility		= RHIShaderVisibility::Pixel;

		mLinkedListsSecondPass.RootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
	}
}

/**
 * Function : CreatePipelineStates
 */
void OITSample::CreatePipelineStates()
{
	// Test pipeline state object.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("TestShader.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("TestShader.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

		psoDesc.RootSignature							= mRootSignature;
		psoDesc.InputLayout								= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.DSVFormat								= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= TRUE;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable			= FALSE;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise	= FALSE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp		= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha	= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend		= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend	= RHIBlend::InvSrcAlpha;

		mPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// PSO of the first WB pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("OIT:WBFirstPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("OIT:WBFirstPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= TRUE;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::Off;
		psoDesc.DepthStencilState.StencilEnable			= FALSE;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise	= FALSE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;

		RHIUtils::Fill::GraphicsPipelineStateDesc(&mWBFirstPass, &psoDesc);

		mWBFirstPass.SetPSO(gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc));
	}

	// PSO of the second WB pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("OIT:WBSecondPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("OIT:WBSecondPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

		psoDesc.RootSignature							= mWBSecondPass.RootSignature;
		psoDesc.InputLayout								= { InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc, _countof(InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= FALSE;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise	= TRUE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode				= RHICullMode::Back;

		psoDesc.BlendState									= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable		= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp			= RHIBlendOp::Add;
		psoDesc.BlendState.SetBlendFunc(0, RHIBlend::One, RHIBlend::Zero);

		mWBSecondPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// PSO of the clear OITwLL pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("OIT:LLClearPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("OIT:LLClearPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

		psoDesc.RootSignature							= mLinkedListsClearPass.RootSignature;
		psoDesc.InputLayout								= { InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc, _countof(InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= mLinkedListsFirstPass.HeadsOfLists.Texture()->GetDesc().Format;
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= false;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise	= FALSE;
		psoDesc.RasterizerState.CullMode				= RHICullMode::Front;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());

		mLinkedListsClearPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// PSO of the first OIT with LL pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("OIT:LLFirstPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("OIT:LLFirstPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

		psoDesc.RootSignature							= mLinkedListsFirstPass.RootSignature;
		psoDesc.InputLayout								= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 0;
		psoDesc.DSVFormat								= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= true;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::Off;
		psoDesc.DepthStencilState.StencilEnable			= false;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise	= TRUE;
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());

		mLinkedListsFirstPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	// PSO of the second OIT with LL pass.
	{
		RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("OIT:LLSecondPass.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("OIT:LLSecondPass.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc;
		ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

		psoDesc.RootSignature							= mLinkedListsSecondPass.RootSignature;
		psoDesc.InputLayout								= { InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc, _countof(InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Triangle;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= false;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.FrontCounterClockwise	= FALSE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode				= RHICullMode::Front;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.SetBlendFunc(0, RHIBlend::SrcAlpha, RHIBlend::InvSrcAlpha);

		mLinkedListsSecondPass.PSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : RenderImGui
 */
void OITSample::RenderImGui()
{
	ImGui::Text("It represents %d methods from OIT class:", ToInt(Method::Count));
	if (ImGui::ListBox("Methods", &mMethod, methods, ToInt(Method::Count)))
		SetMethod((Method)mMethod);

	if (mMethod == ToInt(Method::WeightedBlended))
	{
		ImGui::Checkbox("Show RT", &mWBFirstPass.ShowTexture());
		if (mWBFirstPass.ShowTexture())
		{
			if (ImGui::RadioButton("Accumulated color", &mWBFirstPass.ShowTextureId(), 0))
				mScreenQuad->SetTexture(mWBFirstPass.RenderTarget(0).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);

			if (ImGui::RadioButton("Accumulated alpha", &mWBFirstPass.ShowTextureId(), 1))
				mScreenQuad->SetTexture(mWBFirstPass.RenderTarget(1).SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);
		}
	}
}

/**
 * Function : SetMethod
 */
void OITSample::SetMethod(const Method method)
{
	mMethod = ToInt(method);

	using namespace std::placeholders;
	switch (method)
	{
		case Method::WeightedBlended:
		{
			mRenderMethod = std::bind(&OITSample::WeightedBlendedRender, this, _1);
			break;
		}

		case Method::LinkedLists:
		{
			mRenderMethod = std::bind(&OITSample::LinkedListsRender, this, _1);
			break;
		}

		default:
			assert(false && "It isn't implemented yet.");
	}
}

/**
 * Function : Update
 */
void OITSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	for (auto& object : mOpaqueObjects)
		object.Update(deltaTime);

	for (auto& object : mTransparentObjects)
		object.Update(deltaTime);

	if (mMethod == ToInt(Method::LinkedLists))
	{
		UINT clearValue = 0;
		gRenderer->UpdateBuffer(&mLinkedListsFirstPass.LinkedLists.Counter(), &RHIUtils::GetSubresource(&clearValue, sizeof(clearValue)));
	}
}

/**
 * Function : Render
 */
void OITSample::Render(RHICommandList* commandList)
{
	mRenderMethod(commandList);
}

/**
 * Function : WeightedBlendedRender
 */
void OITSample::WeightedBlendedRender(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };

	// Render opaque objects.
	{
		gRenderer->StartRendering();
		gRenderer->ClearRTV(&mBGColor.x);
		gRenderer->ClearDSV();

		commandList->SetPipelineState(mPSO);
		commandList->SetGraphicsRootSignature(mRootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);

		commandList->SetGraphicsRootDescriptorTable(ToInt(TestPassRootSignature::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		for (auto& object : mOpaqueObjects)
		{
			object.Draw(commandList, ToInt(TestPassRootSignature::PerObject));
		}

		gRenderer->StopRendering();
	}

	// Copy the result of opaque pass to in a texture.
	{
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);

		RHIResource* src = gRenderer->CurrentRenderTarget();
		RHIResource* dst = mWBSecondPass.OpaquePassTexture.Texture();
		RHIResourceBarrier startBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(src, RHIResourceState::Present, RHIResourceState::CopySource),
			RHIResourceBarrier::MakeTransition(dst, RHIResourceState::PixelShaderResource, RHIResourceState::CopyDest)
		};

		RHIResourceBarrier stopBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(dst, RHIResourceState::CopyDest, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(src, RHIResourceState::CopySource, RHIResourceState::Present)
		};

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(src, RHIResourceState::Present, RHIResourceState::CopySource));
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(dst, RHIResourceState::GenericRead, RHIResourceState::CopyDest));
		commandList->CopyResource(dst, src);
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(dst, RHIResourceState::CopyDest, RHIResourceState::GenericRead));
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(src, RHIResourceState::CopySource, RHIResourceState::Present));

		gRenderer->CommitCommandList(commandList);
	}

	// The first pass.
	{
		RHIResourceBarrier startBarriers[] = 
		{
			RHIResourceBarrier::MakeTransition(mWBFirstPass.RenderTarget(ToInt(WBSecondPassRootSignature::Accumulate)).Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),
			RHIResourceBarrier::MakeTransition(mWBFirstPass.RenderTarget(ToInt(WBSecondPassRootSignature::Revealage)).Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthRead)
		};

		RHIResourceBarrier stopBarriers[] = 
		{
			RHIResourceBarrier::MakeTransition(mWBFirstPass.RenderTarget(ToInt(WBSecondPassRootSignature::Accumulate)).Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(mWBFirstPass.RenderTarget(ToInt(WBSecondPassRootSignature::Revealage)).Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthRead, RHIResourceState::Present)
		};

		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), mWBFirstPass.PSO());
		commandList->ResourceBarrier(_countof(startBarriers), startBarriers);
		commandList->OMSetRenderTargets(mWBFirstPass.NumRT(), mWBFirstPass.RtvCpuHandles(), false, &gRenderer->CurrentDepthStencilCpuHandle());

		for (int i = 0; i < mWBFirstPass.NumRT(); ++i)
			commandList->ClearRenderTargetView(mWBFirstPass.RenderTarget(i).RtvHandle().Cpu(), mWBFirstPass.ClearValue(i).Color, 0, nullptr);

		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		commandList->SetGraphicsRootSignature(mWBFirstPass.RootSignature());
		commandList->SetGraphicsRootDescriptorTable(ToInt(WBFirstPassRootSignature::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());

		//for (auto& object : mTransparentObjects)
		for (int i = 0; i < mTransparentObjects.size(); ++i)
		//for (int i = mTransparentObjects.size() - 1; i >= 0 ; --i)
		{
			StaticObject& object = mTransparentObjects[i];
			object.Draw(commandList, ToInt(WBFirstPassRootSignature::PerObject));
		}

		commandList->ResourceBarrier(_countof(stopBarriers), stopBarriers);
		gRenderer->CommitCommandList(commandList);
	}

	// The second pass.
	{
		//gRenderer->StartRendering();
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);

		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetPipelineState(mWBSecondPass.PSO);
		commandList->SetGraphicsRootSignature(mWBSecondPass.RootSignature);
		commandList->SetGraphicsRootDescriptorTable(ToInt(WBSecondPassRootSignature::Accumulate), mWBFirstPass.RenderTarget(ToInt(WBSecondPassRootSignature::Accumulate)).SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(WBSecondPassRootSignature::Revealage), mWBFirstPass.RenderTarget(ToInt(WBSecondPassRootSignature::Revealage)).SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(WBSecondPassRootSignature::OpaquePass), mWBSecondPass.OpaquePassTexture.SrvHandle().Gpu());
		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);
		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
		commandList->IASetVertexBuffers(0, 1, mWBSecondPass.VertexBuffer.View());
		commandList->DrawInstanced(mWBSecondPass.VertexBuffer.NumElements(), 1, 0, 0);

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
		gRenderer->CommitCommandList(commandList);
		//gRenderer->StopRendering();
	}

	if (mWBFirstPass.ShowTexture())
	{
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);

		DrawScreenQuad(commandList, heaps, _countof(heaps));

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
		gRenderer->CommitCommandList(commandList);
	}
}

/**
 * Function : LinkedListsRender
 */
void OITSample::LinkedListsRender(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };

	{
		//RHIResourceBarrier barriers[] =
		//{
		//	RHIResourceBarrier::MakeTransition(mOpaquePassResult.Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),
		//	RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthWrite),
		//
		//	RHIResourceBarrier::MakeTransition(mOpaquePassResult.Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource),
		//	RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present)
		//};
		//
		//gRenderer->CommandAllocator()->Reset();
		//commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		//commandList->ResourceBarrier(2, &barriers[0]);
		//
		//commandList->OMSetRenderTargets(1, &mOpaquePassResult.RtvHandle().Cpu(), false, gRenderer->CurrentDepthStencilCpuHandle());
		//
		//commandList->ClearRenderTargetView(mOpaquePassResult.RtvHandle().Cpu(), &mBGColor.x, 0, nullptr);
		//commandList->ClearDepthStencilView(*gRenderer->CurrentDepthStencilCpuHandle(), RHIClearFlags::Depth, 1.0f, 0, 0, nullptr);
		gRenderer->StartRendering();
		gRenderer->ClearRTV(&mBGColor.x);
		gRenderer->ClearDSV();

		commandList->SetPipelineState(mPSO);
		commandList->SetGraphicsRootSignature(mRootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);

		commandList->SetGraphicsRootDescriptorTable(ToInt(TestPassRootSignature::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		for (auto& object : mOpaqueObjects)
		{
			object.Draw(commandList, ToInt(TestPassRootSignature::PerObject));
		}

		gRenderer->StopRendering();
	}

	// The clear pass.
	{
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mLinkedListsFirstPass.HeadsOfLists.Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget));

		const RHICPUDescriptorHandle& rtvHandle = mLinkedListsFirstPass.HeadsOfLists.RtvHandle().Cpu();
		commandList->OMSetRenderTargets(1, &rtvHandle, false, nullptr);

		commandList->SetPipelineState(mLinkedListsClearPass.PSO);
		commandList->SetGraphicsRootSignature(mLinkedListsClearPass.RootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		commandList->SetGraphicsRootConstantBufferView(ToInt(LLClearPassRootSignature::ClearValue), mLinkedListsClearPass.ClearValueCB.GpuVirtualAddress());

		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
		commandList->IASetVertexBuffers(0, 1, mQuadVertexBuffer.View());
		commandList->DrawInstanced(mQuadVertexBuffer.NumElements(), 1, 0, 0);

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mLinkedListsFirstPass.HeadsOfLists.Texture(), RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource));
		gRenderer->CommitCommandList(commandList);
	}

	// The first pass.
	{
		RHIResourceBarrier barriers[] =
		{
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthRead),
			RHIResourceBarrier::MakeTransition(mLinkedListsFirstPass.HeadsOfLists.Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::UnorderedAccess),
			RHIResourceBarrier::MakeTransition(mLinkedListsFirstPass.LinkedLists.Buffer(), RHIResourceState::PixelShaderResource, RHIResourceState::UnorderedAccess),

			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthRead, RHIResourceState::Present),
			RHIResourceBarrier::MakeTransition(mLinkedListsFirstPass.HeadsOfLists.Texture(), RHIResourceState::UnorderedAccess, RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(mLinkedListsFirstPass.LinkedLists.Buffer(), RHIResourceState::UnorderedAccess, RHIResourceState::PixelShaderResource)
		};

		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(3, &barriers[0]);

		commandList->OMSetRenderTargets(0, nullptr, false, &gRenderer->CurrentDepthStencilCpuHandle());

		commandList->SetPipelineState(mLinkedListsFirstPass.PSO);
		commandList->SetGraphicsRootSignature(mLinkedListsFirstPass.RootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		commandList->SetGraphicsRootDescriptorTable(ToInt(LLFirstPassRootSignature::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(LLFirstPassRootSignature::StartOffsets), mLinkedListsFirstPass.HeadsOfLists.UavHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(LLFirstPassRootSignature::FragmentAndLinkBuffer), mLinkedListsFirstPass.LinkedLists.UavHandle().Gpu());
		for (auto& object : mTransparentObjects)
			object.Draw(commandList, ToInt(LLFirstPassRootSignature::PerObject));

		commandList->ResourceBarrier(3, &barriers[3]);
		gRenderer->CommitCommandList(commandList);
	}

	// The second pass.
	{
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));

		//XMFLOAT4 clearColor(0.75f, 0.75f, 0.75f, 1.0f);
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);
		//commandList->ClearRenderTargetView(*gRenderer->CurrentRenderTargetCpuHandle(), &clearColor.x, 0, nullptr);

		commandList->SetPipelineState(mLinkedListsSecondPass.PSO);
		commandList->SetGraphicsRootSignature(mLinkedListsSecondPass.RootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		commandList->SetGraphicsRootDescriptorTable(ToInt(LLSecondPassRootSignature::StartOffsets), mLinkedListsFirstPass.HeadsOfLists.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(LLSecondPassRootSignature::FragmentAndLinkBuffer), mLinkedListsFirstPass.LinkedLists.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(ToInt(LLSecondPassRootSignature::OpaquePassResult), mOpaquePassResult.SrvHandle().Gpu());

		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
		commandList->IASetVertexBuffers(0, 1, mQuadVertexBuffer.View());
		commandList->DrawInstanced(mQuadVertexBuffer.NumElements(), 1, 0, 0);

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
		gRenderer->CommitCommandList(commandList);
	}
}
