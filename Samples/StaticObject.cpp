#include "StdafxSamples.h"
#include "StaticObject.h"

#include <ECS/MeshComponent.h>
#include <ECS/SceneComponent.h>

#include "InputLayouts.h"
#include <Render/IRenderer.h>

#include <RHIUtils.h>

using Vertex = InputLayouts::Main::Vertex;

/**
 * Function : Release
 */
void StaticObject::Release()
{
	mConstBuffer.Release();
}

/**
 * Function : Initialize
 */
void StaticObject::Initialize(const bool createSceneComponent, const bool createMeshComponent)
{
	mConstBuffer.Data().Color.w = 1.0f;
	mConstBuffer.Initialize(__TEXT("StaticObject:ConstBuffer"), RHIUtils::GetConstBufferSize(), 1, sizeof(mConstBuffer.Data()), nullptr, RHIHeapType::Upload);

	if (createSceneComponent)
		GetSceneComponent().reset(BaseComponent::CreateSubobject<SceneComponent>());

	if (createMeshComponent)
		GetMeshComponent().reset(BaseComponent::CreateSubobject<MeshComponent>());
}

/**
 * Function : Update
 */
void StaticObject::Update(const float deltaTime)
{
	if (GetSceneComponent()->IsDirty())
	{
		mConstBuffer.Data().WorldMatrix		= GetSceneComponent()->WorldMatrix();
		mConstBuffer.Data().NormalMatrix	= GetSceneComponent()->LocalRotateMatrix();
		mConstBuffer.UpdateDataOnGPU();
	}
}

/**
 * Functuin : Draw
 */
void StaticObject::Draw(RHICommandList* commandList, const UINT graphicsRootDescriptorTableId)
{
	if (graphicsRootDescriptorTableId != (UINT)-1)
		commandList->SetGraphicsRootDescriptorTable(graphicsRootDescriptorTableId, mConstBuffer.SrvHandle().Gpu());
	GetMeshComponent()->Draw(commandList);
}
