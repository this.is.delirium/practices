include(../Cmake/Macroes.cmake)

set(SAMPLES_SOURCES
	Application.cpp
	Application.h
	BuildTasks.cpp
	BuildTasks.h
	Main.cpp
	Sample.cpp
	Sample.h
	PSOEditor.cpp
	PSOEditor.h
	ScreenQuadPart.cpp
	ScreenQuadPart.h
	StaticObject.cpp
	StaticObject.h
	StdafxSamples.cpp
	StdafxSamples.h
)

set(SIMPLE_SAMPLES
	SimpleSamples/CSGSample.cpp
	SimpleSamples/CSGSample.h
	SimpleSamples/CSWriteToTextureSample.cpp
	SimpleSamples/CSWriteToTextureSample.h
	SimpleSamples/FractalSample.cpp
	SimpleSamples/FractalSample.h
	SimpleSamples/HeightMapSample.cpp
	SimpleSamples/HeightMapSample.h
	SimpleSamples/ParallaxMappingSample.cpp
	SimpleSamples/ParallaxMappingSample.h
	SimpleSamples/RenderToTextureSample.cpp
	SimpleSamples/RenderToTextureSample.h
	SimpleSamples/StreamOutputSample.cpp
	SimpleSamples/StreamOutputSample.h
	SimpleSamples/TesselationSample.cpp
	SimpleSamples/TesselationSample.h
	SimpleSamples/WelcomeScreenSample.cpp
	SimpleSamples/WelcomeScreenSample.h
)

set(EXTENDED_SAMPLES
	ExtendedSamples/DeferredShadingSample.cpp
	ExtendedSamples/DeferredShadingSample.h
	ExtendedSamples/OITSample.cpp
	ExtendedSamples/OITSample.h
	ExtendedSamples/ParticlesSample.cpp
	ExtendedSamples/ParticlesSample.h
	ExtendedSamples/PBRSample.cpp
	ExtendedSamples/PBRSample.h
	ExtendedSamples/PSSMSample.cpp
	ExtendedSamples/PSSMSample.h
	ExtendedSamples/ShadowMapSample.cpp
	ExtendedSamples/ShadowMapSample.h
	ExtendedSamples/SSAOSample.cpp
	ExtendedSamples/SSAOSample.h
	ExtendedSamples/SSLRSample.cpp
	ExtendedSamples/SSLRSample.h
	ExtendedSamples/VarianceShadowMappingSample.cpp
	ExtendedSamples/VarianceShadowMappingSample.h
)

set(ShaderFolder "../Data/Shaders/HLSL")
set(SHADERS_COMMON
	${ShaderFolder}/ConstantBuffers.inl
	${ShaderFolder}/InputLayouts.inl

	${ShaderFolder}/ScreenQuad.ps
	${ShaderFolder}/ScreenQuad.vs
	${ShaderFolder}/ScreenQuadCommon.inl

	${ShaderFolder}/TestShader.ps
	${ShaderFolder}/TestShader.vs
	${ShaderFolder}/TestShaderCommon.inl
)

set(CS_WRITE_TO_TEXTURE_SHADER
	${ShaderFolder}/CSWriteToTexture/Main.compute
)

set(DEFERRED_SHADING_SHADERS
	${ShaderFolder}/DeferredShading/FirstLightPass.vs

	${ShaderFolder}/DeferredShading/GeometryPass.ps
	${ShaderFolder}/DeferredShading/GeometryPass.vs
	${ShaderFolder}/DeferredShading/GeometryPassCommon.inl

	${ShaderFolder}/DeferredShading/SecondLightPass.ps
	${ShaderFolder}/DeferredShading/SecondLightPass.vs
	${ShaderFolder}/DeferredShading/SecondLightPassCommon.inl

	${ShaderFolder}/DeferredShading/SunPass.ps
	${ShaderFolder}/DeferredShading/SunPass.vs
	${ShaderFolder}/DeferredShading/SunPassCommon.inl
)

set(FRACTALS_SHADERS
	${ShaderFolder}/Fractals/MandelbrotFractal.ps
	${ShaderFolder}/Fractals/MandelbrotFractal.vs
	${ShaderFolder}/Fractals/MandelbrotFractal.inl
)

set(OIT_SHADERS
	${ShaderFolder}/OIT/LinkedListsCommon.inl
	${ShaderFolder}/OIT/LLClearPass.ps
	${ShaderFolder}/OIT/LLClearPass.vs
	${ShaderFolder}/OIT/LLClearPassCommon.inl
	${ShaderFolder}/OIT/LLFirstPass.ps
	${ShaderFolder}/OIT/LLFirstPass.vs
	${ShaderFolder}/OIT/LLFirstPassCommon.inl
	${ShaderFolder}/OIT/LLSecondPass.ps
	${ShaderFolder}/OIT/LLSecondPass.vs
	${ShaderFolder}/OIT/LLSecondPassCommon.inl
	${ShaderFolder}/OIT/WBFirstPass.ps
	${ShaderFolder}/OIT/WBFirstPass.vs
	${ShaderFolder}/OIT/WBFirstPassCommon.inl
	${ShaderFolder}/OIT/WBSecondPass.ps
	${ShaderFolder}/OIT/WBSecondPass.vs
	${ShaderFolder}/OIT/WBSecondPassCommon.inl
)

set(PARALLAX_MAPPING_SHADERS
	${ShaderFolder}/ParallaxMapping/Main.ps
	${ShaderFolder}/ParallaxMapping/Main.vs
	${ShaderFolder}/ParallaxMapping/MainCommon.inl
)

set(PARTICLES_SHADERS
	${ShaderFolder}/Particles/ParticlesRender.gs
	${ShaderFolder}/Particles/ParticlesRender.ps
	${ShaderFolder}/Particles/ParticlesRender.vs
	${ShaderFolder}/Particles/ParticlesRenderCommon.inl
	${ShaderFolder}/Particles/ParticlesUpdate.compute
)

set(PBR_SHADERS
	${ShaderFolder}/PBR/CubeMapPass.ps
	${ShaderFolder}/PBR/CubeMapPass.vs
	${ShaderFolder}/PBR/CubeMapPass.inl
	${ShaderFolder}/PBR/Main.ps
	${ShaderFolder}/PBR/Main.vs
	${ShaderFolder}/PBR/Main.inl
)

set(PSSM_SHADERS
	${ShaderFolder}/PSSM/DepthPass.vs
	${ShaderFolder}/PSSM/DepthPass.ps
	${ShaderFolder}/PSSM/DepthPassCommon.inl
	${ShaderFolder}/PSSM/RenderPass.vs
	${ShaderFolder}/PSSM/RenderPass.ps
	${ShaderFolder}/PSSM/RenderPassCommon.inl
	${ShaderFolder}/PSSM/Skydome.vs
	${ShaderFolder}/PSSM/Skydome.ps
	${ShaderFolder}/PSSM/SkydomeCommon.inl
)

set(RENDER_TO_TEXTURE_SHADERS
	${ShaderFolder}/RenderToTexture/DepthPass.ps
	${ShaderFolder}/RenderToTexture/DepthPass.vs
	${ShaderFolder}/RenderToTexture/DepthPassCommon.inl
)

set(SHADOW_MAPPING_SHADERS
	${ShaderFolder}/ShadowMapping/DepthPass.ps
	${ShaderFolder}/ShadowMapping/DepthPass.vs
	${ShaderFolder}/ShadowMapping/DepthPassCommon.inl

	${ShaderFolder}/ShadowMapping/RenderPass.ps
	${ShaderFolder}/ShadowMapping/RenderPass.vs
	${ShaderFolder}/ShadowMapping/RenderPassCommon.inl
)

set(SSAO_SHADERS
	${ShaderFolder}/SSAO/Common.inl
	${ShaderFolder}/SSAO/BlurPass.inl
	${ShaderFolder}/SSAO/BlurPass.ps
	${ShaderFolder}/SSAO/BlurPass.vs
	${ShaderFolder}/SSAO/GeometryPass.ps
	${ShaderFolder}/SSAO/GeometryPass.vs
	${ShaderFolder}/SSAO/GeometryPass.inl
	${ShaderFolder}/SSAO/SSAOPass.ps
	${ShaderFolder}/SSAO/SSAOPass.vs
	${ShaderFolder}/SSAO/SSAOPass.inl
)

set(SSLR_SHADERS
	${ShaderFolder}/SSLR/Common.inl
	${ShaderFolder}/SSLR/GeometryPass.inl
	${ShaderFolder}/SSLR/GeometryPass.ps
	${ShaderFolder}/SSLR/GeometryPass.vs
	${ShaderFolder}/SSLR/SSLRPass.inl
	${ShaderFolder}/SSLR/SSLRPass.ps
	${ShaderFolder}/SSLR/SSLRPass.vs
)

set(TESSELLATION_SHADERS
	${ShaderFolder}/Tessellation/Tessellation.ds
	${ShaderFolder}/Tessellation/Tessellation.hs
	${ShaderFolder}/Tessellation/Tessellation.ps
	${ShaderFolder}/Tessellation/Tessellation.vs

	${ShaderFolder}/Tessellation/TessellationWireframe.ds
	${ShaderFolder}/Tessellation/TessellationWireframe.gs
	${ShaderFolder}/Tessellation/TessellationWireframe.hs
	${ShaderFolder}/Tessellation/TessellationWireframe.ps
	${ShaderFolder}/Tessellation/TessellationWireframe.vs
)

set(VARIANCE_SHADOW_MAPPING_SHADERS
	${ShaderFolder}/VarianceShadowMapping/DepthPassCommon.inl
	${ShaderFolder}/VarianceShadowMapping/DepthPass.ps
	${ShaderFolder}/VarianceShadowMapping/DepthPass.vs
	${ShaderFolder}/VarianceShadowMapping/RenderPassCommon.inl
	${ShaderFolder}/VarianceShadowMapping/RenderPass.ps
	${ShaderFolder}/VarianceShadowMapping/RenderPass.vs
)

set(VIEWFRUSTUM_SHADERS
	${ShaderFolder}/ViewFrustum/Main.gs
	${ShaderFolder}/ViewFrustum/Main.ps
	${ShaderFolder}/ViewFrustum/Main.vs
	${ShaderFolder}/ViewFrustum/MainCommon.inl
)

source_group("" FILES ${SAMPLES_SOURCES})
source_group("SimpleSamples" FILES ${SIMPLE_SAMPLES})
source_group("ExtendedSamples" FILES ${EXTENDED_SAMPLES})

source_group("Shaders" FILES ${SHADERS_COMMON})
source_group("Shaders\\CS write o texture" FILES	${CS_WRITE_TO_TEXTURE_SHADER})
source_group("Shaders\\Deferred shading" FILES		${DEFERRED_SHADING_SHADERS})
source_group("Shaders\\Fractals" FILES				${FRACTALS_SHADERS})
source_group("Shaders\\OIT" FILES					${OIT_SHADERS})
source_group("Shaders\\ParallaxMapping" FILES		${PARALLAX_MAPPING_SHADERS})
source_group("Shaders\\Particles" FILES				${PARTICLES_SHADERS})
source_group("Shaders\\PBR" FILES					${PBR_SHADERS})
source_group("Shaders\\PSSM" FILES					${PSSM_SHADERS})
source_group("Shaders\\Render to texture" FILES		${RENDER_TO_TEXTURE_SHADERS})
source_group("Shaders\\Shadow mapping" FILES		${SHADOW_MAPPING_SHADERS})
source_group("Shaders\\SSAO" FILES					${SSAO_SHADERS})
source_group("Shaders\\SSLR" FILES					${SSLR_SHADERS})
source_group("Shaders\\Tessellation" FILES			${TESSELLATION_SHADERS})
source_group("Shaders\\VarianceSM" FILES			${VARIANCE_SHADOW_MAPPING_SHADERS})
source_group("Shaders\\View frustum" FILES			${VIEWFRUSTUM_SHADERS})

add_executable(Samples WIN32
	${SAMPLES_SOURCES}
	${SIMPLE_SAMPLES}
	${EXTENDED_SAMPLES}

	${SHADERS_COMMON}
	${CS_WRITE_TO_TEXTURE_SHADER}
	${DEFERRED_SHADING_SHADERS}
	${FRACTALS_SHADERS}
	${OIT_SHADERS}
	${PARALLAX_MAPPING_SHADERS}
	${PARTICLES_SHADERS}
	${PBR_SHADERS}
	${PSSM_SHADERS}
	${RENDER_TO_TEXTURE_SHADERS}
	${SHADOW_MAPPING_SHADERS}
	${SSAO_SHADERS}
	${SSLR_SHADERS}
	${TESSELLATION_SHADERS}
	${VARIANCE_SHADOW_MAPPING_SHADERS}
	${VIEWFRUSTUM_SHADERS}
)

target_link_libraries(Samples
	PRIVATE
	Core
	ImGui
	RHI
	WinPixEventRuntime.lib
)

target_include_directories(Samples
	PUBLIC
	.
)

add_custom_command(TARGET Samples POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E copy_if_different
	"..\\..\\${THIRD_PARTY_DIR}\\FBX\\2015.1\\lib\\vs2013\\x64\\release\\libfbxsdk.dll"
	$(TargetDir)libfbxsdk.dll

	COMMAND ${CMAKE_COMMAND} -E copy_if_different
	"..\\..\\${THIRD_PARTY_DIR}\\WinPixEventRuntime\\bin\\WinPixEventRuntime.dll"
	$(TargetDir)WinPixEventRuntime.dll

	COMMAND ${CMAKE_COMMAND} -E copy_if_different
	"..\\..\\${THIRD_PARTY_DIR}\\assimp\\bin\\x64\\assimp-vc140-mt.dll"
	$(TargetDir)assimp-vc140-mt.dll

	COMMAND ${CMAKE_COMMAND} -E copy_if_different
	"..\\..\\${THIRD_PARTY_DIR}\\rttr\\bin\\rttr_core.dll"
	$(TargetDir)rttr_core.dll

	COMMAND ${CMAKE_COMMAND} -E copy_if_different
	"..\\..\\${THIRD_PARTY_DIR}\\rttr\\bin\\rttr_core_d.dll"
	$(TargetDir)rttr_core_d.dll
)

USE_PRECOMPILED_HEADER(Samples "StdafxSamples.h" "StdafxSamples.cpp")
