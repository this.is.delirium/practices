#pragma once
#include "StdafxSamples.h"
#include "Sample.h"

class CSGSample : public Sample
{
public:
	CSGSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor) { }

	void Release() override final;

	void Initialize() override final;

	void Update(const float deltaTime) override final { }
	void Render(RHICommandList* commandList) override final { }

	void RenderImGui() override final;

protected:
	void InitializeRenderingResources() override final { }
	void CreateRootSignatures() override final { }
	void CreatePipelineStates() override final { }
};
