#include "StdafxSamples.h"

#include "HeightMapSample.h"
#include <Application.h>
#include <BuildTasks.h>
#include <Logger.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHIUtils.h>
#include <TaskManager.h>

#include <HeightMapGenerator.h>
#include <PerlinNoiseAlgo.h>

#include <RHI.h>

using namespace GraphicsCore;

/**
 * Function : Release
 */
void HeightMapSample::Release()
{
	Sample::Release();

	mTerrainVertexBuffer.Release();
	mTerrainIndexBuffer.Release();

	RHISafeRelease(mRenderHeightMapTexture);
	RHISafeRelease(mBuildHeightMapTexture);

	gRenderer->CbvSrvUavHeapManager().FreeDescriptors(mTexturesDescriptros);
}

/**
 * Function : Initialize
 */
void HeightMapSample::Initialize()
{
	{
		mSettings.ScreenQuadSize		= DirectX::XMFLOAT4(1040.0f, mApp->GetWindow()->Width() - 15, 360.0f, mApp->GetWindow()->Height() - 15.0f);
		mSettings.SizeMap				= DirectX::XMUINT2(512, 512);

		mSettings.TerrainSizeInWorld	= DirectX::XMFLOAT2(50.0f, 50.0f);
		mSettings.TerrainHeightRange	= DirectX::XMFLOAT2(0.0f, 10.0f);

		mBuildHeightMapProgress = std::make_shared<ProgressIndicator>(0, 1);
		mBuildTerrainProgress = std::make_shared<ProgressIndicator>(0, 1);
	}

	Sample::Initialize();
}

/**
 * Function : InitializeRenderingResources
 */
void HeightMapSample::InitializeRenderingResources()
{
	mTexturesDescriptros = gRenderer->CbvSrvUavHeapManager().GetNewDescriptors(2);
	{
		mGPURenderTextureDescriptor	= gRenderer->CbvSrvUavHeapManager().GpuHandle(mTexturesDescriptros[0]);
		mGPUBuildTextureDescriptor	= gRenderer->CbvSrvUavHeapManager().GpuHandle(mTexturesDescriptros[1]);
	}

	const UINT maxVertices = mSettings.SizeMap.x * mSettings.SizeMap.y;
	// Create a vertex buffer.
	{
		Vertex quads[8];
		quads[0].Position = DirectX::XMFLOAT3(-1.0f, 1.0f, -1.0f);	quads[0].Normal = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
		quads[1].Position = DirectX::XMFLOAT3(-1.0f, -1.0f, -1.0f);	quads[1].Normal = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
		quads[2].Position = DirectX::XMFLOAT3(1.0f, -1.0f, -1.0f);	quads[2].Normal = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
		quads[3].Position = DirectX::XMFLOAT3(1.0f, 1.0f, -1.0f);	quads[3].Normal = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);

		quads[4].Position = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);	quads[4].Normal = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);
		quads[5].Position = DirectX::XMFLOAT3(0.0f, -1.0f, 0.0f);	quads[5].Normal = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);
		quads[6].Position = DirectX::XMFLOAT3(2.0f, -1.0f, 0.0f);	quads[6].Normal = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);
		quads[7].Position = DirectX::XMFLOAT3(2.0f, 1.0f, 0.0f);	quads[7].Normal = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);

		mTerrainVertexBuffer.Initialize(__TEXT("HeightMap:TerrainVertexBuffer"), maxVertices * sizeof(Vertex), _countof(quads), sizeof(Vertex), quads);
	}

	// Create an index buffer.
	{
		uint32_t indices[] =
		{
			0, 1, 2,
			0, 2, 3,
			4, 5, 6,
			4, 6, 7
		};

		mTerrainIndexBuffer.Initialize(__TEXT("HeightMap:TerrainIndexBuffer"), sizeof(uint32_t) * maxVertices * 6, _countof(indices), sizeof(uint32_t), indices);
	}

	// Create a texture.
	{
		RHIResource* textureUploadHeap;

		RHIResourceDesc desc	= {};
		desc.Dimension			= RHIResourceDimension::Texture2D;
		desc.Format				= RHIFormat::R32Float;
		desc.MipLevels			= 1;
		desc.Width				= mSettings.SizeMap.x;
		desc.Height				= mSettings.SizeMap.y;
		desc.Flags				= RHIResourceFlag::None;
		desc.DepthOrArraySize	= 1;
		desc.SampleDesc.Count	= 1;
		desc.SampleDesc.Quality	= 0;

		mRenderHeightMapTexture = gRenderer->Device()->CreateCommittedResource(
			&RHIHeapProperties(RHICpuPageProperty::WriteCombine, RHIMemoryPool::L0),
			RHIHeapFlag::None,
			&desc,
			RHIResourceState::Common,
			nullptr);

		mBuildHeightMapTexture = gRenderer->Device()->CreateCommittedResource(
			&RHIHeapProperties(RHICpuPageProperty::WriteCombine, RHIMemoryPool::L0),
			RHIHeapFlag::None,
			&desc,
			RHIResourceState::Common,
			nullptr);

		UINT64 uploadBufferSize = gRenderer->Device()->GetRequiredIntermediateSize(mRenderHeightMapTexture, 0, 1);

		// Create a GPU upload buffer.
		textureUploadHeap = gRenderer->Device()->CreateCommittedResource(
			&RHIHeapProperties(RHIHeapType::Upload),
			RHIHeapFlag::None,
			&RHIResourceDesc::Buffer(uploadBufferSize),
			RHIResourceState::GenericRead,
			nullptr);

		std::vector<float> textureData = GenerateTextureData();

		RHISubresourceData subresourceData	= {};
		subresourceData.Data				= textureData.data();
		subresourceData.RowPitch			= mSettings.SizeMap.x * sizeof(float);
		subresourceData.SlicePitch			= mSettings.SizeMap.y * subresourceData.RowPitch;

		RHIBox box	= {};
		box.left	= 0;
		box.top		= 0;
		box.right	= (UINT)desc.Width;
		box.bottom	= (UINT)desc.Height;
		box.front	= 0u;
		box.back	= 1u;

		mRenderHeightMapTexture->Map(0, nullptr, nullptr);
		mRenderHeightMapTexture->WriteToSubresource(0, &box, subresourceData.Data, subresourceData.RowPitch, subresourceData.SlicePitch);
		mRenderHeightMapTexture->Unmap(0, nullptr);

		mBuildHeightMapTexture->Map(0, nullptr, nullptr);
		mBuildHeightMapTexture->WriteToSubresource(0, &box, subresourceData.Data, subresourceData.RowPitch, subresourceData.SlicePitch);
		mBuildHeightMapTexture->Unmap(0, nullptr);


		// Describe and create a SRV for the texture.
		RHIShaderResourceViewDesc srvDesc	= {};
		srvDesc.Shader4ComponentMapping		= D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.Format						= desc.Format;
		srvDesc.ViewDimension				= RHISRVDimension::Texture2D;
		srvDesc.Texture2D.MipLevels			= 1;

		gRenderer->Device()->CreateShaderResourceView(mRenderHeightMapTexture, &srvDesc, gRenderer->CbvSrvUavHeapManager().CpuHandle(mTexturesDescriptros[0]));
		gRenderer->Device()->CreateShaderResourceView(mBuildHeightMapTexture, &srvDesc, gRenderer->CbvSrvUavHeapManager().CpuHandle(mTexturesDescriptros[1]));

		RHISafeRelease(textureUploadHeap);
	}

	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, true);
}

/**
 * Function : CreateRootSignatures
 */
void HeightMapSample::CreateRootSignatures()
{
	RHIDescriptorRange ranges[2];
	ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);
	ranges[1].Init(RHIDescriptorRangeType::CBV, 1, 1);
		
	RHIRootParameter rootParameters[2];
	rootParameters[0].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::Vertex);
	rootParameters[1].InitAsDescriptorTable(1, &ranges[1], RHIShaderVisibility::Vertex);

	RHIRootSignatureDesc rootSignatureDesc;
	rootSignatureDesc.Init(_countof(rootParameters), &rootParameters[0], 0, nullptr, GetRootSignatureFlags(true, true, false, false, false, false, false));

	mRootSignature = gRenderer->SerializeAndCreateRootSignature(&rootSignatureDesc, RHIRootSignatureVersion::Ver1);
}

/**
 * Function : CreatePipelineStates
 */
void HeightMapSample::CreatePipelineStates()
{
	RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("TestShader.vs"), RHIShaderType::Vertex));
	RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("TestShader.ps"), RHIShaderType::Pixel));

	if (vertexShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
		return;

	RHIGraphicsPipelineStateDesc psoDesc				= {};
	psoDesc.RootSignature								= mRootSignature;
	psoDesc.InputLayout									= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
	psoDesc.VS											= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
	psoDesc.PS											= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
	psoDesc.PrimitiveTopologyType						= RHIPrimitiveTopologyType::Triangle;
	psoDesc.SampleMask									= UINT_MAX;
	psoDesc.NumRenderTargets							= 1;
	psoDesc.RTVFormats[0]								= gRenderer->RenderTargetFormat();
	psoDesc.DSVFormat									= gRenderer->DepthStencilViewFormat();
	psoDesc.SampleDesc.Count							= 1;

	psoDesc.DepthStencilState.DepthEnable				= TRUE;
	psoDesc.DepthStencilState.DepthFunc					= RHIDepthFunc::LEqual;
	psoDesc.DepthStencilState.DepthWriteMask			= RHIDepthWriteMask::On;
	psoDesc.DepthStencilState.StencilEnable				= FALSE;

	psoDesc.RasterizerState								= RHIRasterizerDesc(RHIDefault());
	psoDesc.RasterizerState.CullMode					= RHICullMode::None;
	psoDesc.RasterizerState.FrontCounterClockwise		= FALSE;

	psoDesc.BlendState									= RHIBlendDesc(RHIDefault());
	psoDesc.BlendState.RenderTarget[0].BlendEnable		= TRUE;
	psoDesc.BlendState.RenderTarget[0].SrcBlend			= RHIBlend::SrcAlpha;
	psoDesc.BlendState.RenderTarget[0].DestBlend		= RHIBlend::InvSrcAlpha;
	psoDesc.BlendState.RenderTarget[0].SrcBlendAlpha	= RHIBlend::SrcAlpha;
	psoDesc.BlendState.RenderTarget[0].DestBlendAlpha	= RHIBlend::InvSrcAlpha;

	mPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
}

/**
 * Function : GenerateTextureData
 */
std::vector<float> HeightMapSample::GenerateTextureData()
{
	std::vector<float> data(mSettings.SizeMap.x * mSettings.SizeMap.y);

	for (UINT y = 0; y < mSettings.SizeMap.y; ++y)
	{
		for (UINT x = 0; x < mSettings.SizeMap.x; ++x)
		{
			data[x + y * mSettings.SizeMap.x] = (float)y / (float)mSettings.SizeMap.y;
		}
	}

	return data;
}

/**
 * Function : RenderImGui
 */
void HeightMapSample::RenderImGui()
{
	ImGui::InputInt("Octaves", &mSettings.NbOfOctaves);
	ImGui::InputFloat("Persistence", &mSettings.Persistence, 0.1f, 1.0f);
	ImGui::InputFloat("Frequency", &mSettings.Frequency, 0.1f, 1.0f);
	ImGui::InputFloat("Amplitude", &mSettings.Amplitude, 0.1f, 1.0f);

	if (ImGui::Button("Build height map"))
	{
		BuildHeightMap();
	}
	ImGui::SameLine();
	ImGui::ProgressBar(mBuildHeightMapProgress->FloatCurrentValue());

	ImGui::SliderFloat2("size", &mSettings.TerrainSizeInWorld.x, 10.0f, 250.0f);
	ImGui::SliderFloat2("height range:", &mSettings.TerrainHeightRange.x, 0.0f, 50.0f);
	if (ImGui::Button("Build terrain"))
	{
		BuildTerrain();
	}
	ImGui::SameLine();
	ImGui::ProgressBar(mBuildTerrainProgress->FloatCurrentValue());

	if (ImGui::Button("Swap textures"))
	{
		std::swap(mGPURenderTextureDescriptor, mGPUBuildTextureDescriptor);
		std::swap(mRenderHeightMapTexture, mBuildHeightMapTexture);
	}

	ImGui::Checkbox("Show texture", &mUseScreenQuad);
}

/**
 * Function : Update
 */
void HeightMapSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	mScreenQuad->SetTexture(mGPURenderTextureDescriptor, ScreenQuadPart::TextureType::ColorRGBA);
}

/**
 * Function : Render
 */
void HeightMapSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };
	if (mTerrainIndexBuffer.NumElements() > 0)
	{
		commandList->SetPipelineState(mPSO);
		commandList->SetGraphicsRootSignature(mRootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetGraphicsRootDescriptorTable(0, mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(1, mConstBufferPerObject.SrvHandle().Gpu());
		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);
		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
		commandList->IASetVertexBuffers(0, 1, mTerrainVertexBuffer.View());
		commandList->IASetIndexBuffer(mTerrainIndexBuffer.View());
		commandList->DrawIndexedInstanced(mTerrainIndexBuffer.NumElements(), 1, 0, 0, 0);
	}

	if (mUseScreenQuad)
	{
		DrawScreenQuad(commandList, heaps, _countof(heaps));
	}
}

/**
 * Function : Activate
 */
void HeightMapSample::Activate()
{
	Initialize();

	if (mHeightMapGenerator.IsEmpty())
	{
		BuildHeightMap();
		BuildTerrain();
	}
}

/**
 * Function : BuildHeightMap
 */
void HeightMapSample::BuildHeightMap()
{
	mHeightMapGenerator.CreateAlgo<PerlinNoiseAlgo>(mSettings.NbOfOctaves, mSettings.Persistence, mSettings.Frequency, mSettings.Amplitude);

	BuildTasks::HeightMapTask* task = new BuildTasks::HeightMapTask(&mRenderHeightMapTexture, &mBuildHeightMapTexture, &mGPURenderTextureDescriptor, &mGPUBuildTextureDescriptor,
		&mHeightMapGenerator, mSettings.SizeMap, mBuildHeightMapProgress.get());

	STaskManager::Instance().Add(task);
}

/**
 * Function : BuildTerrain
 */
void HeightMapSample::BuildTerrain()
{
	BuildTasks::TerrainTask* task = new BuildTasks::TerrainTask(&mTerrainVertexBuffer, &mTerrainIndexBuffer,
		&mTerrainBuilder, std::cref(mHeightMapGenerator.Map()), std::cref(mSettings.TerrainSizeInWorld), std::cref(mSettings.TerrainHeightRange), mBuildTerrainProgress.get());
	
	STaskManager::Instance().Add(task);
}
