#include "StdafxSamples.h"
#include "TesselationSample.h"
#include <Application.h>
#include <InputLayouts.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHIUtils.h>

using namespace GraphicsCore;

namespace
{
	static const char* kNamedType[] =
	{
		"Triangle",
		"Icosahedron",
	};

	enum class RootParams
	{
		Tessellation	= 0,
		PerFrame		= 1,
		PerObject		= 2
	};
}

/**
 * Function : Release
 */
void TessellationSample::Release()
{
	Sample::Release();

	RHISafeRelease(mTessellationPSO);
	RHISafeRelease(mTessellationRootSignature);
	RHISafeRelease(mTessellationWireframePSO);
	RHISafeRelease(mTessellationWireframeRootSignature);

	mTriangleVertexBuffer.Release();

	mIcosahedronVertexBuffer.Release();
	mIcosahedronIndexBuffer.Release();

	mParams.Release();
}

/**
 * Function : Initialize
 */
void TessellationSample::Initialize()
{
	mParams.Data().InnerLevel	= DirectX::XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f);
	mParams.Data().OuterLevel	= DirectX::XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	mTypeOfPrimitive			= Primitive::Icosahedron;

	Sample::Initialize();
}

/**
 * Function : InitializeRenderingResources
 */
void TessellationSample::InitializeRenderingResources()
{
	// Create a vertex buffers and fill it.
	{
		// Create a vertex buffer for a triangle.
		{
			const float triangle[] =
			{
				-5.0f, 0.0f, 0.0f,
				0.0f, -5.0f, 0.0f,
				0.0f, 5.0f, 0.0f
			};
			mTriangleVertexBuffer.Initialize(__TEXT("Tessellation:TriangleVB"), sizeof(triangle), 3, sizeof(float) * 3, triangle);
		}

		// Create a vertex buffer for a icosahedron.
		{
			const float icosahedron[] =
			{
				0.000f,  0.000f,  1.000f,
				0.894f,  0.000f,  0.447f,
				0.276f,  0.851f,  0.447f,
				-0.724f,  0.526f,  0.447f,
				-0.724f, -0.526f,  0.447f,
				0.276f, -0.851f,  0.447f,
				0.724f,  0.526f, -0.447f,
				-0.276f,  0.851f, -0.447f,
				-0.894f,  0.000f, -0.447f,
				-0.276f, -0.851f, -0.447f,
				0.724f, -0.526f, -0.447f,
				0.000f,  0.000f, -1.000f
			};

			mIcosahedronVertexBuffer.Initialize(__TEXT("Tessellation:IcosahedronVB"), sizeof(icosahedron), _countof(icosahedron) / 3, sizeof(float) * 3, icosahedron);
		}
	}

	// Create an index buffer and fill it.
	{
		const int indices[] = {
			2, 1, 0,
			3, 2, 0,
			4, 3, 0,
			5, 4, 0,
			1, 5, 0,
			11, 6,  7,
			11, 7,  8,
			11, 8,  9,
			11, 9,  10,
			11, 10, 6,
			1, 2, 6,
			2, 3, 7,
			3, 4, 8,
			4, 5, 9,
			5, 1, 10,
			2,  7, 6,
			3,  8, 7,
			4,  9, 8,
			5, 10, 9,
			1, 6, 10
		};
		mIcosahedronIndexBuffer.Initialize(__TEXT("Tessellation:TriangleIB"), sizeof(indices), _countof(indices), sizeof(uint32_t), indices);
	}

	// Fill constant buffers.
	{
		const UINT64 size = 1024 * 4;

		mParams.Initialize(__TEXT("Tessellation:CBParams"), size, 1, sizeof(ConstantBuffers::Tessellation));
		CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, true);
	}
}

/**
 * Function : CreateRootSignatures
 */
void TessellationSample::CreateRootSignatures()
{
	{
		RHIDescriptorRange descriptorRanges[3];
		descriptorRanges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);
		descriptorRanges[1].Init(RHIDescriptorRangeType::CBV, 1, 1);
		descriptorRanges[2].Init(RHIDescriptorRangeType::CBV, 1, 2);

		RHIRootParameter rootParameters[3];
		rootParameters[ToInt(RootParams::Tessellation)].InitAsDescriptorTable(1, &descriptorRanges[0], RHIShaderVisibility::All);
		rootParameters[ToInt(RootParams::PerFrame)].InitAsDescriptorTable(1, &descriptorRanges[1], RHIShaderVisibility::All);
		rootParameters[ToInt(RootParams::PerObject)].InitAsDescriptorTable(1, &descriptorRanges[2], RHIShaderVisibility::All);

		RHIRootSignatureDesc rootSignatureDesc;
		RHIRootSignatureFlags flags = GetRootSignatureFlags(true, true, true, true, true, true, false);

		rootSignatureDesc.Init(_countof(rootParameters), &rootParameters[0], 0, nullptr, flags);

		mTessellationRootSignature = gRenderer->SerializeAndCreateRootSignature(&rootSignatureDesc, RHIRootSignatureVersion::Ver1);
	}

	{
		RHIDescriptorRange descriptorRanges[3];
		descriptorRanges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);
		descriptorRanges[1].Init(RHIDescriptorRangeType::CBV, 1, 1);
		descriptorRanges[2].Init(RHIDescriptorRangeType::CBV, 1, 2);

		RHIRootParameter rootParameters[3];
		rootParameters[ToInt(RootParams::Tessellation)].InitAsDescriptorTable(1, &descriptorRanges[0], RHIShaderVisibility::All);
		rootParameters[ToInt(RootParams::PerFrame)].InitAsDescriptorTable(1, &descriptorRanges[1], RHIShaderVisibility::All);
		rootParameters[ToInt(RootParams::PerObject)].InitAsDescriptorTable(1, &descriptorRanges[2], RHIShaderVisibility::All);

		RHIRootSignatureDesc rootSignatureDesc;
		RHIRootSignatureFlags flags = GetRootSignatureFlags(true, true, true, true, true, true, false);
		rootSignatureDesc.Init(_countof(rootParameters), &rootParameters[0], 0, nullptr, flags);

		mTessellationWireframeRootSignature = gRenderer->SerializeAndCreateRootSignature(&rootSignatureDesc, RHIRootSignatureVersion::Ver1);
	}
}

/**
 * Function : CreatePipelineStates
 */
void TessellationSample::CreatePipelineStates()
{
	{
		RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("Tessellation:Tessellation.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper hullShader		(gRenderer->LoadShader(__TEXT("Tessellation:Tessellation.hs"), RHIShaderType::Hull));
		RHIBlobWrapper domainShader		(gRenderer->LoadShader(__TEXT("Tessellation:Tessellation.ds"), RHIShaderType::Domain));
		RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("Tessellation:Tessellation.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || hullShader.Blob->IsNull() || domainShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc			= {};
		psoDesc.RootSignature							= mTessellationRootSignature;
		psoDesc.InputLayout								= { InputLayouts::Tessellation::inputElementDesc, _countof(InputLayouts::Tessellation::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
		psoDesc.HS										= { hullShader.GetBufferPointer(), hullShader.GetBufferSize() };
		psoDesc.DS										= { domainShader.GetBufferPointer(), domainShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Patch;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.DSVFormat								= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= TRUE;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable			= FALSE;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;
		psoDesc.RasterizerState.FrontCounterClockwise	= FALSE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp		= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha	= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend		= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend	= RHIBlend::InvSrcAlpha;

		mTessellationPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	{
		RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("Tessellation:TessellationWireframe.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper hullShader		(gRenderer->LoadShader(__TEXT("Tessellation:TessellationWireframe.hs"), RHIShaderType::Hull));
		RHIBlobWrapper domainShader		(gRenderer->LoadShader(__TEXT("Tessellation:TessellationWireframe.ds"), RHIShaderType::Domain));
		RHIBlobWrapper geometryShader	(gRenderer->LoadShader(__TEXT("Tessellation:TessellationWireframe.gs"), RHIShaderType::Geometry));
		RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("Tessellation:TessellationWireframe.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || hullShader.Blob->IsNull() || domainShader.Blob->IsNull() || geometryShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc			= {};
		psoDesc.RootSignature							= mTessellationWireframeRootSignature;
		psoDesc.InputLayout								= { InputLayouts::Tessellation::inputElementDesc, _countof(InputLayouts::Tessellation::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.PS										= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
		psoDesc.HS										= { hullShader.GetBufferPointer(), hullShader.GetBufferSize() };
		psoDesc.DS										= { domainShader.GetBufferPointer(), domainShader.GetBufferSize() };
		psoDesc.GS										= { geometryShader.GetBufferPointer(), geometryShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Patch;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.DSVFormat								= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= TRUE;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable			= FALSE;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;
		psoDesc.RasterizerState.FrontCounterClockwise	= TRUE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp		= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha	= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend		= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend	= RHIBlend::InvSrcAlpha;

		mTessellationWireframePSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : RenderImGui
 */
void TessellationSample::RenderImGui()
{
	ImGui::Combo("Mesh", reinterpret_cast<int*> (&mTypeOfPrimitive), kNamedType, _countof(kNamedType));
	ImGui::InputFloat2("Inner level", &mParams.Data().InnerLevel.x);
	ImGui::InputFloat4("Outer level", &mParams.Data().OuterLevel.x);

	ImGui::SliderFloat("Scale", &mScale, 1.0f, 10.0f);

	ImGui::Checkbox("Wireframe", &mIsWireframe);
	ImGui::SameLine();
	ImGui::Checkbox("Rotate", &mIsRotate);
}

/**
 * Function : Update
 */
void TessellationSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	// Update local transform.
	{
		static float time = 0.0f;

		DirectX::XMMATRIX locTrans = DirectX::XMMatrixScaling(mScale, mScale, mScale) * DirectX::XMMatrixRotationAxis(DirectX::XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f), time * DirectX::XMConvertToRadians(180.0f));
		mConstBufferPerObject.Data().WorldMatrix = locTrans;
		if (mIsRotate)
		{
			time += deltaTime;
		}

		mConstBufferPerObject.UpdateDataOnGPU();
	}

	// Update tesselation params.
	mParams.UpdateDataOnGPU();
}

/**
 * Function : Render
 */
void TessellationSample::Render(RHICommandList* commandList)
{
	RHIPipelineState* pso			= mIsWireframe ? mTessellationWireframePSO : mTessellationPSO;
	RHIRootSignature* rootSignature	= mIsWireframe ? mTessellationWireframeRootSignature : mTessellationRootSignature;

	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };

	commandList->SetPipelineState(pso);
	commandList->SetGraphicsRootSignature(rootSignature);
	commandList->SetDescriptorHeaps(_countof(heaps), heaps);
	commandList->SetGraphicsRootDescriptorTable(ToInt(RootParams::Tessellation), mParams.SrvHandle().Gpu());
	commandList->SetGraphicsRootDescriptorTable(ToInt(RootParams::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
	commandList->SetGraphicsRootDescriptorTable(ToInt(RootParams::PerObject), mConstBufferPerObject.SrvHandle().Gpu());
	commandList->RSSetViewports(1, &mViewport);
	commandList->RSSetScissorRects(1, &mScissorRect);
	commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::Control_Point_Patchlist_3);

	if (mTypeOfPrimitive == Primitive::Triangle)
	{
		commandList->IASetVertexBuffers(0, 1, mTriangleVertexBuffer.View());
		commandList->DrawInstanced(mTriangleVertexBuffer.NumElements(), 1, 0, 0);
	}
	else if (mTypeOfPrimitive == Primitive::Icosahedron)
	{
		commandList->IASetVertexBuffers(0, 1, mIcosahedronVertexBuffer.View());
		commandList->IASetIndexBuffer(mIcosahedronIndexBuffer.View());
		commandList->DrawIndexedInstanced(mIcosahedronIndexBuffer.NumElements(), 1, 0, 0, 0);
	}
}
