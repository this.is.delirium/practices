#pragma once
#include "Sample.h"
#include <RHI.h>
#include <RHIIndexBuffer.h>
#include <RHIVertexBuffer.h>

class RHIPipelineState;
class RHIResource;

class TessellationSample : public Sample
{
public:
	TessellationSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor) { }

	void Release() override final;

	void Initialize() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	void RenderImGui() override final;

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

protected:
	enum class Primitive
	{
		Triangle,
		Icosahedron
	};

protected:
	Primitive				mTypeOfPrimitive;

	RHIPipelineState*		mTessellationPSO					= nullptr;
	RHIPipelineState*		mTessellationWireframePSO			= nullptr;
	RHIRootSignature*		mTessellationRootSignature			= nullptr;
	RHIRootSignature*		mTessellationWireframeRootSignature	= nullptr;
	bool					mIsWireframe						= true;

	RHIVertexBuffer			mTriangleVertexBuffer;

	RHIVertexBuffer			mIcosahedronVertexBuffer;
	RHIIndexBuffer			mIcosahedronIndexBuffer;

	ConstBufferTessellation	mParams;

	bool					mIsRotate							= true;
	float					mScale								= 1.0f;
};
