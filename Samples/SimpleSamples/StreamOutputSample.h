#pragma once
#include "StdafxSamples.h"
#include <InputLayouts.h>
#include <Sample.h>

class StreamOutputSample : public Sample
{
public:
	StreamOutputSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor) { }
	~StreamOutputSample() { }

	void Release() override final;

	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

protected:
	__declspec(align(16))
	struct ParticleGeneratorSettings
	{
		glm::vec4	Position;
		glm::vec4	MinVelocity;
		glm::vec4	MaxVelocity;
		glm::vec4	GravityVector;
		glm::vec4	Color;
		glm::vec4	Seed;
		glm::vec4 	Settings; // x - minLifeTime, y - maxLifeTime, z - size, w - timePassed
		int			Count;
	};

protected:
	RHIPipelineState*			mUpdatePSO						= nullptr;
	RHIPipelineState*			mRenderPSO						= nullptr;

	RHIRootSignature*			mUpdateRootSignature			= nullptr;
	RHIRootSignature*			mRenderRootSignature			= nullptr;

	RHIGPUDescriptorHandle		mUpdateGpuHandleRootParameter0;

	RHIResource*				mSOBuffer[2]					= { nullptr, nullptr };
	RHIStreamOutputBufferView	mSOBufferView[2];
	RHIVertexBufferView			mSOVertexBufferView[2];

	RHIResource*				mVertexBuffer[2]				= { nullptr, nullptr };
	RHIVertexBufferView			mVertexBufferView[2];

	RHIResource*				mReadbackBuffer					= nullptr;

	UINT						mCurrentVB;
	UINT						mCurrentSOB;

	RHIResource*				mConstBufferGeneratorSettings	= nullptr;
	ParticleGeneratorSettings	mConstBufferGeneratorSettingsData;

	float						mTime = 0.0f;
	float						mGenerateEveryNSeconds;
	UINT						mGenerateParticles;

	UINT						mCurrentNbOfParticles;
	const int					mMaxParticles = 100000;

	using Vertex = InputLayouts::StreamOutput::Vertex;
};
