#pragma once
#include "StdafxSamples.h"
#include "Sample.h"
#include <RHIVertexBuffer.h>

class RHIPipelineState;

class FractalSample : public Sample
{
public:
	FractalSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor) { }

	void Release() override final;

	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

protected:
	using DisplaySettings = std::function < void(void) > ;

	void DisplayMandelbrot2dSettings();
	void DisplayJulia2dSettings();

	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

protected:
	enum class FractalType
	{
		Mandelbrot2d = 0,
		BurningShip2d
	};

	std::map<FractalType, DisplaySettings> mDisplaySettings;
	
	ConstBufferMandelbrot2d	mMandelbrot2dConstBuffer;

	FractalType				mType;

	RHIVertexBuffer			mQuadVertexBuffer;
};
