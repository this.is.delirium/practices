#include "StdafxSamples.h"
#include "CSWriteToTextureSample.h"

#include <Application.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RootSignature.h>

using namespace GraphicsCore;

namespace
{
	enum class ComputePassRootSignature
	{
		WriteTexture = 0,
		Count
	};
}

/**
 * Function : Release
 */
void CSWriteToTextureSample::Release()
{
	Sample::Release();

	mTexture.Release();
}

/**
 * Function : Initialize
 */
void CSWriteToTextureSample::Initialize()
{
	Sample::Initialize();

	if (mUseScreenQuad)
		mScreenQuad->SetTexture(mTexture.SrvHandle().Gpu(), ScreenQuadPart::TextureType::ColorRGBA);
}

/**
 * Function : InitializeRenderingResources
 */
void CSWriteToTextureSample::InitializeRenderingResources()
{
	RHISwapChainDesc desc;
	gRenderer->SwapChain()->GetDesc(&desc);

	RHIResourceDesc textureDesc		= {};
	textureDesc.Dimension			= RHIResourceDimension::Texture2D;
	textureDesc.Format				= RHIFormat::R8G8B8A8UNorm;
	textureDesc.Width				= desc.BufferDesc.Width;
	textureDesc.Height				= desc.BufferDesc.Height;
	textureDesc.Flags				= RHIResourceFlag::AllowUnorderedAccess;
	textureDesc.MipLevels			= 1;
	textureDesc.DepthOrArraySize	= 1;
	textureDesc.SampleDesc.Count	= 1;
	textureDesc.SampleDesc.Quality	= 0;

	mTexture.Initialize(&textureDesc);
	mTexture.Texture()->SetName(__TEXT("Full screen texture"));

	gRenderer->ChangeResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mTexture.Texture(), RHIResourceState::Common, RHIResourceState::PixelShaderResource));
}

/**
 * Function : CreateRootSignatures
 */
void CSWriteToTextureSample::CreateRootSignatures()
{
	// Compute pass.
	{
		RHIUtils::RootSignature rootSignature(ToInt(ComputePassRootSignature::Count));
		rootSignature[ToInt(ComputePassRootSignature::WriteTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::All).InitRange(0, RHIDescriptorRangeType::UAV, 1, 0);

		mRootSignature = rootSignature.Finalize(GetRootSignatureFlags(false, false, false, false, false, false, false));
	}
}

/**
 * Function : CreatePipelineStates
 */
void CSWriteToTextureSample::CreatePipelineStates()
{
	// Render pass.
	{
		RHIBlobWrapper computeShader (gRenderer->LoadShader(__TEXT("CSWriteToTexture:Main.compute"), RHIShaderType::Compute));

		if (computeShader.Blob->IsNull())
			return;

		RHIComputePipelineStateDesc psoDesc	= {};
		psoDesc.RootSignature				= mRootSignature;
		psoDesc.CS							= { computeShader.GetBufferPointer(), computeShader.GetBufferSize() };

		mPSO = gRenderer->Device()->CreateComputePipelineState(&psoDesc);
	}
}

/**
 * Function : RenderImGui
 */
void CSWriteToTextureSample::RenderImGui()
{
	ImGui::Text("Param-pam-pam");
}

/**
 * Function : Update
 */
void CSWriteToTextureSample::Update(const float deltaTime)
{

}

/**
 * Function : Render
 */
void CSWriteToTextureSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };
	// Compute pass.
	{
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mTexture.Texture(), RHIResourceState::PixelShaderResource, RHIResourceState::UnorderedAccess));

		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetPipelineState(mPSO);
		commandList->SetComputeRootSignature(mRootSignature);
		commandList->SetComputeRootDescriptorTable(ToInt(ComputePassRootSignature::WriteTexture), mTexture.UavHandle().Gpu());

		commandList->Dispatch((mApp->GetWindow()->Width() / 32) + 1, (mApp->GetWindow()->Height() / 32) + 1, 1);

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mTexture.Texture(), RHIResourceState::UnorderedAccess, RHIResourceState::PixelShaderResource));
	}

	// Draw screen quad.
	if (mUseScreenQuad)
		DrawScreenQuad(commandList, heaps, _countof(heaps));
}
