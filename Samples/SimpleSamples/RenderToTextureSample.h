#pragma once
#include "StdafxSamples.h"
#include <Sample.h>
#include <RHIVertexBuffer.h>

class RenderToTextureSample : public Sample
{
public:
	RenderToTextureSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor, true, DirectX::XMFLOAT2(256, 256)) { }
	~RenderToTextureSample() { }

	void Release() override final;

	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

public:
	bool IsUseOwnRendering() const override final { return true; }

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

	void CopyTextureToTexture(RHICommandList* commandList, RHIResource* src, RHIResource* dst);

protected:
	RHIVertexBuffer			mVertexBuffer;

	RHIResource*			mRTTexture		= nullptr;
	RHIResource*			mCopyTexture	= nullptr;

	RHICPUDescriptorHandle	mCpuHandleRtvRTTexture;
	RHIGPUDescriptorHandle	mGpuHandleRtvRTTexture;

	RHICPUDescriptorHandle	mCpuHandleSrvRTTexture;
	RHIGPUDescriptorHandle	mGpuHandleSrvRTTexture;

	RHIGPUDescriptorHandle	mGpuHandleCopyTexture;

	std::vector<int>		mCopyTextureDescriptor;
	std::vector<int>		mRenderTextureDescriptor;
	std::vector<int>		mRtvDescriptors;

protected:
	enum class Type
	{
		CopyRenderTargetToTexture = 0,
		UseTextureAsRenderTarget,
	};

	Type mType				= Type::CopyRenderTargetToTexture;

	bool mIsShowRTTexture	= false;

};
