#include "StdafxSamples.h"
#include "CSGSample.h"
#include "Application.h"

/**
 * Function : Release
 */
void CSGSample::Release()
{
	Sample::Release();
}

/**
 * Function : Initialize
 */
void CSGSample::Initialize()
{
	Sample::Initialize();
}

/**
 * Function : RenderImGui
 */
void CSGSample::RenderImGui()
{
	ImGui::Text("Not impliment yet.");
}
