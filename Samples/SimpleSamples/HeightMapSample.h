#pragma once
#include "StdafxSamples.h"
#include "Sample.h"
#include <HeightMapGenerator.h>
#include <ProgressIndicator.h>
#include <RHI.h>
#include <RHIIndexBuffer.h>
#include <RHIVertexBuffer.h>
#include <ScreenQuadPart.h>
#include <TerrainBuilder.h>

class OpenGLDevice;
class RHICamera;
class RHIPipelineState;
class RHISampler;
class Window;

class HeightMapSample : public Sample
{
public:
	HeightMapSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor, true, DirectX::XMFLOAT2(256.0f, 256.0f)) { }

	void Release() override final;
	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	void Activate() override final;

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

	std::vector<float> GenerateTextureData();

	void BuildHeightMap();
	void BuildTerrain();

protected:
	struct Settings
	{
		DirectX::XMFLOAT4	ScreenQuadSize;

		DirectX::XMUINT2	SizeMap;
		int					NbOfOctaves	= 5;
		float				Persistence	= 0.5f;
		float				Frequency	= 0.01f;
		float				Amplitude	= 0.5f;

		DirectX::XMFLOAT2	TerrainSizeInWorld;
		DirectX::XMFLOAT2	TerrainHeightRange;
	} mSettings;
	
	std::shared_ptr<ProgressIndicator>	mBuildHeightMapProgress;
	std::shared_ptr<ProgressIndicator>	mBuildTerrainProgress;

	using Vertex = InputLayouts::Main::Vertex;
	TerrainBuilder<Vertex>				mTerrainBuilder;

	RHIVertexBuffer						mTerrainVertexBuffer;
	RHIIndexBuffer						mTerrainIndexBuffer;

	HeightMapGenerator					mHeightMapGenerator;
	RHIResource*						mRenderHeightMapTexture	= nullptr;
	RHIResource*						mBuildHeightMapTexture	= nullptr;

	RHIGPUDescriptorHandle				mGPURenderTextureDescriptor;
	RHIGPUDescriptorHandle				mGPUBuildTextureDescriptor;
	std::vector<int>					mTexturesDescriptros;
};
