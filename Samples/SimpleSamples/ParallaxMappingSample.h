#pragma once
#include "StdafxSamples.h"
#include "Sample.h"
#include <StaticObject.h>
#include <RHITexture.h>

// http://sunandblackcat.com/tipFullView.php?topicid=28
class ParallaxMappingSample : public Sample
{
public:
	ParallaxMappingSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor) { }

	void Release() override final;

	void Initialize() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	void RenderImGui() override final;

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

protected:
	struct Material
	{
		void Release()
		{
			Diffuse.Release();
			NormalMap.Release();
			HeightMap.Release();
		}

		RHITexture Diffuse;
		RHITexture NormalMap;
		RHITexture HeightMap;
	};

	struct ParallaxMappingParams
	{
		DirectX::XMFLOAT4 Params;

		float& Scale() { return Params.x; }
		float& DivideZoneX() { return Params.z; }
		float& Type() { return Params.w; }
	};

protected:
	Application*							mApplication	= nullptr;
	std::shared_ptr<StaticObject>			mFloor;
	Material								mFloorMaterial;
	RHIConstBuffer<ParallaxMappingParams>	mParallaxMappingParamsCB;
};
