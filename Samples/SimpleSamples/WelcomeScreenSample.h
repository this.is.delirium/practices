#pragma once
#include "StdafxSamples.h"
#include "Sample.h"
#include <RHIVertexBuffer.h>

class WelcomeScreenSample : public Sample
{
public:
	WelcomeScreenSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor) { }

	void Release() override final;

	void Initialize() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	void RenderImGui() override final;

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

protected:
	RHIVertexBuffer mTriangleVertexBuffer;
};
