#include "StdafxSamples.h"
#include "RenderToTextureSample.h"

#include <Application.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>

using namespace GraphicsCore;

/**
 * Function : Release
 */
void RenderToTextureSample::Release()
{
	Sample::Release();

	mVertexBuffer.Release();

	RHISafeRelease(mRTTexture);
	RHISafeRelease(mCopyTexture);

	gRenderer->CbvSrvUavHeapManager().FreeDescriptors(mCopyTextureDescriptor);
	gRenderer->CbvSrvUavHeapManager().FreeDescriptors(mRenderTextureDescriptor);
	gRenderer->RtvHeapManager().FreeDescriptors(mRtvDescriptors);
}

/**
 * Function : Initialize
 */
void RenderToTextureSample::Initialize()
{
	Sample::Initialize();

	mCamera->SetPosition(DirectX::XMFLOAT4(110.0f, 0.0f, 30.0f, 1.0f));
}

/**
 * Function : InitializeRenderingResources
 */
void RenderToTextureSample::InitializeRenderingResources()
{
	// Create a vertex buffer.
	{
		using Vertex = InputLayouts::RenderToTexture::Vertex;
		Vertex vertices[] =
		{
			{ DirectX::XMFLOAT3(-900.0f, 0.0f, 0.0f) },
			{ DirectX::XMFLOAT3(50.0f, -25.0f, 0.0f) },
			{ DirectX::XMFLOAT3(50.0f, 25.0f, 0.0f) }
		};

		mVertexBuffer.Initialize(__TEXT("RenderToTexture:VertexBuffer"), sizeof(vertices), _countof(vertices), sizeof(Vertex), vertices);
	}

	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, true);

	// Create textures.
	{
		RHIResourceDesc desc	= {};
		desc.Dimension			= RHIResourceDimension::Texture2D;
		desc.Format				= gRenderer->RenderTargetFormat();
		desc.MipLevels			= 1;
		desc.Width				= mApp->GetWindow()->Width();
		desc.Height				= mApp->GetWindow()->Height();
		desc.Flags				= RHIResourceFlag::None;
		desc.DepthOrArraySize	= 1;
		desc.SampleDesc.Count	= 1;
		desc.SampleDesc.Quality	= 0;

		mCopyTexture = gRenderer->Device()->CreateCommittedResource(
			&RHIHeapProperties(RHIHeapType::Default),
			RHIHeapFlag::None,
			&desc,
			RHIResourceState::GenericRead,
			nullptr);

		desc.Flags = RHIResourceFlag::AllowRenderTarget;
		RHIClearValue defaultClearValue;
		defaultClearValue.Format = desc.Format;
		defaultClearValue.Color[0] = mBGColor.x;
		defaultClearValue.Color[1] = mBGColor.y;
		defaultClearValue.Color[2] = mBGColor.z;
		defaultClearValue.Color[3] = mBGColor.w;
		mRTTexture = gRenderer->Device()->CreateCommittedResource(
			&RHIHeapProperties(RHIHeapType::Default),
			RHIHeapFlag::None,
			&desc,
			RHIResourceState::RenderTarget,
			&defaultClearValue);

		// Describe and create a SRV for the texture.
		RHIShaderResourceViewDesc srvDesc	= {};
		srvDesc.Shader4ComponentMapping		= D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.Format						= gRenderer->RenderTargetFormat();
		srvDesc.ViewDimension				= RHISRVDimension::Texture2D;
		srvDesc.Texture2D.MipLevels			= 1;

		mCopyTextureDescriptor = gRenderer->CbvSrvUavHeapManager().GetNewDescriptors(1);

		mGpuHandleCopyTexture = gRenderer->CbvSrvUavHeapManager().GpuHandle(mCopyTextureDescriptor[0]);
		gRenderer->Device()->CreateShaderResourceView(mCopyTexture, &srvDesc, gRenderer->CbvSrvUavHeapManager().CpuHandle(mCopyTextureDescriptor[0]));

		mRtvDescriptors			= gRenderer->RtvHeapManager().GetNewDescriptors(1);
		mCpuHandleRtvRTTexture	= gRenderer->RtvHeapManager().CpuHandle(mRtvDescriptors[0]);
		mGpuHandleRtvRTTexture	= gRenderer->RtvHeapManager().GpuHandle(mRtvDescriptors[0]);
		gRenderer->Device()->CreateRenderTargetView(mRTTexture, nullptr, mCpuHandleRtvRTTexture);

		mRenderTextureDescriptor	= gRenderer->CbvSrvUavHeapManager().GetNewDescriptors(1);
		mCpuHandleSrvRTTexture		= gRenderer->CbvSrvUavHeapManager().CpuHandle(mRenderTextureDescriptor[0]);
		mGpuHandleSrvRTTexture		= gRenderer->CbvSrvUavHeapManager().GpuHandle(mRenderTextureDescriptor[0]);
		gRenderer->Device()->CreateShaderResourceView(mRTTexture, nullptr, mCpuHandleSrvRTTexture);

		gRenderer->CommandList()->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mRTTexture, RHIResourceState::RenderTarget, RHIResourceState::PixelShaderResource));
	}
}

/**
 * Function : CreateRootSignatures
 */
void RenderToTextureSample::CreateRootSignatures()
{
	RHIDescriptorRange ranges[2];
	ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);
	ranges[1].Init(RHIDescriptorRangeType::CBV, 1, 1);

	RHIRootParameter rootParameters[2];
	rootParameters[0].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::Vertex);
	rootParameters[1].InitAsDescriptorTable(1, &ranges[1], RHIShaderVisibility::Vertex);

	RHIRootSignatureDesc rootSignatureDesc;
	rootSignatureDesc.Init(_countof(rootParameters), &rootParameters[0], 0, nullptr, RHIRootSignatureFlags::AllowInputAssemblerInputLayout);

	mRootSignature = gRenderer->SerializeAndCreateRootSignature(&rootSignatureDesc, RHIRootSignatureVersion::Ver1);
}

/**
 * Function : CreatePipelineStates
 */
void RenderToTextureSample::CreatePipelineStates()
{
	RHIBlobWrapper vertexShader	(gRenderer->LoadShader(__TEXT("RenderToTexture:DepthPass.vs"), RHIShaderType::Vertex));
	RHIBlobWrapper pixelShader	(gRenderer->LoadShader(__TEXT("RenderToTexture:DepthPass.ps"), RHIShaderType::Pixel));

	if (vertexShader.Blob->IsNull() || pixelShader.Blob->IsNull())
		return;

	RHIGraphicsPipelineStateDesc psoDesc;
	ZeroMemory(&psoDesc, sizeof(RHIGraphicsPipelineStateDesc));

	psoDesc.RootSignature									= mRootSignature;
	psoDesc.InputLayout										= { InputLayouts::RenderToTexture::inputElementDesc, _countof(InputLayouts::RenderToTexture::inputElementDesc) };
	
	psoDesc.VS												= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
	psoDesc.PS												= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
	psoDesc.PrimitiveTopologyType							= RHIPrimitiveTopologyType::Triangle;
	psoDesc.SampleMask										= UINT_MAX;
	psoDesc.NumRenderTargets								= 1;
	psoDesc.RTVFormats[0]									= gRenderer->RenderTargetFormat();
	psoDesc.DSVFormat										= gRenderer->DepthStencilViewFormat();
	psoDesc.SampleDesc.Count								= 1;

	psoDesc.DepthStencilState.DepthEnable					= TRUE;
	psoDesc.DepthStencilState.DepthFunc						= RHIDepthFunc::LEqual;
	psoDesc.DepthStencilState.DepthWriteMask				= RHIDepthWriteMask::On;
	psoDesc.DepthStencilState.StencilEnable					= TRUE;
	psoDesc.DepthStencilState.StencilReadMask				= 0;
	psoDesc.DepthStencilState.StencilWriteMask				= 0xff;
	psoDesc.DepthStencilState.FrontFace.StencilFailOp		= RHIStencilOp::Zero;
	psoDesc.DepthStencilState.FrontFace.StencilDepthFailOp	= RHIStencilOp::Zero;
	psoDesc.DepthStencilState.FrontFace.StencilPassOp		= RHIStencilOp::Zero;
	psoDesc.DepthStencilState.FrontFace.StencilFunc			= RHIComparisonFunc::Always;
	psoDesc.DepthStencilState.BackFace.StencilFailOp		= RHIStencilOp::Zero;
	psoDesc.DepthStencilState.BackFace.StencilDepthFailOp	= RHIStencilOp::Zero;
	psoDesc.DepthStencilState.BackFace.StencilPassOp		= RHIStencilOp::Zero;
	psoDesc.DepthStencilState.BackFace.StencilFunc			= RHIComparisonFunc::Always;

	psoDesc.RasterizerState									= RHIRasterizerDesc(RHIDefault());
	psoDesc.RasterizerState.FrontCounterClockwise			= FALSE;
	psoDesc.RasterizerState.FillMode						= RHIFillMode::Solid;
	psoDesc.RasterizerState.CullMode						= RHICullMode::None;
	psoDesc.RasterizerState.DepthBias						= 1000;
	psoDesc.RasterizerState.DepthBiasClamp					= 1.0f;
	psoDesc.RasterizerState.SlopeScaledDepthBias			= 1.0f;
	psoDesc.RasterizerState.DepthClipEnable					= TRUE;
	psoDesc.RasterizerState.MultisampleEnable				= FALSE;
	psoDesc.RasterizerState.AntialiasedLineEnable			= FALSE;
	psoDesc.RasterizerState.ForcedSampleCount				= 0;
	psoDesc.RasterizerState.ConservativeRaster				= RHIConservativeRasterizationMode::Off;

	psoDesc.BlendState										= RHIBlendDesc(RHIDefault());
	psoDesc.BlendState.RenderTarget[0].BlendEnable			= true;
	psoDesc.BlendState.RenderTarget[0].BlendOp				= RHIBlendOp::Add;
	psoDesc.BlendState.RenderTarget[0].BlendOpAlpha			= RHIBlendOp::Add;
	psoDesc.BlendState.RenderTarget[0].SrcBlend				= RHIBlend::SrcAlpha;
	psoDesc.BlendState.RenderTarget[0].DestBlend			= RHIBlend::InvSrcAlpha;

	mPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
}

/**
 * Function : CopyTextureToTexture
 */
void RenderToTextureSample::CopyTextureToTexture(RHICommandList* commandList, RHIResource* src, RHIResource* dst)
{
	RHIResourceDesc srcDesc = src->GetDesc();
	RHIResourceDesc dstDesc = dst->GetDesc();

	if (srcDesc.Width != dstDesc.Width || srcDesc.Height != dstDesc.Height || srcDesc.DepthOrArraySize != dstDesc.DepthOrArraySize)
	{
		return;
	}

	commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(src, RHIResourceState::Present, RHIResourceState::CopySource));
	commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(dst, RHIResourceState::GenericRead, RHIResourceState::CopyDest));
	commandList->CopyResource(dst, src);
	commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(dst, RHIResourceState::CopyDest, RHIResourceState::GenericRead));
	commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(src, RHIResourceState::CopySource, RHIResourceState::Present));
}

/**
 * Function : RenderImGui
 */
void RenderToTextureSample::RenderImGui()
{
	ImGui::Text("A simple example shows how to render to texture.");

	ImGui::RadioButton("Copy render target to another texture", reinterpret_cast<int*> (&mType), ToInt(Type::CopyRenderTargetToTexture));
	ImGui::RadioButton("Use texture as render target", reinterpret_cast<int*> (&mType), ToInt(Type::UseTextureAsRenderTarget));

	if (ImGui::CollapsingHeader("Debug settings", 0, true, true))
	{
		ImGui::Checkbox("Show texture", &mUseScreenQuad);
		if (mUseScreenQuad)
			ImGui::Checkbox("Show render targer texture", &mIsShowRTTexture);
	}

	RHIGPUDescriptorHandle& texHandle	= mIsShowRTTexture
											? ((mType == Type::CopyRenderTargetToTexture) ? mGpuHandleCopyTexture : mGpuHandleSrvRTTexture)
											: gRenderer->CurrentDepthStencilGpuHandle() /*RHIGPUDescriptorHandle(mGpuHandleDSTextures, gRenderer->FrameIndex(), gRenderer->Device()->GetDescriptorHandleIncrementSize(RHIDescriptorHeapType::CBV_SRV_UAV))*/;
	mScreenQuad->SetTexture(texHandle, ScreenQuadPart::TextureType::ColorRGBA);
}

/**
 * Function : Update
 */
void RenderToTextureSample::Update(const float deltaTime)
{
	UpdateCameraResource();
}

/**
 * Function : Render
 */
void RenderToTextureSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };
	{
		//gRenderer->StartRendering();
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);

		RHIResource* renderTarget = (mType == Type::CopyRenderTargetToTexture) ? gRenderer->CurrentRenderTarget() : mRTTexture;
		RHICPUDescriptorHandle rtHandle = (mType == Type::CopyRenderTargetToTexture) ? gRenderer->CurrentRenderTargetCpuHandle() : mCpuHandleRtvRTTexture;

		RHIResourceBarrier startBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(renderTarget, (mType == Type::CopyRenderTargetToTexture) ? RHIResourceState::Present : RHIResourceState::PixelShaderResource, RHIResourceState::RenderTarget),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::DepthWrite)
		};

		RHIResourceBarrier stopBarriers[] =
		{
			RHIResourceBarrier::MakeTransition(renderTarget, RHIResourceState::RenderTarget, (mType == Type::CopyRenderTargetToTexture) ? RHIResourceState::Present : RHIResourceState::PixelShaderResource),
			RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::DepthWrite, RHIResourceState::Present)
		};

		commandList->ResourceBarrier(_countof(startBarriers), startBarriers);
		commandList->OMSetRenderTargets(1, &rtHandle, false, &gRenderer->CurrentDepthStencilCpuHandle());
		commandList->ClearRenderTargetView(rtHandle, &mBGColor.x, 0, nullptr);
		//gRenderer->ClearRTV(glm::value_ptr(mBGColor));
		gRenderer->ClearDSV();

		commandList->SetPipelineState(mPSO);
		commandList->SetGraphicsRootSignature(mRootSignature);
		commandList->SetDescriptorHeaps(_countof(heaps), heaps);
		commandList->SetGraphicsRootDescriptorTable(0, mConstBufferPerFrame.SrvHandle().Gpu());
		commandList->SetGraphicsRootDescriptorTable(1, mConstBufferPerObject.SrvHandle().Gpu());

		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);

		commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
		commandList->IASetVertexBuffers(0, 1, mVertexBuffer.View());
		commandList->DrawInstanced(mVertexBuffer.NumElements(), 1, 0, 0);

		commandList->ResourceBarrier(_countof(stopBarriers), stopBarriers);
		gRenderer->CommitCommandList(commandList);
		//gRenderer->StopRendering();

		if (mType == Type::CopyRenderTargetToTexture)
		{
			commandList->Reset(gRenderer->CommandAllocator(), nullptr);
			RHIResource* src = gRenderer->CurrentRenderTarget();
			RHIResource* dst = mCopyTexture;
			CopyTextureToTexture(commandList, src, dst);
			gRenderer->CommitCommandList(commandList);
		}
	}

	if (mUseScreenQuad)
	{
		gRenderer->CommandAllocator()->Reset();
		commandList->Reset(gRenderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));
		commandList->OMSetRenderTargets(1, &gRenderer->CurrentRenderTargetCpuHandle(), false, nullptr);

		if (!mIsShowRTTexture)
		{
			commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::Present, RHIResourceState::PixelShaderResource));
		}

		DrawScreenQuad(commandList, heaps, _countof(heaps));

		if (!mIsShowRTTexture)
		{
			commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentDepthStencil(), RHIResourceState::PixelShaderResource, RHIResourceState::Present));
		}

		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(gRenderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
		gRenderer->CommitCommandList(commandList);
	}
}
