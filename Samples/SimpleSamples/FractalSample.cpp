#include "StdafxSamples.h"
#include "FractalSample.h"
#include <Application.h>
#include <InputLayouts.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHI.h>
#include <RHIUtils.h>

using namespace GraphicsCore;

namespace
{
	static const char* kNamedType[] =
	{
		"Mandelbrot 2D",
		"Burning ship 2D",
	};
}

/**
 * Function : Release
 */
void FractalSample::Release()
{
	Sample::Release();

	mQuadVertexBuffer.Release();
	mMandelbrot2dConstBuffer.Release();
}

/**
 * Function : Initialize
 */
void FractalSample::Initialize()
{
	Sample::Initialize();

	mDisplaySettings[FractalType::Mandelbrot2d]		= std::bind(&FractalSample::DisplayMandelbrot2dSettings, this);
	mDisplaySettings[FractalType::BurningShip2d]	= std::bind(&FractalSample::DisplayMandelbrot2dSettings, this);
	mType											= FractalType::Mandelbrot2d;
	mMandelbrot2dConstBuffer.Data().Type			= ToInt(mType);
}

/**
 * Function : InitializeRenderingResource
 */
void FractalSample::InitializeRenderingResources()
{
	using QuadVertex = InputLayouts::ScreenQuadWithoutMatrix::Vertex;

	// Create a vertex buffer.
	{
		const QuadVertex vertices[] =
		{
			{ DirectX::XMFLOAT2(-1.0f, 1.0f) },
			{ DirectX::XMFLOAT2(-1.0f, -1.0f) },
			{ DirectX::XMFLOAT2(1.0f, -1.0f) },

			{ DirectX::XMFLOAT2(-1.0f, 1.0f) },
			{ DirectX::XMFLOAT2(1.0f, -1.0f) },
			{ DirectX::XMFLOAT2(1.0f, 1.0f) }
		};

		mQuadVertexBuffer.Initialize(__TEXT("Fractal:QuadVB"), sizeof(vertices), _countof(vertices), sizeof(QuadVertex), vertices);
	}

	// Create a constant buffer & a constant buffer view.
	{
		const UINT64 size = 1024 * 4;
		mMandelbrot2dConstBuffer.Initialize(__TEXT("Fractal:CBMandelbrot"), size, 1, sizeof(mMandelbrot2dConstBuffer.Data()), &mMandelbrot2dConstBuffer.Data());
	}
}

/**
 * Function : CreateRootSignatures
 * TODO: move constant buffer in the root signature as constants.
 */
void FractalSample::CreateRootSignatures()
{
	RHIDescriptorRange ranges[1];
	ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);

	RHIRootParameter rootParameters[1];
	rootParameters[0].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::All);

	RHIRootSignatureDesc rootSignatureDesc;
	RHIRootSignatureFlags flags = GetRootSignatureFlags(true, true, false, false, false, true, false);

	rootSignatureDesc.Init(_countof(rootParameters), &rootParameters[0], 0, nullptr, flags);

	mRootSignature = gRenderer->SerializeAndCreateRootSignature(&rootSignatureDesc, RHIRootSignatureVersion::Ver1);
}

/**
 * Function : CreatePipelineStates
 */
void FractalSample::CreatePipelineStates()
{
	RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("Fractals:MandelbrotFractal.vs"), RHIShaderType::Vertex));
	RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("Fractals:MandelbrotFractal.ps"), RHIShaderType::Pixel));

	if (vertexShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
		return ;

	RHIGraphicsPipelineStateDesc psoDesc				= {};
	psoDesc.RootSignature								= mRootSignature;
	psoDesc.InputLayout									= { InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc, _countof(InputLayouts::ScreenQuadWithoutMatrix::inputElementDesc) };
	psoDesc.VS											= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
	psoDesc.PS											= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
	psoDesc.PrimitiveTopologyType						= RHIPrimitiveTopologyType::Triangle;
	psoDesc.SampleMask									= UINT_MAX;
	psoDesc.NumRenderTargets							= 1;
	psoDesc.RTVFormats[0]								= gRenderer->RenderTargetFormat();
	//psoDesc.DSVFormat									= mDepthStencilViewFormat;
	psoDesc.SampleDesc.Count							= 1;

	psoDesc.DepthStencilState.DepthEnable				= false;

	psoDesc.RasterizerState								= RHIRasterizerDesc(RHIDefault());
	psoDesc.RasterizerState.CullMode					= RHICullMode::Front;
	psoDesc.RasterizerState.FrontCounterClockwise		= FALSE;

	psoDesc.BlendState									= RHIBlendDesc(RHIDefault());
	psoDesc.BlendState.RenderTarget[0].BlendEnable		= TRUE;
	psoDesc.BlendState.RenderTarget[0].SrcBlend			= RHIBlend::SrcAlpha;
	psoDesc.BlendState.RenderTarget[0].DestBlend		= RHIBlend::InvSrcAlpha;
	psoDesc.BlendState.RenderTarget[0].SrcBlendAlpha	= RHIBlend::SrcAlpha;
	psoDesc.BlendState.RenderTarget[0].DestBlendAlpha	= RHIBlend::InvSrcAlpha;

	mPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
}

/**
 * Function : RenderImGui
 */
void FractalSample::RenderImGui()
{
	ImGui::Combo("Fractal", reinterpret_cast<int*> (&mType), kNamedType, _countof(kNamedType));

	mDisplaySettings[mType]();
}

/**
 * Function : DisplayMandelbrot2dSettings
 */
void FractalSample::DisplayMandelbrot2dSettings()
{
	ImGui::SliderFloat2("View pos", &mMandelbrot2dConstBuffer.Data().ViewPos.x, -5.0f, 5.0f);
	ImGui::SliderFloat("View zoom", &mMandelbrot2dConstBuffer.Data().ViewZoom, 0.1f, 30.0f);
	ImGui::InputFloat("Ratio", &mMandelbrot2dConstBuffer.Data().ViewRatio);

	{
		static float k = (float)mMandelbrot2dConstBuffer.Data().Iterations;
		static float delta = 0.5f;

		const float min = 10.0f;
		const float max = 70.0f;
		k += delta;

		if (k >= max || k <= min)
			delta *= -1.0f;

		mMandelbrot2dConstBuffer.Data().Iterations = static_cast<int>(k);

		ImGui::SliderInt("Iterations", &mMandelbrot2dConstBuffer.Data().Iterations, (int)min, (int)max);
	}
}

/**
 * Function : DisplayJulia2dSettings
 */
void FractalSample::DisplayJulia2dSettings()
{

}

/**
 * Function : Update
 */
void FractalSample::Update(const float deltaTime)
{
	mMandelbrot2dConstBuffer.Data().Type = ToInt(mType);
	switch (mType)
	{
		case FractalType::Mandelbrot2d:
		case FractalType::BurningShip2d:
		{
			mMandelbrot2dConstBuffer.UpdateDataOnGPU();
			break;
		}
	}
}

/**
 * Function : Render
 */
void FractalSample::Render(RHICommandList* commandList)
{
	switch (mType)
	{
		case FractalType::Mandelbrot2d:
		case FractalType::BurningShip2d:
		{
			RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };

			commandList->SetPipelineState(mPSO);
			commandList->SetGraphicsRootSignature(mRootSignature);
			commandList->SetDescriptorHeaps(_countof(heaps), heaps);
			commandList->SetGraphicsRootDescriptorTable(0, mMandelbrot2dConstBuffer.SrvHandle().Gpu());
			commandList->RSSetViewports(1, &mViewport);
			commandList->RSSetScissorRects(1, &mScissorRect);
			commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
			commandList->IASetVertexBuffers(0, 1, mQuadVertexBuffer.View());
			commandList->DrawInstanced(6, 1, 0, 0);
			break;
		}
	}
}
