#include "StdafxSamples.h"
#include "StreamOutputSample.h"

#include <Application.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>

using namespace GraphicsCore;

/**
 * Function : Release
 */
void StreamOutputSample::Release()
{
	Sample::Release();

	RHISafeRelease(mUpdatePSO);
	RHISafeRelease(mUpdateRootSignature);
	RHISafeRelease(mRenderPSO);
	RHISafeRelease(mRenderRootSignature);

	RHISafeRelease(mSOBuffer[0]);
	RHISafeRelease(mSOBuffer[1]);
	RHISafeRelease(mVertexBuffer[0]);
	RHISafeRelease(mVertexBuffer[1]);
	RHISafeRelease(mReadbackBuffer);

	RHISafeRelease(mConstBufferGeneratorSettings);
}

/**
 * Function : Initialize
 */
void StreamOutputSample::Initialize()
{
	mConstBufferGeneratorSettingsData.Position		= glm::vec4(0.0f, 0.0f, 2.0f, 0.0f);
	mConstBufferGeneratorSettingsData.MinVelocity	= glm::vec4(-5.0f, -5.0f, 0.0f, 0.0f);
	mConstBufferGeneratorSettingsData.MaxVelocity	= glm::vec4(5.0f, 5.0f, 10.0f, 0.0f);
	mConstBufferGeneratorSettingsData.GravityVector	= glm::vec4(0.0f, 0.0f, -5.0f, 0.0f);
	mConstBufferGeneratorSettingsData.Color			= glm::vec4(1.2f, 1.4f, 0.8f, 0.0f);
	mConstBufferGeneratorSettingsData.Settings		= glm::vec4(1.5f, 3.0f, 0.3f, 0.0f);
	mConstBufferGeneratorSettingsData.Count			= 1;

	Sample::Initialize();

	mGenerateParticles		= 1;
	mGenerateEveryNSeconds	= 1.5f;
}

/**
 * Function : InitializeRenderingResources
 */
void StreamOutputSample::InitializeRenderingResources()
{
	/*UINT renderCbvDescriptor		= gRenderer->CbvHeapManager().GetNewDescriptors(2);
	UINT updateCbvDescriptor		= gRenderer->CbvHeapManager().GetNewDescriptors(1);
	mUpdateGpuHandleRootParameter0	= gRenderer->CbvHeapManager().GpuHandle(updateCbvDescriptor);

	// Create a stream output buffer.
	{
		const UINT64 size = sizeof(Vertex) * mMaxParticles;

		mCurrentNbOfParticles = 1;
		Vertex initParticle;
		initParticle.Position	= glm::vec3(mConstBufferGeneratorSettingsData.Position);
		initParticle.Velocity	= glm::vec3(0.0f, 0.0f, 0.0001f);
		initParticle.Settings	= glm::vec2(0.0f, 0.3f);
		initParticle.Color		= glm::vec3(mConstBufferGeneratorSettingsData.Color);
		initParticle.Type		= 0;

		for (UINT i = 0; i < 2; ++i)
		{
			mVertexBuffer[i] = gRenderer->Device()->CreateCommittedResource(
				&RHIHeapProperties(RHIHeapType::Upload),
				RHIHeapFlags::None,
				&RHIResourceDesc::Buffer(size),
				RHIResourceState::GenericRead,
				nullptr);
			mVertexBuffer[i]->InitializeSubData(0, &initParticle, sizeof(Vertex));

			mVertexBufferView[i].BufferLocation	= mVertexBuffer[i]->GetGPUVirtualAddress();
			mVertexBufferView[i].SizeInBytes	= sizeof(Vertex);
			mVertexBufferView[i].StrideInBytes	= sizeof(Vertex);


			mSOBuffer[i] = gRenderer->Device()->CreateCommittedResource(
				&RHIHeapProperties(RHIHeapType::Default),
				RHIHeapFlags::None,
				&RHIResourceDesc::Buffer(size + sizeof(UINT64)),
				RHIResourceState::StreamOut,
				nullptr);

			mSOBufferView[i].BufferFilledSizeLocation	= mSOBuffer[i]->GetGPUVirtualAddress();
			mSOBufferView[i].BufferLocation				= mSOBufferView[i].BufferFilledSizeLocation + sizeof(UINT64);
			mSOBufferView[i].SizeInBytes				= size;

			mSOVertexBufferView[i].BufferLocation		= mSOBufferView[i].BufferLocation;
			mSOVertexBufferView[i].SizeInBytes			= size;
			mSOVertexBufferView[i].StrideInBytes		= sizeof(Vertex);
		}
	}

	// Create a readback buffer.
	{
		const UINT64 size = sizeof(Vertex) * mMaxParticles + sizeof(UINT64);//AlignTo256Bytes(sizeof(UINT64));
		mReadbackBuffer = gRenderer->Device()->CreateCommittedResource(
			&RHIHeapProperties(RHIHeapType::Readback),
			RHIHeapFlags::None,
			&RHIResourceDesc::Buffer(size),
			RHIResourceState::CopyDest,
			nullptr);
	}

	// Create constant buffers.
	{
		const UINT64 cbSize = 1024 * 4;
		// Create a constant buffer of generate settings.
		{
			mConstBufferGeneratorSettings = gRenderer->Device()->CreateCommittedResource(
				&RHIHeapProperties(RHIHeapType::Upload),
				RHIHeapFlags::None,
				&RHIResourceDesc::Buffer(cbSize),
				RHIResourceState::GenericRead,
				nullptr);
			mConstBufferGeneratorSettings->InitializeSubData(0, &mConstBufferGeneratorSettingsData, sizeof(ParticleGeneratorSettings));

			RHIConstantBufferViewDesc cbvDesc	= {};
			cbvDesc.BufferLocation				= mConstBufferGeneratorSettings->GetGPUVirtualAddress();
			cbvDesc.SizeInBytes					= AlignTo256Bytes(sizeof(ParticleGeneratorSettings));

			gRenderer->Device()->CreateConstantBufferView(&cbvDesc, gRenderer->CbvHeapManager().CpuHandle(updateCbvDescriptor));
		}


		CreateConstantBuffersForMatrix(gRenderer->CbvHeapManager().Heap(), renderCbvDescriptor, -1);

		// Create a constant buffer for camera.
		{
			mConstBufferForCamera = gRenderer->Device()->CreateCommittedResource(
				&RHIHeapProperties(RHIHeapType::Upload),
				RHIHeapFlags::None,
				&RHIResourceDesc::Buffer(cbSize),
				RHIResourceState::GenericRead,
				nullptr);

			RHIConstantBufferViewDesc cbvDesc	= {};
			cbvDesc.BufferLocation				= mConstBufferForCamera->GetGPUVirtualAddress();
			cbvDesc.SizeInBytes					= AlignTo256Bytes(sizeof(ConstantBuffers::Camera));

			gRenderer->Device()->CreateConstantBufferView(&cbvDesc, gRenderer->CbvHeapManager().CpuHandle(renderCbvDescriptor + 1));
		}
	}*/

	mCurrentVB = 0;
	mCurrentSOB = 1;
}

/**
 * Function : CreateRootSignatures
 */
void StreamOutputSample::CreateRootSignatures()
{
	{
		RHIDescriptorRange ranges[1];
		ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);

		RHIRootParameter rootParameters[1];
		rootParameters[0].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::Geometry);

		RHIRootSignatureDesc	desc = {};
		RHIRootSignatureFlags	flags = GetRootSignatureFlags(true, true, false, false, false, false, true);

		desc.Init(1, &rootParameters[0], 0, nullptr, flags);

		mUpdateRootSignature = gRenderer->SerializeAndCreateRootSignature(&desc, RHIRootSignatureVersion::Ver1);
	}

	{
		RHIDescriptorRange ranges[1];
		ranges[0].Init(RHIDescriptorRangeType::CBV, 2, 0);

		RHIRootParameter rootParameters[1];
		rootParameters[0].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::Geometry);

		RHIRootSignatureDesc desc;
		desc.Init(1, &rootParameters[0], 0, nullptr, RHIRootSignatureFlags::AllowInputAssemblerInputLayout);

		mRenderRootSignature =  gRenderer->SerializeAndCreateRootSignature(&desc, RHIRootSignatureVersion::Ver1);
	}
}

/**
 * Function : CreatePipelineStates
 */
void StreamOutputSample::CreatePipelineStates()
{
	{
		RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("StreamOutput:ParticlesUpdate.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper geometryShader	(gRenderer->LoadShader(__TEXT("StreamOutput:ParticlesUpdate.gs"), RHIShaderType::Geometry));
		RHIBlobWrapper pixelShader		(gRenderer->LoadShader(__TEXT("StreamOutput:ParticlesUpdate.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || geometryShader.Blob->IsNull() /*|| pixelShader.Blob->IsNull()*/)
			return;

		RHIStreamOutputDeclarationEntry soEntries[] =
		{
			{ 0, "POSITION",	0, 0, 3, 0 },
			{ 0, "VELOCITY",	0, 0, 3, 0 },
			{ 0, "COLOR",		0, 0, 3, 0 },
			{ 0, "SETTINGS",	0, 0, 2, 0 },
			{ 0, "TYPE",		0, 0, 1, 0 }
		};

		const UINT kMaxOutputStreams = 4;
		UINT strides[kMaxOutputStreams];
		memset(strides, 0, sizeof(strides));
		strides[0] = sizeof(float) * (3 + 3 + 3 + 2) + sizeof(int);

		RHIGraphicsPipelineStateDesc psoDesc			= {};
		psoDesc.RootSignature							= mUpdateRootSignature;
		psoDesc.InputLayout								= { InputLayouts::StreamOutput::inputElementDesc, _countof(InputLayouts::StreamOutput::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.GS										= { geometryShader.GetBufferPointer(), geometryShader.GetBufferSize() };
		psoDesc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Point;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.DSVFormat								= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= FALSE;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable			= FALSE;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;
		psoDesc.RasterizerState.FrontCounterClockwise	= FALSE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp		= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha	= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend		= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend	= RHIBlend::InvSrcAlpha;

		psoDesc.StreamOutput.SODeclaration				= soEntries;
		psoDesc.StreamOutput.NumElements				= _countof(soEntries);
		psoDesc.StreamOutput.BufferStrides				= strides;
		psoDesc.StreamOutput.NumStrides					= kMaxOutputStreams;
		psoDesc.StreamOutput.RasterizedStream			= 0;

		mUpdatePSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}

	{
		RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("StreamOutput:ParticlesRender.vs"), RHIShaderType::Vertex));
		RHIBlobWrapper geometryShader	(gRenderer->LoadShader(__TEXT("StreamOutput:ParticlesRender.gs"), RHIShaderType::Geometry));
		RHIBlobWrapper pixelShader		(gRenderer->LoadShader(__TEXT("StreamOutput:ParticlesRender.ps"), RHIShaderType::Pixel));

		if (vertexShader.Blob->IsNull() || geometryShader.Blob->IsNull() || pixelShader.Blob->IsNull())
			return;

		RHIGraphicsPipelineStateDesc psoDesc			= {};
		psoDesc.RootSignature							= mRenderRootSignature;
		psoDesc.InputLayout								= { InputLayouts::StreamOutput::inputElementDesc, _countof(InputLayouts::StreamOutput::inputElementDesc) };
		psoDesc.VS										= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
		psoDesc.GS										= { geometryShader.GetBufferPointer(), geometryShader.GetBufferSize() };
		psoDesc.PS										= { pixelShader.GetBufferPointer(), pixelShader.GetBufferSize() };
		psoDesc.PrimitiveTopologyType					= RHIPrimitiveTopologyType::Point;
		psoDesc.SampleMask								= UINT_MAX;
		psoDesc.NumRenderTargets						= 1;
		psoDesc.RTVFormats[0]							= gRenderer->RenderTargetFormat();
		psoDesc.DSVFormat								= gRenderer->DepthStencilViewFormat();
		psoDesc.SampleDesc.Count						= 1;

		psoDesc.DepthStencilState.DepthEnable			= FALSE;
		psoDesc.DepthStencilState.DepthFunc				= RHIDepthFunc::LEqual;
		psoDesc.DepthStencilState.DepthWriteMask		= RHIDepthWriteMask::On;
		psoDesc.DepthStencilState.StencilEnable			= FALSE;

		psoDesc.RasterizerState							= RHIRasterizerDesc(RHIDefault());
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;
		psoDesc.RasterizerState.FrontCounterClockwise	= FALSE;
		psoDesc.RasterizerState.FillMode				= RHIFillMode::Solid;
		psoDesc.RasterizerState.CullMode				= RHICullMode::None;

		psoDesc.BlendState								= RHIBlendDesc(RHIDefault());
		psoDesc.BlendState.RenderTarget[0].BlendEnable	= true;
		psoDesc.BlendState.RenderTarget[0].BlendOp		= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].BlendOpAlpha	= RHIBlendOp::Add;
		psoDesc.BlendState.RenderTarget[0].SrcBlend		= RHIBlend::SrcAlpha;
		psoDesc.BlendState.RenderTarget[0].DestBlend	= RHIBlend::InvSrcAlpha;

		mRenderPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
	}
}

/**
 * Function : RenderImGui
 */
void StreamOutputSample::RenderImGui()
{
	ImGui::Text("Number of particles: %u", mCurrentNbOfParticles);
}

/**
 * Function : Update
 */
void StreamOutputSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	// Update generator settings.
	{
		mConstBufferGeneratorSettingsData.Settings.w	= deltaTime;
		mConstBufferGeneratorSettingsData.Count			= 0;

		mTime += deltaTime;
		if (mTime >= mGenerateEveryNSeconds)
		{
			mConstBufferGeneratorSettingsData.Count		= mGenerateParticles;
			mTime -= mGenerateEveryNSeconds;
		}

		mConstBufferGeneratorSettings->InitializeSubData(0, &mConstBufferGeneratorSettingsData, sizeof(ParticleGeneratorSettings));
	}
}

/**
 * Function : Render
 */
void StreamOutputSample::Render(RHICommandList* commandList)
{
	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };
	// Update particles.
	commandList->SetPipelineState(mUpdatePSO);
	commandList->SetGraphicsRootSignature(mUpdateRootSignature);
	commandList->SetDescriptorHeaps(_countof(heaps), heaps);
	commandList->SetGraphicsRootDescriptorTable(0, mUpdateGpuHandleRootParameter0);

	commandList->RSSetViewports(1, &mViewport);
	commandList->RSSetScissorRects(1, &mScissorRect);

	commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::PointList);
	commandList->IASetVertexBuffers(0, 1, &mVertexBufferView[0]);
	commandList->SOSetTargets(0, 1, &mSOBufferView[0]);

	commandList->DrawInstanced(1, 1, 0, 0);

	gRenderer->CloseAndWait(commandList);

	// Get a number of particles.
	commandList->Reset(gRenderer->CommandAllocator(), nullptr);

	UINT64 dstOffset = 0;
	UINT64 srcOffset = 0;

	commandList->CopyBufferRegion(mReadbackBuffer, dstOffset, mSOBuffer[0], srcOffset, sizeof(UINT64));
	gRenderer->CloseAndWait(commandList);

	RHIRange range(0, 0);
	UINT64* value;
	mReadbackBuffer->Map(0, &range, reinterpret_cast<void**>(&value));
	mCurrentNbOfParticles = (UINT)(*value / mSOVertexBufferView[0].StrideInBytes);
	mReadbackBuffer->Unmap(0, &range);

	// Get particles.
	commandList->Reset(gRenderer->CommandAllocator(), nullptr);
	dstOffset = 0;
	srcOffset = sizeof(UINT64);
	commandList->CopyBufferRegion(mReadbackBuffer, dstOffset, mSOBuffer[0], srcOffset, sizeof(Vertex) * mCurrentNbOfParticles);
	gRenderer->CloseAndWait(commandList);

	Vertex* particles = 0;
	mReadbackBuffer->Map(0, &range, reinterpret_cast<void**>(&particles));

	for (UINT i = 0; i < mCurrentNbOfParticles; ++i)
	{
		Vertex& particle = particles[i];
		const int a = 0;
	}

	mReadbackBuffer->Unmap(0, &range);


	/*commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mSOBuffer[0], RHIResourceState::StreamOut, RHIResourceState::GenericRead));
	Vertex* test;
	mSOBuffer[0]->Map(0, &range, reinterpret_cast<void**>(&test));
	for (int i = 0; i < mCurrentNbOfParticles; ++i)
	{
		Vertex& particle = test[i];
		const int a = 0;
	}
	mSOBuffer[0]->Unmap(0, &range);
	commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mSOBuffer[0], RHIResourceState::GenericRead, RHIResourceState::StreamOut));*/
	//mCurrentNbOfParticles = 1;
	//commandList->Reset(gRenderer->CommandAllocator(), gRenderer->MainPSO());
	// Render the particles.
	gRenderer->StartRendering(false);

	/*commandList->SetPipelineState(mRenderPSO);
	commandList->SetGraphicsRootSignature(mRenderRootSignature);
	commandList->SetDescriptorHeaps(1, &mRenderCBVHeap);
	commandList->SetGraphicsRootDescriptorTable(0, mRenderCBVHeap->GetGPUDescriptorHandleForHeapStart());

	commandList->RSSetViewports(1, &mViewport);
	commandList->RSSetScissorRects(1, &mScissorRect);

	commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mSOBuffer[mCurrentSOB], RHIResourceState::StreamOut, RHIResourceState::GenericRead));
	commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::PointList);
	commandList->IASetVertexBuffers(0, 1, &mSOVertexBufferView[mCurrentSOB]);
	commandList->DrawInstanced(mCurrentNbOfParticles, 1, 0, 0);
	commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(mSOBuffer[mCurrentSOB], RHIResourceState::GenericRead, RHIResourceState::StreamOut));

	const UINT64 size = (mCurrentNbOfParticles + 10) * sizeof(Vertex);
	mVertexBufferView[mCurrentVB].SizeInBytes = size;
	mSOBufferView[mCurrentSOB].SizeInBytes = size + sizeof(UINT64);
	mSOVertexBufferView[mCurrentSOB].SizeInBytes = size;*/

	mCurrentVB = mCurrentSOB;
	mCurrentSOB ^= 1;
}
