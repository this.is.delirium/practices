#pragma once
#include "StdafxSamples.h"
#include "Sample.h"

#include <RHITexture.h>

class CSWriteToTextureSample : public Sample
{
public:
	CSWriteToTextureSample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor)
		: Sample(app, name, bgColor, true, DirectX::XMFLOAT2(400.0f, 400.0f)) { }

	void Release() override final;

	void Initialize() override final;

	void RenderImGui() override final;

	void Update(const float deltaTime) override final;
	void Render(RHICommandList* commandList) override final;

	bool IsUseOwnRendering() const { return false; }

protected:
	void InitializeRenderingResources() override final;
	void CreateRootSignatures() override final;
	void CreatePipelineStates() override final;

protected:
	RHITexture	mTexture;
};
