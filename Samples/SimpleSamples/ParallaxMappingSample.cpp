#include "StdafxSamples.h"
#include "ParallaxMappingSample.h"
#include <Application.h>
#include <ECS/SceneComponent.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHIUtils.h>
#include <RootSignature.h>
#include <StaticGeometryBuilder.h>

using namespace GraphicsCore;

namespace
{
	enum class ParallaxMappingRootParameters
	{
		PerFrame = 0,
		PerObject,
		Params,
		DiffuseTexture,
		NormalMap,
		HeightMap,
		Count
	};

	enum class ParallaxMappingType
	{
		Steep = 0,
		Relief,
		Occlusion
	};
}

/**
 * Function : Release
 */
void ParallaxMappingSample::Release()
{
	Sample::Release();

	if (mFloor)
		mFloor->Release();

	mFloorMaterial.Release();
	mParallaxMappingParamsCB.Release();
}

/**
 * Function : Initialize:
 */
void ParallaxMappingSample::Initialize()
{
	Sample::Initialize();
}

/**
 * Function : InitializeRenderingResources
 */
void ParallaxMappingSample::InitializeRenderingResources()
{
	// Create a test plane.
	{
		mFloor = std::make_shared<StaticObject>();
		mFloor->Initialize();
		StaticGeometryBuilder::LoadPlane(mFloor->GetMeshComponent(), mFloor->GetSceneComponent());
		mFloor->GetSceneComponent()->SetScale(DirectX::XMFLOAT3(3.0f, 3.0f, 3.0f));
		mFloor->GetSceneComponent()->SetPosition(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	}

	// Load textures.
	{
		mFloorMaterial.Diffuse.InitializeByName(__TEXT("Textures:ParallaxMapping:Diffuse.jpg"));
		mFloorMaterial.NormalMap.InitializeByName(__TEXT("Textures:ParallaxMapping:NormalMap.jpg"));
		mFloorMaterial.HeightMap.InitializeByName(__TEXT("Textures:ParallaxMapping:HeightMap.jpg"));
	}

	// Create CB of parallax mapping params.
	{
		mParallaxMappingParamsCB.Data().Scale() = 0.1f;
		mParallaxMappingParamsCB.Data().DivideZoneX() = 0.5f;
		mParallaxMappingParamsCB.Initialize(__TEXT("ParallaxMappingParams"), RHIUtils::GetConstBufferSize(), 1, sizeof(mParallaxMappingParamsCB.Data()), &mParallaxMappingParamsCB.Data());
	}

	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, true);
}

/**
 * Function : CreateRootSignatures
 */
void ParallaxMappingSample::CreateRootSignatures()
{
	RHIUtils::RootSignature rootSignature(ToInt(ParallaxMappingRootParameters::Count), 1u);
	rootSignature[ToInt(ParallaxMappingRootParameters::PerFrame)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 0);
	rootSignature[ToInt(ParallaxMappingRootParameters::PerObject)].InitAsDescriptorTable(1, RHIShaderVisibility::Vertex).InitRange(0, RHIDescriptorRangeType::CBV, 1, 1);
	rootSignature[ToInt(ParallaxMappingRootParameters::Params)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::CBV, 1, 2);
	rootSignature[ToInt(ParallaxMappingRootParameters::DiffuseTexture)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 0);
	rootSignature[ToInt(ParallaxMappingRootParameters::NormalMap)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 1);
	rootSignature[ToInt(ParallaxMappingRootParameters::HeightMap)].InitAsDescriptorTable(1, RHIShaderVisibility::Pixel).InitRange(0, RHIDescriptorRangeType::SRV, 1, 2);

	RHIStaticSamplerDesc& samplerDesc	= rootSignature.SamplerDesc(0);
	samplerDesc							= RHIStaticSamplerDesc(RHIDefault());
	samplerDesc.Filter					= RHIFilter::MinMagMipLinear;

	mRootSignature = rootSignature.Finalize(GetRootSignatureFlags(true, true, false, false, false, true, false));
}

/**
 * Function : CreatePipelineStates
 */
void ParallaxMappingSample::CreatePipelineStates()
{
	RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("ParallaxMapping:Main.vs"), RHIShaderType::Vertex));
	RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("ParallaxMapping:Main.ps"), RHIShaderType::Pixel));

	if (vertexShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
		return;

	RHIGraphicsPipelineStateDesc psoDesc				= {};
	psoDesc.RootSignature								= mRootSignature;
	psoDesc.InputLayout									= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
	psoDesc.VS											= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
	psoDesc.PS											= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
	psoDesc.PrimitiveTopologyType						= RHIPrimitiveTopologyType::Triangle;
	psoDesc.SampleMask									= UINT_MAX;
	psoDesc.NumRenderTargets							= 1;
	psoDesc.RTVFormats[0]								= gRenderer->RenderTargetFormat();
	psoDesc.DSVFormat									= gRenderer->DepthStencilViewFormat();
	psoDesc.SampleDesc.Count							= 1;

	psoDesc.DepthStencilState.DepthEnable				= TRUE;
	psoDesc.DepthStencilState.DepthFunc					= RHIDepthFunc::LEqual;
	psoDesc.DepthStencilState.DepthWriteMask			= RHIDepthWriteMask::On;
	psoDesc.DepthStencilState.StencilEnable				= FALSE;

	psoDesc.RasterizerState								= RHIRasterizerDesc(RHIDefault());
	psoDesc.RasterizerState.CullMode					= RHICullMode::None;
	psoDesc.RasterizerState.FrontCounterClockwise		= true;

	psoDesc.BlendState									= RHIBlendDesc(RHIDefault());
	psoDesc.BlendState.RenderTarget[0].BlendEnable		= TRUE;
	psoDesc.BlendState.RenderTarget[0].SrcBlend			= RHIBlend::SrcAlpha;
	psoDesc.BlendState.RenderTarget[0].DestBlend		= RHIBlend::InvSrcAlpha;
	psoDesc.BlendState.RenderTarget[0].SrcBlendAlpha	= RHIBlend::SrcAlpha;
	psoDesc.BlendState.RenderTarget[0].DestBlendAlpha	= RHIBlend::InvSrcAlpha;

	mPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
}

/**
 * Function : RenderImGui
 */
void ParallaxMappingSample::RenderImGui()
{
	ParallaxMappingParams& params = mParallaxMappingParamsCB.Data();
	if (ImGui::SliderFloat("Scale", &params.Scale(), 0.0f, 1.0f))
		mParallaxMappingParamsCB.MarkDirty();

	if (ImGui::SliderFloat("DivideZoneX", &params.DivideZoneX(), 0.0f, 1.0f))
		mParallaxMappingParamsCB.MarkDirty();

	static int type = 0;
	ImGui::RadioButton("steep", &type, ToInt(ParallaxMappingType::Steep));
	ImGui::RadioButton("relief", &type, ToInt(ParallaxMappingType::Relief));
	ImGui::RadioButton("occlusion", &type, ToInt(ParallaxMappingType::Occlusion));
	mParallaxMappingParamsCB.Data().Type() = type;
	mParallaxMappingParamsCB.MarkDirty();
}

/**
 * Function : Update
 */
void ParallaxMappingSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	if (mParallaxMappingParamsCB.IsDirty())
		mParallaxMappingParamsCB.UpdateDataOnGPU();

	mFloor->Update(deltaTime);
	// Update local transform.
	/*{
		static float time = 0.0f;

		mConstBufferPerObject.Data().WorldMatrix	= DirectX::XMMatrixRotationAxis(DirectX::XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f), time * DirectX::XMConvertToRadians(180.0f));
		mConstBufferPerObject.Data().NormalMatrix	= DirectX::XMMatrixIdentity();
		mConstBufferPerObject.UpdateDataOnGPU();

		time += deltaTime;
	}*/
}

/**
 * Function : Render
 */
void ParallaxMappingSample::Render(RHICommandList* commandList)
{
	commandList->SetPipelineState(mPSO);
	commandList->SetGraphicsRootSignature(mRootSignature);

	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };
	commandList->SetDescriptorHeaps(_countof(heaps), heaps);

	commandList->SetGraphicsRootDescriptorTable(ToInt(ParallaxMappingRootParameters::PerFrame), mConstBufferPerFrame.SrvHandle().Gpu());
	commandList->SetGraphicsRootDescriptorTable(ToInt(ParallaxMappingRootParameters::Params), mParallaxMappingParamsCB.SrvHandle().Gpu());
	commandList->SetGraphicsRootDescriptorTable(ToInt(ParallaxMappingRootParameters::DiffuseTexture), mFloorMaterial.Diffuse.SrvHandle().Gpu());
	commandList->SetGraphicsRootDescriptorTable(ToInt(ParallaxMappingRootParameters::NormalMap), mFloorMaterial.NormalMap.SrvHandle().Gpu());
	commandList->SetGraphicsRootDescriptorTable(ToInt(ParallaxMappingRootParameters::HeightMap), mFloorMaterial.HeightMap.SrvHandle().Gpu());
	commandList->RSSetViewports(1, &mViewport);
	commandList->RSSetScissorRects(1, &mScissorRect);

	mFloor->Draw(commandList, ToInt(ParallaxMappingRootParameters::PerObject));
}
