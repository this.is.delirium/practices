#include "StdafxSamples.h"
#include "WelcomeScreenSample.h"
#include <Application.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHIUtils.h>

using namespace GraphicsCore;

/**
 * Function : Release
 */
void WelcomeScreenSample::Release()
{
	Sample::Release();

	mTriangleVertexBuffer.Release();
}

/**
 * Function : Initialize:
 */
void WelcomeScreenSample::Initialize()
{
	Sample::Initialize();
}

/**
 * Function : InitializeRenderingResources
 */
void WelcomeScreenSample::InitializeRenderingResources()
{
	// Fill vertex buffer.
	{
		InputLayouts::Main::Vertex triangle[3];
		triangle[0].Position = DirectX::XMFLOAT3(5.0f, 0.0f, 0.0f);		triangle[0].Normal = DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f);
		triangle[1].Position = DirectX::XMFLOAT3(0.0f, -2.5f, 0.0f);	triangle[1].Normal = DirectX::XMFLOAT3(1.0f, 1.0f, 0.0f);
		triangle[2].Position = DirectX::XMFLOAT3(0.0f, 2.5f, 0.0f);		triangle[2].Normal = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);

		mTriangleVertexBuffer.Initialize(__TEXT("WelcomeScreen:TriangleVB"), sizeof(triangle), _countof(triangle), sizeof(InputLayouts::Main::Vertex), triangle);
	}

	CreateConstantBuffersForMatrix(gRenderer->CbvSrvUavHeapManager().Heap(), true, true);
}

/**
 * Function : CreateRootSignatures
 */
void WelcomeScreenSample::CreateRootSignatures()
{
	RHIDescriptorRange ranges[2];
	ranges[0].Init(RHIDescriptorRangeType::CBV, 1, 0);
	ranges[1].Init(RHIDescriptorRangeType::CBV, 1, 1);
		
	RHIRootParameter rootParameters[2];
	rootParameters[0].InitAsDescriptorTable(1, &ranges[0], RHIShaderVisibility::Vertex);
	rootParameters[1].InitAsDescriptorTable(1, &ranges[1], RHIShaderVisibility::Vertex);

	RHIRootSignatureDesc rootSignatureDesc;
	RHIRootSignatureFlags flags = GetRootSignatureFlags(true, true, false, false, false, false, false);

	rootSignatureDesc.Init(_countof(rootParameters), &rootParameters[0], 0, nullptr, flags);

	mRootSignature = gRenderer->SerializeAndCreateRootSignature(&rootSignatureDesc, RHIRootSignatureVersion::Ver1);
}

/**
 * Function : CreatePipelineStates
 */
void WelcomeScreenSample::CreatePipelineStates()
{
	RHIBlobWrapper vertexShader		(gRenderer->LoadShader(__TEXT("TestShader.vs"), RHIShaderType::Vertex));
	RHIBlobWrapper fragmentShader	(gRenderer->LoadShader(__TEXT("TestShader.ps"), RHIShaderType::Pixel));

	if (vertexShader.Blob->IsNull() || fragmentShader.Blob->IsNull())
		return;

	RHIGraphicsPipelineStateDesc psoDesc				= {};
	psoDesc.RootSignature								= mRootSignature;
	psoDesc.InputLayout									= { InputLayouts::Main::inputElementDesc, _countof(InputLayouts::Main::inputElementDesc) };
	psoDesc.VS											= { vertexShader.GetBufferPointer(), vertexShader.GetBufferSize() };
	psoDesc.PS											= { fragmentShader.GetBufferPointer(), fragmentShader.GetBufferSize() };
	psoDesc.PrimitiveTopologyType						= RHIPrimitiveTopologyType::Triangle;
	psoDesc.SampleMask									= UINT_MAX;
	psoDesc.NumRenderTargets							= 1;
	psoDesc.RTVFormats[0]								= gRenderer->RenderTargetFormat();
	psoDesc.DSVFormat									= gRenderer->DepthStencilViewFormat();
	psoDesc.SampleDesc.Count							= 1;

	psoDesc.DepthStencilState.DepthEnable				= TRUE;
	psoDesc.DepthStencilState.DepthFunc					= RHIDepthFunc::LEqual;
	psoDesc.DepthStencilState.DepthWriteMask			= RHIDepthWriteMask::On;
	psoDesc.DepthStencilState.StencilEnable				= FALSE;

	psoDesc.RasterizerState								= RHIRasterizerDesc(RHIDefault());
	psoDesc.RasterizerState.CullMode					= RHICullMode::None;
	psoDesc.RasterizerState.FrontCounterClockwise		= true;

	psoDesc.BlendState									= RHIBlendDesc(RHIDefault());
	psoDesc.BlendState.RenderTarget[0].BlendEnable		= TRUE;
	psoDesc.BlendState.RenderTarget[0].SrcBlend			= RHIBlend::SrcAlpha;
	psoDesc.BlendState.RenderTarget[0].DestBlend		= RHIBlend::InvSrcAlpha;
	psoDesc.BlendState.RenderTarget[0].SrcBlendAlpha	= RHIBlend::SrcAlpha;
	psoDesc.BlendState.RenderTarget[0].DestBlendAlpha	= RHIBlend::InvSrcAlpha;

	mPSO = gRenderer->Device()->CreateGraphicsPipelineState(&psoDesc);
}

/**
 * Function : RenderImGui
 */
void WelcomeScreenSample::RenderImGui()
{
	ImGui::ColorEdit4("Background color", &mBGColor.x);
}

/**
 * Function : Update
 */
void WelcomeScreenSample::Update(const float deltaTime)
{
	UpdateCameraResource();

	// Update local transform.
	{
		static float time = 0.0f;

		mConstBufferPerObject.Data().WorldMatrix	= DirectX::XMMatrixRotationAxis(DirectX::XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f), time * DirectX::XMConvertToRadians(180.0f));
		mConstBufferPerObject.Data().NormalMatrix	= DirectX::XMMatrixIdentity();
		mConstBufferPerObject.UpdateDataOnGPU();

		time += deltaTime;
	}
}

/**
 * Function : Render
 */
void WelcomeScreenSample::Render(RHICommandList* commandList)
{
	commandList->SetPipelineState(mPSO);
	commandList->SetGraphicsRootSignature(mRootSignature);

	RHIDescriptorHeap* heaps[] = { gRenderer->CbvSrvUavHeapManager().Heap() };
	commandList->SetDescriptorHeaps(_countof(heaps), heaps);

	commandList->SetGraphicsRootDescriptorTable(0, mConstBufferPerFrame.SrvHandle().Gpu());
	commandList->SetGraphicsRootDescriptorTable(1, mConstBufferPerObject.SrvHandle().Gpu());
	commandList->RSSetViewports(1, &mViewport);
	commandList->RSSetScissorRects(1, &mScissorRect);

	commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
	commandList->IASetVertexBuffers(0, 1, mTriangleVertexBuffer.View());
	commandList->DrawInstanced(mTriangleVertexBuffer.NumElements(), 1, 0, 0);
}
