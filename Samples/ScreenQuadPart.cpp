#include "StdafxSamples.h"
#include "ScreenQuadPart.h"
#include <Render/IRenderer.h>
#include <Render/GraphicsCore.h>
#include <ResourceManager.h>
#include <RHIUtils.h>

/**
 * Function : Release
 */
void ScreenQuadPart::Release()
{
	mVertexBuffer.Release();
	mConstBufferPerFrame.Release();
	mConstBufferTextureType.Release();
}

/**
 * Function : Initialize
 */
void ScreenQuadPart::Initialize()
{
	// Create an ortho camera.
	{
		mOrthoCamera = std::make_shared<Camera>();

		Camera::CameraDesc desc;
		desc.Common.Position	= DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
		desc.Common.ZRange		= DirectX::XMFLOAT2(-1.0f, 1.0f);

		desc.ProjType			= Camera::ProjectionType::Orthographic;
		desc.OrthoPart.View		= DirectX::XMFLOAT4(0.0f, mWindowSize.x, 0.0f, mWindowSize.y);

		desc.Format				= Camera::InitializeFormat::FromDirection;
		desc.DirPart.Direction	= DirectX::XMFLOAT4(0.0f, 0.0f, -1.0f, 0.0f);
		desc.DirPart.Up			= DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f);

		mOrthoCamera->Initialize(desc);
	}

	// Create a screen quad.
	{
		const UINT64 size = sizeof(ScreenVertex) * 6;
		mVertexBuffer.Initialize(__TEXT("ScreenQuad:VertexBuffer"), size, 6, sizeof(ScreenVertex));

		UpdateScreenQuad();
	}

	// Create constant buffers for screen quad.
	{
		mConstBufferPerFrame.Data().ViewProjMatrix = mOrthoCamera->Projection();

		mConstBufferPerFrame.Initialize(__TEXT("ScreenQuad:CBPerFrame"), RHIUtils::GetConstBufferSize(), 1, sizeof(mConstBufferPerFrame.Data()), &mConstBufferPerFrame.Data());

		mConstBufferTextureType.Initialize(__TEXT("ScreenQuad:CBTextureType"), RHIUtils::GetConstBufferSize(), 1, sizeof(mConstBufferTextureType.Data()), &mConstBufferTextureType.Data());
	}
}

/**
 * Function : SetTextureType
 */
void ScreenQuadPart::SetTexture(RHIGPUDescriptorHandle srvGpuHandle, const TextureType texType)
{
	mTextureSrvGpuHandle = srvGpuHandle;

	mConstBufferTextureType.Data().Type = ToInt(texType);
	mConstBufferTextureType.UpdateDataOnGPU();
}

/**
 * Function : UpdateDepthParams
 */
void ScreenQuadPart::UpdateDepthParams(const DirectX::XMFLOAT4& depthParams)
{
	mConstBufferPerFrame.Data().DepthParams = depthParams;
	mConstBufferPerFrame.UpdateDataOnGPU();
}

/**
 * Function : UpdateScreenQuad
 */
void ScreenQuadPart::UpdateScreenQuad()
{
	BuildScreenQuad(mPosition.x, mPosition.x + mSize.x, mPosition.y, mPosition.y + mSize.y);
	GraphicsCore::rs().renderer()->UpdateBuffer(&mVertexBuffer, &RHIUtils::GetSubresource(mScreenQuad.data(), sizeof(ScreenVertex) * mScreenQuad.size()));
}

/**
 * Function : BuildScreenQuad
 */
void ScreenQuadPart::BuildScreenQuad(const float left, const float right, const float top, const float bottom)
{
	mScreenQuad.clear();

	mScreenQuad.push_back(ScreenVertex(left, top, 0.0f, 1.0f));
	mScreenQuad.push_back(ScreenVertex(left, bottom, 0.0f, 0.0f));
	mScreenQuad.push_back(ScreenVertex(right, bottom, 1.0f, 0.0f));

	mScreenQuad.push_back(ScreenVertex(left, top, 0.0f, 1.0f));
	mScreenQuad.push_back(ScreenVertex(right, bottom, 1.0f, 0.0f));
	mScreenQuad.push_back(ScreenVertex(right, top, 1.0f, 1.0f));
}
