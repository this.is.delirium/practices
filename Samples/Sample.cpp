#include "StdafxSamples.h"
#include "Sample.h"
#include "Application.h"
#include <Camera.h>
#include <Render/GraphicsCore.h>
#include <RHIUtils.h>
#include <ViewFrustum.h>
#include <Window.h>

using namespace GraphicsCore;

/**
 * Function : Sample
 */
Sample::Sample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor, const bool useScreenQuad, const DirectX::XMFLOAT2& quadSize)
	: mApp(app), mName(name), mBGColor(bgColor), mUseScreenQuad(useScreenQuad), mScreenQuadSize(quadSize) { }

/**
 * Function : Release
 */
void Sample::Release()
{
	RHISafeRelease(mRootSignature);
	RHISafeRelease(mPSO);

	mConstBufferPerFrame.Release();
	mConstBufferPerObject.Release();

	RHISafeRelease(mScreenQuad);
}

/**
 * Function : Initialize
 */
void Sample::Initialize()
{
	mViewport			= {};
	mViewport.Width		= static_cast<FLOAT> (mApp->GetWindow()->Width());
	mViewport.Height	= static_cast<FLOAT> (mApp->GetWindow()->Height());
	mViewport.MaxDepth	= 1.0f;

	mScissorRect		= {};
	mScissorRect.right	= static_cast<LONG> (mApp->GetWindow()->Width());
	mScissorRect.bottom	= static_cast<LONG> (mApp->GetWindow()->Height());

	mConstBufferPerFrame.Data().DepthParams = DirectX::XMFLOAT4(0.1f, 500.0f, 0.0f, 0.0f);

	if (mUseScreenQuad)
	{
		InitializeScreenQuadPart();
	}

	SetCameras();

	InitializeRenderingResources();
	CreateRootSignatures();
	CreatePipelineStates();

	mApp->SetBackgroundColor(&mBGColor);
}

/**
 * Function : SetCameras
 */
void Sample::SetCameras()
{
	CreateDefaultCamera(mApp->GetWindow()->AspectRatio());

	mCamera->SetName("Default Camera");
	mApp->AddCamera(mCamera);
	mApp->SetCurrentCamera(mCamera);
}

/**
 * Function : InitializeScreenQuadPart
 */
void Sample::InitializeScreenQuadPart()
{
	DirectX::XMFLOAT2 windowSize = mApp->GetWindow()->Size();
	DirectX::XMFLOAT2 quadPosition(windowSize.x - mScreenQuadSize.x - 16.0f, windowSize.y - mScreenQuadSize.y - 16.0f);
	mScreenQuad = new ScreenQuadPart(windowSize, quadPosition, mScreenQuadSize);
	mScreenQuad->Initialize();
}

/**
 * Function : CreateDefaultCamera
 */
void Sample::CreateDefaultCamera(const float aspectRatio)
{
	Camera::CameraDesc desc;
	desc.Common.Position			= DirectX::XMFLOAT4(15.0f, 0.0f, 10.0f, 1.0f);
	desc.Common.ZRange				= DirectX::XMFLOAT2(mConstBufferPerFrame.Data().DepthParams.x, mConstBufferPerFrame.Data().DepthParams.y);

	desc.ProjType					= Camera::ProjectionType::Perspective;
	desc.PerspPart.FoV				= 45.0f;
	desc.PerspPart.AspectRatio		= mApp->GetWindow()->AspectRatio();

	desc.Format						= Camera::InitializeFormat::FromSphericalAngles;
	desc.AnglesPart.AngleXRange		= DirectX::XMFLOAT2(-DirectX::XM_PI, DirectX::XM_PI);
	desc.AnglesPart.AngleYRange		= DirectX::XMFLOAT2(-DirectX::XM_PIDIV2, DirectX::XM_PIDIV2);
	desc.AnglesPart.SphericalAngles	= DirectX::XMFLOAT2(DirectX::XM_PI * 1.0f, -DirectX::XM_PIDIV2 * 0.3f);

	mCamera = std::make_shared<Camera>();
	mCamera->Initialize(desc);
}

/**
 * Function : UpdateCameraResource
 */
void Sample::UpdateCameraResource()
{
	if (!mConstBufferPerFrame.IsNull())
	{
		std::shared_ptr<Camera>& camera = mApp->CurrentCamera();
		if (camera.get())
		{
			mConstBufferPerFrame.Data().ViewMatrix		= camera->LookAt();
			mConstBufferPerFrame.Data().ProjMatrix		= camera->Projection();
			mConstBufferPerFrame.Data().ViewProjMatrix	= camera->ViewProjectionMatrix();
			mConstBufferPerFrame.Data().InvProjMatrix	= camera->InverseProjection();
			mConstBufferPerFrame.Data().InvViewMatrix	= camera->InverseLookAt();
			//mConstBufferPerFrame.Data().DepthParams	= DirectX::XMFLOAT4(camera->ZRange().x, camera->ZRange().y, 0.0f, 0.0f);
			mConstBufferPerFrame.Data().DepthParams.x	= camera->ZRange().x;
			mConstBufferPerFrame.Data().DepthParams.y	= camera->ZRange().y;
			mConstBufferPerFrame.Data().CameraPosition	= camera->Position();
			mConstBufferPerFrame.Data().CameraDirection	= camera->Direction();
			mConstBufferPerFrame.Data().CameraUp		= camera->UpVector();
			mConstBufferPerFrame.Data().WindowParams	= DirectX::XMFLOAT4(mApp->GetWindow()->Width(), mApp->GetWindow()->Height(), 0.0f, 0.0f);

			mConstBufferPerFrame.UpdateDataOnGPU();
		}
	}
}

/**
 * Function : CreateConstantBuffersForMatrix
 */
void Sample::CreateConstantBuffersForMatrix(RHIDescriptorHeap* cbvHeap, const bool perFrame, const bool perObject)
{
	const UINT64 size = 1024 * 4;

	// Constant buffer per frame.
	if (perFrame)
	{
		// Create a constant buffer.
		mConstBufferPerFrame.Initialize(__TEXT("Sample:CBPerFrame"), size, 1, sizeof(mConstBufferPerFrame.Data()));

		// Fill the constant buffer for per frame.
		//mConstBufferPerFrameData.ViewProjMatrix	= mCamera->Projection() * mCamera->LookAt();
		//mConstBufferPerFrameData.ViewMatrix		= mCamera->LookAt();
		//mConstBufferPerFrame->InitializeSubData(0, &mConstBufferPerFrameData, sizeof(ConstantBuffers::PerFrame));
	}

	// Constant buffer per object.
	if (perObject)
	{
		// Create a constant buffer.
		mConstBufferPerObject.Initialize(__TEXT("Sample:CBPerObject"), size, 1, sizeof(mConstBufferPerObject.Data()));

		// Fill the constant buffer per object.
		mConstBufferPerObject.Data().WorldMatrix	= DirectX::XMMatrixIdentity();
		mConstBufferPerObject.Data().NormalMatrix	= DirectX::XMMatrixIdentity();
		mConstBufferPerObject.UpdateDataOnGPU();
	}
}

/**
 * Function : DrawScreenQuad
 */
void Sample::DrawScreenQuad(RHICommandList* commandList, RHIDescriptorHeap** heaps, const UINT countHeaps)
{
	commandList->SetPipelineState(gRenderer->ScreenQuadPSO());
	commandList->SetGraphicsRootSignature(gRenderer->ScreenQuadRootSignature());

	commandList->SetDescriptorHeaps(countHeaps, heaps);
	commandList->SetGraphicsRootDescriptorTable(0, mScreenQuad->GetConstBufferPerFrame().SrvHandle().Gpu());
	commandList->SetGraphicsRootDescriptorTable(1, mScreenQuad->GetConstBufferTextureType().SrvHandle().Gpu());
	commandList->SetGraphicsRootDescriptorTable(2, mScreenQuad->TextureSrvGpuHandle());

	commandList->RSSetViewports(1, &mViewport);
	commandList->RSSetScissorRects(1, &mScissorRect);
	commandList->IASetPrimitiveTopology(RHIPrimitiveTopology::TriangleList);
	commandList->IASetVertexBuffers(0, 1, mScreenQuad->VertexBufferView());
	commandList->DrawInstanced(6, 1, 0, 0);
}
