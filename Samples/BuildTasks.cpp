#include "StdafxSamples.h"
#include "BuildTasks.h"
#include <Render/GraphicsCore.h>
#include <Render/IRenderer.h>

using namespace GraphicsCore;

namespace BuildTasks
{
	/**
	 * Function : Execute
	 */
	void TerrainDeviceTask::Execute()
	{
		{
			RHISubresourceData subresData	= {};
			subresData.Data					= Vertices;
			subresData.RowPitch				= VerticesSize;
			subresData.SlicePitch			= subresData.RowPitch;

			gRenderer->UpdateBuffer(VertexBuffer, &subresData);
		}

		{
			RHISubresourceData subresData	= {};
			subresData.Data					= Indices;
			subresData.RowPitch				= IndicesSize;
			subresData.SlicePitch			= subresData.RowPitch;

			gRenderer->UpdateBuffer(IndexBuffer, &subresData);
		}

		VertexBuffer->View()->SizeInBytes	= VerticesSize;
		IndexBuffer->View()->SizeInBytes	= IndicesSize;

		IndexBuffer->SetNumElements(IndexCount);
	}
}
