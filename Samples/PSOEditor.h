#pragma once
#include "StdafxSamples.h"
#include <StdafxCore.h>
#include <RHI.h>
#include <RHIUtils.h>

class PSOEditor
{
public:
	void Release();

	void LoadShader(const String& hierarchy);
	void SaveShader(const String& hierarchy);

	FORCEINLINE char* Buffer() { return &mShaderBuffer[0]; }
	FORCEINLINE int BufferSize() const { return mShaderBuffer.size() * sizeof(char); }
	FORCEINLINE bool IsReady() const { return mIsReady; }

protected:
	bool		mIsReady = false;
	std::string	mShaderBuffer;

protected:
	struct ShaderInfo
	{
		~ShaderInfo()
		{
			RHISafeRelease(Blob);
		}

		String			Name;
		String			Hierarchy;
		RHIShaderType	Type	= RHIShaderType::Unknown;
		RHIBlob*		Blob	= nullptr;
	};

	struct PSOInfo
	{
		String					Name;
		RHIPipelineState**		PSO = nullptr;
		RHIPipelineStateStream	PSODesc;

		ShaderInfo				Shaders[ToInt(RHIShaderType::Count)];

		void AttachShaders(const RHIUtils::Fill::ShadersArray& shaders);

	protected:
		void RegisterShader(const String& name, const RHIShaderType type, RHIBlob* blob);
	};

public:
	using ArrayPSOInfo		= std::list<PSOInfo>;
	using RegisteredPSOs	= std::unordered_map<String, ArrayPSOInfo>; // Name of mode, shaders.

public:
	PSOInfo& RegisterPSO(const String& modeName, const String& psoName, RHIPipelineState** pso, const RHIPipelineStateStream& psoDesc);

	void RebuildShader(PSOInfo* psoInfo, const ShaderInfo* shaderInfo);

	FORCEINLINE bool CheckMode(const String& modeName) const { return mRegisteredPSOs.find(modeName) != mRegisteredPSOs.cend(); }
	FORCEINLINE ArrayPSOInfo& PSOs(const String& modeName) { return mRegisteredPSOs[modeName]; }

	FORCEINLINE ShaderInfo*& SelectedShader() { return mSelectedShader; }
	FORCEINLINE PSOInfo*& SelectedPSO() { return mSelectedPSO; }

protected:
	RegisteredPSOs	mRegisteredPSOs;
	ShaderInfo*		mSelectedShader	= nullptr;
	PSOInfo*		mSelectedPSO	= nullptr;
};
