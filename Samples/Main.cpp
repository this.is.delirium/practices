#include "StdafxSamples.h"
#include <Application.h>

INT APIENTRY WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevIntsance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow)
{
	AppDesc desc;
	desc.gAPI					= GraphicsAPI::DirectX12;
	desc.wndDesc.className		= String(__TEXT("practicesClass"));
	desc.wndDesc.title			= String(__TEXT("practices"));
	desc.wndDesc.hInstance		= hInstance;
	desc.wndDesc.nCmdShow		= nCmdShow;
	desc.wndDesc.width			= 1500;
	desc.wndDesc.height			= 800;
	desc.wndDesc.isFullscreen	= false;

	Application app;
	if (!app.Initialize(&desc))
	{
		MessageBox(NULL, __TEXT("Can't create an instance of application."), __TEXT("Error"), MB_OK | MB_ICONERROR);
		app.Release();
		return 1;
	}

	app.Run();

	app.Release();

	return 0;
}
