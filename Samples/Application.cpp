#include "StdafxSamples.h"

#include "Application.h"
#include <FileManager.h>
#include <Render/ImGuiDirectX12.h>
#include <Logger.h>
#include "PSOEditor.h"
#include <RenderSurface.h>
#include <ResourceManager.h>
#include <ExtendedSamples/DeferredShadingSample.h>
#include <ExtendedSamples/OITSample.h>
#include <ExtendedSamples/ParticlesSample.h>
#include <ExtendedSamples/PBRSample.h>
#include <ExtendedSamples/PSSMSample.h>
#include <ExtendedSamples/ShadowMapSample.h>
#include <ExtendedSamples/SSAOSample.h>
#include <ExtendedSamples/SSLRSample.h>
#include <ExtendedSamples/VarianceShadowMappingSample.h>
#include <SimpleSamples/CSGSample.h>
#include <SimpleSamples/FractalSample.h>
#include <SimpleSamples/HeightMapSample.h>
#include <SimpleSamples/ParallaxMappingSample.h>
#include <SimpleSamples/RenderToTextureSample.h>
#include <SimpleSamples/StreamOutputSample.h>
#include <SimpleSamples/TesselationSample.h>
#include <SimpleSamples/CSWriteToTextureSample.h>
#include <SimpleSamples/WelcomeScreenSample.h>
#include <StringUtils.h>
#include <TaskManager.h>
#include <ViewFrustum.h>

#include <D3D12/D3D12Renderer.h>

#ifndef NoOpenGL
	#include <ImGuiOpenGL.h>
	#include <OpenGL/OpenGLRenderer.h>
#endif

/**
 * Function : Release
 */
void Application::Release()
{
	mPSOEditor->Release();

	CurrentSample()->Release();
	mSamples.clear();
	//for (auto Sample : mSamples)
	//	Sample.second->Release();

	for (auto& frustum : mViewFrustums)
		frustum->Release();

	if (mTaskThread.native_handle())
		mTaskThread.detach();

	RenderSystem::Instance().Release();

	mWindow.Release();

	SLogger::Instance().FlushToDisk();
}

/**
 * Function : Initialize
 */
bool Application::Initialize(const AppDesc* desc)
{
	mIsDone		= false;
	bool result = true;

	SLogger::Instance().SetOutputFile(__TEXT("log.txt"));
	SResourceManager::Instance().FindRootFolder();
	SetScreenShotsFolder(__TEXT("D:/Tmp"));

	result &= mWindow.Initialize(&desc->wndDesc);
	mCurrentGAPI = desc->gAPI;

	// Initialize render system.
	{
		RenderSystem::RSDesc rsDesc = {};
		rsDesc.gAPI = desc->gAPI;
		rsDesc.window = &mWindow;

		RenderSystem::Instance().Initialize(rsDesc);
	}


	mPSOEditor = std::make_shared<PSOEditor>();

	SamplesInitialize();

	//GraphicsCore::rs().renderer()->CommitCommandList(GraphicsCore::rs().renderer()->CommandList());

	mWindow.mOnClose = std::bind(&Application::Stop, this);
	mWindow.SetOnGuiHandler(this, &Application::ImGuiWndProcHandler);
	mWindow.SetOnMouseMoveHandler(this, &Application::MouseMoveHandle);
	mWindow.SetOnMouseWheel(this, &Application::MouseWheelHandler);

	SetSample(AppSample::VarianceShadowMapping);

	mTaskThread = std::thread(&Application::ExecuteTasks, this);

	SetMousePosition(glm::ivec2(mWindow.Width() / 2, mWindow.Height() / 2));

	return result;
}

/**
 * Function : SetSample
 */
void Application::SetSample(const AppSample sample)
{
	if (mSample != AppSample::Unknown)
		CurrentSample()->Deactivate();

	ResetActiveCameras();
	ResetViewFrustums();

	mSample = sample;
	CurrentSample()->Activate();
}

/**
 * Function : SamplesInitialize
 */
void Application::SamplesInitialize()
{
	mSamples[AppSample::WelcomeScreen]					= std::make_shared<WelcomeScreenSample>(this, __TEXT("Welcome screen"), DirectX::XMFLOAT4(0.2f, 0.8f, 0.4f, 1.0f));
	mSamples[AppSample::Fractal]						= std::make_shared<FractalSample>(this, __TEXT("Fractals"), DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	mSamples[AppSample::HeightMap]						= std::make_shared<HeightMapSample>(this, __TEXT("Height map"), DirectX::XMFLOAT4(1.0f, 1.0f, 0.2f, 1.0f));
	mSamples[AppSample::Tesselation]					= std::make_shared<TessellationSample>(this, __TEXT("Test tesselation"), DirectX::XMFLOAT4(0.8f, 0.4f, 0.2f, 1.0f));
	mSamples[AppSample::RenderToTexture]				= std::make_shared<RenderToTextureSample>(this, __TEXT("Render to texture"), DirectX::XMFLOAT4(0.8f, 0.4f, 0.2f, 1.0f));
	mSamples[AppSample::CSWriteToTexture]				= std::make_shared<CSWriteToTextureSample>(this, __TEXT("Write to texture from compute shader"), DirectX::XMFLOAT4(0.2f, 0.4f, 0.8f, 1.0f));
	mSamples[AppSample::ParallaxMapping]				= std::make_shared<ParallaxMappingSample>(this, __TEXT("Parallax mapping"), DirectX::XMFLOAT4(0.2f, 0.8f, 0.0f, 1.0f));
	//mSamples[AppSample::CSG]							= std::make_shared<CSGSample>(this, __TEXT("CSG"), DirectX::XMFLOAT4(0.0f));
	//mSamples[AppSample::StreamOutput]					= std::make_shared<StreamOutputSample>(this, __TEXT("Stream output"), DirectX::XMFLOAT4(0.2f, 0.4f, 0.8f, 1.0f));

	mSamples[AppSample::ShadowMapping]					= std::make_shared<ShadowMapSample>(this, __TEXT("Shadow mapping"), DirectX::XMFLOAT4(0.3f, 0.5f, 0.8f, 1.0f));
	mSamples[AppSample::DeferredShading]				= std::make_shared<DeferredShadingSample>(this, __TEXT("Deferred shading"), DirectX::XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f));
	mSamples[AppSample::OrderIndependentTransparency]	= std::make_shared<OITSample>(this, __TEXT("Order independent transparency"), DirectX::XMFLOAT4(0.0f, 0.8f, 0.3f, 1.0f));
	mSamples[AppSample::ParallelSplitShadowMapping]		= std::make_shared<PSSMSample>(this, __TEXT("Parallel split shadow mapping"), DirectX::XMFLOAT4(0.4f, 0.2f, 0.8f, 1.0f));
	mSamples[AppSample::Particles]						= std::make_shared<ParticlesSample>(this, __TEXT("Particles"), DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	mSamples[AppSample::ScreenSpaceAmbientOcclusion]	= std::make_shared<SSAOSample>(this, __TEXT("Screen Space Ambient Occlusion"), DirectX::XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f));
	mSamples[AppSample::ScreenSpaceLocalReflection]		= std::make_shared<SSLRSample>(this, __TEXT("Screen Space Local Reflection"), DirectX::XMFLOAT4(0.2f, 0.8f, 0.4f, 1.0f));
	mSamples[AppSample::PBR]							= std::make_shared<PBRSample>(this, __TEXT("Physically Based Rendering viewer"), DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	mSamples[AppSample::VarianceShadowMapping]			= std::make_shared<VarianceShadowMappingSample>(this, __TEXT("Variance Shadow Mapping"), DirectX::XMFLOAT4(0.2f, 0.4f, 0.8f, 1.0f));
}

/**
 * Function : Stop
 */
void Application::Stop()
{
	mIsWorking	= false;
	mIsDone		= true;
}

/**
 * Function : Run
 */
void Application::Run()
{
	mIsWorking = true;

	auto lastTime = std::chrono::high_resolution_clock::now();
	while (mIsWorking)
	{
		mWindow.PeekMessages();

		auto currentTime = std::chrono::high_resolution_clock::now();
		float elapsedTime = std::chrono::duration_cast<std::chrono::duration<float>>(currentTime - lastTime).count();
		if (elapsedTime > 1.0f / 60.0f)
		{
			lastTime = currentTime;

			ImGuiPlaceWidgets();

			Update(elapsedTime);
			Render();
		}
	}
}

/**
 * Function : Update
 */
void Application::Update(const float deltaTime)
{
	ExecuteDeviceStuffOfTasks();

	for (auto& frustum : mViewFrustums)
		frustum->Update(deltaTime);

	CurrentSample()->Update(deltaTime);
}

/**
 * Function : Render
 */
void Application::Render()
{
	IRenderer* renderer = GraphicsCore::rs().renderer();
	auto* imgui = GraphicsCore::rp()->GUI();

	RHICommandList* cmdList = renderer->CommandList();

	if (CurrentSample()->IsUseOwnRendering())
	{
		CurrentSample()->Render(cmdList);

		RenderViewFrustum();

		renderer->StartRendering();
		imgui->Render(renderer->CommandList()); // ToDo: Reconsider later.
		renderer->StopRendering();
	}
	else
	{
		renderer->StartRendering();
		renderer->ClearRTV(&mBGColor->x);
		renderer->ClearDSV();

		CurrentSample()->Render(cmdList);

		RenderViewFrustum();

		imgui->Render(renderer->CommandList()); // ToDo: Reconsider later.

		renderer->StopRendering();
	}

	renderer->Present();

	if (mMakeScreenshot)
	{
		ImageFormat format	= ImageFormat::WicJpeg;
		String path			= mScreenShotsFolder + GetScreenShotName(format);
		SaveScreenInFile(path, format);
	}
}

/**
 * Function : RenderViewFrustum
 */
void Application::RenderViewFrustum()
{
	if (!mShowViewFrustum)
		return;

	IRenderer* renderer = GraphicsCore::rs().renderer();

	Sample*				sample		= CurrentSample();
	ViewFrustumPtr		frustum		= mViewFrustums[mCurrentViewFrustumIdx];
	RHICommandList*		commandList	= renderer->CommandList();
	RHIDescriptorHeap*	heaps[]		= { renderer->CbvSrvUavHeapManager().Heap() };

	if (sample->IsUseOwnRendering())
	{
		commandList->Reset(renderer->CommandAllocator(), nullptr);
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(renderer->CurrentRenderTarget(), RHIResourceState::Present, RHIResourceState::RenderTarget));
		commandList->OMSetRenderTargets(1, &renderer->CurrentRenderTargetCpuHandle(), false, renderer->FrustumDepthStencilCpuHandle());
		commandList->ClearDepthStencilView(*renderer->FrustumDepthStencilCpuHandle(), RHIClearFlags::Depth, 1.0f, 0, 0, nullptr);
	}

	commandList->SetPipelineState(renderer->FrustumPSO());
	commandList->SetGraphicsRootSignature(renderer->FrustumRootSignature());

	commandList->SetDescriptorHeaps(_countof(heaps), heaps);
	commandList->SetGraphicsRootConstantBufferView(0, frustum->ConstBufferPerFrame().GpuVirtualAddress());

	commandList->RSSetViewports(1, sample->Viewport());
	commandList->RSSetScissorRects(1, sample->ScissorRect());

	if (mPartOfViewFrustum == -1)
		frustum->DrawAll(commandList);
	else
		frustum->Draw(commandList, mPartOfViewFrustum);

	if (sample->IsUseOwnRendering())
	{
		commandList->ResourceBarrier(1, &RHIResourceBarrier::MakeTransition(renderer->CurrentRenderTarget(), RHIResourceState::RenderTarget, RHIResourceState::Present));
		renderer->CommitCommandList(commandList);
	}

}

/**
 * Function : RunTasks
 */
void Application::ExecuteTasks()
{
	while (!mIsDone)
		STaskManager::Instance().Run();
}

/**
 * Function : ExecuteDeivceStuffOfTasks
 */
void Application::ExecuteDeviceStuffOfTasks()
{
	std::lock_guard<std::mutex> lock(STaskManager::Instance().DeviceMutex());
	std::queue<TaskManager::DeviceTask*>& deviceTasks = STaskManager::Instance().DeviceTasks();
	while (!deviceTasks.empty())
	{
		TaskManager::DeviceTask* task = deviceTasks.front();
		task->Execute();
		delete task;
		deviceTasks.pop();
	}
}

/**
 * Function : MouseMoveHandle
 */
void Application::MouseMoveHandle(const MouseEventParams& params)
{
	if (CurrentCamera().get())
	{
		CurrentCamera()->RotateFromMouse(-params.Diff.x, -params.Diff.y, 0.5f);
	}
}

/**
 * Function : SetMousePosition
 */
void Application::SetMousePosition(const glm::ivec2& pos)
{
	POINT pt;
	pt.x = pos.x;
	pt.y = pos.y;

	ClientToScreen(mWindow.GetHwnd(), &pt);
	SetCursorPos(pt.x, pt.y);
}

/**
 * Function : MouseWheelHandler
 */
void Application::MouseWheelHandler(float delta)
{
	if (CurrentCamera().get())
	{
		CurrentCamera()->Move(Camera::MoveType::Forward, delta * 2.0f);
	}
}

/**
 * Function : ImGuiWndProcHandler
 * TODO: Improve (see https://github.com/ocornut/imgui/issues/364).
 */
IMGUI_API LRESULT Application::ImGuiWndProcHandler(HWND, UINT msg, WPARAM wParam, LPARAM lParam)
{
	ImGuiIO& io = ImGui::GetIO();

	switch (msg)
	{
	case WM_LBUTTONDOWN:
		io.MouseDown[0] = true;
		break;

	case WM_LBUTTONUP:
		io.MouseDown[0] = false;
		break;

	case WM_RBUTTONDOWN:
		io.MouseDown[1] = true;
		break;

	case WM_RBUTTONUP:
		io.MouseDown[1] = false;
		break;

	case WM_MBUTTONDOWN:
		io.MouseDown[2] = true;
		break;

	case WM_MBUTTONUP:
		io.MouseDown[2] = false;
		break;

	case WM_MOUSEWHEEL:
		io.MouseWheel += GET_WHEEL_DELTA_WPARAM(wParam) > 0 ? +1.0f : -1.0f;
		break;

	case WM_MOUSEMOVE:
		io.MousePos.x = (signed short)(lParam);
		io.MousePos.y = (signed short)(lParam >> 16);
		break;

	case WM_KEYDOWN:
		if (wParam < 256)
			io.KeysDown[wParam] = 1;
		break;

	case WM_KEYUP:
		if (wParam < 256)
			io.KeysDown[wParam] = 0;
		break;

	case WM_CHAR:
		// You can also use ToAscii()+GetKeyboardState() to retrieve characters.
		if (wParam > 0 && wParam < 0x10000)
			io.AddInputCharacter((unsigned short)wParam);
		break;
	}

	bool directInputToImgui = GraphicsCore::rp()->GUI()->FindHoveredWindow(&io.MousePos, false);

	return directInputToImgui;
}

/**
 * Function : SetScreenShotsFolder
 */
void Application::SetScreenShotsFolder(const String& folder)
{
	mScreenShotsFolder = folder;
}

/**
 * Function : GetScreenShotName
 */
String Application::GetScreenShotName(const ImageFormat format)
{
	String result(__TEXT("/PracticesScreenShot_"));
	String extension = GetExtension(format);
	int index = 0;

	String startFolderRegExp = mScreenShotsFolder + __TEXT("/PracticesScreenShot_*") + extension;

	std::vector<FindData> files;
	SFileManager::Instance().FindFilesInFolder(startFolderRegExp, files);

	for (auto& file : files)
	{
		String test(file.FileName());
		size_t posUnderline = test.find_last_of(__TEXT('_'));
		size_t posDot = test.find_last_of(__TEXT('.'));

		String strIndex = test.substr(posUnderline + 1, posDot - posUnderline - 1);

		int testIndex = std::stoi(strIndex);
		index = std::max(index, testIndex + 1);
	}

	return result + ToString(index) + extension;
}

/**
 * Function : SaveScreenInFile
 */
void Application::SaveScreenInFile(const String& path, const ImageFormat format)
{
	GraphicsCore::rs().renderer()->DumpRenderTargetToFile(path, format);
	mMakeScreenshot = false;
}

/**
 * Function : ImGuiPlaceWidgets
 */
void Application::ImGuiPlaceWidgets()
{
	bool show = true;

	GraphicsCore::rp()->GUI()->NewFrame();

	RenderMainMenu();
}

/**
 * Function : RenderMainMenu
 */
void Application::RenderMainMenu()
{
	if (ImGui::BeginMainMenuBar())
	{
		MenuExamples();
		MenuSettings();
		MenuHelp();

		ImGui::EndMainMenuBar();
	}

	ShowExampleSettings();
	ShowFPS();
	ShowShaderEditor();

	if (mShowCameraSettingsInSeparateWindow)
	{
		ImGui::Begin("Camera settings");
		CameraSettings();
		ImGui::End();
	}

	if (mShowViewFrustumSettingsInSeparateWindow)
	{
		ImGui::Begin("View frustum settings");
		ViewFrustumSettings();
		ImGui::End();
	}
}

/**
 * Function : MenuExamples
 */
void Application::MenuExamples()
{
	if (ImGui::BeginMenu("File"))
	{
		bool isEnabled = CurrentSample()->IsAllowLoading();
		bool isSelected = false;

		if (ImGui::MenuItem("Open", "Ctrl+O", &isSelected, isEnabled))
		{
			//
		}

		ImGui::EndMenu();
	}

	if (ImGui::BeginMenu("Examples"))
	{
		int currentSample = ToInt(mSample);
		if (ImGui::BeginMenu("Simple"))
		{
			for (const auto& sample : mSamples)
			{
				if (sample.first > AppSample::ExtendedSamplesBegin)
					continue;

				std::string name = StringUtils::Converter::WStringToString(sample.second->Name());
				ImGui::RadioButton(name.c_str(), &currentSample, static_cast<int>(sample.first));
			}

			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Extended"))
		{
			for (const auto& sample : mSamples)
			{
				if (sample.first < AppSample::ExtendedSamplesBegin)
					continue;

				std::string name = StringUtils::Converter::WStringToString(sample.second->Name());
				ImGui::RadioButton(name.c_str(), &currentSample, static_cast<int>(sample.first));
			}
			ImGui::EndMenu();
		}
		ImGui::EndMenu();

		if (currentSample != ToInt(mSample))
			SetSample(AppSample(currentSample));
	}
}

/**
 * Function : MenuSettings
 */
void Application::MenuSettings()
{
	if (ImGui::BeginMenu("Settings"))
	{
		ImGui::MenuItem("Show settings", NULL, &mShowExampleSettings);
		ImGui::Separator();
		ShowCameraSettings();
		ShowViewFrustumSettings();
		ImGui::EndMenu();
	}
}

/**
 * Function : MenuHelp
 */
void Application::MenuHelp()
{
	if (ImGui::BeginMenu("Help"))
	{
		ImGui::MenuItem("Show FPS", nullptr, &mShowFPS);
		ImGui::MenuItem("Make screenshot", nullptr, &mMakeScreenshot);

		if (ImGui::MenuItem("Open shader editor", nullptr, &mIsOpenedShaderEditor, mPSOEditor->CheckMode(CurrentSample()->Name())))
			ToggleShaderEditor();

		ImGui::EndMenu();
	}
}

/**
 * Function : ShowExampleSettings
 */
void Application::ShowExampleSettings()
{
	if (mShowExampleSettings)
	{
		ImGui::Begin("Settings", &mShowExampleSettings, ImGuiWindowFlags_AlwaysAutoResize);

		mSamples[mSample]->RenderImGui();

		ImGui::End();
	}
}

/**
 * Function : ShowFPS
 */
void Application::ShowFPS()
{
	if (mShowFPS)
	{
		ImGui::Begin("Current FPS:", &mShowFPS);
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::End();
	}
}

/**
 * Function : ShowCameraSettings
 */
void Application::ShowCameraSettings()
{
	if (mShowCameraSettingsInSeparateWindow)
		return;

	bool isEnabled = (CurrentCamera().get() != nullptr);

	if (ImGui::BeginMenu("Camera", isEnabled))
	{
		CameraSettings();

		ImGui::EndMenu();
	}
}

/**
 * Function : ShowViewFrustumSettings
 */
void Application::ShowViewFrustumSettings()
{
	if (mShowViewFrustumSettingsInSeparateWindow)
		return;

	if (ImGui::BeginMenu("View frustum"))
	{
		ViewFrustumSettings();

		ImGui::EndMenu();
	}
}

/**
 * Function : CameraSettings
 */
void Application::CameraSettings()
{
	CameraPtr& currentCamera = CurrentCamera();
	DirectX::XMFLOAT2&			angles		= currentCamera->Angles();
	const DirectX::XMFLOAT2&	angleXRange	= currentCamera->AngleXRange();
	const DirectX::XMFLOAT2&	angleYRange	= currentCamera->AngleYRange();

	{
		std::string items;
		for (int id = 0; id < mActiveCameras.size(); ++id)
		{
			CameraPtr& camera = mActiveCameras[id];

			items += camera->Name();
			items += '\0';
		}

		if (ImGui::Combo("Cameras", &mCurrentCameraIdx, items.data()))
			SetCurrentCamera(CurrentCamera());
	}

	ImGui::Separator();

	if (ImGui::InputFloat3("Camera pos", &currentCamera->Position().x))
		currentCamera->MarkDirty();

	if (ImGui::SliderFloat("Horizontal angle", &angles.x, angleXRange.x, angleXRange.y))
		currentCamera->MarkDirty();

	if (ImGui::SliderFloat("Vertical angle", &angles.y, angleYRange.x, angleYRange.y))
		currentCamera->MarkDirty();

	if (ImGui::SliderFloat("FOV", &currentCamera->FoV(), 10.0f, 90.0f))
		currentCamera->MarkDirty();

	if (ImGui::SliderFloat("zNear", &currentCamera->ZRange().x, 0.1f, currentCamera->ZRange().y))
		currentCamera->MarkDirty();

	if (ImGui::SliderFloat("zFar", &currentCamera->ZRange().y, currentCamera->ZRange().x + 0.1f, 1000.0f))
		currentCamera->MarkDirty();

	ImGui::Checkbox("Show in a separate window.", &mShowCameraSettingsInSeparateWindow);
}

/**
 * Function : ViewFrustumSettings
 */
void Application::ViewFrustumSettings()
{
	{
		std::string items;
		for (size_t i = 0; i < mViewFrustums.size(); ++i)
		{
			ViewFrustumPtr& frustum = mViewFrustums[i];

			items += "View frustum of " + frustum->FrustumCamera()->Name();
			items += '\0';
		}

		ImGui::Combo("View frustums", &mCurrentViewFrustumIdx, items.data());
	}

	if (mViewFrustums.size() == 0)
		return;

	ImGui::Checkbox("Show the view frustum", &mShowViewFrustum);
	if (mShowViewFrustum)
	{
		if (ImGui::Checkbox("Show all parts", &mShowAllPartsOfViewFrustum))
		{
			if (mShowAllPartsOfViewFrustum)
				mPartOfViewFrustum = -1;
			else
				mPartOfViewFrustum = 0;
		}

		ViewFrustumPtr& currentViewFrustum = mViewFrustums[mCurrentViewFrustumIdx];
		if (!mShowAllPartsOfViewFrustum)
		{
			for (int i = 0; i < currentViewFrustum->NbOfSplit(); ++i)
			{
				std::string label("Split ");
				label += std::to_string(i);
				ImGui::RadioButton(label.c_str(), &mPartOfViewFrustum, i);
			}
		}
	}
}

/**
 * Function : SetCurrentCamera
 */
void Application::SetCurrentCamera(CameraPtr& camera)
{
	static const int InvalidIdx = -1;

	mCurrentCameraIdx = InvalidIdx;
	for (int id = 0; id < mActiveCameras.size(); ++id)
	{
		if (mActiveCameras[id] == camera)
		{
			mCurrentCameraIdx = id;
			break;
		}
	}

	if (mCurrentCameraIdx == InvalidIdx)
		assert(false);

	for (auto& frustum : mViewFrustums)
		frustum->ChangeViewCamera(CurrentCamera().get());
}

/**
 * Function : ToggleShaderEditor
 */
void Application::ToggleShaderEditor()
{

}

/**
 * Function : ShowShaderEditor
 */
void Application::ShowShaderEditor()
{
	if (!mIsOpenedShaderEditor)
		return;

	ImGui::Begin("Shader editor", nullptr, ImVec2(600, 300), -1.0f, ImGuiWindowFlags_AlwaysVerticalScrollbar);

	// left
	ImGui::BeginChild("left pane", ImVec2(150, 0), true);
	{
		for (auto& registeredPSO : mPSOEditor->PSOs(CurrentSample()->Name()))
		{
		#ifdef UNICODE
			std::string name = StringUtils::Converter::WStringToString(registeredPSO.Name);
		#else
			std::string& name = registeredShaders.first;
		#endif
			if (ImGui::TreeNodeEx(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
			{
				mPSOEditor->SelectedPSO() = &registeredPSO;
				for (auto& shaderInfo : registeredPSO.Shaders)
				{
					if (shaderInfo.Type == RHIShaderType::Unknown)
						continue;

				#ifdef UNICODE
					std::string asciiShaderName = StringUtils::Converter::WStringToString(shaderInfo.Name);
				#else
					std::string asciiShaderName& = shaderName;
				#endif

					if (ImGui::Selectable(asciiShaderName.c_str()))
					{
						mPSOEditor->LoadShader(GraphicsCore::rs().renderer()->ShaderFolder() + shaderInfo.Hierarchy);
						mPSOEditor->SelectedShader() = &shaderInfo;
					}
				}
				ImGui::TreePop();
			}
		}
	}
	ImGui::EndChild();

	ImGui::SameLine();

	ImGui::BeginGroup();
	{
		static int selected = 0;
		ImGui::BeginChild("Shader view", ImVec2(0, -ImGui::GetItemsLineHeightWithSpacing())); // Leave room for 1 line below us
		{
			ImGuiInputTextFlags flags = ImGuiInputTextFlags_AllowTabInput;

			if (!mPSOEditor->IsReady())
				flags |= ImGuiInputTextFlags_ReadOnly;

			ImGui::InputTextMultiline("Shader", mPSOEditor->Buffer(), mPSOEditor->BufferSize(), ImVec2(600, 300), flags);
		}
		ImGui::EndChild();

		ImGui::BeginChild("buttons");
		{
			if (ImGui::Button("Rebuild"))
				mPSOEditor->RebuildShader(mPSOEditor->SelectedPSO(), mPSOEditor->SelectedShader());

			ImGui::SameLine();
			if (ImGui::Button("Save") && mPSOEditor->SelectedShader())
					mPSOEditor->SaveShader(GraphicsCore::rs().renderer()->ShaderFolder() + mPSOEditor->SelectedShader()->Name);

			ImGui::SameLine();
			if (ImGui::Button("Resize buffer (x2 memory)")) {}
		}
		ImGui::EndChild();
	}
	ImGui::EndGroup();

	ImGui::End();
}
