#pragma once

#include <chrono>
#include <map>
#include <memory>
#include <stdlib.h>
#include <string>
#include <unordered_map>
#include <random>

#include <imgui.h>

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windowsx.h>
#include <windows.h>
#include <commdlg.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#ifdef _WIN32
#include <DirectXTex.h>
#include "d3dx12.h"

#define ENABLE_PIX_EVENTS
#ifndef ENABLE_PIX_EVENTS
#define PIXBeginEvent(x, y, z) __noop();
#define PIXEndEvent(x) __noop();
#define PIXSetMarker(x, y, z) __noop();
#else
#include <pix3.h>
#endif

#endif
