#pragma once
#include "StdafxSamples.h"
#include <HeightMapGenerator.h>
#include <InputLayouts.h>
#include <RHI.h>
#include <RHIIndexBuffer.h>
#include <RHIVertexBuffer.h>
#include <TaskManager.h>
#include <TerrainBuilder.h>

namespace BuildTasks
{
	class HeightMapDeviceTask : public TaskManager::DeviceTask
	{
	public:
		HeightMapDeviceTask(RHIResource** renderTexture, RHIResource** buildTexture,
			RHIGPUDescriptorHandle* gpuRenderTextureDescriptor, RHIGPUDescriptorHandle* gpuBuildTextureDescriptor,
			HeightMapGenerator* generator, const DirectX::XMUINT2& size)
			: RenderTexture(renderTexture), BuildTexture(buildTexture),
			GPURenderTextureDescriptor(gpuRenderTextureDescriptor), GPUBuildTextureDescriptor(gpuBuildTextureDescriptor),
			Generator(generator), Size(size) { }

		void Execute() override final
		{
			RHIBox box	= {};
			box.left	= 0;
			box.top		= 0;
			box.right	= Size.x;
			box.bottom	= Size.y;
			box.front	= 0u;
			box.back	= 1u;

			RHISubresourceData subresourceData	= {};
			subresourceData.Data				= Generator->Result();
			subresourceData.RowPitch			= Size.x * Generator->SizeOfType();
			subresourceData.SlicePitch			= Size.y * subresourceData.RowPitch;

			(*BuildTexture)->Map(0, nullptr, nullptr);
			(*BuildTexture)->WriteToSubresource(0, &box, subresourceData.Data, subresourceData.RowPitch, subresourceData.SlicePitch);
			(*BuildTexture)->Unmap(0, nullptr);

			std::swap(RenderTexture, BuildTexture);
			std::swap(*GPURenderTextureDescriptor, *GPUBuildTextureDescriptor);
		}

	protected:
		RHIResource**			RenderTexture;
		RHIResource**			BuildTexture;
		RHIGPUDescriptorHandle*	GPURenderTextureDescriptor;
		RHIGPUDescriptorHandle*	GPUBuildTextureDescriptor;
		HeightMapGenerator*		Generator;
		DirectX::XMUINT2		Size;
	};

	class HeightMapTask : public TaskManager::Task
	{
	public:
		HeightMapTask(RHIResource** renderTexture, RHIResource** buildTexture,
			RHIGPUDescriptorHandle* gpuRenderTextureDescriptor, RHIGPUDescriptorHandle* gpuBuildTextureDescriptor,
			HeightMapGenerator* generator, const DirectX::XMUINT2& size, ProgressIndicator* progress)
			: RenderTexture(renderTexture), BuildTexture(buildTexture),
			GPURenderTextureDescriptor(gpuRenderTextureDescriptor), GPUBuildTextureDescriptor(gpuBuildTextureDescriptor),
			Generator(generator), Size(size), Progress(progress) { }

		void Execute() override final
		{
			(*Generator)(Size, Progress);
		}

		TaskManager::DeviceTask* SpawnDeviceStuffTask() override final
		{
			bool isSpawnDevice = RenderTexture && BuildTexture && GPURenderTextureDescriptor && GPUBuildTextureDescriptor;

			return isSpawnDevice ? new HeightMapDeviceTask(RenderTexture, BuildTexture, GPURenderTextureDescriptor, GPUBuildTextureDescriptor, Generator, Size) : nullptr;
		}

	protected:
		RHIResource**			RenderTexture;
		RHIResource**			BuildTexture;
		RHIGPUDescriptorHandle*	GPURenderTextureDescriptor;
		RHIGPUDescriptorHandle*	GPUBuildTextureDescriptor;
		HeightMapGenerator*		Generator;
		DirectX::XMUINT2		Size;
		ProgressIndicator*		Progress;
	};

	class TerrainDeviceTask : public TaskManager::DeviceTask
	{
	public:
		TerrainDeviceTask(RHIVertexBuffer* vertexBuffer, RHIIndexBuffer* indexBuffer,
			const void* vertices, const size_t verticesSize,
			const void* indices, const size_t indicesSize,
			const uint32_t indexCount)
			: VertexBuffer(vertexBuffer), IndexBuffer(indexBuffer),
			Vertices(vertices), VerticesSize(verticesSize), Indices(indices), IndicesSize(indicesSize), IndexCount(indexCount) { }

		void Execute() override final;

	protected:
		const void*				Vertices;
		UINT					VerticesSize;
		const void*				Indices;
		UINT					IndicesSize;
		RHIVertexBuffer*		VertexBuffer;
		RHIIndexBuffer*			IndexBuffer;
		uint32_t				IndexCount;
	};

	class TerrainTask : public TaskManager::Task
	{
		using Vertex = InputLayouts::Main::Vertex;

	public:
		TerrainTask(RHIVertexBuffer* vertexBuffer, RHIIndexBuffer* indexBuffer,
			TerrainBuilder<Vertex>* builder, const HeightMap& map, const DirectX::XMFLOAT2& size, const DirectX::XMFLOAT2& range, ProgressIndicator* progress)
			: VertexBuffer(vertexBuffer), IndexBuffer(indexBuffer),
			Builder(builder), Map(map), Size(size), HeightRange(range), Progress(progress) { }

		void Execute() override final
		{
			Builder->Build(Map, Size, HeightRange, Progress);
		}

		TaskManager::DeviceTask* SpawnDeviceStuffTask() override final
		{
			SLogger::Instance().AddMessage(LogMessageType::Debug, __TEXT("V(%u), I(%u), %u"), Builder->NbOfVertices(), Builder->NbOfIndices());
			return new TerrainDeviceTask(VertexBuffer, IndexBuffer,
				Builder->Vertices(), Builder->VerticesSizeInBytes(), Builder->Indices(), Builder->IndicesSizeInBytes(), Builder->NbOfIndices());
		}
	protected:
		RHIVertexBuffer*		VertexBuffer;
		RHIIndexBuffer*			IndexBuffer;

		TerrainBuilder<Vertex>*	Builder;

		const HeightMap&		Map;
		DirectX::XMFLOAT2		Size;
		DirectX::XMFLOAT2		HeightRange;
		ProgressIndicator*		Progress;
	};
}
