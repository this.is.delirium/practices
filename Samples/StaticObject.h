#pragma once
#include "StdafxSamples.h"
#include "ConstantBuffers.h"
#include <RHI.h>

class MeshComponent;
class SceneComponent;

class StaticObject
{
public:
	void Release();

	void Initialize(const bool createSceneComponent = true, const bool createMeshComponent = true);

	void Update(const float deltaTime);
	void Draw(RHICommandList* commandList, const UINT graphicsRootDescriptorTableId);

	FORCEINLINE std::shared_ptr<SceneComponent>& GetSceneComponent() { return mSceneComponent; }
	FORCEINLINE std::shared_ptr<MeshComponent>& GetMeshComponent() { return mMeshComponent; }

	FORCEINLINE void SetTransparency(const float value) { mConstBuffer.Data().Color.w = value; }
	FORCEINLINE void SetColor(const DirectX::XMFLOAT4& color) { mConstBuffer.Data().Color = color; }

protected:

	std::shared_ptr<MeshComponent>	mMeshComponent;
	std::shared_ptr<SceneComponent>	mSceneComponent;

	ConstBufferPerObject			mConstBuffer;
};
