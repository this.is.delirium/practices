#pragma once
#include "StdafxSamples.h"
#include <Camera.h>
#include <ConstantBuffers.h>
#include <InputLayouts.h>
#include <RHI.h>
#include <RHIConstBuffer.h>
#include <RHIVertexBuffer.h>

class IRenderer;

class ScreenQuadPart
{
public:
	enum class TextureType
	{
		ColorRGBA	= 0,
		Depth		= 1
	};

public:
	ScreenQuadPart(const DirectX::XMFLOAT2& sizeWindow, const DirectX::XMFLOAT2& positionQuad, const DirectX::XMFLOAT2& sizeQuad)
		: mWindowSize(sizeWindow), mPosition(positionQuad), mSize(sizeQuad)
	{
		mConstBufferTextureType.Data().Type = ToInt(TextureType::ColorRGBA);
	}

	void Release();
	void Initialize();

	FORCEINLINE RHIResource* VertexBuffer() const { return mVertexBuffer.Buffer(); }
	FORCEINLINE RHIVertexBufferView* VertexBufferView() { return mVertexBuffer.View(); }

	FORCEINLINE const ConstBufferPerFrame& GetConstBufferPerFrame() const { return mConstBufferPerFrame; }
	FORCEINLINE const ConstBufferTextureType& GetConstBufferTextureType() const { return mConstBufferTextureType; }

	// TODO: Use RHIDescriptorHandle.
	FORCEINLINE RHIGPUDescriptorHandle TextureSrvGpuHandle() const { return mTextureSrvGpuHandle; }

public:
	void SetTexture(RHIGPUDescriptorHandle srvGpuHandle, const TextureType texType);
	void UpdateDepthParams(const DirectX::XMFLOAT4& depthParams);

protected:
	void UpdateScreenQuad();
	void BuildScreenQuad(const float left, const float right, const float top, const float bottom);

protected:
	DirectX::XMFLOAT2	mWindowSize;
	DirectX::XMFLOAT2	mPosition;
	DirectX::XMFLOAT2	mSize;

protected:
	using ScreenVertex = InputLayouts::ScreenQuad::Vertex;
	std::vector<ScreenVertex>	mScreenQuad;

	std::shared_ptr<Camera>		mOrthoCamera;

	RHIVertexBuffer				mVertexBuffer;

	ConstBufferPerFrame			mConstBufferPerFrame;
	ConstBufferTextureType		mConstBufferTextureType;

	// TODO: Use RHIDescriptorHandle.
	RHIGPUDescriptorHandle		mTextureSrvGpuHandle;
};
