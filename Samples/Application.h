#pragma once
#include "StdafxSamples.h"
#include <Camera.h>
#include <IImGui.h>
#include <ImageFormat.h>
#include <Render/IRenderer.h>
#include <Render/GraphicsAPI.h>
#include <Window.h>

class Sample;
class PSOEditor;
class ViewFrustum;

enum class AppSample
{
	Unknown = -1,
	SimpeSamplesBegin = 0,
	CSWriteToTexture,
	Fractal,
	//CSG,
	HeightMap,
	ParallaxMapping,
	RenderToTexture,
	StreamOutput,
	Tesselation,
	WelcomeScreen,

	ExtendedSamplesBegin,
	DeferredShading,
	OrderIndependentTransparency,
	ParallelSplitShadowMapping,
	Particles,
	PBR,
	ShadowMapping,
	ScreenSpaceAmbientOcclusion,
	ScreenSpaceLocalReflection,
	VarianceShadowMapping,
	Count
};

struct AppDesc
{
	WindowDesc	wndDesc;
	GraphicsAPI	gAPI;
};

class Application
{
protected:
	using CameraPtr				= std::shared_ptr<Camera>;
	using ViewFrustumPtr		= std::shared_ptr<ViewFrustum>;

	using MapOfSamples			= std::unordered_map<AppSample, std::shared_ptr<Sample>>;
	using ArrayOfViewFrustum	= std::vector<ViewFrustumPtr>;

public:
	Application() { }
	~Application() { }

	void Release();
	bool Initialize(const AppDesc* desc);

	void Run();

	void Stop();

	FORCEINLINE Sample* CurrentSample() { return mSamples[mSample].get(); }

	FORCEINLINE const Window* GetWindow() const { return &mWindow; }

	FORCEINLINE GraphicsAPI GraphicAPI() const { return mCurrentGAPI; }

	FORCEINLINE std::shared_ptr<PSOEditor>& GetPSOEditor() { return mPSOEditor; }

public:
	void SetBackgroundColor(DirectX::XMFLOAT4* color) { mBGColor = color; }

public:
	void AddCamera(CameraPtr& camera) { mActiveCameras.push_back(camera); }
	void SetCurrentCamera(CameraPtr& camera);
	FORCEINLINE CameraPtr& CurrentCamera() { return mActiveCameras[mCurrentCameraIdx]; }

protected:
	FORCEINLINE void ResetActiveCameras() { mActiveCameras.clear(); }

public:
	FORCEINLINE void AddViewFrustum(ViewFrustumPtr& frustum) { mViewFrustums.push_back(frustum); }

protected:
	FORCEINLINE void ResetViewFrustums() { mViewFrustums.clear(); }

protected:
	void SetSample(const AppSample sample);

protected:
	void Update(const float deltaTime);
	void Render();
	void RenderViewFrustum();

	//! Executes tasks in another thread!
	void ExecuteTasks();
	//! Executes final part (device stuff) of tasks in the main thread!
	void ExecuteDeviceStuffOfTasks();

protected:
	void SetScreenShotsFolder(const String& folder);
	String GetScreenShotName(const ImageFormat format);
	void SaveScreenInFile(const String& file, const ImageFormat format);

protected: //! part of ImGui.
	IMGUI_API LRESULT ImGuiWndProcHandler(HWND, UINT msg, WPARAM wParam, LPARAM lParam);

	void ImGuiPlaceWidgets();

	void RenderMainMenu();
	void ShowExampleSettings();
	void ShowFPS();

	void ShowCameraSettings();
	void CameraSettings();

	void ShowViewFrustumSettings();
	void ViewFrustumSettings();

protected: //! part of main menu.
	void MenuExamples();
	void MenuSettings();
	void MenuHelp();

protected: //! part of shader editor.
	void ToggleShaderEditor();
	void ShowShaderEditor();

protected:
	void SamplesInitialize();

protected:
	void MouseMoveHandle(const MouseEventParams& params);
	void SetMousePosition(const glm::ivec2& pos);

	void MouseWheelHandler(float delta);

protected:

	std::vector<CameraPtr>		mActiveCameras;
	int							mCurrentCameraIdx;

	ArrayOfViewFrustum			mViewFrustums;
	int							mCurrentViewFrustumIdx = 0;

	DirectX::XMFLOAT4*			mBGColor;
	GraphicsAPI					mCurrentGAPI;

	Window						mWindow;
	bool						mIsWorking;
	AppSample					mSample										= AppSample::Unknown;

	MapOfSamples				mSamples;

	std::thread					mTaskThread;
	bool						mIsDone;

	std::shared_ptr<PSOEditor>	mPSOEditor;

	//! part of ImGui
	bool						mShowFPS									= true;
	bool						mShowExampleSettings						= true;
	bool						mShowCameraSettingsInSeparateWindow			= false;
	bool						mShowViewFrustumSettingsInSeparateWindow	= false;
	bool						mShowViewFrustum							= false;
	bool						mShowAllPartsOfViewFrustum					= true;
	int							mPartOfViewFrustum							= -1;
	bool						mMakeScreenshot								= false;
	bool						mIsOpenedShaderEditor						= false;

	String						mScreenShotsFolder;
};
