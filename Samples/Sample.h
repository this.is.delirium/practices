#pragma once
#include "StdafxSamples.h"
#include <StdafxCore.h>

#include "ConstantBuffers.h"
#include "InputLayouts.h"
#include <Render/IRenderer.h>
#include <RHIConstBuffer.h>
#include <ScreenQuadPart.h>

class Application;
class Camera;
class RHICommandList;
class ViewFrustum;
class Window;

class Sample
{
public:
	Sample(Application* app, const String& name, const DirectX::XMFLOAT4& bgColor, const bool useScreenQuad = false, const DirectX::XMFLOAT2& quadSize = DirectX::XMFLOAT2(0.0f, 0.0f));

	virtual void Release() = 0;

	virtual void Initialize() = 0;

	virtual void RenderImGui() = 0;

	virtual void Update(const float deltaTime) = 0;
	virtual void Render(RHICommandList* commandList) = 0;

	const String& Name() const { return mName; }

	virtual bool IsAllowLoading() const { return false; }
	virtual bool IsUseOwnRendering() const { return false; }

	virtual void Activate() { Initialize(); }
	virtual void Deactivate() { Release(); }

public:
	const RHIViewport* Viewport() const { return &mViewport; }
	const RHIRect* ScissorRect() const { return &mScissorRect; }

protected:
	virtual void InitializeRenderingResources() = 0;
	virtual void CreateRootSignatures() = 0;
	virtual void CreatePipelineStates() = 0;

	virtual void SetCameras();

	void InitializeScreenQuadPart();

	void CreateConstantBuffersForMatrix(RHIDescriptorHeap* cbvHeap, const bool perFrame, const bool perObject);

	void CreateDefaultCamera(const float aspectRatio);

	void UpdateCameraResource();

protected:
	virtual void DrawScreenQuad(RHICommandList* commandList, RHIDescriptorHeap** heaps, const UINT countHeaps);

protected:
	DirectX::XMFLOAT4		mBGColor;
	String					mName;
	Application*			mApp					= nullptr;
	RHIRootSignature*		mRootSignature			= nullptr;
	RHIPipelineState*		mPSO					= nullptr;

	// Camera settings.
	ConstBufferPerFrame		mConstBufferPerFrame;
	ConstBufferPerObject	mConstBufferPerObject;

	std::shared_ptr<Camera>	mCamera;

	RHIViewport				mViewport;
	RHIRect					mScissorRect;

	bool					mUseScreenQuad			= false;
	ScreenQuadPart*			mScreenQuad				= nullptr;
	DirectX::XMFLOAT2		mScreenQuadSize;
};
