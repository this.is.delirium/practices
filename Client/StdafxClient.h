#pragma once

#include <atomic>
#include <chrono>
#include <filesystem>
#include <map>
#include <memory>
#include <mutex>
#include <stdlib.h>
#include <string>
#include <unordered_map>
#include <random>
#include <imgui.h>

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windowsx.h>
#include <windows.h>
#include <commdlg.h>

#ifdef _WIN32
#include <d3d12.h>
#include <DirectXTex.h>
//#include "d3dx12.h"

//#define ENABLE_PIX_EVENTS
#ifndef ENABLE_PIX_EVENTS
#define PIXBeginEvent(x, y, z) __noop();
#define PIXEndEvent(x) __noop();
#define PIXSetMarker(x, y, z) __noop();
#else
#include <pix3.h>
#endif

#include <rttr/rttr_enable.h>
#include <rttr/type.h>
#include <rttr/registration.h>
#include <rttr/registration_friend.h>


#ifdef UNICODE
#define WIN_MAIN wWinMain
using CmdLine = LPWSTR;
#else
#define WIN_MAIN WinMain
using CmdLine = LPSTR;
#endif // UNICODE

#endif // _WIN32
