#include "StdafxClient.h"

#include "Application.h"
#include <FileManager.h>
#include <ImGuiHelper.h>
#include <InputLayouts.h>
#include <Logger.h>
#include <Render/Pass/GeometryPass.h>
#include <Render/GraphicsCore.h>
#include <Render/Material/MaterialHelper.h>
#include <Render/Material/Predefined/DefaultMaterial.h>
#include <Render/Mode/OpaqueMode.h>
#include <RenderSurface.h>
#include <ResourceManager.h>
#include <RootSignature.h>
#include <Scene/SceneObject.h>

#include <StaticGeometryBuilder.h>
#include <StringUtils.h>
#include <TaskManager.h>
#include <TextureManager.h>
#include <ViewFrustum.h>

#include <D3D12/D3D12Renderer.h>
#include <Render/ImGuiDirectX12.h>

#ifndef NoOpenGL
#include <OpenGL/OpenGLRenderer.h>
#include <ImGuiOpenGL.h>
#endif

using namespace GraphicsCore;

/**
 * Function : Release
 */
void Application::Release()
{
	RenderSystem::Instance().Release();

	mWindow.Release();

	SLogger::Instance().FlushToDisk();
}

/**
 * Function : ParseCommandLine
 */
void Application::ParseCommandLine(CmdLine cmdLine, Settings& settings)
{
	String line(cmdLine);
	std::vector<String> tokens;
	StringUtils::Split(line, __TEXT(" "), tokens);

	SResourceManager::Instance().FindRootFolder();

	// Setup default values.
	settings.ScreenShotsFolder	= __TEXT("screenshots");
	settings.LogFile			= __TEXT("log.txt");
	settings.DataFolder			= SResourceManager::Instance().RootFolder();
	settings.WorldsFile			= __TEXT("worlds.json");

	for (size_t i = 0; i < tokens.size(); ++i)
	{
		if (tokens[i] == __TEXT("-screenshotsfolder"))
		{
			settings.ScreenShotsFolder = tokens[++i];
		}
		else if (tokens[i] == __TEXT("-logfile"))
		{
			settings.LogFile = tokens[++i];
		}
		else if (tokens[i] == __TEXT("-worlds"))
		{
			settings.WorldsFile = tokens[++i];
		}
		else if (tokens[i] == __TEXT("-data"))
		{
			settings.DataFolder = tokens[++i];
		}
	}
}

/**
 * Function : Initialize
 */
bool Application::Initialize(const AppDesc* desc)
{
	bool result = true;

	//-- Parse command line.
	ParseCommandLine(desc->cmdLine, mSettings);

	//-- Setup systems.
	SLogger::Instance().SetOutputFile(mSettings.LogFile);
	SResourceManager::Instance().SetRootFolder(mSettings.DataFolder);
	SetScreenShotsFolder(mSettings.ScreenShotsFolder);

	//-- Initialize window.
	result &= mWindow.Initialize(&desc->wndDesc);

	//-- Initialize render system.
	{
		RenderSystem::RSDesc rsDesc = {};
		rsDesc.gAPI		= desc->gAPI;
		rsDesc.window	= &mWindow;

		RenderSystem::Instance().Initialize(rsDesc);
	}

	//-- Load worlds.
	LoadWorlds(mSettings.WorldsFile);

	//-- Initialize rendering resources.
	{
		InitializeRenderingResources();

		CreateRootSignatures();
		CreatePipelineStates();
	}

	//-- Test.
	{
		mFloor = std::make_shared<SceneObject>("SceneObject");
		StaticGeometryBuilder::LoadPlane(*mFloor);

		//-- Looks ugly. ToDo: Reconsider later.
		mFloor->GetLocalTransform().Scale()			= DirectX::XMFLOAT3(5.0f, 5.0f, 5.0f);
		mFloor->GetLocalTransform().Translation()	= DirectX::XMFLOAT3(7.0f, 0.0f, 0.0f);
		mFloor->GetLocalTransform().WorldMatrix(mFloor->GetTransforms().WorldMatrix);
		mFloor->GetConstBuffer().UpdateDataOnGPU();

		auto& material = mFloor->GetMaterial();
		material = Materials::Helper::CreateMaterial("default");

		auto* defaultMat = reinterpret_cast<Materials::Predefined::DefaultMaterial*>(material.get());

		defaultMat->Albedo()	= TextureManager::Instance().GetTexture(SResourceManager::Instance().Find(__TEXT("Textures:Test:NM_bricks_diffuse.png")));
		defaultMat->Normal()	= TextureManager::Instance().GetTexture(SResourceManager::Instance().Find(__TEXT("Textures:Test:NM_bricks_normals.png")));
		defaultMat->Specular()	= nullptr;
		defaultMat->Gloss()		= nullptr;

		{
			using namespace Pass::GeometryPass;
			//material->SetSrvPerObject(ToUnderType(GeometryPassRootSignatureParams::PerObject), obj->GetConstBuffer().SrvHandle().Gpu());
			material->SetSrvPerObject(ToUnderType(RootSignatureParams::PerObject), mFloor->GetConstBuffer().SrvHandle().Gpu());

			RHIGPUDescriptorHandle dummyHandle;
			material->SetSrvPerObject(ToUnderType(RootSignatureParams::AlbedoTexture), defaultMat->Albedo() ? defaultMat->Albedo()->SrvHandle().Gpu() : dummyHandle);
			material->SetSrvPerObject(ToUnderType(RootSignatureParams::NormalTexture), defaultMat->Normal() ? defaultMat->Normal()->SrvHandle().Gpu() : dummyHandle);
			material->SetSrvPerObject(ToUnderType(RootSignatureParams::SpecularTexture), defaultMat->Specular() ? defaultMat->Specular()->SrvHandle().Gpu() : dummyHandle);
		}

		mFloor->GetMaterial()->SetShader(GraphicsCore::rs().shaderManager().GetShader("GeometryPass"));

		GetActiveWorld()->GetScene().AddObject<OpaqueMode>(mFloor);
	}

	//-- Setup handlers.
	mWindow.mOnClose = std::bind(&Application::Stop, this);
	mWindow.SetOnGuiHandler(this, &Application::ImGuiWndProcHandler);
	mWindow.SetOnMouseMoveHandler(this, &Application::MouseMoveHandle);
	mWindow.SetOnMouseWheel(this, &Application::MouseWheelHandler);

	SetMousePosition(DirectX::XMINT2(mWindow.Width() / 2, mWindow.Height() / 2));

	//ImGuiHelper::Instance().Register

	return result;
}

/**
 * Function : Stop
 */
void Application::Stop()
{
	mIsWorking = false;
}

/**
 * Function : Run
 */
void Application::Run()
{
	mIsWorking = true;

	auto lastTime = std::chrono::high_resolution_clock::now();
	while (mIsWorking)
	{
		mWindow.PeekMessages();

		auto currentTime = std::chrono::high_resolution_clock::now();
		float elapsedTime = std::chrono::duration_cast<std::chrono::duration<float>>(currentTime - lastTime).count();
		if (elapsedTime > 1.0f / 60.0f)
		{
			lastTime = currentTime;

			ImGuiPlaceWidgets();

			Update(elapsedTime);
			Render();
		}
	}
}

/**
 * Function : Update
 */
void Application::Update(const float deltaTime)
{
	// ToDo: Tick all worlds?
	GetActiveWorld()->GetScene().Tick(deltaTime);

	if (CurrentCamera())
	{
		const float moveValue = deltaTime * 5.0f;
		if (GetAsyncKeyState('W'))
		{
			CurrentCamera()->Move(Camera::MoveType::Forward, moveValue);
		}
		else if (GetAsyncKeyState('S'))
		{
			CurrentCamera()->Move(Camera::MoveType::Forward, -moveValue);
		}
		else if (GetAsyncKeyState('D'))
		{
			CurrentCamera()->Move(Camera::MoveType::Right, moveValue);
		}
		else if (GetAsyncKeyState('A'))
		{
			CurrentCamera()->Move(Camera::MoveType::Right, -moveValue);
		}
	}

	//mFloor->GetLocalTransform().NormalMatrix(mFloor->GetTransforms().NormalMatrix);
	mFloor->GetTransforms().NormalMatrix = DirectX::XMMatrixIdentity();
	mFloor->GetConstBuffer().UpdateDataOnGPU();
}

/**
 * Function : Render
 */
void Application::Render()
{
	GraphicsCore::rs().renderGraph().Evaluate(*GetActiveWorld());

	// ToDo: Reconsider later.
	GraphicsCore::gRenderer->Present();

	//-- ToDo: Reconsider later.
	if (mImGuiSettings.MakeScreenshot)
	{
		ImageFormat format	= ImageFormat::WicJpeg;
		String path			= mScreenShotsFolder + GetScreenShotName(format);
		SaveScreenInFile(path, format);
	}
}

/**
 * Function : MouseMoveHandle
 */
void Application::MouseMoveHandle(const MouseEventParams& params)
{
	if (CurrentCamera().get())
	{
		CurrentCamera()->RotateFromMouse(-params.Diff.x, -params.Diff.y, 0.5f);
	}
}

/**
 * Function : SetMousePosition
 */
void Application::SetMousePosition(const DirectX::XMINT2& pos)
{
	POINT pt;
	pt.x = pos.x;
	pt.y = pos.y;

	ClientToScreen(mWindow.GetHwnd(), &pt);
	SetCursorPos(pt.x, pt.y);
}

/**
 * Function : MouseWheelHandler
 */
void Application::MouseWheelHandler(float delta)
{
	if (CurrentCamera().get())
	{
		CurrentCamera()->Move(Camera::MoveType::Forward, delta);
	}
}

/**
 * Function : ImGuiWndProcHandler
 * TODO: Improve (see https://github.com/ocornut/imgui/issues/364).
 */
IMGUI_API LRESULT Application::ImGuiWndProcHandler(HWND, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (ImGui::GetCurrentContext() == NULL)
		return 0;

	ImGuiIO& io = ImGui::GetIO();
	switch (msg)
	{
	case WM_LBUTTONDOWN: case WM_LBUTTONDBLCLK:
	case WM_RBUTTONDOWN: case WM_RBUTTONDBLCLK:
	case WM_MBUTTONDOWN: case WM_MBUTTONDBLCLK:
	{
		int button = 0;
		if (msg == WM_LBUTTONDOWN || msg == WM_LBUTTONDBLCLK) button = 0;
		if (msg == WM_RBUTTONDOWN || msg == WM_RBUTTONDBLCLK) button = 1;
		if (msg == WM_MBUTTONDOWN || msg == WM_MBUTTONDBLCLK) button = 2;
		if (!ImGui::IsAnyMouseDown() && ::GetCapture() == NULL)
			::SetCapture(GetWindow()->GetHwnd());
		io.MouseDown[button] = true;
		return 0;
	}
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MBUTTONUP:
	{
		int button = 0;
		if (msg == WM_LBUTTONUP) button = 0;
		if (msg == WM_RBUTTONUP) button = 1;
		if (msg == WM_MBUTTONUP) button = 2;
		io.MouseDown[button] = false;
		if (!ImGui::IsAnyMouseDown() && ::GetCapture() == GetWindow()->GetHwnd())
			::ReleaseCapture();
		return 0;
	}
	case WM_MOUSEWHEEL:
		io.MouseWheel += GET_WHEEL_DELTA_WPARAM(wParam) > 0 ? +1.0f : -1.0f;
		return 0;
	case WM_MOUSEHWHEEL:
		io.MouseWheelH += GET_WHEEL_DELTA_WPARAM(wParam) > 0 ? +1.0f : -1.0f;
		return 0;
	case WM_MOUSEMOVE:
		io.MousePos.x = (signed short)(lParam);
		io.MousePos.y = (signed short)(lParam >> 16);
		return 0;
	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
		if (wParam < 256)
			io.KeysDown[wParam] = 1;
		return 0;
	case WM_KEYUP:
	case WM_SYSKEYUP:
		if (wParam < 256)
			io.KeysDown[wParam] = 0;
		return 0;
	case WM_CHAR:
		// You can also use ToAscii()+GetKeyboardState() to retrieve characters.
		if (wParam > 0 && wParam < 0x10000)
			io.AddInputCharacter((unsigned short)wParam);
		return 0;
	case WM_SETCURSOR:
		if (LOWORD(lParam) == HTCLIENT && rp()->GUI()->UpdateMouseCursor())
			return 1;
		return 0;
	}
	return 0;

	//bool directInputToImgui = mImGui->FindHoveredWindow(&io.MousePos, false);
	bool directInputToImgui = GraphicsCore::rp()->GUI()->FindHoveredWindow(&io.MousePos, false);

	return directInputToImgui;
}

/**
 * Function : SetScreenShotsFolder
 */
void Application::SetScreenShotsFolder(const String& folder)
{
	mScreenShotsFolder = folder;

	if (!SFileManager::Instance().IsExist(folder))
	{
		SFileManager::Instance().CreateFolder(folder);
	}
}

/**
 * Function : GetScreenShotName
 */
String Application::GetScreenShotName(const ImageFormat format)
{
	String result(__TEXT("/PracticesScreenShot_"));
	String extension = GetExtension(format);
	int index = 0;

	String startFolderRegExp = mScreenShotsFolder + __TEXT("/PracticesScreenShot_*") + extension;

	std::vector<FindData> files;
	SFileManager::Instance().FindFilesInFolder(startFolderRegExp, files);

	for (auto& file : files)
	{
		String test(file.FileName());
		size_t posUnderline = test.find_last_of(__TEXT('_'));
		size_t posDot = test.find_last_of(__TEXT('.'));

		String strIndex = test.substr(posUnderline + 1, posDot - posUnderline - 1);

		int testIndex = std::stoi(strIndex);
		index = std::max(index, testIndex + 1);
	}

	return result + ToString(index) + extension;
}

/**
 * Function : SaveScreenInFile
 */
void Application::SaveScreenInFile(const String& path, const ImageFormat format)
{
	// ToDo: Restore
	//mRenderer->DumpRenderTargetToFile(path, format);
	mImGuiSettings.MakeScreenshot = false;
}

/**
 * Function : ImGuiPlaceWidgets
 */
void Application::ImGuiPlaceWidgets()
{
	GraphicsCore::rp()->GUI()->NewFrame();

	RenderMainMenu();
}

/**
 * Function : RenderMainMenu
 */
void Application::RenderMainMenu()
{
	if (ImGui::BeginMainMenuBar())
	{
		MenuExamples();
		MenuSettings();
		MenuHelp();

		ImGui::EndMainMenuBar();
	}

	ShowFPS();
	ShowScene();
	ShowTextures();

	if (mImGuiSettings.ShowCameraSettingsInSeparateWindow)
	{
		ImGui::Begin("Camera settings");
		CameraSettings();
		ImGui::End();
	}
}

/**
 * Function : MenuExamples
 */
void Application::MenuExamples()
{
	if (ImGui::BeginMenu("File"))
	{
		if (ImGui::MenuItem("Save worlds"))
		{
			SaveWorlds();
		}

		ImGui::EndMenu();
	}
}

/**
 * Function : MenuSettings
 */
void Application::MenuSettings()
{
	if (ImGui::BeginMenu("Settings"))
	{
		ShowCameraSettings();
		ImGui::EndMenu();
	}
}

/**
 * Function : MenuHelp
 */
void Application::MenuHelp()
{
	if (ImGui::BeginMenu("Help"))
	{
		ImGui::MenuItem("Show FPS", nullptr, &mImGuiSettings.ShowFPS);
		ImGui::MenuItem("Show Scene settings", nullptr, &mImGuiSettings.ShowSceneSettings);
		ImGui::MenuItem("Show textures", nullptr, &mImGuiSettings.ShowTextures);
		ImGui::MenuItem("Make screenshot", nullptr, &mImGuiSettings.MakeScreenshot);
		ImGui::EndMenu();
	}
}

/**
 * Function : ShowFPS
 */
void Application::ShowFPS()
{
	if (mImGuiSettings.ShowFPS)
	{
		ImGui::Begin("Current FPS:", &mImGuiSettings.ShowFPS);
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::End();
	}
}

/**
 * Function : ShowCameraSettings
 */
void Application::ShowCameraSettings()
{
	if (mImGuiSettings.ShowCameraSettingsInSeparateWindow)
		return;

	bool isEnabled = (CurrentCamera().get() != nullptr);

	if (ImGui::BeginMenu("Camera", isEnabled))
	{
		CameraSettings();

		ImGui::EndMenu();
	}
}

/**
 * Function : CameraSettings
 */
void Application::CameraSettings()
{
	CameraPtr& currentCamera = CurrentCamera();
	DirectX::XMFLOAT2&			angles		= currentCamera->Angles();
	const DirectX::XMFLOAT2&	angleXRange	= currentCamera->AngleXRange();
	const DirectX::XMFLOAT2&	angleYRange	= currentCamera->AngleYRange();

	/*{
		std::string items;
		for (int id = 0; id < mActiveCameras.size(); ++id)
		{
			CameraPtr& camera = mActiveCameras[id];

			items += camera->Name();
			items += '\0';
		}

		if (ImGui::Combo("Cameras", &mCurrentCameraIdx, items.data()))
			SetCurrentCamera(CurrentCamera());
	}*/

	ImGui::Separator();

	if (ImGui::InputFloat3("Camera pos", &currentCamera->Position().x))
		currentCamera->MarkDirty();

	if (ImGui::SliderFloat("Horizontal angle", &angles.x, angleXRange.x, angleXRange.y))
		currentCamera->MarkDirty();

	if (ImGui::SliderFloat("Vertical angle", &angles.y, angleYRange.x, angleYRange.y))
		currentCamera->MarkDirty();

	if (ImGui::SliderFloat("FOV", &currentCamera->FoV(), 10.0f, 90.0f))
		currentCamera->MarkDirty();

	if (ImGui::SliderFloat("zNear", &currentCamera->ZRange().x, 0.1f, currentCamera->ZRange().y))
		currentCamera->MarkDirty();

	if (ImGui::SliderFloat("zFar", &currentCamera->ZRange().y, currentCamera->ZRange().x + 0.1f, 1000.0f))
		currentCamera->MarkDirty();

	ImGui::Checkbox("Show in a separate window.", &mImGuiSettings.ShowCameraSettingsInSeparateWindow);
}

/**
 * Function : ShowScene
 */
void Application::ShowScene()
{
	if (!mImGuiSettings.ShowSceneSettings)
		return;

	Scene& scene = GetActiveWorld()->GetScene();

	ImGui::Begin("Scene", &mImGuiSettings.ShowSceneSettings);

	const auto& modeObjects = scene.GetModeObjects();

	//-- Settings.
	//-- ToDo: Reconsider later.
	{
		DirectX::XMFLOAT4& sunDir = scene.GetConstBufferSunLight().Data().LightDir;
		ImGui::DragFloat3("SunDir", &sunDir.x, 0.05f, -1.0f, 1.0f);
	}

	int elementId = 0;
	for (const auto& [id, objects] : modeObjects)
	{
		for (const auto& obj : objects)
		{
			ImGui::PushID(elementId);
			if (ImGui::CollapsingHeader(obj->GetName().c_str()))
			{
				rttr::instance inst(*obj);
				if (ImGuiHelper::Instance().ShowInstance(inst))
				{
					obj->GetLocalTransform().WorldMatrix(obj->GetTransforms().WorldMatrix);
					obj->GetConstBuffer().UpdateDataOnGPU();
				}
				ImGui::Text("MatType: %s", typeid(*obj->GetMaterial().get()).name());
			}
			ImGui::PopID();

			++elementId;
		}
	}

	ImGui::End();
}

/**
 * Function : ShowTextures
 */
void Application::ShowTextures()
{
	if (!mImGuiSettings.ShowTextures)
		return;

	ImGui::Begin("Textures", &mImGuiSettings.ShowTextures);

	ImGui::SliderFloat("Zoom", &mImGuiSettings.Zoom, 1.0f, 16.0f);
	constexpr DirectX::XMFLOAT2 size(256.0f, 256.0f);
	constexpr DirectX::XMFLOAT2 tooltipSize(256.0f, 256.0f);
	auto& textures = TextureManager::Instance().GetTextures();
	for (auto& [name, texture] : textures)
	{
		std::string strName = StringUtils::Converter::WStringToString(name);
		if (ImGui::CollapsingHeader(strName.c_str()))
		{
			ImGuiHelper::Instance().ShowTexture(texture.get(), size, tooltipSize, mImGuiSettings.Zoom);
		}
	}

	ImGui::End();
}

/**
 * Function : InitializeRenderingResources
 */
void Application::InitializeRenderingResources()
{
	//-- ToDo:
}

/**
 * Function : CreateRootSignatures
 */
void Application::CreateRootSignatures()
{
	//-- ToDo:
}

/**
 * Function : CreatePipelineStates
 */
void Application::CreatePipelineStates()
{
	//-- ToDo:
}

/**
 * Function : AddWorld
 */
WorldPtr& Application::AddWorld(const std::string& name)
{
	mWorlds.emplace_back(std::make_shared<World>(name));

	mWorlds.back()->GetScene().Initialize();

	return mWorlds.back();
}

/**
 * Function : SetActiveWorld
 */
void Application::SetActiveWorld(const WorldPtr& world)
{
	mActiveWorld = world;
}

/**
 * Function : LoadWorlds
 */
void Application::LoadWorlds(const String& path)
{
#if UNICODE
	std::string convertedPath = StringUtils::Converter::WStringToString(SResourceManager::Instance().Find(path));
	std::ifstream readStream(convertedPath);
#else
	std::ifstream readStream(file);
#endif // UNICDE

	if (!readStream.is_open())
	{
		throw;
	}

	nlohmann::json js;
	readStream >> js;

	Deserialize(js);
}

/**
 * Function : SaveWorlds
 */
void Application::SaveWorlds() const
{
	nlohmann::json js;
	Serialize(js);

	std::string dump = js.dump(1, '\t');
	SResourceManager::Instance().SaveFile(SResourceManager::Instance().Find(mSettings.WorldsFile), dump);
}

/**
 * Function : Serialize
 */
void Application::Serialize(nlohmann::json& js) const
{
	using namespace nlohmann;

	auto& jsWorlds = js["worlds"];
	jsWorlds = json::array();

	for (auto& world : mWorlds)
	{
		auto& jsWorld = json::object();

		world->Serialize(jsWorld);
		if (world == mActiveWorld)
		{
			jsWorld["active"] = true;
		}

		jsWorlds.push_back(jsWorld);
	}
}

/**
 * Function : Deserialize
 */
void Application::Deserialize(nlohmann::json& js)
{
	for (auto& jsWorld : js["worlds"])
	{
		auto& world = AddWorld(jsWorld["name"]);

		auto& jsScene = jsWorld["scene"];

		world->Deserialize(jsScene);

		if (jsWorld.find("active") != jsWorld.end())
		{
			SetActiveWorld(world);
		}
	}
}
