#pragma once
#include "StdafxClient.h"
#include "IApp.h"
#include <Camera.h>
#include <IImGui.h>
#include <ImageFormat.h>
#include <Render/FrameRenderGraph.h>
#include <Render/GraphicsAPI.h>
#include <Render/IRenderer.h>
#include <Render/Shader.h>
#include <Serialize/ISerializable.h>
#include <World.h>
#include <Window.h>

struct AppDesc
{
	WindowDesc	wndDesc;
	GraphicsAPI	gAPI;
	CmdLine		cmdLine;
};

class Application : public IApp, public ISerializable
{
protected:
	struct Settings
	{
		String	ScreenShotsFolder;
		String	LogFile;
		String	WorldsFile;
		String	DataFolder;
	};

public:
	Application() { }
	~Application() { }

	void Release();
	bool Initialize(const AppDesc* desc);

	void Run();

	void Stop();

	FORCEINLINE const Window* GetWindow() const override final { return &mWindow; }

public: //-- ISerializable.
	void Serialize(nlohmann::json& js) const override final;
	void Deserialize(nlohmann::json& js) override final;

public: //-- world.
	WorldPtr& AddWorld(const std::string& name) override final;
	void SetActiveWorld(const WorldPtr& world) override final;
	FORCEINLINE WorldPtr& GetActiveWorld() override final { return mActiveWorld; }

public:
	FORCEINLINE CameraPtr& CurrentCamera() { return GetActiveWorld()->GetScene().GetActiveCamera(); }

protected:
	void InitializeRenderingResources();
	void CreateRootSignatures();
	void CreatePipelineStates();

	void SaveWorlds() const;
	void LoadWorlds(const String& path);

protected:
	void Update(const float deltaTime);
	void Render();

protected:
	void SetScreenShotsFolder(const String& folder);
	String GetScreenShotName(const ImageFormat format);
	void SaveScreenInFile(const String& file, const ImageFormat format);

	void ParseCommandLine(CmdLine cmdLine, Settings& settings);

protected: //! part of ImGui.
	IMGUI_API LRESULT ImGuiWndProcHandler(HWND, UINT msg, WPARAM wParam, LPARAM lParam);

	void ImGuiPlaceWidgets();

	void RenderMainMenu();

	void ShowFPS();
	void ShowScene();
	void ShowTextures();

	void ShowCameraSettings();
	void CameraSettings();

protected: //! part of main menu.
	void MenuExamples();
	void MenuSettings();
	void MenuHelp();

protected:
	void MouseMoveHandle(const MouseEventParams& params);
	void SetMousePosition(const DirectX::XMINT2& pos);

	void MouseWheelHandler(float delta);

protected:
	Window							mWindow;
	bool							mIsWorking;

	//! part of ImGui
	struct ImGuiSettings
	{
		bool ShowFPS							= true;
		bool ShowSceneSettings					= true;
		bool ShowTextures						= false;
		bool ShowCameraSettingsInSeparateWindow	= false;
		bool MakeScreenshot						= false;
		float Zoom								= 1.0f;
	} mImGuiSettings;

	String							mScreenShotsFolder;

	std::list<WorldPtr>				mWorlds;
	WorldPtr						mActiveWorld;

	Settings						mSettings;
	SceneObjectPtr					mFloor;
};
